﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ValuePlus.Service
{
    public interface ILogService
    {
        void LogDetail(string stationId, Guid pageId, string url, string type, float loadSeconds, DateTime createTime);
    }
}
