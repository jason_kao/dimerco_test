﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ValuePlus.Domain.eSM;

namespace ValuePlus.Service
{
    public interface ICountryService
    {
        IList<SMCountry> GetList();
    }
}
