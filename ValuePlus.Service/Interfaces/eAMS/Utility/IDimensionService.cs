﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ValuePlus.Domain.eChainVP;

namespace ValuePlus.Service.eAMS.Utility
{
    public interface IDimensionService
    {
        IList<AEHAWBDim> GetList(String Mode, String SourceID);
        IList<AEHAWBDim> UpDateData(List<AEHAWBDim> objs);
    }
}
