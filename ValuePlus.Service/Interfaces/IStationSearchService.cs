﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ValuePlus.ViewModel;

namespace ValuePlus.Service
{
    public interface IStationSearchService
    {
        IList<StationSearchViewModel> GetList();
        StationSearchViewModel GetData(int hqId);
        void Save(StationSearchViewModel vm);
    }
}
