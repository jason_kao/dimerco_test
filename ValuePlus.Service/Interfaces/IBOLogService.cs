﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ValuePlus.Service
{
    public interface IBOLogService
    {
        void LogService(string entryClass, string functionName, string functionParameters, string ip,decimal executionSeconds);
        void LogAction(string controller, string action, string parameters, string ip, decimal executionSeconds);
        void LogException(string controller, string action, string parameters, string ip, decimal executionSeconds, string exceptionMessage);
    }
}
