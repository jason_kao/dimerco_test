﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ValuePlus.Domain;

namespace ValuePlus.Service
{
    public interface IspSAGetGroupScoreCardService
    {
        IList<spSAGetGroupScoreCard_Result> GetList(String partyType, String partyID, String Year, String Month, String UserID);
    }
}
