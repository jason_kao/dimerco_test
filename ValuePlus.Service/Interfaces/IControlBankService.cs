﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ValuePlus.Domain.eChainVP;

namespace ValuePlus.Service
{
    public interface IControlBankService
    {
        IList<AEControlBank> GetList(string Number, string txtNumber, string ddlDateFT, DateTime DateFrom, DateTime DateTo);
    }
}
