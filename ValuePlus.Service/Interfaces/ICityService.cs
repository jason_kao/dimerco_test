﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ValuePlus.Domain.eSM;

namespace ValuePlus.Service
{
    public interface ICityService
    {
        IList<SMCity> GetList(int countryId, int stateId);
        IList<SMCity> GetListByText(string text);
    }
}
