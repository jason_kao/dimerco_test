﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ValuePlus.Dao;
using ValuePlus.Domain.eChainVP;
using ValuePlus.Service.eAMS.Utility;

namespace ValuePlus.Service
{
    public class DimensionService:IDimensionService
    {
        private GenericRepository_LocaleChainVP<AEHAWBDim> repCity = new GenericRepository_LocaleChainVP<AEHAWBDim>();

        public IList<AEHAWBDim> GetList(String Mode, String SourceID)
        {
            int HAWBID = Convert.ToInt32(SourceID);
            var list = repCity.GetAll().Where(p => p.HAWBID == HAWBID).ToList();
            return list;
        }

        public IList<AEHAWBDim> UpDateData(List<AEHAWBDim> objs)
        {
            int HAWBID = 0;
            if (objs.Count != 0)
                HAWBID = objs[0].HAWBID;
            //update data need to modify
            var list = repCity.GetAll().Where(p => p.HAWBID == HAWBID).ToList();
            return list;
        }
    }
}
