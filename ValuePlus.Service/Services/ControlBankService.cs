﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ValuePlus.Dao;
using ValuePlus.Domain.eChainVP;


namespace ValuePlus.Service
{
    public class ControlBankService : IControlBankService
    {
        private GenericRepository<AEControlBank> repCity = new GenericRepository<AEControlBank>();

        public IList<AEControlBank> GetList(string Number, string txtNumber, string ddlDateFT, DateTime DateFrom, DateTime DateTo)
        {

            var db = Entity<eChainVP_Entities>.Db();
            var list = (from station in db.AEControlBank
                        //join city in db.SMCities on station.City equals city.HQID
                        where ((Number == "MAWB#" && (station.MAWBNo == txtNumber || txtNumber == "")) ||
                                 (Number == "Prefix" && (station.MAWBNo.Contains(txtNumber) || txtNumber == ""))) ||
                                 ((ddlDateFT == "Received" && (station.ReceivedDate >= DateFrom || DateFrom.ToString() == "")) ||
                                 (ddlDateFT == "Received" && (station.ReceivedDate <= DateTo || DateTo.ToString() == ""))) ||
                                 ((ddlDateFT == "Used" && (station.AssignedDate >= DateFrom || DateFrom.ToString() == "")) ||
                                 (ddlDateFT == "Used" && (station.AssignedDate <= DateTo || DateTo.ToString() == "")))
                        select new AEControlBank()
                        {
                            Issuer = station.Issuer,
                            LotNo = station.LotNo,
                            MAWBNo = station.MAWBNo,
                            ReceivedDate = station.ReceivedDate
                        }).OrderBy(p => p.MAWBNo).ToList();
            return list;
        }
    }
}