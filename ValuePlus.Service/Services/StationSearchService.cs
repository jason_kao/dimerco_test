﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ValuePlus.Dao;
using ValuePlus.Domain.eSM;
using ValuePlus.ViewModel;

namespace ValuePlus.Service
{
    public class StationSearchService: IStationSearchService
    {
        private GenericRepository<SMStation> repStation = new GenericRepository<SMStation>();

        public IList<StationSearchViewModel> GetList()
        {
            var db = Entity<eSM_Entities>.Db();

            var list = (from station in db.SMStation
                        join city in db.SMCity on station.CityID equals city.HQID
                        join country in db.SMCountry on city.CountryID equals country.HQID
                        select new StationSearchViewModel()
                        {
                            StationId = station.HQID,
                            StationCode = station.StationCode,
                            StationName = station.StationName,
                            StationAddress1 = station.StationAddress1,
                            CityId = city.HQID,
                            CityCode = city.CityCode,
                            CountryId = country.HQID,
                            CountryCode = country.CountryCode,
                            StateId= city.StateID,
                            StationZIP = station.StationZIP,
                            Status = station.Status,
                            LocalWebURL = station.LocalWebURL,
                            LocalWebPort = station.LocalWebPort
                        }).OrderBy(p => p.StationCode).ToList();

            return list;
        }

        public StationSearchViewModel GetData(int hqId)
        {
            var db = Entity<eSM_Entities>.Db();

            var data = (from station in db.SMStation
                       join city in db.SMCity on station.CityID equals city.HQID
                       join country in db.SMCountry on city.CountryID equals country.HQID
                       where station.HQID == hqId
                        select new StationSearchViewModel()
                       {
                           StationId = station.HQID,
                           StationCode = station.StationCode,
                           StationName = station.StationName,
                           StationAddress1 = station.StationAddress1,
                           CityId = city.HQID,
                           CityCode = city.CityCode,
                           CountryId = country.HQID,
                           CountryCode = country.CountryCode,
                           StateId = city.StateID,
                           StationZIP = station.StationZIP,
                           Status = station.Status,
                           LocalWebURL = station.LocalWebURL,
                           LocalWebPort = station.LocalWebPort
                       }).FirstOrDefault();
        
            return data;
        }

        public void Save(StationSearchViewModel vm)
        {

            var data = repStation.GetFirstOrDefault(p => p.HQID == vm.StationId);
            data.StationName = vm.StationName;
            data.StationAddress1 = vm.StationAddress1;
            data.CityID = vm.CityId;
            data.StationZIP = vm.StationZIP;

            repStation.Save();
        }
    }
}
