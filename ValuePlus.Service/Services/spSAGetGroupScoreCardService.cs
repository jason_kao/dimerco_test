﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ValuePlus.Dao;
using ValuePlus.Domain;


namespace ValuePlus.Service
{
    public class spSAGetGroupScoreCardService: IspSAGetGroupScoreCardService
    {
        private GenericRepository_eSAM<spSAGetGroupScoreCardService> rep= new GenericRepository_eSAM<spSAGetGroupScoreCardService>();

        public IList<spSAGetGroupScoreCard_Result> GetList(String partyType, String partyID, String Year, String Month, String UserID)
        {
            //var list = rep.GetQueryable()
            //return list;

            var list = new eChainVP_eSAM_EmptyEntities().spSAGetGroupScoreCard(partyType, partyID, Year, Month, UserID);
            return list;
        }
    }
}
