﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ValuePlus.Dao;
using ValuePlus.Domain.eSM;

namespace ValuePlus.Service
{
    public class CityService:ICityService
    {
        private GenericRepository<SMCity> repCity = new GenericRepository<SMCity>();

        public IList<SMCity> GetList(int countryId,int stateId)
        {
            var list = repCity.GetAll().Where(p => p.CountryID == countryId && p.StateID == stateId).ToList();
            return list;
        }

        public IList<SMCity> GetListByText(string text)
        {
            var list = repCity.GetAll().Where(p => p.CityName.StartsWith(text)).ToList();
            return list;
        }
    }
}
