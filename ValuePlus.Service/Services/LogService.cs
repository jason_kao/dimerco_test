﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ValuePlus.Dao;
using ValuePlus.Domain.eChainVP;

namespace ValuePlus.Service
{
    public class LogService: ILogService
    {
        private GenericRepository_LocaleChainVP<LogDetail> repLogDetail = new GenericRepository_LocaleChainVP<LogDetail>();

        public void LogDetail(string stationId, Guid pageId, string url, string type, float loadSeconds, DateTime createTime)
        {
            var log = new LogDetail();

            log.StationId = stationId;
            log.Url = url;
            log.LoadSeconds = loadSeconds;
            log.CreateTime = createTime;
            log.Type = type;
            log.PageId = pageId;
            repLogDetail.Create(log);
            repLogDetail.Save();
        }
    }
}
