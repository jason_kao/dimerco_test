﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ValuePlus.Dao;
using ValuePlus.Domain.eSM;

namespace ValuePlus.Service
{
    public class StateService:IStateService
    {
        private GenericRepository<SMState> repCountry = new GenericRepository<SMState>();

        public IList<SMState> GetList(int countryId)
        {
            var list = repCountry.GetAll().Where(p => p.CountryID == countryId).ToList();
            return list;
        }
    }
}
