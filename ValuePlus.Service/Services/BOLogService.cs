﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ValuePlus.Dao;
using ValuePlus.Domain.eChainVP;

namespace ValuePlus.Service
{
    public class BOLogService: IBOLogService
    {
        private GenericRepository_LocaleChainVP<BOLogDetails> repLogDetail = new GenericRepository_LocaleChainVP<BOLogDetails>();

        public void LogService(string entryClass, string functionName, string functionParameters, string ip, decimal executionSeconds)
        {
            var logDetail = new BOLogDetails();
            logDetail.CreatedTime = DateTime.UtcNow;
            logDetail.ClientIP = ip;
            logDetail.FunctionName = functionName;
            logDetail.FunctionParameters = functionParameters;
            logDetail.EntryClass = entryClass;
            logDetail.ExecutionSeconds = executionSeconds;

            repLogDetail.Create(logDetail);
            repLogDetail.Save();
        }

        public void LogAction(string controller, string action, string parameters, string ip, decimal executionSeconds)
        {
            var logDetail = new BOLogDetails();
            logDetail.CreatedTime = DateTime.UtcNow;
            logDetail.ClientIP = ip;
            logDetail.FunctionName = action;
            logDetail.FunctionParameters = parameters;
            logDetail.EntryClass = controller;
            logDetail.ExecutionSeconds = executionSeconds;

            repLogDetail.Create(logDetail);
            repLogDetail.Save();
        }

        public void LogException(string controller, string action, string parameters, string ip, decimal executionSeconds,string exceptionMessage)
        {
            var logDetail = new BOLogDetails();
            logDetail.CreatedTime = DateTime.UtcNow;
            logDetail.ClientIP = ip;
            logDetail.FunctionName = action;
            logDetail.FunctionParameters = parameters;
            logDetail.EntryClass = controller;
            logDetail.ExecutionSeconds = executionSeconds;
            logDetail.ExceptionMessage = exceptionMessage;

            repLogDetail.Create(logDetail);
            repLogDetail.Save();
        }
    }
}
