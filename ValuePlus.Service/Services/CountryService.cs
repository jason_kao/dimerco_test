﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ValuePlus.Dao;
using ValuePlus.Domain.eSM;

namespace ValuePlus.Service
{
    public class CountryService:ICountryService
    {
        private GenericRepository<SMCountry> repCountry = new GenericRepository<SMCountry>();

        public IList<SMCountry> GetList()
        {
            var list = repCountry.GetAll().ToList();
            return list;
        }
    }
}
