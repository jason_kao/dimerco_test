﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Linq.Expressions;
using System.Data.Entity.Validation;
using ValuePlus.Domain.eSAM;

namespace ValuePlus.Dao
{
    public class GenericRepository_eSAM<TEntity> where TEntity : class
    {
        private DbContext _db;
        public DbContext Db
        {
            get
            {
                return _db ?? Entity<eSAM_Entities>.Db();
            }
            set
            {
                _db = value;
            }
        }

        private DbSet<TEntity> objset;
        private DbSet<TEntity> ObjectSet
        {
            get
            {
                return objset ?? (objset = Db.Set<TEntity>());
            }
        }

        public string EntityName
        {
            get
            {
                return typeof(TEntity).Name;
            }
        }

        public virtual DbSet<TEntity> GetAll()
        {
            return ObjectSet;
        }

        public virtual void Create(TEntity entity)
        {
            ObjectSet.Add(entity);
        }

        public virtual void CreateRange(IEnumerable<TEntity> entites)
        {
            ObjectSet.AddRange(entites);
        }

        public virtual void Delete(TEntity entity)
        {
            ObjectSet.Remove(entity);
        }

        public virtual TEntity Find(params object[] keyValue)
        {
            return Db.Set<TEntity>().Find(keyValue);

        }

        public virtual TEntity GetFirstOrDefault(Expression<Func<TEntity, bool>> expression)
        {
            return Db.Set<TEntity>().FirstOrDefault(expression);
        }

        public virtual IList<TEntity> Where(Expression<Func<TEntity, bool>> expression)
        {
            return ObjectSet.Where(expression).ToList();
        }

        public virtual IQueryable<TEntity> GetQueryable(Expression<Func<TEntity, bool>> expression)
        {
            return ObjectSet.Where(expression);
        }

        public virtual IQueryable<TEntity> Include(Expression<Func<TEntity, object>> expression)
        {
            return Db.Set<TEntity>().Include(expression);

        }

        public virtual void Save()
        {
            try
            {
                Db.SaveChanges();
            }
            catch (DbEntityValidationException ex)
            {
                var errorMessage =
                    ex.EntityValidationErrors.SelectMany(x => x.ValidationErrors).Select(x => x.ErrorMessage);

                var errorMessages = string.Join("; ", errorMessage);

                var exceptionMessage = string.Concat(ex.Message, "errors are: ", errorMessages);


                throw ex;
            }
        }

    }
}
