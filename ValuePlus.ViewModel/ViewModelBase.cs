﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Linq;
using System.Web.ModelBinding;

namespace ValuePlus.ViewModel
{
    public class ViewModelBase: IValidatableObject
    {

        public IDictionary<string, string> Errors = new Dictionary<string, string>();

        protected IList<ValidationResult> Results = new List<ValidationResult>();

        public virtual IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            return Results;
        }

    }
}
