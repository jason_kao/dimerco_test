using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ValuePlus.ViewModel;



namespace ValuePlus.ViewModel.LocaleChainVP
{
    
    /// <summary>
    /// AEColoadBooking class
    /// </summary>
    //[MetadataType(typeof(AEColoadBookingViewModel))]
    //public  partial class AEColoadBooking
    //{
    
    	/// <summary>
    	/// AEColoadBooking Metadata class
    	/// </summary>
    	public   class AEColoadBookingViewModel:ViewModelBase
    	{
    		    
    		/// <summary>
    		/// ID
    		/// </summary>        
    	//    [DisplayName("ID")]
            [Required(ErrorMessage = "ID is required")]
    		public int  ID { get; set; }
    
    		    
    		/// <summary>
    		/// Booking ID
    		/// </summary>        
    	//    [DisplayName("Booking ID")]
            [Required(ErrorMessage = "Booking ID is required")]
    		public int  BookingID { get; set; }
    
    		    
    		/// <summary>
    		/// Coload ID
    		/// </summary>        
    	//    [DisplayName("Coload ID")]
    		public Nullable<int>  ColoadID { get; set; }
    
    		    
    		/// <summary>
    		/// Link Station
    		/// </summary>        
    	//    [DisplayName("Link Station")]
            [MaxLength(10, ErrorMessage = "Link Station cannot be longer than 10 characters")]
    		public string  LinkStation { get; set; }
    
    		    
    		/// <summary>
    		/// DEPT
    		/// </summary>        
    	//    [DisplayName("DEPT")]
    		public Nullable<int>  DEPT { get; set; }
    
    		    
    		/// <summary>
    		/// DSTN
    		/// </summary>        
    	//    [DisplayName("DSTN")]
    		public Nullable<int>  DSTN { get; set; }
    
    		    
    		/// <summary>
    		/// Shipper
    		/// </summary>        
    	//    [DisplayName("Shipper")]
    		public Nullable<int>  Shipper { get; set; }
    
    		    
    		/// <summary>
    		/// Shipper Name
    		/// </summary>        
    	//    [DisplayName("Shipper Name")]
            [MaxLength(100, ErrorMessage = "Shipper Name cannot be longer than 100 characters")]
    		public string  ShipperName { get; set; }
    
    		    
    		/// <summary>
    		/// Shipper Address
    		/// </summary>        
    	//    [DisplayName("Shipper Address")]
            [MaxLength(200, ErrorMessage = "Shipper Address cannot be longer than 200 characters")]
    		public string  ShipperAddress { get; set; }
    
    		    
    		/// <summary>
    		/// Shipper Map
    		/// </summary>        
    	//    [DisplayName("Shipper Map")]
    		public Nullable<int>  ShipperMap { get; set; }
    
    		    
    		/// <summary>
    		/// CNEE
    		/// </summary>        
    	//    [DisplayName("CNEE")]
    		public Nullable<int>  CNEE { get; set; }
    
    		    
    		/// <summary>
    		/// CNEEName
    		/// </summary>        
    	//    [DisplayName("CNEEName")]
            [MaxLength(100, ErrorMessage = "CNEEName cannot be longer than 100 characters")]
    		public string  CNEEName { get; set; }
    
    		    
    		/// <summary>
    		/// CNEEAddress
    		/// </summary>        
    	//    [DisplayName("CNEEAddress")]
            [MaxLength(200, ErrorMessage = "CNEEAddress cannot be longer than 200 characters")]
    		public string  CNEEAddress { get; set; }
    
    		    
    		/// <summary>
    		/// CNEEMap
    		/// </summary>        
    	//    [DisplayName("CNEEMap")]
    		public Nullable<int>  CNEEMap { get; set; }
    
    		    
    		/// <summary>
    		/// Notify
    		/// </summary>        
    	//    [DisplayName("Notify")]
    		public Nullable<int>  Notify { get; set; }
    
    		    
    		/// <summary>
    		/// Notify Name
    		/// </summary>        
    	//    [DisplayName("Notify Name")]
            [MaxLength(100, ErrorMessage = "Notify Name cannot be longer than 100 characters")]
    		public string  NotifyName { get; set; }
    
    		    
    		/// <summary>
    		/// Notify Address
    		/// </summary>        
    	//    [DisplayName("Notify Address")]
            [MaxLength(200, ErrorMessage = "Notify Address cannot be longer than 200 characters")]
    		public string  NotifyAddress { get; set; }
    
    		    
    		/// <summary>
    		/// Notify Map
    		/// </summary>        
    	//    [DisplayName("Notify Map")]
    		public Nullable<int>  NotifyMap { get; set; }
    
    		    
    		/// <summary>
    		/// Avail Date
    		/// </summary>        
    	//    [DisplayName("Avail Date")]
    		public Nullable<System.DateTime>  AvailDate { get; set; }
    
    		    
    		/// <summary>
    		/// Dept Date
    		/// </summary>        
    	//    [DisplayName("Dept Date")]
    		public Nullable<System.DateTime>  DeptDate { get; set; }
    
    		    
    		/// <summary>
    		/// Arri Date
    		/// </summary>        
    	//    [DisplayName("Arri Date")]
    		public Nullable<System.DateTime>  ArriDate { get; set; }
    
    		    
    		/// <summary>
    		/// PCS
    		/// </summary>        
    	//    [DisplayName("PCS")]
    		public Nullable<int>  PCS { get; set; }
    
    		    
    		/// <summary>
    		/// PCSUOM
    		/// </summary>        
    	//    [DisplayName("PCSUOM")]
            [MaxLength(20, ErrorMessage = "PCSUOM cannot be longer than 20 characters")]
    		public string  PCSUOM { get; set; }
    
    		    
    		/// <summary>
    		/// GWT
    		/// </summary>        
    	//    [DisplayName("GWT")]
    		public Nullable<decimal>  GWT { get; set; }
    
    		    
    		/// <summary>
    		/// GWTUOM
    		/// </summary>        
    	//    [DisplayName("GWTUOM")]
            [MaxLength(20, ErrorMessage = "GWTUOM cannot be longer than 20 characters")]
    		public string  GWTUOM { get; set; }
    
    		    
    		/// <summary>
    		/// Volume
    		/// </summary>        
    	//    [DisplayName("Volume")]
    		public Nullable<decimal>  Volume { get; set; }
    
    		    
    		/// <summary>
    		/// Volume UOM
    		/// </summary>        
    	//    [DisplayName("Volume UOM")]
            [MaxLength(20, ErrorMessage = "Volume UOM cannot be longer than 20 characters")]
    		public string  VolumeUOM { get; set; }
    
    		    
    		/// <summary>
    		/// Descript
    		/// </summary>        
    	//    [DisplayName("Descript")]
            [MaxLength(200, ErrorMessage = "Descript cannot be longer than 200 characters")]
    		public string  Descript { get; set; }
    
    		    
    		/// <summary>
    		/// Export LIC
    		/// </summary>        
    	//    [DisplayName("Export LIC")]
            [MaxLength(200, ErrorMessage = "Export LIC cannot be longer than 200 characters")]
    		public string  ExportLIC { get; set; }
    
    		    
    		/// <summary>
    		/// Comm INV
    		/// </summary>        
    	//    [DisplayName("Comm INV")]
            [MaxLength(200, ErrorMessage = "Comm INV cannot be longer than 200 characters")]
    		public string  CommINV { get; set; }
    
    		    
    		/// <summary>
    		/// Quantity
    		/// </summary>        
    	//    [DisplayName("Quantity")]
            [MaxLength(200, ErrorMessage = "Quantity cannot be longer than 200 characters")]
    		public string  Quantity { get; set; }
    
    		    
    		/// <summary>
    		/// Remarks
    		/// </summary>        
    	//    [DisplayName("Remarks")]
            [MaxLength(500, ErrorMessage = "Remarks cannot be longer than 500 characters")]
    		public string  Remarks { get; set; }
    
    		    
    		/// <summary>
    		/// Marks
    		/// </summary>        
    	//    [DisplayName("Marks")]
            [MaxLength(500, ErrorMessage = "Marks cannot be longer than 500 characters")]
    		public string  Marks { get; set; }
    
    		    
    		/// <summary>
    		/// Created By
    		/// </summary>        
    	//    [DisplayName("Created By")]
            [MaxLength(50, ErrorMessage = "Created By cannot be longer than 50 characters")]
    		public string  CreatedBy { get; set; }
    
    		    
    		/// <summary>
    		/// Created Date
    		/// </summary>        
    	//    [DisplayName("Created Date")]
    		public Nullable<System.DateTime>  CreatedDate { get; set; }
    
    		    
    		/// <summary>
    		/// Updated By
    		/// </summary>        
    	//    [DisplayName("Updated By")]
            [MaxLength(50, ErrorMessage = "Updated By cannot be longer than 50 characters")]
    		public string  UpdatedBy { get; set; }
    
    		    
    		/// <summary>
    		/// Updated Date
    		/// </summary>        
    	//    [DisplayName("Updated Date")]
    		public Nullable<System.DateTime>  UpdatedDate { get; set; }
    
    		    
    		/// <summary>
    		/// Received Date
    		/// </summary>        
    	//    [DisplayName("Received Date")]
    		public Nullable<System.DateTime>  ReceivedDate { get; set; }
    
    		    
    		/// <summary>
    		/// Processed Date
    		/// </summary>        
    	//    [DisplayName("Processed Date")]
    		public Nullable<System.DateTime>  ProcessedDate { get; set; }
    
    		    
    		/// <summary>
    		/// Status
    		/// </summary>        
    	//    [DisplayName("Status")]
            [MaxLength(10, ErrorMessage = "Status cannot be longer than 10 characters")]
    		public string  Status { get; set; }
    
    		    
    		/// <summary>
    		/// AWBType
    		/// </summary>        
    	//    [DisplayName("AWBType")]
            [MaxLength(10, ErrorMessage = "AWBType cannot be longer than 10 characters")]
    		public string  AWBType { get; set; }
    
    		    
    		/// <summary>
    		/// HAWBID
    		/// </summary>        
    	//    [DisplayName("HAWBID")]
    		public Nullable<int>  HAWBID { get; set; }
    
    		    
    		/// <summary>
    		/// Service Level
    		/// </summary>        
    	//    [DisplayName("Service Level")]
            [MaxLength(50, ErrorMessage = "Service Level cannot be longer than 50 characters")]
    		public string  ServiceLevel { get; set; }
    
    		    
    		/// <summary>
    		/// Doc Type
    		/// </summary>        
    	//    [DisplayName("Doc Type")]
            [MaxLength(50, ErrorMessage = "Doc Type cannot be longer than 50 characters")]
    		public string  DocType { get; set; }
    
    		    
    		/// <summary>
    		/// Rate
    		/// </summary>        
    	//    [DisplayName("Rate")]
            [MaxLength(50, ErrorMessage = "Rate cannot be longer than 50 characters")]
    		public string  Rate { get; set; }
    
    		    
    		/// <summary>
    		/// Carrier
    		/// </summary>        
    	//    [DisplayName("Carrier")]
            [MaxLength(50, ErrorMessage = "Carrier cannot be longer than 50 characters")]
    		public string  Carrier { get; set; }
    
    		    
    	}
    //}
    
}
