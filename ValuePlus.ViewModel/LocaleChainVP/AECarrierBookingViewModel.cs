using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ValuePlus.ViewModel;



namespace ValuePlus.ViewModel.LocaleChainVP
{
    
    /// <summary>
    /// AECarrierBooking class
    /// </summary>
    //[MetadataType(typeof(AECarrierBookingViewModel))]
    //public  partial class AECarrierBooking
    //{
    
    	/// <summary>
    	/// AECarrierBooking Metadata class
    	/// </summary>
    	public   class AECarrierBookingViewModel:ViewModelBase
    	{
    		    
    		/// <summary>
    		/// ID
    		/// </summary>        
    	//    [DisplayName("ID")]
            [Required(ErrorMessage = "ID is required")]
    		public int  ID { get; set; }
    
    		    
    		/// <summary>
    		/// MAWBID
    		/// </summary>        
    	//    [DisplayName("MAWBID")]
            [Required(ErrorMessage = "MAWBID is required")]
    		public int  MAWBID { get; set; }
    
    		    
    		/// <summary>
    		/// DEPT
    		/// </summary>        
    	//    [DisplayName("DEPT")]
            [Required(ErrorMessage = "DEPT is required")]
    		public int  DEPT { get; set; }
    
    		    
    		/// <summary>
    		/// DSTN
    		/// </summary>        
    	//    [DisplayName("DSTN")]
            [Required(ErrorMessage = "DSTN is required")]
    		public int  DSTN { get; set; }
    
    		    
    		/// <summary>
    		/// Booked FLT
    		/// </summary>        
    	//    [DisplayName("Booked FLT")]
            [MaxLength(20, ErrorMessage = "Booked FLT cannot be longer than 20 characters")]
    		public string  BookedFLT { get; set; }
    
    		    
    		/// <summary>
    		/// Onboard FLT
    		/// </summary>        
    	//    [DisplayName("Onboard FLT")]
            [MaxLength(20, ErrorMessage = "Onboard FLT cannot be longer than 20 characters")]
    		public string  OnboardFLT { get; set; }
    
    		    
    		/// <summary>
    		/// ETD
    		/// </summary>        
    	//    [DisplayName("ETD")]
    		public Nullable<System.DateTime>  ETD { get; set; }
    
    		    
    		/// <summary>
    		/// ETA
    		/// </summary>        
    	//    [DisplayName("ETA")]
    		public Nullable<System.DateTime>  ETA { get; set; }
    
    		    
    		/// <summary>
    		/// ATA
    		/// </summary>        
    	//    [DisplayName("ATA")]
    		public Nullable<System.DateTime>  ATA { get; set; }
    
    		    
    		/// <summary>
    		/// ATD
    		/// </summary>        
    	//    [DisplayName("ATD")]
    		public Nullable<System.DateTime>  ATD { get; set; }
    
    		    
    		/// <summary>
    		/// Station ID
    		/// </summary>        
    	//    [DisplayName("Station ID")]
            [Required(ErrorMessage = "Station ID is required")]
            [MaxLength(10, ErrorMessage = "Station ID cannot be longer than 10 characters")]
    		public string  StationID { get; set; }
    
    		    
    		/// <summary>
    		/// Created By
    		/// </summary>        
    	//    [DisplayName("Created By")]
            [Required(ErrorMessage = "Created By is required")]
            [MaxLength(6, ErrorMessage = "Created By cannot be longer than 6 characters")]
    		public string  CreatedBy { get; set; }
    
    		    
    		/// <summary>
    		/// Created Date
    		/// </summary>        
    	//    [DisplayName("Created Date")]
            [Required(ErrorMessage = "Created Date is required")]
    		public System.DateTime  CreatedDate { get; set; }
    
    		    
    		/// <summary>
    		/// Updated By
    		/// </summary>        
    	//    [DisplayName("Updated By")]
            [Required(ErrorMessage = "Updated By is required")]
            [MaxLength(6, ErrorMessage = "Updated By cannot be longer than 6 characters")]
    		public string  UpdatedBy { get; set; }
    
    		    
    		/// <summary>
    		/// Updated Date
    		/// </summary>        
    	//    [DisplayName("Updated Date")]
            [Required(ErrorMessage = "Updated Date is required")]
    		public System.DateTime  UpdatedDate { get; set; }
    
    		    
    		/// <summary>
    		/// PCS
    		/// </summary>        
    	//    [DisplayName("PCS")]
    		public Nullable<int>  PCS { get; set; }
    
    		    
    		/// <summary>
    		/// GWT
    		/// </summary>        
    	//    [DisplayName("GWT")]
    		public Nullable<double>  GWT { get; set; }
    
    		    
    		/// <summary>
    		/// VWT
    		/// </summary>        
    	//    [DisplayName("VWT")]
    		public Nullable<double>  VWT { get; set; }
    
    		    
    		/// <summary>
    		/// CWT
    		/// </summary>        
    	//    [DisplayName("CWT")]
    		public Nullable<double>  CWT { get; set; }
    
    		    
    		/// <summary>
    		/// Source ID
    		/// </summary>        
    	//    [DisplayName("Source ID")]
    		public Nullable<int>  SourceID { get; set; }
    
    		    
    		/// <summary>
    		/// Version
    		/// </summary>        
    	//    [DisplayName("Version")]
            [MaxLength(8, ErrorMessage = "Version cannot be longer than 8 characters")]
    		public byte[]  Version { get; set; }
    
    		    
    		/// <summary>
    		/// PCS1
    		/// </summary>        
    	//    [DisplayName("PCS1")]
    		public Nullable<int>  PCS1 { get; set; }
    
    		    
    		/// <summary>
    		/// GWT1
    		/// </summary>        
    	//    [DisplayName("GWT1")]
    		public Nullable<double>  GWT1 { get; set; }
    
    		    
    		/// <summary>
    		/// VWT1
    		/// </summary>        
    	//    [DisplayName("VWT1")]
    		public Nullable<double>  VWT1 { get; set; }
    
    		    
    		/// <summary>
    		/// CWT1
    		/// </summary>        
    	//    [DisplayName("CWT1")]
    		public Nullable<double>  CWT1 { get; set; }
    
    		    
    		/// <summary>
    		/// Source ID1
    		/// </summary>        
    	//    [DisplayName("Source ID1")]
    		public Nullable<int>  SourceID1 { get; set; }
    
    		    
    	}
    //}
    
}
