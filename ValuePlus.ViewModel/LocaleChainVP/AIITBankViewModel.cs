using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ValuePlus.ViewModel;



namespace ValuePlus.ViewModel.LocaleChainVP
{
    
    /// <summary>
    /// AIITBank class
    /// </summary>
    //[MetadataType(typeof(AIITBankViewModel))]
    //public  partial class AIITBank
    //{
    
    	/// <summary>
    	/// AIITBank Metadata class
    	/// </summary>
    	public   class AIITBankViewModel:ViewModelBase
    	{
    		    
    		/// <summary>
    		/// ID
    		/// </summary>        
    	//    [DisplayName("ID")]
            [Required(ErrorMessage = "ID is required")]
    		public int  ID { get; set; }
    
    		    
    		/// <summary>
    		/// ITNo
    		/// </summary>        
    	//    [DisplayName("ITNo")]
            [Required(ErrorMessage = "ITNo is required")]
            [MaxLength(255, ErrorMessage = "ITNo cannot be longer than 255 characters")]
    		public string  ITNo { get; set; }
    
    		    
    		/// <summary>
    		/// MAWBNo
    		/// </summary>        
    	//    [DisplayName("MAWBNo")]
            [MaxLength(12, ErrorMessage = "MAWBNo cannot be longer than 12 characters")]
    		public string  MAWBNo { get; set; }
    
    		    
    		/// <summary>
    		/// Borrow In
    		/// </summary>        
    	//    [DisplayName("Borrow In")]
            [Required(ErrorMessage = "Borrow In is required")]
    		public int  BorrowIn { get; set; }
    
    		    
    		/// <summary>
    		/// Received Date
    		/// </summary>        
    	//    [DisplayName("Received Date")]
    		public Nullable<System.DateTime>  ReceivedDate { get; set; }
    
    		    
    		/// <summary>
    		/// Borrow Out Date
    		/// </summary>        
    	//    [DisplayName("Borrow Out Date")]
    		public Nullable<System.DateTime>  BorrowOutDate { get; set; }
    
    		    
    		/// <summary>
    		/// Borrow Out By
    		/// </summary>        
    	//    [DisplayName("Borrow Out By")]
            [MaxLength(5, ErrorMessage = "Borrow Out By cannot be longer than 5 characters")]
    		public string  BorrowOutBy { get; set; }
    
    		    
    		/// <summary>
    		/// Borrow Out Agent
    		/// </summary>        
    	//    [DisplayName("Borrow Out Agent")]
    		public Nullable<int>  BorrowOutAgent { get; set; }
    
    		    
    		/// <summary>
    		/// Assigned Date
    		/// </summary>        
    	//    [DisplayName("Assigned Date")]
    		public Nullable<System.DateTime>  AssignedDate { get; set; }
    
    		    
    		/// <summary>
    		/// City
    		/// </summary>        
    	//    [DisplayName("City")]
            [Required(ErrorMessage = "City is required")]
    		public int  City { get; set; }
    
    		    
    		/// <summary>
    		/// Used Date
    		/// </summary>        
    	//    [DisplayName("Used Date")]
    		public Nullable<System.DateTime>  UsedDate { get; set; }
    
    		    
    		/// <summary>
    		/// Used By
    		/// </summary>        
    	//    [DisplayName("Used By")]
            [MaxLength(5, ErrorMessage = "Used By cannot be longer than 5 characters")]
    		public string  UsedBy { get; set; }
    
    		    
    		/// <summary>
    		/// Lot No
    		/// </summary>        
    	//    [DisplayName("Lot No")]
            [MaxLength(8, ErrorMessage = "Lot No cannot be longer than 8 characters")]
    		public string  LotNo { get; set; }
    
    		    
    		/// <summary>
    		/// Status
    		/// </summary>        
    	//    [DisplayName("Status")]
            [MaxLength(10, ErrorMessage = "Status cannot be longer than 10 characters")]
    		public string  Status { get; set; }
    
    		    
    		/// <summary>
    		/// Issuer
    		/// </summary>        
    	//    [DisplayName("Issuer")]
    		public Nullable<int>  Issuer { get; set; }
    
    		    
    		/// <summary>
    		/// Mode
    		/// </summary>        
    	//    [DisplayName("Mode")]
            [Required(ErrorMessage = "Mode is required")]
    		public int  Mode { get; set; }
    
    		    
    		/// <summary>
    		/// Station ID
    		/// </summary>        
    	//    [DisplayName("Station ID")]
            [Required(ErrorMessage = "Station ID is required")]
            [MaxLength(10, ErrorMessage = "Station ID cannot be longer than 10 characters")]
    		public string  StationID { get; set; }
    
    		    
    		/// <summary>
    		/// Created By
    		/// </summary>        
    	//    [DisplayName("Created By")]
            [Required(ErrorMessage = "Created By is required")]
            [MaxLength(6, ErrorMessage = "Created By cannot be longer than 6 characters")]
    		public string  CreatedBy { get; set; }
    
    		    
    		/// <summary>
    		/// Created Date
    		/// </summary>        
    	//    [DisplayName("Created Date")]
            [Required(ErrorMessage = "Created Date is required")]
    		public System.DateTime  CreatedDate { get; set; }
    
    		    
    		/// <summary>
    		/// Updated By
    		/// </summary>        
    	//    [DisplayName("Updated By")]
            [Required(ErrorMessage = "Updated By is required")]
            [MaxLength(6, ErrorMessage = "Updated By cannot be longer than 6 characters")]
    		public string  UpdatedBy { get; set; }
    
    		    
    		/// <summary>
    		/// Updated Date
    		/// </summary>        
    	//    [DisplayName("Updated Date")]
            [Required(ErrorMessage = "Updated Date is required")]
    		public System.DateTime  UpdatedDate { get; set; }
    
    		    
    		/// <summary>
    		/// Version
    		/// </summary>        
    	//    [DisplayName("Version")]
            [MaxLength(8, ErrorMessage = "Version cannot be longer than 8 characters")]
    		public byte[]  Version { get; set; }
    
    		    
    	}
    //}
    
}
