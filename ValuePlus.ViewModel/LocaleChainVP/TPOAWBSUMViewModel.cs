using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ValuePlus.ViewModel;



namespace ValuePlus.ViewModel.LocaleChainVP
{
    
    /// <summary>
    /// TPOAWBSUM class
    /// </summary>
    //[MetadataType(typeof(TPOAWBSUMViewModel))]
    //public  partial class TPOAWBSUM
    //{
    
    	/// <summary>
    	/// TPOAWBSUM Metadata class
    	/// </summary>
    	public   class TPOAWBSUMViewModel:ViewModelBase
    	{
    		    
    		/// <summary>
    		/// STATIONID
    		/// </summary>        
    	//    [DisplayName("STATIONID")]
            [Required(ErrorMessage = "STATIONID is required")]
            [MaxLength(10, ErrorMessage = "STATIONID cannot be longer than 10 characters")]
    		public string  STATIONID { get; set; }
    
    		    
    		/// <summary>
    		/// Source ID
    		/// </summary>        
    	//    [DisplayName("Source ID")]
            [Required(ErrorMessage = "Source ID is required")]
    		public int  SourceID { get; set; }
    
    		    
    		/// <summary>
    		/// IDType
    		/// </summary>        
    	//    [DisplayName("IDType")]
            [Required(ErrorMessage = "IDType is required")]
            [MaxLength(6, ErrorMessage = "IDType cannot be longer than 6 characters")]
    		public string  IDType { get; set; }
    
    		    
    		/// <summary>
    		/// FRT_ ECOST
    		/// </summary>        
    	//    [DisplayName("FRT_ ECOST")]
    		public Nullable<double>  FRT_ECOST { get; set; }
    
    		    
    		/// <summary>
    		/// Other_ ECOST
    		/// </summary>        
    	//    [DisplayName("Other_ ECOST")]
    		public Nullable<double>  Other_ECOST { get; set; }
    
    		    
    		/// <summary>
    		/// Extra_ ECOST
    		/// </summary>        
    	//    [DisplayName("Extra_ ECOST")]
    		public Nullable<double>  Extra_ECOST { get; set; }
    
    		    
    		/// <summary>
    		/// Extra_ FRT_ ECost
    		/// </summary>        
    	//    [DisplayName("Extra_ FRT_ ECost")]
    		public Nullable<double>  Extra_FRT_ECost { get; set; }
    
    		    
    		/// <summary>
    		/// Extra_ Other_ ECost
    		/// </summary>        
    	//    [DisplayName("Extra_ Other_ ECost")]
    		public Nullable<double>  Extra_Other_ECost { get; set; }
    
    		    
    		/// <summary>
    		/// FVAT_ ECOST
    		/// </summary>        
    	//    [DisplayName("FVAT_ ECOST")]
    		public Nullable<double>  FVAT_ECOST { get; set; }
    
    		    
    		/// <summary>
    		/// OVAT_ ECOST
    		/// </summary>        
    	//    [DisplayName("OVAT_ ECOST")]
    		public Nullable<double>  OVAT_ECOST { get; set; }
    
    		    
    		/// <summary>
    		/// EVAT_ ECOST
    		/// </summary>        
    	//    [DisplayName("EVAT_ ECOST")]
    		public Nullable<double>  EVAT_ECOST { get; set; }
    
    		    
    		/// <summary>
    		/// EFVAT_ ECOST
    		/// </summary>        
    	//    [DisplayName("EFVAT_ ECOST")]
    		public Nullable<double>  EFVAT_ECOST { get; set; }
    
    		    
    		/// <summary>
    		/// EOVAT_ ECOST
    		/// </summary>        
    	//    [DisplayName("EOVAT_ ECOST")]
    		public Nullable<double>  EOVAT_ECOST { get; set; }
    
    		    
    		/// <summary>
    		/// FRT_ Sales_ PP
    		/// </summary>        
    	//    [DisplayName("FRT_ Sales_ PP")]
    		public Nullable<double>  FRT_Sales_PP { get; set; }
    
    		    
    		/// <summary>
    		/// Other_ Sales_ PP
    		/// </summary>        
    	//    [DisplayName("Other_ Sales_ PP")]
    		public Nullable<double>  Other_Sales_PP { get; set; }
    
    		    
    		/// <summary>
    		/// Extra_ Sales_ PP
    		/// </summary>        
    	//    [DisplayName("Extra_ Sales_ PP")]
    		public Nullable<double>  Extra_Sales_PP { get; set; }
    
    		    
    		/// <summary>
    		/// Extra_ FRT_ Sales_ PP
    		/// </summary>        
    	//    [DisplayName("Extra_ FRT_ Sales_ PP")]
    		public Nullable<double>  Extra_FRT_Sales_PP { get; set; }
    
    		    
    		/// <summary>
    		/// Extra_ Other_ Sales_ PP
    		/// </summary>        
    	//    [DisplayName("Extra_ Other_ Sales_ PP")]
    		public Nullable<double>  Extra_Other_Sales_PP { get; set; }
    
    		    
    		/// <summary>
    		/// FVAT_ Sales_ PP
    		/// </summary>        
    	//    [DisplayName("FVAT_ Sales_ PP")]
    		public Nullable<double>  FVAT_Sales_PP { get; set; }
    
    		    
    		/// <summary>
    		/// OVAT_ Sales_ PP
    		/// </summary>        
    	//    [DisplayName("OVAT_ Sales_ PP")]
    		public Nullable<double>  OVAT_Sales_PP { get; set; }
    
    		    
    		/// <summary>
    		/// EVAT_ Sales_ PP
    		/// </summary>        
    	//    [DisplayName("EVAT_ Sales_ PP")]
    		public Nullable<double>  EVAT_Sales_PP { get; set; }
    
    		    
    		/// <summary>
    		/// EFVAT_ Sales_ PP
    		/// </summary>        
    	//    [DisplayName("EFVAT_ Sales_ PP")]
    		public Nullable<double>  EFVAT_Sales_PP { get; set; }
    
    		    
    		/// <summary>
    		/// EOVAT_ Sales_ PP
    		/// </summary>        
    	//    [DisplayName("EOVAT_ Sales_ PP")]
    		public Nullable<double>  EOVAT_Sales_PP { get; set; }
    
    		    
    		/// <summary>
    		/// FRT_ Sales_ CC
    		/// </summary>        
    	//    [DisplayName("FRT_ Sales_ CC")]
    		public Nullable<double>  FRT_Sales_CC { get; set; }
    
    		    
    		/// <summary>
    		/// Other_ Sales_ CC
    		/// </summary>        
    	//    [DisplayName("Other_ Sales_ CC")]
    		public Nullable<double>  Other_Sales_CC { get; set; }
    
    		    
    		/// <summary>
    		/// Extra_ Sales_ CC
    		/// </summary>        
    	//    [DisplayName("Extra_ Sales_ CC")]
    		public Nullable<double>  Extra_Sales_CC { get; set; }
    
    		    
    		/// <summary>
    		/// Extra_ FRT_ Sales_ CC
    		/// </summary>        
    	//    [DisplayName("Extra_ FRT_ Sales_ CC")]
    		public Nullable<double>  Extra_FRT_Sales_CC { get; set; }
    
    		    
    		/// <summary>
    		/// Extra_ Other_ Sales_ CC
    		/// </summary>        
    	//    [DisplayName("Extra_ Other_ Sales_ CC")]
    		public Nullable<double>  Extra_Other_Sales_CC { get; set; }
    
    		    
    		/// <summary>
    		/// FVAT_ Sales_ CC
    		/// </summary>        
    	//    [DisplayName("FVAT_ Sales_ CC")]
    		public Nullable<double>  FVAT_Sales_CC { get; set; }
    
    		    
    		/// <summary>
    		/// OVAT_ Sales_ CC
    		/// </summary>        
    	//    [DisplayName("OVAT_ Sales_ CC")]
    		public Nullable<double>  OVAT_Sales_CC { get; set; }
    
    		    
    		/// <summary>
    		/// EVAT_ Sales_ CC
    		/// </summary>        
    	//    [DisplayName("EVAT_ Sales_ CC")]
    		public Nullable<double>  EVAT_Sales_CC { get; set; }
    
    		    
    		/// <summary>
    		/// EFVAT_ Sales_ CC
    		/// </summary>        
    	//    [DisplayName("EFVAT_ Sales_ CC")]
    		public Nullable<double>  EFVAT_Sales_CC { get; set; }
    
    		    
    		/// <summary>
    		/// EOVAT_ Sales_ CC
    		/// </summary>        
    	//    [DisplayName("EOVAT_ Sales_ CC")]
    		public Nullable<double>  EOVAT_Sales_CC { get; set; }
    
    		    
    		/// <summary>
    		/// Rpt_ FRT_ ECOST
    		/// </summary>        
    	//    [DisplayName("Rpt_ FRT_ ECOST")]
    		public Nullable<double>  Rpt_FRT_ECOST { get; set; }
    
    		    
    		/// <summary>
    		/// Rpt_ Other_ ECOST
    		/// </summary>        
    	//    [DisplayName("Rpt_ Other_ ECOST")]
    		public Nullable<double>  Rpt_Other_ECOST { get; set; }
    
    		    
    		/// <summary>
    		/// Rpt_ Extra_ ECOST
    		/// </summary>        
    	//    [DisplayName("Rpt_ Extra_ ECOST")]
    		public Nullable<double>  Rpt_Extra_ECOST { get; set; }
    
    		    
    		/// <summary>
    		/// Rpt_ Extra_ FRT_ ECost
    		/// </summary>        
    	//    [DisplayName("Rpt_ Extra_ FRT_ ECost")]
    		public Nullable<double>  Rpt_Extra_FRT_ECost { get; set; }
    
    		    
    		/// <summary>
    		/// Rpt_ Extra_ Other_ ECost
    		/// </summary>        
    	//    [DisplayName("Rpt_ Extra_ Other_ ECost")]
    		public Nullable<double>  Rpt_Extra_Other_ECost { get; set; }
    
    		    
    		/// <summary>
    		/// Rpt_ FVAT_ ECOST
    		/// </summary>        
    	//    [DisplayName("Rpt_ FVAT_ ECOST")]
    		public Nullable<double>  Rpt_FVAT_ECOST { get; set; }
    
    		    
    		/// <summary>
    		/// Rpt_ OVAT_ ECOST
    		/// </summary>        
    	//    [DisplayName("Rpt_ OVAT_ ECOST")]
    		public Nullable<double>  Rpt_OVAT_ECOST { get; set; }
    
    		    
    		/// <summary>
    		/// Rpt_ EVAT_ ECOST
    		/// </summary>        
    	//    [DisplayName("Rpt_ EVAT_ ECOST")]
    		public Nullable<double>  Rpt_EVAT_ECOST { get; set; }
    
    		    
    		/// <summary>
    		/// Rpt_ EFVAT_ ECOST
    		/// </summary>        
    	//    [DisplayName("Rpt_ EFVAT_ ECOST")]
    		public Nullable<double>  Rpt_EFVAT_ECOST { get; set; }
    
    		    
    		/// <summary>
    		/// Rpt_ EOVAT_ ECOST
    		/// </summary>        
    	//    [DisplayName("Rpt_ EOVAT_ ECOST")]
    		public Nullable<double>  Rpt_EOVAT_ECOST { get; set; }
    
    		    
    		/// <summary>
    		/// Rpt_ FRT_ Sales_ PP
    		/// </summary>        
    	//    [DisplayName("Rpt_ FRT_ Sales_ PP")]
    		public Nullable<double>  Rpt_FRT_Sales_PP { get; set; }
    
    		    
    		/// <summary>
    		/// Rpt_ Other_ Sales_ PP
    		/// </summary>        
    	//    [DisplayName("Rpt_ Other_ Sales_ PP")]
    		public Nullable<double>  Rpt_Other_Sales_PP { get; set; }
    
    		    
    		/// <summary>
    		/// Rpt_ Extra_ Sales_ PP
    		/// </summary>        
    	//    [DisplayName("Rpt_ Extra_ Sales_ PP")]
    		public Nullable<double>  Rpt_Extra_Sales_PP { get; set; }
    
    		    
    		/// <summary>
    		/// Rpt_ Extra_ FRT_ Sales_ PP
    		/// </summary>        
    	//    [DisplayName("Rpt_ Extra_ FRT_ Sales_ PP")]
    		public Nullable<double>  Rpt_Extra_FRT_Sales_PP { get; set; }
    
    		    
    		/// <summary>
    		/// Rpt_ Extra_ Other_ Sales_ PP
    		/// </summary>        
    	//    [DisplayName("Rpt_ Extra_ Other_ Sales_ PP")]
    		public Nullable<double>  Rpt_Extra_Other_Sales_PP { get; set; }
    
    		    
    		/// <summary>
    		/// Rpt_ FVAT_ Sales_ PP
    		/// </summary>        
    	//    [DisplayName("Rpt_ FVAT_ Sales_ PP")]
    		public Nullable<double>  Rpt_FVAT_Sales_PP { get; set; }
    
    		    
    		/// <summary>
    		/// Rpt_ OVAT_ Sales_ PP
    		/// </summary>        
    	//    [DisplayName("Rpt_ OVAT_ Sales_ PP")]
    		public Nullable<double>  Rpt_OVAT_Sales_PP { get; set; }
    
    		    
    		/// <summary>
    		/// Rpt_ EVAT_ Sales_ PP
    		/// </summary>        
    	//    [DisplayName("Rpt_ EVAT_ Sales_ PP")]
    		public Nullable<double>  Rpt_EVAT_Sales_PP { get; set; }
    
    		    
    		/// <summary>
    		/// Rpt_ EFVAT_ Sales_ PP
    		/// </summary>        
    	//    [DisplayName("Rpt_ EFVAT_ Sales_ PP")]
    		public Nullable<double>  Rpt_EFVAT_Sales_PP { get; set; }
    
    		    
    		/// <summary>
    		/// Rpt_ EOVAT_ Sales_ PP
    		/// </summary>        
    	//    [DisplayName("Rpt_ EOVAT_ Sales_ PP")]
    		public Nullable<double>  Rpt_EOVAT_Sales_PP { get; set; }
    
    		    
    		/// <summary>
    		/// Rpt_ FRT_ Sales_ CC
    		/// </summary>        
    	//    [DisplayName("Rpt_ FRT_ Sales_ CC")]
    		public Nullable<double>  Rpt_FRT_Sales_CC { get; set; }
    
    		    
    		/// <summary>
    		/// Rpt_ Other_ Sales_ CC
    		/// </summary>        
    	//    [DisplayName("Rpt_ Other_ Sales_ CC")]
    		public Nullable<double>  Rpt_Other_Sales_CC { get; set; }
    
    		    
    		/// <summary>
    		/// Rpt_ Extra_ Sales_ CC
    		/// </summary>        
    	//    [DisplayName("Rpt_ Extra_ Sales_ CC")]
    		public Nullable<double>  Rpt_Extra_Sales_CC { get; set; }
    
    		    
    		/// <summary>
    		/// Rpt_ Extra_ FRT_ Sales_ CC
    		/// </summary>        
    	//    [DisplayName("Rpt_ Extra_ FRT_ Sales_ CC")]
    		public Nullable<double>  Rpt_Extra_FRT_Sales_CC { get; set; }
    
    		    
    		/// <summary>
    		/// Rpt_ Extra_ Other_ Sales_ CC
    		/// </summary>        
    	//    [DisplayName("Rpt_ Extra_ Other_ Sales_ CC")]
    		public Nullable<double>  Rpt_Extra_Other_Sales_CC { get; set; }
    
    		    
    		/// <summary>
    		/// Rpt_ FVAT_ Sales_ CC
    		/// </summary>        
    	//    [DisplayName("Rpt_ FVAT_ Sales_ CC")]
    		public Nullable<double>  Rpt_FVAT_Sales_CC { get; set; }
    
    		    
    		/// <summary>
    		/// Rpt_ OVAT_ Sales_ CC
    		/// </summary>        
    	//    [DisplayName("Rpt_ OVAT_ Sales_ CC")]
    		public Nullable<double>  Rpt_OVAT_Sales_CC { get; set; }
    
    		    
    		/// <summary>
    		/// Rpt_ EVAT_ Sales_ CC
    		/// </summary>        
    	//    [DisplayName("Rpt_ EVAT_ Sales_ CC")]
    		public Nullable<double>  Rpt_EVAT_Sales_CC { get; set; }
    
    		    
    		/// <summary>
    		/// Rpt_ EFVAT_ Sales_ CC
    		/// </summary>        
    	//    [DisplayName("Rpt_ EFVAT_ Sales_ CC")]
    		public Nullable<double>  Rpt_EFVAT_Sales_CC { get; set; }
    
    		    
    		/// <summary>
    		/// Rpt_ EOVAT_ Sales_ CC
    		/// </summary>        
    	//    [DisplayName("Rpt_ EOVAT_ Sales_ CC")]
    		public Nullable<double>  Rpt_EOVAT_Sales_CC { get; set; }
    
    		    
    		/// <summary>
    		/// Report Currency
    		/// </summary>        
    	//    [DisplayName("Report Currency")]
            [MaxLength(50, ErrorMessage = "Report Currency cannot be longer than 50 characters")]
    		public string  ReportCurrency { get; set; }
    
    		    
    		/// <summary>
    		/// Local Currency
    		/// </summary>        
    	//    [DisplayName("Local Currency")]
            [MaxLength(10, ErrorMessage = "Local Currency cannot be longer than 10 characters")]
    		public string  LocalCurrency { get; set; }
    
    		    
    		/// <summary>
    		/// ID
    		/// </summary>        
    	//    [DisplayName("ID")]
            [Required(ErrorMessage = "ID is required")]
    		public int  ID { get; set; }
    
    		    
    		/// <summary>
    		/// Create By
    		/// </summary>        
    	//    [DisplayName("Create By")]
            [Required(ErrorMessage = "Create By is required")]
            [MaxLength(6, ErrorMessage = "Create By cannot be longer than 6 characters")]
    		public string  CreateBy { get; set; }
    
    		    
    		/// <summary>
    		/// Created Time
    		/// </summary>        
    	//    [DisplayName("Created Time")]
            [Required(ErrorMessage = "Created Time is required")]
    		public System.DateTime  CreatedTime { get; set; }
    
    		    
    		/// <summary>
    		/// Updated By
    		/// </summary>        
    	//    [DisplayName("Updated By")]
            [Required(ErrorMessage = "Updated By is required")]
            [MaxLength(6, ErrorMessage = "Updated By cannot be longer than 6 characters")]
    		public string  UpdatedBy { get; set; }
    
    		    
    		/// <summary>
    		/// Updated Time
    		/// </summary>        
    	//    [DisplayName("Updated Time")]
            [Required(ErrorMessage = "Updated Time is required")]
    		public System.DateTime  UpdatedTime { get; set; }
    
    		    
    		/// <summary>
    		/// CUS_ Sales_ PP
    		/// </summary>        
    	//    [DisplayName("CUS_ Sales_ PP")]
    		public Nullable<double>  CUS_Sales_PP { get; set; }
    
    		    
    		/// <summary>
    		/// CUS_ VAT_ Sales_ PP
    		/// </summary>        
    	//    [DisplayName("CUS_ VAT_ Sales_ PP")]
    		public Nullable<double>  CUS_VAT_Sales_PP { get; set; }
    
    		    
    		/// <summary>
    		/// Extra_ CUS_ Sales_ PP
    		/// </summary>        
    	//    [DisplayName("Extra_ CUS_ Sales_ PP")]
    		public Nullable<double>  Extra_CUS_Sales_PP { get; set; }
    
    		    
    		/// <summary>
    		/// Extra_ CUS_ VAT_ Sales_ PP
    		/// </summary>        
    	//    [DisplayName("Extra_ CUS_ VAT_ Sales_ PP")]
    		public Nullable<double>  Extra_CUS_VAT_Sales_PP { get; set; }
    
    		    
    		/// <summary>
    		/// CUS_ Sales_ CC
    		/// </summary>        
    	//    [DisplayName("CUS_ Sales_ CC")]
    		public Nullable<double>  CUS_Sales_CC { get; set; }
    
    		    
    		/// <summary>
    		/// CUS_ VAT_ Sales_ CC
    		/// </summary>        
    	//    [DisplayName("CUS_ VAT_ Sales_ CC")]
    		public Nullable<double>  CUS_VAT_Sales_CC { get; set; }
    
    		    
    		/// <summary>
    		/// Extra_ CUS_ Sales_ CC
    		/// </summary>        
    	//    [DisplayName("Extra_ CUS_ Sales_ CC")]
    		public Nullable<double>  Extra_CUS_Sales_CC { get; set; }
    
    		    
    		/// <summary>
    		/// Extra_ CUS_ VAT_ Sales_ CC
    		/// </summary>        
    	//    [DisplayName("Extra_ CUS_ VAT_ Sales_ CC")]
    		public Nullable<double>  Extra_CUS_VAT_Sales_CC { get; set; }
    
    		    
    		/// <summary>
    		/// CUS_ ECOST
    		/// </summary>        
    	//    [DisplayName("CUS_ ECOST")]
    		public Nullable<double>  CUS_ECOST { get; set; }
    
    		    
    		/// <summary>
    		/// CUS_ VAT_ ECOST
    		/// </summary>        
    	//    [DisplayName("CUS_ VAT_ ECOST")]
    		public Nullable<double>  CUS_VAT_ECOST { get; set; }
    
    		    
    		/// <summary>
    		/// Extra_ CUS_ ECOST
    		/// </summary>        
    	//    [DisplayName("Extra_ CUS_ ECOST")]
    		public Nullable<double>  Extra_CUS_ECOST { get; set; }
    
    		    
    		/// <summary>
    		/// Extra_ CUS_ VAT_ ECOST
    		/// </summary>        
    	//    [DisplayName("Extra_ CUS_ VAT_ ECOST")]
    		public Nullable<double>  Extra_CUS_VAT_ECOST { get; set; }
    
    		    
    		/// <summary>
    		/// DNDim_ Ecost
    		/// </summary>        
    	//    [DisplayName("DNDim_ Ecost")]
    		public Nullable<double>  DNDim_Ecost { get; set; }
    
    		    
    		/// <summary>
    		/// DNDIm_ Cus_ Ecost
    		/// </summary>        
    	//    [DisplayName("DNDIm_ Cus_ Ecost")]
    		public Nullable<double>  DNDIm_Cus_Ecost { get; set; }
    
    		    
    		/// <summary>
    		/// Extra_ DNDim_ Ecost
    		/// </summary>        
    	//    [DisplayName("Extra_ DNDim_ Ecost")]
    		public Nullable<double>  Extra_DNDim_Ecost { get; set; }
    
    		    
    		/// <summary>
    		/// Extra_ DNDIm_ Cus_ Ecost
    		/// </summary>        
    	//    [DisplayName("Extra_ DNDIm_ Cus_ Ecost")]
    		public Nullable<double>  Extra_DNDIm_Cus_Ecost { get; set; }
    
    		    
    		/// <summary>
    		/// Rpt_ CUS_ Sales_ PP
    		/// </summary>        
    	//    [DisplayName("Rpt_ CUS_ Sales_ PP")]
    		public Nullable<double>  Rpt_CUS_Sales_PP { get; set; }
    
    		    
    		/// <summary>
    		/// Rpt_ CUS_ VAT_ Sales_ PP
    		/// </summary>        
    	//    [DisplayName("Rpt_ CUS_ VAT_ Sales_ PP")]
    		public Nullable<double>  Rpt_CUS_VAT_Sales_PP { get; set; }
    
    		    
    		/// <summary>
    		/// Rpt_ Extra_ CUS_ Sales_ PP
    		/// </summary>        
    	//    [DisplayName("Rpt_ Extra_ CUS_ Sales_ PP")]
    		public Nullable<double>  Rpt_Extra_CUS_Sales_PP { get; set; }
    
    		    
    		/// <summary>
    		/// Rpt_ Extra_ CUS_ VAT_ Sales_ PP
    		/// </summary>        
    	//    [DisplayName("Rpt_ Extra_ CUS_ VAT_ Sales_ PP")]
    		public Nullable<double>  Rpt_Extra_CUS_VAT_Sales_PP { get; set; }
    
    		    
    		/// <summary>
    		/// Rpt_ CUS_ Sales_ CC
    		/// </summary>        
    	//    [DisplayName("Rpt_ CUS_ Sales_ CC")]
    		public Nullable<double>  Rpt_CUS_Sales_CC { get; set; }
    
    		    
    		/// <summary>
    		/// Rpt_ CUS_ VAT_ Sales_ CC
    		/// </summary>        
    	//    [DisplayName("Rpt_ CUS_ VAT_ Sales_ CC")]
    		public Nullable<double>  Rpt_CUS_VAT_Sales_CC { get; set; }
    
    		    
    		/// <summary>
    		/// Rpt_ Extra_ CUS_ Sales_ CC
    		/// </summary>        
    	//    [DisplayName("Rpt_ Extra_ CUS_ Sales_ CC")]
    		public Nullable<double>  Rpt_Extra_CUS_Sales_CC { get; set; }
    
    		    
    		/// <summary>
    		/// Rpt_ Extra_ CUS_ VAT_ Sales_ CC
    		/// </summary>        
    	//    [DisplayName("Rpt_ Extra_ CUS_ VAT_ Sales_ CC")]
    		public Nullable<double>  Rpt_Extra_CUS_VAT_Sales_CC { get; set; }
    
    		    
    		/// <summary>
    		/// Rpt_ CUS_ ECOST
    		/// </summary>        
    	//    [DisplayName("Rpt_ CUS_ ECOST")]
    		public Nullable<double>  Rpt_CUS_ECOST { get; set; }
    
    		    
    		/// <summary>
    		/// Rpt_ CUS_ VAT_ ECOST
    		/// </summary>        
    	//    [DisplayName("Rpt_ CUS_ VAT_ ECOST")]
    		public Nullable<double>  Rpt_CUS_VAT_ECOST { get; set; }
    
    		    
    		/// <summary>
    		/// Rpt_ Extra_ CUS_ ECOST
    		/// </summary>        
    	//    [DisplayName("Rpt_ Extra_ CUS_ ECOST")]
    		public Nullable<double>  Rpt_Extra_CUS_ECOST { get; set; }
    
    		    
    		/// <summary>
    		/// Rpt_ Extra_ CUS_ VAT_ ECOST
    		/// </summary>        
    	//    [DisplayName("Rpt_ Extra_ CUS_ VAT_ ECOST")]
    		public Nullable<double>  Rpt_Extra_CUS_VAT_ECOST { get; set; }
    
    		    
    		/// <summary>
    		/// Rpt_ DNDim_ Ecost
    		/// </summary>        
    	//    [DisplayName("Rpt_ DNDim_ Ecost")]
    		public Nullable<double>  Rpt_DNDim_Ecost { get; set; }
    
    		    
    		/// <summary>
    		/// Rpt_ DNDIm_ Cus_ Ecost
    		/// </summary>        
    	//    [DisplayName("Rpt_ DNDIm_ Cus_ Ecost")]
    		public Nullable<double>  Rpt_DNDIm_Cus_Ecost { get; set; }
    
    		    
    		/// <summary>
    		/// Rpt_ Extra_ DNDim_ Ecost
    		/// </summary>        
    	//    [DisplayName("Rpt_ Extra_ DNDim_ Ecost")]
    		public Nullable<double>  Rpt_Extra_DNDim_Ecost { get; set; }
    
    		    
    		/// <summary>
    		/// Rpt_ Extra_ DNDIm_ Cus_ Ecost
    		/// </summary>        
    	//    [DisplayName("Rpt_ Extra_ DNDIm_ Cus_ Ecost")]
    		public Nullable<double>  Rpt_Extra_DNDIm_Cus_Ecost { get; set; }
    
    		    
    		/// <summary>
    		/// Consol_ RDIST_ Sales
    		/// </summary>        
    	//    [DisplayName("Consol_ RDIST_ Sales")]
    		public Nullable<double>  Consol_RDIST_Sales { get; set; }
    
    		    
    		/// <summary>
    		/// Rpt_ Consol_ RDIST_ Sales
    		/// </summary>        
    	//    [DisplayName("Rpt_ Consol_ RDIST_ Sales")]
    		public Nullable<double>  Rpt_Consol_RDIST_Sales { get; set; }
    
    		    
    		/// <summary>
    		/// Oversea DN_ Sales
    		/// </summary>        
    	//    [DisplayName("Oversea DN_ Sales")]
    		public Nullable<double>  OverseaDN_Sales { get; set; }
    
    		    
    		/// <summary>
    		/// Rpt_ Oversea DN_ Sales
    		/// </summary>        
    	//    [DisplayName("Rpt_ Oversea DN_ Sales")]
    		public Nullable<double>  Rpt_OverseaDN_Sales { get; set; }
    
    		    
    		/// <summary>
    		/// DUTY_ Sales
    		/// </summary>        
    	//    [DisplayName("DUTY_ Sales")]
    		public Nullable<double>  DUTY_Sales { get; set; }
    
    		    
    		/// <summary>
    		/// Rpt_ DUTY_ Sales
    		/// </summary>        
    	//    [DisplayName("Rpt_ DUTY_ Sales")]
    		public Nullable<double>  Rpt_DUTY_Sales { get; set; }
    
    		    
    		/// <summary>
    		/// Version
    		/// </summary>        
    	//    [DisplayName("Version")]
            [MaxLength(8, ErrorMessage = "Version cannot be longer than 8 characters")]
    		public byte[]  Version { get; set; }
    
    		    
    	}
    //}
    
}
