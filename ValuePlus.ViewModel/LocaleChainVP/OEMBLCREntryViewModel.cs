using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ValuePlus.ViewModel;



namespace ValuePlus.ViewModel.LocaleChainVP
{
    
    /// <summary>
    /// OEMBLCREntry class
    /// </summary>
    //[MetadataType(typeof(OEMBLCREntryViewModel))]
    //public  partial class OEMBLCREntry
    //{
    
    	/// <summary>
    	/// OEMBLCREntry Metadata class
    	/// </summary>
    	public   class OEMBLCREntryViewModel:ViewModelBase
    	{
    		    
    		/// <summary>
    		/// ID
    		/// </summary>        
    	//    [DisplayName("ID")]
            [Required(ErrorMessage = "ID is required")]
    		public int  ID { get; set; }
    
    		    
    		/// <summary>
    		/// MBLID
    		/// </summary>        
    	//    [DisplayName("MBLID")]
    		public Nullable<int>  MBLID { get; set; }
    
    		    
    		/// <summary>
    		/// MBLType
    		/// </summary>        
    	//    [DisplayName("MBLType")]
            [MaxLength(5, ErrorMessage = "MBLType cannot be longer than 5 characters")]
    		public string  MBLType { get; set; }
    
    		    
    		/// <summary>
    		/// Station ID
    		/// </summary>        
    	//    [DisplayName("Station ID")]
            [MaxLength(5, ErrorMessage = "Station ID cannot be longer than 5 characters")]
    		public string  StationID { get; set; }
    
    		    
    		/// <summary>
    		/// FAXTO
    		/// </summary>        
    	//    [DisplayName("FAXTO")]
            [MaxLength(50, ErrorMessage = "FAXTO cannot be longer than 50 characters")]
    		public string  FAXTO { get; set; }
    
    		    
    		/// <summary>
    		/// Company ATTN
    		/// </summary>        
    	//    [DisplayName("Company ATTN")]
            [MaxLength(50, ErrorMessage = "Company ATTN cannot be longer than 50 characters")]
    		public string  CompanyATTN { get; set; }
    
    		    
    		/// <summary>
    		/// Company ATTNTEL
    		/// </summary>        
    	//    [DisplayName("Company ATTNTEL")]
            [MaxLength(50, ErrorMessage = "Company ATTNTEL cannot be longer than 50 characters")]
    		public string  CompanyATTNTEL { get; set; }
    
    		    
    		/// <summary>
    		/// Company ATTNFAX
    		/// </summary>        
    	//    [DisplayName("Company ATTNFAX")]
            [MaxLength(50, ErrorMessage = "Company ATTNFAX cannot be longer than 50 characters")]
    		public string  CompanyATTNFAX { get; set; }
    
    		    
    		/// <summary>
    		/// Print Date
    		/// </summary>        
    	//    [DisplayName("Print Date")]
    		public Nullable<System.DateTime>  PrintDate { get; set; }
    
    		    
    		/// <summary>
    		/// HBLNO
    		/// </summary>        
    	//    [DisplayName("HBLNO")]
            [MaxLength(50, ErrorMessage = "HBLNO cannot be longer than 50 characters")]
    		public string  HBLNO { get; set; }
    
    		    
    		/// <summary>
    		/// Delivery No
    		/// </summary>        
    	//    [DisplayName("Delivery No")]
            [MaxLength(50, ErrorMessage = "Delivery No cannot be longer than 50 characters")]
    		public string  DeliveryNo { get; set; }
    
    		    
    		/// <summary>
    		/// Operate Type
    		/// </summary>        
    	//    [DisplayName("Operate Type")]
            [MaxLength(50, ErrorMessage = "Operate Type cannot be longer than 50 characters")]
    		public string  OperateType { get; set; }
    
    		    
    		/// <summary>
    		/// VSLVOY
    		/// </summary>        
    	//    [DisplayName("VSLVOY")]
            [MaxLength(100, ErrorMessage = "VSLVOY cannot be longer than 100 characters")]
    		public string  VSLVOY { get; set; }
    
    		    
    		/// <summary>
    		/// MBLNO
    		/// </summary>        
    	//    [DisplayName("MBLNO")]
            [MaxLength(50, ErrorMessage = "MBLNO cannot be longer than 50 characters")]
    		public string  MBLNO { get; set; }
    
    		    
    		/// <summary>
    		/// Carton Type
    		/// </summary>        
    	//    [DisplayName("Carton Type")]
            [MaxLength(50, ErrorMessage = "Carton Type cannot be longer than 50 characters")]
    		public string  CartonType { get; set; }
    
    		    
    		/// <summary>
    		/// POL
    		/// </summary>        
    	//    [DisplayName("POL")]
            [MaxLength(20, ErrorMessage = "POL cannot be longer than 20 characters")]
    		public string  POL { get; set; }
    
    		    
    		/// <summary>
    		/// DEST
    		/// </summary>        
    	//    [DisplayName("DEST")]
            [MaxLength(50, ErrorMessage = "DEST cannot be longer than 50 characters")]
    		public string  DEST { get; set; }
    
    		    
    		/// <summary>
    		/// ETD
    		/// </summary>        
    	//    [DisplayName("ETD")]
            [MaxLength(20, ErrorMessage = "ETD cannot be longer than 20 characters")]
    		public string  ETD { get; set; }
    
    		    
    		/// <summary>
    		/// Re Mark
    		/// </summary>        
    	//    [DisplayName("Re Mark")]
            [MaxLength(255, ErrorMessage = "Re Mark cannot be longer than 255 characters")]
    		public string  ReMark { get; set; }
    
    		    
    		/// <summary>
    		/// Extra1
    		/// </summary>        
    	//    [DisplayName("Extra1")]
            [MaxLength(255, ErrorMessage = "Extra1 cannot be longer than 255 characters")]
    		public string  Extra1 { get; set; }
    
    		    
    		/// <summary>
    		/// Extra2
    		/// </summary>        
    	//    [DisplayName("Extra2")]
            [MaxLength(255, ErrorMessage = "Extra2 cannot be longer than 255 characters")]
    		public string  Extra2 { get; set; }
    
    		    
    		/// <summary>
    		/// Extra3
    		/// </summary>        
    	//    [DisplayName("Extra3")]
            [MaxLength(255, ErrorMessage = "Extra3 cannot be longer than 255 characters")]
    		public string  Extra3 { get; set; }
    
    		    
    		/// <summary>
    		/// Created DT
    		/// </summary>        
    	//    [DisplayName("Created DT")]
    		public Nullable<System.DateTime>  CreatedDT { get; set; }
    
    		    
    		/// <summary>
    		/// Created By
    		/// </summary>        
    	//    [DisplayName("Created By")]
            [MaxLength(50, ErrorMessage = "Created By cannot be longer than 50 characters")]
    		public string  CreatedBy { get; set; }
    
    		    
    		/// <summary>
    		/// Updated DT
    		/// </summary>        
    	//    [DisplayName("Updated DT")]
    		public Nullable<System.DateTime>  UpdatedDT { get; set; }
    
    		    
    		/// <summary>
    		/// Updated By
    		/// </summary>        
    	//    [DisplayName("Updated By")]
            [MaxLength(50, ErrorMessage = "Updated By cannot be longer than 50 characters")]
    		public string  UpdatedBy { get; set; }
    
    		    
    	}
    //}
    
}
