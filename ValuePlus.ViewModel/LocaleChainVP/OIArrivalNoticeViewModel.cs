using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ValuePlus.ViewModel;



namespace ValuePlus.ViewModel.LocaleChainVP
{
    
    /// <summary>
    /// OIArrivalNotice class
    /// </summary>
    //[MetadataType(typeof(OIArrivalNoticeViewModel))]
    //public  partial class OIArrivalNotice
    //{
    
    	/// <summary>
    	/// OIArrivalNotice Metadata class
    	/// </summary>
    	public   class OIArrivalNoticeViewModel:ViewModelBase
    	{
    		    
    		/// <summary>
    		/// ID
    		/// </summary>        
    	//    [DisplayName("ID")]
            [Required(ErrorMessage = "ID is required")]
    		public int  ID { get; set; }
    
    		    
    		/// <summary>
    		/// HBLID
    		/// </summary>        
    	//    [DisplayName("HBLID")]
            [MaxLength(50, ErrorMessage = "HBLID cannot be longer than 50 characters")]
    		public string  HBLID { get; set; }
    
    		    
    		/// <summary>
    		/// Station ID
    		/// </summary>        
    	//    [DisplayName("Station ID")]
            [MaxLength(5, ErrorMessage = "Station ID cannot be longer than 5 characters")]
    		public string  StationID { get; set; }
    
    		    
    		/// <summary>
    		/// Station Name
    		/// </summary>        
    	//    [DisplayName("Station Name")]
            [MaxLength(200, ErrorMessage = "Station Name cannot be longer than 200 characters")]
    		public string  StationName { get; set; }
    
    		    
    		/// <summary>
    		/// Address
    		/// </summary>        
    	//    [DisplayName("Address")]
            [MaxLength(255, ErrorMessage = "Address cannot be longer than 255 characters")]
    		public string  Address { get; set; }
    
    		    
    		/// <summary>
    		/// Tel
    		/// </summary>        
    	//    [DisplayName("Tel")]
            [MaxLength(50, ErrorMessage = "Tel cannot be longer than 50 characters")]
    		public string  Tel { get; set; }
    
    		    
    		/// <summary>
    		/// Fax
    		/// </summary>        
    	//    [DisplayName("Fax")]
            [MaxLength(50, ErrorMessage = "Fax cannot be longer than 50 characters")]
    		public string  Fax { get; set; }
    
    		    
    		/// <summary>
    		/// Shipper
    		/// </summary>        
    	//    [DisplayName("Shipper")]
            [MaxLength(100, ErrorMessage = "Shipper cannot be longer than 100 characters")]
    		public string  Shipper { get; set; }
    
    		    
    		/// <summary>
    		/// MBL
    		/// </summary>        
    	//    [DisplayName("MBL")]
            [MaxLength(50, ErrorMessage = "MBL cannot be longer than 50 characters")]
    		public string  MBL { get; set; }
    
    		    
    		/// <summary>
    		/// AMSBL
    		/// </summary>        
    	//    [DisplayName("AMSBL")]
            [MaxLength(50, ErrorMessage = "AMSBL cannot be longer than 50 characters")]
    		public string  AMSBL { get; set; }
    
    		    
    		/// <summary>
    		/// Cosignee
    		/// </summary>        
    	//    [DisplayName("Cosignee")]
            [MaxLength(250, ErrorMessage = "Cosignee cannot be longer than 250 characters")]
    		public string  Cosignee { get; set; }
    
    		    
    		/// <summary>
    		/// HBL
    		/// </summary>        
    	//    [DisplayName("HBL")]
            [MaxLength(50, ErrorMessage = "HBL cannot be longer than 50 characters")]
    		public string  HBL { get; set; }
    
    		    
    		/// <summary>
    		/// Sub HBL
    		/// </summary>        
    	//    [DisplayName("Sub HBL")]
            [MaxLength(50, ErrorMessage = "Sub HBL cannot be longer than 50 characters")]
    		public string  SubHBL { get; set; }
    
    		    
    		/// <summary>
    		/// Vessel
    		/// </summary>        
    	//    [DisplayName("Vessel")]
            [MaxLength(100, ErrorMessage = "Vessel cannot be longer than 100 characters")]
    		public string  Vessel { get; set; }
    
    		    
    		/// <summary>
    		/// Voyage
    		/// </summary>        
    	//    [DisplayName("Voyage")]
            [MaxLength(100, ErrorMessage = "Voyage cannot be longer than 100 characters")]
    		public string  Voyage { get; set; }
    
    		    
    		/// <summary>
    		/// PLoading
    		/// </summary>        
    	//    [DisplayName("PLoading")]
            [MaxLength(40, ErrorMessage = "PLoading cannot be longer than 40 characters")]
    		public string  PLoading { get; set; }
    
    		    
    		/// <summary>
    		/// ETD
    		/// </summary>        
    	//    [DisplayName("ETD")]
            [MaxLength(50, ErrorMessage = "ETD cannot be longer than 50 characters")]
    		public string  ETD { get; set; }
    
    		    
    		/// <summary>
    		/// PDischarge
    		/// </summary>        
    	//    [DisplayName("PDischarge")]
            [MaxLength(40, ErrorMessage = "PDischarge cannot be longer than 40 characters")]
    		public string  PDischarge { get; set; }
    
    		    
    		/// <summary>
    		/// ETA
    		/// </summary>        
    	//    [DisplayName("ETA")]
            [MaxLength(50, ErrorMessage = "ETA cannot be longer than 50 characters")]
    		public string  ETA { get; set; }
    
    		    
    		/// <summary>
    		/// PDelivery
    		/// </summary>        
    	//    [DisplayName("PDelivery")]
            [MaxLength(40, ErrorMessage = "PDelivery cannot be longer than 40 characters")]
    		public string  PDelivery { get; set; }
    
    		    
    		/// <summary>
    		/// Destination ETA
    		/// </summary>        
    	//    [DisplayName("Destination ETA")]
            [MaxLength(50, ErrorMessage = "Destination ETA cannot be longer than 50 characters")]
    		public string  DestinationETA { get; set; }
    
    		    
    		/// <summary>
    		/// Freight Location
    		/// </summary>        
    	//    [DisplayName("Freight Location")]
            [MaxLength(250, ErrorMessage = "Freight Location cannot be longer than 250 characters")]
    		public string  FreightLocation { get; set; }
    
    		    
    		/// <summary>
    		/// ITNumber
    		/// </summary>        
    	//    [DisplayName("ITNumber")]
            [MaxLength(50, ErrorMessage = "ITNumber cannot be longer than 50 characters")]
    		public string  ITNumber { get; set; }
    
    		    
    		/// <summary>
    		/// ITDate
    		/// </summary>        
    	//    [DisplayName("ITDate")]
            [MaxLength(50, ErrorMessage = "ITDate cannot be longer than 50 characters")]
    		public string  ITDate { get; set; }
    
    		    
    		/// <summary>
    		/// ITPort
    		/// </summary>        
    	//    [DisplayName("ITPort")]
            [MaxLength(50, ErrorMessage = "ITPort cannot be longer than 50 characters")]
    		public string  ITPort { get; set; }
    
    		    
    		/// <summary>
    		/// Container Nos
    		/// </summary>        
    	//    [DisplayName("Container Nos")]
            [MaxLength(500, ErrorMessage = "Container Nos cannot be longer than 500 characters")]
    		public string  ContainerNos { get; set; }
    
    		    
    		/// <summary>
    		/// Pkgs
    		/// </summary>        
    	//    [DisplayName("Pkgs")]
            [MaxLength(300, ErrorMessage = "Pkgs cannot be longer than 300 characters")]
    		public string  Pkgs { get; set; }
    
    		    
    		/// <summary>
    		/// Goods
    		/// </summary>        
    	//    [DisplayName("Goods")]
            [MaxLength(500, ErrorMessage = "Goods cannot be longer than 500 characters")]
    		public string  Goods { get; set; }
    
    		    
    		/// <summary>
    		/// Gross Weight
    		/// </summary>        
    	//    [DisplayName("Gross Weight")]
            [MaxLength(300, ErrorMessage = "Gross Weight cannot be longer than 300 characters")]
    		public string  GrossWeight { get; set; }
    
    		    
    		/// <summary>
    		/// Measurement
    		/// </summary>        
    	//    [DisplayName("Measurement")]
            [MaxLength(500, ErrorMessage = "Measurement cannot be longer than 500 characters")]
    		public string  Measurement { get; set; }
    
    		    
    		/// <summary>
    		/// Charge
    		/// </summary>        
    	//    [DisplayName("Charge")]
            [MaxLength(200, ErrorMessage = "Charge cannot be longer than 200 characters")]
    		public string  Charge { get; set; }
    
    		    
    		/// <summary>
    		/// Collect
    		/// </summary>        
    	//    [DisplayName("Collect")]
            [MaxLength(200, ErrorMessage = "Collect cannot be longer than 200 characters")]
    		public string  Collect { get; set; }
    
    		    
    		/// <summary>
    		/// Total
    		/// </summary>        
    	//    [DisplayName("Total")]
            [MaxLength(50, ErrorMessage = "Total cannot be longer than 50 characters")]
    		public string  Total { get; set; }
    
    		    
    		/// <summary>
    		/// Standard Remark
    		/// </summary>        
    	//    [DisplayName("Standard Remark")]
            [MaxLength(500, ErrorMessage = "Standard Remark cannot be longer than 500 characters")]
    		public string  StandardRemark { get; set; }
    
    		    
    		/// <summary>
    		/// Input Remark
    		/// </summary>        
    	//    [DisplayName("Input Remark")]
            [MaxLength(500, ErrorMessage = "Input Remark cannot be longer than 500 characters")]
    		public string  InputRemark { get; set; }
    
    		    
    		/// <summary>
    		/// Prepared By
    		/// </summary>        
    	//    [DisplayName("Prepared By")]
            [MaxLength(50, ErrorMessage = "Prepared By cannot be longer than 50 characters")]
    		public string  PreparedBy { get; set; }
    
    		    
    		/// <summary>
    		/// Prepared Date
    		/// </summary>        
    	//    [DisplayName("Prepared Date")]
            [MaxLength(50, ErrorMessage = "Prepared Date cannot be longer than 50 characters")]
    		public string  PreparedDate { get; set; }
    
    		    
    		/// <summary>
    		/// Reference No
    		/// </summary>        
    	//    [DisplayName("Reference No")]
            [MaxLength(50, ErrorMessage = "Reference No cannot be longer than 50 characters")]
    		public string  ReferenceNo { get; set; }
    
    		    
    		/// <summary>
    		/// Created By
    		/// </summary>        
    	//    [DisplayName("Created By")]
            [MaxLength(50, ErrorMessage = "Created By cannot be longer than 50 characters")]
    		public string  CreatedBy { get; set; }
    
    		    
    		/// <summary>
    		/// Created DT
    		/// </summary>        
    	//    [DisplayName("Created DT")]
    		public Nullable<System.DateTime>  CreatedDT { get; set; }
    
    		    
    		/// <summary>
    		/// Updated By
    		/// </summary>        
    	//    [DisplayName("Updated By")]
            [MaxLength(50, ErrorMessage = "Updated By cannot be longer than 50 characters")]
    		public string  UpdatedBy { get; set; }
    
    		    
    		/// <summary>
    		/// Updated DT
    		/// </summary>        
    	//    [DisplayName("Updated DT")]
    		public Nullable<System.DateTime>  UpdatedDT { get; set; }
    
    		    
    		/// <summary>
    		/// Notify Party
    		/// </summary>        
    	//    [DisplayName("Notify Party")]
            [MaxLength(300, ErrorMessage = "Notify Party cannot be longer than 300 characters")]
    		public string  NotifyParty { get; set; }
    
    		    
    		/// <summary>
    		/// Telex Release
    		/// </summary>        
    	//    [DisplayName("Telex Release")]
            [MaxLength(50, ErrorMessage = "Telex Release cannot be longer than 50 characters")]
    		public string  TelexRelease { get; set; }
    
    		    
    		/// <summary>
    		/// Broker
    		/// </summary>        
    	//    [DisplayName("Broker")]
            [MaxLength(250, ErrorMessage = "Broker cannot be longer than 250 characters")]
    		public string  Broker { get; set; }
    
    		    
    		/// <summary>
    		/// Final Location
    		/// </summary>        
    	//    [DisplayName("Final Location")]
            [MaxLength(250, ErrorMessage = "Final Location cannot be longer than 250 characters")]
    		public string  FinalLocation { get; set; }
    
    		    
    	}
    //}
    
}
