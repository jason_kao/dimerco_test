using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ValuePlus.ViewModel;



namespace ValuePlus.ViewModel.LocaleChainVP
{
    
    /// <summary>
    /// OEHBLWSAssignRecord class
    /// </summary>
    //[MetadataType(typeof(OEHBLWSAssignRecordViewModel))]
    //public  partial class OEHBLWSAssignRecord
    //{
    
    	/// <summary>
    	/// OEHBLWSAssignRecord Metadata class
    	/// </summary>
    	public   class OEHBLWSAssignRecordViewModel:ViewModelBase
    	{
    		    
    		/// <summary>
    		/// ID
    		/// </summary>        
    	//    [DisplayName("ID")]
            [Required(ErrorMessage = "ID is required")]
    		public int  ID { get; set; }
    
    		    
    		/// <summary>
    		/// HBLID
    		/// </summary>        
    	//    [DisplayName("HBLID")]
    		public Nullable<int>  HBLID { get; set; }
    
    		    
    		/// <summary>
    		/// Assign To1
    		/// </summary>        
    	//    [DisplayName("Assign To1")]
            [MaxLength(20, ErrorMessage = "Assign To1 cannot be longer than 20 characters")]
    		public string  AssignTo1 { get; set; }
    
    		    
    		/// <summary>
    		/// Assign To2
    		/// </summary>        
    	//    [DisplayName("Assign To2")]
            [MaxLength(20, ErrorMessage = "Assign To2 cannot be longer than 20 characters")]
    		public string  AssignTo2 { get; set; }
    
    		    
    		/// <summary>
    		/// Assign To3
    		/// </summary>        
    	//    [DisplayName("Assign To3")]
            [MaxLength(20, ErrorMessage = "Assign To3 cannot be longer than 20 characters")]
    		public string  AssignTo3 { get; set; }
    
    		    
    		/// <summary>
    		/// Created By
    		/// </summary>        
    	//    [DisplayName("Created By")]
            [MaxLength(50, ErrorMessage = "Created By cannot be longer than 50 characters")]
    		public string  CreatedBy { get; set; }
    
    		    
    		/// <summary>
    		/// Created Time
    		/// </summary>        
    	//    [DisplayName("Created Time")]
    		public Nullable<System.DateTime>  CreatedTime { get; set; }
    
    		    
    		/// <summary>
    		/// Updated By
    		/// </summary>        
    	//    [DisplayName("Updated By")]
            [MaxLength(50, ErrorMessage = "Updated By cannot be longer than 50 characters")]
    		public string  UpdatedBy { get; set; }
    
    		    
    		/// <summary>
    		/// Updated Time
    		/// </summary>        
    	//    [DisplayName("Updated Time")]
    		public Nullable<System.DateTime>  UpdatedTime { get; set; }
    
    		    
    		/// <summary>
    		/// Station ID
    		/// </summary>        
    	//    [DisplayName("Station ID")]
            [MaxLength(5, ErrorMessage = "Station ID cannot be longer than 5 characters")]
    		public string  StationID { get; set; }
    
    		    
    	}
    //}
    
}
