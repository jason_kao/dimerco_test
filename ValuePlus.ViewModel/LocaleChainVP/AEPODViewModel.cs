using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ValuePlus.ViewModel;



namespace ValuePlus.ViewModel.LocaleChainVP
{
    
    /// <summary>
    /// AEPOD class
    /// </summary>
    //[MetadataType(typeof(AEPODViewModel))]
    //public  partial class AEPOD
    //{
    
    	/// <summary>
    	/// AEPOD Metadata class
    	/// </summary>
    	public   class AEPODViewModel:ViewModelBase
    	{
    		    
    		/// <summary>
    		/// ID
    		/// </summary>        
    	//    [DisplayName("ID")]
            [Required(ErrorMessage = "ID is required")]
    		public int  ID { get; set; }
    
    		    
    		/// <summary>
    		/// HAWBID
    		/// </summary>        
    	//    [DisplayName("HAWBID")]
            [Required(ErrorMessage = "HAWBID is required")]
    		public int  HAWBID { get; set; }
    
    		    
    		/// <summary>
    		/// FLTNo
    		/// </summary>        
    	//    [DisplayName("FLTNo")]
            [MaxLength(20, ErrorMessage = "FLTNo cannot be longer than 20 characters")]
    		public string  FLTNo { get; set; }
    
    		    
    		/// <summary>
    		/// ATA
    		/// </summary>        
    	//    [DisplayName("ATA")]
    		public Nullable<System.DateTime>  ATA { get; set; }
    
    		    
    		/// <summary>
    		/// SCHRelease No
    		/// </summary>        
    	//    [DisplayName("SCHRelease No")]
            [MaxLength(20, ErrorMessage = "SCHRelease No cannot be longer than 20 characters")]
    		public string  SCHReleaseNo { get; set; }
    
    		    
    		/// <summary>
    		/// SCHRelease Date
    		/// </summary>        
    	//    [DisplayName("SCHRelease Date")]
    		public Nullable<System.DateTime>  SCHReleaseDate { get; set; }
    
    		    
    		/// <summary>
    		/// SCHNotify Party
    		/// </summary>        
    	//    [DisplayName("SCHNotify Party")]
    		public Nullable<int>  SCHNotifyParty { get; set; }
    
    		    
    		/// <summary>
    		/// SCHNotify Date
    		/// </summary>        
    	//    [DisplayName("SCHNotify Date")]
    		public Nullable<System.DateTime>  SCHNotifyDate { get; set; }
    
    		    
    		/// <summary>
    		/// SCHDoc Release Party
    		/// </summary>        
    	//    [DisplayName("SCHDoc Release Party")]
    		public Nullable<int>  SCHDocReleaseParty { get; set; }
    
    		    
    		/// <summary>
    		/// SCHDoc Release Date
    		/// </summary>        
    	//    [DisplayName("SCHDoc Release Date")]
    		public Nullable<System.DateTime>  SCHDocReleaseDate { get; set; }
    
    		    
    		/// <summary>
    		/// SCHDoc Release Type
    		/// </summary>        
    	//    [DisplayName("SCHDoc Release Type")]
            [MaxLength(10, ErrorMessage = "SCHDoc Release Type cannot be longer than 10 characters")]
    		public string  SCHDocReleaseType { get; set; }
    
    		    
    		/// <summary>
    		/// SCHNotify Type
    		/// </summary>        
    	//    [DisplayName("SCHNotify Type")]
            [MaxLength(10, ErrorMessage = "SCHNotify Type cannot be longer than 10 characters")]
    		public string  SCHNotifyType { get; set; }
    
    		    
    		/// <summary>
    		/// SCHCargo Sign By
    		/// </summary>        
    	//    [DisplayName("SCHCargo Sign By")]
            [MaxLength(50, ErrorMessage = "SCHCargo Sign By cannot be longer than 50 characters")]
    		public string  SCHCargoSignBy { get; set; }
    
    		    
    		/// <summary>
    		/// SCHLicense Date
    		/// </summary>        
    	//    [DisplayName("SCHLicense Date")]
    		public Nullable<System.DateTime>  SCHLicenseDate { get; set; }
    
    		    
    		/// <summary>
    		/// SCHCustom Date
    		/// </summary>        
    	//    [DisplayName("SCHCustom Date")]
    		public Nullable<System.DateTime>  SCHCustomDate { get; set; }
    
    		    
    		/// <summary>
    		/// SCHDoc Sign By
    		/// </summary>        
    	//    [DisplayName("SCHDoc Sign By")]
            [MaxLength(50, ErrorMessage = "SCHDoc Sign By cannot be longer than 50 characters")]
    		public string  SCHDocSignBy { get; set; }
    
    		    
    		/// <summary>
    		/// SCHCargo Release Party
    		/// </summary>        
    	//    [DisplayName("SCHCargo Release Party")]
    		public Nullable<int>  SCHCargoReleaseParty { get; set; }
    
    		    
    		/// <summary>
    		/// SCHCargo Release Date
    		/// </summary>        
    	//    [DisplayName("SCHCargo Release Date")]
    		public Nullable<System.DateTime>  SCHCargoReleaseDate { get; set; }
    
    		    
    		/// <summary>
    		/// SCHCargo Release Type
    		/// </summary>        
    	//    [DisplayName("SCHCargo Release Type")]
            [MaxLength(10, ErrorMessage = "SCHCargo Release Type cannot be longer than 10 characters")]
    		public string  SCHCargoReleaseType { get; set; }
    
    		    
    		/// <summary>
    		/// Station ID
    		/// </summary>        
    	//    [DisplayName("Station ID")]
            [Required(ErrorMessage = "Station ID is required")]
            [MaxLength(10, ErrorMessage = "Station ID cannot be longer than 10 characters")]
    		public string  StationID { get; set; }
    
    		    
    		/// <summary>
    		/// Created By
    		/// </summary>        
    	//    [DisplayName("Created By")]
            [Required(ErrorMessage = "Created By is required")]
            [MaxLength(6, ErrorMessage = "Created By cannot be longer than 6 characters")]
    		public string  CreatedBy { get; set; }
    
    		    
    		/// <summary>
    		/// Created Date
    		/// </summary>        
    	//    [DisplayName("Created Date")]
            [Required(ErrorMessage = "Created Date is required")]
    		public System.DateTime  CreatedDate { get; set; }
    
    		    
    		/// <summary>
    		/// Updated By
    		/// </summary>        
    	//    [DisplayName("Updated By")]
            [Required(ErrorMessage = "Updated By is required")]
            [MaxLength(6, ErrorMessage = "Updated By cannot be longer than 6 characters")]
    		public string  UpdatedBy { get; set; }
    
    		    
    		/// <summary>
    		/// Updated Date
    		/// </summary>        
    	//    [DisplayName("Updated Date")]
    		public Nullable<System.DateTime>  UpdatedDate { get; set; }
    
    		    
    		/// <summary>
    		/// PODTemplate
    		/// </summary>        
    	//    [DisplayName("PODTemplate")]
            [MaxLength(50, ErrorMessage = "PODTemplate cannot be longer than 50 characters")]
    		public string  PODTemplate { get; set; }
    
    		    
    		/// <summary>
    		/// SCHCargo Terminal Date
    		/// </summary>        
    	//    [DisplayName("SCHCargo Terminal Date")]
    		public Nullable<System.DateTime>  SCHCargoTerminalDate { get; set; }
    
    		    
    		/// <summary>
    		/// Version
    		/// </summary>        
    	//    [DisplayName("Version")]
            [MaxLength(8, ErrorMessage = "Version cannot be longer than 8 characters")]
    		public byte[]  Version { get; set; }
    
    		    
    	}
    //}
    
}
