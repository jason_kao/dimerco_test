using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ValuePlus.ViewModel;



namespace ValuePlus.ViewModel.LocaleChainVP
{
    
    /// <summary>
    /// OEHBLWHEntry class
    /// </summary>
    //[MetadataType(typeof(OEHBLWHEntryViewModel))]
    //public  partial class OEHBLWHEntry
    //{
    
    	/// <summary>
    	/// OEHBLWHEntry Metadata class
    	/// </summary>
    	public   class OEHBLWHEntryViewModel:ViewModelBase
    	{
    		    
    		/// <summary>
    		/// ID
    		/// </summary>        
    	//    [DisplayName("ID")]
            [Required(ErrorMessage = "ID is required")]
    		public int  ID { get; set; }
    
    		    
    		/// <summary>
    		/// HBLID
    		/// </summary>        
    	//    [DisplayName("HBLID")]
    		public Nullable<int>  HBLID { get; set; }
    
    		    
    		/// <summary>
    		/// Station ID
    		/// </summary>        
    	//    [DisplayName("Station ID")]
            [MaxLength(5, ErrorMessage = "Station ID cannot be longer than 5 characters")]
    		public string  StationID { get; set; }
    
    		    
    		/// <summary>
    		/// Client Name
    		/// </summary>        
    	//    [DisplayName("Client Name")]
            [MaxLength(50, ErrorMessage = "Client Name cannot be longer than 50 characters")]
    		public string  ClientName { get; set; }
    
    		    
    		/// <summary>
    		/// ATTN
    		/// </summary>        
    	//    [DisplayName("ATTN")]
            [MaxLength(50, ErrorMessage = "ATTN cannot be longer than 50 characters")]
    		public string  ATTN { get; set; }
    
    		    
    		/// <summary>
    		/// Customs No
    		/// </summary>        
    	//    [DisplayName("Customs No")]
            [MaxLength(50, ErrorMessage = "Customs No cannot be longer than 50 characters")]
    		public string  CustomsNo { get; set; }
    
    		    
    		/// <summary>
    		/// Customs Name
    		/// </summary>        
    	//    [DisplayName("Customs Name")]
            [MaxLength(100, ErrorMessage = "Customs Name cannot be longer than 100 characters")]
    		public string  CustomsName { get; set; }
    
    		    
    		/// <summary>
    		/// Delivery No
    		/// </summary>        
    	//    [DisplayName("Delivery No")]
            [MaxLength(50, ErrorMessage = "Delivery No cannot be longer than 50 characters")]
    		public string  DeliveryNo { get; set; }
    
    		    
    		/// <summary>
    		/// PKGS
    		/// </summary>        
    	//    [DisplayName("PKGS")]
            [MaxLength(20, ErrorMessage = "PKGS cannot be longer than 20 characters")]
    		public string  PKGS { get; set; }
    
    		    
    		/// <summary>
    		/// GW
    		/// </summary>        
    	//    [DisplayName("GW")]
            [MaxLength(20, ErrorMessage = "GW cannot be longer than 20 characters")]
    		public string  GW { get; set; }
    
    		    
    		/// <summary>
    		/// VOL
    		/// </summary>        
    	//    [DisplayName("VOL")]
            [MaxLength(20, ErrorMessage = "VOL cannot be longer than 20 characters")]
    		public string  VOL { get; set; }
    
    		    
    		/// <summary>
    		/// Carton Type
    		/// </summary>        
    	//    [DisplayName("Carton Type")]
            [MaxLength(50, ErrorMessage = "Carton Type cannot be longer than 50 characters")]
    		public string  CartonType { get; set; }
    
    		    
    		/// <summary>
    		/// Marks
    		/// </summary>        
    	//    [DisplayName("Marks")]
            [MaxLength(255, ErrorMessage = "Marks cannot be longer than 255 characters")]
    		public string  Marks { get; set; }
    
    		    
    		/// <summary>
    		/// POL
    		/// </summary>        
    	//    [DisplayName("POL")]
            [MaxLength(20, ErrorMessage = "POL cannot be longer than 20 characters")]
    		public string  POL { get; set; }
    
    		    
    		/// <summary>
    		/// ETD
    		/// </summary>        
    	//    [DisplayName("ETD")]
            [MaxLength(50, ErrorMessage = "ETD cannot be longer than 50 characters")]
    		public string  ETD { get; set; }
    
    		    
    		/// <summary>
    		/// Delivery Date
    		/// </summary>        
    	//    [DisplayName("Delivery Date")]
            [MaxLength(50, ErrorMessage = "Delivery Date cannot be longer than 50 characters")]
    		public string  DeliveryDate { get; set; }
    
    		    
    		/// <summary>
    		/// Delivery Extra
    		/// </summary>        
    	//    [DisplayName("Delivery Extra")]
            [MaxLength(20, ErrorMessage = "Delivery Extra cannot be longer than 20 characters")]
    		public string  DeliveryExtra { get; set; }
    
    		    
    		/// <summary>
    		/// Warehouse Name
    		/// </summary>        
    	//    [DisplayName("Warehouse Name")]
            [MaxLength(50, ErrorMessage = "Warehouse Name cannot be longer than 50 characters")]
    		public string  WarehouseName { get; set; }
    
    		    
    		/// <summary>
    		/// Warehouse Address
    		/// </summary>        
    	//    [DisplayName("Warehouse Address")]
            [MaxLength(255, ErrorMessage = "Warehouse Address cannot be longer than 255 characters")]
    		public string  WarehouseAddress { get; set; }
    
    		    
    		/// <summary>
    		/// WHContact
    		/// </summary>        
    	//    [DisplayName("WHContact")]
            [MaxLength(50, ErrorMessage = "WHContact cannot be longer than 50 characters")]
    		public string  WHContact { get; set; }
    
    		    
    		/// <summary>
    		/// WHContact TEL
    		/// </summary>        
    	//    [DisplayName("WHContact TEL")]
            [MaxLength(50, ErrorMessage = "WHContact TEL cannot be longer than 50 characters")]
    		public string  WHContactTEL { get; set; }
    
    		    
    		/// <summary>
    		/// Customs Delivery Date
    		/// </summary>        
    	//    [DisplayName("Customs Delivery Date")]
            [MaxLength(50, ErrorMessage = "Customs Delivery Date cannot be longer than 50 characters")]
    		public string  CustomsDeliveryDate { get; set; }
    
    		    
    		/// <summary>
    		/// Customs Delivery Extra
    		/// </summary>        
    	//    [DisplayName("Customs Delivery Extra")]
            [MaxLength(20, ErrorMessage = "Customs Delivery Extra cannot be longer than 20 characters")]
    		public string  CustomsDeliveryExtra { get; set; }
    
    		    
    		/// <summary>
    		/// Company Address
    		/// </summary>        
    	//    [DisplayName("Company Address")]
            [MaxLength(255, ErrorMessage = "Company Address cannot be longer than 255 characters")]
    		public string  CompanyAddress { get; set; }
    
    		    
    		/// <summary>
    		/// Company ATTN
    		/// </summary>        
    	//    [DisplayName("Company ATTN")]
            [MaxLength(50, ErrorMessage = "Company ATTN cannot be longer than 50 characters")]
    		public string  CompanyATTN { get; set; }
    
    		    
    		/// <summary>
    		/// Company ATTNTEL
    		/// </summary>        
    	//    [DisplayName("Company ATTNTEL")]
            [MaxLength(50, ErrorMessage = "Company ATTNTEL cannot be longer than 50 characters")]
    		public string  CompanyATTNTEL { get; set; }
    
    		    
    		/// <summary>
    		/// Company ATTNFAX
    		/// </summary>        
    	//    [DisplayName("Company ATTNFAX")]
            [MaxLength(50, ErrorMessage = "Company ATTNFAX cannot be longer than 50 characters")]
    		public string  CompanyATTNFAX { get; set; }
    
    		    
    		/// <summary>
    		/// Company ATTNEmail
    		/// </summary>        
    	//    [DisplayName("Company ATTNEmail")]
            [MaxLength(50, ErrorMessage = "Company ATTNEmail cannot be longer than 50 characters")]
    		public string  CompanyATTNEmail { get; set; }
    
    		    
    		/// <summary>
    		/// Re Mark
    		/// </summary>        
    	//    [DisplayName("Re Mark")]
            [MaxLength(255, ErrorMessage = "Re Mark cannot be longer than 255 characters")]
    		public string  ReMark { get; set; }
    
    		    
    		/// <summary>
    		/// Extra1
    		/// </summary>        
    	//    [DisplayName("Extra1")]
            [MaxLength(255, ErrorMessage = "Extra1 cannot be longer than 255 characters")]
    		public string  Extra1 { get; set; }
    
    		    
    		/// <summary>
    		/// Extra2
    		/// </summary>        
    	//    [DisplayName("Extra2")]
            [MaxLength(255, ErrorMessage = "Extra2 cannot be longer than 255 characters")]
    		public string  Extra2 { get; set; }
    
    		    
    		/// <summary>
    		/// Extra3
    		/// </summary>        
    	//    [DisplayName("Extra3")]
            [MaxLength(255, ErrorMessage = "Extra3 cannot be longer than 255 characters")]
    		public string  Extra3 { get; set; }
    
    		    
    		/// <summary>
    		/// Created DT
    		/// </summary>        
    	//    [DisplayName("Created DT")]
    		public Nullable<System.DateTime>  CreatedDT { get; set; }
    
    		    
    		/// <summary>
    		/// Created By
    		/// </summary>        
    	//    [DisplayName("Created By")]
            [MaxLength(50, ErrorMessage = "Created By cannot be longer than 50 characters")]
    		public string  CreatedBy { get; set; }
    
    		    
    		/// <summary>
    		/// Updated DT
    		/// </summary>        
    	//    [DisplayName("Updated DT")]
    		public Nullable<System.DateTime>  UpdatedDT { get; set; }
    
    		    
    		/// <summary>
    		/// Updated By
    		/// </summary>        
    	//    [DisplayName("Updated By")]
            [MaxLength(50, ErrorMessage = "Updated By cannot be longer than 50 characters")]
    		public string  UpdatedBy { get; set; }
    
    		    
    	}
    //}
    
}
