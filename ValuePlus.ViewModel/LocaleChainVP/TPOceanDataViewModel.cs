using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ValuePlus.ViewModel;



namespace ValuePlus.ViewModel.LocaleChainVP
{
    
    /// <summary>
    /// TPOceanData class
    /// </summary>
    //[MetadataType(typeof(TPOceanDataViewModel))]
    //public  partial class TPOceanData
    //{
    
    	/// <summary>
    	/// TPOceanData Metadata class
    	/// </summary>
    	public   class TPOceanDataViewModel:ViewModelBase
    	{
    		    
    		/// <summary>
    		/// ID
    		/// </summary>        
    	//    [DisplayName("ID")]
            [Required(ErrorMessage = "ID is required")]
    		public int  ID { get; set; }
    
    		    
    		/// <summary>
    		/// Station ID
    		/// </summary>        
    	//    [DisplayName("Station ID")]
            [Required(ErrorMessage = "Station ID is required")]
            [MaxLength(3, ErrorMessage = "Station ID cannot be longer than 3 characters")]
    		public string  StationID { get; set; }
    
    		    
    		/// <summary>
    		/// TPID
    		/// </summary>        
    	//    [DisplayName("TPID")]
            [Required(ErrorMessage = "TPID is required")]
            [MaxLength(15, ErrorMessage = "TPID cannot be longer than 15 characters")]
    		public string  TPID { get; set; }
    
    		    
    		/// <summary>
    		/// Customer ID
    		/// </summary>        
    	//    [DisplayName("Customer ID")]
            [Required(ErrorMessage = "Customer ID is required")]
    		public int  CustomerID { get; set; }
    
    		    
    		/// <summary>
    		/// Source Station ID
    		/// </summary>        
    	//    [DisplayName("Source Station ID")]
            [MaxLength(3, ErrorMessage = "Source Station ID cannot be longer than 3 characters")]
    		public string  SourceStationID { get; set; }
    
    		    
    		/// <summary>
    		/// Source Type
    		/// </summary>        
    	//    [DisplayName("Source Type")]
            [MaxLength(1, ErrorMessage = "Source Type cannot be longer than 1 characters")]
    		public string  SourceType { get; set; }
    
    		    
    		/// <summary>
    		/// Source ID
    		/// </summary>        
    	//    [DisplayName("Source ID")]
    		public Nullable<int>  SourceID { get; set; }
    
    		    
    		/// <summary>
    		/// Master No
    		/// </summary>        
    	//    [DisplayName("Master No")]
            [Required(ErrorMessage = "Master No is required")]
            [MaxLength(30, ErrorMessage = "Master No cannot be longer than 30 characters")]
    		public string  MasterNo { get; set; }
    
    		    
    		/// <summary>
    		/// House No
    		/// </summary>        
    	//    [DisplayName("House No")]
            [Required(ErrorMessage = "House No is required")]
            [MaxLength(30, ErrorMessage = "House No cannot be longer than 30 characters")]
    		public string  HouseNo { get; set; }
    
    		    
    		/// <summary>
    		/// Lot
    		/// </summary>        
    	//    [DisplayName("Lot")]
            [MaxLength(30, ErrorMessage = "Lot cannot be longer than 30 characters")]
    		public string  Lot { get; set; }
    
    		    
    		/// <summary>
    		/// Shipper
    		/// </summary>        
    	//    [DisplayName("Shipper")]
            [Required(ErrorMessage = "Shipper is required")]
    		public int  Shipper { get; set; }
    
    		    
    		/// <summary>
    		/// Cnee
    		/// </summary>        
    	//    [DisplayName("Cnee")]
            [Required(ErrorMessage = "Cnee is required")]
    		public int  Cnee { get; set; }
    
    		    
    		/// <summary>
    		/// Receipt
    		/// </summary>        
    	//    [DisplayName("Receipt")]
            [Required(ErrorMessage = "Receipt is required")]
    		public int  Receipt { get; set; }
    
    		    
    		/// <summary>
    		/// Delivery
    		/// </summary>        
    	//    [DisplayName("Delivery")]
            [Required(ErrorMessage = "Delivery is required")]
    		public int  Delivery { get; set; }
    
    		    
    		/// <summary>
    		/// POL
    		/// </summary>        
    	//    [DisplayName("POL")]
            [Required(ErrorMessage = "POL is required")]
    		public int  POL { get; set; }
    
    		    
    		/// <summary>
    		/// POD
    		/// </summary>        
    	//    [DisplayName("POD")]
            [Required(ErrorMessage = "POD is required")]
    		public int  POD { get; set; }
    
    		    
    		/// <summary>
    		/// Neture Of Good
    		/// </summary>        
    	//    [DisplayName("Neture Of Good")]
            [MaxLength(30, ErrorMessage = "Neture Of Good cannot be longer than 30 characters")]
    		public string  NetureOfGood { get; set; }
    
    		    
    		/// <summary>
    		/// HType
    		/// </summary>        
    	//    [DisplayName("HType")]
            [Required(ErrorMessage = "HType is required")]
            [MaxLength(3, ErrorMessage = "HType cannot be longer than 3 characters")]
    		public string  HType { get; set; }
    
    		    
    		/// <summary>
    		/// CBM
    		/// </summary>        
    	//    [DisplayName("CBM")]
    		public Nullable<double>  CBM { get; set; }
    
    		    
    		/// <summary>
    		/// Sales Person
    		/// </summary>        
    	//    [DisplayName("Sales Person")]
    		public Nullable<int>  SalesPerson { get; set; }
    
    		    
    		/// <summary>
    		/// ATD
    		/// </summary>        
    	//    [DisplayName("ATD")]
    		public Nullable<System.DateTime>  ATD { get; set; }
    
    		    
    		/// <summary>
    		/// ATA
    		/// </summary>        
    	//    [DisplayName("ATA")]
    		public Nullable<System.DateTime>  ATA { get; set; }
    
    		    
    		/// <summary>
    		/// Lot Status
    		/// </summary>        
    	//    [DisplayName("Lot Status")]
            [Required(ErrorMessage = "Lot Status is required")]
            [MaxLength(1, ErrorMessage = "Lot Status cannot be longer than 1 characters")]
    		public string  LotStatus { get; set; }
    
    		    
    		/// <summary>
    		/// Is Mani Fast
    		/// </summary>        
    	//    [DisplayName("Is Mani Fast")]
            [Required(ErrorMessage = "Is Mani Fast is required")]
            [MaxLength(1, ErrorMessage = "Is Mani Fast cannot be longer than 1 characters")]
    		public string  IsManiFast { get; set; }
    
    		    
    		/// <summary>
    		/// Created By
    		/// </summary>        
    	//    [DisplayName("Created By")]
            [MaxLength(6, ErrorMessage = "Created By cannot be longer than 6 characters")]
    		public string  CreatedBy { get; set; }
    
    		    
    		/// <summary>
    		/// Created Date
    		/// </summary>        
    	//    [DisplayName("Created Date")]
    		public Nullable<System.DateTime>  CreatedDate { get; set; }
    
    		    
    		/// <summary>
    		/// Updated By
    		/// </summary>        
    	//    [DisplayName("Updated By")]
            [Required(ErrorMessage = "Updated By is required")]
            [MaxLength(6, ErrorMessage = "Updated By cannot be longer than 6 characters")]
    		public string  UpdatedBy { get; set; }
    
    		    
    		/// <summary>
    		/// Updated Date
    		/// </summary>        
    	//    [DisplayName("Updated Date")]
            [Required(ErrorMessage = "Updated Date is required")]
    		public System.DateTime  UpdatedDate { get; set; }
    
    		    
    		/// <summary>
    		/// Issue Date
    		/// </summary>        
    	//    [DisplayName("Issue Date")]
    		public Nullable<System.DateTime>  IssueDate { get; set; }
    
    		    
    		/// <summary>
    		/// Vessel
    		/// </summary>        
    	//    [DisplayName("Vessel")]
            [MaxLength(100, ErrorMessage = "Vessel cannot be longer than 100 characters")]
    		public string  Vessel { get; set; }
    
    		    
    		/// <summary>
    		/// Voyage
    		/// </summary>        
    	//    [DisplayName("Voyage")]
            [MaxLength(100, ErrorMessage = "Voyage cannot be longer than 100 characters")]
    		public string  Voyage { get; set; }
    
    		    
    		/// <summary>
    		/// Move Type
    		/// </summary>        
    	//    [DisplayName("Move Type")]
            [MaxLength(20, ErrorMessage = "Move Type cannot be longer than 20 characters")]
    		public string  MoveType { get; set; }
    
    		    
    		/// <summary>
    		/// Marks
    		/// </summary>        
    	//    [DisplayName("Marks")]
            [MaxLength(255, ErrorMessage = "Marks cannot be longer than 255 characters")]
    		public string  Marks { get; set; }
    
    		    
    		/// <summary>
    		/// Description
    		/// </summary>        
    	//    [DisplayName("Description")]
            [MaxLength(255, ErrorMessage = "Description cannot be longer than 255 characters")]
    		public string  Description { get; set; }
    
    		    
    		/// <summary>
    		/// Container
    		/// </summary>        
    	//    [DisplayName("Container")]
            [MaxLength(255, ErrorMessage = "Container cannot be longer than 255 characters")]
    		public string  Container { get; set; }
    
    		    
    		/// <summary>
    		/// PCS
    		/// </summary>        
    	//    [DisplayName("PCS")]
    		public Nullable<int>  PCS { get; set; }
    
    		    
    		/// <summary>
    		/// PCSUOM
    		/// </summary>        
    	//    [DisplayName("PCSUOM")]
            [MaxLength(30, ErrorMessage = "PCSUOM cannot be longer than 30 characters")]
    		public string  PCSUOM { get; set; }
    
    		    
    		/// <summary>
    		/// Weight
    		/// </summary>        
    	//    [DisplayName("Weight")]
    		public Nullable<decimal>  Weight { get; set; }
    
    		    
    		/// <summary>
    		/// WUOM
    		/// </summary>        
    	//    [DisplayName("WUOM")]
            [MaxLength(30, ErrorMessage = "WUOM cannot be longer than 30 characters")]
    		public string  WUOM { get; set; }
    
    		    
    		/// <summary>
    		/// Ref Number
    		/// </summary>        
    	//    [DisplayName("Ref Number")]
            [MaxLength(20, ErrorMessage = "Ref Number cannot be longer than 20 characters")]
    		public string  RefNumber { get; set; }
    
    		    
    		/// <summary>
    		/// Pre Master
    		/// </summary>        
    	//    [DisplayName("Pre Master")]
            [MaxLength(30, ErrorMessage = "Pre Master cannot be longer than 30 characters")]
    		public string  PreMaster { get; set; }
    
    		    
    		/// <summary>
    		/// Confirm ID
    		/// </summary>        
    	//    [DisplayName("Confirm ID")]
            [MaxLength(50, ErrorMessage = "Confirm ID cannot be longer than 50 characters")]
    		public string  ConfirmID { get; set; }
    
    		    
    		/// <summary>
    		/// Shpt Type
    		/// </summary>        
    	//    [DisplayName("Shpt Type")]
            [MaxLength(10, ErrorMessage = "Shpt Type cannot be longer than 10 characters")]
    		public string  ShptType { get; set; }
    
    		    
    		/// <summary>
    		/// HBLControl No
    		/// </summary>        
    	//    [DisplayName("HBLControl No")]
            [MaxLength(10, ErrorMessage = "HBLControl No cannot be longer than 10 characters")]
    		public string  HBLControlNo { get; set; }
    
    		    
    		/// <summary>
    		/// Pickup Date Time
    		/// </summary>        
    	//    [DisplayName("Pickup Date Time")]
    		public Nullable<System.DateTime>  PickupDateTime { get; set; }
    
    		    
    		/// <summary>
    		/// PReceipt ETD
    		/// </summary>        
    	//    [DisplayName("PReceipt ETD")]
    		public Nullable<System.DateTime>  PReceiptETD { get; set; }
    
    		    
    		/// <summary>
    		/// PLoading ETD
    		/// </summary>        
    	//    [DisplayName("PLoading ETD")]
    		public Nullable<System.DateTime>  PLoadingETD { get; set; }
    
    		    
    		/// <summary>
    		/// PDischarge ETD
    		/// </summary>        
    	//    [DisplayName("PDischarge ETD")]
    		public Nullable<System.DateTime>  PDischargeETD { get; set; }
    
    		    
    		/// <summary>
    		/// PDelivery ETD
    		/// </summary>        
    	//    [DisplayName("PDelivery ETD")]
    		public Nullable<System.DateTime>  PDeliveryETD { get; set; }
    
    		    
    		/// <summary>
    		/// Final Dest ETD
    		/// </summary>        
    	//    [DisplayName("Final Dest ETD")]
    		public Nullable<System.DateTime>  FinalDestETD { get; set; }
    
    		    
    		/// <summary>
    		/// PLoading ATD
    		/// </summary>        
    	//    [DisplayName("PLoading ATD")]
    		public Nullable<System.DateTime>  PLoadingATD { get; set; }
    
    		    
    		/// <summary>
    		/// Carrier
    		/// </summary>        
    	//    [DisplayName("Carrier")]
    		public Nullable<int>  Carrier { get; set; }
    
    		    
    		/// <summary>
    		/// Booking No
    		/// </summary>        
    	//    [DisplayName("Booking No")]
            [MaxLength(20, ErrorMessage = "Booking No cannot be longer than 20 characters")]
    		public string  BookingNo { get; set; }
    
    		    
    		/// <summary>
    		/// Version
    		/// </summary>        
    	//    [DisplayName("Version")]
            [MaxLength(8, ErrorMessage = "Version cannot be longer than 8 characters")]
    		public byte[]  Version { get; set; }
    
    		    
    	}
    //}
    
}
