using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ValuePlus.ViewModel;



namespace ValuePlus.ViewModel.LocaleChainVP
{
    
    /// <summary>
    /// OIEntry class
    /// </summary>
    //[MetadataType(typeof(OIEntryViewModel))]
    //public  partial class OIEntry
    //{
    
    	/// <summary>
    	/// OIEntry Metadata class
    	/// </summary>
    	public   class OIEntryViewModel:ViewModelBase
    	{
    		    
    		/// <summary>
    		/// ID
    		/// </summary>        
    	//    [DisplayName("ID")]
            [Required(ErrorMessage = "ID is required")]
    		public int  ID { get; set; }
    
    		    
    		/// <summary>
    		/// HBLID
    		/// </summary>        
    	//    [DisplayName("HBLID")]
            [Required(ErrorMessage = "HBLID is required")]
    		public int  HBLID { get; set; }
    
    		    
    		/// <summary>
    		/// MBLID
    		/// </summary>        
    	//    [DisplayName("MBLID")]
            [Required(ErrorMessage = "MBLID is required")]
    		public int  MBLID { get; set; }
    
    		    
    		/// <summary>
    		/// PManifest
    		/// </summary>        
    	//    [DisplayName("PManifest")]
    		public Nullable<decimal>  PManifest { get; set; }
    
    		    
    		/// <summary>
    		/// POutturn
    		/// </summary>        
    	//    [DisplayName("POutturn")]
    		public Nullable<decimal>  POutturn { get; set; }
    
    		    
    		/// <summary>
    		/// PShort
    		/// </summary>        
    	//    [DisplayName("PShort")]
    		public Nullable<decimal>  PShort { get; set; }
    
    		    
    		/// <summary>
    		/// PSurplus
    		/// </summary>        
    	//    [DisplayName("PSurplus")]
    		public Nullable<decimal>  PSurplus { get; set; }
    
    		    
    		/// <summary>
    		/// PPillaged
    		/// </summary>        
    	//    [DisplayName("PPillaged")]
    		public Nullable<decimal>  PPillaged { get; set; }
    
    		    
    		/// <summary>
    		/// PDamaged
    		/// </summary>        
    	//    [DisplayName("PDamaged")]
    		public Nullable<decimal>  PDamaged { get; set; }
    
    		    
    		/// <summary>
    		/// WManifest
    		/// </summary>        
    	//    [DisplayName("WManifest")]
    		public Nullable<decimal>  WManifest { get; set; }
    
    		    
    		/// <summary>
    		/// WOutturn
    		/// </summary>        
    	//    [DisplayName("WOutturn")]
    		public Nullable<decimal>  WOutturn { get; set; }
    
    		    
    		/// <summary>
    		/// WShort
    		/// </summary>        
    	//    [DisplayName("WShort")]
    		public Nullable<decimal>  WShort { get; set; }
    
    		    
    		/// <summary>
    		/// WSurplus
    		/// </summary>        
    	//    [DisplayName("WSurplus")]
    		public Nullable<decimal>  WSurplus { get; set; }
    
    		    
    		/// <summary>
    		/// WPillaged
    		/// </summary>        
    	//    [DisplayName("WPillaged")]
    		public Nullable<decimal>  WPillaged { get; set; }
    
    		    
    		/// <summary>
    		/// WDamaged
    		/// </summary>        
    	//    [DisplayName("WDamaged")]
    		public Nullable<decimal>  WDamaged { get; set; }
    
    		    
    		/// <summary>
    		/// VManifest
    		/// </summary>        
    	//    [DisplayName("VManifest")]
    		public Nullable<decimal>  VManifest { get; set; }
    
    		    
    		/// <summary>
    		/// VOutturn
    		/// </summary>        
    	//    [DisplayName("VOutturn")]
    		public Nullable<decimal>  VOutturn { get; set; }
    
    		    
    		/// <summary>
    		/// VShort
    		/// </summary>        
    	//    [DisplayName("VShort")]
    		public Nullable<decimal>  VShort { get; set; }
    
    		    
    		/// <summary>
    		/// VSurplus
    		/// </summary>        
    	//    [DisplayName("VSurplus")]
    		public Nullable<decimal>  VSurplus { get; set; }
    
    		    
    		/// <summary>
    		/// VPillaged
    		/// </summary>        
    	//    [DisplayName("VPillaged")]
    		public Nullable<decimal>  VPillaged { get; set; }
    
    		    
    		/// <summary>
    		/// VDamaged
    		/// </summary>        
    	//    [DisplayName("VDamaged")]
    		public Nullable<decimal>  VDamaged { get; set; }
    
    		    
    		/// <summary>
    		/// Created By
    		/// </summary>        
    	//    [DisplayName("Created By")]
            [MaxLength(10, ErrorMessage = "Created By cannot be longer than 10 characters")]
    		public string  CreatedBy { get; set; }
    
    		    
    		/// <summary>
    		/// Created Date
    		/// </summary>        
    	//    [DisplayName("Created Date")]
    		public Nullable<System.DateTime>  CreatedDate { get; set; }
    
    		    
    		/// <summary>
    		/// Updated By
    		/// </summary>        
    	//    [DisplayName("Updated By")]
            [MaxLength(10, ErrorMessage = "Updated By cannot be longer than 10 characters")]
    		public string  UpdatedBy { get; set; }
    
    		    
    		/// <summary>
    		/// Updated Date
    		/// </summary>        
    	//    [DisplayName("Updated Date")]
    		public Nullable<System.DateTime>  UpdatedDate { get; set; }
    
    		    
    	}
    //}
    
}
