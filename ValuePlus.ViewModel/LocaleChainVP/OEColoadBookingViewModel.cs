using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ValuePlus.ViewModel;



namespace ValuePlus.ViewModel.LocaleChainVP
{
    
    /// <summary>
    /// OEColoadBooking class
    /// </summary>
    //[MetadataType(typeof(OEColoadBookingViewModel))]
    //public  partial class OEColoadBooking
    //{
    
    	/// <summary>
    	/// OEColoadBooking Metadata class
    	/// </summary>
    	public   class OEColoadBookingViewModel:ViewModelBase
    	{
    		    
    		/// <summary>
    		/// ID
    		/// </summary>        
    	//    [DisplayName("ID")]
            [Required(ErrorMessage = "ID is required")]
    		public int  ID { get; set; }
    
    		    
    		/// <summary>
    		/// Booking ID
    		/// </summary>        
    	//    [DisplayName("Booking ID")]
            [Required(ErrorMessage = "Booking ID is required")]
    		public int  BookingID { get; set; }
    
    		    
    		/// <summary>
    		/// Coload ID
    		/// </summary>        
    	//    [DisplayName("Coload ID")]
    		public Nullable<int>  ColoadID { get; set; }
    
    		    
    		/// <summary>
    		/// Link Station
    		/// </summary>        
    	//    [DisplayName("Link Station")]
            [MaxLength(10, ErrorMessage = "Link Station cannot be longer than 10 characters")]
    		public string  LinkStation { get; set; }
    
    		    
    		/// <summary>
    		/// PReceipt
    		/// </summary>        
    	//    [DisplayName("PReceipt")]
    		public Nullable<int>  PReceipt { get; set; }
    
    		    
    		/// <summary>
    		/// PLoading
    		/// </summary>        
    	//    [DisplayName("PLoading")]
    		public Nullable<int>  PLoading { get; set; }
    
    		    
    		/// <summary>
    		/// PDischarge
    		/// </summary>        
    	//    [DisplayName("PDischarge")]
    		public Nullable<int>  PDischarge { get; set; }
    
    		    
    		/// <summary>
    		/// PDelivery
    		/// </summary>        
    	//    [DisplayName("PDelivery")]
    		public Nullable<int>  PDelivery { get; set; }
    
    		    
    		/// <summary>
    		/// Shipper
    		/// </summary>        
    	//    [DisplayName("Shipper")]
    		public Nullable<int>  Shipper { get; set; }
    
    		    
    		/// <summary>
    		/// Shipper Name
    		/// </summary>        
    	//    [DisplayName("Shipper Name")]
            [MaxLength(100, ErrorMessage = "Shipper Name cannot be longer than 100 characters")]
    		public string  ShipperName { get; set; }
    
    		    
    		/// <summary>
    		/// Shipper Address
    		/// </summary>        
    	//    [DisplayName("Shipper Address")]
            [MaxLength(200, ErrorMessage = "Shipper Address cannot be longer than 200 characters")]
    		public string  ShipperAddress { get; set; }
    
    		    
    		/// <summary>
    		/// Shipper Map
    		/// </summary>        
    	//    [DisplayName("Shipper Map")]
    		public Nullable<int>  ShipperMap { get; set; }
    
    		    
    		/// <summary>
    		/// CNEE
    		/// </summary>        
    	//    [DisplayName("CNEE")]
    		public Nullable<int>  CNEE { get; set; }
    
    		    
    		/// <summary>
    		/// CNEEName
    		/// </summary>        
    	//    [DisplayName("CNEEName")]
            [MaxLength(100, ErrorMessage = "CNEEName cannot be longer than 100 characters")]
    		public string  CNEEName { get; set; }
    
    		    
    		/// <summary>
    		/// CNEEAddress
    		/// </summary>        
    	//    [DisplayName("CNEEAddress")]
            [MaxLength(200, ErrorMessage = "CNEEAddress cannot be longer than 200 characters")]
    		public string  CNEEAddress { get; set; }
    
    		    
    		/// <summary>
    		/// CNEEMap
    		/// </summary>        
    	//    [DisplayName("CNEEMap")]
    		public Nullable<int>  CNEEMap { get; set; }
    
    		    
    		/// <summary>
    		/// Notify
    		/// </summary>        
    	//    [DisplayName("Notify")]
    		public Nullable<int>  Notify { get; set; }
    
    		    
    		/// <summary>
    		/// Notify Name
    		/// </summary>        
    	//    [DisplayName("Notify Name")]
            [MaxLength(100, ErrorMessage = "Notify Name cannot be longer than 100 characters")]
    		public string  NotifyName { get; set; }
    
    		    
    		/// <summary>
    		/// Notify Address
    		/// </summary>        
    	//    [DisplayName("Notify Address")]
            [MaxLength(200, ErrorMessage = "Notify Address cannot be longer than 200 characters")]
    		public string  NotifyAddress { get; set; }
    
    		    
    		/// <summary>
    		/// Notify Map
    		/// </summary>        
    	//    [DisplayName("Notify Map")]
    		public Nullable<int>  NotifyMap { get; set; }
    
    		    
    		/// <summary>
    		/// Also Notify
    		/// </summary>        
    	//    [DisplayName("Also Notify")]
    		public Nullable<int>  AlsoNotify { get; set; }
    
    		    
    		/// <summary>
    		/// Also Notify Name
    		/// </summary>        
    	//    [DisplayName("Also Notify Name")]
            [MaxLength(100, ErrorMessage = "Also Notify Name cannot be longer than 100 characters")]
    		public string  AlsoNotifyName { get; set; }
    
    		    
    		/// <summary>
    		/// Also Notify Address
    		/// </summary>        
    	//    [DisplayName("Also Notify Address")]
            [MaxLength(200, ErrorMessage = "Also Notify Address cannot be longer than 200 characters")]
    		public string  AlsoNotifyAddress { get; set; }
    
    		    
    		/// <summary>
    		/// Also Notify Map
    		/// </summary>        
    	//    [DisplayName("Also Notify Map")]
    		public Nullable<int>  AlsoNotifyMap { get; set; }
    
    		    
    		/// <summary>
    		/// Truck
    		/// </summary>        
    	//    [DisplayName("Truck")]
    		public Nullable<int>  Truck { get; set; }
    
    		    
    		/// <summary>
    		/// Truck Name
    		/// </summary>        
    	//    [DisplayName("Truck Name")]
            [MaxLength(100, ErrorMessage = "Truck Name cannot be longer than 100 characters")]
    		public string  TruckName { get; set; }
    
    		    
    		/// <summary>
    		/// Truck Address
    		/// </summary>        
    	//    [DisplayName("Truck Address")]
            [MaxLength(200, ErrorMessage = "Truck Address cannot be longer than 200 characters")]
    		public string  TruckAddress { get; set; }
    
    		    
    		/// <summary>
    		/// Truck Map
    		/// </summary>        
    	//    [DisplayName("Truck Map")]
    		public Nullable<int>  TruckMap { get; set; }
    
    		    
    		/// <summary>
    		/// Cargo Loading Location
    		/// </summary>        
    	//    [DisplayName("Cargo Loading Location")]
            [MaxLength(200, ErrorMessage = "Cargo Loading Location cannot be longer than 200 characters")]
    		public string  CargoLoadingLocation { get; set; }
    
    		    
    		/// <summary>
    		/// PReceipt ETD
    		/// </summary>        
    	//    [DisplayName("PReceipt ETD")]
    		public Nullable<System.DateTime>  PReceiptETD { get; set; }
    
    		    
    		/// <summary>
    		/// PLoading ETD
    		/// </summary>        
    	//    [DisplayName("PLoading ETD")]
    		public Nullable<System.DateTime>  PLoadingETD { get; set; }
    
    		    
    		/// <summary>
    		/// PDischarge ETD
    		/// </summary>        
    	//    [DisplayName("PDischarge ETD")]
    		public Nullable<System.DateTime>  PDischargeETD { get; set; }
    
    		    
    		/// <summary>
    		/// PDelivery ETD
    		/// </summary>        
    	//    [DisplayName("PDelivery ETD")]
    		public Nullable<System.DateTime>  PDeliveryETD { get; set; }
    
    		    
    		/// <summary>
    		/// PLoading ATD
    		/// </summary>        
    	//    [DisplayName("PLoading ATD")]
    		public Nullable<System.DateTime>  PLoadingATD { get; set; }
    
    		    
    		/// <summary>
    		/// Final Dest ETD
    		/// </summary>        
    	//    [DisplayName("Final Dest ETD")]
    		public Nullable<System.DateTime>  FinalDestETD { get; set; }
    
    		    
    		/// <summary>
    		/// Cargo Available Date
    		/// </summary>        
    	//    [DisplayName("Cargo Available Date")]
    		public Nullable<System.DateTime>  CargoAvailableDate { get; set; }
    
    		    
    		/// <summary>
    		/// Instruction
    		/// </summary>        
    	//    [DisplayName("Instruction")]
            [MaxLength(255, ErrorMessage = "Instruction cannot be longer than 255 characters")]
    		public string  Instruction { get; set; }
    
    		    
    		/// <summary>
    		/// Move Type
    		/// </summary>        
    	//    [DisplayName("Move Type")]
            [MaxLength(20, ErrorMessage = "Move Type cannot be longer than 20 characters")]
    		public string  MoveType { get; set; }
    
    		    
    		/// <summary>
    		/// Freight Pay Type
    		/// </summary>        
    	//    [DisplayName("Freight Pay Type")]
            [MaxLength(20, ErrorMessage = "Freight Pay Type cannot be longer than 20 characters")]
    		public string  FreightPayType { get; set; }
    
    		    
    		/// <summary>
    		/// Nature Of Goods Type
    		/// </summary>        
    	//    [DisplayName("Nature Of Goods Type")]
            [MaxLength(50, ErrorMessage = "Nature Of Goods Type cannot be longer than 50 characters")]
    		public string  NatureOfGoodsType { get; set; }
    
    		    
    		/// <summary>
    		/// Load Type
    		/// </summary>        
    	//    [DisplayName("Load Type")]
            [MaxLength(20, ErrorMessage = "Load Type cannot be longer than 20 characters")]
    		public string  LoadType { get; set; }
    
    		    
    		/// <summary>
    		/// Last Milestone
    		/// </summary>        
    	//    [DisplayName("Last Milestone")]
            [MaxLength(50, ErrorMessage = "Last Milestone cannot be longer than 50 characters")]
    		public string  LastMilestone { get; set; }
    
    		    
    		/// <summary>
    		/// Milestone Date1
    		/// </summary>        
    	//    [DisplayName("Milestone Date1")]
    		public Nullable<System.DateTime>  MilestoneDate1 { get; set; }
    
    		    
    		/// <summary>
    		/// Milestone Date2
    		/// </summary>        
    	//    [DisplayName("Milestone Date2")]
    		public Nullable<System.DateTime>  MilestoneDate2 { get; set; }
    
    		    
    		/// <summary>
    		/// Milestone Date3
    		/// </summary>        
    	//    [DisplayName("Milestone Date3")]
    		public Nullable<System.DateTime>  MilestoneDate3 { get; set; }
    
    		    
    		/// <summary>
    		/// Milestone Date4
    		/// </summary>        
    	//    [DisplayName("Milestone Date4")]
    		public Nullable<System.DateTime>  MilestoneDate4 { get; set; }
    
    		    
    		/// <summary>
    		/// MBLNO
    		/// </summary>        
    	//    [DisplayName("MBLNO")]
            [MaxLength(50, ErrorMessage = "MBLNO cannot be longer than 50 characters")]
    		public string  MBLNO { get; set; }
    
    		    
    		/// <summary>
    		/// PCS
    		/// </summary>        
    	//    [DisplayName("PCS")]
    		public Nullable<int>  PCS { get; set; }
    
    		    
    		/// <summary>
    		/// PCSUOM
    		/// </summary>        
    	//    [DisplayName("PCSUOM")]
            [MaxLength(10, ErrorMessage = "PCSUOM cannot be longer than 10 characters")]
    		public string  PCSUOM { get; set; }
    
    		    
    		/// <summary>
    		/// Weight
    		/// </summary>        
    	//    [DisplayName("Weight")]
    		public Nullable<decimal>  Weight { get; set; }
    
    		    
    		/// <summary>
    		/// Weight UOM
    		/// </summary>        
    	//    [DisplayName("Weight UOM")]
            [MaxLength(10, ErrorMessage = "Weight UOM cannot be longer than 10 characters")]
    		public string  WeightUOM { get; set; }
    
    		    
    		/// <summary>
    		/// CBM
    		/// </summary>        
    	//    [DisplayName("CBM")]
    		public Nullable<decimal>  CBM { get; set; }
    
    		    
    		/// <summary>
    		/// CBMUOM
    		/// </summary>        
    	//    [DisplayName("CBMUOM")]
            [MaxLength(10, ErrorMessage = "CBMUOM cannot be longer than 10 characters")]
    		public string  CBMUOM { get; set; }
    
    		    
    		/// <summary>
    		/// SOCount
    		/// </summary>        
    	//    [DisplayName("SOCount")]
    		public Nullable<int>  SOCount { get; set; }
    
    		    
    		/// <summary>
    		/// CTNRType
    		/// </summary>        
    	//    [DisplayName("CTNRType")]
            [MaxLength(20, ErrorMessage = "CTNRType cannot be longer than 20 characters")]
    		public string  CTNRType { get; set; }
    
    		    
    		/// <summary>
    		/// Marks
    		/// </summary>        
    	//    [DisplayName("Marks")]
            [MaxLength(1000, ErrorMessage = "Marks cannot be longer than 1000 characters")]
    		public string  Marks { get; set; }
    
    		    
    		/// <summary>
    		/// Description
    		/// </summary>        
    	//    [DisplayName("Description")]
            [MaxLength(1000, ErrorMessage = "Description cannot be longer than 1000 characters")]
    		public string  Description { get; set; }
    
    		    
    		/// <summary>
    		/// Send
    		/// </summary>        
    	//    [DisplayName("Send")]
            [MaxLength(10, ErrorMessage = "Send cannot be longer than 10 characters")]
    		public string  Send { get; set; }
    
    		    
    		/// <summary>
    		/// Created By
    		/// </summary>        
    	//    [DisplayName("Created By")]
            [MaxLength(50, ErrorMessage = "Created By cannot be longer than 50 characters")]
    		public string  CreatedBy { get; set; }
    
    		    
    		/// <summary>
    		/// Created Date
    		/// </summary>        
    	//    [DisplayName("Created Date")]
    		public Nullable<System.DateTime>  CreatedDate { get; set; }
    
    		    
    		/// <summary>
    		/// Updated By
    		/// </summary>        
    	//    [DisplayName("Updated By")]
            [MaxLength(50, ErrorMessage = "Updated By cannot be longer than 50 characters")]
    		public string  UpdatedBy { get; set; }
    
    		    
    		/// <summary>
    		/// Updated Date
    		/// </summary>        
    	//    [DisplayName("Updated Date")]
    		public Nullable<System.DateTime>  UpdatedDate { get; set; }
    
    		    
    		/// <summary>
    		/// Received Date
    		/// </summary>        
    	//    [DisplayName("Received Date")]
    		public Nullable<System.DateTime>  ReceivedDate { get; set; }
    
    		    
    		/// <summary>
    		/// Processed Date
    		/// </summary>        
    	//    [DisplayName("Processed Date")]
    		public Nullable<System.DateTime>  ProcessedDate { get; set; }
    
    		    
    		/// <summary>
    		/// Status
    		/// </summary>        
    	//    [DisplayName("Status")]
            [MaxLength(10, ErrorMessage = "Status cannot be longer than 10 characters")]
    		public string  Status { get; set; }
    
    		    
    		/// <summary>
    		/// OWBType
    		/// </summary>        
    	//    [DisplayName("OWBType")]
            [MaxLength(10, ErrorMessage = "OWBType cannot be longer than 10 characters")]
    		public string  OWBType { get; set; }
    
    		    
    		/// <summary>
    		/// HBLID
    		/// </summary>        
    	//    [DisplayName("HBLID")]
    		public Nullable<int>  HBLID { get; set; }
    
    		    
    	}
    //}
    
}
