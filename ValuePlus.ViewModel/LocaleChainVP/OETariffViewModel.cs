using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ValuePlus.ViewModel;



namespace ValuePlus.ViewModel.LocaleChainVP
{
    
    /// <summary>
    /// OETariff class
    /// </summary>
    //[MetadataType(typeof(OETariffViewModel))]
    //public  partial class OETariff
    //{
    
    	/// <summary>
    	/// OETariff Metadata class
    	/// </summary>
    	public   class OETariffViewModel:ViewModelBase
    	{
    		    
    		/// <summary>
    		/// ID
    		/// </summary>        
    	//    [DisplayName("ID")]
            [Required(ErrorMessage = "ID is required")]
    		public int  ID { get; set; }
    
    		    
    		/// <summary>
    		/// Station ID
    		/// </summary>        
    	//    [DisplayName("Station ID")]
            [MaxLength(5, ErrorMessage = "Station ID cannot be longer than 5 characters")]
    		public string  StationID { get; set; }
    
    		    
    		/// <summary>
    		/// HBLID
    		/// </summary>        
    	//    [DisplayName("HBLID")]
    		public Nullable<int>  HBLID { get; set; }
    
    		    
    		/// <summary>
    		/// Tariff No
    		/// </summary>        
    	//    [DisplayName("Tariff No")]
            [MaxLength(50, ErrorMessage = "Tariff No cannot be longer than 50 characters")]
    		public string  TariffNo { get; set; }
    
    		    
    		/// <summary>
    		/// Currency
    		/// </summary>        
    	//    [DisplayName("Currency")]
            [MaxLength(10, ErrorMessage = "Currency cannot be longer than 10 characters")]
    		public string  Currency { get; set; }
    
    		    
    		/// <summary>
    		/// Amount
    		/// </summary>        
    	//    [DisplayName("Amount")]
    		public Nullable<double>  Amount { get; set; }
    
    		    
    		/// <summary>
    		/// Collect Amount
    		/// </summary>        
    	//    [DisplayName("Collect Amount")]
    		public Nullable<double>  CollectAmount { get; set; }
    
    		    
    		/// <summary>
    		/// Prepaid Amount
    		/// </summary>        
    	//    [DisplayName("Prepaid Amount")]
    		public Nullable<double>  PrepaidAmount { get; set; }
    
    		    
    		/// <summary>
    		/// Created By
    		/// </summary>        
    	//    [DisplayName("Created By")]
            [MaxLength(50, ErrorMessage = "Created By cannot be longer than 50 characters")]
    		public string  CreatedBy { get; set; }
    
    		    
    		/// <summary>
    		/// Created DT
    		/// </summary>        
    	//    [DisplayName("Created DT")]
    		public Nullable<System.DateTime>  CreatedDT { get; set; }
    
    		    
    		/// <summary>
    		/// Updated By
    		/// </summary>        
    	//    [DisplayName("Updated By")]
            [MaxLength(50, ErrorMessage = "Updated By cannot be longer than 50 characters")]
    		public string  UpdatedBy { get; set; }
    
    		    
    		/// <summary>
    		/// Updated DT
    		/// </summary>        
    	//    [DisplayName("Updated DT")]
    		public Nullable<System.DateTime>  UpdatedDT { get; set; }
    
    		    
    		/// <summary>
    		/// Version
    		/// </summary>        
    	//    [DisplayName("Version")]
            [Required(ErrorMessage = "Version is required")]
            [MaxLength(8, ErrorMessage = "Version cannot be longer than 8 characters")]
    		public byte[]  Version { get; set; }
    
    		    
    	}
    //}
    
}
