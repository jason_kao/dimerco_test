using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ValuePlus.ViewModel;



namespace ValuePlus.ViewModel.LocaleChainVP
{
    
    /// <summary>
    /// AEPODMilestone class
    /// </summary>
    //[MetadataType(typeof(AEPODMilestoneViewModel))]
    //public  partial class AEPODMilestone
    //{
    
    	/// <summary>
    	/// AEPODMilestone Metadata class
    	/// </summary>
    	public   class AEPODMilestoneViewModel:ViewModelBase
    	{
    		    
    		/// <summary>
    		/// ID
    		/// </summary>        
    	//    [DisplayName("ID")]
            [Required(ErrorMessage = "ID is required")]
    		public int  ID { get; set; }
    
    		    
    		/// <summary>
    		/// Station ID
    		/// </summary>        
    	//    [DisplayName("Station ID")]
            [Required(ErrorMessage = "Station ID is required")]
            [MaxLength(10, ErrorMessage = "Station ID cannot be longer than 10 characters")]
    		public string  StationID { get; set; }
    
    		    
    		/// <summary>
    		/// Source ID
    		/// </summary>        
    	//    [DisplayName("Source ID")]
            [Required(ErrorMessage = "Source ID is required")]
    		public int  SourceID { get; set; }
    
    		    
    		/// <summary>
    		/// PODTEMPLID
    		/// </summary>        
    	//    [DisplayName("PODTEMPLID")]
    		public Nullable<int>  PODTEMPLID { get; set; }
    
    		    
    		/// <summary>
    		/// PODTEMPLDTID
    		/// </summary>        
    	//    [DisplayName("PODTEMPLDTID")]
            [Required(ErrorMessage = "PODTEMPLDTID is required")]
    		public int  PODTEMPLDTID { get; set; }
    
    		    
    		/// <summary>
    		/// Mode
    		/// </summary>        
    	//    [DisplayName("Mode")]
            [MaxLength(5, ErrorMessage = "Mode cannot be longer than 5 characters")]
    		public string  Mode { get; set; }
    
    		    
    		/// <summary>
    		/// MSDate
    		/// </summary>        
    	//    [DisplayName("MSDate")]
    		public Nullable<System.DateTime>  MSDate { get; set; }
    
    		    
    		/// <summary>
    		/// MSParty
    		/// </summary>        
    	//    [DisplayName("MSParty")]
            [MaxLength(50, ErrorMessage = "MSParty cannot be longer than 50 characters")]
    		public string  MSParty { get; set; }
    
    		    
    		/// <summary>
    		/// MSSign By
    		/// </summary>        
    	//    [DisplayName("MSSign By")]
            [MaxLength(50, ErrorMessage = "MSSign By cannot be longer than 50 characters")]
    		public string  MSSignBy { get; set; }
    
    		    
    		/// <summary>
    		/// MSReason
    		/// </summary>        
    	//    [DisplayName("MSReason")]
            [MaxLength(200, ErrorMessage = "MSReason cannot be longer than 200 characters")]
    		public string  MSReason { get; set; }
    
    		    
    		/// <summary>
    		/// Created By
    		/// </summary>        
    	//    [DisplayName("Created By")]
            [MaxLength(10, ErrorMessage = "Created By cannot be longer than 10 characters")]
    		public string  CreatedBy { get; set; }
    
    		    
    		/// <summary>
    		/// Created Date
    		/// </summary>        
    	//    [DisplayName("Created Date")]
    		public Nullable<System.DateTime>  CreatedDate { get; set; }
    
    		    
    		/// <summary>
    		/// Updated By
    		/// </summary>        
    	//    [DisplayName("Updated By")]
            [MaxLength(10, ErrorMessage = "Updated By cannot be longer than 10 characters")]
    		public string  UpdatedBy { get; set; }
    
    		    
    		/// <summary>
    		/// Updated Date
    		/// </summary>        
    	//    [DisplayName("Updated Date")]
    		public Nullable<System.DateTime>  UpdatedDate { get; set; }
    
    		    
    	}
    //}
    
}
