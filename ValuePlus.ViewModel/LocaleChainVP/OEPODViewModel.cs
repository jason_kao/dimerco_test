using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ValuePlus.ViewModel;



namespace ValuePlus.ViewModel.LocaleChainVP
{
    
    /// <summary>
    /// OEPOD class
    /// </summary>
    //[MetadataType(typeof(OEPODViewModel))]
    //public  partial class OEPOD
    //{
    
    	/// <summary>
    	/// OEPOD Metadata class
    	/// </summary>
    	public   class OEPODViewModel:ViewModelBase
    	{
    		    
    		/// <summary>
    		/// ID
    		/// </summary>        
    	//    [DisplayName("ID")]
            [Required(ErrorMessage = "ID is required")]
    		public int  ID { get; set; }
    
    		    
    		/// <summary>
    		/// Station ID
    		/// </summary>        
    	//    [DisplayName("Station ID")]
            [MaxLength(5, ErrorMessage = "Station ID cannot be longer than 5 characters")]
    		public string  StationID { get; set; }
    
    		    
    		/// <summary>
    		/// Src ID
    		/// </summary>        
    	//    [DisplayName("Src ID")]
    		public Nullable<int>  SrcID { get; set; }
    
    		    
    		/// <summary>
    		/// Src Station ID
    		/// </summary>        
    	//    [DisplayName("Src Station ID")]
            [MaxLength(5, ErrorMessage = "Src Station ID cannot be longer than 5 characters")]
    		public string  SrcStationID { get; set; }
    
    		    
    		/// <summary>
    		/// HBLID
    		/// </summary>        
    	//    [DisplayName("HBLID")]
    		public Nullable<int>  HBLID { get; set; }
    
    		    
    		/// <summary>
    		/// Document Pick DT
    		/// </summary>        
    	//    [DisplayName("Document Pick DT")]
    		public Nullable<System.DateTime>  DocumentPickDT { get; set; }
    
    		    
    		/// <summary>
    		/// Notify Party DT
    		/// </summary>        
    	//    [DisplayName("Notify Party DT")]
    		public Nullable<System.DateTime>  NotifyPartyDT { get; set; }
    
    		    
    		/// <summary>
    		/// Notify Party
    		/// </summary>        
    	//    [DisplayName("Notify Party")]
    		public Nullable<int>  NotifyParty { get; set; }
    
    		    
    		/// <summary>
    		/// Customs Release DT
    		/// </summary>        
    	//    [DisplayName("Customs Release DT")]
    		public Nullable<System.DateTime>  CustomsReleaseDT { get; set; }
    
    		    
    		/// <summary>
    		/// Customs Release No
    		/// </summary>        
    	//    [DisplayName("Customs Release No")]
            [MaxLength(60, ErrorMessage = "Customs Release No cannot be longer than 60 characters")]
    		public string  CustomsReleaseNo { get; set; }
    
    		    
    		/// <summary>
    		/// Document Release DT
    		/// </summary>        
    	//    [DisplayName("Document Release DT")]
    		public Nullable<System.DateTime>  DocumentReleaseDT { get; set; }
    
    		    
    		/// <summary>
    		/// Document Release Party
    		/// </summary>        
    	//    [DisplayName("Document Release Party")]
    		public Nullable<int>  DocumentReleaseParty { get; set; }
    
    		    
    		/// <summary>
    		/// Document Release Type
    		/// </summary>        
    	//    [DisplayName("Document Release Type")]
            [MaxLength(20, ErrorMessage = "Document Release Type cannot be longer than 20 characters")]
    		public string  DocumentReleaseType { get; set; }
    
    		    
    		/// <summary>
    		/// Freight Release DT
    		/// </summary>        
    	//    [DisplayName("Freight Release DT")]
    		public Nullable<System.DateTime>  FreightReleaseDT { get; set; }
    
    		    
    		/// <summary>
    		/// Freight Release Party
    		/// </summary>        
    	//    [DisplayName("Freight Release Party")]
    		public Nullable<int>  FreightReleaseParty { get; set; }
    
    		    
    		/// <summary>
    		/// Freight Release Type
    		/// </summary>        
    	//    [DisplayName("Freight Release Type")]
            [MaxLength(20, ErrorMessage = "Freight Release Type cannot be longer than 20 characters")]
    		public string  FreightReleaseType { get; set; }
    
    		    
    		/// <summary>
    		/// Created By
    		/// </summary>        
    	//    [DisplayName("Created By")]
            [MaxLength(50, ErrorMessage = "Created By cannot be longer than 50 characters")]
    		public string  CreatedBy { get; set; }
    
    		    
    		/// <summary>
    		/// Created DT
    		/// </summary>        
    	//    [DisplayName("Created DT")]
    		public Nullable<System.DateTime>  CreatedDT { get; set; }
    
    		    
    		/// <summary>
    		/// Updated By
    		/// </summary>        
    	//    [DisplayName("Updated By")]
            [MaxLength(50, ErrorMessage = "Updated By cannot be longer than 50 characters")]
    		public string  UpdatedBy { get; set; }
    
    		    
    		/// <summary>
    		/// Updated DT
    		/// </summary>        
    	//    [DisplayName("Updated DT")]
    		public Nullable<System.DateTime>  UpdatedDT { get; set; }
    
    		    
    		/// <summary>
    		/// Version
    		/// </summary>        
    	//    [DisplayName("Version")]
            [Required(ErrorMessage = "Version is required")]
            [MaxLength(8, ErrorMessage = "Version cannot be longer than 8 characters")]
    		public byte[]  Version { get; set; }
    
    		    
    		/// <summary>
    		/// Notify Party Type
    		/// </summary>        
    	//    [DisplayName("Notify Party Type")]
            [MaxLength(20, ErrorMessage = "Notify Party Type cannot be longer than 20 characters")]
    		public string  NotifyPartyType { get; set; }
    
    		    
    		/// <summary>
    		/// Container Unstuff DT
    		/// </summary>        
    	//    [DisplayName("Container Unstuff DT")]
    		public Nullable<System.DateTime>  ContainerUnstuffDT { get; set; }
    
    		    
    		/// <summary>
    		/// Remarks
    		/// </summary>        
    	//    [DisplayName("Remarks")]
            [MaxLength(500, ErrorMessage = "Remarks cannot be longer than 500 characters")]
    		public string  Remarks { get; set; }
    
    		    
    	}
    //}
    
}
