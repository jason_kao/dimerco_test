using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ValuePlus.ViewModel;



namespace ValuePlus.ViewModel.LocaleChainVP
{
    
    /// <summary>
    /// AEBookConsole class
    /// </summary>
    //[MetadataType(typeof(AEBookConsoleViewModel))]
    //public  partial class AEBookConsole
    //{
    
    	/// <summary>
    	/// AEBookConsole Metadata class
    	/// </summary>
    	public   class AEBookConsoleViewModel:ViewModelBase
    	{
    		    
    		/// <summary>
    		/// ID
    		/// </summary>        
    	//    [DisplayName("ID")]
            [Required(ErrorMessage = "ID is required")]
    		public int  ID { get; set; }
    
    		    
    		/// <summary>
    		/// Station ID
    		/// </summary>        
    	//    [DisplayName("Station ID")]
            [Required(ErrorMessage = "Station ID is required")]
            [MaxLength(10, ErrorMessage = "Station ID cannot be longer than 10 characters")]
    		public string  StationID { get; set; }
    
    		    
    		/// <summary>
    		/// Org HAWBID
    		/// </summary>        
    	//    [DisplayName("Org HAWBID")]
            [Required(ErrorMessage = "Org HAWBID is required")]
    		public int  OrgHAWBID { get; set; }
    
    		    
    		/// <summary>
    		/// Org Station ID
    		/// </summary>        
    	//    [DisplayName("Org Station ID")]
            [Required(ErrorMessage = "Org Station ID is required")]
            [MaxLength(10, ErrorMessage = "Org Station ID cannot be longer than 10 characters")]
    		public string  OrgStationID { get; set; }
    
    		    
    		/// <summary>
    		/// Cargo Avail Date
    		/// </summary>        
    	//    [DisplayName("Cargo Avail Date")]
    		public Nullable<System.DateTime>  CargoAvailDate { get; set; }
    
    		    
    		/// <summary>
    		/// Area
    		/// </summary>        
    	//    [DisplayName("Area")]
            [MaxLength(10, ErrorMessage = "Area cannot be longer than 10 characters")]
    		public string  Area { get; set; }
    
    		    
    		/// <summary>
    		/// Service Level
    		/// </summary>        
    	//    [DisplayName("Service Level")]
            [MaxLength(50, ErrorMessage = "Service Level cannot be longer than 50 characters")]
    		public string  ServiceLevel { get; set; }
    
    		    
    		/// <summary>
    		/// DEPT
    		/// </summary>        
    	//    [DisplayName("DEPT")]
            [MaxLength(10, ErrorMessage = "DEPT cannot be longer than 10 characters")]
    		public string  DEPT { get; set; }
    
    		    
    		/// <summary>
    		/// DSTN
    		/// </summary>        
    	//    [DisplayName("DSTN")]
            [MaxLength(10, ErrorMessage = "DSTN cannot be longer than 10 characters")]
    		public string  DSTN { get; set; }
    
    		    
    		/// <summary>
    		/// Booking PCS
    		/// </summary>        
    	//    [DisplayName("Booking PCS")]
    		public Nullable<int>  BookingPCS { get; set; }
    
    		    
    		/// <summary>
    		/// Booking PCSUOM
    		/// </summary>        
    	//    [DisplayName("Booking PCSUOM")]
            [MaxLength(10, ErrorMessage = "Booking PCSUOM cannot be longer than 10 characters")]
    		public string  BookingPCSUOM { get; set; }
    
    		    
    		/// <summary>
    		/// Booking Weight
    		/// </summary>        
    	//    [DisplayName("Booking Weight")]
    		public Nullable<decimal>  BookingWeight { get; set; }
    
    		    
    		/// <summary>
    		/// Booking CWT
    		/// </summary>        
    	//    [DisplayName("Booking CWT")]
    		public Nullable<decimal>  BookingCWT { get; set; }
    
    		    
    		/// <summary>
    		/// Booking Volume
    		/// </summary>        
    	//    [DisplayName("Booking Volume")]
    		public Nullable<decimal>  BookingVolume { get; set; }
    
    		    
    		/// <summary>
    		/// Shipper ID
    		/// </summary>        
    	//    [DisplayName("Shipper ID")]
            [MaxLength(10, ErrorMessage = "Shipper ID cannot be longer than 10 characters")]
    		public string  ShipperID { get; set; }
    
    		    
    		/// <summary>
    		/// Shipper Local Name
    		/// </summary>        
    	//    [DisplayName("Shipper Local Name")]
            [MaxLength(100, ErrorMessage = "Shipper Local Name cannot be longer than 100 characters")]
    		public string  ShipperLocalName { get; set; }
    
    		    
    		/// <summary>
    		/// Commodity
    		/// </summary>        
    	//    [DisplayName("Commodity")]
            [MaxLength(100, ErrorMessage = "Commodity cannot be longer than 100 characters")]
    		public string  Commodity { get; set; }
    
    		    
    		/// <summary>
    		/// Doc Type
    		/// </summary>        
    	//    [DisplayName("Doc Type")]
            [MaxLength(50, ErrorMessage = "Doc Type cannot be longer than 50 characters")]
    		public string  DocType { get; set; }
    
    		    
    		/// <summary>
    		/// Carrier
    		/// </summary>        
    	//    [DisplayName("Carrier")]
            [MaxLength(50, ErrorMessage = "Carrier cannot be longer than 50 characters")]
    		public string  Carrier { get; set; }
    
    		    
    		/// <summary>
    		/// HAWBNo
    		/// </summary>        
    	//    [DisplayName("HAWBNo")]
            [MaxLength(50, ErrorMessage = "HAWBNo cannot be longer than 50 characters")]
    		public string  HAWBNo { get; set; }
    
    		    
    		/// <summary>
    		/// CS
    		/// </summary>        
    	//    [DisplayName("CS")]
            [MaxLength(50, ErrorMessage = "CS cannot be longer than 50 characters")]
    		public string  CS { get; set; }
    
    		    
    		/// <summary>
    		/// Rate
    		/// </summary>        
    	//    [DisplayName("Rate")]
            [MaxLength(50, ErrorMessage = "Rate cannot be longer than 50 characters")]
    		public string  Rate { get; set; }
    
    		    
    		/// <summary>
    		/// Sales
    		/// </summary>        
    	//    [DisplayName("Sales")]
            [MaxLength(50, ErrorMessage = "Sales cannot be longer than 50 characters")]
    		public string  Sales { get; set; }
    
    		    
    		/// <summary>
    		/// Shiping Remark
    		/// </summary>        
    	//    [DisplayName("Shiping Remark")]
            [MaxLength(200, ErrorMessage = "Shiping Remark cannot be longer than 200 characters")]
    		public string  ShipingRemark { get; set; }
    
    		    
    		/// <summary>
    		/// Booking No
    		/// </summary>        
    	//    [DisplayName("Booking No")]
            [MaxLength(50, ErrorMessage = "Booking No cannot be longer than 50 characters")]
    		public string  BookingNo { get; set; }
    
    		    
    		/// <summary>
    		/// He Xiao Dan No
    		/// </summary>        
    	//    [DisplayName("He Xiao Dan No")]
            [MaxLength(50, ErrorMessage = "He Xiao Dan No cannot be longer than 50 characters")]
    		public string  HeXiaoDanNo { get; set; }
    
    		    
    		/// <summary>
    		/// Tui Dan No
    		/// </summary>        
    	//    [DisplayName("Tui Dan No")]
            [MaxLength(50, ErrorMessage = "Tui Dan No cannot be longer than 50 characters")]
    		public string  TuiDanNo { get; set; }
    
    		    
    		/// <summary>
    		/// Created By
    		/// </summary>        
    	//    [DisplayName("Created By")]
            [MaxLength(10, ErrorMessage = "Created By cannot be longer than 10 characters")]
    		public string  CreatedBy { get; set; }
    
    		    
    		/// <summary>
    		/// Created Date
    		/// </summary>        
    	//    [DisplayName("Created Date")]
    		public Nullable<System.DateTime>  CreatedDate { get; set; }
    
    		    
    		/// <summary>
    		/// Updated By
    		/// </summary>        
    	//    [DisplayName("Updated By")]
            [MaxLength(10, ErrorMessage = "Updated By cannot be longer than 10 characters")]
    		public string  UpdatedBy { get; set; }
    
    		    
    		/// <summary>
    		/// Updated Date
    		/// </summary>        
    	//    [DisplayName("Updated Date")]
    		public Nullable<System.DateTime>  UpdatedDate { get; set; }
    
    		    
    		/// <summary>
    		/// Load Master Loader
    		/// </summary>        
    	//    [DisplayName("Load Master Loader")]
            [MaxLength(50, ErrorMessage = "Load Master Loader cannot be longer than 50 characters")]
    		public string  LoadMasterLoader { get; set; }
    
    		    
    		/// <summary>
    		/// Load MAWBNo
    		/// </summary>        
    	//    [DisplayName("Load MAWBNo")]
            [MaxLength(50, ErrorMessage = "Load MAWBNo cannot be longer than 50 characters")]
    		public string  LoadMAWBNo { get; set; }
    
    		    
    		/// <summary>
    		/// Load Carrier
    		/// </summary>        
    	//    [DisplayName("Load Carrier")]
            [MaxLength(50, ErrorMessage = "Load Carrier cannot be longer than 50 characters")]
    		public string  LoadCarrier { get; set; }
    
    		    
    		/// <summary>
    		/// Load FLTNo
    		/// </summary>        
    	//    [DisplayName("Load FLTNo")]
            [MaxLength(50, ErrorMessage = "Load FLTNo cannot be longer than 50 characters")]
    		public string  LoadFLTNo { get; set; }
    
    		    
    		/// <summary>
    		/// Load ETD
    		/// </summary>        
    	//    [DisplayName("Load ETD")]
    		public Nullable<System.DateTime>  LoadETD { get; set; }
    
    		    
    		/// <summary>
    		/// Load ETA
    		/// </summary>        
    	//    [DisplayName("Load ETA")]
    		public Nullable<System.DateTime>  LoadETA { get; set; }
    
    		    
    		/// <summary>
    		/// Load ATD
    		/// </summary>        
    	//    [DisplayName("Load ATD")]
    		public Nullable<System.DateTime>  LoadATD { get; set; }
    
    		    
    		/// <summary>
    		/// Load PCS
    		/// </summary>        
    	//    [DisplayName("Load PCS")]
    		public Nullable<int>  LoadPCS { get; set; }
    
    		    
    		/// <summary>
    		/// Load PCSType
    		/// </summary>        
    	//    [DisplayName("Load PCSType")]
            [MaxLength(50, ErrorMessage = "Load PCSType cannot be longer than 50 characters")]
    		public string  LoadPCSType { get; set; }
    
    		    
    		/// <summary>
    		/// Load Weight
    		/// </summary>        
    	//    [DisplayName("Load Weight")]
    		public Nullable<decimal>  LoadWeight { get; set; }
    
    		    
    		/// <summary>
    		/// Load Weight Type
    		/// </summary>        
    	//    [DisplayName("Load Weight Type")]
            [MaxLength(50, ErrorMessage = "Load Weight Type cannot be longer than 50 characters")]
    		public string  LoadWeightType { get; set; }
    
    		    
    		/// <summary>
    		/// Load Volume
    		/// </summary>        
    	//    [DisplayName("Load Volume")]
    		public Nullable<decimal>  LoadVolume { get; set; }
    
    		    
    		/// <summary>
    		/// Load Volume Type
    		/// </summary>        
    	//    [DisplayName("Load Volume Type")]
            [MaxLength(50, ErrorMessage = "Load Volume Type cannot be longer than 50 characters")]
    		public string  LoadVolumeType { get; set; }
    
    		    
    		/// <summary>
    		/// Load VWT
    		/// </summary>        
    	//    [DisplayName("Load VWT")]
    		public Nullable<decimal>  LoadVWT { get; set; }
    
    		    
    		/// <summary>
    		/// Load Terminal
    		/// </summary>        
    	//    [DisplayName("Load Terminal")]
            [MaxLength(10, ErrorMessage = "Load Terminal cannot be longer than 10 characters")]
    		public string  LoadTerminal { get; set; }
    
    		    
    		/// <summary>
    		/// Load Pallet
    		/// </summary>        
    	//    [DisplayName("Load Pallet")]
            [MaxLength(50, ErrorMessage = "Load Pallet cannot be longer than 50 characters")]
    		public string  LoadPallet { get; set; }
    
    		    
    		/// <summary>
    		/// Load Status
    		/// </summary>        
    	//    [DisplayName("Load Status")]
            [MaxLength(10, ErrorMessage = "Load Status cannot be longer than 10 characters")]
    		public string  LoadStatus { get; set; }
    
    		    
    		/// <summary>
    		/// Load Est Cost
    		/// </summary>        
    	//    [DisplayName("Load Est Cost")]
            [MaxLength(50, ErrorMessage = "Load Est Cost cannot be longer than 50 characters")]
    		public string  LoadEstCost { get; set; }
    
    		    
    		/// <summary>
    		/// Load Remark
    		/// </summary>        
    	//    [DisplayName("Load Remark")]
            [MaxLength(200, ErrorMessage = "Load Remark cannot be longer than 200 characters")]
    		public string  LoadRemark { get; set; }
    
    		    
    		/// <summary>
    		/// Load Created By
    		/// </summary>        
    	//    [DisplayName("Load Created By")]
            [MaxLength(50, ErrorMessage = "Load Created By cannot be longer than 50 characters")]
    		public string  LoadCreatedBy { get; set; }
    
    		    
    		/// <summary>
    		/// Load Created Date
    		/// </summary>        
    	//    [DisplayName("Load Created Date")]
    		public Nullable<System.DateTime>  LoadCreatedDate { get; set; }
    
    		    
    		/// <summary>
    		/// Load Updated By
    		/// </summary>        
    	//    [DisplayName("Load Updated By")]
            [MaxLength(50, ErrorMessage = "Load Updated By cannot be longer than 50 characters")]
    		public string  LoadUpdatedBy { get; set; }
    
    		    
    		/// <summary>
    		/// Load Updated Date
    		/// </summary>        
    	//    [DisplayName("Load Updated Date")]
    		public Nullable<System.DateTime>  LoadUpdatedDate { get; set; }
    
    		    
    		/// <summary>
    		/// WHPCS
    		/// </summary>        
    	//    [DisplayName("WHPCS")]
    		public Nullable<int>  WHPCS { get; set; }
    
    		    
    		/// <summary>
    		/// WHPCSType
    		/// </summary>        
    	//    [DisplayName("WHPCSType")]
            [MaxLength(50, ErrorMessage = "WHPCSType cannot be longer than 50 characters")]
    		public string  WHPCSType { get; set; }
    
    		    
    		/// <summary>
    		/// WHWeight
    		/// </summary>        
    	//    [DisplayName("WHWeight")]
    		public Nullable<decimal>  WHWeight { get; set; }
    
    		    
    		/// <summary>
    		/// WHWeight Type
    		/// </summary>        
    	//    [DisplayName("WHWeight Type")]
            [MaxLength(50, ErrorMessage = "WHWeight Type cannot be longer than 50 characters")]
    		public string  WHWeightType { get; set; }
    
    		    
    		/// <summary>
    		/// WHVolume
    		/// </summary>        
    	//    [DisplayName("WHVolume")]
    		public Nullable<decimal>  WHVolume { get; set; }
    
    		    
    		/// <summary>
    		/// WHVolume Type
    		/// </summary>        
    	//    [DisplayName("WHVolume Type")]
            [MaxLength(50, ErrorMessage = "WHVolume Type cannot be longer than 50 characters")]
    		public string  WHVolumeType { get; set; }
    
    		    
    		/// <summary>
    		/// WHCargo Status
    		/// </summary>        
    	//    [DisplayName("WHCargo Status")]
            [MaxLength(50, ErrorMessage = "WHCargo Status cannot be longer than 50 characters")]
    		public string  WHCargoStatus { get; set; }
    
    		    
    		/// <summary>
    		/// WHRemark
    		/// </summary>        
    	//    [DisplayName("WHRemark")]
            [MaxLength(200, ErrorMessage = "WHRemark cannot be longer than 200 characters")]
    		public string  WHRemark { get; set; }
    
    		    
    		/// <summary>
    		/// WHLocation
    		/// </summary>        
    	//    [DisplayName("WHLocation")]
            [MaxLength(50, ErrorMessage = "WHLocation cannot be longer than 50 characters")]
    		public string  WHLocation { get; set; }
    
    		    
    		/// <summary>
    		/// WHPullout DT
    		/// </summary>        
    	//    [DisplayName("WHPullout DT")]
    		public Nullable<System.DateTime>  WHPulloutDT { get; set; }
    
    		    
    		/// <summary>
    		/// WHReceipt No
    		/// </summary>        
    	//    [DisplayName("WHReceipt No")]
            [MaxLength(50, ErrorMessage = "WHReceipt No cannot be longer than 50 characters")]
    		public string  WHReceiptNo { get; set; }
    
    		    
    		/// <summary>
    		/// WHCreated By
    		/// </summary>        
    	//    [DisplayName("WHCreated By")]
            [MaxLength(50, ErrorMessage = "WHCreated By cannot be longer than 50 characters")]
    		public string  WHCreatedBy { get; set; }
    
    		    
    		/// <summary>
    		/// WHCreated Date
    		/// </summary>        
    	//    [DisplayName("WHCreated Date")]
    		public Nullable<System.DateTime>  WHCreatedDate { get; set; }
    
    		    
    		/// <summary>
    		/// WHUpdated By
    		/// </summary>        
    	//    [DisplayName("WHUpdated By")]
            [MaxLength(50, ErrorMessage = "WHUpdated By cannot be longer than 50 characters")]
    		public string  WHUpdatedBy { get; set; }
    
    		    
    		/// <summary>
    		/// WHUpdated Date
    		/// </summary>        
    	//    [DisplayName("WHUpdated Date")]
    		public Nullable<System.DateTime>  WHUpdatedDate { get; set; }
    
    		    
    		/// <summary>
    		/// WHHandover Date
    		/// </summary>        
    	//    [DisplayName("WHHandover Date")]
    		public Nullable<System.DateTime>  WHHandoverDate { get; set; }
    
    		    
    		/// <summary>
    		/// WHArrival Date
    		/// </summary>        
    	//    [DisplayName("WHArrival Date")]
    		public Nullable<System.DateTime>  WHArrivalDate { get; set; }
    
    		    
    		/// <summary>
    		/// WHPallet
    		/// </summary>        
    	//    [DisplayName("WHPallet")]
            [MaxLength(50, ErrorMessage = "WHPallet cannot be longer than 50 characters")]
    		public string  WHPallet { get; set; }
    
    		    
    		/// <summary>
    		/// WHDimension
    		/// </summary>        
    	//    [DisplayName("WHDimension")]
            [MaxLength(200, ErrorMessage = "WHDimension cannot be longer than 200 characters")]
    		public string  WHDimension { get; set; }
    
    		    
    		/// <summary>
    		/// CSDeclare Party
    		/// </summary>        
    	//    [DisplayName("CSDeclare Party")]
    		public Nullable<int>  CSDeclareParty { get; set; }
    
    		    
    		/// <summary>
    		/// CSHandled By
    		/// </summary>        
    	//    [DisplayName("CSHandled By")]
    		public Nullable<int>  CSHandledBy { get; set; }
    
    		    
    		/// <summary>
    		/// CSDec Date
    		/// </summary>        
    	//    [DisplayName("CSDec Date")]
    		public Nullable<System.DateTime>  CSDecDate { get; set; }
    
    		    
    		/// <summary>
    		/// CSRelease Date
    		/// </summary>        
    	//    [DisplayName("CSRelease Date")]
    		public Nullable<System.DateTime>  CSReleaseDate { get; set; }
    
    		    
    		/// <summary>
    		/// CSStatus
    		/// </summary>        
    	//    [DisplayName("CSStatus")]
            [MaxLength(50, ErrorMessage = "CSStatus cannot be longer than 50 characters")]
    		public string  CSStatus { get; set; }
    
    		    
    		/// <summary>
    		/// CSRemark
    		/// </summary>        
    	//    [DisplayName("CSRemark")]
            [MaxLength(200, ErrorMessage = "CSRemark cannot be longer than 200 characters")]
    		public string  CSRemark { get; set; }
    
    		    
    		/// <summary>
    		/// CSAvail Date
    		/// </summary>        
    	//    [DisplayName("CSAvail Date")]
    		public Nullable<System.DateTime>  CSAvailDate { get; set; }
    
    		    
    		/// <summary>
    		/// CSCreated By
    		/// </summary>        
    	//    [DisplayName("CSCreated By")]
            [MaxLength(50, ErrorMessage = "CSCreated By cannot be longer than 50 characters")]
    		public string  CSCreatedBy { get; set; }
    
    		    
    		/// <summary>
    		/// CSCreated Date
    		/// </summary>        
    	//    [DisplayName("CSCreated Date")]
    		public Nullable<System.DateTime>  CSCreatedDate { get; set; }
    
    		    
    		/// <summary>
    		/// CSUpdated By
    		/// </summary>        
    	//    [DisplayName("CSUpdated By")]
            [MaxLength(50, ErrorMessage = "CSUpdated By cannot be longer than 50 characters")]
    		public string  CSUpdatedBy { get; set; }
    
    		    
    		/// <summary>
    		/// CSUpdated Date
    		/// </summary>        
    	//    [DisplayName("CSUpdated Date")]
    		public Nullable<System.DateTime>  CSUpdatedDate { get; set; }
    
    		    
    		/// <summary>
    		/// RTHand Book No
    		/// </summary>        
    	//    [DisplayName("RTHand Book No")]
            [MaxLength(50, ErrorMessage = "RTHand Book No cannot be longer than 50 characters")]
    		public string  RTHandBookNo { get; set; }
    
    		    
    		/// <summary>
    		/// RTHand Book Date
    		/// </summary>        
    	//    [DisplayName("RTHand Book Date")]
    		public Nullable<System.DateTime>  RTHandBookDate { get; set; }
    
    		    
    		/// <summary>
    		/// RTHand Book Over Date
    		/// </summary>        
    	//    [DisplayName("RTHand Book Over Date")]
    		public Nullable<System.DateTime>  RTHandBookOverDate { get; set; }
    
    		    
    		/// <summary>
    		/// RTHand Book Sign By
    		/// </summary>        
    	//    [DisplayName("RTHand Book Sign By")]
            [MaxLength(50, ErrorMessage = "RTHand Book Sign By cannot be longer than 50 characters")]
    		public string  RTHandBookSignBy { get; set; }
    
    		    
    		/// <summary>
    		/// RTHand Book Courier
    		/// </summary>        
    	//    [DisplayName("RTHand Book Courier")]
            [MaxLength(50, ErrorMessage = "RTHand Book Courier cannot be longer than 50 characters")]
    		public string  RTHandBookCourier { get; set; }
    
    		    
    		/// <summary>
    		/// RTHexiao Dan No
    		/// </summary>        
    	//    [DisplayName("RTHexiao Dan No")]
            [MaxLength(50, ErrorMessage = "RTHexiao Dan No cannot be longer than 50 characters")]
    		public string  RTHexiaoDanNo { get; set; }
    
    		    
    		/// <summary>
    		/// RTHexiao Dan Date
    		/// </summary>        
    	//    [DisplayName("RTHexiao Dan Date")]
    		public Nullable<System.DateTime>  RTHexiaoDanDate { get; set; }
    
    		    
    		/// <summary>
    		/// RTHexiao Dan Over Date
    		/// </summary>        
    	//    [DisplayName("RTHexiao Dan Over Date")]
    		public Nullable<System.DateTime>  RTHexiaoDanOverDate { get; set; }
    
    		    
    		/// <summary>
    		/// RTHexiao Dan Sign By
    		/// </summary>        
    	//    [DisplayName("RTHexiao Dan Sign By")]
            [MaxLength(50, ErrorMessage = "RTHexiao Dan Sign By cannot be longer than 50 characters")]
    		public string  RTHexiaoDanSignBy { get; set; }
    
    		    
    		/// <summary>
    		/// RTHexiao Dan Courier
    		/// </summary>        
    	//    [DisplayName("RTHexiao Dan Courier")]
            [MaxLength(50, ErrorMessage = "RTHexiao Dan Courier cannot be longer than 50 characters")]
    		public string  RTHexiaoDanCourier { get; set; }
    
    		    
    		/// <summary>
    		/// RTTui Shui No
    		/// </summary>        
    	//    [DisplayName("RTTui Shui No")]
            [MaxLength(50, ErrorMessage = "RTTui Shui No cannot be longer than 50 characters")]
    		public string  RTTuiShuiNo { get; set; }
    
    		    
    		/// <summary>
    		/// RTTui Shui Date
    		/// </summary>        
    	//    [DisplayName("RTTui Shui Date")]
    		public Nullable<System.DateTime>  RTTuiShuiDate { get; set; }
    
    		    
    		/// <summary>
    		/// RTTui Shui Over Date
    		/// </summary>        
    	//    [DisplayName("RTTui Shui Over Date")]
    		public Nullable<System.DateTime>  RTTuiShuiOverDate { get; set; }
    
    		    
    		/// <summary>
    		/// RTTui Shui Sign By
    		/// </summary>        
    	//    [DisplayName("RTTui Shui Sign By")]
            [MaxLength(50, ErrorMessage = "RTTui Shui Sign By cannot be longer than 50 characters")]
    		public string  RTTuiShuiSignBy { get; set; }
    
    		    
    		/// <summary>
    		/// RTTui Shui Courier
    		/// </summary>        
    	//    [DisplayName("RTTui Shui Courier")]
            [MaxLength(50, ErrorMessage = "RTTui Shui Courier cannot be longer than 50 characters")]
    		public string  RTTuiShuiCourier { get; set; }
    
    		    
    		/// <summary>
    		/// RTRemark
    		/// </summary>        
    	//    [DisplayName("RTRemark")]
            [MaxLength(200, ErrorMessage = "RTRemark cannot be longer than 200 characters")]
    		public string  RTRemark { get; set; }
    
    		    
    		/// <summary>
    		/// RTCreated By
    		/// </summary>        
    	//    [DisplayName("RTCreated By")]
            [MaxLength(50, ErrorMessage = "RTCreated By cannot be longer than 50 characters")]
    		public string  RTCreatedBy { get; set; }
    
    		    
    		/// <summary>
    		/// RTCreated Date
    		/// </summary>        
    	//    [DisplayName("RTCreated Date")]
    		public Nullable<System.DateTime>  RTCreatedDate { get; set; }
    
    		    
    		/// <summary>
    		/// RTUpdated By
    		/// </summary>        
    	//    [DisplayName("RTUpdated By")]
            [MaxLength(50, ErrorMessage = "RTUpdated By cannot be longer than 50 characters")]
    		public string  RTUpdatedBy { get; set; }
    
    		    
    		/// <summary>
    		/// RTUpdated Date
    		/// </summary>        
    	//    [DisplayName("RTUpdated Date")]
    		public Nullable<System.DateTime>  RTUpdatedDate { get; set; }
    
    		    
    		/// <summary>
    		/// Remark
    		/// </summary>        
    	//    [DisplayName("Remark")]
            [MaxLength(200, ErrorMessage = "Remark cannot be longer than 200 characters")]
    		public string  Remark { get; set; }
    
    		    
    		/// <summary>
    		/// Status
    		/// </summary>        
    	//    [DisplayName("Status")]
            [MaxLength(5, ErrorMessage = "Status cannot be longer than 5 characters")]
    		public string  Status { get; set; }
    
    		    
    		/// <summary>
    		/// ETD
    		/// </summary>        
    	//    [DisplayName("ETD")]
    		public Nullable<System.DateTime>  ETD { get; set; }
    
    		    
    		/// <summary>
    		/// Sales Person
    		/// </summary>        
    	//    [DisplayName("Sales Person")]
            [MaxLength(50, ErrorMessage = "Sales Person cannot be longer than 50 characters")]
    		public string  SalesPerson { get; set; }
    
    		    
    		/// <summary>
    		/// WHCargo Date
    		/// </summary>        
    	//    [DisplayName("WHCargo Date")]
    		public Nullable<System.DateTime>  WHCargoDate { get; set; }
    
    		    
    		/// <summary>
    		/// WHService Level
    		/// </summary>        
    	//    [DisplayName("WHService Level")]
            [MaxLength(50, ErrorMessage = "WHService Level cannot be longer than 50 characters")]
    		public string  WHServiceLevel { get; set; }
    
    		    
    		/// <summary>
    		/// AWBType
    		/// </summary>        
    	//    [DisplayName("AWBType")]
            [MaxLength(5, ErrorMessage = "AWBType cannot be longer than 5 characters")]
    		public string  AWBType { get; set; }
    
    		    
    		/// <summary>
    		/// Book Send
    		/// </summary>        
    	//    [DisplayName("Book Send")]
            [MaxLength(10, ErrorMessage = "Book Send cannot be longer than 10 characters")]
    		public string  BookSend { get; set; }
    
    		    
    		/// <summary>
    		/// Booking WTUOM
    		/// </summary>        
    	//    [DisplayName("Booking WTUOM")]
            [MaxLength(10, ErrorMessage = "Booking WTUOM cannot be longer than 10 characters")]
    		public string  BookingWTUOM { get; set; }
    
    		    
    		/// <summary>
    		/// Booking Volume UOM
    		/// </summary>        
    	//    [DisplayName("Booking Volume UOM")]
            [MaxLength(10, ErrorMessage = "Booking Volume UOM cannot be longer than 10 characters")]
    		public string  BookingVolumeUOM { get; set; }
    
    		    
    		/// <summary>
    		/// Load Master Load Name
    		/// </summary>        
    	//    [DisplayName("Load Master Load Name")]
            [MaxLength(50, ErrorMessage = "Load Master Load Name cannot be longer than 50 characters")]
    		public string  LoadMasterLoadName { get; set; }
    
    		    
    		/// <summary>
    		/// Load Plan ID
    		/// </summary>        
    	//    [DisplayName("Load Plan ID")]
    		public Nullable<int>  LoadPlanID { get; set; }
    
    		    
    		/// <summary>
    		/// Agent No
    		/// </summary>        
    	//    [DisplayName("Agent No")]
            [MaxLength(50, ErrorMessage = "Agent No cannot be longer than 50 characters")]
    		public string  AgentNo { get; set; }
    
    		    
    		/// <summary>
    		/// RTStatus
    		/// </summary>        
    	//    [DisplayName("RTStatus")]
            [MaxLength(50, ErrorMessage = "RTStatus cannot be longer than 50 characters")]
    		public string  RTStatus { get; set; }
    
    		    
    		/// <summary>
    		/// Hub Remark
    		/// </summary>        
    	//    [DisplayName("Hub Remark")]
            [MaxLength(400, ErrorMessage = "Hub Remark cannot be longer than 400 characters")]
    		public string  HubRemark { get; set; }
    
    		    
    		/// <summary>
    		/// WHCWT
    		/// </summary>        
    	//    [DisplayName("WHCWT")]
    		public Nullable<decimal>  WHCWT { get; set; }
    
    		    
    	}
    //}
    
}
