using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ValuePlus.ViewModel;



namespace ValuePlus.ViewModel.LocaleChainVP
{
    
    /// <summary>
    /// OEFTPLading class
    /// </summary>
    //[MetadataType(typeof(OEFTPLadingViewModel))]
    //public  partial class OEFTPLading
    //{
    
    	/// <summary>
    	/// OEFTPLading Metadata class
    	/// </summary>
    	public   class OEFTPLadingViewModel:ViewModelBase
    	{
    		    
    		/// <summary>
    		/// ID
    		/// </summary>        
    	//    [DisplayName("ID")]
            [Required(ErrorMessage = "ID is required")]
    		public int  ID { get; set; }
    
    		    
    		/// <summary>
    		/// Station ID
    		/// </summary>        
    	//    [DisplayName("Station ID")]
            [MaxLength(5, ErrorMessage = "Station ID cannot be longer than 5 characters")]
    		public string  StationID { get; set; }
    
    		    
    		/// <summary>
    		/// HBLID
    		/// </summary>        
    	//    [DisplayName("HBLID")]
    		public Nullable<int>  HBLID { get; set; }
    
    		    
    		/// <summary>
    		/// HBLNo
    		/// </summary>        
    	//    [DisplayName("HBLNo")]
            [MaxLength(20, ErrorMessage = "HBLNo cannot be longer than 20 characters")]
    		public string  HBLNo { get; set; }
    
    		    
    		/// <summary>
    		/// SONumber
    		/// </summary>        
    	//    [DisplayName("SONumber")]
            [MaxLength(100, ErrorMessage = "SONumber cannot be longer than 100 characters")]
    		public string  SONumber { get; set; }
    
    		    
    		/// <summary>
    		/// FMCNumber
    		/// </summary>        
    	//    [DisplayName("FMCNumber")]
            [MaxLength(50, ErrorMessage = "FMCNumber cannot be longer than 50 characters")]
    		public string  FMCNumber { get; set; }
    
    		    
    		/// <summary>
    		/// SHPR
    		/// </summary>        
    	//    [DisplayName("SHPR")]
            [MaxLength(300, ErrorMessage = "SHPR cannot be longer than 300 characters")]
    		public string  SHPR { get; set; }
    
    		    
    		/// <summary>
    		/// CNEE
    		/// </summary>        
    	//    [DisplayName("CNEE")]
            [MaxLength(300, ErrorMessage = "CNEE cannot be longer than 300 characters")]
    		public string  CNEE { get; set; }
    
    		    
    		/// <summary>
    		/// DESTAgent
    		/// </summary>        
    	//    [DisplayName("DESTAgent")]
            [MaxLength(300, ErrorMessage = "DESTAgent cannot be longer than 300 characters")]
    		public string  DESTAgent { get; set; }
    
    		    
    		/// <summary>
    		/// NTFY
    		/// </summary>        
    	//    [DisplayName("NTFY")]
            [MaxLength(300, ErrorMessage = "NTFY cannot be longer than 300 characters")]
    		public string  NTFY { get; set; }
    
    		    
    		/// <summary>
    		/// Also NTFY
    		/// </summary>        
    	//    [DisplayName("Also NTFY")]
            [MaxLength(300, ErrorMessage = "Also NTFY cannot be longer than 300 characters")]
    		public string  AlsoNTFY { get; set; }
    
    		    
    		/// <summary>
    		/// Export Reference
    		/// </summary>        
    	//    [DisplayName("Export Reference")]
            [MaxLength(100, ErrorMessage = "Export Reference cannot be longer than 100 characters")]
    		public string  ExportReference { get; set; }
    
    		    
    		/// <summary>
    		/// Origin
    		/// </summary>        
    	//    [DisplayName("Origin")]
            [MaxLength(50, ErrorMessage = "Origin cannot be longer than 50 characters")]
    		public string  Origin { get; set; }
    
    		    
    		/// <summary>
    		/// Pre Carriage By
    		/// </summary>        
    	//    [DisplayName("Pre Carriage By")]
            [MaxLength(30, ErrorMessage = "Pre Carriage By cannot be longer than 30 characters")]
    		public string  PreCarriageBy { get; set; }
    
    		    
    		/// <summary>
    		/// PReceipt
    		/// </summary>        
    	//    [DisplayName("PReceipt")]
            [MaxLength(60, ErrorMessage = "PReceipt cannot be longer than 60 characters")]
    		public string  PReceipt { get; set; }
    
    		    
    		/// <summary>
    		/// Vessel
    		/// </summary>        
    	//    [DisplayName("Vessel")]
            [MaxLength(100, ErrorMessage = "Vessel cannot be longer than 100 characters")]
    		public string  Vessel { get; set; }
    
    		    
    		/// <summary>
    		/// Voyage
    		/// </summary>        
    	//    [DisplayName("Voyage")]
            [MaxLength(100, ErrorMessage = "Voyage cannot be longer than 100 characters")]
    		public string  Voyage { get; set; }
    
    		    
    		/// <summary>
    		/// PLoading
    		/// </summary>        
    	//    [DisplayName("PLoading")]
            [MaxLength(40, ErrorMessage = "PLoading cannot be longer than 40 characters")]
    		public string  PLoading { get; set; }
    
    		    
    		/// <summary>
    		/// PDischarge
    		/// </summary>        
    	//    [DisplayName("PDischarge")]
            [MaxLength(40, ErrorMessage = "PDischarge cannot be longer than 40 characters")]
    		public string  PDischarge { get; set; }
    
    		    
    		/// <summary>
    		/// Movement
    		/// </summary>        
    	//    [DisplayName("Movement")]
            [MaxLength(10, ErrorMessage = "Movement cannot be longer than 10 characters")]
    		public string  Movement { get; set; }
    
    		    
    		/// <summary>
    		/// PDelivery
    		/// </summary>        
    	//    [DisplayName("PDelivery")]
            [MaxLength(40, ErrorMessage = "PDelivery cannot be longer than 40 characters")]
    		public string  PDelivery { get; set; }
    
    		    
    		/// <summary>
    		/// CTNRIn Words
    		/// </summary>        
    	//    [DisplayName("CTNRIn Words")]
            [MaxLength(120, ErrorMessage = "CTNRIn Words cannot be longer than 120 characters")]
    		public string  CTNRInWords { get; set; }
    
    		    
    		/// <summary>
    		/// Pay At
    		/// </summary>        
    	//    [DisplayName("Pay At")]
            [MaxLength(60, ErrorMessage = "Pay At cannot be longer than 60 characters")]
    		public string  PayAt { get; set; }
    
    		    
    		/// <summary>
    		/// Date On Board
    		/// </summary>        
    	//    [DisplayName("Date On Board")]
            [MaxLength(60, ErrorMessage = "Date On Board cannot be longer than 60 characters")]
    		public string  DateOnBoard { get; set; }
    
    		    
    		/// <summary>
    		/// No Of Org BL
    		/// </summary>        
    	//    [DisplayName("No Of Org BL")]
            [MaxLength(10, ErrorMessage = "No Of Org BL cannot be longer than 10 characters")]
    		public string  NoOfOrgBL { get; set; }
    
    		    
    		/// <summary>
    		/// Issue Place Date
    		/// </summary>        
    	//    [DisplayName("Issue Place Date")]
            [MaxLength(60, ErrorMessage = "Issue Place Date cannot be longer than 60 characters")]
    		public string  IssuePlaceDate { get; set; }
    
    		    
    		/// <summary>
    		/// Declare Value
    		/// </summary>        
    	//    [DisplayName("Declare Value")]
            [MaxLength(15, ErrorMessage = "Declare Value cannot be longer than 15 characters")]
    		public string  DeclareValue { get; set; }
    
    		    
    		/// <summary>
    		/// CTNRNo
    		/// </summary>        
    	//    [DisplayName("CTNRNo")]
    		public string  CTNRNo { get; set; }
    
    		    
    		/// <summary>
    		/// PCS
    		/// </summary>        
    	//    [DisplayName("PCS")]
            [MaxLength(500, ErrorMessage = "PCS cannot be longer than 500 characters")]
    		public string  PCS { get; set; }
    
    		    
    		/// <summary>
    		/// Goods Desc
    		/// </summary>        
    	//    [DisplayName("Goods Desc")]
    		public string  GoodsDesc { get; set; }
    
    		    
    		/// <summary>
    		/// GWT
    		/// </summary>        
    	//    [DisplayName("GWT")]
            [MaxLength(500, ErrorMessage = "GWT cannot be longer than 500 characters")]
    		public string  GWT { get; set; }
    
    		    
    		/// <summary>
    		/// Measurement
    		/// </summary>        
    	//    [DisplayName("Measurement")]
            [MaxLength(500, ErrorMessage = "Measurement cannot be longer than 500 characters")]
    		public string  Measurement { get; set; }
    
    		    
    		/// <summary>
    		/// Created DT
    		/// </summary>        
    	//    [DisplayName("Created DT")]
    		public Nullable<System.DateTime>  CreatedDT { get; set; }
    
    		    
    		/// <summary>
    		/// Created By
    		/// </summary>        
    	//    [DisplayName("Created By")]
            [MaxLength(50, ErrorMessage = "Created By cannot be longer than 50 characters")]
    		public string  CreatedBy { get; set; }
    
    		    
    		/// <summary>
    		/// Updated DT
    		/// </summary>        
    	//    [DisplayName("Updated DT")]
    		public Nullable<System.DateTime>  UpdatedDT { get; set; }
    
    		    
    		/// <summary>
    		/// Updated By
    		/// </summary>        
    	//    [DisplayName("Updated By")]
            [MaxLength(50, ErrorMessage = "Updated By cannot be longer than 50 characters")]
    		public string  UpdatedBy { get; set; }
    
    		    
    		/// <summary>
    		/// OEType
    		/// </summary>        
    	//    [DisplayName("OEType")]
            [MaxLength(5, ErrorMessage = "OEType cannot be longer than 5 characters")]
    		public string  OEType { get; set; }
    
    		    
    		/// <summary>
    		/// Station Info
    		/// </summary>        
    	//    [DisplayName("Station Info")]
            [MaxLength(150, ErrorMessage = "Station Info cannot be longer than 150 characters")]
    		public string  StationInfo { get; set; }
    
    		    
    		/// <summary>
    		/// Tariff Charge
    		/// </summary>        
    	//    [DisplayName("Tariff Charge")]
            [MaxLength(500, ErrorMessage = "Tariff Charge cannot be longer than 500 characters")]
    		public string  TariffCharge { get; set; }
    
    		    
    		/// <summary>
    		/// Tariff Collect
    		/// </summary>        
    	//    [DisplayName("Tariff Collect")]
            [MaxLength(500, ErrorMessage = "Tariff Collect cannot be longer than 500 characters")]
    		public string  TariffCollect { get; set; }
    
    		    
    		/// <summary>
    		/// Tariff Collect Total
    		/// </summary>        
    	//    [DisplayName("Tariff Collect Total")]
            [MaxLength(500, ErrorMessage = "Tariff Collect Total cannot be longer than 500 characters")]
    		public string  TariffCollectTotal { get; set; }
    
    		    
    		/// <summary>
    		/// Tariff Item No
    		/// </summary>        
    	//    [DisplayName("Tariff Item No")]
            [MaxLength(500, ErrorMessage = "Tariff Item No cannot be longer than 500 characters")]
    		public string  TariffItemNo { get; set; }
    
    		    
    		/// <summary>
    		/// Tariff Prepaid
    		/// </summary>        
    	//    [DisplayName("Tariff Prepaid")]
            [MaxLength(500, ErrorMessage = "Tariff Prepaid cannot be longer than 500 characters")]
    		public string  TariffPrepaid { get; set; }
    
    		    
    		/// <summary>
    		/// Tariff Prepaid Total
    		/// </summary>        
    	//    [DisplayName("Tariff Prepaid Total")]
            [MaxLength(500, ErrorMessage = "Tariff Prepaid Total cannot be longer than 500 characters")]
    		public string  TariffPrepaidTotal { get; set; }
    
    		    
    		/// <summary>
    		/// Marks
    		/// </summary>        
    	//    [DisplayName("Marks")]
    		public string  Marks { get; set; }
    
    		    
    		/// <summary>
    		/// Sum PCS
    		/// </summary>        
    	//    [DisplayName("Sum PCS")]
            [MaxLength(50, ErrorMessage = "Sum PCS cannot be longer than 50 characters")]
    		public string  SumPCS { get; set; }
    
    		    
    		/// <summary>
    		/// Sum GWT
    		/// </summary>        
    	//    [DisplayName("Sum GWT")]
            [MaxLength(50, ErrorMessage = "Sum GWT cannot be longer than 50 characters")]
    		public string  SumGWT { get; set; }
    
    		    
    		/// <summary>
    		/// Sum CBM
    		/// </summary>        
    	//    [DisplayName("Sum CBM")]
            [MaxLength(50, ErrorMessage = "Sum CBM cannot be longer than 50 characters")]
    		public string  SumCBM { get; set; }
    
    		    
    		/// <summary>
    		/// HBLRemarks
    		/// </summary>        
    	//    [DisplayName("HBLRemarks")]
            [MaxLength(500, ErrorMessage = "HBLRemarks cannot be longer than 500 characters")]
    		public string  HBLRemarks { get; set; }
    
    		    
    	}
    //}
    
}
