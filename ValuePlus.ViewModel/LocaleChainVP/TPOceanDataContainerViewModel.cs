using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ValuePlus.ViewModel;



namespace ValuePlus.ViewModel.LocaleChainVP
{
    
    /// <summary>
    /// TPOceanDataContainer class
    /// </summary>
    //[MetadataType(typeof(TPOceanDataContainerViewModel))]
    //public  partial class TPOceanDataContainer
    //{
    
    	/// <summary>
    	/// TPOceanDataContainer Metadata class
    	/// </summary>
    	public   class TPOceanDataContainerViewModel:ViewModelBase
    	{
    		    
    		/// <summary>
    		/// ID
    		/// </summary>        
    	//    [DisplayName("ID")]
            [Required(ErrorMessage = "ID is required")]
    		public int  ID { get; set; }
    
    		    
    		/// <summary>
    		/// Station ID
    		/// </summary>        
    	//    [DisplayName("Station ID")]
            [Required(ErrorMessage = "Station ID is required")]
            [MaxLength(3, ErrorMessage = "Station ID cannot be longer than 3 characters")]
    		public string  StationID { get; set; }
    
    		    
    		/// <summary>
    		/// TPID
    		/// </summary>        
    	//    [DisplayName("TPID")]
            [Required(ErrorMessage = "TPID is required")]
            [MaxLength(15, ErrorMessage = "TPID cannot be longer than 15 characters")]
    		public string  TPID { get; set; }
    
    		    
    		/// <summary>
    		/// Container
    		/// </summary>        
    	//    [DisplayName("Container")]
            [Required(ErrorMessage = "Container is required")]
            [MaxLength(255, ErrorMessage = "Container cannot be longer than 255 characters")]
    		public string  Container { get; set; }
    
    		    
    		/// <summary>
    		/// Container Type
    		/// </summary>        
    	//    [DisplayName("Container Type")]
            [Required(ErrorMessage = "Container Type is required")]
            [MaxLength(10, ErrorMessage = "Container Type cannot be longer than 10 characters")]
    		public string  ContainerType { get; set; }
    
    		    
    		/// <summary>
    		/// CBM
    		/// </summary>        
    	//    [DisplayName("CBM")]
    		public Nullable<double>  CBM { get; set; }
    
    		    
    		/// <summary>
    		/// PCS
    		/// </summary>        
    	//    [DisplayName("PCS")]
    		public Nullable<int>  PCS { get; set; }
    
    		    
    		/// <summary>
    		/// PCSUOM
    		/// </summary>        
    	//    [DisplayName("PCSUOM")]
            [MaxLength(10, ErrorMessage = "PCSUOM cannot be longer than 10 characters")]
    		public string  PCSUOM { get; set; }
    
    		    
    		/// <summary>
    		/// WT
    		/// </summary>        
    	//    [DisplayName("WT")]
    		public Nullable<double>  WT { get; set; }
    
    		    
    		/// <summary>
    		/// WTUOM
    		/// </summary>        
    	//    [DisplayName("WTUOM")]
            [MaxLength(10, ErrorMessage = "WTUOM cannot be longer than 10 characters")]
    		public string  WTUOM { get; set; }
    
    		    
    		/// <summary>
    		/// Created By
    		/// </summary>        
    	//    [DisplayName("Created By")]
            [Required(ErrorMessage = "Created By is required")]
            [MaxLength(6, ErrorMessage = "Created By cannot be longer than 6 characters")]
    		public string  CreatedBy { get; set; }
    
    		    
    		/// <summary>
    		/// Created Date
    		/// </summary>        
    	//    [DisplayName("Created Date")]
            [Required(ErrorMessage = "Created Date is required")]
    		public System.DateTime  CreatedDate { get; set; }
    
    		    
    		/// <summary>
    		/// Updated By
    		/// </summary>        
    	//    [DisplayName("Updated By")]
            [Required(ErrorMessage = "Updated By is required")]
            [MaxLength(6, ErrorMessage = "Updated By cannot be longer than 6 characters")]
    		public string  UpdatedBy { get; set; }
    
    		    
    		/// <summary>
    		/// Updated Date
    		/// </summary>        
    	//    [DisplayName("Updated Date")]
            [Required(ErrorMessage = "Updated Date is required")]
    		public System.DateTime  UpdatedDate { get; set; }
    
    		    
    		/// <summary>
    		/// Version
    		/// </summary>        
    	//    [DisplayName("Version")]
            [MaxLength(8, ErrorMessage = "Version cannot be longer than 8 characters")]
    		public byte[]  Version { get; set; }
    
    		    
    	}
    //}
    
}
