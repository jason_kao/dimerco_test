using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ValuePlus.ViewModel;



namespace ValuePlus.ViewModel.LocaleChainVP
{
    
    /// <summary>
    /// AEMAWB class
    /// </summary>
    //[MetadataType(typeof(AEMAWBViewModel))]
    //public  partial class AEMAWB
    //{
    
    	/// <summary>
    	/// AEMAWB Metadata class
    	/// </summary>
    	public   class AEMAWBViewModel:ViewModelBase
    	{
    		    
    		/// <summary>
    		/// ID
    		/// </summary>        
    	//    [DisplayName("ID")]
            [Required(ErrorMessage = "ID is required")]
    		public int  ID { get; set; }
    
    		    
    		/// <summary>
    		/// MAWBNo
    		/// </summary>        
    	//    [DisplayName("MAWBNo")]
            [Required(ErrorMessage = "MAWBNo is required")]
            [MaxLength(20, ErrorMessage = "MAWBNo cannot be longer than 20 characters")]
    		public string  MAWBNo { get; set; }
    
    		    
    		/// <summary>
    		/// Other
    		/// </summary>        
    	//    [DisplayName("Other")]
            [MaxLength(5, ErrorMessage = "Other cannot be longer than 5 characters")]
    		public string  Other { get; set; }
    
    		    
    		/// <summary>
    		/// FRT
    		/// </summary>        
    	//    [DisplayName("FRT")]
            [MaxLength(5, ErrorMessage = "FRT cannot be longer than 5 characters")]
    		public string  FRT { get; set; }
    
    		    
    		/// <summary>
    		/// MAWBType
    		/// </summary>        
    	//    [DisplayName("MAWBType")]
    		public Nullable<int>  MAWBType { get; set; }
    
    		    
    		/// <summary>
    		/// Issue City
    		/// </summary>        
    	//    [DisplayName("Issue City")]
    		public Nullable<int>  IssueCity { get; set; }
    
    		    
    		/// <summary>
    		/// Issue Date
    		/// </summary>        
    	//    [DisplayName("Issue Date")]
    		public Nullable<System.DateTime>  IssueDate { get; set; }
    
    		    
    		/// <summary>
    		/// Lot No
    		/// </summary>        
    	//    [DisplayName("Lot No")]
            [MaxLength(13, ErrorMessage = "Lot No cannot be longer than 13 characters")]
    		public string  LotNo { get; set; }
    
    		    
    		/// <summary>
    		/// GWT
    		/// </summary>        
    	//    [DisplayName("GWT")]
    		public Nullable<double>  GWT { get; set; }
    
    		    
    		/// <summary>
    		/// CWT
    		/// </summary>        
    	//    [DisplayName("CWT")]
    		public Nullable<double>  CWT { get; set; }
    
    		    
    		/// <summary>
    		/// Currency
    		/// </summary>        
    	//    [DisplayName("Currency")]
            [MaxLength(5, ErrorMessage = "Currency cannot be longer than 5 characters")]
    		public string  Currency { get; set; }
    
    		    
    		/// <summary>
    		/// Show
    		/// </summary>        
    	//    [DisplayName("Show")]
    		public Nullable<double>  Show { get; set; }
    
    		    
    		/// <summary>
    		/// CTCT
    		/// </summary>        
    	//    [DisplayName("CTCT")]
    		public Nullable<double>  CTCT { get; set; }
    
    		    
    		/// <summary>
    		/// Comm
    		/// </summary>        
    	//    [DisplayName("Comm")]
    		public Nullable<double>  Comm { get; set; }
    
    		    
    		/// <summary>
    		/// Ex Comm
    		/// </summary>        
    	//    [DisplayName("Ex Comm")]
    		public Nullable<double>  ExComm { get; set; }
    
    		    
    		/// <summary>
    		/// Issuer
    		/// </summary>        
    	//    [DisplayName("Issuer")]
    		public Nullable<int>  Issuer { get; set; }
    
    		    
    		/// <summary>
    		/// Shipper
    		/// </summary>        
    	//    [DisplayName("Shipper")]
    		public Nullable<int>  Shipper { get; set; }
    
    		    
    		/// <summary>
    		/// CNEE
    		/// </summary>        
    	//    [DisplayName("CNEE")]
    		public Nullable<int>  CNEE { get; set; }
    
    		    
    		/// <summary>
    		/// Notify
    		/// </summary>        
    	//    [DisplayName("Notify")]
    		public Nullable<int>  Notify { get; set; }
    
    		    
    		/// <summary>
    		/// WTUOM
    		/// </summary>        
    	//    [DisplayName("WTUOM")]
            [MaxLength(5, ErrorMessage = "WTUOM cannot be longer than 5 characters")]
    		public string  WTUOM { get; set; }
    
    		    
    		/// <summary>
    		/// PCSUOM
    		/// </summary>        
    	//    [DisplayName("PCSUOM")]
            [MaxLength(5, ErrorMessage = "PCSUOM cannot be longer than 5 characters")]
    		public string  PCSUOM { get; set; }
    
    		    
    		/// <summary>
    		/// PCS
    		/// </summary>        
    	//    [DisplayName("PCS")]
    		public Nullable<int>  PCS { get; set; }
    
    		    
    		/// <summary>
    		/// Class Rate
    		/// </summary>        
    	//    [DisplayName("Class Rate")]
            [MaxLength(5, ErrorMessage = "Class Rate cannot be longer than 5 characters")]
    		public string  ClassRate { get; set; }
    
    		    
    		/// <summary>
    		/// SPINST
    		/// </summary>        
    	//    [DisplayName("SPINST")]
            [MaxLength(50, ErrorMessage = "SPINST cannot be longer than 50 characters")]
    		public string  SPINST { get; set; }
    
    		    
    		/// <summary>
    		/// Status
    		/// </summary>        
    	//    [DisplayName("Status")]
            [MaxLength(10, ErrorMessage = "Status cannot be longer than 10 characters")]
    		public string  Status { get; set; }
    
    		    
    		/// <summary>
    		/// Packaging Description
    		/// </summary>        
    	//    [DisplayName("Packaging Description")]
            [MaxLength(255, ErrorMessage = "Packaging Description cannot be longer than 255 characters")]
    		public string  PackagingDescription { get; set; }
    
    		    
    		/// <summary>
    		/// Port Of DEPT
    		/// </summary>        
    	//    [DisplayName("Port Of DEPT")]
    		public Nullable<int>  PortOfDEPT { get; set; }
    
    		    
    		/// <summary>
    		/// Port Of DSTN
    		/// </summary>        
    	//    [DisplayName("Port Of DSTN")]
    		public Nullable<int>  PortOfDSTN { get; set; }
    
    		    
    		/// <summary>
    		/// Print Person
    		/// </summary>        
    	//    [DisplayName("Print Person")]
    		public Nullable<int>  PrintPerson { get; set; }
    
    		    
    		/// <summary>
    		/// Due To Agent
    		/// </summary>        
    	//    [DisplayName("Due To Agent")]
    		public Nullable<double>  DueToAgent { get; set; }
    
    		    
    		/// <summary>
    		/// Due To Carrier
    		/// </summary>        
    	//    [DisplayName("Due To Carrier")]
    		public Nullable<double>  DueToCarrier { get; set; }
    
    		    
    		/// <summary>
    		/// Pre Alert By
    		/// </summary>        
    	//    [DisplayName("Pre Alert By")]
            [MaxLength(6, ErrorMessage = "Pre Alert By cannot be longer than 6 characters")]
    		public string  PreAlertBy { get; set; }
    
    		    
    		/// <summary>
    		/// Last Pre Alert Time
    		/// </summary>        
    	//    [DisplayName("Last Pre Alert Time")]
    		public Nullable<System.DateTime>  LastPreAlertTime { get; set; }
    
    		    
    		/// <summary>
    		/// Laste Manifest Time
    		/// </summary>        
    	//    [DisplayName("Laste Manifest Time")]
    		public Nullable<System.DateTime>  LasteManifestTime { get; set; }
    
    		    
    		/// <summary>
    		/// e Manifest By
    		/// </summary>        
    	//    [DisplayName("e Manifest By")]
            [MaxLength(6, ErrorMessage = "e Manifest By cannot be longer than 6 characters")]
    		public string  eManifestBy { get; set; }
    
    		    
    		/// <summary>
    		/// Issuer Type
    		/// </summary>        
    	//    [DisplayName("Issuer Type")]
            [MaxLength(10, ErrorMessage = "Issuer Type cannot be longer than 10 characters")]
    		public string  IssuerType { get; set; }
    
    		    
    		/// <summary>
    		/// is AMS
    		/// </summary>        
    	//    [DisplayName("is AMS")]
            [MaxLength(50, ErrorMessage = "is AMS cannot be longer than 50 characters")]
    		public string  isAMS { get; set; }
    
    		    
    		/// <summary>
    		/// Station ID
    		/// </summary>        
    	//    [DisplayName("Station ID")]
            [Required(ErrorMessage = "Station ID is required")]
            [MaxLength(10, ErrorMessage = "Station ID cannot be longer than 10 characters")]
    		public string  StationID { get; set; }
    
    		    
    		/// <summary>
    		/// Created By
    		/// </summary>        
    	//    [DisplayName("Created By")]
            [Required(ErrorMessage = "Created By is required")]
            [MaxLength(6, ErrorMessage = "Created By cannot be longer than 6 characters")]
    		public string  CreatedBy { get; set; }
    
    		    
    		/// <summary>
    		/// Created Date
    		/// </summary>        
    	//    [DisplayName("Created Date")]
            [Required(ErrorMessage = "Created Date is required")]
    		public System.DateTime  CreatedDate { get; set; }
    
    		    
    		/// <summary>
    		/// Updated By
    		/// </summary>        
    	//    [DisplayName("Updated By")]
            [Required(ErrorMessage = "Updated By is required")]
            [MaxLength(6, ErrorMessage = "Updated By cannot be longer than 6 characters")]
    		public string  UpdatedBy { get; set; }
    
    		    
    		/// <summary>
    		/// Updated Date
    		/// </summary>        
    	//    [DisplayName("Updated Date")]
            [Required(ErrorMessage = "Updated Date is required")]
    		public System.DateTime  UpdatedDate { get; set; }
    
    		    
    		/// <summary>
    		/// CYMNo
    		/// </summary>        
    	//    [DisplayName("CYMNo")]
            [MaxLength(50, ErrorMessage = "CYMNo cannot be longer than 50 characters")]
    		public string  CYMNo { get; set; }
    
    		    
    		/// <summary>
    		/// VWT
    		/// </summary>        
    	//    [DisplayName("VWT")]
    		public Nullable<double>  VWT { get; set; }
    
    		    
    		/// <summary>
    		/// SPL
    		/// </summary>        
    	//    [DisplayName("SPL")]
    		public Nullable<int>  SPL { get; set; }
    
    		    
    		/// <summary>
    		/// DBID
    		/// </summary>        
    	//    [DisplayName("DBID")]
            [MaxLength(3, ErrorMessage = "DBID cannot be longer than 3 characters")]
    		public string  DBID { get; set; }
    
    		    
    		/// <summary>
    		/// AMSBy
    		/// </summary>        
    	//    [DisplayName("AMSBy")]
            [MaxLength(10, ErrorMessage = "AMSBy cannot be longer than 10 characters")]
    		public string  AMSBy { get; set; }
    
    		    
    		/// <summary>
    		/// AMSDate
    		/// </summary>        
    	//    [DisplayName("AMSDate")]
    		public Nullable<System.DateTime>  AMSDate { get; set; }
    
    		    
    		/// <summary>
    		/// Re Weight
    		/// </summary>        
    	//    [DisplayName("Re Weight")]
            [Required(ErrorMessage = "Re Weight is required")]
            [MaxLength(1, ErrorMessage = "Re Weight cannot be longer than 1 characters")]
    		public string  ReWeight { get; set; }
    
    		    
    		/// <summary>
    		/// Weight By
    		/// </summary>        
    	//    [DisplayName("Weight By")]
            [MaxLength(10, ErrorMessage = "Weight By cannot be longer than 10 characters")]
    		public string  WeightBy { get; set; }
    
    		    
    		/// <summary>
    		/// Weight Date
    		/// </summary>        
    	//    [DisplayName("Weight Date")]
    		public Nullable<System.DateTime>  WeightDate { get; set; }
    
    		    
    		/// <summary>
    		/// Version
    		/// </summary>        
    	//    [DisplayName("Version")]
            [MaxLength(8, ErrorMessage = "Version cannot be longer than 8 characters")]
    		public byte[]  Version { get; set; }
    
    		    
    	}
    //}
    
}
