using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ValuePlus.ViewModel;



namespace ValuePlus.ViewModel.LocaleChainVP
{
    
    /// <summary>
    /// AEBookConsoleLoad class
    /// </summary>
    //[MetadataType(typeof(AEBookConsoleLoadViewModel))]
    //public  partial class AEBookConsoleLoad
    //{
    
    	/// <summary>
    	/// AEBookConsoleLoad Metadata class
    	/// </summary>
    	public   class AEBookConsoleLoadViewModel:ViewModelBase
    	{
    		    
    		/// <summary>
    		/// ID
    		/// </summary>        
    	//    [DisplayName("ID")]
            [Required(ErrorMessage = "ID is required")]
    		public int  ID { get; set; }
    
    		    
    		/// <summary>
    		/// Load Master Loader
    		/// </summary>        
    	//    [DisplayName("Load Master Loader")]
            [MaxLength(50, ErrorMessage = "Load Master Loader cannot be longer than 50 characters")]
    		public string  LoadMasterLoader { get; set; }
    
    		    
    		/// <summary>
    		/// Load MAWBNo
    		/// </summary>        
    	//    [DisplayName("Load MAWBNo")]
            [MaxLength(50, ErrorMessage = "Load MAWBNo cannot be longer than 50 characters")]
    		public string  LoadMAWBNo { get; set; }
    
    		    
    		/// <summary>
    		/// MAWBNo Extra
    		/// </summary>        
    	//    [DisplayName("MAWBNo Extra")]
            [MaxLength(50, ErrorMessage = "MAWBNo Extra cannot be longer than 50 characters")]
    		public string  MAWBNoExtra { get; set; }
    
    		    
    		/// <summary>
    		/// Load Carrier
    		/// </summary>        
    	//    [DisplayName("Load Carrier")]
            [MaxLength(50, ErrorMessage = "Load Carrier cannot be longer than 50 characters")]
    		public string  LoadCarrier { get; set; }
    
    		    
    		/// <summary>
    		/// Load DSTN
    		/// </summary>        
    	//    [DisplayName("Load DSTN")]
    		public Nullable<int>  LoadDSTN { get; set; }
    
    		    
    		/// <summary>
    		/// Load Area
    		/// </summary>        
    	//    [DisplayName("Load Area")]
            [MaxLength(10, ErrorMessage = "Load Area cannot be longer than 10 characters")]
    		public string  LoadArea { get; set; }
    
    		    
    		/// <summary>
    		/// Load Date
    		/// </summary>        
    	//    [DisplayName("Load Date")]
    		public Nullable<System.DateTime>  LoadDate { get; set; }
    
    		    
    		/// <summary>
    		/// Load FLTNo
    		/// </summary>        
    	//    [DisplayName("Load FLTNo")]
            [MaxLength(50, ErrorMessage = "Load FLTNo cannot be longer than 50 characters")]
    		public string  LoadFLTNo { get; set; }
    
    		    
    		/// <summary>
    		/// Load ETD
    		/// </summary>        
    	//    [DisplayName("Load ETD")]
    		public Nullable<System.DateTime>  LoadETD { get; set; }
    
    		    
    		/// <summary>
    		/// Load ETA
    		/// </summary>        
    	//    [DisplayName("Load ETA")]
    		public Nullable<System.DateTime>  LoadETA { get; set; }
    
    		    
    		/// <summary>
    		/// Load ATD
    		/// </summary>        
    	//    [DisplayName("Load ATD")]
    		public Nullable<System.DateTime>  LoadATD { get; set; }
    
    		    
    		/// <summary>
    		/// Load ATA
    		/// </summary>        
    	//    [DisplayName("Load ATA")]
    		public Nullable<System.DateTime>  LoadATA { get; set; }
    
    		    
    		/// <summary>
    		/// Load PCS
    		/// </summary>        
    	//    [DisplayName("Load PCS")]
    		public Nullable<int>  LoadPCS { get; set; }
    
    		    
    		/// <summary>
    		/// Load PCSType
    		/// </summary>        
    	//    [DisplayName("Load PCSType")]
            [MaxLength(50, ErrorMessage = "Load PCSType cannot be longer than 50 characters")]
    		public string  LoadPCSType { get; set; }
    
    		    
    		/// <summary>
    		/// Load Weight
    		/// </summary>        
    	//    [DisplayName("Load Weight")]
    		public Nullable<decimal>  LoadWeight { get; set; }
    
    		    
    		/// <summary>
    		/// Load Weight Type
    		/// </summary>        
    	//    [DisplayName("Load Weight Type")]
            [MaxLength(50, ErrorMessage = "Load Weight Type cannot be longer than 50 characters")]
    		public string  LoadWeightType { get; set; }
    
    		    
    		/// <summary>
    		/// Load Volume
    		/// </summary>        
    	//    [DisplayName("Load Volume")]
    		public Nullable<decimal>  LoadVolume { get; set; }
    
    		    
    		/// <summary>
    		/// Load Volume Type
    		/// </summary>        
    	//    [DisplayName("Load Volume Type")]
            [MaxLength(50, ErrorMessage = "Load Volume Type cannot be longer than 50 characters")]
    		public string  LoadVolumeType { get; set; }
    
    		    
    		/// <summary>
    		/// Load VWT
    		/// </summary>        
    	//    [DisplayName("Load VWT")]
    		public Nullable<decimal>  LoadVWT { get; set; }
    
    		    
    		/// <summary>
    		/// Load Terminal
    		/// </summary>        
    	//    [DisplayName("Load Terminal")]
            [MaxLength(10, ErrorMessage = "Load Terminal cannot be longer than 10 characters")]
    		public string  LoadTerminal { get; set; }
    
    		    
    		/// <summary>
    		/// Load Pallet
    		/// </summary>        
    	//    [DisplayName("Load Pallet")]
            [MaxLength(50, ErrorMessage = "Load Pallet cannot be longer than 50 characters")]
    		public string  LoadPallet { get; set; }
    
    		    
    		/// <summary>
    		/// Load Status
    		/// </summary>        
    	//    [DisplayName("Load Status")]
            [MaxLength(10, ErrorMessage = "Load Status cannot be longer than 10 characters")]
    		public string  LoadStatus { get; set; }
    
    		    
    		/// <summary>
    		/// Load Est Cost
    		/// </summary>        
    	//    [DisplayName("Load Est Cost")]
            [MaxLength(50, ErrorMessage = "Load Est Cost cannot be longer than 50 characters")]
    		public string  LoadEstCost { get; set; }
    
    		    
    		/// <summary>
    		/// Load Remark
    		/// </summary>        
    	//    [DisplayName("Load Remark")]
            [MaxLength(200, ErrorMessage = "Load Remark cannot be longer than 200 characters")]
    		public string  LoadRemark { get; set; }
    
    		    
    		/// <summary>
    		/// Created By
    		/// </summary>        
    	//    [DisplayName("Created By")]
            [MaxLength(50, ErrorMessage = "Created By cannot be longer than 50 characters")]
    		public string  CreatedBy { get; set; }
    
    		    
    		/// <summary>
    		/// Created Date
    		/// </summary>        
    	//    [DisplayName("Created Date")]
    		public Nullable<System.DateTime>  CreatedDate { get; set; }
    
    		    
    		/// <summary>
    		/// Updated By
    		/// </summary>        
    	//    [DisplayName("Updated By")]
            [MaxLength(50, ErrorMessage = "Updated By cannot be longer than 50 characters")]
    		public string  UpdatedBy { get; set; }
    
    		    
    		/// <summary>
    		/// Updated Date
    		/// </summary>        
    	//    [DisplayName("Updated Date")]
    		public Nullable<System.DateTime>  UpdatedDate { get; set; }
    
    		    
    		/// <summary>
    		/// Remark
    		/// </summary>        
    	//    [DisplayName("Remark")]
            [MaxLength(200, ErrorMessage = "Remark cannot be longer than 200 characters")]
    		public string  Remark { get; set; }
    
    		    
    		/// <summary>
    		/// Load Master Load Name
    		/// </summary>        
    	//    [DisplayName("Load Master Load Name")]
            [MaxLength(50, ErrorMessage = "Load Master Load Name cannot be longer than 50 characters")]
    		public string  LoadMasterLoadName { get; set; }
    
    		    
    		/// <summary>
    		/// DEPT1
    		/// </summary>        
    	//    [DisplayName("DEPT1")]
    		public Nullable<int>  DEPT1 { get; set; }
    
    		    
    		/// <summary>
    		/// DSTN1
    		/// </summary>        
    	//    [DisplayName("DSTN1")]
    		public Nullable<int>  DSTN1 { get; set; }
    
    		    
    		/// <summary>
    		/// FLTNo1
    		/// </summary>        
    	//    [DisplayName("FLTNo1")]
            [MaxLength(50, ErrorMessage = "FLTNo1 cannot be longer than 50 characters")]
    		public string  FLTNo1 { get; set; }
    
    		    
    		/// <summary>
    		/// ETD1
    		/// </summary>        
    	//    [DisplayName("ETD1")]
    		public Nullable<System.DateTime>  ETD1 { get; set; }
    
    		    
    		/// <summary>
    		/// ETA1
    		/// </summary>        
    	//    [DisplayName("ETA1")]
    		public Nullable<System.DateTime>  ETA1 { get; set; }
    
    		    
    		/// <summary>
    		/// ATD1
    		/// </summary>        
    	//    [DisplayName("ATD1")]
    		public Nullable<System.DateTime>  ATD1 { get; set; }
    
    		    
    		/// <summary>
    		/// ATA1
    		/// </summary>        
    	//    [DisplayName("ATA1")]
    		public Nullable<System.DateTime>  ATA1 { get; set; }
    
    		    
    		/// <summary>
    		/// DEPT2
    		/// </summary>        
    	//    [DisplayName("DEPT2")]
    		public Nullable<int>  DEPT2 { get; set; }
    
    		    
    		/// <summary>
    		/// DSTN2
    		/// </summary>        
    	//    [DisplayName("DSTN2")]
    		public Nullable<int>  DSTN2 { get; set; }
    
    		    
    		/// <summary>
    		/// FLTNo2
    		/// </summary>        
    	//    [DisplayName("FLTNo2")]
            [MaxLength(50, ErrorMessage = "FLTNo2 cannot be longer than 50 characters")]
    		public string  FLTNo2 { get; set; }
    
    		    
    		/// <summary>
    		/// ETD2
    		/// </summary>        
    	//    [DisplayName("ETD2")]
    		public Nullable<System.DateTime>  ETD2 { get; set; }
    
    		    
    		/// <summary>
    		/// ETA2
    		/// </summary>        
    	//    [DisplayName("ETA2")]
    		public Nullable<System.DateTime>  ETA2 { get; set; }
    
    		    
    		/// <summary>
    		/// ATD2
    		/// </summary>        
    	//    [DisplayName("ATD2")]
    		public Nullable<System.DateTime>  ATD2 { get; set; }
    
    		    
    		/// <summary>
    		/// ATA2
    		/// </summary>        
    	//    [DisplayName("ATA2")]
    		public Nullable<System.DateTime>  ATA2 { get; set; }
    
    		    
    		/// <summary>
    		/// Status
    		/// </summary>        
    	//    [DisplayName("Status")]
            [MaxLength(10, ErrorMessage = "Status cannot be longer than 10 characters")]
    		public string  Status { get; set; }
    
    		    
    	}
    //}
    
}
