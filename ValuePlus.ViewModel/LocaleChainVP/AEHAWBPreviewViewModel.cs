using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ValuePlus.ViewModel;



namespace ValuePlus.ViewModel.LocaleChainVP
{
    
    /// <summary>
    /// AEHAWBPreview class
    /// </summary>
    //[MetadataType(typeof(AEHAWBPreviewViewModel))]
    //public  partial class AEHAWBPreview
    //{
    
    	/// <summary>
    	/// AEHAWBPreview Metadata class
    	/// </summary>
    	public   class AEHAWBPreviewViewModel:ViewModelBase
    	{
    		    
    		/// <summary>
    		/// ID
    		/// </summary>        
    	//    [DisplayName("ID")]
            [Required(ErrorMessage = "ID is required")]
    		public int  ID { get; set; }
    
    		    
    		/// <summary>
    		/// HAWBID
    		/// </summary>        
    	//    [DisplayName("HAWBID")]
    		public Nullable<int>  HAWBID { get; set; }
    
    		    
    		/// <summary>
    		/// Shipper Info
    		/// </summary>        
    	//    [DisplayName("Shipper Info")]
            [MaxLength(255, ErrorMessage = "Shipper Info cannot be longer than 255 characters")]
    		public string  ShipperInfo { get; set; }
    
    		    
    		/// <summary>
    		/// CNEEInfo
    		/// </summary>        
    	//    [DisplayName("CNEEInfo")]
            [MaxLength(255, ErrorMessage = "CNEEInfo cannot be longer than 255 characters")]
    		public string  CNEEInfo { get; set; }
    
    		    
    		/// <summary>
    		/// Issuing Agent Info
    		/// </summary>        
    	//    [DisplayName("Issuing Agent Info")]
            [MaxLength(255, ErrorMessage = "Issuing Agent Info cannot be longer than 255 characters")]
    		public string  IssuingAgentInfo { get; set; }
    
    		    
    		/// <summary>
    		/// Agent Code
    		/// </summary>        
    	//    [DisplayName("Agent Code")]
            [MaxLength(255, ErrorMessage = "Agent Code cannot be longer than 255 characters")]
    		public string  AgentCode { get; set; }
    
    		    
    		/// <summary>
    		/// Port Of DEPT
    		/// </summary>        
    	//    [DisplayName("Port Of DEPT")]
            [MaxLength(50, ErrorMessage = "Port Of DEPT cannot be longer than 50 characters")]
    		public string  PortOfDEPT { get; set; }
    
    		    
    		/// <summary>
    		/// Arrive One
    		/// </summary>        
    	//    [DisplayName("Arrive One")]
            [MaxLength(255, ErrorMessage = "Arrive One cannot be longer than 255 characters")]
    		public string  ArriveOne { get; set; }
    
    		    
    		/// <summary>
    		/// Arrive Two
    		/// </summary>        
    	//    [DisplayName("Arrive Two")]
            [MaxLength(255, ErrorMessage = "Arrive Two cannot be longer than 255 characters")]
    		public string  ArriveTwo { get; set; }
    
    		    
    		/// <summary>
    		/// Arrive Three
    		/// </summary>        
    	//    [DisplayName("Arrive Three")]
            [MaxLength(255, ErrorMessage = "Arrive Three cannot be longer than 255 characters")]
    		public string  ArriveThree { get; set; }
    
    		    
    		/// <summary>
    		/// FLTOne
    		/// </summary>        
    	//    [DisplayName("FLTOne")]
            [MaxLength(255, ErrorMessage = "FLTOne cannot be longer than 255 characters")]
    		public string  FLTOne { get; set; }
    
    		    
    		/// <summary>
    		/// FLTTwo
    		/// </summary>        
    	//    [DisplayName("FLTTwo")]
            [MaxLength(255, ErrorMessage = "FLTTwo cannot be longer than 255 characters")]
    		public string  FLTTwo { get; set; }
    
    		    
    		/// <summary>
    		/// FLTThree
    		/// </summary>        
    	//    [DisplayName("FLTThree")]
            [MaxLength(255, ErrorMessage = "FLTThree cannot be longer than 255 characters")]
    		public string  FLTThree { get; set; }
    
    		    
    		/// <summary>
    		/// Port Of DSTN
    		/// </summary>        
    	//    [DisplayName("Port Of DSTN")]
            [MaxLength(50, ErrorMessage = "Port Of DSTN cannot be longer than 50 characters")]
    		public string  PortOfDSTN { get; set; }
    
    		    
    		/// <summary>
    		/// Handling Info
    		/// </summary>        
    	//    [DisplayName("Handling Info")]
            [MaxLength(255, ErrorMessage = "Handling Info cannot be longer than 255 characters")]
    		public string  HandlingInfo { get; set; }
    
    		    
    		/// <summary>
    		/// Issuer Info
    		/// </summary>        
    	//    [DisplayName("Issuer Info")]
            [MaxLength(255, ErrorMessage = "Issuer Info cannot be longer than 255 characters")]
    		public string  IssuerInfo { get; set; }
    
    		    
    		/// <summary>
    		/// Notify Info
    		/// </summary>        
    	//    [DisplayName("Notify Info")]
            [MaxLength(255, ErrorMessage = "Notify Info cannot be longer than 255 characters")]
    		public string  NotifyInfo { get; set; }
    
    		    
    		/// <summary>
    		/// Currency
    		/// </summary>        
    	//    [DisplayName("Currency")]
            [MaxLength(3, ErrorMessage = "Currency cannot be longer than 3 characters")]
    		public string  Currency { get; set; }
    
    		    
    		/// <summary>
    		/// Frt Pay Type
    		/// </summary>        
    	//    [DisplayName("Frt Pay Type")]
            [MaxLength(2, ErrorMessage = "Frt Pay Type cannot be longer than 2 characters")]
    		public string  FrtPayType { get; set; }
    
    		    
    		/// <summary>
    		/// Other Pay Type
    		/// </summary>        
    	//    [DisplayName("Other Pay Type")]
            [MaxLength(2, ErrorMessage = "Other Pay Type cannot be longer than 2 characters")]
    		public string  OtherPayType { get; set; }
    
    		    
    		/// <summary>
    		/// Dec For Carriage
    		/// </summary>        
    	//    [DisplayName("Dec For Carriage")]
            [MaxLength(255, ErrorMessage = "Dec For Carriage cannot be longer than 255 characters")]
    		public string  DecForCarriage { get; set; }
    
    		    
    		/// <summary>
    		/// Dec For Customs
    		/// </summary>        
    	//    [DisplayName("Dec For Customs")]
            [MaxLength(255, ErrorMessage = "Dec For Customs cannot be longer than 255 characters")]
    		public string  DecForCustoms { get; set; }
    
    		    
    		/// <summary>
    		/// Amount Of Ins
    		/// </summary>        
    	//    [DisplayName("Amount Of Ins")]
            [MaxLength(255, ErrorMessage = "Amount Of Ins cannot be longer than 255 characters")]
    		public string  AmountOfIns { get; set; }
    
    		    
    		/// <summary>
    		/// Weight Charge
    		/// </summary>        
    	//    [DisplayName("Weight Charge")]
            [MaxLength(255, ErrorMessage = "Weight Charge cannot be longer than 255 characters")]
    		public string  WeightCharge { get; set; }
    
    		    
    		/// <summary>
    		/// Total Amount
    		/// </summary>        
    	//    [DisplayName("Total Amount")]
            [MaxLength(255, ErrorMessage = "Total Amount cannot be longer than 255 characters")]
    		public string  TotalAmount { get; set; }
    
    		    
    		/// <summary>
    		/// Other Charges
    		/// </summary>        
    	//    [DisplayName("Other Charges")]
            [MaxLength(255, ErrorMessage = "Other Charges cannot be longer than 255 characters")]
    		public string  OtherCharges { get; set; }
    
    		    
    		/// <summary>
    		/// SHPRCertify
    		/// </summary>        
    	//    [DisplayName("SHPRCertify")]
            [MaxLength(255, ErrorMessage = "SHPRCertify cannot be longer than 255 characters")]
    		public string  SHPRCertify { get; set; }
    
    		    
    		/// <summary>
    		/// Carrier Certify
    		/// </summary>        
    	//    [DisplayName("Carrier Certify")]
            [MaxLength(255, ErrorMessage = "Carrier Certify cannot be longer than 255 characters")]
    		public string  CarrierCertify { get; set; }
    
    		    
    		/// <summary>
    		/// PCS
    		/// </summary>        
    	//    [DisplayName("PCS")]
    		public Nullable<int>  PCS { get; set; }
    
    		    
    		/// <summary>
    		/// GWT
    		/// </summary>        
    	//    [DisplayName("GWT")]
    		public Nullable<double>  GWT { get; set; }
    
    		    
    		/// <summary>
    		/// WTUOM
    		/// </summary>        
    	//    [DisplayName("WTUOM")]
            [MaxLength(2, ErrorMessage = "WTUOM cannot be longer than 2 characters")]
    		public string  WTUOM { get; set; }
    
    		    
    		/// <summary>
    		/// Rate Class
    		/// </summary>        
    	//    [DisplayName("Rate Class")]
            [MaxLength(1, ErrorMessage = "Rate Class cannot be longer than 1 characters")]
    		public string  RateClass { get; set; }
    
    		    
    		/// <summary>
    		/// CWT
    		/// </summary>        
    	//    [DisplayName("CWT")]
    		public Nullable<double>  CWT { get; set; }
    
    		    
    		/// <summary>
    		/// Rate
    		/// </summary>        
    	//    [DisplayName("Rate")]
    		public Nullable<double>  Rate { get; set; }
    
    		    
    		/// <summary>
    		/// Nature Of Goods
    		/// </summary>        
    	//    [DisplayName("Nature Of Goods")]
            [MaxLength(255, ErrorMessage = "Nature Of Goods cannot be longer than 255 characters")]
    		public string  NatureOfGoods { get; set; }
    
    		    
    		/// <summary>
    		/// HAWBNo
    		/// </summary>        
    	//    [DisplayName("HAWBNo")]
            [MaxLength(20, ErrorMessage = "HAWBNo cannot be longer than 20 characters")]
    		public string  HAWBNo { get; set; }
    
    		    
    		/// <summary>
    		/// MAWBNo
    		/// </summary>        
    	//    [DisplayName("MAWBNo")]
            [MaxLength(20, ErrorMessage = "MAWBNo cannot be longer than 20 characters")]
    		public string  MAWBNo { get; set; }
    
    		    
    		/// <summary>
    		/// Nature Of Goods1
    		/// </summary>        
    	//    [DisplayName("Nature Of Goods1")]
            [MaxLength(255, ErrorMessage = "Nature Of Goods1 cannot be longer than 255 characters")]
    		public string  NatureOfGoods1 { get; set; }
    
    		    
    		/// <summary>
    		/// Nature Of Goods2
    		/// </summary>        
    	//    [DisplayName("Nature Of Goods2")]
            [MaxLength(255, ErrorMessage = "Nature Of Goods2 cannot be longer than 255 characters")]
    		public string  NatureOfGoods2 { get; set; }
    
    		    
    		/// <summary>
    		/// Nature Of Goods3
    		/// </summary>        
    	//    [DisplayName("Nature Of Goods3")]
            [MaxLength(255, ErrorMessage = "Nature Of Goods3 cannot be longer than 255 characters")]
    		public string  NatureOfGoods3 { get; set; }
    
    		    
    		/// <summary>
    		/// Nature Of Goods4
    		/// </summary>        
    	//    [DisplayName("Nature Of Goods4")]
            [MaxLength(255, ErrorMessage = "Nature Of Goods4 cannot be longer than 255 characters")]
    		public string  NatureOfGoods4 { get; set; }
    
    		    
    		/// <summary>
    		/// Shipper Name
    		/// </summary>        
    	//    [DisplayName("Shipper Name")]
            [MaxLength(255, ErrorMessage = "Shipper Name cannot be longer than 255 characters")]
    		public string  ShipperName { get; set; }
    
    		    
    		/// <summary>
    		/// CNEEName
    		/// </summary>        
    	//    [DisplayName("CNEEName")]
            [MaxLength(255, ErrorMessage = "CNEEName cannot be longer than 255 characters")]
    		public string  CNEEName { get; set; }
    
    		    
    		/// <summary>
    		/// Issuing Agent Name
    		/// </summary>        
    	//    [DisplayName("Issuing Agent Name")]
            [MaxLength(255, ErrorMessage = "Issuing Agent Name cannot be longer than 255 characters")]
    		public string  IssuingAgentName { get; set; }
    
    		    
    		/// <summary>
    		/// Notify Name
    		/// </summary>        
    	//    [DisplayName("Notify Name")]
            [MaxLength(255, ErrorMessage = "Notify Name cannot be longer than 255 characters")]
    		public string  NotifyName { get; set; }
    
    		    
    		/// <summary>
    		/// Pay Type Info
    		/// </summary>        
    	//    [DisplayName("Pay Type Info")]
            [MaxLength(255, ErrorMessage = "Pay Type Info cannot be longer than 255 characters")]
    		public string  PayTypeInfo { get; set; }
    
    		    
    		/// <summary>
    		/// Prepaid Due Agent
    		/// </summary>        
    	//    [DisplayName("Prepaid Due Agent")]
            [MaxLength(255, ErrorMessage = "Prepaid Due Agent cannot be longer than 255 characters")]
    		public string  PrepaidDueAgent { get; set; }
    
    		    
    		/// <summary>
    		/// Collect Due Agent
    		/// </summary>        
    	//    [DisplayName("Collect Due Agent")]
            [MaxLength(255, ErrorMessage = "Collect Due Agent cannot be longer than 255 characters")]
    		public string  CollectDueAgent { get; set; }
    
    		    
    		/// <summary>
    		/// Prepaid Due Carrier
    		/// </summary>        
    	//    [DisplayName("Prepaid Due Carrier")]
            [MaxLength(255, ErrorMessage = "Prepaid Due Carrier cannot be longer than 255 characters")]
    		public string  PrepaidDueCarrier { get; set; }
    
    		    
    		/// <summary>
    		/// Collect Due Carrier
    		/// </summary>        
    	//    [DisplayName("Collect Due Carrier")]
            [MaxLength(255, ErrorMessage = "Collect Due Carrier cannot be longer than 255 characters")]
    		public string  CollectDueCarrier { get; set; }
    
    		    
    		/// <summary>
    		/// Total Prepaid
    		/// </summary>        
    	//    [DisplayName("Total Prepaid")]
            [MaxLength(255, ErrorMessage = "Total Prepaid cannot be longer than 255 characters")]
    		public string  TotalPrepaid { get; set; }
    
    		    
    		/// <summary>
    		/// Total Collect
    		/// </summary>        
    	//    [DisplayName("Total Collect")]
            [MaxLength(255, ErrorMessage = "Total Collect cannot be longer than 255 characters")]
    		public string  TotalCollect { get; set; }
    
    		    
    		/// <summary>
    		/// Prepaid Valuation
    		/// </summary>        
    	//    [DisplayName("Prepaid Valuation")]
            [MaxLength(255, ErrorMessage = "Prepaid Valuation cannot be longer than 255 characters")]
    		public string  PrepaidValuation { get; set; }
    
    		    
    		/// <summary>
    		/// Collect Valuation
    		/// </summary>        
    	//    [DisplayName("Collect Valuation")]
            [MaxLength(255, ErrorMessage = "Collect Valuation cannot be longer than 255 characters")]
    		public string  CollectValuation { get; set; }
    
    		    
    		/// <summary>
    		/// Prepaid Tax
    		/// </summary>        
    	//    [DisplayName("Prepaid Tax")]
            [MaxLength(255, ErrorMessage = "Prepaid Tax cannot be longer than 255 characters")]
    		public string  PrepaidTax { get; set; }
    
    		    
    		/// <summary>
    		/// Collect Tax
    		/// </summary>        
    	//    [DisplayName("Collect Tax")]
            [MaxLength(255, ErrorMessage = "Collect Tax cannot be longer than 255 characters")]
    		public string  CollectTax { get; set; }
    
    		    
    		/// <summary>
    		/// Total PCS
    		/// </summary>        
    	//    [DisplayName("Total PCS")]
    		public Nullable<int>  TotalPCS { get; set; }
    
    		    
    		/// <summary>
    		/// Total GWT
    		/// </summary>        
    	//    [DisplayName("Total GWT")]
    		public Nullable<double>  TotalGWT { get; set; }
    
    		    
    		/// <summary>
    		/// Amount
    		/// </summary>        
    	//    [DisplayName("Amount")]
    		public Nullable<double>  Amount { get; set; }
    
    		    
    		/// <summary>
    		/// Marks
    		/// </summary>        
    	//    [DisplayName("Marks")]
            [MaxLength(255, ErrorMessage = "Marks cannot be longer than 255 characters")]
    		public string  Marks { get; set; }
    
    		    
    		/// <summary>
    		/// Issue Date
    		/// </summary>        
    	//    [DisplayName("Issue Date")]
    		public Nullable<System.DateTime>  IssueDate { get; set; }
    
    		    
    		/// <summary>
    		/// Station ID
    		/// </summary>        
    	//    [DisplayName("Station ID")]
            [Required(ErrorMessage = "Station ID is required")]
            [MaxLength(10, ErrorMessage = "Station ID cannot be longer than 10 characters")]
    		public string  StationID { get; set; }
    
    		    
    		/// <summary>
    		/// Created By
    		/// </summary>        
    	//    [DisplayName("Created By")]
            [Required(ErrorMessage = "Created By is required")]
            [MaxLength(6, ErrorMessage = "Created By cannot be longer than 6 characters")]
    		public string  CreatedBy { get; set; }
    
    		    
    		/// <summary>
    		/// Created Date
    		/// </summary>        
    	//    [DisplayName("Created Date")]
            [Required(ErrorMessage = "Created Date is required")]
    		public System.DateTime  CreatedDate { get; set; }
    
    		    
    		/// <summary>
    		/// Updated By
    		/// </summary>        
    	//    [DisplayName("Updated By")]
            [Required(ErrorMessage = "Updated By is required")]
            [MaxLength(6, ErrorMessage = "Updated By cannot be longer than 6 characters")]
    		public string  UpdatedBy { get; set; }
    
    		    
    		/// <summary>
    		/// Updated Date
    		/// </summary>        
    	//    [DisplayName("Updated Date")]
            [Required(ErrorMessage = "Updated Date is required")]
    		public System.DateTime  UpdatedDate { get; set; }
    
    		    
    		/// <summary>
    		/// Account No
    		/// </summary>        
    	//    [DisplayName("Account No")]
            [MaxLength(50, ErrorMessage = "Account No cannot be longer than 50 characters")]
    		public string  AccountNo { get; set; }
    
    		    
    		/// <summary>
    		/// From AI
    		/// </summary>        
    	//    [DisplayName("From AI")]
            [MaxLength(1, ErrorMessage = "From AI cannot be longer than 1 characters")]
    		public string  FromAI { get; set; }
    
    		    
    		/// <summary>
    		/// ORGShipper
    		/// </summary>        
    	//    [DisplayName("ORGShipper")]
            [MaxLength(10, ErrorMessage = "ORGShipper cannot be longer than 10 characters")]
    		public string  ORGShipper { get; set; }
    
    		    
    		/// <summary>
    		/// ORGShipper Name
    		/// </summary>        
    	//    [DisplayName("ORGShipper Name")]
            [MaxLength(255, ErrorMessage = "ORGShipper Name cannot be longer than 255 characters")]
    		public string  ORGShipperName { get; set; }
    
    		    
    		/// <summary>
    		/// ORGShipper Info
    		/// </summary>        
    	//    [DisplayName("ORGShipper Info")]
            [MaxLength(255, ErrorMessage = "ORGShipper Info cannot be longer than 255 characters")]
    		public string  ORGShipperInfo { get; set; }
    
    		    
    		/// <summary>
    		/// ORGPort
    		/// </summary>        
    	//    [DisplayName("ORGPort")]
            [MaxLength(10, ErrorMessage = "ORGPort cannot be longer than 10 characters")]
    		public string  ORGPort { get; set; }
    
    		    
    		/// <summary>
    		/// ORGPort Name
    		/// </summary>        
    	//    [DisplayName("ORGPort Name")]
            [MaxLength(255, ErrorMessage = "ORGPort Name cannot be longer than 255 characters")]
    		public string  ORGPortName { get; set; }
    
    		    
    		/// <summary>
    		/// ORGArrive One
    		/// </summary>        
    	//    [DisplayName("ORGArrive One")]
            [MaxLength(255, ErrorMessage = "ORGArrive One cannot be longer than 255 characters")]
    		public string  ORGArriveOne { get; set; }
    
    		    
    		/// <summary>
    		/// ORGFLTOne
    		/// </summary>        
    	//    [DisplayName("ORGFLTOne")]
            [MaxLength(255, ErrorMessage = "ORGFLTOne cannot be longer than 255 characters")]
    		public string  ORGFLTOne { get; set; }
    
    		    
    		/// <summary>
    		/// ORGHAWBNo
    		/// </summary>        
    	//    [DisplayName("ORGHAWBNo")]
            [MaxLength(20, ErrorMessage = "ORGHAWBNo cannot be longer than 20 characters")]
    		public string  ORGHAWBNo { get; set; }
    
    		    
    		/// <summary>
    		/// CNEE_ Alt Add ID
    		/// </summary>        
    	//    [DisplayName("CNEE_ Alt Add ID")]
    		public Nullable<int>  CNEE_AltAddID { get; set; }
    
    		    
    		/// <summary>
    		/// Shipper_ Alt Add ID
    		/// </summary>        
    	//    [DisplayName("Shipper_ Alt Add ID")]
    		public Nullable<int>  Shipper_AltAddID { get; set; }
    
    		    
    		/// <summary>
    		/// Print By
    		/// </summary>        
    	//    [DisplayName("Print By")]
            [MaxLength(6, ErrorMessage = "Print By cannot be longer than 6 characters")]
    		public string  PrintBy { get; set; }
    
    		    
    		/// <summary>
    		/// Print Date
    		/// </summary>        
    	//    [DisplayName("Print Date")]
    		public Nullable<System.DateTime>  PrintDate { get; set; }
    
    		    
    		/// <summary>
    		/// CANNo
    		/// </summary>        
    	//    [DisplayName("CANNo")]
            [MaxLength(25, ErrorMessage = "CANNo cannot be longer than 25 characters")]
    		public string  CANNo { get; set; }
    
    		    
    		/// <summary>
    		/// Foot Show
    		/// </summary>        
    	//    [DisplayName("Foot Show")]
            [MaxLength(50, ErrorMessage = "Foot Show cannot be longer than 50 characters")]
    		public string  FootShow { get; set; }
    
    		    
    		/// <summary>
    		/// Lot No
    		/// </summary>        
    	//    [DisplayName("Lot No")]
            [MaxLength(10, ErrorMessage = "Lot No cannot be longer than 10 characters")]
    		public string  LotNo { get; set; }
    
    		    
    		/// <summary>
    		/// Trade Term
    		/// </summary>        
    	//    [DisplayName("Trade Term")]
            [MaxLength(5, ErrorMessage = "Trade Term cannot be longer than 5 characters")]
    		public string  TradeTerm { get; set; }
    
    		    
    		/// <summary>
    		/// Version
    		/// </summary>        
    	//    [DisplayName("Version")]
            [MaxLength(8, ErrorMessage = "Version cannot be longer than 8 characters")]
    		public byte[]  Version { get; set; }
    
    		    
    	}
    //}
    
}
