using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ValuePlus.ViewModel;



namespace ValuePlus.ViewModel.LocaleChainVP
{
    
    /// <summary>
    /// AEBookConsoleDimension class
    /// </summary>
    //[MetadataType(typeof(AEBookConsoleDimensionViewModel))]
    //public  partial class AEBookConsoleDimension
    //{
    
    	/// <summary>
    	/// AEBookConsoleDimension Metadata class
    	/// </summary>
    	public   class AEBookConsoleDimensionViewModel:ViewModelBase
    	{
    		    
    		/// <summary>
    		/// ID
    		/// </summary>        
    	//    [DisplayName("ID")]
            [Required(ErrorMessage = "ID is required")]
    		public int  ID { get; set; }
    
    		    
    		/// <summary>
    		/// Org Station ID
    		/// </summary>        
    	//    [DisplayName("Org Station ID")]
            [MaxLength(10, ErrorMessage = "Org Station ID cannot be longer than 10 characters")]
    		public string  OrgStationID { get; set; }
    
    		    
    		/// <summary>
    		/// Org HAWBID
    		/// </summary>        
    	//    [DisplayName("Org HAWBID")]
    		public Nullable<int>  OrgHAWBID { get; set; }
    
    		    
    		/// <summary>
    		/// Length
    		/// </summary>        
    	//    [DisplayName("Length")]
    		public Nullable<int>  Length { get; set; }
    
    		    
    		/// <summary>
    		/// Width
    		/// </summary>        
    	//    [DisplayName("Width")]
    		public Nullable<int>  Width { get; set; }
    
    		    
    		/// <summary>
    		/// Height
    		/// </summary>        
    	//    [DisplayName("Height")]
    		public Nullable<int>  Height { get; set; }
    
    		    
    		/// <summary>
    		/// UOM
    		/// </summary>        
    	//    [DisplayName("UOM")]
            [MaxLength(50, ErrorMessage = "UOM cannot be longer than 50 characters")]
    		public string  UOM { get; set; }
    
    		    
    		/// <summary>
    		/// PCS
    		/// </summary>        
    	//    [DisplayName("PCS")]
    		public Nullable<int>  PCS { get; set; }
    
    		    
    		/// <summary>
    		/// Volume
    		/// </summary>        
    	//    [DisplayName("Volume")]
    		public Nullable<decimal>  Volume { get; set; }
    
    		    
    		/// <summary>
    		/// VWT
    		/// </summary>        
    	//    [DisplayName("VWT")]
    		public Nullable<decimal>  VWT { get; set; }
    
    		    
    		/// <summary>
    		/// Created By
    		/// </summary>        
    	//    [DisplayName("Created By")]
            [MaxLength(50, ErrorMessage = "Created By cannot be longer than 50 characters")]
    		public string  CreatedBy { get; set; }
    
    		    
    		/// <summary>
    		/// Created Date
    		/// </summary>        
    	//    [DisplayName("Created Date")]
    		public Nullable<System.DateTime>  CreatedDate { get; set; }
    
    		    
    		/// <summary>
    		/// Updated By
    		/// </summary>        
    	//    [DisplayName("Updated By")]
            [MaxLength(50, ErrorMessage = "Updated By cannot be longer than 50 characters")]
    		public string  UpdatedBy { get; set; }
    
    		    
    		/// <summary>
    		/// Updated Date
    		/// </summary>        
    	//    [DisplayName("Updated Date")]
    		public Nullable<System.DateTime>  UpdatedDate { get; set; }
    
    		    
    	}
    //}
    
}
