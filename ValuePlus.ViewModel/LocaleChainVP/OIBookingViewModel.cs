using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ValuePlus.ViewModel;



namespace ValuePlus.ViewModel.LocaleChainVP
{
    
    /// <summary>
    /// OIBooking class
    /// </summary>
    //[MetadataType(typeof(OIBookingViewModel))]
    //public  partial class OIBooking
    //{
    
    	/// <summary>
    	/// OIBooking Metadata class
    	/// </summary>
    	public   class OIBookingViewModel:ViewModelBase
    	{
    		    
    		/// <summary>
    		/// ID
    		/// </summary>        
    	//    [DisplayName("ID")]
            [Required(ErrorMessage = "ID is required")]
    		public int  ID { get; set; }
    
    		    
    		/// <summary>
    		/// Booking No
    		/// </summary>        
    	//    [DisplayName("Booking No")]
            [MaxLength(20, ErrorMessage = "Booking No cannot be longer than 20 characters")]
    		public string  BookingNo { get; set; }
    
    		    
    		/// <summary>
    		/// Station ID
    		/// </summary>        
    	//    [DisplayName("Station ID")]
            [MaxLength(5, ErrorMessage = "Station ID cannot be longer than 5 characters")]
    		public string  StationID { get; set; }
    
    		    
    		/// <summary>
    		/// Src Station ID
    		/// </summary>        
    	//    [DisplayName("Src Station ID")]
            [MaxLength(5, ErrorMessage = "Src Station ID cannot be longer than 5 characters")]
    		public string  SrcStationID { get; set; }
    
    		    
    		/// <summary>
    		/// Src ID
    		/// </summary>        
    	//    [DisplayName("Src ID")]
    		public Nullable<int>  SrcID { get; set; }
    
    		    
    		/// <summary>
    		/// HBLID
    		/// </summary>        
    	//    [DisplayName("HBLID")]
    		public Nullable<int>  HBLID { get; set; }
    
    		    
    		/// <summary>
    		/// PCS
    		/// </summary>        
    	//    [DisplayName("PCS")]
    		public Nullable<int>  PCS { get; set; }
    
    		    
    		/// <summary>
    		/// Customer
    		/// </summary>        
    	//    [DisplayName("Customer")]
    		public Nullable<int>  Customer { get; set; }
    
    		    
    		/// <summary>
    		/// SHPR
    		/// </summary>        
    	//    [DisplayName("SHPR")]
    		public Nullable<int>  SHPR { get; set; }
    
    		    
    		/// <summary>
    		/// CNEE
    		/// </summary>        
    	//    [DisplayName("CNEE")]
    		public Nullable<int>  CNEE { get; set; }
    
    		    
    		/// <summary>
    		/// NTFY
    		/// </summary>        
    	//    [DisplayName("NTFY")]
    		public Nullable<int>  NTFY { get; set; }
    
    		    
    		/// <summary>
    		/// Ocean Vessel
    		/// </summary>        
    	//    [DisplayName("Ocean Vessel")]
            [MaxLength(60, ErrorMessage = "Ocean Vessel cannot be longer than 60 characters")]
    		public string  OceanVessel { get; set; }
    
    		    
    		/// <summary>
    		/// PReceipt
    		/// </summary>        
    	//    [DisplayName("PReceipt")]
    		public Nullable<int>  PReceipt { get; set; }
    
    		    
    		/// <summary>
    		/// PLoading
    		/// </summary>        
    	//    [DisplayName("PLoading")]
    		public Nullable<int>  PLoading { get; set; }
    
    		    
    		/// <summary>
    		/// PDischarge
    		/// </summary>        
    	//    [DisplayName("PDischarge")]
    		public Nullable<int>  PDischarge { get; set; }
    
    		    
    		/// <summary>
    		/// PDelivery
    		/// </summary>        
    	//    [DisplayName("PDelivery")]
    		public Nullable<int>  PDelivery { get; set; }
    
    		    
    		/// <summary>
    		/// Is ISF
    		/// </summary>        
    	//    [DisplayName("Is ISF")]
            [MaxLength(1, ErrorMessage = "Is ISF cannot be longer than 1 characters")]
    		public string  IsISF { get; set; }
    
    		    
    		/// <summary>
    		/// Is AMS
    		/// </summary>        
    	//    [DisplayName("Is AMS")]
            [MaxLength(1, ErrorMessage = "Is AMS cannot be longer than 1 characters")]
    		public string  IsAMS { get; set; }
    
    		    
    		/// <summary>
    		/// Is Co Load In
    		/// </summary>        
    	//    [DisplayName("Is Co Load In")]
            [MaxLength(1, ErrorMessage = "Is Co Load In cannot be longer than 1 characters")]
    		public string  IsCoLoadIn { get; set; }
    
    		    
    		/// <summary>
    		/// Booking Remark
    		/// </summary>        
    	//    [DisplayName("Booking Remark")]
            [MaxLength(255, ErrorMessage = "Booking Remark cannot be longer than 255 characters")]
    		public string  BookingRemark { get; set; }
    
    		    
    		/// <summary>
    		/// Shpt Type
    		/// </summary>        
    	//    [DisplayName("Shpt Type")]
            [MaxLength(20, ErrorMessage = "Shpt Type cannot be longer than 20 characters")]
    		public string  ShptType { get; set; }
    
    		    
    		/// <summary>
    		/// Move Type
    		/// </summary>        
    	//    [DisplayName("Move Type")]
            [MaxLength(20, ErrorMessage = "Move Type cannot be longer than 20 characters")]
    		public string  MoveType { get; set; }
    
    		    
    		/// <summary>
    		/// Freight Pay Type
    		/// </summary>        
    	//    [DisplayName("Freight Pay Type")]
            [MaxLength(20, ErrorMessage = "Freight Pay Type cannot be longer than 20 characters")]
    		public string  FreightPayType { get; set; }
    
    		    
    		/// <summary>
    		/// FCalculate Type
    		/// </summary>        
    	//    [DisplayName("FCalculate Type")]
            [MaxLength(10, ErrorMessage = "FCalculate Type cannot be longer than 10 characters")]
    		public string  FCalculateType { get; set; }
    
    		    
    		/// <summary>
    		/// Trade Type
    		/// </summary>        
    	//    [DisplayName("Trade Type")]
            [MaxLength(20, ErrorMessage = "Trade Type cannot be longer than 20 characters")]
    		public string  TradeType { get; set; }
    
    		    
    		/// <summary>
    		/// PReceipt ETD
    		/// </summary>        
    	//    [DisplayName("PReceipt ETD")]
    		public Nullable<System.DateTime>  PReceiptETD { get; set; }
    
    		    
    		/// <summary>
    		/// PLoading ETD
    		/// </summary>        
    	//    [DisplayName("PLoading ETD")]
    		public Nullable<System.DateTime>  PLoadingETD { get; set; }
    
    		    
    		/// <summary>
    		/// PDischarge ETD
    		/// </summary>        
    	//    [DisplayName("PDischarge ETD")]
    		public Nullable<System.DateTime>  PDischargeETD { get; set; }
    
    		    
    		/// <summary>
    		/// PDelivery ETD
    		/// </summary>        
    	//    [DisplayName("PDelivery ETD")]
    		public Nullable<System.DateTime>  PDeliveryETD { get; set; }
    
    		    
    		/// <summary>
    		/// Cargo Ready Date
    		/// </summary>        
    	//    [DisplayName("Cargo Ready Date")]
    		public Nullable<System.DateTime>  CargoReadyDate { get; set; }
    
    		    
    		/// <summary>
    		/// Booking Cut Off Date
    		/// </summary>        
    	//    [DisplayName("Booking Cut Off Date")]
    		public Nullable<System.DateTime>  BookingCutOffDate { get; set; }
    
    		    
    		/// <summary>
    		/// Assign To
    		/// </summary>        
    	//    [DisplayName("Assign To")]
            [MaxLength(10, ErrorMessage = "Assign To cannot be longer than 10 characters")]
    		public string  AssignTo { get; set; }
    
    		    
    		/// <summary>
    		/// Sales Type
    		/// </summary>        
    	//    [DisplayName("Sales Type")]
            [MaxLength(10, ErrorMessage = "Sales Type cannot be longer than 10 characters")]
    		public string  SalesType { get; set; }
    
    		    
    		/// <summary>
    		/// CTNRIn Word
    		/// </summary>        
    	//    [DisplayName("CTNRIn Word")]
            [MaxLength(100, ErrorMessage = "CTNRIn Word cannot be longer than 100 characters")]
    		public string  CTNRInWord { get; set; }
    
    		    
    		/// <summary>
    		/// Dec Value Curr
    		/// </summary>        
    	//    [DisplayName("Dec Value Curr")]
            [MaxLength(10, ErrorMessage = "Dec Value Curr cannot be longer than 10 characters")]
    		public string  DecValueCurr { get; set; }
    
    		    
    		/// <summary>
    		/// Telex Release
    		/// </summary>        
    	//    [DisplayName("Telex Release")]
            [MaxLength(50, ErrorMessage = "Telex Release cannot be longer than 50 characters")]
    		public string  TelexRelease { get; set; }
    
    		    
    		/// <summary>
    		/// Is Draft
    		/// </summary>        
    	//    [DisplayName("Is Draft")]
            [MaxLength(1, ErrorMessage = "Is Draft cannot be longer than 1 characters")]
    		public string  IsDraft { get; set; }
    
    		    
    		/// <summary>
    		/// Is Void
    		/// </summary>        
    	//    [DisplayName("Is Void")]
            [MaxLength(1, ErrorMessage = "Is Void cannot be longer than 1 characters")]
    		public string  IsVoid { get; set; }
    
    		    
    		/// <summary>
    		/// DSTNStation ID
    		/// </summary>        
    	//    [DisplayName("DSTNStation ID")]
            [MaxLength(5, ErrorMessage = "DSTNStation ID cannot be longer than 5 characters")]
    		public string  DSTNStationID { get; set; }
    
    		    
    		/// <summary>
    		/// DSTNID
    		/// </summary>        
    	//    [DisplayName("DSTNID")]
    		public Nullable<int>  DSTNID { get; set; }
    
    		    
    		/// <summary>
    		/// Booking Send By
    		/// </summary>        
    	//    [DisplayName("Booking Send By")]
            [MaxLength(50, ErrorMessage = "Booking Send By cannot be longer than 50 characters")]
    		public string  BookingSendBy { get; set; }
    
    		    
    		/// <summary>
    		/// Booking Send DT
    		/// </summary>        
    	//    [DisplayName("Booking Send DT")]
    		public Nullable<System.DateTime>  BookingSendDT { get; set; }
    
    		    
    		/// <summary>
    		/// Quote Type
    		/// </summary>        
    	//    [DisplayName("Quote Type")]
            [MaxLength(250, ErrorMessage = "Quote Type cannot be longer than 250 characters")]
    		public string  QuoteType { get; set; }
    
    		    
    		/// <summary>
    		/// Created By
    		/// </summary>        
    	//    [DisplayName("Created By")]
            [MaxLength(50, ErrorMessage = "Created By cannot be longer than 50 characters")]
    		public string  CreatedBy { get; set; }
    
    		    
    		/// <summary>
    		/// Created DT
    		/// </summary>        
    	//    [DisplayName("Created DT")]
    		public Nullable<System.DateTime>  CreatedDT { get; set; }
    
    		    
    		/// <summary>
    		/// Updated By
    		/// </summary>        
    	//    [DisplayName("Updated By")]
            [MaxLength(50, ErrorMessage = "Updated By cannot be longer than 50 characters")]
    		public string  UpdatedBy { get; set; }
    
    		    
    		/// <summary>
    		/// Updated DT
    		/// </summary>        
    	//    [DisplayName("Updated DT")]
    		public Nullable<System.DateTime>  UpdatedDT { get; set; }
    
    		    
    		/// <summary>
    		/// Status
    		/// </summary>        
    	//    [DisplayName("Status")]
            [MaxLength(50, ErrorMessage = "Status cannot be longer than 50 characters")]
    		public string  Status { get; set; }
    
    		    
    		/// <summary>
    		/// Status Detail
    		/// </summary>        
    	//    [DisplayName("Status Detail")]
            [MaxLength(255, ErrorMessage = "Status Detail cannot be longer than 255 characters")]
    		public string  StatusDetail { get; set; }
    
    		    
    		/// <summary>
    		/// Quote No
    		/// </summary>        
    	//    [DisplayName("Quote No")]
            [MaxLength(50, ErrorMessage = "Quote No cannot be longer than 50 characters")]
    		public string  QuoteNo { get; set; }
    
    		    
    		/// <summary>
    		/// Ocean Voyage
    		/// </summary>        
    	//    [DisplayName("Ocean Voyage")]
            [MaxLength(80, ErrorMessage = "Ocean Voyage cannot be longer than 80 characters")]
    		public string  OceanVoyage { get; set; }
    
    		    
    		/// <summary>
    		/// Version
    		/// </summary>        
    	//    [DisplayName("Version")]
            [MaxLength(8, ErrorMessage = "Version cannot be longer than 8 characters")]
    		public byte[]  Version { get; set; }
    
    		    
    	}
    //}
    
}
