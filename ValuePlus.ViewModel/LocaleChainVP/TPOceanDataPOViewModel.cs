using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ValuePlus.ViewModel;



namespace ValuePlus.ViewModel.LocaleChainVP
{
    
    /// <summary>
    /// TPOceanDataPO class
    /// </summary>
    //[MetadataType(typeof(TPOceanDataPOViewModel))]
    //public  partial class TPOceanDataPO
    //{
    
    	/// <summary>
    	/// TPOceanDataPO Metadata class
    	/// </summary>
    	public   class TPOceanDataPOViewModel:ViewModelBase
    	{
    		    
    		/// <summary>
    		/// ID
    		/// </summary>        
    	//    [DisplayName("ID")]
            [Required(ErrorMessage = "ID is required")]
    		public int  ID { get; set; }
    
    		    
    		/// <summary>
    		/// Station ID
    		/// </summary>        
    	//    [DisplayName("Station ID")]
            [MaxLength(5, ErrorMessage = "Station ID cannot be longer than 5 characters")]
    		public string  StationID { get; set; }
    
    		    
    		/// <summary>
    		/// TPID
    		/// </summary>        
    	//    [DisplayName("TPID")]
            [Required(ErrorMessage = "TPID is required")]
            [MaxLength(15, ErrorMessage = "TPID cannot be longer than 15 characters")]
    		public string  TPID { get; set; }
    
    		    
    		/// <summary>
    		/// Source Station ID
    		/// </summary>        
    	//    [DisplayName("Source Station ID")]
            [MaxLength(3, ErrorMessage = "Source Station ID cannot be longer than 3 characters")]
    		public string  SourceStationID { get; set; }
    
    		    
    		/// <summary>
    		/// Source ID
    		/// </summary>        
    	//    [DisplayName("Source ID")]
    		public Nullable<int>  SourceID { get; set; }
    
    		    
    		/// <summary>
    		/// PONo
    		/// </summary>        
    	//    [DisplayName("PONo")]
            [MaxLength(30, ErrorMessage = "PONo cannot be longer than 30 characters")]
    		public string  PONo { get; set; }
    
    		    
    		/// <summary>
    		/// Serial No
    		/// </summary>        
    	//    [DisplayName("Serial No")]
            [MaxLength(60, ErrorMessage = "Serial No cannot be longer than 60 characters")]
    		public string  SerialNo { get; set; }
    
    		    
    		/// <summary>
    		/// Remarks
    		/// </summary>        
    	//    [DisplayName("Remarks")]
            [MaxLength(200, ErrorMessage = "Remarks cannot be longer than 200 characters")]
    		public string  Remarks { get; set; }
    
    		    
    		/// <summary>
    		/// Item No
    		/// </summary>        
    	//    [DisplayName("Item No")]
            [MaxLength(60, ErrorMessage = "Item No cannot be longer than 60 characters")]
    		public string  ItemNo { get; set; }
    
    		    
    		/// <summary>
    		/// Description
    		/// </summary>        
    	//    [DisplayName("Description")]
            [MaxLength(200, ErrorMessage = "Description cannot be longer than 200 characters")]
    		public string  Description { get; set; }
    
    		    
    		/// <summary>
    		/// Quantity
    		/// </summary>        
    	//    [DisplayName("Quantity")]
            [MaxLength(100, ErrorMessage = "Quantity cannot be longer than 100 characters")]
    		public string  Quantity { get; set; }
    
    		    
    		/// <summary>
    		/// SONO
    		/// </summary>        
    	//    [DisplayName("SONO")]
            [MaxLength(30, ErrorMessage = "SONO cannot be longer than 30 characters")]
    		public string  SONO { get; set; }
    
    		    
    		/// <summary>
    		/// Invoice No
    		/// </summary>        
    	//    [DisplayName("Invoice No")]
            [MaxLength(20, ErrorMessage = "Invoice No cannot be longer than 20 characters")]
    		public string  InvoiceNo { get; set; }
    
    		    
    		/// <summary>
    		/// Curr
    		/// </summary>        
    	//    [DisplayName("Curr")]
            [MaxLength(4, ErrorMessage = "Curr cannot be longer than 4 characters")]
    		public string  Curr { get; set; }
    
    		    
    		/// <summary>
    		/// Final Destination
    		/// </summary>        
    	//    [DisplayName("Final Destination")]
            [MaxLength(50, ErrorMessage = "Final Destination cannot be longer than 50 characters")]
    		public string  FinalDestination { get; set; }
    
    		    
    		/// <summary>
    		/// PODate
    		/// </summary>        
    	//    [DisplayName("PODate")]
    		public Nullable<System.DateTime>  PODate { get; set; }
    
    		    
    		/// <summary>
    		/// Invoice Date
    		/// </summary>        
    	//    [DisplayName("Invoice Date")]
    		public Nullable<System.DateTime>  InvoiceDate { get; set; }
    
    		    
    		/// <summary>
    		/// Amount
    		/// </summary>        
    	//    [DisplayName("Amount")]
    		public Nullable<decimal>  Amount { get; set; }
    
    		    
    		/// <summary>
    		/// SCTNS
    		/// </summary>        
    	//    [DisplayName("SCTNS")]
    		public Nullable<int>  SCTNS { get; set; }
    
    		    
    		/// <summary>
    		/// POExtra1
    		/// </summary>        
    	//    [DisplayName("POExtra1")]
            [MaxLength(50, ErrorMessage = "POExtra1 cannot be longer than 50 characters")]
    		public string  POExtra1 { get; set; }
    
    		    
    		/// <summary>
    		/// POExtra2
    		/// </summary>        
    	//    [DisplayName("POExtra2")]
            [MaxLength(50, ErrorMessage = "POExtra2 cannot be longer than 50 characters")]
    		public string  POExtra2 { get; set; }
    
    		    
    		/// <summary>
    		/// POExtra3
    		/// </summary>        
    	//    [DisplayName("POExtra3")]
            [MaxLength(50, ErrorMessage = "POExtra3 cannot be longer than 50 characters")]
    		public string  POExtra3 { get; set; }
    
    		    
    		/// <summary>
    		/// POExtra4
    		/// </summary>        
    	//    [DisplayName("POExtra4")]
            [MaxLength(50, ErrorMessage = "POExtra4 cannot be longer than 50 characters")]
    		public string  POExtra4 { get; set; }
    
    		    
    		/// <summary>
    		/// POExtra5
    		/// </summary>        
    	//    [DisplayName("POExtra5")]
            [MaxLength(50, ErrorMessage = "POExtra5 cannot be longer than 50 characters")]
    		public string  POExtra5 { get; set; }
    
    		    
    		/// <summary>
    		/// POExtra6
    		/// </summary>        
    	//    [DisplayName("POExtra6")]
            [MaxLength(50, ErrorMessage = "POExtra6 cannot be longer than 50 characters")]
    		public string  POExtra6 { get; set; }
    
    		    
    		/// <summary>
    		/// Created By
    		/// </summary>        
    	//    [DisplayName("Created By")]
            [MaxLength(50, ErrorMessage = "Created By cannot be longer than 50 characters")]
    		public string  CreatedBy { get; set; }
    
    		    
    		/// <summary>
    		/// Created DT
    		/// </summary>        
    	//    [DisplayName("Created DT")]
    		public Nullable<System.DateTime>  CreatedDT { get; set; }
    
    		    
    		/// <summary>
    		/// Updated By
    		/// </summary>        
    	//    [DisplayName("Updated By")]
            [MaxLength(50, ErrorMessage = "Updated By cannot be longer than 50 characters")]
    		public string  UpdatedBy { get; set; }
    
    		    
    		/// <summary>
    		/// Updated DT
    		/// </summary>        
    	//    [DisplayName("Updated DT")]
    		public Nullable<System.DateTime>  UpdatedDT { get; set; }
    
    		    
    		/// <summary>
    		/// POMSID
    		/// </summary>        
    	//    [DisplayName("POMSID")]
    		public Nullable<int>  POMSID { get; set; }
    
    		    
    		/// <summary>
    		/// UOM
    		/// </summary>        
    	//    [DisplayName("UOM")]
            [MaxLength(50, ErrorMessage = "UOM cannot be longer than 50 characters")]
    		public string  UOM { get; set; }
    
    		    
    	}
    //}
    
}
