using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ValuePlus.ViewModel;



namespace ValuePlus.ViewModel.LocaleChainVP
{
    
    /// <summary>
    /// AEMAWBINST class
    /// </summary>
    //[MetadataType(typeof(AEMAWBINSTViewModel))]
    //public  partial class AEMAWBINST
    //{
    
    	/// <summary>
    	/// AEMAWBINST Metadata class
    	/// </summary>
    	public   class AEMAWBINSTViewModel:ViewModelBase
    	{
    		    
    		/// <summary>
    		/// ID
    		/// </summary>        
    	//    [DisplayName("ID")]
            [Required(ErrorMessage = "ID is required")]
    		public int  ID { get; set; }
    
    		    
    		/// <summary>
    		/// From
    		/// </summary>        
    	//    [DisplayName("From")]
            [MaxLength(10, ErrorMessage = "From cannot be longer than 10 characters")]
    		public string  From { get; set; }
    
    		    
    		/// <summary>
    		/// Category
    		/// </summary>        
    	//    [DisplayName("Category")]
            [MaxLength(20, ErrorMessage = "Category cannot be longer than 20 characters")]
    		public string  Category { get; set; }
    
    		    
    		/// <summary>
    		/// Subject
    		/// </summary>        
    	//    [DisplayName("Subject")]
            [MaxLength(255, ErrorMessage = "Subject cannot be longer than 255 characters")]
    		public string  Subject { get; set; }
    
    		    
    		/// <summary>
    		/// Date
    		/// </summary>        
    	//    [DisplayName("Date")]
            [Required(ErrorMessage = "Date is required")]
    		public System.DateTime  Date { get; set; }
    
    		    
    		/// <summary>
    		/// MAWBID
    		/// </summary>        
    	//    [DisplayName("MAWBID")]
            [Required(ErrorMessage = "MAWBID is required")]
    		public int  MAWBID { get; set; }
    
    		    
    		/// <summary>
    		/// Station ID
    		/// </summary>        
    	//    [DisplayName("Station ID")]
            [Required(ErrorMessage = "Station ID is required")]
            [MaxLength(10, ErrorMessage = "Station ID cannot be longer than 10 characters")]
    		public string  StationID { get; set; }
    
    		    
    		/// <summary>
    		/// Created By
    		/// </summary>        
    	//    [DisplayName("Created By")]
            [Required(ErrorMessage = "Created By is required")]
            [MaxLength(6, ErrorMessage = "Created By cannot be longer than 6 characters")]
    		public string  CreatedBy { get; set; }
    
    		    
    		/// <summary>
    		/// Created Date
    		/// </summary>        
    	//    [DisplayName("Created Date")]
            [Required(ErrorMessage = "Created Date is required")]
    		public System.DateTime  CreatedDate { get; set; }
    
    		    
    		/// <summary>
    		/// Updated By
    		/// </summary>        
    	//    [DisplayName("Updated By")]
            [Required(ErrorMessage = "Updated By is required")]
            [MaxLength(6, ErrorMessage = "Updated By cannot be longer than 6 characters")]
    		public string  UpdatedBy { get; set; }
    
    		    
    		/// <summary>
    		/// Updated Date
    		/// </summary>        
    	//    [DisplayName("Updated Date")]
            [Required(ErrorMessage = "Updated Date is required")]
    		public System.DateTime  UpdatedDate { get; set; }
    
    		    
    		/// <summary>
    		/// Version
    		/// </summary>        
    	//    [DisplayName("Version")]
            [MaxLength(8, ErrorMessage = "Version cannot be longer than 8 characters")]
    		public byte[]  Version { get; set; }
    
    		    
    	}
    //}
    
}
