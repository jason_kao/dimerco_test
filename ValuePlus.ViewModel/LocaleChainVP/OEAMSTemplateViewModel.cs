using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ValuePlus.ViewModel;



namespace ValuePlus.ViewModel.LocaleChainVP
{
    
    /// <summary>
    /// OEAMSTemplate class
    /// </summary>
    //[MetadataType(typeof(OEAMSTemplateViewModel))]
    //public  partial class OEAMSTemplate
    //{
    
    	/// <summary>
    	/// OEAMSTemplate Metadata class
    	/// </summary>
    	public   class OEAMSTemplateViewModel:ViewModelBase
    	{
    		    
    		/// <summary>
    		/// ID
    		/// </summary>        
    	//    [DisplayName("ID")]
            [Required(ErrorMessage = "ID is required")]
    		public int  ID { get; set; }
    
    		    
    		/// <summary>
    		/// Template ID
    		/// </summary>        
    	//    [DisplayName("Template ID")]
    		public Nullable<int>  TemplateID { get; set; }
    
    		    
    		/// <summary>
    		/// Template Name
    		/// </summary>        
    	//    [DisplayName("Template Name")]
            [MaxLength(50, ErrorMessage = "Template Name cannot be longer than 50 characters")]
    		public string  TemplateName { get; set; }
    
    		    
    		/// <summary>
    		/// SHPRName
    		/// </summary>        
    	//    [DisplayName("SHPRName")]
            [MaxLength(255, ErrorMessage = "SHPRName cannot be longer than 255 characters")]
    		public string  SHPRName { get; set; }
    
    		    
    		/// <summary>
    		/// SHPRAddress1
    		/// </summary>        
    	//    [DisplayName("SHPRAddress1")]
            [MaxLength(255, ErrorMessage = "SHPRAddress1 cannot be longer than 255 characters")]
    		public string  SHPRAddress1 { get; set; }
    
    		    
    		/// <summary>
    		/// SHPRAddress2
    		/// </summary>        
    	//    [DisplayName("SHPRAddress2")]
            [MaxLength(255, ErrorMessage = "SHPRAddress2 cannot be longer than 255 characters")]
    		public string  SHPRAddress2 { get; set; }
    
    		    
    		/// <summary>
    		/// SHPRCity
    		/// </summary>        
    	//    [DisplayName("SHPRCity")]
            [MaxLength(50, ErrorMessage = "SHPRCity cannot be longer than 50 characters")]
    		public string  SHPRCity { get; set; }
    
    		    
    		/// <summary>
    		/// SHPRState
    		/// </summary>        
    	//    [DisplayName("SHPRState")]
            [MaxLength(50, ErrorMessage = "SHPRState cannot be longer than 50 characters")]
    		public string  SHPRState { get; set; }
    
    		    
    		/// <summary>
    		/// SHPRZip
    		/// </summary>        
    	//    [DisplayName("SHPRZip")]
            [MaxLength(10, ErrorMessage = "SHPRZip cannot be longer than 10 characters")]
    		public string  SHPRZip { get; set; }
    
    		    
    		/// <summary>
    		/// SHPRPhone
    		/// </summary>        
    	//    [DisplayName("SHPRPhone")]
            [MaxLength(50, ErrorMessage = "SHPRPhone cannot be longer than 50 characters")]
    		public string  SHPRPhone { get; set; }
    
    		    
    		/// <summary>
    		/// SHPRCountry
    		/// </summary>        
    	//    [DisplayName("SHPRCountry")]
            [MaxLength(50, ErrorMessage = "SHPRCountry cannot be longer than 50 characters")]
    		public string  SHPRCountry { get; set; }
    
    		    
    		/// <summary>
    		/// SHPRContact Name
    		/// </summary>        
    	//    [DisplayName("SHPRContact Name")]
            [MaxLength(255, ErrorMessage = "SHPRContact Name cannot be longer than 255 characters")]
    		public string  SHPRContactName { get; set; }
    
    		    
    		/// <summary>
    		/// CNEEName
    		/// </summary>        
    	//    [DisplayName("CNEEName")]
            [MaxLength(255, ErrorMessage = "CNEEName cannot be longer than 255 characters")]
    		public string  CNEEName { get; set; }
    
    		    
    		/// <summary>
    		/// CNEEAddress1
    		/// </summary>        
    	//    [DisplayName("CNEEAddress1")]
            [MaxLength(255, ErrorMessage = "CNEEAddress1 cannot be longer than 255 characters")]
    		public string  CNEEAddress1 { get; set; }
    
    		    
    		/// <summary>
    		/// CNEEAddress2
    		/// </summary>        
    	//    [DisplayName("CNEEAddress2")]
            [MaxLength(255, ErrorMessage = "CNEEAddress2 cannot be longer than 255 characters")]
    		public string  CNEEAddress2 { get; set; }
    
    		    
    		/// <summary>
    		/// CNEECity
    		/// </summary>        
    	//    [DisplayName("CNEECity")]
            [MaxLength(50, ErrorMessage = "CNEECity cannot be longer than 50 characters")]
    		public string  CNEECity { get; set; }
    
    		    
    		/// <summary>
    		/// CNEEState
    		/// </summary>        
    	//    [DisplayName("CNEEState")]
            [MaxLength(50, ErrorMessage = "CNEEState cannot be longer than 50 characters")]
    		public string  CNEEState { get; set; }
    
    		    
    		/// <summary>
    		/// CNEEZip
    		/// </summary>        
    	//    [DisplayName("CNEEZip")]
            [MaxLength(10, ErrorMessage = "CNEEZip cannot be longer than 10 characters")]
    		public string  CNEEZip { get; set; }
    
    		    
    		/// <summary>
    		/// CNEEPhone
    		/// </summary>        
    	//    [DisplayName("CNEEPhone")]
            [MaxLength(50, ErrorMessage = "CNEEPhone cannot be longer than 50 characters")]
    		public string  CNEEPhone { get; set; }
    
    		    
    		/// <summary>
    		/// CNEECountry
    		/// </summary>        
    	//    [DisplayName("CNEECountry")]
            [MaxLength(50, ErrorMessage = "CNEECountry cannot be longer than 50 characters")]
    		public string  CNEECountry { get; set; }
    
    		    
    		/// <summary>
    		/// CNEEContact Name
    		/// </summary>        
    	//    [DisplayName("CNEEContact Name")]
            [MaxLength(255, ErrorMessage = "CNEEContact Name cannot be longer than 255 characters")]
    		public string  CNEEContactName { get; set; }
    
    		    
    		/// <summary>
    		/// NTFYName
    		/// </summary>        
    	//    [DisplayName("NTFYName")]
            [MaxLength(255, ErrorMessage = "NTFYName cannot be longer than 255 characters")]
    		public string  NTFYName { get; set; }
    
    		    
    		/// <summary>
    		/// NTFYAddress1
    		/// </summary>        
    	//    [DisplayName("NTFYAddress1")]
            [MaxLength(255, ErrorMessage = "NTFYAddress1 cannot be longer than 255 characters")]
    		public string  NTFYAddress1 { get; set; }
    
    		    
    		/// <summary>
    		/// NTFYAddress2
    		/// </summary>        
    	//    [DisplayName("NTFYAddress2")]
            [MaxLength(255, ErrorMessage = "NTFYAddress2 cannot be longer than 255 characters")]
    		public string  NTFYAddress2 { get; set; }
    
    		    
    		/// <summary>
    		/// NTFYCity
    		/// </summary>        
    	//    [DisplayName("NTFYCity")]
            [MaxLength(50, ErrorMessage = "NTFYCity cannot be longer than 50 characters")]
    		public string  NTFYCity { get; set; }
    
    		    
    		/// <summary>
    		/// NTFYState
    		/// </summary>        
    	//    [DisplayName("NTFYState")]
            [MaxLength(50, ErrorMessage = "NTFYState cannot be longer than 50 characters")]
    		public string  NTFYState { get; set; }
    
    		    
    		/// <summary>
    		/// NTFYZip
    		/// </summary>        
    	//    [DisplayName("NTFYZip")]
            [MaxLength(10, ErrorMessage = "NTFYZip cannot be longer than 10 characters")]
    		public string  NTFYZip { get; set; }
    
    		    
    		/// <summary>
    		/// NTFYPhone
    		/// </summary>        
    	//    [DisplayName("NTFYPhone")]
            [MaxLength(50, ErrorMessage = "NTFYPhone cannot be longer than 50 characters")]
    		public string  NTFYPhone { get; set; }
    
    		    
    		/// <summary>
    		/// NTFYCountry
    		/// </summary>        
    	//    [DisplayName("NTFYCountry")]
            [MaxLength(50, ErrorMessage = "NTFYCountry cannot be longer than 50 characters")]
    		public string  NTFYCountry { get; set; }
    
    		    
    		/// <summary>
    		/// NTFYContact Name
    		/// </summary>        
    	//    [DisplayName("NTFYContact Name")]
            [MaxLength(255, ErrorMessage = "NTFYContact Name cannot be longer than 255 characters")]
    		public string  NTFYContactName { get; set; }
    
    		    
    		/// <summary>
    		/// NTFY2 Name
    		/// </summary>        
    	//    [DisplayName("NTFY2 Name")]
            [MaxLength(255, ErrorMessage = "NTFY2 Name cannot be longer than 255 characters")]
    		public string  NTFY2Name { get; set; }
    
    		    
    		/// <summary>
    		/// NTFY2 Address1
    		/// </summary>        
    	//    [DisplayName("NTFY2 Address1")]
            [MaxLength(255, ErrorMessage = "NTFY2 Address1 cannot be longer than 255 characters")]
    		public string  NTFY2Address1 { get; set; }
    
    		    
    		/// <summary>
    		/// NTFY2 Address2
    		/// </summary>        
    	//    [DisplayName("NTFY2 Address2")]
            [MaxLength(255, ErrorMessage = "NTFY2 Address2 cannot be longer than 255 characters")]
    		public string  NTFY2Address2 { get; set; }
    
    		    
    		/// <summary>
    		/// NTFY2 City
    		/// </summary>        
    	//    [DisplayName("NTFY2 City")]
            [MaxLength(50, ErrorMessage = "NTFY2 City cannot be longer than 50 characters")]
    		public string  NTFY2City { get; set; }
    
    		    
    		/// <summary>
    		/// NTFY2 State
    		/// </summary>        
    	//    [DisplayName("NTFY2 State")]
            [MaxLength(50, ErrorMessage = "NTFY2 State cannot be longer than 50 characters")]
    		public string  NTFY2State { get; set; }
    
    		    
    		/// <summary>
    		/// NTFY2 Zip
    		/// </summary>        
    	//    [DisplayName("NTFY2 Zip")]
            [MaxLength(50, ErrorMessage = "NTFY2 Zip cannot be longer than 50 characters")]
    		public string  NTFY2Zip { get; set; }
    
    		    
    		/// <summary>
    		/// NTFY2 Phone
    		/// </summary>        
    	//    [DisplayName("NTFY2 Phone")]
            [MaxLength(50, ErrorMessage = "NTFY2 Phone cannot be longer than 50 characters")]
    		public string  NTFY2Phone { get; set; }
    
    		    
    		/// <summary>
    		/// NTFY2 Country
    		/// </summary>        
    	//    [DisplayName("NTFY2 Country")]
            [MaxLength(50, ErrorMessage = "NTFY2 Country cannot be longer than 50 characters")]
    		public string  NTFY2Country { get; set; }
    
    		    
    		/// <summary>
    		/// NTFY2 Contact Name
    		/// </summary>        
    	//    [DisplayName("NTFY2 Contact Name")]
            [MaxLength(255, ErrorMessage = "NTFY2 Contact Name cannot be longer than 255 characters")]
    		public string  NTFY2ContactName { get; set; }
    
    		    
    		/// <summary>
    		/// Is Void
    		/// </summary>        
    	//    [DisplayName("Is Void")]
            [MaxLength(1, ErrorMessage = "Is Void cannot be longer than 1 characters")]
    		public string  IsVoid { get; set; }
    
    		    
    	}
    //}
    
}
