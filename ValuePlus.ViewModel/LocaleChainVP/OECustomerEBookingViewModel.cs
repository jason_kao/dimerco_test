using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ValuePlus.ViewModel;



namespace ValuePlus.ViewModel.LocaleChainVP
{
    
    /// <summary>
    /// OECustomerEBooking class
    /// </summary>
    //[MetadataType(typeof(OECustomerEBookingViewModel))]
    //public  partial class OECustomerEBooking
    //{
    
    	/// <summary>
    	/// OECustomerEBooking Metadata class
    	/// </summary>
    	public   class OECustomerEBookingViewModel:ViewModelBase
    	{
    		    
    		/// <summary>
    		/// ID
    		/// </summary>        
    	//    [DisplayName("ID")]
            [Required(ErrorMessage = "ID is required")]
    		public int  ID { get; set; }
    
    		    
    		/// <summary>
    		/// Cust ID
    		/// </summary>        
    	//    [DisplayName("Cust ID")]
    		public Nullable<int>  CustID { get; set; }
    
    		    
    		/// <summary>
    		/// Src ID
    		/// </summary>        
    	//    [DisplayName("Src ID")]
    		public Nullable<int>  SrcID { get; set; }
    
    		    
    		/// <summary>
    		/// HBLID
    		/// </summary>        
    	//    [DisplayName("HBLID")]
    		public Nullable<int>  HBLID { get; set; }
    
    		    
    		/// <summary>
    		/// Station ID
    		/// </summary>        
    	//    [DisplayName("Station ID")]
            [MaxLength(10, ErrorMessage = "Station ID cannot be longer than 10 characters")]
    		public string  StationID { get; set; }
    
    		    
    		/// <summary>
    		/// Booking No
    		/// </summary>        
    	//    [DisplayName("Booking No")]
            [MaxLength(20, ErrorMessage = "Booking No cannot be longer than 20 characters")]
    		public string  BookingNo { get; set; }
    
    		    
    		/// <summary>
    		/// PLoading
    		/// </summary>        
    	//    [DisplayName("PLoading")]
    		public Nullable<int>  PLoading { get; set; }
    
    		    
    		/// <summary>
    		/// PLoading ETD
    		/// </summary>        
    	//    [DisplayName("PLoading ETD")]
    		public Nullable<System.DateTime>  PLoadingETD { get; set; }
    
    		    
    		/// <summary>
    		/// PDelivery
    		/// </summary>        
    	//    [DisplayName("PDelivery")]
    		public Nullable<int>  PDelivery { get; set; }
    
    		    
    		/// <summary>
    		/// Move Type
    		/// </summary>        
    	//    [DisplayName("Move Type")]
            [MaxLength(20, ErrorMessage = "Move Type cannot be longer than 20 characters")]
    		public string  MoveType { get; set; }
    
    		    
    		/// <summary>
    		/// Nature Of Goods Type
    		/// </summary>        
    	//    [DisplayName("Nature Of Goods Type")]
            [MaxLength(50, ErrorMessage = "Nature Of Goods Type cannot be longer than 50 characters")]
    		public string  NatureOfGoodsType { get; set; }
    
    		    
    		/// <summary>
    		/// OPType1
    		/// </summary>        
    	//    [DisplayName("OPType1")]
            [MaxLength(10, ErrorMessage = "OPType1 cannot be longer than 10 characters")]
    		public string  OPType1 { get; set; }
    
    		    
    		/// <summary>
    		/// OPType3
    		/// </summary>        
    	//    [DisplayName("OPType3")]
            [MaxLength(10, ErrorMessage = "OPType3 cannot be longer than 10 characters")]
    		public string  OPType3 { get; set; }
    
    		    
    		/// <summary>
    		/// OPType3 Detail
    		/// </summary>        
    	//    [DisplayName("OPType3 Detail")]
            [MaxLength(50, ErrorMessage = "OPType3 Detail cannot be longer than 50 characters")]
    		public string  OPType3Detail { get; set; }
    
    		    
    		/// <summary>
    		/// OPType3 Remark
    		/// </summary>        
    	//    [DisplayName("OPType3 Remark")]
            [MaxLength(50, ErrorMessage = "OPType3 Remark cannot be longer than 50 characters")]
    		public string  OPType3Remark { get; set; }
    
    		    
    		/// <summary>
    		/// OPType2
    		/// </summary>        
    	//    [DisplayName("OPType2")]
            [MaxLength(10, ErrorMessage = "OPType2 cannot be longer than 10 characters")]
    		public string  OPType2 { get; set; }
    
    		    
    		/// <summary>
    		/// OPType Detail
    		/// </summary>        
    	//    [DisplayName("OPType Detail")]
            [MaxLength(50, ErrorMessage = "OPType Detail cannot be longer than 50 characters")]
    		public string  OPTypeDetail { get; set; }
    
    		    
    		/// <summary>
    		/// Freight Pay Type
    		/// </summary>        
    	//    [DisplayName("Freight Pay Type")]
            [MaxLength(20, ErrorMessage = "Freight Pay Type cannot be longer than 20 characters")]
    		public string  FreightPayType { get; set; }
    
    		    
    		/// <summary>
    		/// Trade Type
    		/// </summary>        
    	//    [DisplayName("Trade Type")]
            [MaxLength(20, ErrorMessage = "Trade Type cannot be longer than 20 characters")]
    		public string  TradeType { get; set; }
    
    		    
    		/// <summary>
    		/// Client PIC
    		/// </summary>        
    	//    [DisplayName("Client PIC")]
            [MaxLength(50, ErrorMessage = "Client PIC cannot be longer than 50 characters")]
    		public string  ClientPIC { get; set; }
    
    		    
    		/// <summary>
    		/// Client Email
    		/// </summary>        
    	//    [DisplayName("Client Email")]
            [MaxLength(50, ErrorMessage = "Client Email cannot be longer than 50 characters")]
    		public string  ClientEmail { get; set; }
    
    		    
    		/// <summary>
    		/// Client Tel
    		/// </summary>        
    	//    [DisplayName("Client Tel")]
            [MaxLength(50, ErrorMessage = "Client Tel cannot be longer than 50 characters")]
    		public string  ClientTel { get; set; }
    
    		    
    		/// <summary>
    		/// Client Fax
    		/// </summary>        
    	//    [DisplayName("Client Fax")]
            [MaxLength(50, ErrorMessage = "Client Fax cannot be longer than 50 characters")]
    		public string  ClientFax { get; set; }
    
    		    
    		/// <summary>
    		/// Is Self Consol
    		/// </summary>        
    	//    [DisplayName("Is Self Consol")]
            [MaxLength(1, ErrorMessage = "Is Self Consol cannot be longer than 1 characters")]
    		public string  IsSelfConsol { get; set; }
    
    		    
    		/// <summary>
    		/// Consol Group
    		/// </summary>        
    	//    [DisplayName("Consol Group")]
    		public Nullable<int>  ConsolGroup { get; set; }
    
    		    
    		/// <summary>
    		/// Created By
    		/// </summary>        
    	//    [DisplayName("Created By")]
            [MaxLength(50, ErrorMessage = "Created By cannot be longer than 50 characters")]
    		public string  CreatedBy { get; set; }
    
    		    
    		/// <summary>
    		/// Created Date
    		/// </summary>        
    	//    [DisplayName("Created Date")]
    		public Nullable<System.DateTime>  CreatedDate { get; set; }
    
    		    
    		/// <summary>
    		/// Updated By
    		/// </summary>        
    	//    [DisplayName("Updated By")]
            [MaxLength(50, ErrorMessage = "Updated By cannot be longer than 50 characters")]
    		public string  UpdatedBy { get; set; }
    
    		    
    		/// <summary>
    		/// Updated Date
    		/// </summary>        
    	//    [DisplayName("Updated Date")]
    		public Nullable<System.DateTime>  UpdatedDate { get; set; }
    
    		    
    		/// <summary>
    		/// HBLType
    		/// </summary>        
    	//    [DisplayName("HBLType")]
            [MaxLength(5, ErrorMessage = "HBLType cannot be longer than 5 characters")]
    		public string  HBLType { get; set; }
    
    		    
    		/// <summary>
    		/// Cargo Ready Date
    		/// </summary>        
    	//    [DisplayName("Cargo Ready Date")]
    		public Nullable<System.DateTime>  CargoReadyDate { get; set; }
    
    		    
    	}
    //}
    
}
