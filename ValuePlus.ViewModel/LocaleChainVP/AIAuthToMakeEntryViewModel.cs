using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ValuePlus.ViewModel;



namespace ValuePlus.ViewModel.LocaleChainVP
{
    
    /// <summary>
    /// AIAuthToMakeEntry class
    /// </summary>
    //[MetadataType(typeof(AIAuthToMakeEntryViewModel))]
    //public  partial class AIAuthToMakeEntry
    //{
    
    	/// <summary>
    	/// AIAuthToMakeEntry Metadata class
    	/// </summary>
    	public   class AIAuthToMakeEntryViewModel:ViewModelBase
    	{
    		    
    		/// <summary>
    		/// ID
    		/// </summary>        
    	//    [DisplayName("ID")]
            [Required(ErrorMessage = "ID is required")]
    		public int  ID { get; set; }
    
    		    
    		/// <summary>
    		/// HAWBID
    		/// </summary>        
    	//    [DisplayName("HAWBID")]
            [Required(ErrorMessage = "HAWBID is required")]
    		public int  HAWBID { get; set; }
    
    		    
    		/// <summary>
    		/// Created By
    		/// </summary>        
    	//    [DisplayName("Created By")]
            [Required(ErrorMessage = "Created By is required")]
            [MaxLength(6, ErrorMessage = "Created By cannot be longer than 6 characters")]
    		public string  CreatedBy { get; set; }
    
    		    
    		/// <summary>
    		/// Created Date
    		/// </summary>        
    	//    [DisplayName("Created Date")]
            [Required(ErrorMessage = "Created Date is required")]
    		public System.DateTime  CreatedDate { get; set; }
    
    		    
    		/// <summary>
    		/// Updated By
    		/// </summary>        
    	//    [DisplayName("Updated By")]
            [Required(ErrorMessage = "Updated By is required")]
            [MaxLength(6, ErrorMessage = "Updated By cannot be longer than 6 characters")]
    		public string  UpdatedBy { get; set; }
    
    		    
    		/// <summary>
    		/// Updated Date
    		/// </summary>        
    	//    [DisplayName("Updated Date")]
            [Required(ErrorMessage = "Updated Date is required")]
    		public System.DateTime  UpdatedDate { get; set; }
    
    		    
    		/// <summary>
    		/// Import On
    		/// </summary>        
    	//    [DisplayName("Import On")]
            [MaxLength(20, ErrorMessage = "Import On cannot be longer than 20 characters")]
    		public string  ImportOn { get; set; }
    
    		    
    		/// <summary>
    		/// VIA
    		/// </summary>        
    	//    [DisplayName("VIA")]
            [MaxLength(100, ErrorMessage = "VIA cannot be longer than 100 characters")]
    		public string  VIA { get; set; }
    
    		    
    		/// <summary>
    		/// From
    		/// </summary>        
    	//    [DisplayName("From")]
            [MaxLength(20, ErrorMessage = "From cannot be longer than 20 characters")]
    		public string  From { get; set; }
    
    		    
    		/// <summary>
    		/// Flight NO
    		/// </summary>        
    	//    [DisplayName("Flight NO")]
            [MaxLength(20, ErrorMessage = "Flight NO cannot be longer than 20 characters")]
    		public string  FlightNO { get; set; }
    
    		    
    		/// <summary>
    		/// MAWBNO
    		/// </summary>        
    	//    [DisplayName("MAWBNO")]
            [MaxLength(20, ErrorMessage = "MAWBNO cannot be longer than 20 characters")]
    		public string  MAWBNO { get; set; }
    
    		    
    		/// <summary>
    		/// ITNo
    		/// </summary>        
    	//    [DisplayName("ITNo")]
            [MaxLength(20, ErrorMessage = "ITNo cannot be longer than 20 characters")]
    		public string  ITNo { get; set; }
    
    		    
    		/// <summary>
    		/// Entry Port
    		/// </summary>        
    	//    [DisplayName("Entry Port")]
            [MaxLength(100, ErrorMessage = "Entry Port cannot be longer than 100 characters")]
    		public string  EntryPort { get; set; }
    
    		    
    		/// <summary>
    		/// Lot No
    		/// </summary>        
    	//    [DisplayName("Lot No")]
            [MaxLength(20, ErrorMessage = "Lot No cannot be longer than 20 characters")]
    		public string  LotNo { get; set; }
    
    		    
    		/// <summary>
    		/// HAWBNo
    		/// </summary>        
    	//    [DisplayName("HAWBNo")]
            [MaxLength(20, ErrorMessage = "HAWBNo cannot be longer than 20 characters")]
    		public string  HAWBNo { get; set; }
    
    		    
    		/// <summary>
    		/// Freight Loc
    		/// </summary>        
    	//    [DisplayName("Freight Loc")]
            [MaxLength(255, ErrorMessage = "Freight Loc cannot be longer than 255 characters")]
    		public string  FreightLoc { get; set; }
    
    		    
    		/// <summary>
    		/// HAWB
    		/// </summary>        
    	//    [DisplayName("HAWB")]
            [MaxLength(100, ErrorMessage = "HAWB cannot be longer than 100 characters")]
    		public string  HAWB { get; set; }
    
    		    
    		/// <summary>
    		/// Sub HAWB
    		/// </summary>        
    	//    [DisplayName("Sub HAWB")]
            [MaxLength(100, ErrorMessage = "Sub HAWB cannot be longer than 100 characters")]
    		public string  SubHAWB { get; set; }
    
    		    
    		/// <summary>
    		/// DESC
    		/// </summary>        
    	//    [DisplayName("DESC")]
            [MaxLength(255, ErrorMessage = "DESC cannot be longer than 255 characters")]
    		public string  DESC { get; set; }
    
    		    
    		/// <summary>
    		/// PCS
    		/// </summary>        
    	//    [DisplayName("PCS")]
            [MaxLength(100, ErrorMessage = "PCS cannot be longer than 100 characters")]
    		public string  PCS { get; set; }
    
    		    
    		/// <summary>
    		/// Weight
    		/// </summary>        
    	//    [DisplayName("Weight")]
            [MaxLength(100, ErrorMessage = "Weight cannot be longer than 100 characters")]
    		public string  Weight { get; set; }
    
    		    
    		/// <summary>
    		/// Due To
    		/// </summary>        
    	//    [DisplayName("Due To")]
            [MaxLength(100, ErrorMessage = "Due To cannot be longer than 100 characters")]
    		public string  DueTo { get; set; }
    
    		    
    		/// <summary>
    		/// Storage
    		/// </summary>        
    	//    [DisplayName("Storage")]
            [MaxLength(100, ErrorMessage = "Storage cannot be longer than 100 characters")]
    		public string  Storage { get; set; }
    
    		    
    		/// <summary>
    		/// Consignee
    		/// </summary>        
    	//    [DisplayName("Consignee")]
            [MaxLength(100, ErrorMessage = "Consignee cannot be longer than 100 characters")]
    		public string  Consignee { get; set; }
    
    		    
    		/// <summary>
    		/// Authorise To
    		/// </summary>        
    	//    [DisplayName("Authorise To")]
            [MaxLength(100, ErrorMessage = "Authorise To cannot be longer than 100 characters")]
    		public string  AuthoriseTo { get; set; }
    
    		    
    		/// <summary>
    		/// Attorney In Fact
    		/// </summary>        
    	//    [DisplayName("Attorney In Fact")]
            [MaxLength(100, ErrorMessage = "Attorney In Fact cannot be longer than 100 characters")]
    		public string  AttorneyInFact { get; set; }
    
    		    
    		/// <summary>
    		/// Broker
    		/// </summary>        
    	//    [DisplayName("Broker")]
            [MaxLength(100, ErrorMessage = "Broker cannot be longer than 100 characters")]
    		public string  Broker { get; set; }
    
    		    
    		/// <summary>
    		/// MCHEDImport
    		/// </summary>        
    	//    [DisplayName("MCHEDImport")]
            [MaxLength(100, ErrorMessage = "MCHEDImport cannot be longer than 100 characters")]
    		public string  MCHEDImport { get; set; }
    
    		    
    		/// <summary>
    		/// ITDate
    		/// </summary>        
    	//    [DisplayName("ITDate")]
            [MaxLength(20, ErrorMessage = "ITDate cannot be longer than 20 characters")]
    		public string  ITDate { get; set; }
    
    		    
    		/// <summary>
    		/// Version
    		/// </summary>        
    	//    [DisplayName("Version")]
            [MaxLength(8, ErrorMessage = "Version cannot be longer than 8 characters")]
    		public byte[]  Version { get; set; }
    
    		    
    	}
    //}
    
}
