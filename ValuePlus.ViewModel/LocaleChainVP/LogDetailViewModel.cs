using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ValuePlus.ViewModel;



namespace ValuePlus.ViewModel.LocaleChainVP
{
    
    /// <summary>
    /// LogDetail class
    /// </summary>
    //[MetadataType(typeof(LogDetailViewModel))]
    //public  partial class LogDetail
    //{
    
    	/// <summary>
    	/// LogDetail Metadata class
    	/// </summary>
    	public   class LogDetailViewModel:ViewModelBase
    	{
    		    
    		/// <summary>
    		/// Id
    		/// </summary>        
    	//    [DisplayName("Id")]
            [Required(ErrorMessage = "Id is required")]
    		public int  Id { get; set; }
    
    		    
    		/// <summary>
    		/// Station Id
    		/// </summary>        
    	//    [DisplayName("Station Id")]
            [Required(ErrorMessage = "Station Id is required")]
            [MaxLength(50, ErrorMessage = "Station Id cannot be longer than 50 characters")]
    		public string  StationId { get; set; }
    
    		    
    		/// <summary>
    		/// Page Id
    		/// </summary>        
    	//    [DisplayName("Page Id")]
            [Required(ErrorMessage = "Page Id is required")]
    		public System.Guid  PageId { get; set; }
    
    		    
    		/// <summary>
    		/// Url
    		/// </summary>        
    	//    [DisplayName("Url")]
            [Required(ErrorMessage = "Url is required")]
    		public string  Url { get; set; }
    
    		    
    		/// <summary>
    		/// Type
    		/// </summary>        
    	//    [DisplayName("Type")]
            [Required(ErrorMessage = "Type is required")]
            [MaxLength(50, ErrorMessage = "Type cannot be longer than 50 characters")]
    		public string  Type { get; set; }
    
    		    
    		/// <summary>
    		/// Load Seconds
    		/// </summary>        
    	//    [DisplayName("Load Seconds")]
            [Required(ErrorMessage = "Load Seconds is required")]
    		public double  LoadSeconds { get; set; }
    
    		    
    		/// <summary>
    		/// Create Time
    		/// </summary>        
    	//    [DisplayName("Create Time")]
    		public Nullable<System.DateTime>  CreateTime { get; set; }
    
    		    
    	}
    //}
    
}
