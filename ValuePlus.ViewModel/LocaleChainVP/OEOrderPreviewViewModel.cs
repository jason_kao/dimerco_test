using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ValuePlus.ViewModel;



namespace ValuePlus.ViewModel.LocaleChainVP
{
    
    /// <summary>
    /// OEOrderPreview class
    /// </summary>
    //[MetadataType(typeof(OEOrderPreviewViewModel))]
    //public  partial class OEOrderPreview
    //{
    
    	/// <summary>
    	/// OEOrderPreview Metadata class
    	/// </summary>
    	public   class OEOrderPreviewViewModel:ViewModelBase
    	{
    		    
    		/// <summary>
    		/// ID
    		/// </summary>        
    	//    [DisplayName("ID")]
            [Required(ErrorMessage = "ID is required")]
    		public int  ID { get; set; }
    
    		    
    		/// <summary>
    		/// Station ID
    		/// </summary>        
    	//    [DisplayName("Station ID")]
            [MaxLength(5, ErrorMessage = "Station ID cannot be longer than 5 characters")]
    		public string  StationID { get; set; }
    
    		    
    		/// <summary>
    		/// HBLID
    		/// </summary>        
    	//    [DisplayName("HBLID")]
    		public Nullable<int>  HBLID { get; set; }
    
    		    
    		/// <summary>
    		/// Pickup Type
    		/// </summary>        
    	//    [DisplayName("Pickup Type")]
            [MaxLength(10, ErrorMessage = "Pickup Type cannot be longer than 10 characters")]
    		public string  PickupType { get; set; }
    
    		    
    		/// <summary>
    		/// Pickup From
    		/// </summary>        
    	//    [DisplayName("Pickup From")]
    		public Nullable<int>  PickupFrom { get; set; }
    
    		    
    		/// <summary>
    		/// Pickup Address
    		/// </summary>        
    	//    [DisplayName("Pickup Address")]
            [MaxLength(255, ErrorMessage = "Pickup Address cannot be longer than 255 characters")]
    		public string  PickupAddress { get; set; }
    
    		    
    		/// <summary>
    		/// Pickup Avail
    		/// </summary>        
    	//    [DisplayName("Pickup Avail")]
    		public Nullable<System.DateTime>  PickupAvail { get; set; }
    
    		    
    		/// <summary>
    		/// Pickup Actual
    		/// </summary>        
    	//    [DisplayName("Pickup Actual")]
    		public Nullable<System.DateTime>  PickupActual { get; set; }
    
    		    
    		/// <summary>
    		/// Deliver To
    		/// </summary>        
    	//    [DisplayName("Deliver To")]
    		public Nullable<int>  DeliverTo { get; set; }
    
    		    
    		/// <summary>
    		/// Deliver Address
    		/// </summary>        
    	//    [DisplayName("Deliver Address")]
            [MaxLength(255, ErrorMessage = "Deliver Address cannot be longer than 255 characters")]
    		public string  DeliverAddress { get; set; }
    
    		    
    		/// <summary>
    		/// Deliver ETD
    		/// </summary>        
    	//    [DisplayName("Deliver ETD")]
    		public Nullable<System.DateTime>  DeliverETD { get; set; }
    
    		    
    		/// <summary>
    		/// Deliver ATD
    		/// </summary>        
    	//    [DisplayName("Deliver ATD")]
    		public Nullable<System.DateTime>  DeliverATD { get; set; }
    
    		    
    		/// <summary>
    		/// Contact Person
    		/// </summary>        
    	//    [DisplayName("Contact Person")]
            [MaxLength(100, ErrorMessage = "Contact Person cannot be longer than 100 characters")]
    		public string  ContactPerson { get; set; }
    
    		    
    		/// <summary>
    		/// Trucker
    		/// </summary>        
    	//    [DisplayName("Trucker")]
            [MaxLength(60, ErrorMessage = "Trucker cannot be longer than 60 characters")]
    		public string  Trucker { get; set; }
    
    		    
    		/// <summary>
    		/// Remarks
    		/// </summary>        
    	//    [DisplayName("Remarks")]
            [MaxLength(255, ErrorMessage = "Remarks cannot be longer than 255 characters")]
    		public string  Remarks { get; set; }
    
    		    
    		/// <summary>
    		/// PCS
    		/// </summary>        
    	//    [DisplayName("PCS")]
            [MaxLength(20, ErrorMessage = "PCS cannot be longer than 20 characters")]
    		public string  PCS { get; set; }
    
    		    
    		/// <summary>
    		/// PCSUOM
    		/// </summary>        
    	//    [DisplayName("PCSUOM")]
            [MaxLength(10, ErrorMessage = "PCSUOM cannot be longer than 10 characters")]
    		public string  PCSUOM { get; set; }
    
    		    
    		/// <summary>
    		/// WT
    		/// </summary>        
    	//    [DisplayName("WT")]
            [MaxLength(20, ErrorMessage = "WT cannot be longer than 20 characters")]
    		public string  WT { get; set; }
    
    		    
    		/// <summary>
    		/// WTUOM
    		/// </summary>        
    	//    [DisplayName("WTUOM")]
            [MaxLength(10, ErrorMessage = "WTUOM cannot be longer than 10 characters")]
    		public string  WTUOM { get; set; }
    
    		    
    		/// <summary>
    		/// CBM
    		/// </summary>        
    	//    [DisplayName("CBM")]
    		public Nullable<double>  CBM { get; set; }
    
    		    
    		/// <summary>
    		/// CBMUOM
    		/// </summary>        
    	//    [DisplayName("CBMUOM")]
            [MaxLength(10, ErrorMessage = "CBMUOM cannot be longer than 10 characters")]
    		public string  CBMUOM { get; set; }
    
    		    
    		/// <summary>
    		/// Marks
    		/// </summary>        
    	//    [DisplayName("Marks")]
            [MaxLength(255, ErrorMessage = "Marks cannot be longer than 255 characters")]
    		public string  Marks { get; set; }
    
    		    
    		/// <summary>
    		/// Description
    		/// </summary>        
    	//    [DisplayName("Description")]
            [MaxLength(255, ErrorMessage = "Description cannot be longer than 255 characters")]
    		public string  Description { get; set; }
    
    		    
    		/// <summary>
    		/// Need INV
    		/// </summary>        
    	//    [DisplayName("Need INV")]
            [MaxLength(1, ErrorMessage = "Need INV cannot be longer than 1 characters")]
    		public string  NeedINV { get; set; }
    
    		    
    		/// <summary>
    		/// Need PL
    		/// </summary>        
    	//    [DisplayName("Need PL")]
            [MaxLength(1, ErrorMessage = "Need PL cannot be longer than 1 characters")]
    		public string  NeedPL { get; set; }
    
    		    
    		/// <summary>
    		/// Created By
    		/// </summary>        
    	//    [DisplayName("Created By")]
            [MaxLength(50, ErrorMessage = "Created By cannot be longer than 50 characters")]
    		public string  CreatedBy { get; set; }
    
    		    
    		/// <summary>
    		/// Created DT
    		/// </summary>        
    	//    [DisplayName("Created DT")]
    		public Nullable<System.DateTime>  CreatedDT { get; set; }
    
    		    
    		/// <summary>
    		/// Updated By
    		/// </summary>        
    	//    [DisplayName("Updated By")]
            [MaxLength(50, ErrorMessage = "Updated By cannot be longer than 50 characters")]
    		public string  UpdatedBy { get; set; }
    
    		    
    		/// <summary>
    		/// Updated DT
    		/// </summary>        
    	//    [DisplayName("Updated DT")]
    		public Nullable<System.DateTime>  UpdatedDT { get; set; }
    
    		    
    		/// <summary>
    		/// Version
    		/// </summary>        
    	//    [DisplayName("Version")]
            [Required(ErrorMessage = "Version is required")]
            [MaxLength(8, ErrorMessage = "Version cannot be longer than 8 characters")]
    		public byte[]  Version { get; set; }
    
    		    
    		/// <summary>
    		/// Contact Number
    		/// </summary>        
    	//    [DisplayName("Contact Number")]
            [MaxLength(80, ErrorMessage = "Contact Number cannot be longer than 80 characters")]
    		public string  ContactNumber { get; set; }
    
    		    
    		/// <summary>
    		/// SONO
    		/// </summary>        
    	//    [DisplayName("SONO")]
            [MaxLength(60, ErrorMessage = "SONO cannot be longer than 60 characters")]
    		public string  SONO { get; set; }
    
    		    
    		/// <summary>
    		/// Delivery Contact Person
    		/// </summary>        
    	//    [DisplayName("Delivery Contact Person")]
            [MaxLength(100, ErrorMessage = "Delivery Contact Person cannot be longer than 100 characters")]
    		public string  DeliveryContactPerson { get; set; }
    
    		    
    		/// <summary>
    		/// Delivery Contact Number
    		/// </summary>        
    	//    [DisplayName("Delivery Contact Number")]
            [MaxLength(80, ErrorMessage = "Delivery Contact Number cannot be longer than 80 characters")]
    		public string  DeliveryContactNumber { get; set; }
    
    		    
    		/// <summary>
    		/// Delivery Trucker
    		/// </summary>        
    	//    [DisplayName("Delivery Trucker")]
            [MaxLength(60, ErrorMessage = "Delivery Trucker cannot be longer than 60 characters")]
    		public string  DeliveryTrucker { get; set; }
    
    		    
    		/// <summary>
    		/// Delivery Need INV
    		/// </summary>        
    	//    [DisplayName("Delivery Need INV")]
            [MaxLength(1, ErrorMessage = "Delivery Need INV cannot be longer than 1 characters")]
    		public string  DeliveryNeedINV { get; set; }
    
    		    
    		/// <summary>
    		/// Delivery Need PL
    		/// </summary>        
    	//    [DisplayName("Delivery Need PL")]
            [MaxLength(1, ErrorMessage = "Delivery Need PL cannot be longer than 1 characters")]
    		public string  DeliveryNeedPL { get; set; }
    
    		    
    		/// <summary>
    		/// Delivery Remarks
    		/// </summary>        
    	//    [DisplayName("Delivery Remarks")]
            [MaxLength(255, ErrorMessage = "Delivery Remarks cannot be longer than 255 characters")]
    		public string  DeliveryRemarks { get; set; }
    
    		    
    		/// <summary>
    		/// SOList Type
    		/// </summary>        
    	//    [DisplayName("SOList Type")]
            [MaxLength(20, ErrorMessage = "SOList Type cannot be longer than 20 characters")]
    		public string  SOListType { get; set; }
    
    		    
    		/// <summary>
    		/// Order No
    		/// </summary>        
    	//    [DisplayName("Order No")]
            [MaxLength(50, ErrorMessage = "Order No cannot be longer than 50 characters")]
    		public string  OrderNo { get; set; }
    
    		    
    		/// <summary>
    		/// Order ID
    		/// </summary>        
    	//    [DisplayName("Order ID")]
    		public Nullable<int>  OrderID { get; set; }
    
    		    
    		/// <summary>
    		/// Pickup From Name
    		/// </summary>        
    	//    [DisplayName("Pickup From Name")]
            [MaxLength(50, ErrorMessage = "Pickup From Name cannot be longer than 50 characters")]
    		public string  PickupFromName { get; set; }
    
    		    
    		/// <summary>
    		/// Deliver To Name
    		/// </summary>        
    	//    [DisplayName("Deliver To Name")]
            [MaxLength(50, ErrorMessage = "Deliver To Name cannot be longer than 50 characters")]
    		public string  DeliverToName { get; set; }
    
    		    
    		/// <summary>
    		/// PDelivery
    		/// </summary>        
    	//    [DisplayName("PDelivery")]
            [MaxLength(100, ErrorMessage = "PDelivery cannot be longer than 100 characters")]
    		public string  PDelivery { get; set; }
    
    		    
    		/// <summary>
    		/// PDischarge
    		/// </summary>        
    	//    [DisplayName("PDischarge")]
            [MaxLength(100, ErrorMessage = "PDischarge cannot be longer than 100 characters")]
    		public string  PDischarge { get; set; }
    
    		    
    		/// <summary>
    		/// MBLNO
    		/// </summary>        
    	//    [DisplayName("MBLNO")]
            [MaxLength(50, ErrorMessage = "MBLNO cannot be longer than 50 characters")]
    		public string  MBLNO { get; set; }
    
    		    
    		/// <summary>
    		/// HBLNO
    		/// </summary>        
    	//    [DisplayName("HBLNO")]
            [MaxLength(50, ErrorMessage = "HBLNO cannot be longer than 50 characters")]
    		public string  HBLNO { get; set; }
    
    		    
    		/// <summary>
    		/// Ocean Vessel
    		/// </summary>        
    	//    [DisplayName("Ocean Vessel")]
            [MaxLength(50, ErrorMessage = "Ocean Vessel cannot be longer than 50 characters")]
    		public string  OceanVessel { get; set; }
    
    		    
    		/// <summary>
    		/// Ocean Voyage
    		/// </summary>        
    	//    [DisplayName("Ocean Voyage")]
            [MaxLength(50, ErrorMessage = "Ocean Voyage cannot be longer than 50 characters")]
    		public string  OceanVoyage { get; set; }
    
    		    
    		/// <summary>
    		/// Issue By
    		/// </summary>        
    	//    [DisplayName("Issue By")]
            [MaxLength(50, ErrorMessage = "Issue By cannot be longer than 50 characters")]
    		public string  IssueBy { get; set; }
    
    		    
    		/// <summary>
    		/// Issue Date
    		/// </summary>        
    	//    [DisplayName("Issue Date")]
    		public Nullable<System.DateTime>  IssueDate { get; set; }
    
    		    
    		/// <summary>
    		/// Station Name
    		/// </summary>        
    	//    [DisplayName("Station Name")]
            [MaxLength(50, ErrorMessage = "Station Name cannot be longer than 50 characters")]
    		public string  StationName { get; set; }
    
    		    
    		/// <summary>
    		/// Station Address
    		/// </summary>        
    	//    [DisplayName("Station Address")]
            [MaxLength(255, ErrorMessage = "Station Address cannot be longer than 255 characters")]
    		public string  StationAddress { get; set; }
    
    		    
    		/// <summary>
    		/// Station Phone
    		/// </summary>        
    	//    [DisplayName("Station Phone")]
            [MaxLength(50, ErrorMessage = "Station Phone cannot be longer than 50 characters")]
    		public string  StationPhone { get; set; }
    
    		    
    		/// <summary>
    		/// Station Fax
    		/// </summary>        
    	//    [DisplayName("Station Fax")]
            [MaxLength(50, ErrorMessage = "Station Fax cannot be longer than 50 characters")]
    		public string  StationFax { get; set; }
    
    		    
    		/// <summary>
    		/// CTNRNo
    		/// </summary>        
    	//    [DisplayName("CTNRNo")]
            [MaxLength(50, ErrorMessage = "CTNRNo cannot be longer than 50 characters")]
    		public string  CTNRNo { get; set; }
    
    		    
    		/// <summary>
    		/// Seal No
    		/// </summary>        
    	//    [DisplayName("Seal No")]
            [MaxLength(50, ErrorMessage = "Seal No cannot be longer than 50 characters")]
    		public string  SealNo { get; set; }
    
    		    
    		/// <summary>
    		/// Need Booking
    		/// </summary>        
    	//    [DisplayName("Need Booking")]
            [MaxLength(1, ErrorMessage = "Need Booking cannot be longer than 1 characters")]
    		public string  NeedBooking { get; set; }
    
    		    
    		/// <summary>
    		/// Delivery Need Booking
    		/// </summary>        
    	//    [DisplayName("Delivery Need Booking")]
            [MaxLength(1, ErrorMessage = "Delivery Need Booking cannot be longer than 1 characters")]
    		public string  DeliveryNeedBooking { get; set; }
    
    		    
    		/// <summary>
    		/// SOList Count
    		/// </summary>        
    	//    [DisplayName("SOList Count")]
    		public Nullable<int>  SOListCount { get; set; }
    
    		    
    		/// <summary>
    		/// Lotno
    		/// </summary>        
    	//    [DisplayName("Lotno")]
            [MaxLength(20, ErrorMessage = "Lotno cannot be longer than 20 characters")]
    		public string  Lotno { get; set; }
    
    		    
    		/// <summary>
    		/// Mode
    		/// </summary>        
    	//    [DisplayName("Mode")]
            [MaxLength(10, ErrorMessage = "Mode cannot be longer than 10 characters")]
    		public string  Mode { get; set; }
    
    		    
    		/// <summary>
    		/// Pickup From TWO
    		/// </summary>        
    	//    [DisplayName("Pickup From TWO")]
    		public Nullable<int>  PickupFromTWO { get; set; }
    
    		    
    		/// <summary>
    		/// Pickup From Name TWO
    		/// </summary>        
    	//    [DisplayName("Pickup From Name TWO")]
            [MaxLength(100, ErrorMessage = "Pickup From Name TWO cannot be longer than 100 characters")]
    		public string  PickupFromNameTWO { get; set; }
    
    		    
    		/// <summary>
    		/// Pickup Address TWO
    		/// </summary>        
    	//    [DisplayName("Pickup Address TWO")]
            [MaxLength(255, ErrorMessage = "Pickup Address TWO cannot be longer than 255 characters")]
    		public string  PickupAddressTWO { get; set; }
    
    		    
    		/// <summary>
    		/// Pickup Avail TWO
    		/// </summary>        
    	//    [DisplayName("Pickup Avail TWO")]
    		public Nullable<System.DateTime>  PickupAvailTWO { get; set; }
    
    		    
    		/// <summary>
    		/// Pickup Actual TWO
    		/// </summary>        
    	//    [DisplayName("Pickup Actual TWO")]
    		public Nullable<System.DateTime>  PickupActualTWO { get; set; }
    
    		    
    		/// <summary>
    		/// Pickup Contact Person TWO
    		/// </summary>        
    	//    [DisplayName("Pickup Contact Person TWO")]
            [MaxLength(100, ErrorMessage = "Pickup Contact Person TWO cannot be longer than 100 characters")]
    		public string  PickupContactPersonTWO { get; set; }
    
    		    
    		/// <summary>
    		/// Pickup Contact Number TWO
    		/// </summary>        
    	//    [DisplayName("Pickup Contact Number TWO")]
            [MaxLength(80, ErrorMessage = "Pickup Contact Number TWO cannot be longer than 80 characters")]
    		public string  PickupContactNumberTWO { get; set; }
    
    		    
    		/// <summary>
    		/// Pickup Trucker TWO
    		/// </summary>        
    	//    [DisplayName("Pickup Trucker TWO")]
            [MaxLength(60, ErrorMessage = "Pickup Trucker TWO cannot be longer than 60 characters")]
    		public string  PickupTruckerTWO { get; set; }
    
    		    
    		/// <summary>
    		/// Pickup Need INVTWO
    		/// </summary>        
    	//    [DisplayName("Pickup Need INVTWO")]
            [MaxLength(1, ErrorMessage = "Pickup Need INVTWO cannot be longer than 1 characters")]
    		public string  PickupNeedINVTWO { get; set; }
    
    		    
    		/// <summary>
    		/// Pickup Need PLTWO
    		/// </summary>        
    	//    [DisplayName("Pickup Need PLTWO")]
            [MaxLength(1, ErrorMessage = "Pickup Need PLTWO cannot be longer than 1 characters")]
    		public string  PickupNeedPLTWO { get; set; }
    
    		    
    		/// <summary>
    		/// Pickup Need Booking TWO
    		/// </summary>        
    	//    [DisplayName("Pickup Need Booking TWO")]
            [MaxLength(1, ErrorMessage = "Pickup Need Booking TWO cannot be longer than 1 characters")]
    		public string  PickupNeedBookingTWO { get; set; }
    
    		    
    		/// <summary>
    		/// Pickup Remarks TWO
    		/// </summary>        
    	//    [DisplayName("Pickup Remarks TWO")]
            [MaxLength(255, ErrorMessage = "Pickup Remarks TWO cannot be longer than 255 characters")]
    		public string  PickupRemarksTWO { get; set; }
    
    		    
    		/// <summary>
    		/// Delivery To TWO
    		/// </summary>        
    	//    [DisplayName("Delivery To TWO")]
    		public Nullable<int>  DeliveryToTWO { get; set; }
    
    		    
    		/// <summary>
    		/// Delivery To Name TWO
    		/// </summary>        
    	//    [DisplayName("Delivery To Name TWO")]
            [MaxLength(100, ErrorMessage = "Delivery To Name TWO cannot be longer than 100 characters")]
    		public string  DeliveryToNameTWO { get; set; }
    
    		    
    		/// <summary>
    		/// Delivery Address TWO
    		/// </summary>        
    	//    [DisplayName("Delivery Address TWO")]
            [MaxLength(255, ErrorMessage = "Delivery Address TWO cannot be longer than 255 characters")]
    		public string  DeliveryAddressTWO { get; set; }
    
    		    
    		/// <summary>
    		/// Delivery Avail TWO
    		/// </summary>        
    	//    [DisplayName("Delivery Avail TWO")]
    		public Nullable<System.DateTime>  DeliveryAvailTWO { get; set; }
    
    		    
    		/// <summary>
    		/// Delivery Actual TWO
    		/// </summary>        
    	//    [DisplayName("Delivery Actual TWO")]
    		public Nullable<System.DateTime>  DeliveryActualTWO { get; set; }
    
    		    
    		/// <summary>
    		/// Delivery Contact Person TWO
    		/// </summary>        
    	//    [DisplayName("Delivery Contact Person TWO")]
            [MaxLength(100, ErrorMessage = "Delivery Contact Person TWO cannot be longer than 100 characters")]
    		public string  DeliveryContactPersonTWO { get; set; }
    
    		    
    		/// <summary>
    		/// Delivery Contact Number TWO
    		/// </summary>        
    	//    [DisplayName("Delivery Contact Number TWO")]
            [MaxLength(80, ErrorMessage = "Delivery Contact Number TWO cannot be longer than 80 characters")]
    		public string  DeliveryContactNumberTWO { get; set; }
    
    		    
    		/// <summary>
    		/// Delivery Trucker TWO
    		/// </summary>        
    	//    [DisplayName("Delivery Trucker TWO")]
            [MaxLength(60, ErrorMessage = "Delivery Trucker TWO cannot be longer than 60 characters")]
    		public string  DeliveryTruckerTWO { get; set; }
    
    		    
    		/// <summary>
    		/// Delivery Need INVTWO
    		/// </summary>        
    	//    [DisplayName("Delivery Need INVTWO")]
            [MaxLength(1, ErrorMessage = "Delivery Need INVTWO cannot be longer than 1 characters")]
    		public string  DeliveryNeedINVTWO { get; set; }
    
    		    
    		/// <summary>
    		/// Delivery Need PLTWO
    		/// </summary>        
    	//    [DisplayName("Delivery Need PLTWO")]
            [MaxLength(1, ErrorMessage = "Delivery Need PLTWO cannot be longer than 1 characters")]
    		public string  DeliveryNeedPLTWO { get; set; }
    
    		    
    		/// <summary>
    		/// Delivery Need Booking TWO
    		/// </summary>        
    	//    [DisplayName("Delivery Need Booking TWO")]
            [MaxLength(1, ErrorMessage = "Delivery Need Booking TWO cannot be longer than 1 characters")]
    		public string  DeliveryNeedBookingTWO { get; set; }
    
    		    
    		/// <summary>
    		/// Delivery Remarks TWO
    		/// </summary>        
    	//    [DisplayName("Delivery Remarks TWO")]
            [MaxLength(255, ErrorMessage = "Delivery Remarks TWO cannot be longer than 255 characters")]
    		public string  DeliveryRemarksTWO { get; set; }
    
    		    
    	}
    //}
    
}
