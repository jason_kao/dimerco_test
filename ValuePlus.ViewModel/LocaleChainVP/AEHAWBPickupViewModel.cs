using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ValuePlus.ViewModel;



namespace ValuePlus.ViewModel.LocaleChainVP
{
    
    /// <summary>
    /// AEHAWBPickup class
    /// </summary>
    //[MetadataType(typeof(AEHAWBPickupViewModel))]
    //public  partial class AEHAWBPickup
    //{
    
    	/// <summary>
    	/// AEHAWBPickup Metadata class
    	/// </summary>
    	public   class AEHAWBPickupViewModel:ViewModelBase
    	{
    		    
    		/// <summary>
    		/// ID
    		/// </summary>        
    	//    [DisplayName("ID")]
            [Required(ErrorMessage = "ID is required")]
    		public int  ID { get; set; }
    
    		    
    		/// <summary>
    		/// HAWBID
    		/// </summary>        
    	//    [DisplayName("HAWBID")]
            [Required(ErrorMessage = "HAWBID is required")]
    		public int  HAWBID { get; set; }
    
    		    
    		/// <summary>
    		/// Address
    		/// </summary>        
    	//    [DisplayName("Address")]
            [MaxLength(255, ErrorMessage = "Address cannot be longer than 255 characters")]
    		public string  Address { get; set; }
    
    		    
    		/// <summary>
    		/// Truck
    		/// </summary>        
    	//    [DisplayName("Truck")]
    		public Nullable<int>  Truck { get; set; }
    
    		    
    		/// <summary>
    		/// Avail Pickup
    		/// </summary>        
    	//    [DisplayName("Avail Pickup")]
    		public Nullable<System.DateTime>  AvailPickup { get; set; }
    
    		    
    		/// <summary>
    		/// Actual Pickup
    		/// </summary>        
    	//    [DisplayName("Actual Pickup")]
    		public Nullable<System.DateTime>  ActualPickup { get; set; }
    
    		    
    		/// <summary>
    		/// Remark
    		/// </summary>        
    	//    [DisplayName("Remark")]
            [MaxLength(255, ErrorMessage = "Remark cannot be longer than 255 characters")]
    		public string  Remark { get; set; }
    
    		    
    		/// <summary>
    		/// Pickup From
    		/// </summary>        
    	//    [DisplayName("Pickup From")]
            [Required(ErrorMessage = "Pickup From is required")]
    		public int  PickupFrom { get; set; }
    
    		    
    		/// <summary>
    		/// Station ID
    		/// </summary>        
    	//    [DisplayName("Station ID")]
            [Required(ErrorMessage = "Station ID is required")]
            [MaxLength(10, ErrorMessage = "Station ID cannot be longer than 10 characters")]
    		public string  StationID { get; set; }
    
    		    
    		/// <summary>
    		/// Created By
    		/// </summary>        
    	//    [DisplayName("Created By")]
            [Required(ErrorMessage = "Created By is required")]
            [MaxLength(6, ErrorMessage = "Created By cannot be longer than 6 characters")]
    		public string  CreatedBy { get; set; }
    
    		    
    		/// <summary>
    		/// Created Date
    		/// </summary>        
    	//    [DisplayName("Created Date")]
            [Required(ErrorMessage = "Created Date is required")]
    		public System.DateTime  CreatedDate { get; set; }
    
    		    
    		/// <summary>
    		/// Updated By
    		/// </summary>        
    	//    [DisplayName("Updated By")]
            [Required(ErrorMessage = "Updated By is required")]
            [MaxLength(6, ErrorMessage = "Updated By cannot be longer than 6 characters")]
    		public string  UpdatedBy { get; set; }
    
    		    
    		/// <summary>
    		/// Updated Date
    		/// </summary>        
    	//    [DisplayName("Updated Date")]
            [Required(ErrorMessage = "Updated Date is required")]
    		public System.DateTime  UpdatedDate { get; set; }
    
    		    
    		/// <summary>
    		/// Vehicle No
    		/// </summary>        
    	//    [DisplayName("Vehicle No")]
            [MaxLength(50, ErrorMessage = "Vehicle No cannot be longer than 50 characters")]
    		public string  VehicleNo { get; set; }
    
    		    
    		/// <summary>
    		/// Driver
    		/// </summary>        
    	//    [DisplayName("Driver")]
            [MaxLength(50, ErrorMessage = "Driver cannot be longer than 50 characters")]
    		public string  Driver { get; set; }
    
    		    
    		/// <summary>
    		/// Phone
    		/// </summary>        
    	//    [DisplayName("Phone")]
            [MaxLength(50, ErrorMessage = "Phone cannot be longer than 50 characters")]
    		public string  Phone { get; set; }
    
    		    
    		/// <summary>
    		/// Show
    		/// </summary>        
    	//    [DisplayName("Show")]
    		public Nullable<bool>  Show { get; set; }
    
    		    
    		/// <summary>
    		/// DELVTo
    		/// </summary>        
    	//    [DisplayName("DELVTo")]
    		public Nullable<int>  DELVTo { get; set; }
    
    		    
    		/// <summary>
    		/// DELVAddr
    		/// </summary>        
    	//    [DisplayName("DELVAddr")]
            [MaxLength(255, ErrorMessage = "DELVAddr cannot be longer than 255 characters")]
    		public string  DELVAddr { get; set; }
    
    		    
    		/// <summary>
    		/// Weight
    		/// </summary>        
    	//    [DisplayName("Weight")]
    		public Nullable<double>  Weight { get; set; }
    
    		    
    		/// <summary>
    		/// Weight UOM
    		/// </summary>        
    	//    [DisplayName("Weight UOM")]
            [MaxLength(10, ErrorMessage = "Weight UOM cannot be longer than 10 characters")]
    		public string  WeightUOM { get; set; }
    
    		    
    		/// <summary>
    		/// Avail Pickup To
    		/// </summary>        
    	//    [DisplayName("Avail Pickup To")]
    		public Nullable<System.DateTime>  AvailPickupTo { get; set; }
    
    		    
    		/// <summary>
    		/// Actual Pickup To
    		/// </summary>        
    	//    [DisplayName("Actual Pickup To")]
    		public Nullable<System.DateTime>  ActualPickupTo { get; set; }
    
    		    
    		/// <summary>
    		/// PCS
    		/// </summary>        
    	//    [DisplayName("PCS")]
    		public Nullable<int>  PCS { get; set; }
    
    		    
    		/// <summary>
    		/// PCSUOM
    		/// </summary>        
    	//    [DisplayName("PCSUOM")]
            [MaxLength(50, ErrorMessage = "PCSUOM cannot be longer than 50 characters")]
    		public string  PCSUOM { get; set; }
    
    		    
    		/// <summary>
    		/// CWT
    		/// </summary>        
    	//    [DisplayName("CWT")]
    		public Nullable<double>  CWT { get; set; }
    
    		    
    		/// <summary>
    		/// Send By
    		/// </summary>        
    	//    [DisplayName("Send By")]
            [MaxLength(10, ErrorMessage = "Send By cannot be longer than 10 characters")]
    		public string  SendBy { get; set; }
    
    		    
    		/// <summary>
    		/// Send Date
    		/// </summary>        
    	//    [DisplayName("Send Date")]
    		public Nullable<System.DateTime>  SendDate { get; set; }
    
    		    
    		/// <summary>
    		/// Version
    		/// </summary>        
    	//    [DisplayName("Version")]
            [MaxLength(8, ErrorMessage = "Version cannot be longer than 8 characters")]
    		public byte[]  Version { get; set; }
    
    		    
    	}
    //}
    
}
