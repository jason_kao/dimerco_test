using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ValuePlus.ViewModel;



namespace ValuePlus.ViewModel.LocaleChainVP
{
    
    /// <summary>
    /// AEPODData class
    /// </summary>
    //[MetadataType(typeof(AEPODDataViewModel))]
    //public  partial class AEPODData
    //{
    
    	/// <summary>
    	/// AEPODData Metadata class
    	/// </summary>
    	public   class AEPODDataViewModel:ViewModelBase
    	{
    		    
    		/// <summary>
    		/// ID
    		/// </summary>        
    	//    [DisplayName("ID")]
            [Required(ErrorMessage = "ID is required")]
    		public int  ID { get; set; }
    
    		    
    		/// <summary>
    		/// HAWBID
    		/// </summary>        
    	//    [DisplayName("HAWBID")]
            [Required(ErrorMessage = "HAWBID is required")]
    		public int  HAWBID { get; set; }
    
    		    
    		/// <summary>
    		/// PODID
    		/// </summary>        
    	//    [DisplayName("PODID")]
            [Required(ErrorMessage = "PODID is required")]
    		public int  PODID { get; set; }
    
    		    
    		/// <summary>
    		/// Value
    		/// </summary>        
    	//    [DisplayName("Value")]
            [Required(ErrorMessage = "Value is required")]
    		public string  Value { get; set; }
    
    		    
    	}
    //}
    
}
