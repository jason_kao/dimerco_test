using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ValuePlus.ViewModel;



namespace ValuePlus.ViewModel.LocaleChainVP
{
    
    /// <summary>
    /// OEHBL class
    /// </summary>
    //[MetadataType(typeof(OEHBLViewModel))]
    //public  partial class OEHBL
    //{
    
    	/// <summary>
    	/// OEHBL Metadata class
    	/// </summary>
    	public   class OEHBLViewModel:ViewModelBase
    	{
    		    
    		/// <summary>
    		/// ID
    		/// </summary>        
    	//    [DisplayName("ID")]
            [Required(ErrorMessage = "ID is required")]
    		public int  ID { get; set; }
    
    		    
    		/// <summary>
    		/// HBLNo
    		/// </summary>        
    	//    [DisplayName("HBLNo")]
            [MaxLength(20, ErrorMessage = "HBLNo cannot be longer than 20 characters")]
    		public string  HBLNo { get; set; }
    
    		    
    		/// <summary>
    		/// Station ID
    		/// </summary>        
    	//    [DisplayName("Station ID")]
            [MaxLength(5, ErrorMessage = "Station ID cannot be longer than 5 characters")]
    		public string  StationID { get; set; }
    
    		    
    		/// <summary>
    		/// MBLID
    		/// </summary>        
    	//    [DisplayName("MBLID")]
    		public Nullable<int>  MBLID { get; set; }
    
    		    
    		/// <summary>
    		/// PCS
    		/// </summary>        
    	//    [DisplayName("PCS")]
    		public Nullable<int>  PCS { get; set; }
    
    		    
    		/// <summary>
    		/// Customer
    		/// </summary>        
    	//    [DisplayName("Customer")]
    		public Nullable<int>  Customer { get; set; }
    
    		    
    		/// <summary>
    		/// SHPR
    		/// </summary>        
    	//    [DisplayName("SHPR")]
    		public Nullable<int>  SHPR { get; set; }
    
    		    
    		/// <summary>
    		/// CNEE
    		/// </summary>        
    	//    [DisplayName("CNEE")]
    		public Nullable<int>  CNEE { get; set; }
    
    		    
    		/// <summary>
    		/// NTFY
    		/// </summary>        
    	//    [DisplayName("NTFY")]
    		public Nullable<int>  NTFY { get; set; }
    
    		    
    		/// <summary>
    		/// Third Party
    		/// </summary>        
    	//    [DisplayName("Third Party")]
    		public Nullable<int>  ThirdParty { get; set; }
    
    		    
    		/// <summary>
    		/// PReceipt
    		/// </summary>        
    	//    [DisplayName("PReceipt")]
    		public Nullable<int>  PReceipt { get; set; }
    
    		    
    		/// <summary>
    		/// PLoading
    		/// </summary>        
    	//    [DisplayName("PLoading")]
    		public Nullable<int>  PLoading { get; set; }
    
    		    
    		/// <summary>
    		/// PDischarge
    		/// </summary>        
    	//    [DisplayName("PDischarge")]
    		public Nullable<int>  PDischarge { get; set; }
    
    		    
    		/// <summary>
    		/// PDelivery
    		/// </summary>        
    	//    [DisplayName("PDelivery")]
    		public Nullable<int>  PDelivery { get; set; }
    
    		    
    		/// <summary>
    		/// Final Dest
    		/// </summary>        
    	//    [DisplayName("Final Dest")]
    		public Nullable<int>  FinalDest { get; set; }
    
    		    
    		/// <summary>
    		/// Co Loader
    		/// </summary>        
    	//    [DisplayName("Co Loader")]
    		public Nullable<int>  CoLoader { get; set; }
    
    		    
    		/// <summary>
    		/// Carrier
    		/// </summary>        
    	//    [DisplayName("Carrier")]
    		public Nullable<int>  Carrier { get; set; }
    
    		    
    		/// <summary>
    		/// FWDAgent
    		/// </summary>        
    	//    [DisplayName("FWDAgent")]
    		public Nullable<int>  FWDAgent { get; set; }
    
    		    
    		/// <summary>
    		/// DESTAgent
    		/// </summary>        
    	//    [DisplayName("DESTAgent")]
    		public Nullable<int>  DESTAgent { get; set; }
    
    		    
    		/// <summary>
    		/// Instruction
    		/// </summary>        
    	//    [DisplayName("Instruction")]
            [MaxLength(255, ErrorMessage = "Instruction cannot be longer than 255 characters")]
    		public string  Instruction { get; set; }
    
    		    
    		/// <summary>
    		/// Shpt Type
    		/// </summary>        
    	//    [DisplayName("Shpt Type")]
            [MaxLength(20, ErrorMessage = "Shpt Type cannot be longer than 20 characters")]
    		public string  ShptType { get; set; }
    
    		    
    		/// <summary>
    		/// Move Type
    		/// </summary>        
    	//    [DisplayName("Move Type")]
            [MaxLength(20, ErrorMessage = "Move Type cannot be longer than 20 characters")]
    		public string  MoveType { get; set; }
    
    		    
    		/// <summary>
    		/// Freight Pay Type
    		/// </summary>        
    	//    [DisplayName("Freight Pay Type")]
            [MaxLength(20, ErrorMessage = "Freight Pay Type cannot be longer than 20 characters")]
    		public string  FreightPayType { get; set; }
    
    		    
    		/// <summary>
    		/// Trade Type
    		/// </summary>        
    	//    [DisplayName("Trade Type")]
            [MaxLength(20, ErrorMessage = "Trade Type cannot be longer than 20 characters")]
    		public string  TradeType { get; set; }
    
    		    
    		/// <summary>
    		/// PReceipt ETD
    		/// </summary>        
    	//    [DisplayName("PReceipt ETD")]
    		public Nullable<System.DateTime>  PReceiptETD { get; set; }
    
    		    
    		/// <summary>
    		/// PLoading ETD
    		/// </summary>        
    	//    [DisplayName("PLoading ETD")]
    		public Nullable<System.DateTime>  PLoadingETD { get; set; }
    
    		    
    		/// <summary>
    		/// PDischarge ETD
    		/// </summary>        
    	//    [DisplayName("PDischarge ETD")]
    		public Nullable<System.DateTime>  PDischargeETD { get; set; }
    
    		    
    		/// <summary>
    		/// PDelivery ETD
    		/// </summary>        
    	//    [DisplayName("PDelivery ETD")]
    		public Nullable<System.DateTime>  PDeliveryETD { get; set; }
    
    		    
    		/// <summary>
    		/// Final Dest ETD
    		/// </summary>        
    	//    [DisplayName("Final Dest ETD")]
    		public Nullable<System.DateTime>  FinalDestETD { get; set; }
    
    		    
    		/// <summary>
    		/// PLoading ATD
    		/// </summary>        
    	//    [DisplayName("PLoading ATD")]
    		public Nullable<System.DateTime>  PLoadingATD { get; set; }
    
    		    
    		/// <summary>
    		/// Sales Person
    		/// </summary>        
    	//    [DisplayName("Sales Person")]
    		public Nullable<int>  SalesPerson { get; set; }
    
    		    
    		/// <summary>
    		/// Sales Type
    		/// </summary>        
    	//    [DisplayName("Sales Type")]
            [MaxLength(10, ErrorMessage = "Sales Type cannot be longer than 10 characters")]
    		public string  SalesType { get; set; }
    
    		    
    		/// <summary>
    		/// Export Ref
    		/// </summary>        
    	//    [DisplayName("Export Ref")]
            [MaxLength(100, ErrorMessage = "Export Ref cannot be longer than 100 characters")]
    		public string  ExportRef { get; set; }
    
    		    
    		/// <summary>
    		/// CTNRIn Word
    		/// </summary>        
    	//    [DisplayName("CTNRIn Word")]
            [MaxLength(100, ErrorMessage = "CTNRIn Word cannot be longer than 100 characters")]
    		public string  CTNRInWord { get; set; }
    
    		    
    		/// <summary>
    		/// Dec Value
    		/// </summary>        
    	//    [DisplayName("Dec Value")]
    		public Nullable<double>  DecValue { get; set; }
    
    		    
    		/// <summary>
    		/// Dec Value Curr
    		/// </summary>        
    	//    [DisplayName("Dec Value Curr")]
            [MaxLength(10, ErrorMessage = "Dec Value Curr cannot be longer than 10 characters")]
    		public string  DecValueCurr { get; set; }
    
    		    
    		/// <summary>
    		/// Telex Release
    		/// </summary>        
    	//    [DisplayName("Telex Release")]
            [MaxLength(50, ErrorMessage = "Telex Release cannot be longer than 50 characters")]
    		public string  TelexRelease { get; set; }
    
    		    
    		/// <summary>
    		/// Is Draft
    		/// </summary>        
    	//    [DisplayName("Is Draft")]
            [MaxLength(1, ErrorMessage = "Is Draft cannot be longer than 1 characters")]
    		public string  IsDraft { get; set; }
    
    		    
    		/// <summary>
    		/// Is Bookmark
    		/// </summary>        
    	//    [DisplayName("Is Bookmark")]
            [MaxLength(1, ErrorMessage = "Is Bookmark cannot be longer than 1 characters")]
    		public string  IsBookmark { get; set; }
    
    		    
    		/// <summary>
    		/// Is Void
    		/// </summary>        
    	//    [DisplayName("Is Void")]
            [MaxLength(1, ErrorMessage = "Is Void cannot be longer than 1 characters")]
    		public string  IsVoid { get; set; }
    
    		    
    		/// <summary>
    		/// SHNum Of DR
    		/// </summary>        
    	//    [DisplayName("SHNum Of DR")]
            [MaxLength(50, ErrorMessage = "SHNum Of DR cannot be longer than 50 characters")]
    		public string  SHNumOfDR { get; set; }
    
    		    
    		/// <summary>
    		/// USOTILic
    		/// </summary>        
    	//    [DisplayName("USOTILic")]
            [MaxLength(100, ErrorMessage = "USOTILic cannot be longer than 100 characters")]
    		public string  USOTILic { get; set; }
    
    		    
    		/// <summary>
    		/// USSVILic
    		/// </summary>        
    	//    [DisplayName("USSVILic")]
            [MaxLength(100, ErrorMessage = "USSVILic cannot be longer than 100 characters")]
    		public string  USSVILic { get; set; }
    
    		    
    		/// <summary>
    		/// USExport Ref
    		/// </summary>        
    	//    [DisplayName("USExport Ref")]
            [MaxLength(80, ErrorMessage = "USExport Ref cannot be longer than 80 characters")]
    		public string  USExportRef { get; set; }
    
    		    
    		/// <summary>
    		/// USDecl Value
    		/// </summary>        
    	//    [DisplayName("USDecl Value")]
            [MaxLength(60, ErrorMessage = "USDecl Value cannot be longer than 60 characters")]
    		public string  USDeclValue { get; set; }
    
    		    
    		/// <summary>
    		/// Created By
    		/// </summary>        
    	//    [DisplayName("Created By")]
            [MaxLength(50, ErrorMessage = "Created By cannot be longer than 50 characters")]
    		public string  CreatedBy { get; set; }
    
    		    
    		/// <summary>
    		/// Created DT
    		/// </summary>        
    	//    [DisplayName("Created DT")]
    		public Nullable<System.DateTime>  CreatedDT { get; set; }
    
    		    
    		/// <summary>
    		/// Updated By
    		/// </summary>        
    	//    [DisplayName("Updated By")]
            [MaxLength(50, ErrorMessage = "Updated By cannot be longer than 50 characters")]
    		public string  UpdatedBy { get; set; }
    
    		    
    		/// <summary>
    		/// Updated DT
    		/// </summary>        
    	//    [DisplayName("Updated DT")]
    		public Nullable<System.DateTime>  UpdatedDT { get; set; }
    
    		    
    		/// <summary>
    		/// Version
    		/// </summary>        
    	//    [DisplayName("Version")]
            [Required(ErrorMessage = "Version is required")]
            [MaxLength(8, ErrorMessage = "Version cannot be longer than 8 characters")]
    		public byte[]  Version { get; set; }
    
    		    
    		/// <summary>
    		/// Pre Alert By
    		/// </summary>        
    	//    [DisplayName("Pre Alert By")]
            [MaxLength(50, ErrorMessage = "Pre Alert By cannot be longer than 50 characters")]
    		public string  PreAlertBy { get; set; }
    
    		    
    		/// <summary>
    		/// Pre Alert DT
    		/// </summary>        
    	//    [DisplayName("Pre Alert DT")]
    		public Nullable<System.DateTime>  PreAlertDT { get; set; }
    
    		    
    		/// <summary>
    		/// OBLIssued
    		/// </summary>        
    	//    [DisplayName("OBLIssued")]
    		public Nullable<int>  OBLIssued { get; set; }
    
    		    
    		/// <summary>
    		/// Feeder Vessel
    		/// </summary>        
    	//    [DisplayName("Feeder Vessel")]
            [MaxLength(60, ErrorMessage = "Feeder Vessel cannot be longer than 60 characters")]
    		public string  FeederVessel { get; set; }
    
    		    
    		/// <summary>
    		/// Feeder Voyage
    		/// </summary>        
    	//    [DisplayName("Feeder Voyage")]
            [MaxLength(80, ErrorMessage = "Feeder Voyage cannot be longer than 80 characters")]
    		public string  FeederVoyage { get; set; }
    
    		    
    		/// <summary>
    		/// Ocean Vessel
    		/// </summary>        
    	//    [DisplayName("Ocean Vessel")]
            [MaxLength(60, ErrorMessage = "Ocean Vessel cannot be longer than 60 characters")]
    		public string  OceanVessel { get; set; }
    
    		    
    		/// <summary>
    		/// Ocean Voyage
    		/// </summary>        
    	//    [DisplayName("Ocean Voyage")]
            [MaxLength(80, ErrorMessage = "Ocean Voyage cannot be longer than 80 characters")]
    		public string  OceanVoyage { get; set; }
    
    		    
    		/// <summary>
    		/// Telex Release DT
    		/// </summary>        
    	//    [DisplayName("Telex Release DT")]
    		public Nullable<System.DateTime>  TelexReleaseDT { get; set; }
    
    		    
    		/// <summary>
    		/// Booking No
    		/// </summary>        
    	//    [DisplayName("Booking No")]
            [MaxLength(20, ErrorMessage = "Booking No cannot be longer than 20 characters")]
    		public string  BookingNo { get; set; }
    
    		    
    		/// <summary>
    		/// Src ID
    		/// </summary>        
    	//    [DisplayName("Src ID")]
    		public Nullable<int>  SrcID { get; set; }
    
    		    
    		/// <summary>
    		/// Src Station ID
    		/// </summary>        
    	//    [DisplayName("Src Station ID")]
            [MaxLength(5, ErrorMessage = "Src Station ID cannot be longer than 5 characters")]
    		public string  SrcStationID { get; set; }
    
    		    
    		/// <summary>
    		/// FCalculate Type
    		/// </summary>        
    	//    [DisplayName("FCalculate Type")]
            [MaxLength(10, ErrorMessage = "FCalculate Type cannot be longer than 10 characters")]
    		public string  FCalculateType { get; set; }
    
    		    
    		/// <summary>
    		/// Service Type
    		/// </summary>        
    	//    [DisplayName("Service Type")]
            [MaxLength(30, ErrorMessage = "Service Type cannot be longer than 30 characters")]
    		public string  ServiceType { get; set; }
    
    		    
    		/// <summary>
    		/// Quote Type
    		/// </summary>        
    	//    [DisplayName("Quote Type")]
            [MaxLength(250, ErrorMessage = "Quote Type cannot be longer than 250 characters")]
    		public string  QuoteType { get; set; }
    
    		    
    		/// <summary>
    		/// CYMBy
    		/// </summary>        
    	//    [DisplayName("CYMBy")]
            [MaxLength(30, ErrorMessage = "CYMBy cannot be longer than 30 characters")]
    		public string  CYMBy { get; set; }
    
    		    
    		/// <summary>
    		/// CYMDT
    		/// </summary>        
    	//    [DisplayName("CYMDT")]
    		public Nullable<System.DateTime>  CYMDT { get; set; }
    
    		    
    		/// <summary>
    		/// HBLControl No
    		/// </summary>        
    	//    [DisplayName("HBLControl No")]
            [MaxLength(50, ErrorMessage = "HBLControl No cannot be longer than 50 characters")]
    		public string  HBLControlNo { get; set; }
    
    		    
    		/// <summary>
    		/// OE3 PManifest Send By
    		/// </summary>        
    	//    [DisplayName("OE3 PManifest Send By")]
            [MaxLength(50, ErrorMessage = "OE3 PManifest Send By cannot be longer than 50 characters")]
    		public string  OE3PManifestSendBy { get; set; }
    
    		    
    		/// <summary>
    		/// OE3 PManifest Send DT
    		/// </summary>        
    	//    [DisplayName("OE3 PManifest Send DT")]
    		public Nullable<System.DateTime>  OE3PManifestSendDT { get; set; }
    
    		    
    		/// <summary>
    		/// Nature Of Goods Type
    		/// </summary>        
    	//    [DisplayName("Nature Of Goods Type")]
            [MaxLength(50, ErrorMessage = "Nature Of Goods Type cannot be longer than 50 characters")]
    		public string  NatureOfGoodsType { get; set; }
    
    		    
    		/// <summary>
    		/// IMPStation
    		/// </summary>        
    	//    [DisplayName("IMPStation")]
            [MaxLength(10, ErrorMessage = "IMPStation cannot be longer than 10 characters")]
    		public string  IMPStation { get; set; }
    
    		    
    		/// <summary>
    		/// Also NTFY
    		/// </summary>        
    	//    [DisplayName("Also NTFY")]
    		public Nullable<int>  AlsoNTFY { get; set; }
    
    		    
    		/// <summary>
    		/// Re Quote Type
    		/// </summary>        
    	//    [DisplayName("Re Quote Type")]
            [MaxLength(250, ErrorMessage = "Re Quote Type cannot be longer than 250 characters")]
    		public string  ReQuoteType { get; set; }
    
    		    
    		/// <summary>
    		/// Quote Type Created By
    		/// </summary>        
    	//    [DisplayName("Quote Type Created By")]
            [MaxLength(50, ErrorMessage = "Quote Type Created By cannot be longer than 50 characters")]
    		public string  QuoteTypeCreatedBy { get; set; }
    
    		    
    		/// <summary>
    		/// Re Quote Type Created By
    		/// </summary>        
    	//    [DisplayName("Re Quote Type Created By")]
            [MaxLength(50, ErrorMessage = "Re Quote Type Created By cannot be longer than 50 characters")]
    		public string  ReQuoteTypeCreatedBy { get; set; }
    
    		    
    		/// <summary>
    		/// Quote Type Created DT
    		/// </summary>        
    	//    [DisplayName("Quote Type Created DT")]
    		public Nullable<System.DateTime>  QuoteTypeCreatedDT { get; set; }
    
    		    
    		/// <summary>
    		/// Re Quote Type Created DT
    		/// </summary>        
    	//    [DisplayName("Re Quote Type Created DT")]
    		public Nullable<System.DateTime>  ReQuoteTypeCreatedDT { get; set; }
    
    		    
    		/// <summary>
    		/// Post Sales DT
    		/// </summary>        
    	//    [DisplayName("Post Sales DT")]
    		public Nullable<System.DateTime>  PostSalesDT { get; set; }
    
    		    
    		/// <summary>
    		/// MTSNO
    		/// </summary>        
    	//    [DisplayName("MTSNO")]
            [MaxLength(15, ErrorMessage = "MTSNO cannot be longer than 15 characters")]
    		public string  MTSNO { get; set; }
    
    		    
    		/// <summary>
    		/// Cargo Ready Date
    		/// </summary>        
    	//    [DisplayName("Cargo Ready Date")]
    		public Nullable<System.DateTime>  CargoReadyDate { get; set; }
    
    		    
    		/// <summary>
    		/// Booking Cut Off Date
    		/// </summary>        
    	//    [DisplayName("Booking Cut Off Date")]
    		public Nullable<System.DateTime>  BookingCutOffDate { get; set; }
    
    		    
    		/// <summary>
    		/// Assign To
    		/// </summary>        
    	//    [DisplayName("Assign To")]
            [MaxLength(10, ErrorMessage = "Assign To cannot be longer than 10 characters")]
    		public string  AssignTo { get; set; }
    
    		    
    		/// <summary>
    		/// Booking Remark
    		/// </summary>        
    	//    [DisplayName("Booking Remark")]
            [MaxLength(255, ErrorMessage = "Booking Remark cannot be longer than 255 characters")]
    		public string  BookingRemark { get; set; }
    
    		    
    		/// <summary>
    		/// Booking Send By
    		/// </summary>        
    	//    [DisplayName("Booking Send By")]
            [MaxLength(50, ErrorMessage = "Booking Send By cannot be longer than 50 characters")]
    		public string  BookingSendBy { get; set; }
    
    		    
    		/// <summary>
    		/// Booking Send DT
    		/// </summary>        
    	//    [DisplayName("Booking Send DT")]
    		public Nullable<System.DateTime>  BookingSendDT { get; set; }
    
    		    
    		/// <summary>
    		/// DSTNStation ID
    		/// </summary>        
    	//    [DisplayName("DSTNStation ID")]
            [MaxLength(5, ErrorMessage = "DSTNStation ID cannot be longer than 5 characters")]
    		public string  DSTNStationID { get; set; }
    
    		    
    		/// <summary>
    		/// Confirm ID
    		/// </summary>        
    	//    [DisplayName("Confirm ID")]
            [MaxLength(20, ErrorMessage = "Confirm ID cannot be longer than 20 characters")]
    		public string  ConfirmID { get; set; }
    
    		    
    		/// <summary>
    		/// Quote No
    		/// </summary>        
    	//    [DisplayName("Quote No")]
            [MaxLength(20, ErrorMessage = "Quote No cannot be longer than 20 characters")]
    		public string  QuoteNo { get; set; }
    
    		    
    		/// <summary>
    		/// Booking Status
    		/// </summary>        
    	//    [DisplayName("Booking Status")]
            [MaxLength(10, ErrorMessage = "Booking Status cannot be longer than 10 characters")]
    		public string  BookingStatus { get; set; }
    
    		    
    		/// <summary>
    		/// Is ISF
    		/// </summary>        
    	//    [DisplayName("Is ISF")]
            [MaxLength(1, ErrorMessage = "Is ISF cannot be longer than 1 characters")]
    		public string  IsISF { get; set; }
    
    		    
    		/// <summary>
    		/// Is AMS
    		/// </summary>        
    	//    [DisplayName("Is AMS")]
            [MaxLength(1, ErrorMessage = "Is AMS cannot be longer than 1 characters")]
    		public string  IsAMS { get; set; }
    
    		    
    		/// <summary>
    		/// Is Co Load In
    		/// </summary>        
    	//    [DisplayName("Is Co Load In")]
            [MaxLength(1, ErrorMessage = "Is Co Load In cannot be longer than 1 characters")]
    		public string  IsCoLoadIn { get; set; }
    
    		    
    		/// <summary>
    		/// Load Plan ID
    		/// </summary>        
    	//    [DisplayName("Load Plan ID")]
    		public Nullable<int>  LoadPlanID { get; set; }
    
    		    
    		/// <summary>
    		/// Extra0
    		/// </summary>        
    	//    [DisplayName("Extra0")]
            [MaxLength(50, ErrorMessage = "Extra0 cannot be longer than 50 characters")]
    		public string  Extra0 { get; set; }
    
    		    
    		/// <summary>
    		/// Extra1
    		/// </summary>        
    	//    [DisplayName("Extra1")]
            [MaxLength(50, ErrorMessage = "Extra1 cannot be longer than 50 characters")]
    		public string  Extra1 { get; set; }
    
    		    
    		/// <summary>
    		/// Extra2
    		/// </summary>        
    	//    [DisplayName("Extra2")]
            [MaxLength(50, ErrorMessage = "Extra2 cannot be longer than 50 characters")]
    		public string  Extra2 { get; set; }
    
    		    
    		/// <summary>
    		/// Extra3
    		/// </summary>        
    	//    [DisplayName("Extra3")]
            [MaxLength(50, ErrorMessage = "Extra3 cannot be longer than 50 characters")]
    		public string  Extra3 { get; set; }
    
    		    
    		/// <summary>
    		/// Extra4
    		/// </summary>        
    	//    [DisplayName("Extra4")]
            [MaxLength(50, ErrorMessage = "Extra4 cannot be longer than 50 characters")]
    		public string  Extra4 { get; set; }
    
    		    
    		/// <summary>
    		/// Extra5
    		/// </summary>        
    	//    [DisplayName("Extra5")]
            [MaxLength(50, ErrorMessage = "Extra5 cannot be longer than 50 characters")]
    		public string  Extra5 { get; set; }
    
    		    
    		/// <summary>
    		/// Extra6
    		/// </summary>        
    	//    [DisplayName("Extra6")]
            [MaxLength(50, ErrorMessage = "Extra6 cannot be longer than 50 characters")]
    		public string  Extra6 { get; set; }
    
    		    
    		/// <summary>
    		/// Extra7
    		/// </summary>        
    	//    [DisplayName("Extra7")]
            [MaxLength(50, ErrorMessage = "Extra7 cannot be longer than 50 characters")]
    		public string  Extra7 { get; set; }
    
    		    
    		/// <summary>
    		/// Extra8
    		/// </summary>        
    	//    [DisplayName("Extra8")]
            [MaxLength(50, ErrorMessage = "Extra8 cannot be longer than 50 characters")]
    		public string  Extra8 { get; set; }
    
    		    
    		/// <summary>
    		/// Extra9
    		/// </summary>        
    	//    [DisplayName("Extra9")]
            [MaxLength(50, ErrorMessage = "Extra9 cannot be longer than 50 characters")]
    		public string  Extra9 { get; set; }
    
    		    
    		/// <summary>
    		/// Cust Booking No
    		/// </summary>        
    	//    [DisplayName("Cust Booking No")]
            [MaxLength(20, ErrorMessage = "Cust Booking No cannot be longer than 20 characters")]
    		public string  CustBookingNo { get; set; }
    
    		    
    		/// <summary>
    		/// UNNumber
    		/// </summary>        
    	//    [DisplayName("UNNumber")]
            [MaxLength(20, ErrorMessage = "UNNumber cannot be longer than 20 characters")]
    		public string  UNNumber { get; set; }
    
    		    
    		/// <summary>
    		/// PReceipt ATD
    		/// </summary>        
    	//    [DisplayName("PReceipt ATD")]
    		public Nullable<System.DateTime>  PReceiptATD { get; set; }
    
    		    
    		/// <summary>
    		/// PDelivery ATD
    		/// </summary>        
    	//    [DisplayName("PDelivery ATD")]
    		public Nullable<System.DateTime>  PDeliveryATD { get; set; }
    
    		    
    		/// <summary>
    		/// Revenue Ton
    		/// </summary>        
    	//    [DisplayName("Revenue Ton")]
    		public Nullable<decimal>  RevenueTon { get; set; }
    
    		    
    	}
    //}
    
}
