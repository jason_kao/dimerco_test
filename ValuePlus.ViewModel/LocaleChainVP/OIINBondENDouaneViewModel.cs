using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ValuePlus.ViewModel;



namespace ValuePlus.ViewModel.LocaleChainVP
{
    
    /// <summary>
    /// OIINBondENDouane class
    /// </summary>
    //[MetadataType(typeof(OIINBondENDouaneViewModel))]
    //public  partial class OIINBondENDouane
    //{
    
    	/// <summary>
    	/// OIINBondENDouane Metadata class
    	/// </summary>
    	public   class OIINBondENDouaneViewModel:ViewModelBase
    	{
    		    
    		/// <summary>
    		/// ID
    		/// </summary>        
    	//    [DisplayName("ID")]
            [Required(ErrorMessage = "ID is required")]
    		public int  ID { get; set; }
    
    		    
    		/// <summary>
    		/// HBLID
    		/// </summary>        
    	//    [DisplayName("HBLID")]
            [MaxLength(50, ErrorMessage = "HBLID cannot be longer than 50 characters")]
    		public string  HBLID { get; set; }
    
    		    
    		/// <summary>
    		/// Mode
    		/// </summary>        
    	//    [DisplayName("Mode")]
            [MaxLength(5, ErrorMessage = "Mode cannot be longer than 5 characters")]
    		public string  Mode { get; set; }
    
    		    
    		/// <summary>
    		/// Station ID
    		/// </summary>        
    	//    [DisplayName("Station ID")]
            [MaxLength(5, ErrorMessage = "Station ID cannot be longer than 5 characters")]
    		public string  StationID { get; set; }
    
    		    
    		/// <summary>
    		/// Station Name
    		/// </summary>        
    	//    [DisplayName("Station Name")]
            [MaxLength(200, ErrorMessage = "Station Name cannot be longer than 200 characters")]
    		public string  StationName { get; set; }
    
    		    
    		/// <summary>
    		/// Station Address
    		/// </summary>        
    	//    [DisplayName("Station Address")]
            [MaxLength(200, ErrorMessage = "Station Address cannot be longer than 200 characters")]
    		public string  StationAddress { get; set; }
    
    		    
    		/// <summary>
    		/// Station Tel
    		/// </summary>        
    	//    [DisplayName("Station Tel")]
            [MaxLength(50, ErrorMessage = "Station Tel cannot be longer than 50 characters")]
    		public string  StationTel { get; set; }
    
    		    
    		/// <summary>
    		/// Station FAX
    		/// </summary>        
    	//    [DisplayName("Station FAX")]
            [MaxLength(50, ErrorMessage = "Station FAX cannot be longer than 50 characters")]
    		public string  StationFAX { get; set; }
    
    		    
    		/// <summary>
    		/// USPort
    		/// </summary>        
    	//    [DisplayName("USPort")]
            [MaxLength(50, ErrorMessage = "USPort cannot be longer than 50 characters")]
    		public string  USPort { get; set; }
    
    		    
    		/// <summary>
    		/// In Transit
    		/// </summary>        
    	//    [DisplayName("In Transit")]
            [MaxLength(50, ErrorMessage = "In Transit cannot be longer than 50 characters")]
    		public string  InTransit { get; set; }
    
    		    
    		/// <summary>
    		/// Manifest From
    		/// </summary>        
    	//    [DisplayName("Manifest From")]
            [MaxLength(50, ErrorMessage = "Manifest From cannot be longer than 50 characters")]
    		public string  ManifestFrom { get; set; }
    
    		    
    		/// <summary>
    		/// Manifest To
    		/// </summary>        
    	//    [DisplayName("Manifest To")]
            [MaxLength(50, ErrorMessage = "Manifest To cannot be longer than 50 characters")]
    		public string  ManifestTo { get; set; }
    
    		    
    		/// <summary>
    		/// CNEEInfo
    		/// </summary>        
    	//    [DisplayName("CNEEInfo")]
            [MaxLength(250, ErrorMessage = "CNEEInfo cannot be longer than 250 characters")]
    		public string  CNEEInfo { get; set; }
    
    		    
    		/// <summary>
    		/// SHPRInfo
    		/// </summary>        
    	//    [DisplayName("SHPRInfo")]
            [MaxLength(250, ErrorMessage = "SHPRInfo cannot be longer than 250 characters")]
    		public string  SHPRInfo { get; set; }
    
    		    
    		/// <summary>
    		/// Acquittal No
    		/// </summary>        
    	//    [DisplayName("Acquittal No")]
            [MaxLength(50, ErrorMessage = "Acquittal No cannot be longer than 50 characters")]
    		public string  AcquittalNo { get; set; }
    
    		    
    		/// <summary>
    		/// Carrier Code
    		/// </summary>        
    	//    [DisplayName("Carrier Code")]
            [MaxLength(50, ErrorMessage = "Carrier Code cannot be longer than 50 characters")]
    		public string  CarrierCode { get; set; }
    
    		    
    		/// <summary>
    		/// Cargo Control No
    		/// </summary>        
    	//    [DisplayName("Cargo Control No")]
            [MaxLength(50, ErrorMessage = "Cargo Control No cannot be longer than 50 characters")]
    		public string  CargoControlNo { get; set; }
    
    		    
    		/// <summary>
    		/// Previous Cargo Control No
    		/// </summary>        
    	//    [DisplayName("Previous Cargo Control No")]
            [MaxLength(50, ErrorMessage = "Previous Cargo Control No cannot be longer than 50 characters")]
    		public string  PreviousCargoControlNo { get; set; }
    
    		    
    		/// <summary>
    		/// PKGS
    		/// </summary>        
    	//    [DisplayName("PKGS")]
            [MaxLength(150, ErrorMessage = "PKGS cannot be longer than 150 characters")]
    		public string  PKGS { get; set; }
    
    		    
    		/// <summary>
    		/// Marks
    		/// </summary>        
    	//    [DisplayName("Marks")]
            [MaxLength(500, ErrorMessage = "Marks cannot be longer than 500 characters")]
    		public string  Marks { get; set; }
    
    		    
    		/// <summary>
    		/// WT
    		/// </summary>        
    	//    [DisplayName("WT")]
            [MaxLength(150, ErrorMessage = "WT cannot be longer than 150 characters")]
    		public string  WT { get; set; }
    
    		    
    		/// <summary>
    		/// Lot No And HBLNo
    		/// </summary>        
    	//    [DisplayName("Lot No And HBLNo")]
            [MaxLength(50, ErrorMessage = "Lot No And HBLNo cannot be longer than 50 characters")]
    		public string  LotNoAndHBLNo { get; set; }
    
    		    
    		/// <summary>
    		/// PLanding
    		/// </summary>        
    	//    [DisplayName("PLanding")]
            [MaxLength(50, ErrorMessage = "PLanding cannot be longer than 50 characters")]
    		public string  PLanding { get; set; }
    
    		    
    		/// <summary>
    		/// Carrier
    		/// </summary>        
    	//    [DisplayName("Carrier")]
            [MaxLength(50, ErrorMessage = "Carrier cannot be longer than 50 characters")]
    		public string  Carrier { get; set; }
    
    		    
    		/// <summary>
    		/// Customs Clear By
    		/// </summary>        
    	//    [DisplayName("Customs Clear By")]
            [MaxLength(500, ErrorMessage = "Customs Clear By cannot be longer than 500 characters")]
    		public string  CustomsClearBy { get; set; }
    
    		    
    		/// <summary>
    		/// Customs TEL
    		/// </summary>        
    	//    [DisplayName("Customs TEL")]
            [MaxLength(500, ErrorMessage = "Customs TEL cannot be longer than 500 characters")]
    		public string  CustomsTEL { get; set; }
    
    		    
    		/// <summary>
    		/// Customs FAX
    		/// </summary>        
    	//    [DisplayName("Customs FAX")]
            [MaxLength(50, ErrorMessage = "Customs FAX cannot be longer than 50 characters")]
    		public string  CustomsFAX { get; set; }
    
    		    
    		/// <summary>
    		/// Location Goods
    		/// </summary>        
    	//    [DisplayName("Location Goods")]
            [MaxLength(500, ErrorMessage = "Location Goods cannot be longer than 500 characters")]
    		public string  LocationGoods { get; set; }
    
    		    
    		/// <summary>
    		/// Location TEL
    		/// </summary>        
    	//    [DisplayName("Location TEL")]
            [MaxLength(50, ErrorMessage = "Location TEL cannot be longer than 50 characters")]
    		public string  LocationTEL { get; set; }
    
    		    
    		/// <summary>
    		/// Location FAX
    		/// </summary>        
    	//    [DisplayName("Location FAX")]
            [MaxLength(50, ErrorMessage = "Location FAX cannot be longer than 50 characters")]
    		public string  LocationFAX { get; set; }
    
    		    
    		/// <summary>
    		/// Vehicle
    		/// </summary>        
    	//    [DisplayName("Vehicle")]
            [MaxLength(50, ErrorMessage = "Vehicle cannot be longer than 50 characters")]
    		public string  Vehicle { get; set; }
    
    		    
    		/// <summary>
    		/// Cartage By
    		/// </summary>        
    	//    [DisplayName("Cartage By")]
            [MaxLength(500, ErrorMessage = "Cartage By cannot be longer than 500 characters")]
    		public string  CartageBy { get; set; }
    
    		    
    		/// <summary>
    		/// ETA
    		/// </summary>        
    	//    [DisplayName("ETA")]
            [MaxLength(50, ErrorMessage = "ETA cannot be longer than 50 characters")]
    		public string  ETA { get; set; }
    
    		    
    		/// <summary>
    		/// Storage
    		/// </summary>        
    	//    [DisplayName("Storage")]
            [MaxLength(50, ErrorMessage = "Storage cannot be longer than 50 characters")]
    		public string  Storage { get; set; }
    
    		    
    		/// <summary>
    		/// Created By
    		/// </summary>        
    	//    [DisplayName("Created By")]
            [MaxLength(50, ErrorMessage = "Created By cannot be longer than 50 characters")]
    		public string  CreatedBy { get; set; }
    
    		    
    		/// <summary>
    		/// Created DT
    		/// </summary>        
    	//    [DisplayName("Created DT")]
    		public Nullable<System.DateTime>  CreatedDT { get; set; }
    
    		    
    		/// <summary>
    		/// Updated By
    		/// </summary>        
    	//    [DisplayName("Updated By")]
            [MaxLength(50, ErrorMessage = "Updated By cannot be longer than 50 characters")]
    		public string  UpdatedBy { get; set; }
    
    		    
    		/// <summary>
    		/// Updated DT
    		/// </summary>        
    	//    [DisplayName("Updated DT")]
    		public Nullable<System.DateTime>  UpdatedDT { get; set; }
    
    		    
    		/// <summary>
    		/// Report Name
    		/// </summary>        
    	//    [DisplayName("Report Name")]
            [MaxLength(250, ErrorMessage = "Report Name cannot be longer than 250 characters")]
    		public string  ReportName { get; set; }
    
    		    
    	}
    //}
    
}
