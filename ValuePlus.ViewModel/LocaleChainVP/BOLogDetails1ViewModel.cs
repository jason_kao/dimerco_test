using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ValuePlus.ViewModel;



namespace ValuePlus.ViewModel.LocaleChainVP
{
    
    /// <summary>
    /// BOLogDetails1 class
    /// </summary>
    //[MetadataType(typeof(BOLogDetails1ViewModel))]
    //public  partial class BOLogDetails1
    //{
    
    	/// <summary>
    	/// BOLogDetails1 Metadata class
    	/// </summary>
    	public   class BOLogDetails1ViewModel:ViewModelBase
    	{
    		    
    		/// <summary>
    		/// Id
    		/// </summary>        
    	//    [DisplayName("Id")]
            [Required(ErrorMessage = "Id is required")]
    		public int  Id { get; set; }
    
    		    
    		/// <summary>
    		/// Entry Class
    		/// </summary>        
    	//    [DisplayName("Entry Class")]
            [MaxLength(100, ErrorMessage = "Entry Class cannot be longer than 100 characters")]
    		public string  EntryClass { get; set; }
    
    		    
    		/// <summary>
    		/// Function Name
    		/// </summary>        
    	//    [DisplayName("Function Name")]
            [MaxLength(100, ErrorMessage = "Function Name cannot be longer than 100 characters")]
    		public string  FunctionName { get; set; }
    
    		    
    		/// <summary>
    		/// Function Parameters
    		/// </summary>        
    	//    [DisplayName("Function Parameters")]
            [MaxLength(255, ErrorMessage = "Function Parameters cannot be longer than 255 characters")]
    		public string  FunctionParameters { get; set; }
    
    		    
    		/// <summary>
    		/// Client IP
    		/// </summary>        
    	//    [DisplayName("Client IP")]
            [MaxLength(50, ErrorMessage = "Client IP cannot be longer than 50 characters")]
    		public string  ClientIP { get; set; }
    
    		    
    		/// <summary>
    		/// Exception Message
    		/// </summary>        
    	//    [DisplayName("Exception Message")]
    		public string  ExceptionMessage { get; set; }
    
    		    
    		/// <summary>
    		/// Created Time
    		/// </summary>        
    	//    [DisplayName("Created Time")]
    		public Nullable<System.DateTime>  CreatedTime { get; set; }
    
    		    
    		/// <summary>
    		/// Execution Seconds
    		/// </summary>        
    	//    [DisplayName("Execution Seconds")]
    		public Nullable<decimal>  ExecutionSeconds { get; set; }
    
    		    
    	}
    //}
    
}
