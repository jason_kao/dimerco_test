using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ValuePlus.ViewModel;



namespace ValuePlus.ViewModel.LocaleChainVP
{
    
    /// <summary>
    /// OEAMSContainer class
    /// </summary>
    //[MetadataType(typeof(OEAMSContainerViewModel))]
    //public  partial class OEAMSContainer
    //{
    
    	/// <summary>
    	/// OEAMSContainer Metadata class
    	/// </summary>
    	public   class OEAMSContainerViewModel:ViewModelBase
    	{
    		    
    		/// <summary>
    		/// ID
    		/// </summary>        
    	//    [DisplayName("ID")]
            [Required(ErrorMessage = "ID is required")]
    		public int  ID { get; set; }
    
    		    
    		/// <summary>
    		/// HBLID
    		/// </summary>        
    	//    [DisplayName("HBLID")]
    		public Nullable<int>  HBLID { get; set; }
    
    		    
    		/// <summary>
    		/// CNTRNo
    		/// </summary>        
    	//    [DisplayName("CNTRNo")]
            [MaxLength(50, ErrorMessage = "CNTRNo cannot be longer than 50 characters")]
    		public string  CNTRNo { get; set; }
    
    		    
    		/// <summary>
    		/// Seal Number1
    		/// </summary>        
    	//    [DisplayName("Seal Number1")]
            [MaxLength(20, ErrorMessage = "Seal Number1 cannot be longer than 20 characters")]
    		public string  SealNumber1 { get; set; }
    
    		    
    		/// <summary>
    		/// Seal Number2
    		/// </summary>        
    	//    [DisplayName("Seal Number2")]
            [MaxLength(20, ErrorMessage = "Seal Number2 cannot be longer than 20 characters")]
    		public string  SealNumber2 { get; set; }
    
    		    
    		/// <summary>
    		/// PCS
    		/// </summary>        
    	//    [DisplayName("PCS")]
            [MaxLength(200, ErrorMessage = "PCS cannot be longer than 200 characters")]
    		public string  PCS { get; set; }
    
    		    
    		/// <summary>
    		/// PCSUOM
    		/// </summary>        
    	//    [DisplayName("PCSUOM")]
            [MaxLength(10, ErrorMessage = "PCSUOM cannot be longer than 10 characters")]
    		public string  PCSUOM { get; set; }
    
    		    
    		/// <summary>
    		/// Net Weight
    		/// </summary>        
    	//    [DisplayName("Net Weight")]
            [MaxLength(200, ErrorMessage = "Net Weight cannot be longer than 200 characters")]
    		public string  NetWeight { get; set; }
    
    		    
    		/// <summary>
    		/// Net Weight UOM
    		/// </summary>        
    	//    [DisplayName("Net Weight UOM")]
            [MaxLength(10, ErrorMessage = "Net Weight UOM cannot be longer than 10 characters")]
    		public string  NetWeightUOM { get; set; }
    
    		    
    		/// <summary>
    		/// Load Or Empty
    		/// </summary>        
    	//    [DisplayName("Load Or Empty")]
            [MaxLength(5, ErrorMessage = "Load Or Empty cannot be longer than 5 characters")]
    		public string  LoadOrEmpty { get; set; }
    
    		    
    		/// <summary>
    		/// Type Of Service
    		/// </summary>        
    	//    [DisplayName("Type Of Service")]
            [MaxLength(10, ErrorMessage = "Type Of Service cannot be longer than 10 characters")]
    		public string  TypeOfService { get; set; }
    
    		    
    		/// <summary>
    		/// Country Of Origin
    		/// </summary>        
    	//    [DisplayName("Country Of Origin")]
            [MaxLength(20, ErrorMessage = "Country Of Origin cannot be longer than 20 characters")]
    		public string  CountryOfOrigin { get; set; }
    
    		    
    		/// <summary>
    		/// Description
    		/// </summary>        
    	//    [DisplayName("Description")]
            [MaxLength(1000, ErrorMessage = "Description cannot be longer than 1000 characters")]
    		public string  Description { get; set; }
    
    		    
    		/// <summary>
    		/// Marks
    		/// </summary>        
    	//    [DisplayName("Marks")]
            [MaxLength(1000, ErrorMessage = "Marks cannot be longer than 1000 characters")]
    		public string  Marks { get; set; }
    
    		    
    		/// <summary>
    		/// Commodity Code
    		/// </summary>        
    	//    [DisplayName("Commodity Code")]
            [MaxLength(200, ErrorMessage = "Commodity Code cannot be longer than 200 characters")]
    		public string  CommodityCode { get; set; }
    
    		    
    		/// <summary>
    		/// Cash Value
    		/// </summary>        
    	//    [DisplayName("Cash Value")]
            [MaxLength(200, ErrorMessage = "Cash Value cannot be longer than 200 characters")]
    		public string  CashValue { get; set; }
    
    		    
    		/// <summary>
    		/// Hazard Code
    		/// </summary>        
    	//    [DisplayName("Hazard Code")]
            [MaxLength(50, ErrorMessage = "Hazard Code cannot be longer than 50 characters")]
    		public string  HazardCode { get; set; }
    
    		    
    		/// <summary>
    		/// Hazard Code Qualifier
    		/// </summary>        
    	//    [DisplayName("Hazard Code Qualifier")]
            [MaxLength(20, ErrorMessage = "Hazard Code Qualifier cannot be longer than 20 characters")]
    		public string  HazardCodeQualifier { get; set; }
    
    		    
    		/// <summary>
    		/// CTNRType
    		/// </summary>        
    	//    [DisplayName("CTNRType")]
            [MaxLength(20, ErrorMessage = "CTNRType cannot be longer than 20 characters")]
    		public string  CTNRType { get; set; }
    
    		    
    	}
    //}
    
}
