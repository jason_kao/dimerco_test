using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ValuePlus.ViewModel;



namespace ValuePlus.ViewModel.LocaleChainVP
{
    
    /// <summary>
    /// OIHBLAgentInfoMap class
    /// </summary>
    //[MetadataType(typeof(OIHBLAgentInfoMapViewModel))]
    //public  partial class OIHBLAgentInfoMap
    //{
    
    	/// <summary>
    	/// OIHBLAgentInfoMap Metadata class
    	/// </summary>
    	public   class OIHBLAgentInfoMapViewModel:ViewModelBase
    	{
    		    
    		/// <summary>
    		/// ID
    		/// </summary>        
    	//    [DisplayName("ID")]
            [Required(ErrorMessage = "ID is required")]
    		public int  ID { get; set; }
    
    		    
    		/// <summary>
    		/// Station ID
    		/// </summary>        
    	//    [DisplayName("Station ID")]
            [MaxLength(50, ErrorMessage = "Station ID cannot be longer than 50 characters")]
    		public string  StationID { get; set; }
    
    		    
    		/// <summary>
    		/// Agent ID
    		/// </summary>        
    	//    [DisplayName("Agent ID")]
            [Required(ErrorMessage = "Agent ID is required")]
    		public int  AgentID { get; set; }
    
    		    
    		/// <summary>
    		/// Map Customer ID
    		/// </summary>        
    	//    [DisplayName("Map Customer ID")]
    		public Nullable<int>  MapCustomerID { get; set; }
    
    		    
    		/// <summary>
    		/// Company Code
    		/// </summary>        
    	//    [DisplayName("Company Code")]
            [MaxLength(50, ErrorMessage = "Company Code cannot be longer than 50 characters")]
    		public string  CompanyCode { get; set; }
    
    		    
    		/// <summary>
    		/// Company Name
    		/// </summary>        
    	//    [DisplayName("Company Name")]
            [MaxLength(80, ErrorMessage = "Company Name cannot be longer than 80 characters")]
    		public string  CompanyName { get; set; }
    
    		    
    		/// <summary>
    		/// Company Address
    		/// </summary>        
    	//    [DisplayName("Company Address")]
            [Required(ErrorMessage = "Company Address is required")]
            [MaxLength(200, ErrorMessage = "Company Address cannot be longer than 200 characters")]
    		public string  CompanyAddress { get; set; }
    
    		    
    		/// <summary>
    		/// Created By
    		/// </summary>        
    	//    [DisplayName("Created By")]
            [MaxLength(50, ErrorMessage = "Created By cannot be longer than 50 characters")]
    		public string  CreatedBy { get; set; }
    
    		    
    		/// <summary>
    		/// Created Date
    		/// </summary>        
    	//    [DisplayName("Created Date")]
    		public Nullable<System.DateTime>  CreatedDate { get; set; }
    
    		    
    		/// <summary>
    		/// Updated By
    		/// </summary>        
    	//    [DisplayName("Updated By")]
            [MaxLength(50, ErrorMessage = "Updated By cannot be longer than 50 characters")]
    		public string  UpdatedBy { get; set; }
    
    		    
    		/// <summary>
    		/// Updated Date
    		/// </summary>        
    	//    [DisplayName("Updated Date")]
    		public Nullable<System.DateTime>  UpdatedDate { get; set; }
    
    		    
    	}
    //}
    
}
