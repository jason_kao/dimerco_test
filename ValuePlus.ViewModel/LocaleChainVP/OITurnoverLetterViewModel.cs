using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ValuePlus.ViewModel;



namespace ValuePlus.ViewModel.LocaleChainVP
{
    
    /// <summary>
    /// OITurnoverLetter class
    /// </summary>
    //[MetadataType(typeof(OITurnoverLetterViewModel))]
    //public  partial class OITurnoverLetter
    //{
    
    	/// <summary>
    	/// OITurnoverLetter Metadata class
    	/// </summary>
    	public   class OITurnoverLetterViewModel:ViewModelBase
    	{
    		    
    		/// <summary>
    		/// ID
    		/// </summary>        
    	//    [DisplayName("ID")]
            [Required(ErrorMessage = "ID is required")]
    		public int  ID { get; set; }
    
    		    
    		/// <summary>
    		/// HBLID
    		/// </summary>        
    	//    [DisplayName("HBLID")]
            [MaxLength(50, ErrorMessage = "HBLID cannot be longer than 50 characters")]
    		public string  HBLID { get; set; }
    
    		    
    		/// <summary>
    		/// Mode
    		/// </summary>        
    	//    [DisplayName("Mode")]
            [MaxLength(5, ErrorMessage = "Mode cannot be longer than 5 characters")]
    		public string  Mode { get; set; }
    
    		    
    		/// <summary>
    		/// Station ID
    		/// </summary>        
    	//    [DisplayName("Station ID")]
            [MaxLength(5, ErrorMessage = "Station ID cannot be longer than 5 characters")]
    		public string  StationID { get; set; }
    
    		    
    		/// <summary>
    		/// Station Name
    		/// </summary>        
    	//    [DisplayName("Station Name")]
            [MaxLength(200, ErrorMessage = "Station Name cannot be longer than 200 characters")]
    		public string  StationName { get; set; }
    
    		    
    		/// <summary>
    		/// Station Address
    		/// </summary>        
    	//    [DisplayName("Station Address")]
            [MaxLength(200, ErrorMessage = "Station Address cannot be longer than 200 characters")]
    		public string  StationAddress { get; set; }
    
    		    
    		/// <summary>
    		/// Station Tel Fax
    		/// </summary>        
    	//    [DisplayName("Station Tel Fax")]
            [MaxLength(100, ErrorMessage = "Station Tel Fax cannot be longer than 100 characters")]
    		public string  StationTelFax { get; set; }
    
    		    
    		/// <summary>
    		/// Attention
    		/// </summary>        
    	//    [DisplayName("Attention")]
            [MaxLength(200, ErrorMessage = "Attention cannot be longer than 200 characters")]
    		public string  Attention { get; set; }
    
    		    
    		/// <summary>
    		/// CNEE
    		/// </summary>        
    	//    [DisplayName("CNEE")]
            [MaxLength(200, ErrorMessage = "CNEE cannot be longer than 200 characters")]
    		public string  CNEE { get; set; }
    
    		    
    		/// <summary>
    		/// Freight Location
    		/// </summary>        
    	//    [DisplayName("Freight Location")]
            [MaxLength(200, ErrorMessage = "Freight Location cannot be longer than 200 characters")]
    		public string  FreightLocation { get; set; }
    
    		    
    		/// <summary>
    		/// Master BLNo
    		/// </summary>        
    	//    [DisplayName("Master BLNo")]
            [MaxLength(50, ErrorMessage = "Master BLNo cannot be longer than 50 characters")]
    		public string  MasterBLNo { get; set; }
    
    		    
    		/// <summary>
    		/// House BLNo
    		/// </summary>        
    	//    [DisplayName("House BLNo")]
            [MaxLength(50, ErrorMessage = "House BLNo cannot be longer than 50 characters")]
    		public string  HouseBLNo { get; set; }
    
    		    
    		/// <summary>
    		/// AMSBLNo
    		/// </summary>        
    	//    [DisplayName("AMSBLNo")]
            [MaxLength(50, ErrorMessage = "AMSBLNo cannot be longer than 50 characters")]
    		public string  AMSBLNo { get; set; }
    
    		    
    		/// <summary>
    		/// Sub HBLNo
    		/// </summary>        
    	//    [DisplayName("Sub HBLNo")]
            [MaxLength(50, ErrorMessage = "Sub HBLNo cannot be longer than 50 characters")]
    		public string  SubHBLNo { get; set; }
    
    		    
    		/// <summary>
    		/// Vessel
    		/// </summary>        
    	//    [DisplayName("Vessel")]
            [MaxLength(50, ErrorMessage = "Vessel cannot be longer than 50 characters")]
    		public string  Vessel { get; set; }
    
    		    
    		/// <summary>
    		/// Voyage
    		/// </summary>        
    	//    [DisplayName("Voyage")]
            [MaxLength(50, ErrorMessage = "Voyage cannot be longer than 50 characters")]
    		public string  Voyage { get; set; }
    
    		    
    		/// <summary>
    		/// Port Loading
    		/// </summary>        
    	//    [DisplayName("Port Loading")]
            [MaxLength(50, ErrorMessage = "Port Loading cannot be longer than 50 characters")]
    		public string  PortLoading { get; set; }
    
    		    
    		/// <summary>
    		/// Port Discharge
    		/// </summary>        
    	//    [DisplayName("Port Discharge")]
            [MaxLength(50, ErrorMessage = "Port Discharge cannot be longer than 50 characters")]
    		public string  PortDischarge { get; set; }
    
    		    
    		/// <summary>
    		/// Last Free Day
    		/// </summary>        
    	//    [DisplayName("Last Free Day")]
            [MaxLength(50, ErrorMessage = "Last Free Day cannot be longer than 50 characters")]
    		public string  LastFreeDay { get; set; }
    
    		    
    		/// <summary>
    		/// Place Delivery
    		/// </summary>        
    	//    [DisplayName("Place Delivery")]
            [MaxLength(50, ErrorMessage = "Place Delivery cannot be longer than 50 characters")]
    		public string  PlaceDelivery { get; set; }
    
    		    
    		/// <summary>
    		/// ITNo
    		/// </summary>        
    	//    [DisplayName("ITNo")]
            [MaxLength(50, ErrorMessage = "ITNo cannot be longer than 50 characters")]
    		public string  ITNo { get; set; }
    
    		    
    		/// <summary>
    		/// ITDate
    		/// </summary>        
    	//    [DisplayName("ITDate")]
            [MaxLength(50, ErrorMessage = "ITDate cannot be longer than 50 characters")]
    		public string  ITDate { get; set; }
    
    		    
    		/// <summary>
    		/// ITPort
    		/// </summary>        
    	//    [DisplayName("ITPort")]
            [MaxLength(50, ErrorMessage = "ITPort cannot be longer than 50 characters")]
    		public string  ITPort { get; set; }
    
    		    
    		/// <summary>
    		/// Container No
    		/// </summary>        
    	//    [DisplayName("Container No")]
            [MaxLength(4000, ErrorMessage = "Container No cannot be longer than 4000 characters")]
    		public string  ContainerNo { get; set; }
    
    		    
    		/// <summary>
    		/// No Of Pkgs
    		/// </summary>        
    	//    [DisplayName("No Of Pkgs")]
            [MaxLength(300, ErrorMessage = "No Of Pkgs cannot be longer than 300 characters")]
    		public string  NoOfPkgs { get; set; }
    
    		    
    		/// <summary>
    		/// Goods
    		/// </summary>        
    	//    [DisplayName("Goods")]
            [MaxLength(4000, ErrorMessage = "Goods cannot be longer than 4000 characters")]
    		public string  Goods { get; set; }
    
    		    
    		/// <summary>
    		/// GWT
    		/// </summary>        
    	//    [DisplayName("GWT")]
            [MaxLength(300, ErrorMessage = "GWT cannot be longer than 300 characters")]
    		public string  GWT { get; set; }
    
    		    
    		/// <summary>
    		/// Measurement
    		/// </summary>        
    	//    [DisplayName("Measurement")]
            [MaxLength(300, ErrorMessage = "Measurement cannot be longer than 300 characters")]
    		public string  Measurement { get; set; }
    
    		    
    		/// <summary>
    		/// Remarks
    		/// </summary>        
    	//    [DisplayName("Remarks")]
            [MaxLength(1000, ErrorMessage = "Remarks cannot be longer than 1000 characters")]
    		public string  Remarks { get; set; }
    
    		    
    		/// <summary>
    		/// Prepared By
    		/// </summary>        
    	//    [DisplayName("Prepared By")]
            [MaxLength(50, ErrorMessage = "Prepared By cannot be longer than 50 characters")]
    		public string  PreparedBy { get; set; }
    
    		    
    		/// <summary>
    		/// Prepared DT
    		/// </summary>        
    	//    [DisplayName("Prepared DT")]
            [MaxLength(50, ErrorMessage = "Prepared DT cannot be longer than 50 characters")]
    		public string  PreparedDT { get; set; }
    
    		    
    		/// <summary>
    		/// Created By
    		/// </summary>        
    	//    [DisplayName("Created By")]
            [MaxLength(50, ErrorMessage = "Created By cannot be longer than 50 characters")]
    		public string  CreatedBy { get; set; }
    
    		    
    		/// <summary>
    		/// Created DT
    		/// </summary>        
    	//    [DisplayName("Created DT")]
    		public Nullable<System.DateTime>  CreatedDT { get; set; }
    
    		    
    		/// <summary>
    		/// Updated By
    		/// </summary>        
    	//    [DisplayName("Updated By")]
            [MaxLength(50, ErrorMessage = "Updated By cannot be longer than 50 characters")]
    		public string  UpdatedBy { get; set; }
    
    		    
    		/// <summary>
    		/// Updated DT
    		/// </summary>        
    	//    [DisplayName("Updated DT")]
    		public Nullable<System.DateTime>  UpdatedDT { get; set; }
    
    		    
    	}
    //}
    
}
