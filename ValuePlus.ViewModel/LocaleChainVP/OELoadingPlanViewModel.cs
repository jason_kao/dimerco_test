using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ValuePlus.ViewModel;



namespace ValuePlus.ViewModel.LocaleChainVP
{
    
    /// <summary>
    /// OELoadingPlan class
    /// </summary>
    //[MetadataType(typeof(OELoadingPlanViewModel))]
    //public  partial class OELoadingPlan
    //{
    
    	/// <summary>
    	/// OELoadingPlan Metadata class
    	/// </summary>
    	public   class OELoadingPlanViewModel:ViewModelBase
    	{
    		    
    		/// <summary>
    		/// ID
    		/// </summary>        
    	//    [DisplayName("ID")]
            [Required(ErrorMessage = "ID is required")]
    		public int  ID { get; set; }
    
    		    
    		/// <summary>
    		/// Station ID
    		/// </summary>        
    	//    [DisplayName("Station ID")]
            [MaxLength(5, ErrorMessage = "Station ID cannot be longer than 5 characters")]
    		public string  StationID { get; set; }
    
    		    
    		/// <summary>
    		/// Load Plan ID
    		/// </summary>        
    	//    [DisplayName("Load Plan ID")]
    		public Nullable<int>  LoadPlanID { get; set; }
    
    		    
    		/// <summary>
    		/// MBLID
    		/// </summary>        
    	//    [DisplayName("MBLID")]
    		public Nullable<int>  MBLID { get; set; }
    
    		    
    		/// <summary>
    		/// Loading Plan No
    		/// </summary>        
    	//    [DisplayName("Loading Plan No")]
            [MaxLength(20, ErrorMessage = "Loading Plan No cannot be longer than 20 characters")]
    		public string  LoadingPlanNo { get; set; }
    
    		    
    		/// <summary>
    		/// Ocean Vessel
    		/// </summary>        
    	//    [DisplayName("Ocean Vessel")]
            [MaxLength(60, ErrorMessage = "Ocean Vessel cannot be longer than 60 characters")]
    		public string  OceanVessel { get; set; }
    
    		    
    		/// <summary>
    		/// Ocean Voyage
    		/// </summary>        
    	//    [DisplayName("Ocean Voyage")]
            [MaxLength(60, ErrorMessage = "Ocean Voyage cannot be longer than 60 characters")]
    		public string  OceanVoyage { get; set; }
    
    		    
    		/// <summary>
    		/// SHPR
    		/// </summary>        
    	//    [DisplayName("SHPR")]
    		public Nullable<int>  SHPR { get; set; }
    
    		    
    		/// <summary>
    		/// Co Loader Agent
    		/// </summary>        
    	//    [DisplayName("Co Loader Agent")]
    		public Nullable<int>  CoLoaderAgent { get; set; }
    
    		    
    		/// <summary>
    		/// Carrier SONo
    		/// </summary>        
    	//    [DisplayName("Carrier SONo")]
            [MaxLength(60, ErrorMessage = "Carrier SONo cannot be longer than 60 characters")]
    		public string  CarrierSONo { get; set; }
    
    		    
    		/// <summary>
    		/// CTNRType
    		/// </summary>        
    	//    [DisplayName("CTNRType")]
            [MaxLength(20, ErrorMessage = "CTNRType cannot be longer than 20 characters")]
    		public string  CTNRType { get; set; }
    
    		    
    		/// <summary>
    		/// CTNRNo
    		/// </summary>        
    	//    [DisplayName("CTNRNo")]
            [MaxLength(40, ErrorMessage = "CTNRNo cannot be longer than 40 characters")]
    		public string  CTNRNo { get; set; }
    
    		    
    		/// <summary>
    		/// Seal No
    		/// </summary>        
    	//    [DisplayName("Seal No")]
            [MaxLength(40, ErrorMessage = "Seal No cannot be longer than 40 characters")]
    		public string  SealNo { get; set; }
    
    		    
    		/// <summary>
    		/// HBLCount
    		/// </summary>        
    	//    [DisplayName("HBLCount")]
    		public Nullable<int>  HBLCount { get; set; }
    
    		    
    		/// <summary>
    		/// PCS
    		/// </summary>        
    	//    [DisplayName("PCS")]
    		public Nullable<int>  PCS { get; set; }
    
    		    
    		/// <summary>
    		/// Customer
    		/// </summary>        
    	//    [DisplayName("Customer")]
    		public Nullable<int>  Customer { get; set; }
    
    		    
    		/// <summary>
    		/// CNEE
    		/// </summary>        
    	//    [DisplayName("CNEE")]
    		public Nullable<int>  CNEE { get; set; }
    
    		    
    		/// <summary>
    		/// Co Loader
    		/// </summary>        
    	//    [DisplayName("Co Loader")]
    		public Nullable<int>  CoLoader { get; set; }
    
    		    
    		/// <summary>
    		/// NTFY
    		/// </summary>        
    	//    [DisplayName("NTFY")]
    		public Nullable<int>  NTFY { get; set; }
    
    		    
    		/// <summary>
    		/// Booking Party
    		/// </summary>        
    	//    [DisplayName("Booking Party")]
    		public Nullable<int>  BookingParty { get; set; }
    
    		    
    		/// <summary>
    		/// Carrier
    		/// </summary>        
    	//    [DisplayName("Carrier")]
    		public Nullable<int>  Carrier { get; set; }
    
    		    
    		/// <summary>
    		/// Shpt Type
    		/// </summary>        
    	//    [DisplayName("Shpt Type")]
            [MaxLength(15, ErrorMessage = "Shpt Type cannot be longer than 15 characters")]
    		public string  ShptType { get; set; }
    
    		    
    		/// <summary>
    		/// Move Type
    		/// </summary>        
    	//    [DisplayName("Move Type")]
            [MaxLength(15, ErrorMessage = "Move Type cannot be longer than 15 characters")]
    		public string  MoveType { get; set; }
    
    		    
    		/// <summary>
    		/// Freight Pay Type
    		/// </summary>        
    	//    [DisplayName("Freight Pay Type")]
            [MaxLength(15, ErrorMessage = "Freight Pay Type cannot be longer than 15 characters")]
    		public string  FreightPayType { get; set; }
    
    		    
    		/// <summary>
    		/// PReceipt
    		/// </summary>        
    	//    [DisplayName("PReceipt")]
    		public Nullable<int>  PReceipt { get; set; }
    
    		    
    		/// <summary>
    		/// PReceipt ETD
    		/// </summary>        
    	//    [DisplayName("PReceipt ETD")]
    		public Nullable<System.DateTime>  PReceiptETD { get; set; }
    
    		    
    		/// <summary>
    		/// PReceipt ATD
    		/// </summary>        
    	//    [DisplayName("PReceipt ATD")]
    		public Nullable<System.DateTime>  PReceiptATD { get; set; }
    
    		    
    		/// <summary>
    		/// PLoading
    		/// </summary>        
    	//    [DisplayName("PLoading")]
    		public Nullable<int>  PLoading { get; set; }
    
    		    
    		/// <summary>
    		/// PLoading ETD
    		/// </summary>        
    	//    [DisplayName("PLoading ETD")]
    		public Nullable<System.DateTime>  PLoadingETD { get; set; }
    
    		    
    		/// <summary>
    		/// PLoading ATD
    		/// </summary>        
    	//    [DisplayName("PLoading ATD")]
    		public Nullable<System.DateTime>  PLoadingATD { get; set; }
    
    		    
    		/// <summary>
    		/// PDischarge
    		/// </summary>        
    	//    [DisplayName("PDischarge")]
    		public Nullable<int>  PDischarge { get; set; }
    
    		    
    		/// <summary>
    		/// PDischarge ETD
    		/// </summary>        
    	//    [DisplayName("PDischarge ETD")]
    		public Nullable<System.DateTime>  PDischargeETD { get; set; }
    
    		    
    		/// <summary>
    		/// PDischarge ATD
    		/// </summary>        
    	//    [DisplayName("PDischarge ATD")]
    		public Nullable<System.DateTime>  PDischargeATD { get; set; }
    
    		    
    		/// <summary>
    		/// PDelivery
    		/// </summary>        
    	//    [DisplayName("PDelivery")]
    		public Nullable<int>  PDelivery { get; set; }
    
    		    
    		/// <summary>
    		/// PDelivery ETD
    		/// </summary>        
    	//    [DisplayName("PDelivery ETD")]
    		public Nullable<System.DateTime>  PDeliveryETD { get; set; }
    
    		    
    		/// <summary>
    		/// PDelivery ATD
    		/// </summary>        
    	//    [DisplayName("PDelivery ATD")]
    		public Nullable<System.DateTime>  PDeliveryATD { get; set; }
    
    		    
    		/// <summary>
    		/// Status
    		/// </summary>        
    	//    [DisplayName("Status")]
            [MaxLength(10, ErrorMessage = "Status cannot be longer than 10 characters")]
    		public string  Status { get; set; }
    
    		    
    		/// <summary>
    		/// Created By
    		/// </summary>        
    	//    [DisplayName("Created By")]
            [MaxLength(50, ErrorMessage = "Created By cannot be longer than 50 characters")]
    		public string  CreatedBy { get; set; }
    
    		    
    		/// <summary>
    		/// Created DT
    		/// </summary>        
    	//    [DisplayName("Created DT")]
    		public Nullable<System.DateTime>  CreatedDT { get; set; }
    
    		    
    		/// <summary>
    		/// Updated By
    		/// </summary>        
    	//    [DisplayName("Updated By")]
            [MaxLength(50, ErrorMessage = "Updated By cannot be longer than 50 characters")]
    		public string  UpdatedBy { get; set; }
    
    		    
    		/// <summary>
    		/// Updated DT
    		/// </summary>        
    	//    [DisplayName("Updated DT")]
    		public Nullable<System.DateTime>  UpdatedDT { get; set; }
    
    		    
    		/// <summary>
    		/// Version
    		/// </summary>        
    	//    [DisplayName("Version")]
            [Required(ErrorMessage = "Version is required")]
            [MaxLength(8, ErrorMessage = "Version cannot be longer than 8 characters")]
    		public byte[]  Version { get; set; }
    
    		    
    		/// <summary>
    		/// Instruction
    		/// </summary>        
    	//    [DisplayName("Instruction")]
            [MaxLength(255, ErrorMessage = "Instruction cannot be longer than 255 characters")]
    		public string  Instruction { get; set; }
    
    		    
    	}
    //}
    
}
