using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ValuePlus.ViewModel;



namespace ValuePlus.ViewModel.LocaleChainVP
{
    
    /// <summary>
    /// OECustomerEBookingSO class
    /// </summary>
    //[MetadataType(typeof(OECustomerEBookingSOViewModel))]
    //public  partial class OECustomerEBookingSO
    //{
    
    	/// <summary>
    	/// OECustomerEBookingSO Metadata class
    	/// </summary>
    	public   class OECustomerEBookingSOViewModel:ViewModelBase
    	{
    		    
    		/// <summary>
    		/// ID
    		/// </summary>        
    	//    [DisplayName("ID")]
            [Required(ErrorMessage = "ID is required")]
    		public int  ID { get; set; }
    
    		    
    		/// <summary>
    		/// Station ID
    		/// </summary>        
    	//    [DisplayName("Station ID")]
            [MaxLength(5, ErrorMessage = "Station ID cannot be longer than 5 characters")]
    		public string  StationID { get; set; }
    
    		    
    		/// <summary>
    		/// Src ID
    		/// </summary>        
    	//    [DisplayName("Src ID")]
    		public Nullable<int>  SrcID { get; set; }
    
    		    
    		/// <summary>
    		/// BKID
    		/// </summary>        
    	//    [DisplayName("BKID")]
    		public Nullable<int>  BKID { get; set; }
    
    		    
    		/// <summary>
    		/// HBLID
    		/// </summary>        
    	//    [DisplayName("HBLID")]
    		public Nullable<int>  HBLID { get; set; }
    
    		    
    		/// <summary>
    		/// PCS
    		/// </summary>        
    	//    [DisplayName("PCS")]
    		public Nullable<int>  PCS { get; set; }
    
    		    
    		/// <summary>
    		/// PCSUOM
    		/// </summary>        
    	//    [DisplayName("PCSUOM")]
            [MaxLength(10, ErrorMessage = "PCSUOM cannot be longer than 10 characters")]
    		public string  PCSUOM { get; set; }
    
    		    
    		/// <summary>
    		/// Weight
    		/// </summary>        
    	//    [DisplayName("Weight")]
    		public Nullable<decimal>  Weight { get; set; }
    
    		    
    		/// <summary>
    		/// Weight UOM
    		/// </summary>        
    	//    [DisplayName("Weight UOM")]
            [MaxLength(10, ErrorMessage = "Weight UOM cannot be longer than 10 characters")]
    		public string  WeightUOM { get; set; }
    
    		    
    		/// <summary>
    		/// CBM
    		/// </summary>        
    	//    [DisplayName("CBM")]
    		public Nullable<decimal>  CBM { get; set; }
    
    		    
    		/// <summary>
    		/// CBMUOM
    		/// </summary>        
    	//    [DisplayName("CBMUOM")]
            [MaxLength(10, ErrorMessage = "CBMUOM cannot be longer than 10 characters")]
    		public string  CBMUOM { get; set; }
    
    		    
    		/// <summary>
    		/// CTNRType
    		/// </summary>        
    	//    [DisplayName("CTNRType")]
            [MaxLength(20, ErrorMessage = "CTNRType cannot be longer than 20 characters")]
    		public string  CTNRType { get; set; }
    
    		    
    		/// <summary>
    		/// Marks
    		/// </summary>        
    	//    [DisplayName("Marks")]
            [MaxLength(1000, ErrorMessage = "Marks cannot be longer than 1000 characters")]
    		public string  Marks { get; set; }
    
    		    
    		/// <summary>
    		/// Description
    		/// </summary>        
    	//    [DisplayName("Description")]
            [MaxLength(1000, ErrorMessage = "Description cannot be longer than 1000 characters")]
    		public string  Description { get; set; }
    
    		    
    		/// <summary>
    		/// Created By
    		/// </summary>        
    	//    [DisplayName("Created By")]
            [MaxLength(50, ErrorMessage = "Created By cannot be longer than 50 characters")]
    		public string  CreatedBy { get; set; }
    
    		    
    		/// <summary>
    		/// Created DT
    		/// </summary>        
    	//    [DisplayName("Created DT")]
    		public Nullable<System.DateTime>  CreatedDT { get; set; }
    
    		    
    		/// <summary>
    		/// Updated By
    		/// </summary>        
    	//    [DisplayName("Updated By")]
            [MaxLength(50, ErrorMessage = "Updated By cannot be longer than 50 characters")]
    		public string  UpdatedBy { get; set; }
    
    		    
    		/// <summary>
    		/// Updated DT
    		/// </summary>        
    	//    [DisplayName("Updated DT")]
    		public Nullable<System.DateTime>  UpdatedDT { get; set; }
    
    		    
    	}
    //}
    
}
