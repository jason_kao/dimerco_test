using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ValuePlus.ViewModel;



namespace ValuePlus.ViewModel.LocaleChainVP
{
    
    /// <summary>
    /// OEBookingConfirmation class
    /// </summary>
    //[MetadataType(typeof(OEBookingConfirmationViewModel))]
    //public  partial class OEBookingConfirmation
    //{
    
    	/// <summary>
    	/// OEBookingConfirmation Metadata class
    	/// </summary>
    	public   class OEBookingConfirmationViewModel:ViewModelBase
    	{
    		    
    		/// <summary>
    		/// ID
    		/// </summary>        
    	//    [DisplayName("ID")]
            [Required(ErrorMessage = "ID is required")]
    		public int  ID { get; set; }
    
    		    
    		/// <summary>
    		/// HBLID
    		/// </summary>        
    	//    [DisplayName("HBLID")]
            [MaxLength(50, ErrorMessage = "HBLID cannot be longer than 50 characters")]
    		public string  HBLID { get; set; }
    
    		    
    		/// <summary>
    		/// Mode
    		/// </summary>        
    	//    [DisplayName("Mode")]
            [MaxLength(5, ErrorMessage = "Mode cannot be longer than 5 characters")]
    		public string  Mode { get; set; }
    
    		    
    		/// <summary>
    		/// Station ID
    		/// </summary>        
    	//    [DisplayName("Station ID")]
            [MaxLength(5, ErrorMessage = "Station ID cannot be longer than 5 characters")]
    		public string  StationID { get; set; }
    
    		    
    		/// <summary>
    		/// Station Name
    		/// </summary>        
    	//    [DisplayName("Station Name")]
            [MaxLength(200, ErrorMessage = "Station Name cannot be longer than 200 characters")]
    		public string  StationName { get; set; }
    
    		    
    		/// <summary>
    		/// Station Address
    		/// </summary>        
    	//    [DisplayName("Station Address")]
            [MaxLength(200, ErrorMessage = "Station Address cannot be longer than 200 characters")]
    		public string  StationAddress { get; set; }
    
    		    
    		/// <summary>
    		/// Station Tel Fax
    		/// </summary>        
    	//    [DisplayName("Station Tel Fax")]
            [MaxLength(100, ErrorMessage = "Station Tel Fax cannot be longer than 100 characters")]
    		public string  StationTelFax { get; set; }
    
    		    
    		/// <summary>
    		/// Attention
    		/// </summary>        
    	//    [DisplayName("Attention")]
            [MaxLength(200, ErrorMessage = "Attention cannot be longer than 200 characters")]
    		public string  Attention { get; set; }
    
    		    
    		/// <summary>
    		/// Feeder Vessel
    		/// </summary>        
    	//    [DisplayName("Feeder Vessel")]
            [MaxLength(200, ErrorMessage = "Feeder Vessel cannot be longer than 200 characters")]
    		public string  FeederVessel { get; set; }
    
    		    
    		/// <summary>
    		/// Feeder Voyage
    		/// </summary>        
    	//    [DisplayName("Feeder Voyage")]
            [MaxLength(200, ErrorMessage = "Feeder Voyage cannot be longer than 200 characters")]
    		public string  FeederVoyage { get; set; }
    
    		    
    		/// <summary>
    		/// Ocean Vessel
    		/// </summary>        
    	//    [DisplayName("Ocean Vessel")]
            [MaxLength(200, ErrorMessage = "Ocean Vessel cannot be longer than 200 characters")]
    		public string  OceanVessel { get; set; }
    
    		    
    		/// <summary>
    		/// Ocean Voyage
    		/// </summary>        
    	//    [DisplayName("Ocean Voyage")]
            [MaxLength(200, ErrorMessage = "Ocean Voyage cannot be longer than 200 characters")]
    		public string  OceanVoyage { get; set; }
    
    		    
    		/// <summary>
    		/// Booking No
    		/// </summary>        
    	//    [DisplayName("Booking No")]
            [MaxLength(200, ErrorMessage = "Booking No cannot be longer than 200 characters")]
    		public string  BookingNo { get; set; }
    
    		    
    		/// <summary>
    		/// Reference No
    		/// </summary>        
    	//    [DisplayName("Reference No")]
            [MaxLength(200, ErrorMessage = "Reference No cannot be longer than 200 characters")]
    		public string  ReferenceNo { get; set; }
    
    		    
    		/// <summary>
    		/// Ship Call No
    		/// </summary>        
    	//    [DisplayName("Ship Call No")]
            [MaxLength(200, ErrorMessage = "Ship Call No cannot be longer than 200 characters")]
    		public string  ShipCallNo { get; set; }
    
    		    
    		/// <summary>
    		/// Place Of Del
    		/// </summary>        
    	//    [DisplayName("Place Of Del")]
            [MaxLength(200, ErrorMessage = "Place Of Del cannot be longer than 200 characters")]
    		public string  PlaceOfDel { get; set; }
    
    		    
    		/// <summary>
    		/// Port Of Loading
    		/// </summary>        
    	//    [DisplayName("Port Of Loading")]
            [MaxLength(200, ErrorMessage = "Port Of Loading cannot be longer than 200 characters")]
    		public string  PortOfLoading { get; set; }
    
    		    
    		/// <summary>
    		/// ETD
    		/// </summary>        
    	//    [DisplayName("ETD")]
            [MaxLength(200, ErrorMessage = "ETD cannot be longer than 200 characters")]
    		public string  ETD { get; set; }
    
    		    
    		/// <summary>
    		/// Port Of Dis Charge
    		/// </summary>        
    	//    [DisplayName("Port Of Dis Charge")]
            [MaxLength(200, ErrorMessage = "Port Of Dis Charge cannot be longer than 200 characters")]
    		public string  PortOfDisCharge { get; set; }
    
    		    
    		/// <summary>
    		/// ETA
    		/// </summary>        
    	//    [DisplayName("ETA")]
            [MaxLength(200, ErrorMessage = "ETA cannot be longer than 200 characters")]
    		public string  ETA { get; set; }
    
    		    
    		/// <summary>
    		/// Container No
    		/// </summary>        
    	//    [DisplayName("Container No")]
            [MaxLength(500, ErrorMessage = "Container No cannot be longer than 500 characters")]
    		public string  ContainerNo { get; set; }
    
    		    
    		/// <summary>
    		/// No Of Pkags
    		/// </summary>        
    	//    [DisplayName("No Of Pkags")]
            [MaxLength(500, ErrorMessage = "No Of Pkags cannot be longer than 500 characters")]
    		public string  NoOfPkags { get; set; }
    
    		    
    		/// <summary>
    		/// Goods
    		/// </summary>        
    	//    [DisplayName("Goods")]
            [MaxLength(500, ErrorMessage = "Goods cannot be longer than 500 characters")]
    		public string  Goods { get; set; }
    
    		    
    		/// <summary>
    		/// GWT
    		/// </summary>        
    	//    [DisplayName("GWT")]
            [MaxLength(500, ErrorMessage = "GWT cannot be longer than 500 characters")]
    		public string  GWT { get; set; }
    
    		    
    		/// <summary>
    		/// Measurement
    		/// </summary>        
    	//    [DisplayName("Measurement")]
            [MaxLength(500, ErrorMessage = "Measurement cannot be longer than 500 characters")]
    		public string  Measurement { get; set; }
    
    		    
    		/// <summary>
    		/// Eq PULocation
    		/// </summary>        
    	//    [DisplayName("Eq PULocation")]
            [MaxLength(200, ErrorMessage = "Eq PULocation cannot be longer than 200 characters")]
    		public string  EqPULocation { get; set; }
    
    		    
    		/// <summary>
    		/// Eq Return Location
    		/// </summary>        
    	//    [DisplayName("Eq Return Location")]
            [MaxLength(200, ErrorMessage = "Eq Return Location cannot be longer than 200 characters")]
    		public string  EqReturnLocation { get; set; }
    
    		    
    		/// <summary>
    		/// Loading Location
    		/// </summary>        
    	//    [DisplayName("Loading Location")]
            [MaxLength(200, ErrorMessage = "Loading Location cannot be longer than 200 characters")]
    		public string  LoadingLocation { get; set; }
    
    		    
    		/// <summary>
    		/// Close Time
    		/// </summary>        
    	//    [DisplayName("Close Time")]
            [MaxLength(200, ErrorMessage = "Close Time cannot be longer than 200 characters")]
    		public string  CloseTime { get; set; }
    
    		    
    		/// <summary>
    		/// Terminal Location
    		/// </summary>        
    	//    [DisplayName("Terminal Location")]
            [MaxLength(200, ErrorMessage = "Terminal Location cannot be longer than 200 characters")]
    		public string  TerminalLocation { get; set; }
    
    		    
    		/// <summary>
    		/// Remarks
    		/// </summary>        
    	//    [DisplayName("Remarks")]
            [MaxLength(500, ErrorMessage = "Remarks cannot be longer than 500 characters")]
    		public string  Remarks { get; set; }
    
    		    
    		/// <summary>
    		/// Faxed By
    		/// </summary>        
    	//    [DisplayName("Faxed By")]
            [MaxLength(200, ErrorMessage = "Faxed By cannot be longer than 200 characters")]
    		public string  FaxedBy { get; set; }
    
    		    
    		/// <summary>
    		/// Faxed DT
    		/// </summary>        
    	//    [DisplayName("Faxed DT")]
            [MaxLength(200, ErrorMessage = "Faxed DT cannot be longer than 200 characters")]
    		public string  FaxedDT { get; set; }
    
    		    
    		/// <summary>
    		/// Prepared By
    		/// </summary>        
    	//    [DisplayName("Prepared By")]
            [MaxLength(50, ErrorMessage = "Prepared By cannot be longer than 50 characters")]
    		public string  PreparedBy { get; set; }
    
    		    
    		/// <summary>
    		/// Prepared DT
    		/// </summary>        
    	//    [DisplayName("Prepared DT")]
            [MaxLength(50, ErrorMessage = "Prepared DT cannot be longer than 50 characters")]
    		public string  PreparedDT { get; set; }
    
    		    
    		/// <summary>
    		/// Created By
    		/// </summary>        
    	//    [DisplayName("Created By")]
            [MaxLength(50, ErrorMessage = "Created By cannot be longer than 50 characters")]
    		public string  CreatedBy { get; set; }
    
    		    
    		/// <summary>
    		/// Created DT
    		/// </summary>        
    	//    [DisplayName("Created DT")]
    		public Nullable<System.DateTime>  CreatedDT { get; set; }
    
    		    
    		/// <summary>
    		/// Updated By
    		/// </summary>        
    	//    [DisplayName("Updated By")]
            [MaxLength(50, ErrorMessage = "Updated By cannot be longer than 50 characters")]
    		public string  UpdatedBy { get; set; }
    
    		    
    		/// <summary>
    		/// Updated DT
    		/// </summary>        
    	//    [DisplayName("Updated DT")]
    		public Nullable<System.DateTime>  UpdatedDT { get; set; }
    
    		    
    		/// <summary>
    		/// OEType
    		/// </summary>        
    	//    [DisplayName("OEType")]
            [MaxLength(15, ErrorMessage = "OEType cannot be longer than 15 characters")]
    		public string  OEType { get; set; }
    
    		    
    	}
    //}
    
}
