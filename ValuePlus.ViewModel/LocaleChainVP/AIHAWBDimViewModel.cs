using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ValuePlus.ViewModel;



namespace ValuePlus.ViewModel.LocaleChainVP
{
    
    /// <summary>
    /// AIHAWBDim class
    /// </summary>
    //[MetadataType(typeof(AIHAWBDimViewModel))]
    //public  partial class AIHAWBDim
    //{
    
    	/// <summary>
    	/// AIHAWBDim Metadata class
    	/// </summary>
    	public   class AIHAWBDimViewModel:ViewModelBase
    	{
    		    
    		/// <summary>
    		/// ID
    		/// </summary>        
    	//    [DisplayName("ID")]
            [Required(ErrorMessage = "ID is required")]
    		public int  ID { get; set; }
    
    		    
    		/// <summary>
    		/// Length
    		/// </summary>        
    	//    [DisplayName("Length")]
            [Required(ErrorMessage = "Length is required")]
    		public double  Length { get; set; }
    
    		    
    		/// <summary>
    		/// Width
    		/// </summary>        
    	//    [DisplayName("Width")]
            [Required(ErrorMessage = "Width is required")]
    		public double  Width { get; set; }
    
    		    
    		/// <summary>
    		/// Height
    		/// </summary>        
    	//    [DisplayName("Height")]
            [Required(ErrorMessage = "Height is required")]
    		public double  Height { get; set; }
    
    		    
    		/// <summary>
    		/// PCS
    		/// </summary>        
    	//    [DisplayName("PCS")]
            [Required(ErrorMessage = "PCS is required")]
    		public int  PCS { get; set; }
    
    		    
    		/// <summary>
    		/// Factor
    		/// </summary>        
    	//    [DisplayName("Factor")]
    		public Nullable<double>  Factor { get; set; }
    
    		    
    		/// <summary>
    		/// VWT
    		/// </summary>        
    	//    [DisplayName("VWT")]
    		public Nullable<double>  VWT { get; set; }
    
    		    
    		/// <summary>
    		/// CBM
    		/// </summary>        
    	//    [DisplayName("CBM")]
    		public Nullable<double>  CBM { get; set; }
    
    		    
    		/// <summary>
    		/// VWTUOM
    		/// </summary>        
    	//    [DisplayName("VWTUOM")]
            [MaxLength(10, ErrorMessage = "VWTUOM cannot be longer than 10 characters")]
    		public string  VWTUOM { get; set; }
    
    		    
    		/// <summary>
    		/// DIMUOM
    		/// </summary>        
    	//    [DisplayName("DIMUOM")]
            [MaxLength(10, ErrorMessage = "DIMUOM cannot be longer than 10 characters")]
    		public string  DIMUOM { get; set; }
    
    		    
    		/// <summary>
    		/// CUFT
    		/// </summary>        
    	//    [DisplayName("CUFT")]
    		public Nullable<double>  CUFT { get; set; }
    
    		    
    		/// <summary>
    		/// HAWBID
    		/// </summary>        
    	//    [DisplayName("HAWBID")]
            [Required(ErrorMessage = "HAWBID is required")]
    		public int  HAWBID { get; set; }
    
    		    
    		/// <summary>
    		/// Station ID
    		/// </summary>        
    	//    [DisplayName("Station ID")]
            [Required(ErrorMessage = "Station ID is required")]
            [MaxLength(10, ErrorMessage = "Station ID cannot be longer than 10 characters")]
    		public string  StationID { get; set; }
    
    		    
    		/// <summary>
    		/// Created By
    		/// </summary>        
    	//    [DisplayName("Created By")]
            [Required(ErrorMessage = "Created By is required")]
            [MaxLength(6, ErrorMessage = "Created By cannot be longer than 6 characters")]
    		public string  CreatedBy { get; set; }
    
    		    
    		/// <summary>
    		/// Created Date
    		/// </summary>        
    	//    [DisplayName("Created Date")]
            [Required(ErrorMessage = "Created Date is required")]
    		public System.DateTime  CreatedDate { get; set; }
    
    		    
    		/// <summary>
    		/// Updated By
    		/// </summary>        
    	//    [DisplayName("Updated By")]
            [Required(ErrorMessage = "Updated By is required")]
            [MaxLength(6, ErrorMessage = "Updated By cannot be longer than 6 characters")]
    		public string  UpdatedBy { get; set; }
    
    		    
    		/// <summary>
    		/// Updated Date
    		/// </summary>        
    	//    [DisplayName("Updated Date")]
            [Required(ErrorMessage = "Updated Date is required")]
    		public System.DateTime  UpdatedDate { get; set; }
    
    		    
    		/// <summary>
    		/// Version
    		/// </summary>        
    	//    [DisplayName("Version")]
            [MaxLength(8, ErrorMessage = "Version cannot be longer than 8 characters")]
    		public byte[]  Version { get; set; }
    
    		    
    	}
    //}
    
}
