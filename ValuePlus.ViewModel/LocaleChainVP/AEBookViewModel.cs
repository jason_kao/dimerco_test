using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ValuePlus.ViewModel;



namespace ValuePlus.ViewModel.LocaleChainVP
{
    
    /// <summary>
    /// AEBook class
    /// </summary>
    //[MetadataType(typeof(AEBookViewModel))]
    //public  partial class AEBook
    //{
    
    	/// <summary>
    	/// AEBook Metadata class
    	/// </summary>
    	public   class AEBookViewModel:ViewModelBase
    	{
    		    
    		/// <summary>
    		/// ID
    		/// </summary>        
    	//    [DisplayName("ID")]
            [Required(ErrorMessage = "ID is required")]
    		public int  ID { get; set; }
    
    		    
    		/// <summary>
    		/// HAWBID
    		/// </summary>        
    	//    [DisplayName("HAWBID")]
            [Required(ErrorMessage = "HAWBID is required")]
    		public int  HAWBID { get; set; }
    
    		    
    		/// <summary>
    		/// Area
    		/// </summary>        
    	//    [DisplayName("Area")]
            [MaxLength(10, ErrorMessage = "Area cannot be longer than 10 characters")]
    		public string  Area { get; set; }
    
    		    
    		/// <summary>
    		/// Service Level
    		/// </summary>        
    	//    [DisplayName("Service Level")]
            [MaxLength(50, ErrorMessage = "Service Level cannot be longer than 50 characters")]
    		public string  ServiceLevel { get; set; }
    
    		    
    		/// <summary>
    		/// Booking CWT
    		/// </summary>        
    	//    [DisplayName("Booking CWT")]
    		public Nullable<decimal>  BookingCWT { get; set; }
    
    		    
    		/// <summary>
    		/// Booking Volume
    		/// </summary>        
    	//    [DisplayName("Booking Volume")]
    		public Nullable<decimal>  BookingVolume { get; set; }
    
    		    
    		/// <summary>
    		/// Shipper Local Name
    		/// </summary>        
    	//    [DisplayName("Shipper Local Name")]
            [MaxLength(100, ErrorMessage = "Shipper Local Name cannot be longer than 100 characters")]
    		public string  ShipperLocalName { get; set; }
    
    		    
    		/// <summary>
    		/// Local Commodity
    		/// </summary>        
    	//    [DisplayName("Local Commodity")]
            [MaxLength(100, ErrorMessage = "Local Commodity cannot be longer than 100 characters")]
    		public string  LocalCommodity { get; set; }
    
    		    
    		/// <summary>
    		/// Doc Type
    		/// </summary>        
    	//    [DisplayName("Doc Type")]
            [MaxLength(50, ErrorMessage = "Doc Type cannot be longer than 50 characters")]
    		public string  DocType { get; set; }
    
    		    
    		/// <summary>
    		/// DSTNStation ID
    		/// </summary>        
    	//    [DisplayName("DSTNStation ID")]
            [MaxLength(10, ErrorMessage = "DSTNStation ID cannot be longer than 10 characters")]
    		public string  DSTNStationID { get; set; }
    
    		    
    		/// <summary>
    		/// Status
    		/// </summary>        
    	//    [DisplayName("Status")]
            [MaxLength(10, ErrorMessage = "Status cannot be longer than 10 characters")]
    		public string  Status { get; set; }
    
    		    
    		/// <summary>
    		/// CS
    		/// </summary>        
    	//    [DisplayName("CS")]
            [MaxLength(50, ErrorMessage = "CS cannot be longer than 50 characters")]
    		public string  CS { get; set; }
    
    		    
    		/// <summary>
    		/// Rate
    		/// </summary>        
    	//    [DisplayName("Rate")]
            [MaxLength(50, ErrorMessage = "Rate cannot be longer than 50 characters")]
    		public string  Rate { get; set; }
    
    		    
    		/// <summary>
    		/// Remark
    		/// </summary>        
    	//    [DisplayName("Remark")]
            [MaxLength(200, ErrorMessage = "Remark cannot be longer than 200 characters")]
    		public string  Remark { get; set; }
    
    		    
    		/// <summary>
    		/// Shiping Remark
    		/// </summary>        
    	//    [DisplayName("Shiping Remark")]
            [MaxLength(200, ErrorMessage = "Shiping Remark cannot be longer than 200 characters")]
    		public string  ShipingRemark { get; set; }
    
    		    
    		/// <summary>
    		/// Carrier
    		/// </summary>        
    	//    [DisplayName("Carrier")]
            [MaxLength(50, ErrorMessage = "Carrier cannot be longer than 50 characters")]
    		public string  Carrier { get; set; }
    
    		    
    		/// <summary>
    		/// Sales Person
    		/// </summary>        
    	//    [DisplayName("Sales Person")]
            [MaxLength(50, ErrorMessage = "Sales Person cannot be longer than 50 characters")]
    		public string  SalesPerson { get; set; }
    
    		    
    		/// <summary>
    		/// Created By
    		/// </summary>        
    	//    [DisplayName("Created By")]
            [MaxLength(10, ErrorMessage = "Created By cannot be longer than 10 characters")]
    		public string  CreatedBy { get; set; }
    
    		    
    		/// <summary>
    		/// Created Date
    		/// </summary>        
    	//    [DisplayName("Created Date")]
    		public Nullable<System.DateTime>  CreatedDate { get; set; }
    
    		    
    		/// <summary>
    		/// Updated By
    		/// </summary>        
    	//    [DisplayName("Updated By")]
            [MaxLength(10, ErrorMessage = "Updated By cannot be longer than 10 characters")]
    		public string  UpdatedBy { get; set; }
    
    		    
    		/// <summary>
    		/// Updated Date
    		/// </summary>        
    	//    [DisplayName("Updated Date")]
    		public Nullable<System.DateTime>  UpdatedDate { get; set; }
    
    		    
    		/// <summary>
    		/// Load Station1
    		/// </summary>        
    	//    [DisplayName("Load Station1")]
            [MaxLength(50, ErrorMessage = "Load Station1 cannot be longer than 50 characters")]
    		public string  LoadStation1 { get; set; }
    
    		    
    		/// <summary>
    		/// Load Master Loader1
    		/// </summary>        
    	//    [DisplayName("Load Master Loader1")]
            [MaxLength(50, ErrorMessage = "Load Master Loader1 cannot be longer than 50 characters")]
    		public string  LoadMasterLoader1 { get; set; }
    
    		    
    		/// <summary>
    		/// Load Carrier1
    		/// </summary>        
    	//    [DisplayName("Load Carrier1")]
            [MaxLength(50, ErrorMessage = "Load Carrier1 cannot be longer than 50 characters")]
    		public string  LoadCarrier1 { get; set; }
    
    		    
    		/// <summary>
    		/// Load MAWBNo1
    		/// </summary>        
    	//    [DisplayName("Load MAWBNo1")]
            [MaxLength(50, ErrorMessage = "Load MAWBNo1 cannot be longer than 50 characters")]
    		public string  LoadMAWBNo1 { get; set; }
    
    		    
    		/// <summary>
    		/// Load Est Cost1
    		/// </summary>        
    	//    [DisplayName("Load Est Cost1")]
            [MaxLength(50, ErrorMessage = "Load Est Cost1 cannot be longer than 50 characters")]
    		public string  LoadEstCost1 { get; set; }
    
    		    
    		/// <summary>
    		/// Load FLTNo1
    		/// </summary>        
    	//    [DisplayName("Load FLTNo1")]
            [MaxLength(50, ErrorMessage = "Load FLTNo1 cannot be longer than 50 characters")]
    		public string  LoadFLTNo1 { get; set; }
    
    		    
    		/// <summary>
    		/// Load Terminal1
    		/// </summary>        
    	//    [DisplayName("Load Terminal1")]
            [MaxLength(50, ErrorMessage = "Load Terminal1 cannot be longer than 50 characters")]
    		public string  LoadTerminal1 { get; set; }
    
    		    
    		/// <summary>
    		/// Load Station2
    		/// </summary>        
    	//    [DisplayName("Load Station2")]
            [MaxLength(50, ErrorMessage = "Load Station2 cannot be longer than 50 characters")]
    		public string  LoadStation2 { get; set; }
    
    		    
    		/// <summary>
    		/// Load Master Loader2
    		/// </summary>        
    	//    [DisplayName("Load Master Loader2")]
            [MaxLength(50, ErrorMessage = "Load Master Loader2 cannot be longer than 50 characters")]
    		public string  LoadMasterLoader2 { get; set; }
    
    		    
    		/// <summary>
    		/// Load Carrier2
    		/// </summary>        
    	//    [DisplayName("Load Carrier2")]
            [MaxLength(50, ErrorMessage = "Load Carrier2 cannot be longer than 50 characters")]
    		public string  LoadCarrier2 { get; set; }
    
    		    
    		/// <summary>
    		/// Load MAWBNo2
    		/// </summary>        
    	//    [DisplayName("Load MAWBNo2")]
            [MaxLength(50, ErrorMessage = "Load MAWBNo2 cannot be longer than 50 characters")]
    		public string  LoadMAWBNo2 { get; set; }
    
    		    
    		/// <summary>
    		/// Load Est Cost2
    		/// </summary>        
    	//    [DisplayName("Load Est Cost2")]
            [MaxLength(50, ErrorMessage = "Load Est Cost2 cannot be longer than 50 characters")]
    		public string  LoadEstCost2 { get; set; }
    
    		    
    		/// <summary>
    		/// Load FLTNo2
    		/// </summary>        
    	//    [DisplayName("Load FLTNo2")]
            [MaxLength(50, ErrorMessage = "Load FLTNo2 cannot be longer than 50 characters")]
    		public string  LoadFLTNo2 { get; set; }
    
    		    
    		/// <summary>
    		/// Book Send
    		/// </summary>        
    	//    [DisplayName("Book Send")]
            [MaxLength(1, ErrorMessage = "Book Send cannot be longer than 1 characters")]
    		public string  BookSend { get; set; }
    
    		    
    		/// <summary>
    		/// WHCargo Status
    		/// </summary>        
    	//    [DisplayName("WHCargo Status")]
            [MaxLength(50, ErrorMessage = "WHCargo Status cannot be longer than 50 characters")]
    		public string  WHCargoStatus { get; set; }
    
    		    
    		/// <summary>
    		/// WHLocation
    		/// </summary>        
    	//    [DisplayName("WHLocation")]
            [MaxLength(50, ErrorMessage = "WHLocation cannot be longer than 50 characters")]
    		public string  WHLocation { get; set; }
    
    		    
    		/// <summary>
    		/// WHVolume
    		/// </summary>        
    	//    [DisplayName("WHVolume")]
    		public Nullable<decimal>  WHVolume { get; set; }
    
    		    
    		/// <summary>
    		/// WHDimension
    		/// </summary>        
    	//    [DisplayName("WHDimension")]
            [MaxLength(200, ErrorMessage = "WHDimension cannot be longer than 200 characters")]
    		public string  WHDimension { get; set; }
    
    		    
    		/// <summary>
    		/// RTHexiao Dan No
    		/// </summary>        
    	//    [DisplayName("RTHexiao Dan No")]
            [MaxLength(50, ErrorMessage = "RTHexiao Dan No cannot be longer than 50 characters")]
    		public string  RTHexiaoDanNo { get; set; }
    
    		    
    		/// <summary>
    		/// RTTui Shui Date
    		/// </summary>        
    	//    [DisplayName("RTTui Shui Date")]
    		public Nullable<System.DateTime>  RTTuiShuiDate { get; set; }
    
    		    
    		/// <summary>
    		/// CSStatus
    		/// </summary>        
    	//    [DisplayName("CSStatus")]
            [MaxLength(50, ErrorMessage = "CSStatus cannot be longer than 50 characters")]
    		public string  CSStatus { get; set; }
    
    		    
    		/// <summary>
    		/// Booking PCS
    		/// </summary>        
    	//    [DisplayName("Booking PCS")]
    		public Nullable<int>  BookingPCS { get; set; }
    
    		    
    		/// <summary>
    		/// Booking PCSUOM
    		/// </summary>        
    	//    [DisplayName("Booking PCSUOM")]
            [MaxLength(10, ErrorMessage = "Booking PCSUOM cannot be longer than 10 characters")]
    		public string  BookingPCSUOM { get; set; }
    
    		    
    		/// <summary>
    		/// Booking GWT
    		/// </summary>        
    	//    [DisplayName("Booking GWT")]
    		public Nullable<decimal>  BookingGWT { get; set; }
    
    		    
    		/// <summary>
    		/// Booking WTUOM
    		/// </summary>        
    	//    [DisplayName("Booking WTUOM")]
            [MaxLength(10, ErrorMessage = "Booking WTUOM cannot be longer than 10 characters")]
    		public string  BookingWTUOM { get; set; }
    
    		    
    		/// <summary>
    		/// Booking Volume UOM
    		/// </summary>        
    	//    [DisplayName("Booking Volume UOM")]
            [MaxLength(10, ErrorMessage = "Booking Volume UOM cannot be longer than 10 characters")]
    		public string  BookingVolumeUOM { get; set; }
    
    		    
    		/// <summary>
    		/// Hub Cargo Date
    		/// </summary>        
    	//    [DisplayName("Hub Cargo Date")]
    		public Nullable<System.DateTime>  HubCargoDate { get; set; }
    
    		    
    		/// <summary>
    		/// Hub Service Level
    		/// </summary>        
    	//    [DisplayName("Hub Service Level")]
            [MaxLength(50, ErrorMessage = "Hub Service Level cannot be longer than 50 characters")]
    		public string  HubServiceLevel { get; set; }
    
    		    
    	}
    //}
    
}
