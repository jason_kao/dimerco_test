using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ValuePlus.ViewModel;



namespace ValuePlus.ViewModel.LocaleChainVP
{
    
    /// <summary>
    /// OEDR class
    /// </summary>
    //[MetadataType(typeof(OEDRViewModel))]
    //public  partial class OEDR
    //{
    
    	/// <summary>
    	/// OEDR Metadata class
    	/// </summary>
    	public   class OEDRViewModel:ViewModelBase
    	{
    		    
    		/// <summary>
    		/// ID
    		/// </summary>        
    	//    [DisplayName("ID")]
            [Required(ErrorMessage = "ID is required")]
    		public int  ID { get; set; }
    
    		    
    		/// <summary>
    		/// MBLID
    		/// </summary>        
    	//    [DisplayName("MBLID")]
    		public Nullable<int>  MBLID { get; set; }
    
    		    
    		/// <summary>
    		/// MBLNo
    		/// </summary>        
    	//    [DisplayName("MBLNo")]
            [MaxLength(400, ErrorMessage = "MBLNo cannot be longer than 400 characters")]
    		public string  MBLNo { get; set; }
    
    		    
    		/// <summary>
    		/// Station ID
    		/// </summary>        
    	//    [DisplayName("Station ID")]
            [MaxLength(5, ErrorMessage = "Station ID cannot be longer than 5 characters")]
    		public string  StationID { get; set; }
    
    		    
    		/// <summary>
    		/// DRNo
    		/// </summary>        
    	//    [DisplayName("DRNo")]
            [MaxLength(50, ErrorMessage = "DRNo cannot be longer than 50 characters")]
    		public string  DRNo { get; set; }
    
    		    
    		/// <summary>
    		/// BLNo
    		/// </summary>        
    	//    [DisplayName("BLNo")]
            [MaxLength(50, ErrorMessage = "BLNo cannot be longer than 50 characters")]
    		public string  BLNo { get; set; }
    
    		    
    		/// <summary>
    		/// SHPR
    		/// </summary>        
    	//    [DisplayName("SHPR")]
            [MaxLength(500, ErrorMessage = "SHPR cannot be longer than 500 characters")]
    		public string  SHPR { get; set; }
    
    		    
    		/// <summary>
    		/// CNEE
    		/// </summary>        
    	//    [DisplayName("CNEE")]
            [MaxLength(500, ErrorMessage = "CNEE cannot be longer than 500 characters")]
    		public string  CNEE { get; set; }
    
    		    
    		/// <summary>
    		/// NTFY
    		/// </summary>        
    	//    [DisplayName("NTFY")]
            [MaxLength(500, ErrorMessage = "NTFY cannot be longer than 500 characters")]
    		public string  NTFY { get; set; }
    
    		    
    		/// <summary>
    		/// Pre Carriage
    		/// </summary>        
    	//    [DisplayName("Pre Carriage")]
            [MaxLength(50, ErrorMessage = "Pre Carriage cannot be longer than 50 characters")]
    		public string  PreCarriage { get; set; }
    
    		    
    		/// <summary>
    		/// PReceipt
    		/// </summary>        
    	//    [DisplayName("PReceipt")]
            [MaxLength(50, ErrorMessage = "PReceipt cannot be longer than 50 characters")]
    		public string  PReceipt { get; set; }
    
    		    
    		/// <summary>
    		/// Vessel
    		/// </summary>        
    	//    [DisplayName("Vessel")]
            [MaxLength(100, ErrorMessage = "Vessel cannot be longer than 100 characters")]
    		public string  Vessel { get; set; }
    
    		    
    		/// <summary>
    		/// Voyage
    		/// </summary>        
    	//    [DisplayName("Voyage")]
            [MaxLength(100, ErrorMessage = "Voyage cannot be longer than 100 characters")]
    		public string  Voyage { get; set; }
    
    		    
    		/// <summary>
    		/// PLoading
    		/// </summary>        
    	//    [DisplayName("PLoading")]
            [MaxLength(40, ErrorMessage = "PLoading cannot be longer than 40 characters")]
    		public string  PLoading { get; set; }
    
    		    
    		/// <summary>
    		/// PDischarge
    		/// </summary>        
    	//    [DisplayName("PDischarge")]
            [MaxLength(40, ErrorMessage = "PDischarge cannot be longer than 40 characters")]
    		public string  PDischarge { get; set; }
    
    		    
    		/// <summary>
    		/// PDelivery
    		/// </summary>        
    	//    [DisplayName("PDelivery")]
            [MaxLength(40, ErrorMessage = "PDelivery cannot be longer than 40 characters")]
    		public string  PDelivery { get; set; }
    
    		    
    		/// <summary>
    		/// DEST
    		/// </summary>        
    	//    [DisplayName("DEST")]
            [MaxLength(100, ErrorMessage = "DEST cannot be longer than 100 characters")]
    		public string  DEST { get; set; }
    
    		    
    		/// <summary>
    		/// CNTRNo
    		/// </summary>        
    	//    [DisplayName("CNTRNo")]
            [MaxLength(50, ErrorMessage = "CNTRNo cannot be longer than 50 characters")]
    		public string  CNTRNo { get; set; }
    
    		    
    		/// <summary>
    		/// Seal No
    		/// </summary>        
    	//    [DisplayName("Seal No")]
            [MaxLength(500, ErrorMessage = "Seal No cannot be longer than 500 characters")]
    		public string  SealNo { get; set; }
    
    		    
    		/// <summary>
    		/// PCS
    		/// </summary>        
    	//    [DisplayName("PCS")]
            [MaxLength(50, ErrorMessage = "PCS cannot be longer than 50 characters")]
    		public string  PCS { get; set; }
    
    		    
    		/// <summary>
    		/// Goods Desc
    		/// </summary>        
    	//    [DisplayName("Goods Desc")]
            [MaxLength(600, ErrorMessage = "Goods Desc cannot be longer than 600 characters")]
    		public string  GoodsDesc { get; set; }
    
    		    
    		/// <summary>
    		/// GWT
    		/// </summary>        
    	//    [DisplayName("GWT")]
            [MaxLength(50, ErrorMessage = "GWT cannot be longer than 50 characters")]
    		public string  GWT { get; set; }
    
    		    
    		/// <summary>
    		/// CBM
    		/// </summary>        
    	//    [DisplayName("CBM")]
            [MaxLength(50, ErrorMessage = "CBM cannot be longer than 50 characters")]
    		public string  CBM { get; set; }
    
    		    
    		/// <summary>
    		/// CNTRIn Words
    		/// </summary>        
    	//    [DisplayName("CNTRIn Words")]
            [MaxLength(250, ErrorMessage = "CNTRIn Words cannot be longer than 250 characters")]
    		public string  CNTRInWords { get; set; }
    
    		    
    		/// <summary>
    		/// FRTCharge
    		/// </summary>        
    	//    [DisplayName("FRTCharge")]
            [MaxLength(50, ErrorMessage = "FRTCharge cannot be longer than 50 characters")]
    		public string  FRTCharge { get; set; }
    
    		    
    		/// <summary>
    		/// Revenue Tons
    		/// </summary>        
    	//    [DisplayName("Revenue Tons")]
            [MaxLength(50, ErrorMessage = "Revenue Tons cannot be longer than 50 characters")]
    		public string  RevenueTons { get; set; }
    
    		    
    		/// <summary>
    		/// Rate
    		/// </summary>        
    	//    [DisplayName("Rate")]
            [MaxLength(10, ErrorMessage = "Rate cannot be longer than 10 characters")]
    		public string  Rate { get; set; }
    
    		    
    		/// <summary>
    		/// Per
    		/// </summary>        
    	//    [DisplayName("Per")]
            [MaxLength(50, ErrorMessage = "Per cannot be longer than 50 characters")]
    		public string  Per { get; set; }
    
    		    
    		/// <summary>
    		/// PP
    		/// </summary>        
    	//    [DisplayName("PP")]
            [MaxLength(10, ErrorMessage = "PP cannot be longer than 10 characters")]
    		public string  PP { get; set; }
    
    		    
    		/// <summary>
    		/// CC
    		/// </summary>        
    	//    [DisplayName("CC")]
            [MaxLength(10, ErrorMessage = "CC cannot be longer than 10 characters")]
    		public string  CC { get; set; }
    
    		    
    		/// <summary>
    		/// Ex Rate
    		/// </summary>        
    	//    [DisplayName("Ex Rate")]
            [MaxLength(50, ErrorMessage = "Ex Rate cannot be longer than 50 characters")]
    		public string  ExRate { get; set; }
    
    		    
    		/// <summary>
    		/// PPAt
    		/// </summary>        
    	//    [DisplayName("PPAt")]
            [MaxLength(10, ErrorMessage = "PPAt cannot be longer than 10 characters")]
    		public string  PPAt { get; set; }
    
    		    
    		/// <summary>
    		/// CCAt
    		/// </summary>        
    	//    [DisplayName("CCAt")]
            [MaxLength(10, ErrorMessage = "CCAt cannot be longer than 10 characters")]
    		public string  CCAt { get; set; }
    
    		    
    		/// <summary>
    		/// Issue Place
    		/// </summary>        
    	//    [DisplayName("Issue Place")]
            [MaxLength(250, ErrorMessage = "Issue Place cannot be longer than 250 characters")]
    		public string  IssuePlace { get; set; }
    
    		    
    		/// <summary>
    		/// Total PP
    		/// </summary>        
    	//    [DisplayName("Total PP")]
            [MaxLength(50, ErrorMessage = "Total PP cannot be longer than 50 characters")]
    		public string  TotalPP { get; set; }
    
    		    
    		/// <summary>
    		/// RCY
    		/// </summary>        
    	//    [DisplayName("RCY")]
            [MaxLength(50, ErrorMessage = "RCY cannot be longer than 50 characters")]
    		public string  RCY { get; set; }
    
    		    
    		/// <summary>
    		/// RCFS
    		/// </summary>        
    	//    [DisplayName("RCFS")]
            [MaxLength(50, ErrorMessage = "RCFS cannot be longer than 50 characters")]
    		public string  RCFS { get; set; }
    
    		    
    		/// <summary>
    		/// RDoor
    		/// </summary>        
    	//    [DisplayName("RDoor")]
            [MaxLength(50, ErrorMessage = "RDoor cannot be longer than 50 characters")]
    		public string  RDoor { get; set; }
    
    		    
    		/// <summary>
    		/// DCY
    		/// </summary>        
    	//    [DisplayName("DCY")]
            [MaxLength(50, ErrorMessage = "DCY cannot be longer than 50 characters")]
    		public string  DCY { get; set; }
    
    		    
    		/// <summary>
    		/// DCFS
    		/// </summary>        
    	//    [DisplayName("DCFS")]
            [MaxLength(50, ErrorMessage = "DCFS cannot be longer than 50 characters")]
    		public string  DCFS { get; set; }
    
    		    
    		/// <summary>
    		/// DDoor
    		/// </summary>        
    	//    [DisplayName("DDoor")]
            [MaxLength(50, ErrorMessage = "DDoor cannot be longer than 50 characters")]
    		public string  DDoor { get; set; }
    
    		    
    		/// <summary>
    		/// Temp Required
    		/// </summary>        
    	//    [DisplayName("Temp Required")]
            [MaxLength(50, ErrorMessage = "Temp Required cannot be longer than 50 characters")]
    		public string  TempRequired { get; set; }
    
    		    
    		/// <summary>
    		/// Temp F
    		/// </summary>        
    	//    [DisplayName("Temp F")]
            [MaxLength(50, ErrorMessage = "Temp F cannot be longer than 50 characters")]
    		public string  TempF { get; set; }
    
    		    
    		/// <summary>
    		/// Temp C
    		/// </summary>        
    	//    [DisplayName("Temp C")]
            [MaxLength(50, ErrorMessage = "Temp C cannot be longer than 50 characters")]
    		public string  TempC { get; set; }
    
    		    
    		/// <summary>
    		/// Ordinary
    		/// </summary>        
    	//    [DisplayName("Ordinary")]
            [MaxLength(50, ErrorMessage = "Ordinary cannot be longer than 50 characters")]
    		public string  Ordinary { get; set; }
    
    		    
    		/// <summary>
    		/// Reefer
    		/// </summary>        
    	//    [DisplayName("Reefer")]
            [MaxLength(50, ErrorMessage = "Reefer cannot be longer than 50 characters")]
    		public string  Reefer { get; set; }
    
    		    
    		/// <summary>
    		/// Dangerous
    		/// </summary>        
    	//    [DisplayName("Dangerous")]
            [MaxLength(50, ErrorMessage = "Dangerous cannot be longer than 50 characters")]
    		public string  Dangerous { get; set; }
    
    		    
    		/// <summary>
    		/// Auto
    		/// </summary>        
    	//    [DisplayName("Auto")]
            [MaxLength(50, ErrorMessage = "Auto cannot be longer than 50 characters")]
    		public string  Auto { get; set; }
    
    		    
    		/// <summary>
    		/// Liquid
    		/// </summary>        
    	//    [DisplayName("Liquid")]
            [MaxLength(50, ErrorMessage = "Liquid cannot be longer than 50 characters")]
    		public string  Liquid { get; set; }
    
    		    
    		/// <summary>
    		/// Live Animal
    		/// </summary>        
    	//    [DisplayName("Live Animal")]
            [MaxLength(50, ErrorMessage = "Live Animal cannot be longer than 50 characters")]
    		public string  LiveAnimal { get; set; }
    
    		    
    		/// <summary>
    		/// Bulk
    		/// </summary>        
    	//    [DisplayName("Bulk")]
            [MaxLength(50, ErrorMessage = "Bulk cannot be longer than 50 characters")]
    		public string  Bulk { get; set; }
    
    		    
    		/// <summary>
    		/// Other
    		/// </summary>        
    	//    [DisplayName("Other")]
            [MaxLength(250, ErrorMessage = "Other cannot be longer than 250 characters")]
    		public string  Other { get; set; }
    
    		    
    		/// <summary>
    		/// Other Desc
    		/// </summary>        
    	//    [DisplayName("Other Desc")]
            [MaxLength(250, ErrorMessage = "Other Desc cannot be longer than 250 characters")]
    		public string  OtherDesc { get; set; }
    
    		    
    		/// <summary>
    		/// Change Vessel
    		/// </summary>        
    	//    [DisplayName("Change Vessel")]
            [MaxLength(50, ErrorMessage = "Change Vessel cannot be longer than 50 characters")]
    		public string  ChangeVessel { get; set; }
    
    		    
    		/// <summary>
    		/// Can Batch
    		/// </summary>        
    	//    [DisplayName("Can Batch")]
            [MaxLength(50, ErrorMessage = "Can Batch cannot be longer than 50 characters")]
    		public string  CanBatch { get; set; }
    
    		    
    		/// <summary>
    		/// Loading Date
    		/// </summary>        
    	//    [DisplayName("Loading Date")]
            [MaxLength(50, ErrorMessage = "Loading Date cannot be longer than 50 characters")]
    		public string  LoadingDate { get; set; }
    
    		    
    		/// <summary>
    		/// Effective Date
    		/// </summary>        
    	//    [DisplayName("Effective Date")]
            [MaxLength(50, ErrorMessage = "Effective Date cannot be longer than 50 characters")]
    		public string  EffectiveDate { get; set; }
    
    		    
    		/// <summary>
    		/// Amount
    		/// </summary>        
    	//    [DisplayName("Amount")]
            [MaxLength(50, ErrorMessage = "Amount cannot be longer than 50 characters")]
    		public string  Amount { get; set; }
    
    		    
    		/// <summary>
    		/// Print Date
    		/// </summary>        
    	//    [DisplayName("Print Date")]
            [MaxLength(50, ErrorMessage = "Print Date cannot be longer than 50 characters")]
    		public string  PrintDate { get; set; }
    
    		    
    		/// <summary>
    		/// Created DT
    		/// </summary>        
    	//    [DisplayName("Created DT")]
    		public Nullable<System.DateTime>  CreatedDT { get; set; }
    
    		    
    		/// <summary>
    		/// Created By
    		/// </summary>        
    	//    [DisplayName("Created By")]
            [MaxLength(50, ErrorMessage = "Created By cannot be longer than 50 characters")]
    		public string  CreatedBy { get; set; }
    
    		    
    		/// <summary>
    		/// Updated DT
    		/// </summary>        
    	//    [DisplayName("Updated DT")]
    		public Nullable<System.DateTime>  UpdatedDT { get; set; }
    
    		    
    		/// <summary>
    		/// Updated By
    		/// </summary>        
    	//    [DisplayName("Updated By")]
            [MaxLength(50, ErrorMessage = "Updated By cannot be longer than 50 characters")]
    		public string  UpdatedBy { get; set; }
    
    		    
    		/// <summary>
    		/// SONo
    		/// </summary>        
    	//    [DisplayName("SONo")]
            [MaxLength(50, ErrorMessage = "SONo cannot be longer than 50 characters")]
    		public string  SONo { get; set; }
    
    		    
    		/// <summary>
    		/// ETD
    		/// </summary>        
    	//    [DisplayName("ETD")]
            [MaxLength(60, ErrorMessage = "ETD cannot be longer than 60 characters")]
    		public string  ETD { get; set; }
    
    		    
    		/// <summary>
    		/// OPName
    		/// </summary>        
    	//    [DisplayName("OPName")]
            [MaxLength(60, ErrorMessage = "OPName cannot be longer than 60 characters")]
    		public string  OPName { get; set; }
    
    		    
    		/// <summary>
    		/// OPTELNo
    		/// </summary>        
    	//    [DisplayName("OPTELNo")]
            [MaxLength(60, ErrorMessage = "OPTELNo cannot be longer than 60 characters")]
    		public string  OPTELNo { get; set; }
    
    		    
    	}
    //}
    
}
