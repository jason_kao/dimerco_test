using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ValuePlus.ViewModel;



namespace ValuePlus.ViewModel.LocaleChainVP
{
    
    /// <summary>
    /// AIEntry class
    /// </summary>
    //[MetadataType(typeof(AIEntryViewModel))]
    //public  partial class AIEntry
    //{
    
    	/// <summary>
    	/// AIEntry Metadata class
    	/// </summary>
    	public   class AIEntryViewModel:ViewModelBase
    	{
    		    
    		/// <summary>
    		/// ID
    		/// </summary>        
    	//    [DisplayName("ID")]
            [Required(ErrorMessage = "ID is required")]
    		public int  ID { get; set; }
    
    		    
    		/// <summary>
    		/// MAWBID
    		/// </summary>        
    	//    [DisplayName("MAWBID")]
            [Required(ErrorMessage = "MAWBID is required")]
    		public int  MAWBID { get; set; }
    
    		    
    		/// <summary>
    		/// Damage
    		/// </summary>        
    	//    [DisplayName("Damage")]
            [MaxLength(20, ErrorMessage = "Damage cannot be longer than 20 characters")]
    		public string  Damage { get; set; }
    
    		    
    		/// <summary>
    		/// Shortage
    		/// </summary>        
    	//    [DisplayName("Shortage")]
            [MaxLength(20, ErrorMessage = "Shortage cannot be longer than 20 characters")]
    		public string  Shortage { get; set; }
    
    		    
    		/// <summary>
    		/// Offload
    		/// </summary>        
    	//    [DisplayName("Offload")]
            [MaxLength(20, ErrorMessage = "Offload cannot be longer than 20 characters")]
    		public string  Offload { get; set; }
    
    		    
    		/// <summary>
    		/// Customs Date
    		/// </summary>        
    	//    [DisplayName("Customs Date")]
    		public Nullable<System.DateTime>  CustomsDate { get; set; }
    
    		    
    		/// <summary>
    		/// Customs Entry
    		/// </summary>        
    	//    [DisplayName("Customs Entry")]
            [MaxLength(20, ErrorMessage = "Customs Entry cannot be longer than 20 characters")]
    		public string  CustomsEntry { get; set; }
    
    		    
    		/// <summary>
    		/// Entry Port
    		/// </summary>        
    	//    [DisplayName("Entry Port")]
    		public Nullable<int>  EntryPort { get; set; }
    
    		    
    		/// <summary>
    		/// FLTNo
    		/// </summary>        
    	//    [DisplayName("FLTNo")]
            [MaxLength(20, ErrorMessage = "FLTNo cannot be longer than 20 characters")]
    		public string  FLTNo { get; set; }
    
    		    
    		/// <summary>
    		/// Cargo At
    		/// </summary>        
    	//    [DisplayName("Cargo At")]
    		public Nullable<int>  CargoAt { get; set; }
    
    		    
    		/// <summary>
    		/// Total PCS
    		/// </summary>        
    	//    [DisplayName("Total PCS")]
    		public Nullable<int>  TotalPCS { get; set; }
    
    		    
    		/// <summary>
    		/// GWT
    		/// </summary>        
    	//    [DisplayName("GWT")]
    		public Nullable<double>  GWT { get; set; }
    
    		    
    		/// <summary>
    		/// Entry Type
    		/// </summary>        
    	//    [DisplayName("Entry Type")]
            [MaxLength(20, ErrorMessage = "Entry Type cannot be longer than 20 characters")]
    		public string  EntryType { get; set; }
    
    		    
    		/// <summary>
    		/// Storage
    		/// </summary>        
    	//    [DisplayName("Storage")]
            [MaxLength(50, ErrorMessage = "Storage cannot be longer than 50 characters")]
    		public string  Storage { get; set; }
    
    		    
    		/// <summary>
    		/// Landing Date
    		/// </summary>        
    	//    [DisplayName("Landing Date")]
    		public Nullable<System.DateTime>  LandingDate { get; set; }
    
    		    
    		/// <summary>
    		/// Carrier
    		/// </summary>        
    	//    [DisplayName("Carrier")]
    		public Nullable<int>  Carrier { get; set; }
    
    		    
    		/// <summary>
    		/// Station ID
    		/// </summary>        
    	//    [DisplayName("Station ID")]
            [Required(ErrorMessage = "Station ID is required")]
            [MaxLength(10, ErrorMessage = "Station ID cannot be longer than 10 characters")]
    		public string  StationID { get; set; }
    
    		    
    		/// <summary>
    		/// Created By
    		/// </summary>        
    	//    [DisplayName("Created By")]
            [Required(ErrorMessage = "Created By is required")]
            [MaxLength(6, ErrorMessage = "Created By cannot be longer than 6 characters")]
    		public string  CreatedBy { get; set; }
    
    		    
    		/// <summary>
    		/// Created Date
    		/// </summary>        
    	//    [DisplayName("Created Date")]
            [Required(ErrorMessage = "Created Date is required")]
    		public System.DateTime  CreatedDate { get; set; }
    
    		    
    		/// <summary>
    		/// Updated By
    		/// </summary>        
    	//    [DisplayName("Updated By")]
            [Required(ErrorMessage = "Updated By is required")]
            [MaxLength(6, ErrorMessage = "Updated By cannot be longer than 6 characters")]
    		public string  UpdatedBy { get; set; }
    
    		    
    		/// <summary>
    		/// Updated Date
    		/// </summary>        
    	//    [DisplayName("Updated Date")]
            [Required(ErrorMessage = "Updated Date is required")]
    		public System.DateTime  UpdatedDate { get; set; }
    
    		    
    		/// <summary>
    		/// Carriers
    		/// </summary>        
    	//    [DisplayName("Carriers")]
    		public Nullable<int>  Carriers { get; set; }
    
    		    
    		/// <summary>
    		/// Avail
    		/// </summary>        
    	//    [DisplayName("Avail")]
    		public Nullable<int>  Avail { get; set; }
    
    		    
    		/// <summary>
    		/// Avail Addr
    		/// </summary>        
    	//    [DisplayName("Avail Addr")]
            [MaxLength(255, ErrorMessage = "Avail Addr cannot be longer than 255 characters")]
    		public string  AvailAddr { get; set; }
    
    		    
    		/// <summary>
    		/// Avail Date
    		/// </summary>        
    	//    [DisplayName("Avail Date")]
    		public Nullable<System.DateTime>  AvailDate { get; set; }
    
    		    
    		/// <summary>
    		/// Version
    		/// </summary>        
    	//    [DisplayName("Version")]
            [MaxLength(8, ErrorMessage = "Version cannot be longer than 8 characters")]
    		public byte[]  Version { get; set; }
    
    		    
    	}
    //}
    
}
