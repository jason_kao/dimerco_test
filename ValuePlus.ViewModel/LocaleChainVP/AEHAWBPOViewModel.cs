using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ValuePlus.ViewModel;



namespace ValuePlus.ViewModel.LocaleChainVP
{
    
    /// <summary>
    /// AEHAWBPO class
    /// </summary>
    //[MetadataType(typeof(AEHAWBPOViewModel))]
    //public  partial class AEHAWBPO
    //{
    
    	/// <summary>
    	/// AEHAWBPO Metadata class
    	/// </summary>
    	public   class AEHAWBPOViewModel:ViewModelBase
    	{
    		    
    		/// <summary>
    		/// ID
    		/// </summary>        
    	//    [DisplayName("ID")]
            [Required(ErrorMessage = "ID is required")]
    		public int  ID { get; set; }
    
    		    
    		/// <summary>
    		/// HAWBID
    		/// </summary>        
    	//    [DisplayName("HAWBID")]
            [Required(ErrorMessage = "HAWBID is required")]
    		public int  HAWBID { get; set; }
    
    		    
    		/// <summary>
    		/// PONo
    		/// </summary>        
    	//    [DisplayName("PONo")]
            [MaxLength(50, ErrorMessage = "PONo cannot be longer than 50 characters")]
    		public string  PONo { get; set; }
    
    		    
    		/// <summary>
    		/// Date
    		/// </summary>        
    	//    [DisplayName("Date")]
    		public Nullable<System.DateTime>  Date { get; set; }
    
    		    
    		/// <summary>
    		/// Quantity
    		/// </summary>        
    	//    [DisplayName("Quantity")]
    		public Nullable<int>  Quantity { get; set; }
    
    		    
    		/// <summary>
    		/// Place Of DELV
    		/// </summary>        
    	//    [DisplayName("Place Of DELV")]
            [MaxLength(50, ErrorMessage = "Place Of DELV cannot be longer than 50 characters")]
    		public string  PlaceOfDELV { get; set; }
    
    		    
    		/// <summary>
    		/// Item
    		/// </summary>        
    	//    [DisplayName("Item")]
            [MaxLength(20, ErrorMessage = "Item cannot be longer than 20 characters")]
    		public string  Item { get; set; }
    
    		    
    		/// <summary>
    		/// Invoice No
    		/// </summary>        
    	//    [DisplayName("Invoice No")]
            [MaxLength(20, ErrorMessage = "Invoice No cannot be longer than 20 characters")]
    		public string  InvoiceNo { get; set; }
    
    		    
    		/// <summary>
    		/// Station ID
    		/// </summary>        
    	//    [DisplayName("Station ID")]
            [Required(ErrorMessage = "Station ID is required")]
            [MaxLength(10, ErrorMessage = "Station ID cannot be longer than 10 characters")]
    		public string  StationID { get; set; }
    
    		    
    		/// <summary>
    		/// Created By
    		/// </summary>        
    	//    [DisplayName("Created By")]
            [Required(ErrorMessage = "Created By is required")]
            [MaxLength(6, ErrorMessage = "Created By cannot be longer than 6 characters")]
    		public string  CreatedBy { get; set; }
    
    		    
    		/// <summary>
    		/// Created Date
    		/// </summary>        
    	//    [DisplayName("Created Date")]
            [Required(ErrorMessage = "Created Date is required")]
    		public System.DateTime  CreatedDate { get; set; }
    
    		    
    		/// <summary>
    		/// Updated By
    		/// </summary>        
    	//    [DisplayName("Updated By")]
            [Required(ErrorMessage = "Updated By is required")]
            [MaxLength(6, ErrorMessage = "Updated By cannot be longer than 6 characters")]
    		public string  UpdatedBy { get; set; }
    
    		    
    		/// <summary>
    		/// Updated Date
    		/// </summary>        
    	//    [DisplayName("Updated Date")]
            [Required(ErrorMessage = "Updated Date is required")]
    		public System.DateTime  UpdatedDate { get; set; }
    
    		    
    		/// <summary>
    		/// Source ID
    		/// </summary>        
    	//    [DisplayName("Source ID")]
    		public Nullable<int>  SourceID { get; set; }
    
    		    
    		/// <summary>
    		/// POExtra1
    		/// </summary>        
    	//    [DisplayName("POExtra1")]
            [MaxLength(50, ErrorMessage = "POExtra1 cannot be longer than 50 characters")]
    		public string  POExtra1 { get; set; }
    
    		    
    		/// <summary>
    		/// POExtra2
    		/// </summary>        
    	//    [DisplayName("POExtra2")]
            [MaxLength(50, ErrorMessage = "POExtra2 cannot be longer than 50 characters")]
    		public string  POExtra2 { get; set; }
    
    		    
    		/// <summary>
    		/// POExtra3
    		/// </summary>        
    	//    [DisplayName("POExtra3")]
            [MaxLength(50, ErrorMessage = "POExtra3 cannot be longer than 50 characters")]
    		public string  POExtra3 { get; set; }
    
    		    
    		/// <summary>
    		/// POExtra4
    		/// </summary>        
    	//    [DisplayName("POExtra4")]
            [MaxLength(50, ErrorMessage = "POExtra4 cannot be longer than 50 characters")]
    		public string  POExtra4 { get; set; }
    
    		    
    		/// <summary>
    		/// POExtra5
    		/// </summary>        
    	//    [DisplayName("POExtra5")]
            [MaxLength(50, ErrorMessage = "POExtra5 cannot be longer than 50 characters")]
    		public string  POExtra5 { get; set; }
    
    		    
    		/// <summary>
    		/// POExtra6
    		/// </summary>        
    	//    [DisplayName("POExtra6")]
            [MaxLength(50, ErrorMessage = "POExtra6 cannot be longer than 50 characters")]
    		public string  POExtra6 { get; set; }
    
    		    
    		/// <summary>
    		/// PODate
    		/// </summary>        
    	//    [DisplayName("PODate")]
    		public Nullable<System.DateTime>  PODate { get; set; }
    
    		    
    		/// <summary>
    		/// SONO
    		/// </summary>        
    	//    [DisplayName("SONO")]
            [MaxLength(20, ErrorMessage = "SONO cannot be longer than 20 characters")]
    		public string  SONO { get; set; }
    
    		    
    		/// <summary>
    		/// SCTNS
    		/// </summary>        
    	//    [DisplayName("SCTNS")]
    		public Nullable<int>  SCTNS { get; set; }
    
    		    
    		/// <summary>
    		/// Curr
    		/// </summary>        
    	//    [DisplayName("Curr")]
            [MaxLength(4, ErrorMessage = "Curr cannot be longer than 4 characters")]
    		public string  Curr { get; set; }
    
    		    
    		/// <summary>
    		/// Amount
    		/// </summary>        
    	//    [DisplayName("Amount")]
    		public Nullable<decimal>  Amount { get; set; }
    
    		    
    		/// <summary>
    		/// Remark
    		/// </summary>        
    	//    [DisplayName("Remark")]
            [MaxLength(60, ErrorMessage = "Remark cannot be longer than 60 characters")]
    		public string  Remark { get; set; }
    
    		    
    		/// <summary>
    		/// POMSID
    		/// </summary>        
    	//    [DisplayName("POMSID")]
    		public Nullable<int>  POMSID { get; set; }
    
    		    
    		/// <summary>
    		/// UOM
    		/// </summary>        
    	//    [DisplayName("UOM")]
            [MaxLength(50, ErrorMessage = "UOM cannot be longer than 50 characters")]
    		public string  UOM { get; set; }
    
    		    
    		/// <summary>
    		/// Version
    		/// </summary>        
    	//    [DisplayName("Version")]
            [MaxLength(8, ErrorMessage = "Version cannot be longer than 8 characters")]
    		public byte[]  Version { get; set; }
    
    		    
    	}
    //}
    
}
