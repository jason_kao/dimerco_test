using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ValuePlus.ViewModel;



namespace ValuePlus.ViewModel.LocaleChainVP
{
    
    /// <summary>
    /// AIHAWB class
    /// </summary>
    //[MetadataType(typeof(AIHAWBViewModel))]
    //public  partial class AIHAWB
    //{
    
    	/// <summary>
    	/// AIHAWB Metadata class
    	/// </summary>
    	public   class AIHAWBViewModel:ViewModelBase
    	{
    		    
    		/// <summary>
    		/// ID
    		/// </summary>        
    	//    [DisplayName("ID")]
            [Required(ErrorMessage = "ID is required")]
    		public int  ID { get; set; }
    
    		    
    		/// <summary>
    		/// Status
    		/// </summary>        
    	//    [DisplayName("Status")]
            [MaxLength(10, ErrorMessage = "Status cannot be longer than 10 characters")]
    		public string  Status { get; set; }
    
    		    
    		/// <summary>
    		/// Parent AWB
    		/// </summary>        
    	//    [DisplayName("Parent AWB")]
    		public Nullable<int>  ParentAWB { get; set; }
    
    		    
    		/// <summary>
    		/// Customer
    		/// </summary>        
    	//    [DisplayName("Customer")]
            [Required(ErrorMessage = "Customer is required")]
    		public int  Customer { get; set; }
    
    		    
    		/// <summary>
    		/// BBAgent
    		/// </summary>        
    	//    [DisplayName("BBAgent")]
    		public Nullable<int>  BBAgent { get; set; }
    
    		    
    		/// <summary>
    		/// Shipper
    		/// </summary>        
    	//    [DisplayName("Shipper")]
    		public Nullable<int>  Shipper { get; set; }
    
    		    
    		/// <summary>
    		/// CNEE
    		/// </summary>        
    	//    [DisplayName("CNEE")]
    		public Nullable<int>  CNEE { get; set; }
    
    		    
    		/// <summary>
    		/// Notify
    		/// </summary>        
    	//    [DisplayName("Notify")]
    		public Nullable<int>  Notify { get; set; }
    
    		    
    		/// <summary>
    		/// Sales
    		/// </summary>        
    	//    [DisplayName("Sales")]
            [MaxLength(6, ErrorMessage = "Sales cannot be longer than 6 characters")]
    		public string  Sales { get; set; }
    
    		    
    		/// <summary>
    		/// FRT
    		/// </summary>        
    	//    [DisplayName("FRT")]
            [MaxLength(5, ErrorMessage = "FRT cannot be longer than 5 characters")]
    		public string  FRT { get; set; }
    
    		    
    		/// <summary>
    		/// Other
    		/// </summary>        
    	//    [DisplayName("Other")]
            [MaxLength(5, ErrorMessage = "Other cannot be longer than 5 characters")]
    		public string  Other { get; set; }
    
    		    
    		/// <summary>
    		/// Move
    		/// </summary>        
    	//    [DisplayName("Move")]
            [MaxLength(5, ErrorMessage = "Move cannot be longer than 5 characters")]
    		public string  Move { get; set; }
    
    		    
    		/// <summary>
    		/// Act PCS
    		/// </summary>        
    	//    [DisplayName("Act PCS")]
    		public Nullable<int>  ActPCS { get; set; }
    
    		    
    		/// <summary>
    		/// SPUnit
    		/// </summary>        
    	//    [DisplayName("SPUnit")]
    		public Nullable<int>  SPUnit { get; set; }
    
    		    
    		/// <summary>
    		/// SPUnit UOM
    		/// </summary>        
    	//    [DisplayName("SPUnit UOM")]
            [MaxLength(10, ErrorMessage = "SPUnit UOM cannot be longer than 10 characters")]
    		public string  SPUnitUOM { get; set; }
    
    		    
    		/// <summary>
    		/// GWT
    		/// </summary>        
    	//    [DisplayName("GWT")]
    		public Nullable<double>  GWT { get; set; }
    
    		    
    		/// <summary>
    		/// VWT
    		/// </summary>        
    	//    [DisplayName("VWT")]
    		public Nullable<double>  VWT { get; set; }
    
    		    
    		/// <summary>
    		/// CWT
    		/// </summary>        
    	//    [DisplayName("CWT")]
    		public Nullable<double>  CWT { get; set; }
    
    		    
    		/// <summary>
    		/// WTUOM
    		/// </summary>        
    	//    [DisplayName("WTUOM")]
            [MaxLength(5, ErrorMessage = "WTUOM cannot be longer than 5 characters")]
    		public string  WTUOM { get; set; }
    
    		    
    		/// <summary>
    		/// Class Rate
    		/// </summary>        
    	//    [DisplayName("Class Rate")]
            [MaxLength(5, ErrorMessage = "Class Rate cannot be longer than 5 characters")]
    		public string  ClassRate { get; set; }
    
    		    
    		/// <summary>
    		/// Rate
    		/// </summary>        
    	//    [DisplayName("Rate")]
    		public Nullable<double>  Rate { get; set; }
    
    		    
    		/// <summary>
    		/// Currency
    		/// </summary>        
    	//    [DisplayName("Currency")]
            [MaxLength(5, ErrorMessage = "Currency cannot be longer than 5 characters")]
    		public string  Currency { get; set; }
    
    		    
    		/// <summary>
    		/// Show Rate
    		/// </summary>        
    	//    [DisplayName("Show Rate")]
            [Required(ErrorMessage = "Show Rate is required")]
    		public bool  ShowRate { get; set; }
    
    		    
    		/// <summary>
    		/// Export LIC
    		/// </summary>        
    	//    [DisplayName("Export LIC")]
            [MaxLength(255, ErrorMessage = "Export LIC cannot be longer than 255 characters")]
    		public string  ExportLIC { get; set; }
    
    		    
    		/// <summary>
    		/// Quantity
    		/// </summary>        
    	//    [DisplayName("Quantity")]
            [MaxLength(20, ErrorMessage = "Quantity cannot be longer than 20 characters")]
    		public string  Quantity { get; set; }
    
    		    
    		/// <summary>
    		/// Comm Inv
    		/// </summary>        
    	//    [DisplayName("Comm Inv")]
            [MaxLength(20, ErrorMessage = "Comm Inv cannot be longer than 20 characters")]
    		public string  CommInv { get; set; }
    
    		    
    		/// <summary>
    		/// DESC
    		/// </summary>        
    	//    [DisplayName("DESC")]
            [MaxLength(255, ErrorMessage = "DESC cannot be longer than 255 characters")]
    		public string  DESC { get; set; }
    
    		    
    		/// <summary>
    		/// Remark
    		/// </summary>        
    	//    [DisplayName("Remark")]
            [MaxLength(255, ErrorMessage = "Remark cannot be longer than 255 characters")]
    		public string  Remark { get; set; }
    
    		    
    		/// <summary>
    		/// Marks
    		/// </summary>        
    	//    [DisplayName("Marks")]
            [MaxLength(255, ErrorMessage = "Marks cannot be longer than 255 characters")]
    		public string  Marks { get; set; }
    
    		    
    		/// <summary>
    		/// SPINST
    		/// </summary>        
    	//    [DisplayName("SPINST")]
            [MaxLength(20, ErrorMessage = "SPINST cannot be longer than 20 characters")]
    		public string  SPINST { get; set; }
    
    		    
    		/// <summary>
    		/// AWBType
    		/// </summary>        
    	//    [DisplayName("AWBType")]
            [MaxLength(5, ErrorMessage = "AWBType cannot be longer than 5 characters")]
    		public string  AWBType { get; set; }
    
    		    
    		/// <summary>
    		/// Third Party
    		/// </summary>        
    	//    [DisplayName("Third Party")]
    		public Nullable<int>  ThirdParty { get; set; }
    
    		    
    		/// <summary>
    		/// Port Of DEPT
    		/// </summary>        
    	//    [DisplayName("Port Of DEPT")]
    		public Nullable<int>  PortOfDEPT { get; set; }
    
    		    
    		/// <summary>
    		/// Port Of DSTN
    		/// </summary>        
    	//    [DisplayName("Port Of DSTN")]
    		public Nullable<int>  PortOfDSTN { get; set; }
    
    		    
    		/// <summary>
    		/// Place Of RCPT
    		/// </summary>        
    	//    [DisplayName("Place Of RCPT")]
    		public Nullable<int>  PlaceOfRCPT { get; set; }
    
    		    
    		/// <summary>
    		/// Place Of DELV
    		/// </summary>        
    	//    [DisplayName("Place Of DELV")]
    		public Nullable<int>  PlaceOfDELV { get; set; }
    
    		    
    		/// <summary>
    		/// Trade Term
    		/// </summary>        
    	//    [DisplayName("Trade Term")]
            [MaxLength(5, ErrorMessage = "Trade Term cannot be longer than 5 characters")]
    		public string  TradeTerm { get; set; }
    
    		    
    		/// <summary>
    		/// Due To Agent
    		/// </summary>        
    	//    [DisplayName("Due To Agent")]
    		public Nullable<double>  DueToAgent { get; set; }
    
    		    
    		/// <summary>
    		/// Due To Carrier
    		/// </summary>        
    	//    [DisplayName("Due To Carrier")]
    		public Nullable<double>  DueToCarrier { get; set; }
    
    		    
    		/// <summary>
    		/// Issue Date
    		/// </summary>        
    	//    [DisplayName("Issue Date")]
    		public Nullable<System.DateTime>  IssueDate { get; set; }
    
    		    
    		/// <summary>
    		/// Print Person
    		/// </summary>        
    	//    [DisplayName("Print Person")]
            [MaxLength(10, ErrorMessage = "Print Person cannot be longer than 10 characters")]
    		public string  PrintPerson { get; set; }
    
    		    
    		/// <summary>
    		/// CUFT
    		/// </summary>        
    	//    [DisplayName("CUFT")]
    		public Nullable<double>  CUFT { get; set; }
    
    		    
    		/// <summary>
    		/// Act PCSUOM
    		/// </summary>        
    	//    [DisplayName("Act PCSUOM")]
            [MaxLength(10, ErrorMessage = "Act PCSUOM cannot be longer than 10 characters")]
    		public string  ActPCSUOM { get; set; }
    
    		    
    		/// <summary>
    		/// MAWBID
    		/// </summary>        
    	//    [DisplayName("MAWBID")]
    		public Nullable<int>  MAWBID { get; set; }
    
    		    
    		/// <summary>
    		/// Shipped WT
    		/// </summary>        
    	//    [DisplayName("Shipped WT")]
    		public Nullable<double>  ShippedWT { get; set; }
    
    		    
    		/// <summary>
    		/// Shipped PCS
    		/// </summary>        
    	//    [DisplayName("Shipped PCS")]
    		public Nullable<int>  ShippedPCS { get; set; }
    
    		    
    		/// <summary>
    		/// SCHRelease No
    		/// </summary>        
    	//    [DisplayName("SCHRelease No")]
            [MaxLength(20, ErrorMessage = "SCHRelease No cannot be longer than 20 characters")]
    		public string  SCHReleaseNo { get; set; }
    
    		    
    		/// <summary>
    		/// SCHRelease Date
    		/// </summary>        
    	//    [DisplayName("SCHRelease Date")]
    		public Nullable<System.DateTime>  SCHReleaseDate { get; set; }
    
    		    
    		/// <summary>
    		/// SCHNotify Party
    		/// </summary>        
    	//    [DisplayName("SCHNotify Party")]
    		public Nullable<int>  SCHNotifyParty { get; set; }
    
    		    
    		/// <summary>
    		/// SCHNotify Date
    		/// </summary>        
    	//    [DisplayName("SCHNotify Date")]
    		public Nullable<System.DateTime>  SCHNotifyDate { get; set; }
    
    		    
    		/// <summary>
    		/// SCHDoc Release Party
    		/// </summary>        
    	//    [DisplayName("SCHDoc Release Party")]
    		public Nullable<int>  SCHDocReleaseParty { get; set; }
    
    		    
    		/// <summary>
    		/// SCHDoc Release Date
    		/// </summary>        
    	//    [DisplayName("SCHDoc Release Date")]
    		public Nullable<System.DateTime>  SCHDocReleaseDate { get; set; }
    
    		    
    		/// <summary>
    		/// SCHDoc Release Type
    		/// </summary>        
    	//    [DisplayName("SCHDoc Release Type")]
            [MaxLength(10, ErrorMessage = "SCHDoc Release Type cannot be longer than 10 characters")]
    		public string  SCHDocReleaseType { get; set; }
    
    		    
    		/// <summary>
    		/// SCHNotify Type
    		/// </summary>        
    	//    [DisplayName("SCHNotify Type")]
            [MaxLength(10, ErrorMessage = "SCHNotify Type cannot be longer than 10 characters")]
    		public string  SCHNotifyType { get; set; }
    
    		    
    		/// <summary>
    		/// SCHCargo Sign By
    		/// </summary>        
    	//    [DisplayName("SCHCargo Sign By")]
            [MaxLength(50, ErrorMessage = "SCHCargo Sign By cannot be longer than 50 characters")]
    		public string  SCHCargoSignBy { get; set; }
    
    		    
    		/// <summary>
    		/// SCHLicense Date
    		/// </summary>        
    	//    [DisplayName("SCHLicense Date")]
    		public Nullable<System.DateTime>  SCHLicenseDate { get; set; }
    
    		    
    		/// <summary>
    		/// SCHCustom Date
    		/// </summary>        
    	//    [DisplayName("SCHCustom Date")]
    		public Nullable<System.DateTime>  SCHCustomDate { get; set; }
    
    		    
    		/// <summary>
    		/// SCHDoc Sign By
    		/// </summary>        
    	//    [DisplayName("SCHDoc Sign By")]
            [MaxLength(50, ErrorMessage = "SCHDoc Sign By cannot be longer than 50 characters")]
    		public string  SCHDocSignBy { get; set; }
    
    		    
    		/// <summary>
    		/// SCHCargo Release Party
    		/// </summary>        
    	//    [DisplayName("SCHCargo Release Party")]
    		public Nullable<int>  SCHCargoReleaseParty { get; set; }
    
    		    
    		/// <summary>
    		/// SCHCargo Release Date
    		/// </summary>        
    	//    [DisplayName("SCHCargo Release Date")]
    		public Nullable<System.DateTime>  SCHCargoReleaseDate { get; set; }
    
    		    
    		/// <summary>
    		/// SCHCargo Release Type
    		/// </summary>        
    	//    [DisplayName("SCHCargo Release Type")]
            [MaxLength(10, ErrorMessage = "SCHCargo Release Type cannot be longer than 10 characters")]
    		public string  SCHCargoReleaseType { get; set; }
    
    		    
    		/// <summary>
    		/// Customs Declare Party
    		/// </summary>        
    	//    [DisplayName("Customs Declare Party")]
    		public Nullable<int>  CustomsDeclareParty { get; set; }
    
    		    
    		/// <summary>
    		/// Customs Handled By
    		/// </summary>        
    	//    [DisplayName("Customs Handled By")]
    		public Nullable<int>  CustomsHandledBy { get; set; }
    
    		    
    		/// <summary>
    		/// Customs Doc Type
    		/// </summary>        
    	//    [DisplayName("Customs Doc Type")]
            [MaxLength(10, ErrorMessage = "Customs Doc Type cannot be longer than 10 characters")]
    		public string  CustomsDocType { get; set; }
    
    		    
    		/// <summary>
    		/// Customs Ava Date
    		/// </summary>        
    	//    [DisplayName("Customs Ava Date")]
    		public Nullable<System.DateTime>  CustomsAvaDate { get; set; }
    
    		    
    		/// <summary>
    		/// Customs Dec Date
    		/// </summary>        
    	//    [DisplayName("Customs Dec Date")]
    		public Nullable<System.DateTime>  CustomsDecDate { get; set; }
    
    		    
    		/// <summary>
    		/// Customs Remark
    		/// </summary>        
    	//    [DisplayName("Customs Remark")]
            [MaxLength(255, ErrorMessage = "Customs Remark cannot be longer than 255 characters")]
    		public string  CustomsRemark { get; set; }
    
    		    
    		/// <summary>
    		/// Customs Status
    		/// </summary>        
    	//    [DisplayName("Customs Status")]
            [MaxLength(10, ErrorMessage = "Customs Status cannot be longer than 10 characters")]
    		public string  CustomsStatus { get; set; }
    
    		    
    		/// <summary>
    		/// WHAct PCS
    		/// </summary>        
    	//    [DisplayName("WHAct PCS")]
    		public Nullable<int>  WHActPCS { get; set; }
    
    		    
    		/// <summary>
    		/// WHAct PCSUOM
    		/// </summary>        
    	//    [DisplayName("WHAct PCSUOM")]
            [MaxLength(5, ErrorMessage = "WHAct PCSUOM cannot be longer than 5 characters")]
    		public string  WHActPCSUOM { get; set; }
    
    		    
    		/// <summary>
    		/// WHAct WT
    		/// </summary>        
    	//    [DisplayName("WHAct WT")]
    		public Nullable<double>  WHActWT { get; set; }
    
    		    
    		/// <summary>
    		/// WHAct WTUOM
    		/// </summary>        
    	//    [DisplayName("WHAct WTUOM")]
            [MaxLength(5, ErrorMessage = "WHAct WTUOM cannot be longer than 5 characters")]
    		public string  WHActWTUOM { get; set; }
    
    		    
    		/// <summary>
    		/// WHAct VWT
    		/// </summary>        
    	//    [DisplayName("WHAct VWT")]
    		public Nullable<double>  WHActVWT { get; set; }
    
    		    
    		/// <summary>
    		/// WHAct VWTUOM
    		/// </summary>        
    	//    [DisplayName("WHAct VWTUOM")]
            [MaxLength(5, ErrorMessage = "WHAct VWTUOM cannot be longer than 5 characters")]
    		public string  WHActVWTUOM { get; set; }
    
    		    
    		/// <summary>
    		/// WHLocation
    		/// </summary>        
    	//    [DisplayName("WHLocation")]
            [MaxLength(20, ErrorMessage = "WHLocation cannot be longer than 20 characters")]
    		public string  WHLocation { get; set; }
    
    		    
    		/// <summary>
    		/// WHArrival Date
    		/// </summary>        
    	//    [DisplayName("WHArrival Date")]
    		public Nullable<System.DateTime>  WHArrivalDate { get; set; }
    
    		    
    		/// <summary>
    		/// WHPullout Date
    		/// </summary>        
    	//    [DisplayName("WHPullout Date")]
    		public Nullable<System.DateTime>  WHPulloutDate { get; set; }
    
    		    
    		/// <summary>
    		/// WHContainer
    		/// </summary>        
    	//    [DisplayName("WHContainer")]
            [MaxLength(50, ErrorMessage = "WHContainer cannot be longer than 50 characters")]
    		public string  WHContainer { get; set; }
    
    		    
    		/// <summary>
    		/// WHRemark
    		/// </summary>        
    	//    [DisplayName("WHRemark")]
            [MaxLength(255, ErrorMessage = "WHRemark cannot be longer than 255 characters")]
    		public string  WHRemark { get; set; }
    
    		    
    		/// <summary>
    		/// WHCargo Status
    		/// </summary>        
    	//    [DisplayName("WHCargo Status")]
            [MaxLength(10, ErrorMessage = "WHCargo Status cannot be longer than 10 characters")]
    		public string  WHCargoStatus { get; set; }
    
    		    
    		/// <summary>
    		/// Pre Alert User
    		/// </summary>        
    	//    [DisplayName("Pre Alert User")]
            [MaxLength(20, ErrorMessage = "Pre Alert User cannot be longer than 20 characters")]
    		public string  PreAlertUser { get; set; }
    
    		    
    		/// <summary>
    		/// Pre Alert Date
    		/// </summary>        
    	//    [DisplayName("Pre Alert Date")]
    		public Nullable<System.DateTime>  PreAlertDate { get; set; }
    
    		    
    		/// <summary>
    		/// WHReceipt No
    		/// </summary>        
    	//    [DisplayName("WHReceipt No")]
            [MaxLength(20, ErrorMessage = "WHReceipt No cannot be longer than 20 characters")]
    		public string  WHReceiptNo { get; set; }
    
    		    
    		/// <summary>
    		/// HAWBNo
    		/// </summary>        
    	//    [DisplayName("HAWBNo")]
            [MaxLength(20, ErrorMessage = "HAWBNo cannot be longer than 20 characters")]
    		public string  HAWBNo { get; set; }
    
    		    
    		/// <summary>
    		/// is IT
    		/// </summary>        
    	//    [DisplayName("is IT")]
            [Required(ErrorMessage = "is IT is required")]
    		public bool  isIT { get; set; }
    
    		    
    		/// <summary>
    		/// Station ID
    		/// </summary>        
    	//    [DisplayName("Station ID")]
            [Required(ErrorMessage = "Station ID is required")]
            [MaxLength(10, ErrorMessage = "Station ID cannot be longer than 10 characters")]
    		public string  StationID { get; set; }
    
    		    
    		/// <summary>
    		/// Created By
    		/// </summary>        
    	//    [DisplayName("Created By")]
            [Required(ErrorMessage = "Created By is required")]
            [MaxLength(6, ErrorMessage = "Created By cannot be longer than 6 characters")]
    		public string  CreatedBy { get; set; }
    
    		    
    		/// <summary>
    		/// Created Date
    		/// </summary>        
    	//    [DisplayName("Created Date")]
            [Required(ErrorMessage = "Created Date is required")]
    		public System.DateTime  CreatedDate { get; set; }
    
    		    
    		/// <summary>
    		/// Updated By
    		/// </summary>        
    	//    [DisplayName("Updated By")]
            [Required(ErrorMessage = "Updated By is required")]
            [MaxLength(6, ErrorMessage = "Updated By cannot be longer than 6 characters")]
    		public string  UpdatedBy { get; set; }
    
    		    
    		/// <summary>
    		/// Updated Date
    		/// </summary>        
    	//    [DisplayName("Updated Date")]
    		public Nullable<System.DateTime>  UpdatedDate { get; set; }
    
    		    
    		/// <summary>
    		/// SStation ID
    		/// </summary>        
    	//    [DisplayName("SStation ID")]
            [MaxLength(10, ErrorMessage = "SStation ID cannot be longer than 10 characters")]
    		public string  SStationID { get; set; }
    
    		    
    		/// <summary>
    		/// Shpt Type
    		/// </summary>        
    	//    [DisplayName("Shpt Type")]
            [MaxLength(50, ErrorMessage = "Shpt Type cannot be longer than 50 characters")]
    		public string  ShptType { get; set; }
    
    		    
    		/// <summary>
    		/// SPL
    		/// </summary>        
    	//    [DisplayName("SPL")]
            [MaxLength(20, ErrorMessage = "SPL cannot be longer than 20 characters")]
    		public string  SPL { get; set; }
    
    		    
    		/// <summary>
    		/// IMPStation
    		/// </summary>        
    	//    [DisplayName("IMPStation")]
            [MaxLength(10, ErrorMessage = "IMPStation cannot be longer than 10 characters")]
    		public string  IMPStation { get; set; }
    
    		    
    		/// <summary>
    		/// Reference Code
    		/// </summary>        
    	//    [DisplayName("Reference Code")]
            [MaxLength(50, ErrorMessage = "Reference Code cannot be longer than 50 characters")]
    		public string  ReferenceCode { get; set; }
    
    		    
    		/// <summary>
    		/// Service Level
    		/// </summary>        
    	//    [DisplayName("Service Level")]
            [MaxLength(10, ErrorMessage = "Service Level cannot be longer than 10 characters")]
    		public string  ServiceLevel { get; set; }
    
    		    
    		/// <summary>
    		/// PODTemplate
    		/// </summary>        
    	//    [DisplayName("PODTemplate")]
            [MaxLength(50, ErrorMessage = "PODTemplate cannot be longer than 50 characters")]
    		public string  PODTemplate { get; set; }
    
    		    
    		/// <summary>
    		/// MTSNO
    		/// </summary>        
    	//    [DisplayName("MTSNO")]
            [MaxLength(15, ErrorMessage = "MTSNO cannot be longer than 15 characters")]
    		public string  MTSNO { get; set; }
    
    		    
    		/// <summary>
    		/// SCHCargo Terminal Date
    		/// </summary>        
    	//    [DisplayName("SCHCargo Terminal Date")]
    		public Nullable<System.DateTime>  SCHCargoTerminalDate { get; set; }
    
    		    
    		/// <summary>
    		/// Import Agent
    		/// </summary>        
    	//    [DisplayName("Import Agent")]
    		public Nullable<int>  ImportAgent { get; set; }
    
    		    
    		/// <summary>
    		/// FRTParty
    		/// </summary>        
    	//    [DisplayName("FRTParty")]
            [MaxLength(50, ErrorMessage = "FRTParty cannot be longer than 50 characters")]
    		public string  FRTParty { get; set; }
    
    		    
    		/// <summary>
    		/// HBroker
    		/// </summary>        
    	//    [DisplayName("HBroker")]
    		public Nullable<int>  HBroker { get; set; }
    
    		    
    		/// <summary>
    		/// DBID
    		/// </summary>        
    	//    [DisplayName("DBID")]
            [MaxLength(3, ErrorMessage = "DBID cannot be longer than 3 characters")]
    		public string  DBID { get; set; }
    
    		    
    		/// <summary>
    		/// Extra0
    		/// </summary>        
    	//    [DisplayName("Extra0")]
            [MaxLength(50, ErrorMessage = "Extra0 cannot be longer than 50 characters")]
    		public string  Extra0 { get; set; }
    
    		    
    		/// <summary>
    		/// Extra1
    		/// </summary>        
    	//    [DisplayName("Extra1")]
            [MaxLength(50, ErrorMessage = "Extra1 cannot be longer than 50 characters")]
    		public string  Extra1 { get; set; }
    
    		    
    		/// <summary>
    		/// Extra2
    		/// </summary>        
    	//    [DisplayName("Extra2")]
            [MaxLength(50, ErrorMessage = "Extra2 cannot be longer than 50 characters")]
    		public string  Extra2 { get; set; }
    
    		    
    		/// <summary>
    		/// Extra3
    		/// </summary>        
    	//    [DisplayName("Extra3")]
            [MaxLength(50, ErrorMessage = "Extra3 cannot be longer than 50 characters")]
    		public string  Extra3 { get; set; }
    
    		    
    		/// <summary>
    		/// Extra4
    		/// </summary>        
    	//    [DisplayName("Extra4")]
            [MaxLength(50, ErrorMessage = "Extra4 cannot be longer than 50 characters")]
    		public string  Extra4 { get; set; }
    
    		    
    		/// <summary>
    		/// Extra5
    		/// </summary>        
    	//    [DisplayName("Extra5")]
            [MaxLength(50, ErrorMessage = "Extra5 cannot be longer than 50 characters")]
    		public string  Extra5 { get; set; }
    
    		    
    		/// <summary>
    		/// Extra6
    		/// </summary>        
    	//    [DisplayName("Extra6")]
            [MaxLength(50, ErrorMessage = "Extra6 cannot be longer than 50 characters")]
    		public string  Extra6 { get; set; }
    
    		    
    		/// <summary>
    		/// Extra7
    		/// </summary>        
    	//    [DisplayName("Extra7")]
            [MaxLength(50, ErrorMessage = "Extra7 cannot be longer than 50 characters")]
    		public string  Extra7 { get; set; }
    
    		    
    		/// <summary>
    		/// Extra8
    		/// </summary>        
    	//    [DisplayName("Extra8")]
            [MaxLength(50, ErrorMessage = "Extra8 cannot be longer than 50 characters")]
    		public string  Extra8 { get; set; }
    
    		    
    		/// <summary>
    		/// Extra9
    		/// </summary>        
    	//    [DisplayName("Extra9")]
            [MaxLength(50, ErrorMessage = "Extra9 cannot be longer than 50 characters")]
    		public string  Extra9 { get; set; }
    
    		    
    		/// <summary>
    		/// Copy AEBy
    		/// </summary>        
    	//    [DisplayName("Copy AEBy")]
            [MaxLength(6, ErrorMessage = "Copy AEBy cannot be longer than 6 characters")]
    		public string  CopyAEBy { get; set; }
    
    		    
    		/// <summary>
    		/// Copy AEDate
    		/// </summary>        
    	//    [DisplayName("Copy AEDate")]
    		public Nullable<System.DateTime>  CopyAEDate { get; set; }
    
    		    
    		/// <summary>
    		/// Copy AEID
    		/// </summary>        
    	//    [DisplayName("Copy AEID")]
    		public Nullable<int>  CopyAEID { get; set; }
    
    		    
    		/// <summary>
    		/// Version
    		/// </summary>        
    	//    [DisplayName("Version")]
            [MaxLength(8, ErrorMessage = "Version cannot be longer than 8 characters")]
    		public byte[]  Version { get; set; }
    
    		    
    	}
    //}
    
}
