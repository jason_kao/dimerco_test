using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ValuePlus.ViewModel;



namespace ValuePlus.ViewModel.LocaleChainVP
{
    
    /// <summary>
    /// AIITEntry class
    /// </summary>
    //[MetadataType(typeof(AIITEntryViewModel))]
    //public  partial class AIITEntry
    //{
    
    	/// <summary>
    	/// AIITEntry Metadata class
    	/// </summary>
    	public   class AIITEntryViewModel:ViewModelBase
    	{
    		    
    		/// <summary>
    		/// ID
    		/// </summary>        
    	//    [DisplayName("ID")]
            [Required(ErrorMessage = "ID is required")]
    		public int  ID { get; set; }
    
    		    
    		/// <summary>
    		/// Forwarding Type
    		/// </summary>        
    	//    [DisplayName("Forwarding Type")]
    		public Nullable<int>  ForwardingType { get; set; }
    
    		    
    		/// <summary>
    		/// Forwarding ITNo
    		/// </summary>        
    	//    [DisplayName("Forwarding ITNo")]
            [MaxLength(8, ErrorMessage = "Forwarding ITNo cannot be longer than 8 characters")]
    		public string  ForwardingITNo { get; set; }
    
    		    
    		/// <summary>
    		/// Transport By
    		/// </summary>        
    	//    [DisplayName("Transport By")]
    		public Nullable<int>  TransportBy { get; set; }
    
    		    
    		/// <summary>
    		/// Bill Pro No
    		/// </summary>        
    	//    [DisplayName("Bill Pro No")]
            [MaxLength(20, ErrorMessage = "Bill Pro No cannot be longer than 20 characters")]
    		public string  BillProNo { get; set; }
    
    		    
    		/// <summary>
    		/// IEService By
    		/// </summary>        
    	//    [DisplayName("IEService By")]
    		public Nullable<int>  IEServiceBy { get; set; }
    
    		    
    		/// <summary>
    		/// IECarrier AWBNo
    		/// </summary>        
    	//    [DisplayName("IECarrier AWBNo")]
            [MaxLength(12, ErrorMessage = "IECarrier AWBNo cannot be longer than 12 characters")]
    		public string  IECarrierAWBNo { get; set; }
    
    		    
    		/// <summary>
    		/// ETD
    		/// </summary>        
    	//    [DisplayName("ETD")]
    		public Nullable<System.DateTime>  ETD { get; set; }
    
    		    
    		/// <summary>
    		/// ETA
    		/// </summary>        
    	//    [DisplayName("ETA")]
    		public Nullable<System.DateTime>  ETA { get; set; }
    
    		    
    		/// <summary>
    		/// ATD
    		/// </summary>        
    	//    [DisplayName("ATD")]
    		public Nullable<System.DateTime>  ATD { get; set; }
    
    		    
    		/// <summary>
    		/// ATA
    		/// </summary>        
    	//    [DisplayName("ATA")]
    		public Nullable<System.DateTime>  ATA { get; set; }
    
    		    
    		/// <summary>
    		/// MAWBID
    		/// </summary>        
    	//    [DisplayName("MAWBID")]
            [Required(ErrorMessage = "MAWBID is required")]
    		public int  MAWBID { get; set; }
    
    		    
    		/// <summary>
    		/// Station ID
    		/// </summary>        
    	//    [DisplayName("Station ID")]
            [Required(ErrorMessage = "Station ID is required")]
            [MaxLength(10, ErrorMessage = "Station ID cannot be longer than 10 characters")]
    		public string  StationID { get; set; }
    
    		    
    		/// <summary>
    		/// Created By
    		/// </summary>        
    	//    [DisplayName("Created By")]
            [Required(ErrorMessage = "Created By is required")]
            [MaxLength(6, ErrorMessage = "Created By cannot be longer than 6 characters")]
    		public string  CreatedBy { get; set; }
    
    		    
    		/// <summary>
    		/// Created Date
    		/// </summary>        
    	//    [DisplayName("Created Date")]
            [Required(ErrorMessage = "Created Date is required")]
    		public System.DateTime  CreatedDate { get; set; }
    
    		    
    		/// <summary>
    		/// Updated By
    		/// </summary>        
    	//    [DisplayName("Updated By")]
            [Required(ErrorMessage = "Updated By is required")]
            [MaxLength(6, ErrorMessage = "Updated By cannot be longer than 6 characters")]
    		public string  UpdatedBy { get; set; }
    
    		    
    		/// <summary>
    		/// Updated Date
    		/// </summary>        
    	//    [DisplayName("Updated Date")]
            [Required(ErrorMessage = "Updated Date is required")]
    		public System.DateTime  UpdatedDate { get; set; }
    
    		    
    		/// <summary>
    		/// Version
    		/// </summary>        
    	//    [DisplayName("Version")]
            [MaxLength(8, ErrorMessage = "Version cannot be longer than 8 characters")]
    		public byte[]  Version { get; set; }
    
    		    
    	}
    //}
    
}
