using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ValuePlus.ViewModel;



namespace ValuePlus.ViewModel.LocaleChainVP
{
    
    /// <summary>
    /// TPAIRData class
    /// </summary>
    //[MetadataType(typeof(TPAIRDataViewModel))]
    //public  partial class TPAIRData
    //{
    
    	/// <summary>
    	/// TPAIRData Metadata class
    	/// </summary>
    	public   class TPAIRDataViewModel:ViewModelBase
    	{
    		    
    		/// <summary>
    		/// ID
    		/// </summary>        
    	//    [DisplayName("ID")]
            [Required(ErrorMessage = "ID is required")]
    		public int  ID { get; set; }
    
    		    
    		/// <summary>
    		/// Station ID
    		/// </summary>        
    	//    [DisplayName("Station ID")]
            [Required(ErrorMessage = "Station ID is required")]
            [MaxLength(3, ErrorMessage = "Station ID cannot be longer than 3 characters")]
    		public string  StationID { get; set; }
    
    		    
    		/// <summary>
    		/// TPID
    		/// </summary>        
    	//    [DisplayName("TPID")]
            [Required(ErrorMessage = "TPID is required")]
            [MaxLength(15, ErrorMessage = "TPID cannot be longer than 15 characters")]
    		public string  TPID { get; set; }
    
    		    
    		/// <summary>
    		/// Customer ID
    		/// </summary>        
    	//    [DisplayName("Customer ID")]
            [Required(ErrorMessage = "Customer ID is required")]
    		public int  CustomerID { get; set; }
    
    		    
    		/// <summary>
    		/// Source Station ID
    		/// </summary>        
    	//    [DisplayName("Source Station ID")]
            [MaxLength(3, ErrorMessage = "Source Station ID cannot be longer than 3 characters")]
    		public string  SourceStationID { get; set; }
    
    		    
    		/// <summary>
    		/// Source Type
    		/// </summary>        
    	//    [DisplayName("Source Type")]
            [MaxLength(1, ErrorMessage = "Source Type cannot be longer than 1 characters")]
    		public string  SourceType { get; set; }
    
    		    
    		/// <summary>
    		/// Source ID
    		/// </summary>        
    	//    [DisplayName("Source ID")]
    		public Nullable<int>  SourceID { get; set; }
    
    		    
    		/// <summary>
    		/// Master No
    		/// </summary>        
    	//    [DisplayName("Master No")]
            [Required(ErrorMessage = "Master No is required")]
            [MaxLength(30, ErrorMessage = "Master No cannot be longer than 30 characters")]
    		public string  MasterNo { get; set; }
    
    		    
    		/// <summary>
    		/// House No
    		/// </summary>        
    	//    [DisplayName("House No")]
            [Required(ErrorMessage = "House No is required")]
            [MaxLength(30, ErrorMessage = "House No cannot be longer than 30 characters")]
    		public string  HouseNo { get; set; }
    
    		    
    		/// <summary>
    		/// Lot
    		/// </summary>        
    	//    [DisplayName("Lot")]
            [MaxLength(30, ErrorMessage = "Lot cannot be longer than 30 characters")]
    		public string  Lot { get; set; }
    
    		    
    		/// <summary>
    		/// Shipper
    		/// </summary>        
    	//    [DisplayName("Shipper")]
    		public Nullable<int>  Shipper { get; set; }
    
    		    
    		/// <summary>
    		/// Cnee
    		/// </summary>        
    	//    [DisplayName("Cnee")]
    		public Nullable<int>  Cnee { get; set; }
    
    		    
    		/// <summary>
    		/// Receipt
    		/// </summary>        
    	//    [DisplayName("Receipt")]
    		public Nullable<int>  Receipt { get; set; }
    
    		    
    		/// <summary>
    		/// Delivery
    		/// </summary>        
    	//    [DisplayName("Delivery")]
    		public Nullable<int>  Delivery { get; set; }
    
    		    
    		/// <summary>
    		/// ORGN
    		/// </summary>        
    	//    [DisplayName("ORGN")]
    		public Nullable<int>  ORGN { get; set; }
    
    		    
    		/// <summary>
    		/// DSTN
    		/// </summary>        
    	//    [DisplayName("DSTN")]
    		public Nullable<int>  DSTN { get; set; }
    
    		    
    		/// <summary>
    		/// Neture Of Good
    		/// </summary>        
    	//    [DisplayName("Neture Of Good")]
            [MaxLength(30, ErrorMessage = "Neture Of Good cannot be longer than 30 characters")]
    		public string  NetureOfGood { get; set; }
    
    		    
    		/// <summary>
    		/// Act PCS
    		/// </summary>        
    	//    [DisplayName("Act PCS")]
    		public Nullable<int>  ActPCS { get; set; }
    
    		    
    		/// <summary>
    		/// Act PCSUOM
    		/// </summary>        
    	//    [DisplayName("Act PCSUOM")]
            [MaxLength(10, ErrorMessage = "Act PCSUOM cannot be longer than 10 characters")]
    		public string  ActPCSUOM { get; set; }
    
    		    
    		/// <summary>
    		/// CWT
    		/// </summary>        
    	//    [DisplayName("CWT")]
    		public Nullable<double>  CWT { get; set; }
    
    		    
    		/// <summary>
    		/// WTUOM
    		/// </summary>        
    	//    [DisplayName("WTUOM")]
            [MaxLength(5, ErrorMessage = "WTUOM cannot be longer than 5 characters")]
    		public string  WTUOM { get; set; }
    
    		    
    		/// <summary>
    		/// Sales Person
    		/// </summary>        
    	//    [DisplayName("Sales Person")]
    		public Nullable<int>  SalesPerson { get; set; }
    
    		    
    		/// <summary>
    		/// ATD
    		/// </summary>        
    	//    [DisplayName("ATD")]
    		public Nullable<System.DateTime>  ATD { get; set; }
    
    		    
    		/// <summary>
    		/// ATA
    		/// </summary>        
    	//    [DisplayName("ATA")]
    		public Nullable<System.DateTime>  ATA { get; set; }
    
    		    
    		/// <summary>
    		/// Lot Status
    		/// </summary>        
    	//    [DisplayName("Lot Status")]
            [Required(ErrorMessage = "Lot Status is required")]
            [MaxLength(1, ErrorMessage = "Lot Status cannot be longer than 1 characters")]
    		public string  LotStatus { get; set; }
    
    		    
    		/// <summary>
    		/// Is Mani Fast
    		/// </summary>        
    	//    [DisplayName("Is Mani Fast")]
            [Required(ErrorMessage = "Is Mani Fast is required")]
            [MaxLength(1, ErrorMessage = "Is Mani Fast cannot be longer than 1 characters")]
    		public string  IsManiFast { get; set; }
    
    		    
    		/// <summary>
    		/// Created By
    		/// </summary>        
    	//    [DisplayName("Created By")]
            [MaxLength(6, ErrorMessage = "Created By cannot be longer than 6 characters")]
    		public string  CreatedBy { get; set; }
    
    		    
    		/// <summary>
    		/// Created Date
    		/// </summary>        
    	//    [DisplayName("Created Date")]
    		public Nullable<System.DateTime>  CreatedDate { get; set; }
    
    		    
    		/// <summary>
    		/// Updated By
    		/// </summary>        
    	//    [DisplayName("Updated By")]
            [Required(ErrorMessage = "Updated By is required")]
            [MaxLength(6, ErrorMessage = "Updated By cannot be longer than 6 characters")]
    		public string  UpdatedBy { get; set; }
    
    		    
    		/// <summary>
    		/// Updated Date
    		/// </summary>        
    	//    [DisplayName("Updated Date")]
            [Required(ErrorMessage = "Updated Date is required")]
    		public System.DateTime  UpdatedDate { get; set; }
    
    		    
    		/// <summary>
    		/// Issue Date
    		/// </summary>        
    	//    [DisplayName("Issue Date")]
    		public Nullable<System.DateTime>  IssueDate { get; set; }
    
    		    
    		/// <summary>
    		/// Service Level
    		/// </summary>        
    	//    [DisplayName("Service Level")]
            [MaxLength(20, ErrorMessage = "Service Level cannot be longer than 20 characters")]
    		public string  ServiceLevel { get; set; }
    
    		    
    		/// <summary>
    		/// Scale
    		/// </summary>        
    	//    [DisplayName("Scale")]
            [MaxLength(20, ErrorMessage = "Scale cannot be longer than 20 characters")]
    		public string  Scale { get; set; }
    
    		    
    		/// <summary>
    		/// Pickup Date
    		/// </summary>        
    	//    [DisplayName("Pickup Date")]
    		public Nullable<System.DateTime>  PickupDate { get; set; }
    
    		    
    		/// <summary>
    		/// VWT
    		/// </summary>        
    	//    [DisplayName("VWT")]
    		public Nullable<double>  VWT { get; set; }
    
    		    
    		/// <summary>
    		/// Pre Master
    		/// </summary>        
    	//    [DisplayName("Pre Master")]
            [MaxLength(30, ErrorMessage = "Pre Master cannot be longer than 30 characters")]
    		public string  PreMaster { get; set; }
    
    		    
    		/// <summary>
    		/// Confirm ID
    		/// </summary>        
    	//    [DisplayName("Confirm ID")]
            [MaxLength(50, ErrorMessage = "Confirm ID cannot be longer than 50 characters")]
    		public string  ConfirmID { get; set; }
    
    		    
    		/// <summary>
    		/// Shpt Type
    		/// </summary>        
    	//    [DisplayName("Shpt Type")]
            [MaxLength(10, ErrorMessage = "Shpt Type cannot be longer than 10 characters")]
    		public string  ShptType { get; set; }
    
    		    
    		/// <summary>
    		/// Onboard FLT
    		/// </summary>        
    	//    [DisplayName("Onboard FLT")]
            [MaxLength(50, ErrorMessage = "Onboard FLT cannot be longer than 50 characters")]
    		public string  OnboardFLT { get; set; }
    
    		    
    		/// <summary>
    		/// is DAS
    		/// </summary>        
    	//    [DisplayName("is DAS")]
            [MaxLength(1, ErrorMessage = "is DAS cannot be longer than 1 characters")]
    		public string  isDAS { get; set; }
    
    		    
    		/// <summary>
    		/// DESC
    		/// </summary>        
    	//    [DisplayName("DESC")]
            [MaxLength(255, ErrorMessage = "DESC cannot be longer than 255 characters")]
    		public string  DESC { get; set; }
    
    		    
    		/// <summary>
    		/// Remark
    		/// </summary>        
    	//    [DisplayName("Remark")]
            [MaxLength(255, ErrorMessage = "Remark cannot be longer than 255 characters")]
    		public string  Remark { get; set; }
    
    		    
    		/// <summary>
    		/// Marks
    		/// </summary>        
    	//    [DisplayName("Marks")]
            [MaxLength(255, ErrorMessage = "Marks cannot be longer than 255 characters")]
    		public string  Marks { get; set; }
    
    		    
    		/// <summary>
    		/// Version
    		/// </summary>        
    	//    [DisplayName("Version")]
            [MaxLength(8, ErrorMessage = "Version cannot be longer than 8 characters")]
    		public byte[]  Version { get; set; }
    
    		    
    	}
    //}
    
}
