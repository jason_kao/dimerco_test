using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ValuePlus.ViewModel;



namespace ValuePlus.ViewModel.LocaleChainVP
{
    
    /// <summary>
    /// OIBookingSO class
    /// </summary>
    //[MetadataType(typeof(OIBookingSOViewModel))]
    //public  partial class OIBookingSO
    //{
    
    	/// <summary>
    	/// OIBookingSO Metadata class
    	/// </summary>
    	public   class OIBookingSOViewModel:ViewModelBase
    	{
    		    
    		/// <summary>
    		/// ID
    		/// </summary>        
    	//    [DisplayName("ID")]
            [Required(ErrorMessage = "ID is required")]
    		public int  ID { get; set; }
    
    		    
    		/// <summary>
    		/// Station ID
    		/// </summary>        
    	//    [DisplayName("Station ID")]
            [MaxLength(5, ErrorMessage = "Station ID cannot be longer than 5 characters")]
    		public string  StationID { get; set; }
    
    		    
    		/// <summary>
    		/// So For
    		/// </summary>        
    	//    [DisplayName("So For")]
            [MaxLength(5, ErrorMessage = "So For cannot be longer than 5 characters")]
    		public string  SoFor { get; set; }
    
    		    
    		/// <summary>
    		/// Src Station ID
    		/// </summary>        
    	//    [DisplayName("Src Station ID")]
            [MaxLength(5, ErrorMessage = "Src Station ID cannot be longer than 5 characters")]
    		public string  SrcStationID { get; set; }
    
    		    
    		/// <summary>
    		/// Src ID
    		/// </summary>        
    	//    [DisplayName("Src ID")]
    		public Nullable<int>  SrcID { get; set; }
    
    		    
    		/// <summary>
    		/// Booking ID
    		/// </summary>        
    	//    [DisplayName("Booking ID")]
    		public Nullable<int>  BookingID { get; set; }
    
    		    
    		/// <summary>
    		/// HBLID
    		/// </summary>        
    	//    [DisplayName("HBLID")]
    		public Nullable<int>  HBLID { get; set; }
    
    		    
    		/// <summary>
    		/// SONo
    		/// </summary>        
    	//    [DisplayName("SONo")]
            [MaxLength(60, ErrorMessage = "SONo cannot be longer than 60 characters")]
    		public string  SONo { get; set; }
    
    		    
    		/// <summary>
    		/// PCS
    		/// </summary>        
    	//    [DisplayName("PCS")]
    		public Nullable<int>  PCS { get; set; }
    
    		    
    		/// <summary>
    		/// PCSUOM
    		/// </summary>        
    	//    [DisplayName("PCSUOM")]
            [MaxLength(10, ErrorMessage = "PCSUOM cannot be longer than 10 characters")]
    		public string  PCSUOM { get; set; }
    
    		    
    		/// <summary>
    		/// Weight
    		/// </summary>        
    	//    [DisplayName("Weight")]
    		public Nullable<decimal>  Weight { get; set; }
    
    		    
    		/// <summary>
    		/// Weight UOM
    		/// </summary>        
    	//    [DisplayName("Weight UOM")]
            [MaxLength(10, ErrorMessage = "Weight UOM cannot be longer than 10 characters")]
    		public string  WeightUOM { get; set; }
    
    		    
    		/// <summary>
    		/// CBM
    		/// </summary>        
    	//    [DisplayName("CBM")]
    		public Nullable<decimal>  CBM { get; set; }
    
    		    
    		/// <summary>
    		/// CBMUOM
    		/// </summary>        
    	//    [DisplayName("CBMUOM")]
            [MaxLength(10, ErrorMessage = "CBMUOM cannot be longer than 10 characters")]
    		public string  CBMUOM { get; set; }
    
    		    
    		/// <summary>
    		/// Description
    		/// </summary>        
    	//    [DisplayName("Description")]
            [MaxLength(1000, ErrorMessage = "Description cannot be longer than 1000 characters")]
    		public string  Description { get; set; }
    
    		    
    		/// <summary>
    		/// DSTNStation ID
    		/// </summary>        
    	//    [DisplayName("DSTNStation ID")]
            [MaxLength(5, ErrorMessage = "DSTNStation ID cannot be longer than 5 characters")]
    		public string  DSTNStationID { get; set; }
    
    		    
    		/// <summary>
    		/// DSTNID
    		/// </summary>        
    	//    [DisplayName("DSTNID")]
    		public Nullable<int>  DSTNID { get; set; }
    
    		    
    		/// <summary>
    		/// Created By
    		/// </summary>        
    	//    [DisplayName("Created By")]
            [MaxLength(50, ErrorMessage = "Created By cannot be longer than 50 characters")]
    		public string  CreatedBy { get; set; }
    
    		    
    		/// <summary>
    		/// Created DT
    		/// </summary>        
    	//    [DisplayName("Created DT")]
    		public Nullable<System.DateTime>  CreatedDT { get; set; }
    
    		    
    		/// <summary>
    		/// Updated By
    		/// </summary>        
    	//    [DisplayName("Updated By")]
            [MaxLength(50, ErrorMessage = "Updated By cannot be longer than 50 characters")]
    		public string  UpdatedBy { get; set; }
    
    		    
    		/// <summary>
    		/// Updated DT
    		/// </summary>        
    	//    [DisplayName("Updated DT")]
    		public Nullable<System.DateTime>  UpdatedDT { get; set; }
    
    		    
    		/// <summary>
    		/// CTNRType
    		/// </summary>        
    	//    [DisplayName("CTNRType")]
            [MaxLength(20, ErrorMessage = "CTNRType cannot be longer than 20 characters")]
    		public string  CTNRType { get; set; }
    
    		    
    		/// <summary>
    		/// CTNRNo
    		/// </summary>        
    	//    [DisplayName("CTNRNo")]
            [MaxLength(20, ErrorMessage = "CTNRNo cannot be longer than 20 characters")]
    		public string  CTNRNo { get; set; }
    
    		    
    		/// <summary>
    		/// Version
    		/// </summary>        
    	//    [DisplayName("Version")]
            [MaxLength(8, ErrorMessage = "Version cannot be longer than 8 characters")]
    		public byte[]  Version { get; set; }
    
    		    
    	}
    //}
    
}
