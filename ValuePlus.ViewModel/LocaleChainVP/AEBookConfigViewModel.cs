using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ValuePlus.ViewModel;



namespace ValuePlus.ViewModel.LocaleChainVP
{
    
    /// <summary>
    /// AEBookConfig class
    /// </summary>
    //[MetadataType(typeof(AEBookConfigViewModel))]
    //public  partial class AEBookConfig
    //{
    
    	/// <summary>
    	/// AEBookConfig Metadata class
    	/// </summary>
    	public   class AEBookConfigViewModel:ViewModelBase
    	{
    		    
    		/// <summary>
    		/// ID
    		/// </summary>        
    	//    [DisplayName("ID")]
            [Required(ErrorMessage = "ID is required")]
    		public int  ID { get; set; }
    
    		    
    		/// <summary>
    		/// Config Type
    		/// </summary>        
    	//    [DisplayName("Config Type")]
            [Required(ErrorMessage = "Config Type is required")]
            [MaxLength(100, ErrorMessage = "Config Type cannot be longer than 100 characters")]
    		public string  ConfigType { get; set; }
    
    		    
    		/// <summary>
    		/// Config Value
    		/// </summary>        
    	//    [DisplayName("Config Value")]
            [Required(ErrorMessage = "Config Value is required")]
            [MaxLength(100, ErrorMessage = "Config Value cannot be longer than 100 characters")]
    		public string  ConfigValue { get; set; }
    
    		    
    		/// <summary>
    		/// Config Local Value
    		/// </summary>        
    	//    [DisplayName("Config Local Value")]
            [MaxLength(100, ErrorMessage = "Config Local Value cannot be longer than 100 characters")]
    		public string  ConfigLocalValue { get; set; }
    
    		    
    		/// <summary>
    		/// Status
    		/// </summary>        
    	//    [DisplayName("Status")]
            [MaxLength(50, ErrorMessage = "Status cannot be longer than 50 characters")]
    		public string  Status { get; set; }
    
    		    
    		/// <summary>
    		/// Created By
    		/// </summary>        
    	//    [DisplayName("Created By")]
            [MaxLength(10, ErrorMessage = "Created By cannot be longer than 10 characters")]
    		public string  CreatedBy { get; set; }
    
    		    
    		/// <summary>
    		/// Created Date
    		/// </summary>        
    	//    [DisplayName("Created Date")]
    		public Nullable<System.DateTime>  CreatedDate { get; set; }
    
    		    
    		/// <summary>
    		/// Updated By
    		/// </summary>        
    	//    [DisplayName("Updated By")]
            [MaxLength(10, ErrorMessage = "Updated By cannot be longer than 10 characters")]
    		public string  UpdatedBy { get; set; }
    
    		    
    		/// <summary>
    		/// Updated Date
    		/// </summary>        
    	//    [DisplayName("Updated Date")]
    		public Nullable<System.DateTime>  UpdatedDate { get; set; }
    
    		    
    	}
    //}
    
}
