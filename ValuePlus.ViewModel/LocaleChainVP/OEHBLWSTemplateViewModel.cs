using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ValuePlus.ViewModel;



namespace ValuePlus.ViewModel.LocaleChainVP
{
    
    /// <summary>
    /// OEHBLWSTemplate class
    /// </summary>
    //[MetadataType(typeof(OEHBLWSTemplateViewModel))]
    //public  partial class OEHBLWSTemplate
    //{
    
    	/// <summary>
    	/// OEHBLWSTemplate Metadata class
    	/// </summary>
    	public   class OEHBLWSTemplateViewModel:ViewModelBase
    	{
    		    
    		/// <summary>
    		/// ID
    		/// </summary>        
    	//    [DisplayName("ID")]
            [Required(ErrorMessage = "ID is required")]
    		public int  ID { get; set; }
    
    		    
    		/// <summary>
    		/// Template Name
    		/// </summary>        
    	//    [DisplayName("Template Name")]
            [MaxLength(100, ErrorMessage = "Template Name cannot be longer than 100 characters")]
    		public string  TemplateName { get; set; }
    
    		    
    		/// <summary>
    		/// HBLID
    		/// </summary>        
    	//    [DisplayName("HBLID")]
    		public Nullable<int>  HBLID { get; set; }
    
    		    
    		/// <summary>
    		/// BKNo
    		/// </summary>        
    	//    [DisplayName("BKNo")]
            [MaxLength(20, ErrorMessage = "BKNo cannot be longer than 20 characters")]
    		public string  BKNo { get; set; }
    
    		    
    		/// <summary>
    		/// HBLNo
    		/// </summary>        
    	//    [DisplayName("HBLNo")]
            [MaxLength(20, ErrorMessage = "HBLNo cannot be longer than 20 characters")]
    		public string  HBLNo { get; set; }
    
    		    
    		/// <summary>
    		/// Lot No
    		/// </summary>        
    	//    [DisplayName("Lot No")]
            [MaxLength(20, ErrorMessage = "Lot No cannot be longer than 20 characters")]
    		public string  LotNo { get; set; }
    
    		    
    		/// <summary>
    		/// MBLNo
    		/// </summary>        
    	//    [DisplayName("MBLNo")]
            [MaxLength(20, ErrorMessage = "MBLNo cannot be longer than 20 characters")]
    		public string  MBLNo { get; set; }
    
    		    
    		/// <summary>
    		/// PONo
    		/// </summary>        
    	//    [DisplayName("PONo")]
            [MaxLength(200, ErrorMessage = "PONo cannot be longer than 200 characters")]
    		public string  PONo { get; set; }
    
    		    
    		/// <summary>
    		/// Customer Code
    		/// </summary>        
    	//    [DisplayName("Customer Code")]
    		public Nullable<int>  CustomerCode { get; set; }
    
    		    
    		/// <summary>
    		/// Re Mark
    		/// </summary>        
    	//    [DisplayName("Re Mark")]
            [MaxLength(500, ErrorMessage = "Re Mark cannot be longer than 500 characters")]
    		public string  ReMark { get; set; }
    
    		    
    		/// <summary>
    		/// Title
    		/// </summary>        
    	//    [DisplayName("Title")]
            [MaxLength(100, ErrorMessage = "Title cannot be longer than 100 characters")]
    		public string  Title { get; set; }
    
    		    
    		/// <summary>
    		/// Quotation No
    		/// </summary>        
    	//    [DisplayName("Quotation No")]
            [MaxLength(50, ErrorMessage = "Quotation No cannot be longer than 50 characters")]
    		public string  QuotationNo { get; set; }
    
    		    
    		/// <summary>
    		/// Client Name
    		/// </summary>        
    	//    [DisplayName("Client Name")]
            [MaxLength(50, ErrorMessage = "Client Name cannot be longer than 50 characters")]
    		public string  ClientName { get; set; }
    
    		    
    		/// <summary>
    		/// Client Tel
    		/// </summary>        
    	//    [DisplayName("Client Tel")]
            [MaxLength(50, ErrorMessage = "Client Tel cannot be longer than 50 characters")]
    		public string  ClientTel { get; set; }
    
    		    
    		/// <summary>
    		/// Client Fax
    		/// </summary>        
    	//    [DisplayName("Client Fax")]
            [MaxLength(50, ErrorMessage = "Client Fax cannot be longer than 50 characters")]
    		public string  ClientFax { get; set; }
    
    		    
    		/// <summary>
    		/// Client MB
    		/// </summary>        
    	//    [DisplayName("Client MB")]
            [MaxLength(50, ErrorMessage = "Client MB cannot be longer than 50 characters")]
    		public string  ClientMB { get; set; }
    
    		    
    		/// <summary>
    		/// Client PIC
    		/// </summary>        
    	//    [DisplayName("Client PIC")]
            [MaxLength(50, ErrorMessage = "Client PIC cannot be longer than 50 characters")]
    		public string  ClientPIC { get; set; }
    
    		    
    		/// <summary>
    		/// Delivery No
    		/// </summary>        
    	//    [DisplayName("Delivery No")]
            [MaxLength(50, ErrorMessage = "Delivery No cannot be longer than 50 characters")]
    		public string  DeliveryNo { get; set; }
    
    		    
    		/// <summary>
    		/// PKGS
    		/// </summary>        
    	//    [DisplayName("PKGS")]
            [MaxLength(20, ErrorMessage = "PKGS cannot be longer than 20 characters")]
    		public string  PKGS { get; set; }
    
    		    
    		/// <summary>
    		/// GW
    		/// </summary>        
    	//    [DisplayName("GW")]
            [MaxLength(20, ErrorMessage = "GW cannot be longer than 20 characters")]
    		public string  GW { get; set; }
    
    		    
    		/// <summary>
    		/// VOL
    		/// </summary>        
    	//    [DisplayName("VOL")]
            [MaxLength(20, ErrorMessage = "VOL cannot be longer than 20 characters")]
    		public string  VOL { get; set; }
    
    		    
    		/// <summary>
    		/// Carton Type
    		/// </summary>        
    	//    [DisplayName("Carton Type")]
            [MaxLength(20, ErrorMessage = "Carton Type cannot be longer than 20 characters")]
    		public string  CartonType { get; set; }
    
    		    
    		/// <summary>
    		/// POL
    		/// </summary>        
    	//    [DisplayName("POL")]
            [MaxLength(20, ErrorMessage = "POL cannot be longer than 20 characters")]
    		public string  POL { get; set; }
    
    		    
    		/// <summary>
    		/// DEST
    		/// </summary>        
    	//    [DisplayName("DEST")]
            [MaxLength(50, ErrorMessage = "DEST cannot be longer than 50 characters")]
    		public string  DEST { get; set; }
    
    		    
    		/// <summary>
    		/// ETD
    		/// </summary>        
    	//    [DisplayName("ETD")]
    		public Nullable<System.DateTime>  ETD { get; set; }
    
    		    
    		/// <summary>
    		/// ETA
    		/// </summary>        
    	//    [DisplayName("ETA")]
    		public Nullable<System.DateTime>  ETA { get; set; }
    
    		    
    		/// <summary>
    		/// Carrier
    		/// </summary>        
    	//    [DisplayName("Carrier")]
            [MaxLength(10, ErrorMessage = "Carrier cannot be longer than 10 characters")]
    		public string  Carrier { get; set; }
    
    		    
    		/// <summary>
    		/// BKAgent
    		/// </summary>        
    	//    [DisplayName("BKAgent")]
            [MaxLength(10, ErrorMessage = "BKAgent cannot be longer than 10 characters")]
    		public string  BKAgent { get; set; }
    
    		    
    		/// <summary>
    		/// DESTAgent
    		/// </summary>        
    	//    [DisplayName("DESTAgent")]
            [MaxLength(10, ErrorMessage = "DESTAgent cannot be longer than 10 characters")]
    		public string  DESTAgent { get; set; }
    
    		    
    		/// <summary>
    		/// Reference No
    		/// </summary>        
    	//    [DisplayName("Reference No")]
            [MaxLength(20, ErrorMessage = "Reference No cannot be longer than 20 characters")]
    		public string  ReferenceNo { get; set; }
    
    		    
    		/// <summary>
    		/// VSLVOY
    		/// </summary>        
    	//    [DisplayName("VSLVOY")]
            [MaxLength(100, ErrorMessage = "VSLVOY cannot be longer than 100 characters")]
    		public string  VSLVOY { get; set; }
    
    		    
    		/// <summary>
    		/// OPType1
    		/// </summary>        
    	//    [DisplayName("OPType1")]
            [MaxLength(10, ErrorMessage = "OPType1 cannot be longer than 10 characters")]
    		public string  OPType1 { get; set; }
    
    		    
    		/// <summary>
    		/// OPType2
    		/// </summary>        
    	//    [DisplayName("OPType2")]
            [MaxLength(10, ErrorMessage = "OPType2 cannot be longer than 10 characters")]
    		public string  OPType2 { get; set; }
    
    		    
    		/// <summary>
    		/// OPType Detail
    		/// </summary>        
    	//    [DisplayName("OPType Detail")]
            [MaxLength(50, ErrorMessage = "OPType Detail cannot be longer than 50 characters")]
    		public string  OPTypeDetail { get; set; }
    
    		    
    		/// <summary>
    		/// Freight Payable MBL
    		/// </summary>        
    	//    [DisplayName("Freight Payable MBL")]
            [MaxLength(2, ErrorMessage = "Freight Payable MBL cannot be longer than 2 characters")]
    		public string  FreightPayableMBL { get; set; }
    
    		    
    		/// <summary>
    		/// Freight Payable HBL
    		/// </summary>        
    	//    [DisplayName("Freight Payable HBL")]
            [MaxLength(2, ErrorMessage = "Freight Payable HBL cannot be longer than 2 characters")]
    		public string  FreightPayableHBL { get; set; }
    
    		    
    		/// <summary>
    		/// Third Party Place
    		/// </summary>        
    	//    [DisplayName("Third Party Place")]
            [MaxLength(50, ErrorMessage = "Third Party Place cannot be longer than 50 characters")]
    		public string  ThirdPartyPlace { get; set; }
    
    		    
    		/// <summary>
    		/// Other
    		/// </summary>        
    	//    [DisplayName("Other")]
            [MaxLength(50, ErrorMessage = "Other cannot be longer than 50 characters")]
    		public string  Other { get; set; }
    
    		    
    		/// <summary>
    		/// BLRelease
    		/// </summary>        
    	//    [DisplayName("BLRelease")]
            [MaxLength(50, ErrorMessage = "BLRelease cannot be longer than 50 characters")]
    		public string  BLRelease { get; set; }
    
    		    
    		/// <summary>
    		/// ROType
    		/// </summary>        
    	//    [DisplayName("ROType")]
            [MaxLength(2, ErrorMessage = "ROType cannot be longer than 2 characters")]
    		public string  ROType { get; set; }
    
    		    
    		/// <summary>
    		/// Charge FRT
    		/// </summary>        
    	//    [DisplayName("Charge FRT")]
            [MaxLength(50, ErrorMessage = "Charge FRT cannot be longer than 50 characters")]
    		public string  ChargeFRT { get; set; }
    
    		    
    		/// <summary>
    		/// Charge FRT0
    		/// </summary>        
    	//    [DisplayName("Charge FRT0")]
            [MaxLength(50, ErrorMessage = "Charge FRT0 cannot be longer than 50 characters")]
    		public string  ChargeFRT0 { get; set; }
    
    		    
    		/// <summary>
    		/// Charge FRT1
    		/// </summary>        
    	//    [DisplayName("Charge FRT1")]
            [MaxLength(50, ErrorMessage = "Charge FRT1 cannot be longer than 50 characters")]
    		public string  ChargeFRT1 { get; set; }
    
    		    
    		/// <summary>
    		/// Charge FRT2
    		/// </summary>        
    	//    [DisplayName("Charge FRT2")]
            [MaxLength(50, ErrorMessage = "Charge FRT2 cannot be longer than 50 characters")]
    		public string  ChargeFRT2 { get; set; }
    
    		    
    		/// <summary>
    		/// Charge BAFEBS
    		/// </summary>        
    	//    [DisplayName("Charge BAFEBS")]
            [MaxLength(50, ErrorMessage = "Charge BAFEBS cannot be longer than 50 characters")]
    		public string  ChargeBAFEBS { get; set; }
    
    		    
    		/// <summary>
    		/// Charge BAFEBS0
    		/// </summary>        
    	//    [DisplayName("Charge BAFEBS0")]
            [MaxLength(50, ErrorMessage = "Charge BAFEBS0 cannot be longer than 50 characters")]
    		public string  ChargeBAFEBS0 { get; set; }
    
    		    
    		/// <summary>
    		/// Charge BAFEBS1
    		/// </summary>        
    	//    [DisplayName("Charge BAFEBS1")]
            [MaxLength(50, ErrorMessage = "Charge BAFEBS1 cannot be longer than 50 characters")]
    		public string  ChargeBAFEBS1 { get; set; }
    
    		    
    		/// <summary>
    		/// Charge BAFEBS2
    		/// </summary>        
    	//    [DisplayName("Charge BAFEBS2")]
            [MaxLength(50, ErrorMessage = "Charge BAFEBS2 cannot be longer than 50 characters")]
    		public string  ChargeBAFEBS2 { get; set; }
    
    		    
    		/// <summary>
    		/// Charge Select1
    		/// </summary>        
    	//    [DisplayName("Charge Select1")]
            [MaxLength(50, ErrorMessage = "Charge Select1 cannot be longer than 50 characters")]
    		public string  ChargeSelect1 { get; set; }
    
    		    
    		/// <summary>
    		/// Charge Select2
    		/// </summary>        
    	//    [DisplayName("Charge Select2")]
            [MaxLength(50, ErrorMessage = "Charge Select2 cannot be longer than 50 characters")]
    		public string  ChargeSelect2 { get; set; }
    
    		    
    		/// <summary>
    		/// Charge CIC
    		/// </summary>        
    	//    [DisplayName("Charge CIC")]
            [Required(ErrorMessage = "Charge CIC is required")]
            [MaxLength(50, ErrorMessage = "Charge CIC cannot be longer than 50 characters")]
    		public string  ChargeCIC { get; set; }
    
    		    
    		/// <summary>
    		/// Charge CIC0
    		/// </summary>        
    	//    [DisplayName("Charge CIC0")]
            [MaxLength(50, ErrorMessage = "Charge CIC0 cannot be longer than 50 characters")]
    		public string  ChargeCIC0 { get; set; }
    
    		    
    		/// <summary>
    		/// Charge CIC1
    		/// </summary>        
    	//    [DisplayName("Charge CIC1")]
            [MaxLength(50, ErrorMessage = "Charge CIC1 cannot be longer than 50 characters")]
    		public string  ChargeCIC1 { get; set; }
    
    		    
    		/// <summary>
    		/// Charge CIC2
    		/// </summary>        
    	//    [DisplayName("Charge CIC2")]
            [MaxLength(50, ErrorMessage = "Charge CIC2 cannot be longer than 50 characters")]
    		public string  ChargeCIC2 { get; set; }
    
    		    
    		/// <summary>
    		/// Charge AMSACI
    		/// </summary>        
    	//    [DisplayName("Charge AMSACI")]
            [MaxLength(50, ErrorMessage = "Charge AMSACI cannot be longer than 50 characters")]
    		public string  ChargeAMSACI { get; set; }
    
    		    
    		/// <summary>
    		/// Charge AMSACI0
    		/// </summary>        
    	//    [DisplayName("Charge AMSACI0")]
            [MaxLength(50, ErrorMessage = "Charge AMSACI0 cannot be longer than 50 characters")]
    		public string  ChargeAMSACI0 { get; set; }
    
    		    
    		/// <summary>
    		/// Charge AMSACI1
    		/// </summary>        
    	//    [DisplayName("Charge AMSACI1")]
            [MaxLength(50, ErrorMessage = "Charge AMSACI1 cannot be longer than 50 characters")]
    		public string  ChargeAMSACI1 { get; set; }
    
    		    
    		/// <summary>
    		/// Charge AMSACI2
    		/// </summary>        
    	//    [DisplayName("Charge AMSACI2")]
            [MaxLength(50, ErrorMessage = "Charge AMSACI2 cannot be longer than 50 characters")]
    		public string  ChargeAMSACI2 { get; set; }
    
    		    
    		/// <summary>
    		/// Charge LOC
    		/// </summary>        
    	//    [DisplayName("Charge LOC")]
            [MaxLength(50, ErrorMessage = "Charge LOC cannot be longer than 50 characters")]
    		public string  ChargeLOC { get; set; }
    
    		    
    		/// <summary>
    		/// Charge LOC0
    		/// </summary>        
    	//    [DisplayName("Charge LOC0")]
            [MaxLength(50, ErrorMessage = "Charge LOC0 cannot be longer than 50 characters")]
    		public string  ChargeLOC0 { get; set; }
    
    		    
    		/// <summary>
    		/// Charge LOC1
    		/// </summary>        
    	//    [DisplayName("Charge LOC1")]
            [MaxLength(50, ErrorMessage = "Charge LOC1 cannot be longer than 50 characters")]
    		public string  ChargeLOC1 { get; set; }
    
    		    
    		/// <summary>
    		/// Charge LOC2
    		/// </summary>        
    	//    [DisplayName("Charge LOC2")]
            [MaxLength(50, ErrorMessage = "Charge LOC2 cannot be longer than 50 characters")]
    		public string  ChargeLOC2 { get; set; }
    
    		    
    		/// <summary>
    		/// Charge BKF
    		/// </summary>        
    	//    [DisplayName("Charge BKF")]
            [MaxLength(50, ErrorMessage = "Charge BKF cannot be longer than 50 characters")]
    		public string  ChargeBKF { get; set; }
    
    		    
    		/// <summary>
    		/// Charge BKF0
    		/// </summary>        
    	//    [DisplayName("Charge BKF0")]
            [MaxLength(50, ErrorMessage = "Charge BKF0 cannot be longer than 50 characters")]
    		public string  ChargeBKF0 { get; set; }
    
    		    
    		/// <summary>
    		/// Charge BKF1
    		/// </summary>        
    	//    [DisplayName("Charge BKF1")]
            [MaxLength(50, ErrorMessage = "Charge BKF1 cannot be longer than 50 characters")]
    		public string  ChargeBKF1 { get; set; }
    
    		    
    		/// <summary>
    		/// Charge BKF2
    		/// </summary>        
    	//    [DisplayName("Charge BKF2")]
            [MaxLength(50, ErrorMessage = "Charge BKF2 cannot be longer than 50 characters")]
    		public string  ChargeBKF2 { get; set; }
    
    		    
    		/// <summary>
    		/// Charge THC
    		/// </summary>        
    	//    [DisplayName("Charge THC")]
            [MaxLength(50, ErrorMessage = "Charge THC cannot be longer than 50 characters")]
    		public string  ChargeTHC { get; set; }
    
    		    
    		/// <summary>
    		/// Charge THC0
    		/// </summary>        
    	//    [DisplayName("Charge THC0")]
            [MaxLength(50, ErrorMessage = "Charge THC0 cannot be longer than 50 characters")]
    		public string  ChargeTHC0 { get; set; }
    
    		    
    		/// <summary>
    		/// Charge THC1
    		/// </summary>        
    	//    [DisplayName("Charge THC1")]
            [MaxLength(50, ErrorMessage = "Charge THC1 cannot be longer than 50 characters")]
    		public string  ChargeTHC1 { get; set; }
    
    		    
    		/// <summary>
    		/// Charge THC2
    		/// </summary>        
    	//    [DisplayName("Charge THC2")]
            [MaxLength(50, ErrorMessage = "Charge THC2 cannot be longer than 50 characters")]
    		public string  ChargeTHC2 { get; set; }
    
    		    
    		/// <summary>
    		/// Charge CUS
    		/// </summary>        
    	//    [DisplayName("Charge CUS")]
            [MaxLength(50, ErrorMessage = "Charge CUS cannot be longer than 50 characters")]
    		public string  ChargeCUS { get; set; }
    
    		    
    		/// <summary>
    		/// Charge CUS0
    		/// </summary>        
    	//    [DisplayName("Charge CUS0")]
            [MaxLength(50, ErrorMessage = "Charge CUS0 cannot be longer than 50 characters")]
    		public string  ChargeCUS0 { get; set; }
    
    		    
    		/// <summary>
    		/// Charge CUS1
    		/// </summary>        
    	//    [DisplayName("Charge CUS1")]
            [MaxLength(50, ErrorMessage = "Charge CUS1 cannot be longer than 50 characters")]
    		public string  ChargeCUS1 { get; set; }
    
    		    
    		/// <summary>
    		/// Charge CUS2
    		/// </summary>        
    	//    [DisplayName("Charge CUS2")]
            [MaxLength(50, ErrorMessage = "Charge CUS2 cannot be longer than 50 characters")]
    		public string  ChargeCUS2 { get; set; }
    
    		    
    		/// <summary>
    		/// Charge INS
    		/// </summary>        
    	//    [DisplayName("Charge INS")]
            [MaxLength(50, ErrorMessage = "Charge INS cannot be longer than 50 characters")]
    		public string  ChargeINS { get; set; }
    
    		    
    		/// <summary>
    		/// Charge INS0
    		/// </summary>        
    	//    [DisplayName("Charge INS0")]
            [MaxLength(50, ErrorMessage = "Charge INS0 cannot be longer than 50 characters")]
    		public string  ChargeINS0 { get; set; }
    
    		    
    		/// <summary>
    		/// Charge INS1
    		/// </summary>        
    	//    [DisplayName("Charge INS1")]
            [MaxLength(50, ErrorMessage = "Charge INS1 cannot be longer than 50 characters")]
    		public string  ChargeINS1 { get; set; }
    
    		    
    		/// <summary>
    		/// Charge INS2
    		/// </summary>        
    	//    [DisplayName("Charge INS2")]
            [MaxLength(50, ErrorMessage = "Charge INS2 cannot be longer than 50 characters")]
    		public string  ChargeINS2 { get; set; }
    
    		    
    		/// <summary>
    		/// Charge CMI
    		/// </summary>        
    	//    [DisplayName("Charge CMI")]
            [MaxLength(50, ErrorMessage = "Charge CMI cannot be longer than 50 characters")]
    		public string  ChargeCMI { get; set; }
    
    		    
    		/// <summary>
    		/// Charge CMI0
    		/// </summary>        
    	//    [DisplayName("Charge CMI0")]
            [MaxLength(50, ErrorMessage = "Charge CMI0 cannot be longer than 50 characters")]
    		public string  ChargeCMI0 { get; set; }
    
    		    
    		/// <summary>
    		/// Charge CMI1
    		/// </summary>        
    	//    [DisplayName("Charge CMI1")]
            [MaxLength(50, ErrorMessage = "Charge CMI1 cannot be longer than 50 characters")]
    		public string  ChargeCMI1 { get; set; }
    
    		    
    		/// <summary>
    		/// Charge CMI2
    		/// </summary>        
    	//    [DisplayName("Charge CMI2")]
            [MaxLength(50, ErrorMessage = "Charge CMI2 cannot be longer than 50 characters")]
    		public string  ChargeCMI2 { get; set; }
    
    		    
    		/// <summary>
    		/// Charge CTG
    		/// </summary>        
    	//    [DisplayName("Charge CTG")]
            [MaxLength(50, ErrorMessage = "Charge CTG cannot be longer than 50 characters")]
    		public string  ChargeCTG { get; set; }
    
    		    
    		/// <summary>
    		/// Charge CTG0
    		/// </summary>        
    	//    [DisplayName("Charge CTG0")]
            [MaxLength(50, ErrorMessage = "Charge CTG0 cannot be longer than 50 characters")]
    		public string  ChargeCTG0 { get; set; }
    
    		    
    		/// <summary>
    		/// Charge CTG1
    		/// </summary>        
    	//    [DisplayName("Charge CTG1")]
            [MaxLength(50, ErrorMessage = "Charge CTG1 cannot be longer than 50 characters")]
    		public string  ChargeCTG1 { get; set; }
    
    		    
    		/// <summary>
    		/// Charge CTG2
    		/// </summary>        
    	//    [DisplayName("Charge CTG2")]
            [MaxLength(50, ErrorMessage = "Charge CTG2 cannot be longer than 50 characters")]
    		public string  ChargeCTG2 { get; set; }
    
    		    
    		/// <summary>
    		/// Charge UUF
    		/// </summary>        
    	//    [DisplayName("Charge UUF")]
            [MaxLength(50, ErrorMessage = "Charge UUF cannot be longer than 50 characters")]
    		public string  ChargeUUF { get; set; }
    
    		    
    		/// <summary>
    		/// Charge UUF0
    		/// </summary>        
    	//    [DisplayName("Charge UUF0")]
            [MaxLength(50, ErrorMessage = "Charge UUF0 cannot be longer than 50 characters")]
    		public string  ChargeUUF0 { get; set; }
    
    		    
    		/// <summary>
    		/// Charge UUF1
    		/// </summary>        
    	//    [DisplayName("Charge UUF1")]
            [MaxLength(50, ErrorMessage = "Charge UUF1 cannot be longer than 50 characters")]
    		public string  ChargeUUF1 { get; set; }
    
    		    
    		/// <summary>
    		/// Charge UUF2
    		/// </summary>        
    	//    [DisplayName("Charge UUF2")]
            [MaxLength(50, ErrorMessage = "Charge UUF2 cannot be longer than 50 characters")]
    		public string  ChargeUUF2 { get; set; }
    
    		    
    		/// <summary>
    		/// Charge FUM
    		/// </summary>        
    	//    [DisplayName("Charge FUM")]
            [MaxLength(50, ErrorMessage = "Charge FUM cannot be longer than 50 characters")]
    		public string  ChargeFUM { get; set; }
    
    		    
    		/// <summary>
    		/// Charge FUM0
    		/// </summary>        
    	//    [DisplayName("Charge FUM0")]
            [MaxLength(50, ErrorMessage = "Charge FUM0 cannot be longer than 50 characters")]
    		public string  ChargeFUM0 { get; set; }
    
    		    
    		/// <summary>
    		/// Charge FUM1
    		/// </summary>        
    	//    [DisplayName("Charge FUM1")]
            [MaxLength(50, ErrorMessage = "Charge FUM1 cannot be longer than 50 characters")]
    		public string  ChargeFUM1 { get; set; }
    
    		    
    		/// <summary>
    		/// Charge FUM2
    		/// </summary>        
    	//    [DisplayName("Charge FUM2")]
            [MaxLength(50, ErrorMessage = "Charge FUM2 cannot be longer than 50 characters")]
    		public string  ChargeFUM2 { get; set; }
    
    		    
    		/// <summary>
    		/// Charge DOC
    		/// </summary>        
    	//    [DisplayName("Charge DOC")]
            [MaxLength(50, ErrorMessage = "Charge DOC cannot be longer than 50 characters")]
    		public string  ChargeDOC { get; set; }
    
    		    
    		/// <summary>
    		/// Charge DOC0
    		/// </summary>        
    	//    [DisplayName("Charge DOC0")]
            [MaxLength(50, ErrorMessage = "Charge DOC0 cannot be longer than 50 characters")]
    		public string  ChargeDOC0 { get; set; }
    
    		    
    		/// <summary>
    		/// Charge DOC1
    		/// </summary>        
    	//    [DisplayName("Charge DOC1")]
            [MaxLength(50, ErrorMessage = "Charge DOC1 cannot be longer than 50 characters")]
    		public string  ChargeDOC1 { get; set; }
    
    		    
    		/// <summary>
    		/// Charge DOC2
    		/// </summary>        
    	//    [DisplayName("Charge DOC2")]
            [MaxLength(50, ErrorMessage = "Charge DOC2 cannot be longer than 50 characters")]
    		public string  ChargeDOC2 { get; set; }
    
    		    
    		/// <summary>
    		/// Charge COU
    		/// </summary>        
    	//    [DisplayName("Charge COU")]
            [MaxLength(50, ErrorMessage = "Charge COU cannot be longer than 50 characters")]
    		public string  ChargeCOU { get; set; }
    
    		    
    		/// <summary>
    		/// Charge COU0
    		/// </summary>        
    	//    [DisplayName("Charge COU0")]
            [MaxLength(50, ErrorMessage = "Charge COU0 cannot be longer than 50 characters")]
    		public string  ChargeCOU0 { get; set; }
    
    		    
    		/// <summary>
    		/// Charge COU1
    		/// </summary>        
    	//    [DisplayName("Charge COU1")]
            [MaxLength(50, ErrorMessage = "Charge COU1 cannot be longer than 50 characters")]
    		public string  ChargeCOU1 { get; set; }
    
    		    
    		/// <summary>
    		/// Charge COU2
    		/// </summary>        
    	//    [DisplayName("Charge COU2")]
            [MaxLength(50, ErrorMessage = "Charge COU2 cannot be longer than 50 characters")]
    		public string  ChargeCOU2 { get; set; }
    
    		    
    		/// <summary>
    		/// Charge DDUDDP
    		/// </summary>        
    	//    [DisplayName("Charge DDUDDP")]
            [MaxLength(50, ErrorMessage = "Charge DDUDDP cannot be longer than 50 characters")]
    		public string  ChargeDDUDDP { get; set; }
    
    		    
    		/// <summary>
    		/// Charge DDUDDP0
    		/// </summary>        
    	//    [DisplayName("Charge DDUDDP0")]
            [MaxLength(50, ErrorMessage = "Charge DDUDDP0 cannot be longer than 50 characters")]
    		public string  ChargeDDUDDP0 { get; set; }
    
    		    
    		/// <summary>
    		/// Charge DDUDDP1
    		/// </summary>        
    	//    [DisplayName("Charge DDUDDP1")]
            [MaxLength(50, ErrorMessage = "Charge DDUDDP1 cannot be longer than 50 characters")]
    		public string  ChargeDDUDDP1 { get; set; }
    
    		    
    		/// <summary>
    		/// Charge DDUDDP2
    		/// </summary>        
    	//    [DisplayName("Charge DDUDDP2")]
            [MaxLength(50, ErrorMessage = "Charge DDUDDP2 cannot be longer than 50 characters")]
    		public string  ChargeDDUDDP2 { get; set; }
    
    		    
    		/// <summary>
    		/// Charge Refund
    		/// </summary>        
    	//    [DisplayName("Charge Refund")]
            [MaxLength(50, ErrorMessage = "Charge Refund cannot be longer than 50 characters")]
    		public string  ChargeRefund { get; set; }
    
    		    
    		/// <summary>
    		/// Charge Refund0
    		/// </summary>        
    	//    [DisplayName("Charge Refund0")]
            [MaxLength(50, ErrorMessage = "Charge Refund0 cannot be longer than 50 characters")]
    		public string  ChargeRefund0 { get; set; }
    
    		    
    		/// <summary>
    		/// Charge Refund1
    		/// </summary>        
    	//    [DisplayName("Charge Refund1")]
            [MaxLength(50, ErrorMessage = "Charge Refund1 cannot be longer than 50 characters")]
    		public string  ChargeRefund1 { get; set; }
    
    		    
    		/// <summary>
    		/// Charge Refund2
    		/// </summary>        
    	//    [DisplayName("Charge Refund2")]
            [MaxLength(50, ErrorMessage = "Charge Refund2 cannot be longer than 50 characters")]
    		public string  ChargeRefund2 { get; set; }
    
    		    
    		/// <summary>
    		/// Charge BINS
    		/// </summary>        
    	//    [DisplayName("Charge BINS")]
            [MaxLength(50, ErrorMessage = "Charge BINS cannot be longer than 50 characters")]
    		public string  ChargeBINS { get; set; }
    
    		    
    		/// <summary>
    		/// Charge BINS0
    		/// </summary>        
    	//    [DisplayName("Charge BINS0")]
            [MaxLength(50, ErrorMessage = "Charge BINS0 cannot be longer than 50 characters")]
    		public string  ChargeBINS0 { get; set; }
    
    		    
    		/// <summary>
    		/// Charge BINS1
    		/// </summary>        
    	//    [DisplayName("Charge BINS1")]
            [MaxLength(50, ErrorMessage = "Charge BINS1 cannot be longer than 50 characters")]
    		public string  ChargeBINS1 { get; set; }
    
    		    
    		/// <summary>
    		/// Charge BINS2
    		/// </summary>        
    	//    [DisplayName("Charge BINS2")]
            [MaxLength(50, ErrorMessage = "Charge BINS2 cannot be longer than 50 characters")]
    		public string  ChargeBINS2 { get; set; }
    
    		    
    		/// <summary>
    		/// Charge ISF
    		/// </summary>        
    	//    [DisplayName("Charge ISF")]
            [MaxLength(50, ErrorMessage = "Charge ISF cannot be longer than 50 characters")]
    		public string  ChargeISF { get; set; }
    
    		    
    		/// <summary>
    		/// Charge ISF0
    		/// </summary>        
    	//    [DisplayName("Charge ISF0")]
            [MaxLength(50, ErrorMessage = "Charge ISF0 cannot be longer than 50 characters")]
    		public string  ChargeISF0 { get; set; }
    
    		    
    		/// <summary>
    		/// Charge ISF1
    		/// </summary>        
    	//    [DisplayName("Charge ISF1")]
            [MaxLength(50, ErrorMessage = "Charge ISF1 cannot be longer than 50 characters")]
    		public string  ChargeISF1 { get; set; }
    
    		    
    		/// <summary>
    		/// Charge ISF2
    		/// </summary>        
    	//    [DisplayName("Charge ISF2")]
            [MaxLength(50, ErrorMessage = "Charge ISF2 cannot be longer than 50 characters")]
    		public string  ChargeISF2 { get; set; }
    
    		    
    		/// <summary>
    		/// Charge HC
    		/// </summary>        
    	//    [DisplayName("Charge HC")]
            [MaxLength(50, ErrorMessage = "Charge HC cannot be longer than 50 characters")]
    		public string  ChargeHC { get; set; }
    
    		    
    		/// <summary>
    		/// Charge HC0
    		/// </summary>        
    	//    [DisplayName("Charge HC0")]
            [MaxLength(50, ErrorMessage = "Charge HC0 cannot be longer than 50 characters")]
    		public string  ChargeHC0 { get; set; }
    
    		    
    		/// <summary>
    		/// Charge HC1
    		/// </summary>        
    	//    [DisplayName("Charge HC1")]
            [MaxLength(50, ErrorMessage = "Charge HC1 cannot be longer than 50 characters")]
    		public string  ChargeHC1 { get; set; }
    
    		    
    		/// <summary>
    		/// Charge HC2
    		/// </summary>        
    	//    [DisplayName("Charge HC2")]
            [MaxLength(50, ErrorMessage = "Charge HC2 cannot be longer than 50 characters")]
    		public string  ChargeHC2 { get; set; }
    
    		    
    		/// <summary>
    		/// Charge Add1
    		/// </summary>        
    	//    [DisplayName("Charge Add1")]
            [MaxLength(50, ErrorMessage = "Charge Add1 cannot be longer than 50 characters")]
    		public string  ChargeAdd1 { get; set; }
    
    		    
    		/// <summary>
    		/// Charge Add10
    		/// </summary>        
    	//    [DisplayName("Charge Add10")]
            [MaxLength(50, ErrorMessage = "Charge Add10 cannot be longer than 50 characters")]
    		public string  ChargeAdd10 { get; set; }
    
    		    
    		/// <summary>
    		/// Charge Add11
    		/// </summary>        
    	//    [DisplayName("Charge Add11")]
            [MaxLength(50, ErrorMessage = "Charge Add11 cannot be longer than 50 characters")]
    		public string  ChargeAdd11 { get; set; }
    
    		    
    		/// <summary>
    		/// Charge Add12
    		/// </summary>        
    	//    [DisplayName("Charge Add12")]
            [MaxLength(50, ErrorMessage = "Charge Add12 cannot be longer than 50 characters")]
    		public string  ChargeAdd12 { get; set; }
    
    		    
    		/// <summary>
    		/// Charge Add13
    		/// </summary>        
    	//    [DisplayName("Charge Add13")]
            [MaxLength(50, ErrorMessage = "Charge Add13 cannot be longer than 50 characters")]
    		public string  ChargeAdd13 { get; set; }
    
    		    
    		/// <summary>
    		/// Charge Add2
    		/// </summary>        
    	//    [DisplayName("Charge Add2")]
            [MaxLength(50, ErrorMessage = "Charge Add2 cannot be longer than 50 characters")]
    		public string  ChargeAdd2 { get; set; }
    
    		    
    		/// <summary>
    		/// Charge Add20
    		/// </summary>        
    	//    [DisplayName("Charge Add20")]
            [MaxLength(50, ErrorMessage = "Charge Add20 cannot be longer than 50 characters")]
    		public string  ChargeAdd20 { get; set; }
    
    		    
    		/// <summary>
    		/// Charge Add21
    		/// </summary>        
    	//    [DisplayName("Charge Add21")]
            [MaxLength(50, ErrorMessage = "Charge Add21 cannot be longer than 50 characters")]
    		public string  ChargeAdd21 { get; set; }
    
    		    
    		/// <summary>
    		/// Charge Add22
    		/// </summary>        
    	//    [DisplayName("Charge Add22")]
            [MaxLength(50, ErrorMessage = "Charge Add22 cannot be longer than 50 characters")]
    		public string  ChargeAdd22 { get; set; }
    
    		    
    		/// <summary>
    		/// Charge Add23
    		/// </summary>        
    	//    [DisplayName("Charge Add23")]
            [MaxLength(50, ErrorMessage = "Charge Add23 cannot be longer than 50 characters")]
    		public string  ChargeAdd23 { get; set; }
    
    		    
    		/// <summary>
    		/// Charge Add3
    		/// </summary>        
    	//    [DisplayName("Charge Add3")]
            [MaxLength(50, ErrorMessage = "Charge Add3 cannot be longer than 50 characters")]
    		public string  ChargeAdd3 { get; set; }
    
    		    
    		/// <summary>
    		/// Charge Add30
    		/// </summary>        
    	//    [DisplayName("Charge Add30")]
            [MaxLength(50, ErrorMessage = "Charge Add30 cannot be longer than 50 characters")]
    		public string  ChargeAdd30 { get; set; }
    
    		    
    		/// <summary>
    		/// Charge Add31
    		/// </summary>        
    	//    [DisplayName("Charge Add31")]
            [MaxLength(50, ErrorMessage = "Charge Add31 cannot be longer than 50 characters")]
    		public string  ChargeAdd31 { get; set; }
    
    		    
    		/// <summary>
    		/// Charge Add32
    		/// </summary>        
    	//    [DisplayName("Charge Add32")]
            [MaxLength(50, ErrorMessage = "Charge Add32 cannot be longer than 50 characters")]
    		public string  ChargeAdd32 { get; set; }
    
    		    
    		/// <summary>
    		/// Charge Add33
    		/// </summary>        
    	//    [DisplayName("Charge Add33")]
            [MaxLength(50, ErrorMessage = "Charge Add33 cannot be longer than 50 characters")]
    		public string  ChargeAdd33 { get; set; }
    
    		    
    		/// <summary>
    		/// Charge Add Name1
    		/// </summary>        
    	//    [DisplayName("Charge Add Name1")]
            [MaxLength(50, ErrorMessage = "Charge Add Name1 cannot be longer than 50 characters")]
    		public string  ChargeAddName1 { get; set; }
    
    		    
    		/// <summary>
    		/// Charge Add Name11
    		/// </summary>        
    	//    [DisplayName("Charge Add Name11")]
            [MaxLength(50, ErrorMessage = "Charge Add Name11 cannot be longer than 50 characters")]
    		public string  ChargeAddName11 { get; set; }
    
    		    
    		/// <summary>
    		/// Charge Add Name2
    		/// </summary>        
    	//    [DisplayName("Charge Add Name2")]
            [MaxLength(50, ErrorMessage = "Charge Add Name2 cannot be longer than 50 characters")]
    		public string  ChargeAddName2 { get; set; }
    
    		    
    		/// <summary>
    		/// Charge Add Name21
    		/// </summary>        
    	//    [DisplayName("Charge Add Name21")]
            [MaxLength(50, ErrorMessage = "Charge Add Name21 cannot be longer than 50 characters")]
    		public string  ChargeAddName21 { get; set; }
    
    		    
    		/// <summary>
    		/// Charge Add Name3
    		/// </summary>        
    	//    [DisplayName("Charge Add Name3")]
            [MaxLength(50, ErrorMessage = "Charge Add Name3 cannot be longer than 50 characters")]
    		public string  ChargeAddName3 { get; set; }
    
    		    
    		/// <summary>
    		/// Charge Add Name31
    		/// </summary>        
    	//    [DisplayName("Charge Add Name31")]
            [MaxLength(50, ErrorMessage = "Charge Add Name31 cannot be longer than 50 characters")]
    		public string  ChargeAddName31 { get; set; }
    
    		    
    		/// <summary>
    		/// Local CHGPS
    		/// </summary>        
    	//    [DisplayName("Local CHGPS")]
            [MaxLength(50, ErrorMessage = "Local CHGPS cannot be longer than 50 characters")]
    		public string  LocalCHGPS { get; set; }
    
    		    
    		/// <summary>
    		/// Local CHGPS0
    		/// </summary>        
    	//    [DisplayName("Local CHGPS0")]
            [MaxLength(50, ErrorMessage = "Local CHGPS0 cannot be longer than 50 characters")]
    		public string  LocalCHGPS0 { get; set; }
    
    		    
    		/// <summary>
    		/// Local CHGPS1
    		/// </summary>        
    	//    [DisplayName("Local CHGPS1")]
            [MaxLength(50, ErrorMessage = "Local CHGPS1 cannot be longer than 50 characters")]
    		public string  LocalCHGPS1 { get; set; }
    
    		    
    		/// <summary>
    		/// Local CHGPS2
    		/// </summary>        
    	//    [DisplayName("Local CHGPS2")]
            [MaxLength(50, ErrorMessage = "Local CHGPS2 cannot be longer than 50 characters")]
    		public string  LocalCHGPS2 { get; set; }
    
    		    
    		/// <summary>
    		/// OFPS
    		/// </summary>        
    	//    [DisplayName("OFPS")]
            [MaxLength(50, ErrorMessage = "OFPS cannot be longer than 50 characters")]
    		public string  OFPS { get; set; }
    
    		    
    		/// <summary>
    		/// OFPS0
    		/// </summary>        
    	//    [DisplayName("OFPS0")]
            [MaxLength(50, ErrorMessage = "OFPS0 cannot be longer than 50 characters")]
    		public string  OFPS0 { get; set; }
    
    		    
    		/// <summary>
    		/// OFCRTOOver Sales
    		/// </summary>        
    	//    [DisplayName("OFCRTOOver Sales")]
            [MaxLength(50, ErrorMessage = "OFCRTOOver Sales cannot be longer than 50 characters")]
    		public string  OFCRTOOverSales { get; set; }
    
    		    
    		/// <summary>
    		/// OFCRTOOver Sales0
    		/// </summary>        
    	//    [DisplayName("OFCRTOOver Sales0")]
            [MaxLength(50, ErrorMessage = "OFCRTOOver Sales0 cannot be longer than 50 characters")]
    		public string  OFCRTOOverSales0 { get; set; }
    
    		    
    		/// <summary>
    		/// PS
    		/// </summary>        
    	//    [DisplayName("PS")]
            [MaxLength(50, ErrorMessage = "PS cannot be longer than 50 characters")]
    		public string  PS { get; set; }
    
    		    
    		/// <summary>
    		/// HC
    		/// </summary>        
    	//    [DisplayName("HC")]
            [MaxLength(50, ErrorMessage = "HC cannot be longer than 50 characters")]
    		public string  HC { get; set; }
    
    		    
    		/// <summary>
    		/// Debit To
    		/// </summary>        
    	//    [DisplayName("Debit To")]
            [MaxLength(50, ErrorMessage = "Debit To cannot be longer than 50 characters")]
    		public string  DebitTo { get; set; }
    
    		    
    		/// <summary>
    		/// Credit To
    		/// </summary>        
    	//    [DisplayName("Credit To")]
            [MaxLength(50, ErrorMessage = "Credit To cannot be longer than 50 characters")]
    		public string  CreditTo { get; set; }
    
    		    
    		/// <summary>
    		/// OP
    		/// </summary>        
    	//    [DisplayName("OP")]
            [MaxLength(50, ErrorMessage = "OP cannot be longer than 50 characters")]
    		public string  OP { get; set; }
    
    		    
    		/// <summary>
    		/// Sales
    		/// </summary>        
    	//    [DisplayName("Sales")]
            [MaxLength(50, ErrorMessage = "Sales cannot be longer than 50 characters")]
    		public string  Sales { get; set; }
    
    		    
    		/// <summary>
    		/// Approve By
    		/// </summary>        
    	//    [DisplayName("Approve By")]
            [MaxLength(50, ErrorMessage = "Approve By cannot be longer than 50 characters")]
    		public string  ApproveBy { get; set; }
    
    		    
    		/// <summary>
    		/// GLDBy
    		/// </summary>        
    	//    [DisplayName("GLDBy")]
            [MaxLength(50, ErrorMessage = "GLDBy cannot be longer than 50 characters")]
    		public string  GLDBy { get; set; }
    
    		    
    		/// <summary>
    		/// GP
    		/// </summary>        
    	//    [DisplayName("GP")]
            [MaxLength(50, ErrorMessage = "GP cannot be longer than 50 characters")]
    		public string  GP { get; set; }
    
    		    
    		/// <summary>
    		/// Created By
    		/// </summary>        
    	//    [DisplayName("Created By")]
            [MaxLength(10, ErrorMessage = "Created By cannot be longer than 10 characters")]
    		public string  CreatedBy { get; set; }
    
    		    
    		/// <summary>
    		/// Created Time
    		/// </summary>        
    	//    [DisplayName("Created Time")]
    		public Nullable<System.DateTime>  CreatedTime { get; set; }
    
    		    
    		/// <summary>
    		/// Updated By
    		/// </summary>        
    	//    [DisplayName("Updated By")]
            [MaxLength(10, ErrorMessage = "Updated By cannot be longer than 10 characters")]
    		public string  UpdatedBy { get; set; }
    
    		    
    		/// <summary>
    		/// Updated Time
    		/// </summary>        
    	//    [DisplayName("Updated Time")]
    		public Nullable<System.DateTime>  UpdatedTime { get; set; }
    
    		    
    		/// <summary>
    		/// Print Content
    		/// </summary>        
    	//    [DisplayName("Print Content")]
    		public string  PrintContent { get; set; }
    
    		    
    		/// <summary>
    		/// SHPRHQID
    		/// </summary>        
    	//    [DisplayName("SHPRHQID")]
    		public Nullable<int>  SHPRHQID { get; set; }
    
    		    
    		/// <summary>
    		/// Client Email
    		/// </summary>        
    	//    [DisplayName("Client Email")]
            [MaxLength(50, ErrorMessage = "Client Email cannot be longer than 50 characters")]
    		public string  ClientEmail { get; set; }
    
    		    
    		/// <summary>
    		/// Client Address
    		/// </summary>        
    	//    [DisplayName("Client Address")]
            [MaxLength(100, ErrorMessage = "Client Address cannot be longer than 100 characters")]
    		public string  ClientAddress { get; set; }
    
    		    
    		/// <summary>
    		/// Coloader
    		/// </summary>        
    	//    [DisplayName("Coloader")]
            [MaxLength(10, ErrorMessage = "Coloader cannot be longer than 10 characters")]
    		public string  Coloader { get; set; }
    
    		    
    		/// <summary>
    		/// OPType3
    		/// </summary>        
    	//    [DisplayName("OPType3")]
            [MaxLength(10, ErrorMessage = "OPType3 cannot be longer than 10 characters")]
    		public string  OPType3 { get; set; }
    
    		    
    		/// <summary>
    		/// OPType3 Detail
    		/// </summary>        
    	//    [DisplayName("OPType3 Detail")]
            [MaxLength(50, ErrorMessage = "OPType3 Detail cannot be longer than 50 characters")]
    		public string  OPType3Detail { get; set; }
    
    		    
    		/// <summary>
    		/// OPType3 Remark
    		/// </summary>        
    	//    [DisplayName("OPType3 Remark")]
            [MaxLength(50, ErrorMessage = "OPType3 Remark cannot be longer than 50 characters")]
    		public string  OPType3Remark { get; set; }
    
    		    
    		/// <summary>
    		/// Station ID
    		/// </summary>        
    	//    [DisplayName("Station ID")]
            [MaxLength(5, ErrorMessage = "Station ID cannot be longer than 5 characters")]
    		public string  StationID { get; set; }
    
    		    
    	}
    //}
    
}
