using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ValuePlus.ViewModel;



namespace ValuePlus.ViewModel.LocaleChainVP
{
    
    /// <summary>
    /// AIEntryDT class
    /// </summary>
    //[MetadataType(typeof(AIEntryDTViewModel))]
    //public  partial class AIEntryDT
    //{
    
    	/// <summary>
    	/// AIEntryDT Metadata class
    	/// </summary>
    	public   class AIEntryDTViewModel:ViewModelBase
    	{
    		    
    		/// <summary>
    		/// ID
    		/// </summary>        
    	//    [DisplayName("ID")]
            [Required(ErrorMessage = "ID is required")]
    		public int  ID { get; set; }
    
    		    
    		/// <summary>
    		/// HAWBID
    		/// </summary>        
    	//    [DisplayName("HAWBID")]
            [Required(ErrorMessage = "HAWBID is required")]
    		public int  HAWBID { get; set; }
    
    		    
    		/// <summary>
    		/// Entry ID
    		/// </summary>        
    	//    [DisplayName("Entry ID")]
            [Required(ErrorMessage = "Entry ID is required")]
    		public int  EntryID { get; set; }
    
    		    
    		/// <summary>
    		/// Entry PCS
    		/// </summary>        
    	//    [DisplayName("Entry PCS")]
    		public Nullable<int>  EntryPCS { get; set; }
    
    		    
    		/// <summary>
    		/// Entry WT
    		/// </summary>        
    	//    [DisplayName("Entry WT")]
    		public Nullable<double>  EntryWT { get; set; }
    
    		    
    		/// <summary>
    		/// Station ID
    		/// </summary>        
    	//    [DisplayName("Station ID")]
            [Required(ErrorMessage = "Station ID is required")]
            [MaxLength(10, ErrorMessage = "Station ID cannot be longer than 10 characters")]
    		public string  StationID { get; set; }
    
    		    
    		/// <summary>
    		/// Created By
    		/// </summary>        
    	//    [DisplayName("Created By")]
            [Required(ErrorMessage = "Created By is required")]
            [MaxLength(6, ErrorMessage = "Created By cannot be longer than 6 characters")]
    		public string  CreatedBy { get; set; }
    
    		    
    		/// <summary>
    		/// Created Date
    		/// </summary>        
    	//    [DisplayName("Created Date")]
            [Required(ErrorMessage = "Created Date is required")]
    		public System.DateTime  CreatedDate { get; set; }
    
    		    
    		/// <summary>
    		/// Updated By
    		/// </summary>        
    	//    [DisplayName("Updated By")]
            [Required(ErrorMessage = "Updated By is required")]
            [MaxLength(6, ErrorMessage = "Updated By cannot be longer than 6 characters")]
    		public string  UpdatedBy { get; set; }
    
    		    
    		/// <summary>
    		/// Updated Date
    		/// </summary>        
    	//    [DisplayName("Updated Date")]
            [Required(ErrorMessage = "Updated Date is required")]
    		public System.DateTime  UpdatedDate { get; set; }
    
    		    
    		/// <summary>
    		/// Damage
    		/// </summary>        
    	//    [DisplayName("Damage")]
    		public Nullable<int>  Damage { get; set; }
    
    		    
    		/// <summary>
    		/// Offload
    		/// </summary>        
    	//    [DisplayName("Offload")]
    		public Nullable<int>  Offload { get; set; }
    
    		    
    		/// <summary>
    		/// Shortage
    		/// </summary>        
    	//    [DisplayName("Shortage")]
    		public Nullable<int>  Shortage { get; set; }
    
    		    
    		/// <summary>
    		/// Supplus
    		/// </summary>        
    	//    [DisplayName("Supplus")]
    		public Nullable<int>  Supplus { get; set; }
    
    		    
    		/// <summary>
    		/// Damage WT
    		/// </summary>        
    	//    [DisplayName("Damage WT")]
    		public Nullable<double>  DamageWT { get; set; }
    
    		    
    		/// <summary>
    		/// Shortage WT
    		/// </summary>        
    	//    [DisplayName("Shortage WT")]
    		public Nullable<double>  ShortageWT { get; set; }
    
    		    
    		/// <summary>
    		/// Supplus WT
    		/// </summary>        
    	//    [DisplayName("Supplus WT")]
    		public Nullable<double>  SupplusWT { get; set; }
    
    		    
    		/// <summary>
    		/// Remark
    		/// </summary>        
    	//    [DisplayName("Remark")]
            [MaxLength(255, ErrorMessage = "Remark cannot be longer than 255 characters")]
    		public string  Remark { get; set; }
    
    		    
    		/// <summary>
    		/// Version
    		/// </summary>        
    	//    [DisplayName("Version")]
            [MaxLength(8, ErrorMessage = "Version cannot be longer than 8 characters")]
    		public byte[]  Version { get; set; }
    
    		    
    	}
    //}
    
}
