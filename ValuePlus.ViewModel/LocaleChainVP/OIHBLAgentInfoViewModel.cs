using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ValuePlus.ViewModel;



namespace ValuePlus.ViewModel.LocaleChainVP
{
    
    /// <summary>
    /// OIHBLAgentInfo class
    /// </summary>
    //[MetadataType(typeof(OIHBLAgentInfoViewModel))]
    //public  partial class OIHBLAgentInfo
    //{
    
    	/// <summary>
    	/// OIHBLAgentInfo Metadata class
    	/// </summary>
    	public   class OIHBLAgentInfoViewModel:ViewModelBase
    	{
    		    
    		/// <summary>
    		/// ID
    		/// </summary>        
    	//    [DisplayName("ID")]
            [Required(ErrorMessage = "ID is required")]
    		public int  ID { get; set; }
    
    		    
    		/// <summary>
    		/// Station ID
    		/// </summary>        
    	//    [DisplayName("Station ID")]
            [MaxLength(50, ErrorMessage = "Station ID cannot be longer than 50 characters")]
    		public string  StationID { get; set; }
    
    		    
    		/// <summary>
    		/// Agent ID
    		/// </summary>        
    	//    [DisplayName("Agent ID")]
    		public Nullable<int>  AgentID { get; set; }
    
    		    
    		/// <summary>
    		/// HBLID
    		/// </summary>        
    	//    [DisplayName("HBLID")]
            [Required(ErrorMessage = "HBLID is required")]
    		public int  HBLID { get; set; }
    
    		    
    		/// <summary>
    		/// Shipper ID
    		/// </summary>        
    	//    [DisplayName("Shipper ID")]
    		public Nullable<int>  ShipperID { get; set; }
    
    		    
    		/// <summary>
    		/// Shipper Map ID
    		/// </summary>        
    	//    [DisplayName("Shipper Map ID")]
    		public Nullable<int>  ShipperMapID { get; set; }
    
    		    
    		/// <summary>
    		/// CNEEID
    		/// </summary>        
    	//    [DisplayName("CNEEID")]
    		public Nullable<int>  CNEEID { get; set; }
    
    		    
    		/// <summary>
    		/// CNEEMap ID
    		/// </summary>        
    	//    [DisplayName("CNEEMap ID")]
    		public Nullable<int>  CNEEMapID { get; set; }
    
    		    
    		/// <summary>
    		/// NTFYID
    		/// </summary>        
    	//    [DisplayName("NTFYID")]
    		public Nullable<int>  NTFYID { get; set; }
    
    		    
    		/// <summary>
    		/// NTFYMap ID
    		/// </summary>        
    	//    [DisplayName("NTFYMap ID")]
    		public Nullable<int>  NTFYMapID { get; set; }
    
    		    
    		/// <summary>
    		/// Created By
    		/// </summary>        
    	//    [DisplayName("Created By")]
            [MaxLength(50, ErrorMessage = "Created By cannot be longer than 50 characters")]
    		public string  CreatedBy { get; set; }
    
    		    
    		/// <summary>
    		/// Created Date
    		/// </summary>        
    	//    [DisplayName("Created Date")]
            [Required(ErrorMessage = "Created Date is required")]
    		public System.DateTime  CreatedDate { get; set; }
    
    		    
    		/// <summary>
    		/// Updated By
    		/// </summary>        
    	//    [DisplayName("Updated By")]
            [MaxLength(50, ErrorMessage = "Updated By cannot be longer than 50 characters")]
    		public string  UpdatedBy { get; set; }
    
    		    
    		/// <summary>
    		/// Updated Date
    		/// </summary>        
    	//    [DisplayName("Updated Date")]
    		public Nullable<System.DateTime>  UpdatedDate { get; set; }
    
    		    
    	}
    //}
    
}
