using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ValuePlus.ViewModel;



namespace ValuePlus.ViewModel.LocaleChainVP
{
    
    /// <summary>
    /// AEBarCode class
    /// </summary>
    //[MetadataType(typeof(AEBarCodeViewModel))]
    //public  partial class AEBarCode
    //{
    
    	/// <summary>
    	/// AEBarCode Metadata class
    	/// </summary>
    	public   class AEBarCodeViewModel:ViewModelBase
    	{
    		    
    		/// <summary>
    		/// ID
    		/// </summary>        
    	//    [DisplayName("ID")]
            [Required(ErrorMessage = "ID is required")]
    		public int  ID { get; set; }
    
    		    
    		/// <summary>
    		/// MAWBID
    		/// </summary>        
    	//    [DisplayName("MAWBID")]
            [Required(ErrorMessage = "MAWBID is required")]
    		public int  MAWBID { get; set; }
    
    		    
    		/// <summary>
    		/// HAWBID
    		/// </summary>        
    	//    [DisplayName("HAWBID")]
            [Required(ErrorMessage = "HAWBID is required")]
    		public int  HAWBID { get; set; }
    
    		    
    		/// <summary>
    		/// Need Sequence
    		/// </summary>        
    	//    [DisplayName("Need Sequence")]
    		public Nullable<bool>  NeedSequence { get; set; }
    
    		    
    		/// <summary>
    		/// MAWBPCS
    		/// </summary>        
    	//    [DisplayName("MAWBPCS")]
    		public Nullable<int>  MAWBPCS { get; set; }
    
    		    
    		/// <summary>
    		/// HAWBPCS
    		/// </summary>        
    	//    [DisplayName("HAWBPCS")]
    		public Nullable<int>  HAWBPCS { get; set; }
    
    		    
    		/// <summary>
    		/// Start Number
    		/// </summary>        
    	//    [DisplayName("Start Number")]
    		public Nullable<int>  StartNumber { get; set; }
    
    		    
    		/// <summary>
    		/// Print Number
    		/// </summary>        
    	//    [DisplayName("Print Number")]
    		public Nullable<int>  PrintNumber { get; set; }
    
    		    
    		/// <summary>
    		/// Created By
    		/// </summary>        
    	//    [DisplayName("Created By")]
            [MaxLength(6, ErrorMessage = "Created By cannot be longer than 6 characters")]
    		public string  CreatedBy { get; set; }
    
    		    
    		/// <summary>
    		/// Created Date
    		/// </summary>        
    	//    [DisplayName("Created Date")]
    		public Nullable<System.DateTime>  CreatedDate { get; set; }
    
    		    
    		/// <summary>
    		/// Updated By
    		/// </summary>        
    	//    [DisplayName("Updated By")]
            [MaxLength(6, ErrorMessage = "Updated By cannot be longer than 6 characters")]
    		public string  UpdatedBy { get; set; }
    
    		    
    		/// <summary>
    		/// Updated Date
    		/// </summary>        
    	//    [DisplayName("Updated Date")]
    		public Nullable<System.DateTime>  UpdatedDate { get; set; }
    
    		    
    	}
    //}
    
}
