using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ValuePlus.ViewModel;



namespace ValuePlus.ViewModel.LocaleChainVP
{
    
    /// <summary>
    /// AEBookConsoleStation class
    /// </summary>
    //[MetadataType(typeof(AEBookConsoleStationViewModel))]
    //public  partial class AEBookConsoleStation
    //{
    
    	/// <summary>
    	/// AEBookConsoleStation Metadata class
    	/// </summary>
    	public   class AEBookConsoleStationViewModel:ViewModelBase
    	{
    		    
    		/// <summary>
    		/// ID
    		/// </summary>        
    	//    [DisplayName("ID")]
            [Required(ErrorMessage = "ID is required")]
    		public int  ID { get; set; }
    
    		    
    		/// <summary>
    		/// Console Type
    		/// </summary>        
    	//    [DisplayName("Console Type")]
            [Required(ErrorMessage = "Console Type is required")]
            [MaxLength(10, ErrorMessage = "Console Type cannot be longer than 10 characters")]
    		public string  ConsoleType { get; set; }
    
    		    
    		/// <summary>
    		/// Console Station ID
    		/// </summary>        
    	//    [DisplayName("Console Station ID")]
            [Required(ErrorMessage = "Console Station ID is required")]
            [MaxLength(50, ErrorMessage = "Console Station ID cannot be longer than 50 characters")]
    		public string  ConsoleStationID { get; set; }
    
    		    
    		/// <summary>
    		/// Status
    		/// </summary>        
    	//    [DisplayName("Status")]
            [MaxLength(50, ErrorMessage = "Status cannot be longer than 50 characters")]
    		public string  Status { get; set; }
    
    		    
    		/// <summary>
    		/// Created By
    		/// </summary>        
    	//    [DisplayName("Created By")]
            [MaxLength(50, ErrorMessage = "Created By cannot be longer than 50 characters")]
    		public string  CreatedBy { get; set; }
    
    		    
    		/// <summary>
    		/// Created Date
    		/// </summary>        
    	//    [DisplayName("Created Date")]
    		public Nullable<System.DateTime>  CreatedDate { get; set; }
    
    		    
    		/// <summary>
    		/// Updated By
    		/// </summary>        
    	//    [DisplayName("Updated By")]
            [MaxLength(50, ErrorMessage = "Updated By cannot be longer than 50 characters")]
    		public string  UpdatedBy { get; set; }
    
    		    
    		/// <summary>
    		/// Updated Date
    		/// </summary>        
    	//    [DisplayName("Updated Date")]
    		public Nullable<System.DateTime>  UpdatedDate { get; set; }
    
    		    
    	}
    //}
    
}
