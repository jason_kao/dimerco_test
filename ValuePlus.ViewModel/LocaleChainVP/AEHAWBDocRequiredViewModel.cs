using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ValuePlus.ViewModel;



namespace ValuePlus.ViewModel.LocaleChainVP
{
    
    /// <summary>
    /// AEHAWBDocRequired class
    /// </summary>
    //[MetadataType(typeof(AEHAWBDocRequiredViewModel))]
    //public  partial class AEHAWBDocRequired
    //{
    
    	/// <summary>
    	/// AEHAWBDocRequired Metadata class
    	/// </summary>
    	public   class AEHAWBDocRequiredViewModel:ViewModelBase
    	{
    		    
    		/// <summary>
    		/// ID
    		/// </summary>        
    	//    [DisplayName("ID")]
            [Required(ErrorMessage = "ID is required")]
    		public int  ID { get; set; }
    
    		    
    		/// <summary>
    		/// Pickup ID
    		/// </summary>        
    	//    [DisplayName("Pickup ID")]
            [Required(ErrorMessage = "Pickup ID is required")]
    		public int  PickupID { get; set; }
    
    		    
    		/// <summary>
    		/// Doc ID
    		/// </summary>        
    	//    [DisplayName("Doc ID")]
    		public Nullable<int>  DocID { get; set; }
    
    		    
    		/// <summary>
    		/// Doc Name
    		/// </summary>        
    	//    [DisplayName("Doc Name")]
            [MaxLength(255, ErrorMessage = "Doc Name cannot be longer than 255 characters")]
    		public string  DocName { get; set; }
    
    		    
    		/// <summary>
    		/// Confirmed
    		/// </summary>        
    	//    [DisplayName("Confirmed")]
            [Required(ErrorMessage = "Confirmed is required")]
    		public bool  Confirmed { get; set; }
    
    		    
    		/// <summary>
    		/// Station ID
    		/// </summary>        
    	//    [DisplayName("Station ID")]
            [Required(ErrorMessage = "Station ID is required")]
            [MaxLength(10, ErrorMessage = "Station ID cannot be longer than 10 characters")]
    		public string  StationID { get; set; }
    
    		    
    		/// <summary>
    		/// Created By
    		/// </summary>        
    	//    [DisplayName("Created By")]
            [Required(ErrorMessage = "Created By is required")]
            [MaxLength(6, ErrorMessage = "Created By cannot be longer than 6 characters")]
    		public string  CreatedBy { get; set; }
    
    		    
    		/// <summary>
    		/// Created Date
    		/// </summary>        
    	//    [DisplayName("Created Date")]
            [Required(ErrorMessage = "Created Date is required")]
    		public System.DateTime  CreatedDate { get; set; }
    
    		    
    		/// <summary>
    		/// Updated By
    		/// </summary>        
    	//    [DisplayName("Updated By")]
            [Required(ErrorMessage = "Updated By is required")]
            [MaxLength(6, ErrorMessage = "Updated By cannot be longer than 6 characters")]
    		public string  UpdatedBy { get; set; }
    
    		    
    		/// <summary>
    		/// Updated Date
    		/// </summary>        
    	//    [DisplayName("Updated Date")]
            [Required(ErrorMessage = "Updated Date is required")]
    		public System.DateTime  UpdatedDate { get; set; }
    
    		    
    		/// <summary>
    		/// Version
    		/// </summary>        
    	//    [DisplayName("Version")]
            [MaxLength(8, ErrorMessage = "Version cannot be longer than 8 characters")]
    		public byte[]  Version { get; set; }
    
    		    
    	}
    //}
    
}
