using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ValuePlus.ViewModel;



namespace ValuePlus.ViewModel.LocaleChainVP
{
    
    /// <summary>
    /// OIMBL class
    /// </summary>
    //[MetadataType(typeof(OIMBLViewModel))]
    //public  partial class OIMBL
    //{
    
    	/// <summary>
    	/// OIMBL Metadata class
    	/// </summary>
    	public   class OIMBLViewModel:ViewModelBase
    	{
    		    
    		/// <summary>
    		/// ID
    		/// </summary>        
    	//    [DisplayName("ID")]
            [Required(ErrorMessage = "ID is required")]
    		public int  ID { get; set; }
    
    		    
    		/// <summary>
    		/// Station ID
    		/// </summary>        
    	//    [DisplayName("Station ID")]
            [MaxLength(5, ErrorMessage = "Station ID cannot be longer than 5 characters")]
    		public string  StationID { get; set; }
    
    		    
    		/// <summary>
    		/// Src Station ID
    		/// </summary>        
    	//    [DisplayName("Src Station ID")]
            [MaxLength(5, ErrorMessage = "Src Station ID cannot be longer than 5 characters")]
    		public string  SrcStationID { get; set; }
    
    		    
    		/// <summary>
    		/// Src ID
    		/// </summary>        
    	//    [DisplayName("Src ID")]
    		public Nullable<int>  SrcID { get; set; }
    
    		    
    		/// <summary>
    		/// MBLNo
    		/// </summary>        
    	//    [DisplayName("MBLNo")]
            [MaxLength(20, ErrorMessage = "MBLNo cannot be longer than 20 characters")]
    		public string  MBLNo { get; set; }
    
    		    
    		/// <summary>
    		/// Lot No
    		/// </summary>        
    	//    [DisplayName("Lot No")]
            [MaxLength(20, ErrorMessage = "Lot No cannot be longer than 20 characters")]
    		public string  LotNo { get; set; }
    
    		    
    		/// <summary>
    		/// HBLCount
    		/// </summary>        
    	//    [DisplayName("HBLCount")]
    		public Nullable<int>  HBLCount { get; set; }
    
    		    
    		/// <summary>
    		/// PCS
    		/// </summary>        
    	//    [DisplayName("PCS")]
    		public Nullable<int>  PCS { get; set; }
    
    		    
    		/// <summary>
    		/// Customer
    		/// </summary>        
    	//    [DisplayName("Customer")]
    		public Nullable<int>  Customer { get; set; }
    
    		    
    		/// <summary>
    		/// SHPR
    		/// </summary>        
    	//    [DisplayName("SHPR")]
    		public Nullable<int>  SHPR { get; set; }
    
    		    
    		/// <summary>
    		/// CNEE
    		/// </summary>        
    	//    [DisplayName("CNEE")]
    		public Nullable<int>  CNEE { get; set; }
    
    		    
    		/// <summary>
    		/// NTFY
    		/// </summary>        
    	//    [DisplayName("NTFY")]
    		public Nullable<int>  NTFY { get; set; }
    
    		    
    		/// <summary>
    		/// DELAgent
    		/// </summary>        
    	//    [DisplayName("DELAgent")]
    		public Nullable<int>  DELAgent { get; set; }
    
    		    
    		/// <summary>
    		/// Booking Party
    		/// </summary>        
    	//    [DisplayName("Booking Party")]
    		public Nullable<int>  BookingParty { get; set; }
    
    		    
    		/// <summary>
    		/// Carrier
    		/// </summary>        
    	//    [DisplayName("Carrier")]
    		public Nullable<int>  Carrier { get; set; }
    
    		    
    		/// <summary>
    		/// Ocean Vessel
    		/// </summary>        
    	//    [DisplayName("Ocean Vessel")]
            [MaxLength(60, ErrorMessage = "Ocean Vessel cannot be longer than 60 characters")]
    		public string  OceanVessel { get; set; }
    
    		    
    		/// <summary>
    		/// Ocean Voyage
    		/// </summary>        
    	//    [DisplayName("Ocean Voyage")]
            [MaxLength(60, ErrorMessage = "Ocean Voyage cannot be longer than 60 characters")]
    		public string  OceanVoyage { get; set; }
    
    		    
    		/// <summary>
    		/// Feeder Vessel
    		/// </summary>        
    	//    [DisplayName("Feeder Vessel")]
            [MaxLength(60, ErrorMessage = "Feeder Vessel cannot be longer than 60 characters")]
    		public string  FeederVessel { get; set; }
    
    		    
    		/// <summary>
    		/// Feeder Voyage
    		/// </summary>        
    	//    [DisplayName("Feeder Voyage")]
            [MaxLength(60, ErrorMessage = "Feeder Voyage cannot be longer than 60 characters")]
    		public string  FeederVoyage { get; set; }
    
    		    
    		/// <summary>
    		/// Shpt Type
    		/// </summary>        
    	//    [DisplayName("Shpt Type")]
            [MaxLength(15, ErrorMessage = "Shpt Type cannot be longer than 15 characters")]
    		public string  ShptType { get; set; }
    
    		    
    		/// <summary>
    		/// Move Type
    		/// </summary>        
    	//    [DisplayName("Move Type")]
            [MaxLength(15, ErrorMessage = "Move Type cannot be longer than 15 characters")]
    		public string  MoveType { get; set; }
    
    		    
    		/// <summary>
    		/// Freight Pay Type
    		/// </summary>        
    	//    [DisplayName("Freight Pay Type")]
            [MaxLength(15, ErrorMessage = "Freight Pay Type cannot be longer than 15 characters")]
    		public string  FreightPayType { get; set; }
    
    		    
    		/// <summary>
    		/// PReceipt
    		/// </summary>        
    	//    [DisplayName("PReceipt")]
    		public Nullable<int>  PReceipt { get; set; }
    
    		    
    		/// <summary>
    		/// PReceipt ETD
    		/// </summary>        
    	//    [DisplayName("PReceipt ETD")]
    		public Nullable<System.DateTime>  PReceiptETD { get; set; }
    
    		    
    		/// <summary>
    		/// PReceipt ATD
    		/// </summary>        
    	//    [DisplayName("PReceipt ATD")]
    		public Nullable<System.DateTime>  PReceiptATD { get; set; }
    
    		    
    		/// <summary>
    		/// PLoading
    		/// </summary>        
    	//    [DisplayName("PLoading")]
    		public Nullable<int>  PLoading { get; set; }
    
    		    
    		/// <summary>
    		/// PLoading ETD
    		/// </summary>        
    	//    [DisplayName("PLoading ETD")]
    		public Nullable<System.DateTime>  PLoadingETD { get; set; }
    
    		    
    		/// <summary>
    		/// PLoading ATD
    		/// </summary>        
    	//    [DisplayName("PLoading ATD")]
    		public Nullable<System.DateTime>  PLoadingATD { get; set; }
    
    		    
    		/// <summary>
    		/// PDischarge
    		/// </summary>        
    	//    [DisplayName("PDischarge")]
    		public Nullable<int>  PDischarge { get; set; }
    
    		    
    		/// <summary>
    		/// PDischarge ETD
    		/// </summary>        
    	//    [DisplayName("PDischarge ETD")]
    		public Nullable<System.DateTime>  PDischargeETD { get; set; }
    
    		    
    		/// <summary>
    		/// PDischarge ATD
    		/// </summary>        
    	//    [DisplayName("PDischarge ATD")]
    		public Nullable<System.DateTime>  PDischargeATD { get; set; }
    
    		    
    		/// <summary>
    		/// PDelivery
    		/// </summary>        
    	//    [DisplayName("PDelivery")]
    		public Nullable<int>  PDelivery { get; set; }
    
    		    
    		/// <summary>
    		/// PDelivery ETD
    		/// </summary>        
    	//    [DisplayName("PDelivery ETD")]
    		public Nullable<System.DateTime>  PDeliveryETD { get; set; }
    
    		    
    		/// <summary>
    		/// PDelivery ATD
    		/// </summary>        
    	//    [DisplayName("PDelivery ATD")]
    		public Nullable<System.DateTime>  PDeliveryATD { get; set; }
    
    		    
    		/// <summary>
    		/// Final DEST
    		/// </summary>        
    	//    [DisplayName("Final DEST")]
    		public Nullable<int>  FinalDEST { get; set; }
    
    		    
    		/// <summary>
    		/// Final DESTETD
    		/// </summary>        
    	//    [DisplayName("Final DESTETD")]
    		public Nullable<System.DateTime>  FinalDESTETD { get; set; }
    
    		    
    		/// <summary>
    		/// Final DESTATD
    		/// </summary>        
    	//    [DisplayName("Final DESTATD")]
    		public Nullable<System.DateTime>  FinalDESTATD { get; set; }
    
    		    
    		/// <summary>
    		/// Is Bookmark
    		/// </summary>        
    	//    [DisplayName("Is Bookmark")]
            [MaxLength(1, ErrorMessage = "Is Bookmark cannot be longer than 1 characters")]
    		public string  IsBookmark { get; set; }
    
    		    
    		/// <summary>
    		/// Status
    		/// </summary>        
    	//    [DisplayName("Status")]
            [MaxLength(10, ErrorMessage = "Status cannot be longer than 10 characters")]
    		public string  Status { get; set; }
    
    		    
    		/// <summary>
    		/// Created By
    		/// </summary>        
    	//    [DisplayName("Created By")]
            [MaxLength(50, ErrorMessage = "Created By cannot be longer than 50 characters")]
    		public string  CreatedBy { get; set; }
    
    		    
    		/// <summary>
    		/// Created DT
    		/// </summary>        
    	//    [DisplayName("Created DT")]
    		public Nullable<System.DateTime>  CreatedDT { get; set; }
    
    		    
    		/// <summary>
    		/// Updated By
    		/// </summary>        
    	//    [DisplayName("Updated By")]
            [MaxLength(50, ErrorMessage = "Updated By cannot be longer than 50 characters")]
    		public string  UpdatedBy { get; set; }
    
    		    
    		/// <summary>
    		/// Updated DT
    		/// </summary>        
    	//    [DisplayName("Updated DT")]
    		public Nullable<System.DateTime>  UpdatedDT { get; set; }
    
    		    
    		/// <summary>
    		/// Coloader
    		/// </summary>        
    	//    [DisplayName("Coloader")]
    		public Nullable<int>  Coloader { get; set; }
    
    		    
    		/// <summary>
    		/// MBLType
    		/// </summary>        
    	//    [DisplayName("MBLType")]
            [MaxLength(5, ErrorMessage = "MBLType cannot be longer than 5 characters")]
    		public string  MBLType { get; set; }
    
    		    
    		/// <summary>
    		/// Pre Alert By
    		/// </summary>        
    	//    [DisplayName("Pre Alert By")]
            [MaxLength(50, ErrorMessage = "Pre Alert By cannot be longer than 50 characters")]
    		public string  PreAlertBy { get; set; }
    
    		    
    		/// <summary>
    		/// Pre Alert DT
    		/// </summary>        
    	//    [DisplayName("Pre Alert DT")]
    		public Nullable<System.DateTime>  PreAlertDT { get; set; }
    
    		    
    		/// <summary>
    		/// Trade Type
    		/// </summary>        
    	//    [DisplayName("Trade Type")]
            [MaxLength(20, ErrorMessage = "Trade Type cannot be longer than 20 characters")]
    		public string  TradeType { get; set; }
    
    		    
    		/// <summary>
    		/// FCalculate Type
    		/// </summary>        
    	//    [DisplayName("FCalculate Type")]
            [MaxLength(10, ErrorMessage = "FCalculate Type cannot be longer than 10 characters")]
    		public string  FCalculateType { get; set; }
    
    		    
    		/// <summary>
    		/// Service Type
    		/// </summary>        
    	//    [DisplayName("Service Type")]
            [MaxLength(30, ErrorMessage = "Service Type cannot be longer than 30 characters")]
    		public string  ServiceType { get; set; }
    
    		    
    		/// <summary>
    		/// Quote Type
    		/// </summary>        
    	//    [DisplayName("Quote Type")]
            [MaxLength(250, ErrorMessage = "Quote Type cannot be longer than 250 characters")]
    		public string  QuoteType { get; set; }
    
    		    
    		/// <summary>
    		/// Cargo Control No
    		/// </summary>        
    	//    [DisplayName("Cargo Control No")]
            [MaxLength(40, ErrorMessage = "Cargo Control No cannot be longer than 40 characters")]
    		public string  CargoControlNo { get; set; }
    
    		    
    		/// <summary>
    		/// salesperson
    		/// </summary>        
    	//    [DisplayName("salesperson")]
    		public Nullable<int>  salesperson { get; set; }
    
    		    
    		/// <summary>
    		/// Nature Of Goods Type
    		/// </summary>        
    	//    [DisplayName("Nature Of Goods Type")]
            [MaxLength(50, ErrorMessage = "Nature Of Goods Type cannot be longer than 50 characters")]
    		public string  NatureOfGoodsType { get; set; }
    
    		    
    		/// <summary>
    		/// Also NTFY
    		/// </summary>        
    	//    [DisplayName("Also NTFY")]
    		public Nullable<int>  AlsoNTFY { get; set; }
    
    		    
    		/// <summary>
    		/// MTSNO
    		/// </summary>        
    	//    [DisplayName("MTSNO")]
            [MaxLength(15, ErrorMessage = "MTSNO cannot be longer than 15 characters")]
    		public string  MTSNO { get; set; }
    
    		    
    		/// <summary>
    		/// Coloader MBLNo
    		/// </summary>        
    	//    [DisplayName("Coloader MBLNo")]
            [MaxLength(50, ErrorMessage = "Coloader MBLNo cannot be longer than 50 characters")]
    		public string  ColoaderMBLNo { get; set; }
    
    		    
    		/// <summary>
    		/// FRTLocation
    		/// </summary>        
    	//    [DisplayName("FRTLocation")]
    		public Nullable<int>  FRTLocation { get; set; }
    
    		    
    		/// <summary>
    		/// Version
    		/// </summary>        
    	//    [DisplayName("Version")]
            [MaxLength(8, ErrorMessage = "Version cannot be longer than 8 characters")]
    		public byte[]  Version { get; set; }
    
    		    
    	}
    //}
    
}
