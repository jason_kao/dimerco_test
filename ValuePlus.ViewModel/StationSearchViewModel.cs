﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ValuePlus.ViewModel
{
    public class StationSearchViewModel:ViewModelBase
    {
        public int StationId { get; set; }
        [Required(ErrorMessage = "Customer Code must key in")]
        public string StationCode { get; set; }
        public string StationName { get; set; }
        public string StationAddress1 { get; set; }
        public int CityId { get; set; }
        public string CityCode { get; set; }
        public int CountryId { get; set; }
        public string CountryCode { get; set; }
        public int StateId { get; set; }
        public string StationZIP { get; set; }
        public string Status { get; set; }
        public string LocalWebURL { get; set; }
        public string LocalWebPort { get; set; }
    }
}
