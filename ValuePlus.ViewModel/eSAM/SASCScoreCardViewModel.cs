using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ValuePlus.ViewModel;



namespace ValuePlus.ViewModel.eSAM
{
    
    /// <summary>
    /// SASCScoreCard class
    /// </summary>
    //[MetadataType(typeof(SASCScoreCardViewModel))]
    //public  partial class SASCScoreCard
    //{
    
    	/// <summary>
    	/// SASCScoreCard Metadata class
    	/// </summary>
    	public   class SASCScoreCardViewModel:ViewModelBase
    	{
    		    
    		/// <summary>
    		/// HQID
    		/// </summary>        
    	//    [DisplayName("HQID")]
            [Required(ErrorMessage = "HQID is required")]
    		public int  HQID { get; set; }
    
    		    
    		/// <summary>
    		/// User ID
    		/// </summary>        
    	//    [DisplayName("User ID")]
            [MaxLength(10, ErrorMessage = "User ID cannot be longer than 10 characters")]
    		public string  UserID { get; set; }
    
    		    
    		/// <summary>
    		/// Item Name
    		/// </summary>        
    	//    [DisplayName("Item Name")]
            [MaxLength(50, ErrorMessage = "Item Name cannot be longer than 50 characters")]
    		public string  ItemName { get; set; }
    
    		    
    		/// <summary>
    		/// Item Category ID
    		/// </summary>        
    	//    [DisplayName("Item Category ID")]
    		public Nullable<int>  ItemCategoryID { get; set; }
    
    		    
    		/// <summary>
    		/// Calculate Type ID
    		/// </summary>        
    	//    [DisplayName("Calculate Type ID")]
    		public Nullable<int>  CalculateTypeID { get; set; }
    
    		    
    		/// <summary>
    		/// Item Type ID
    		/// </summary>        
    	//    [DisplayName("Item Type ID")]
    		public Nullable<int>  ItemTypeID { get; set; }
    
    		    
    		/// <summary>
    		/// Customer Type ID
    		/// </summary>        
    	//    [DisplayName("Customer Type ID")]
    		public Nullable<int>  CustomerTypeID { get; set; }
    
    		    
    		/// <summary>
    		/// Mode Type
    		/// </summary>        
    	//    [DisplayName("Mode Type")]
            [MaxLength(5, ErrorMessage = "Mode Type cannot be longer than 5 characters")]
    		public string  ModeType { get; set; }
    
    		    
    		/// <summary>
    		/// Home Currency
    		/// </summary>        
    	//    [DisplayName("Home Currency")]
            [MaxLength(3, ErrorMessage = "Home Currency cannot be longer than 3 characters")]
    		public string  HomeCurrency { get; set; }
    
    		    
    		/// <summary>
    		/// KPITarget
    		/// </summary>        
    	//    [DisplayName("KPITarget")]
    		public Nullable<int>  KPITarget { get; set; }
    
    		    
    		/// <summary>
    		/// Value
    		/// </summary>        
    	//    [DisplayName("Value")]
    		public Nullable<double>  Value { get; set; }
    
    		    
    		/// <summary>
    		/// Score
    		/// </summary>        
    	//    [DisplayName("Score")]
    		public Nullable<double>  Score { get; set; }
    
    		    
    		/// <summary>
    		/// YY
    		/// </summary>        
    	//    [DisplayName("YY")]
    		public Nullable<int>  YY { get; set; }
    
    		    
    		/// <summary>
    		/// MM
    		/// </summary>        
    	//    [DisplayName("MM")]
    		public Nullable<int>  MM { get; set; }
    
    		    
    		/// <summary>
    		/// Created Date
    		/// </summary>        
    	//    [DisplayName("Created Date")]
    		public Nullable<System.DateTime>  CreatedDate { get; set; }
    
    		    
    		/// <summary>
    		/// Created By
    		/// </summary>        
    	//    [DisplayName("Created By")]
            [MaxLength(10, ErrorMessage = "Created By cannot be longer than 10 characters")]
    		public string  CreatedBy { get; set; }
    
    		    
    		/// <summary>
    		/// Updated Date
    		/// </summary>        
    	//    [DisplayName("Updated Date")]
    		public Nullable<System.DateTime>  UpdatedDate { get; set; }
    
    		    
    		/// <summary>
    		/// Updated By
    		/// </summary>        
    	//    [DisplayName("Updated By")]
            [MaxLength(10, ErrorMessage = "Updated By cannot be longer than 10 characters")]
    		public string  UpdatedBy { get; set; }
    
    		    
    		/// <summary>
    		/// Status
    		/// </summary>        
    	//    [DisplayName("Status")]
            [MaxLength(10, ErrorMessage = "Status cannot be longer than 10 characters")]
    		public string  Status { get; set; }
    
    		    
    		/// <summary>
    		/// Score Target
    		/// </summary>        
    	//    [DisplayName("Score Target")]
            [MaxLength(30, ErrorMessage = "Score Target cannot be longer than 30 characters")]
    		public string  ScoreTarget { get; set; }
    
    		    
    		/// <summary>
    		/// rate
    		/// </summary>        
    	//    [DisplayName("rate")]
            [MaxLength(30, ErrorMessage = "rate cannot be longer than 30 characters")]
    		public string  rate { get; set; }
    
    		    
    	}
    //}
    
}
