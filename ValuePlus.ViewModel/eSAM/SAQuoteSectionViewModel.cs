using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ValuePlus.ViewModel;



namespace ValuePlus.ViewModel.eSAM
{
    
    /// <summary>
    /// SAQuoteSection class
    /// </summary>
    //[MetadataType(typeof(SAQuoteSectionViewModel))]
    //public  partial class SAQuoteSection
    //{
    
    	/// <summary>
    	/// SAQuoteSection Metadata class
    	/// </summary>
    	public   class SAQuoteSectionViewModel:ViewModelBase
    	{
    		    
    		/// <summary>
    		/// ID
    		/// </summary>        
    	//    [DisplayName("ID")]
            [Required(ErrorMessage = "ID is required")]
    		public int  ID { get; set; }
    
    		    
    		/// <summary>
    		/// Quote No
    		/// </summary>        
    	//    [DisplayName("Quote No")]
            [MaxLength(15, ErrorMessage = "Quote No cannot be longer than 15 characters")]
    		public string  QuoteNo { get; set; }
    
    		    
    		/// <summary>
    		/// Origin ID
    		/// </summary>        
    	//    [DisplayName("Origin ID")]
    		public Nullable<int>  OriginID { get; set; }
    
    		    
    		/// <summary>
    		/// Status
    		/// </summary>        
    	//    [DisplayName("Status")]
            [MaxLength(10, ErrorMessage = "Status cannot be longer than 10 characters")]
    		public string  Status { get; set; }
    
    		    
    		/// <summary>
    		/// Created Station ID
    		/// </summary>        
    	//    [DisplayName("Created Station ID")]
            [MaxLength(10, ErrorMessage = "Created Station ID cannot be longer than 10 characters")]
    		public string  CreatedStationID { get; set; }
    
    		    
    		/// <summary>
    		/// Created User ID
    		/// </summary>        
    	//    [DisplayName("Created User ID")]
            [MaxLength(10, ErrorMessage = "Created User ID cannot be longer than 10 characters")]
    		public string  CreatedUserID { get; set; }
    
    		    
    		/// <summary>
    		/// Product Line
    		/// </summary>        
    	//    [DisplayName("Product Line")]
    		public Nullable<int>  ProductLine { get; set; }
    
    		    
    		/// <summary>
    		/// Comp Name
    		/// </summary>        
    	//    [DisplayName("Comp Name")]
            [MaxLength(255, ErrorMessage = "Comp Name cannot be longer than 255 characters")]
    		public string  CompName { get; set; }
    
    		    
    		/// <summary>
    		/// Greeting
    		/// </summary>        
    	//    [DisplayName("Greeting")]
    		public string  Greeting { get; set; }
    
    		    
    		/// <summary>
    		/// Section1 Break
    		/// </summary>        
    	//    [DisplayName("Section1 Break")]
            [MaxLength(1, ErrorMessage = "Section1 Break cannot be longer than 1 characters")]
    		public string  Section1Break { get; set; }
    
    		    
    		/// <summary>
    		/// Section2 Break
    		/// </summary>        
    	//    [DisplayName("Section2 Break")]
            [MaxLength(1, ErrorMessage = "Section2 Break cannot be longer than 1 characters")]
    		public string  Section2Break { get; set; }
    
    		    
    		/// <summary>
    		/// Section3 Break
    		/// </summary>        
    	//    [DisplayName("Section3 Break")]
            [MaxLength(1, ErrorMessage = "Section3 Break cannot be longer than 1 characters")]
    		public string  Section3Break { get; set; }
    
    		    
    		/// <summary>
    		/// Show Area1
    		/// </summary>        
    	//    [DisplayName("Show Area1")]
            [MaxLength(1, ErrorMessage = "Show Area1 cannot be longer than 1 characters")]
    		public string  ShowArea1 { get; set; }
    
    		    
    		/// <summary>
    		/// Show Area2
    		/// </summary>        
    	//    [DisplayName("Show Area2")]
            [MaxLength(1, ErrorMessage = "Show Area2 cannot be longer than 1 characters")]
    		public string  ShowArea2 { get; set; }
    
    		    
    		/// <summary>
    		/// Show Area3
    		/// </summary>        
    	//    [DisplayName("Show Area3")]
            [MaxLength(1, ErrorMessage = "Show Area3 cannot be longer than 1 characters")]
    		public string  ShowArea3 { get; set; }
    
    		    
    		/// <summary>
    		/// LID
    		/// </summary>        
    	//    [DisplayName("LID")]
    		public Nullable<int>  LID { get; set; }
    
    		    
    		/// <summary>
    		/// Quote ID
    		/// </summary>        
    	//    [DisplayName("Quote ID")]
    		public Nullable<int>  QuoteID { get; set; }
    
    		    
    		/// <summary>
    		/// Doc Name
    		/// </summary>        
    	//    [DisplayName("Doc Name")]
            [MaxLength(10, ErrorMessage = "Doc Name cannot be longer than 10 characters")]
    		public string  DocName { get; set; }
    
    		    
    		/// <summary>
    		/// Doc Url
    		/// </summary>        
    	//    [DisplayName("Doc Url")]
            [MaxLength(10, ErrorMessage = "Doc Url cannot be longer than 10 characters")]
    		public string  DocUrl { get; set; }
    
    		    
    		/// <summary>
    		/// Section4 Break
    		/// </summary>        
    	//    [DisplayName("Section4 Break")]
            [MaxLength(1, ErrorMessage = "Section4 Break cannot be longer than 1 characters")]
    		public string  Section4Break { get; set; }
    
    		    
    		/// <summary>
    		/// Show Area4
    		/// </summary>        
    	//    [DisplayName("Show Area4")]
            [MaxLength(1, ErrorMessage = "Show Area4 cannot be longer than 1 characters")]
    		public string  ShowArea4 { get; set; }
    
    		    
    	}
    //}
    
}
