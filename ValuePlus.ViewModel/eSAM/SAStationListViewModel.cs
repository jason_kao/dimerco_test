using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ValuePlus.ViewModel;



namespace ValuePlus.ViewModel.eSAM
{
    
    /// <summary>
    /// SAStationList class
    /// </summary>
    //[MetadataType(typeof(SAStationListViewModel))]
    //public  partial class SAStationList
    //{
    
    	/// <summary>
    	/// SAStationList Metadata class
    	/// </summary>
    	public   class SAStationListViewModel:ViewModelBase
    	{
    		    
    		/// <summary>
    		/// Station Code
    		/// </summary>        
    	//    [DisplayName("Station Code")]
            [MaxLength(10, ErrorMessage = "Station Code cannot be longer than 10 characters")]
    		public string  StationCode { get; set; }
    
    		    
    		/// <summary>
    		/// Credit Acc
    		/// </summary>        
    	//    [DisplayName("Credit Acc")]
            [MaxLength(10, ErrorMessage = "Credit Acc cannot be longer than 10 characters")]
    		public string  CreditAcc { get; set; }
    
    		    
    		/// <summary>
    		/// Station Name
    		/// </summary>        
    	//    [DisplayName("Station Name")]
            [MaxLength(10, ErrorMessage = "Station Name cannot be longer than 10 characters")]
    		public string  StationName { get; set; }
    
    		    
    		/// <summary>
    		/// Local Curr
    		/// </summary>        
    	//    [DisplayName("Local Curr")]
            [MaxLength(10, ErrorMessage = "Local Curr cannot be longer than 10 characters")]
    		public string  LocalCurr { get; set; }
    
    		    
    		/// <summary>
    		/// Credit Amount
    		/// </summary>        
    	//    [DisplayName("Credit Amount")]
            [MaxLength(10, ErrorMessage = "Credit Amount cannot be longer than 10 characters")]
    		public string  CreditAmount { get; set; }
    
    		    
    		/// <summary>
    		/// Credit Air451st
    		/// </summary>        
    	//    [DisplayName("Credit Air451st")]
            [MaxLength(10, ErrorMessage = "Credit Air451st cannot be longer than 10 characters")]
    		public string  CreditAir451st { get; set; }
    
    		    
    		/// <summary>
    		/// Credit Air451st Deputy
    		/// </summary>        
    	//    [DisplayName("Credit Air451st Deputy")]
            [MaxLength(10, ErrorMessage = "Credit Air451st Deputy cannot be longer than 10 characters")]
    		public string  CreditAir451stDeputy { get; set; }
    
    		    
    		/// <summary>
    		/// Credit Air452nd
    		/// </summary>        
    	//    [DisplayName("Credit Air452nd")]
            [MaxLength(10, ErrorMessage = "Credit Air452nd cannot be longer than 10 characters")]
    		public string  CreditAir452nd { get; set; }
    
    		    
    		/// <summary>
    		/// Credit Air452nd Deputy
    		/// </summary>        
    	//    [DisplayName("Credit Air452nd Deputy")]
            [MaxLength(10, ErrorMessage = "Credit Air452nd Deputy cannot be longer than 10 characters")]
    		public string  CreditAir452ndDeputy { get; set; }
    
    		    
    		/// <summary>
    		/// Credit Air601st
    		/// </summary>        
    	//    [DisplayName("Credit Air601st")]
            [MaxLength(10, ErrorMessage = "Credit Air601st cannot be longer than 10 characters")]
    		public string  CreditAir601st { get; set; }
    
    		    
    		/// <summary>
    		/// Credit Air601st Deputy
    		/// </summary>        
    	//    [DisplayName("Credit Air601st Deputy")]
            [MaxLength(10, ErrorMessage = "Credit Air601st Deputy cannot be longer than 10 characters")]
    		public string  CreditAir601stDeputy { get; set; }
    
    		    
    		/// <summary>
    		/// Credit Air602nd
    		/// </summary>        
    	//    [DisplayName("Credit Air602nd")]
            [MaxLength(10, ErrorMessage = "Credit Air602nd cannot be longer than 10 characters")]
    		public string  CreditAir602nd { get; set; }
    
    		    
    		/// <summary>
    		/// Credit Air602nd Deputy
    		/// </summary>        
    	//    [DisplayName("Credit Air602nd Deputy")]
            [MaxLength(10, ErrorMessage = "Credit Air602nd Deputy cannot be longer than 10 characters")]
    		public string  CreditAir602ndDeputy { get; set; }
    
    		    
    		/// <summary>
    		/// Credit Sea301st
    		/// </summary>        
    	//    [DisplayName("Credit Sea301st")]
            [MaxLength(10, ErrorMessage = "Credit Sea301st cannot be longer than 10 characters")]
    		public string  CreditSea301st { get; set; }
    
    		    
    		/// <summary>
    		/// Credit Sea301st Deputy
    		/// </summary>        
    	//    [DisplayName("Credit Sea301st Deputy")]
            [MaxLength(10, ErrorMessage = "Credit Sea301st Deputy cannot be longer than 10 characters")]
    		public string  CreditSea301stDeputy { get; set; }
    
    		    
    		/// <summary>
    		/// Credit Sea302nd
    		/// </summary>        
    	//    [DisplayName("Credit Sea302nd")]
            [MaxLength(10, ErrorMessage = "Credit Sea302nd cannot be longer than 10 characters")]
    		public string  CreditSea302nd { get; set; }
    
    		    
    		/// <summary>
    		/// Credit Sea302nd Deputy
    		/// </summary>        
    	//    [DisplayName("Credit Sea302nd Deputy")]
            [MaxLength(10, ErrorMessage = "Credit Sea302nd Deputy cannot be longer than 10 characters")]
    		public string  CreditSea302ndDeputy { get; set; }
    
    		    
    		/// <summary>
    		/// Credit Sea451st
    		/// </summary>        
    	//    [DisplayName("Credit Sea451st")]
            [MaxLength(10, ErrorMessage = "Credit Sea451st cannot be longer than 10 characters")]
    		public string  CreditSea451st { get; set; }
    
    		    
    		/// <summary>
    		/// Credit Sea451st Deputy
    		/// </summary>        
    	//    [DisplayName("Credit Sea451st Deputy")]
            [MaxLength(10, ErrorMessage = "Credit Sea451st Deputy cannot be longer than 10 characters")]
    		public string  CreditSea451stDeputy { get; set; }
    
    		    
    		/// <summary>
    		/// Credit Sea452nd
    		/// </summary>        
    	//    [DisplayName("Credit Sea452nd")]
            [MaxLength(10, ErrorMessage = "Credit Sea452nd cannot be longer than 10 characters")]
    		public string  CreditSea452nd { get; set; }
    
    		    
    		/// <summary>
    		/// Credit Sea452nd Deputy
    		/// </summary>        
    	//    [DisplayName("Credit Sea452nd Deputy")]
            [MaxLength(10, ErrorMessage = "Credit Sea452nd Deputy cannot be longer than 10 characters")]
    		public string  CreditSea452ndDeputy { get; set; }
    
    		    
    		/// <summary>
    		/// Credit Sea601st
    		/// </summary>        
    	//    [DisplayName("Credit Sea601st")]
            [MaxLength(10, ErrorMessage = "Credit Sea601st cannot be longer than 10 characters")]
    		public string  CreditSea601st { get; set; }
    
    		    
    		/// <summary>
    		/// Credit Sea601st Deputy
    		/// </summary>        
    	//    [DisplayName("Credit Sea601st Deputy")]
            [MaxLength(10, ErrorMessage = "Credit Sea601st Deputy cannot be longer than 10 characters")]
    		public string  CreditSea601stDeputy { get; set; }
    
    		    
    		/// <summary>
    		/// Credit Sea602nd
    		/// </summary>        
    	//    [DisplayName("Credit Sea602nd")]
            [MaxLength(10, ErrorMessage = "Credit Sea602nd cannot be longer than 10 characters")]
    		public string  CreditSea602nd { get; set; }
    
    		    
    		/// <summary>
    		/// Credit Sea602nd Deputy
    		/// </summary>        
    	//    [DisplayName("Credit Sea602nd Deputy")]
            [MaxLength(10, ErrorMessage = "Credit Sea602nd Deputy cannot be longer than 10 characters")]
    		public string  CreditSea602ndDeputy { get; set; }
    
    		    
    		/// <summary>
    		/// Credit Air751st
    		/// </summary>        
    	//    [DisplayName("Credit Air751st")]
            [MaxLength(10, ErrorMessage = "Credit Air751st cannot be longer than 10 characters")]
    		public string  CreditAir751st { get; set; }
    
    		    
    		/// <summary>
    		/// Credit Air751st Deputy
    		/// </summary>        
    	//    [DisplayName("Credit Air751st Deputy")]
            [MaxLength(10, ErrorMessage = "Credit Air751st Deputy cannot be longer than 10 characters")]
    		public string  CreditAir751stDeputy { get; set; }
    
    		    
    		/// <summary>
    		/// Credit Air752nd
    		/// </summary>        
    	//    [DisplayName("Credit Air752nd")]
            [MaxLength(10, ErrorMessage = "Credit Air752nd cannot be longer than 10 characters")]
    		public string  CreditAir752nd { get; set; }
    
    		    
    		/// <summary>
    		/// Credit Air752nd Deputy
    		/// </summary>        
    	//    [DisplayName("Credit Air752nd Deputy")]
            [MaxLength(10, ErrorMessage = "Credit Air752nd Deputy cannot be longer than 10 characters")]
    		public string  CreditAir752ndDeputy { get; set; }
    
    		    
    		/// <summary>
    		/// Credit Air901st
    		/// </summary>        
    	//    [DisplayName("Credit Air901st")]
            [MaxLength(10, ErrorMessage = "Credit Air901st cannot be longer than 10 characters")]
    		public string  CreditAir901st { get; set; }
    
    		    
    		/// <summary>
    		/// Credit Air901st Deputy
    		/// </summary>        
    	//    [DisplayName("Credit Air901st Deputy")]
            [MaxLength(10, ErrorMessage = "Credit Air901st Deputy cannot be longer than 10 characters")]
    		public string  CreditAir901stDeputy { get; set; }
    
    		    
    		/// <summary>
    		/// Credit Air902nd
    		/// </summary>        
    	//    [DisplayName("Credit Air902nd")]
            [MaxLength(10, ErrorMessage = "Credit Air902nd cannot be longer than 10 characters")]
    		public string  CreditAir902nd { get; set; }
    
    		    
    		/// <summary>
    		/// Credit Air902nd Deputy
    		/// </summary>        
    	//    [DisplayName("Credit Air902nd Deputy")]
            [MaxLength(10, ErrorMessage = "Credit Air902nd Deputy cannot be longer than 10 characters")]
    		public string  CreditAir902ndDeputy { get; set; }
    
    		    
    		/// <summary>
    		/// Credit Sea751st
    		/// </summary>        
    	//    [DisplayName("Credit Sea751st")]
            [MaxLength(10, ErrorMessage = "Credit Sea751st cannot be longer than 10 characters")]
    		public string  CreditSea751st { get; set; }
    
    		    
    		/// <summary>
    		/// Credit Sea751st Deputy
    		/// </summary>        
    	//    [DisplayName("Credit Sea751st Deputy")]
            [MaxLength(10, ErrorMessage = "Credit Sea751st Deputy cannot be longer than 10 characters")]
    		public string  CreditSea751stDeputy { get; set; }
    
    		    
    		/// <summary>
    		/// Credit Sea752nd
    		/// </summary>        
    	//    [DisplayName("Credit Sea752nd")]
            [MaxLength(10, ErrorMessage = "Credit Sea752nd cannot be longer than 10 characters")]
    		public string  CreditSea752nd { get; set; }
    
    		    
    		/// <summary>
    		/// Credit Sea752nd Deputy
    		/// </summary>        
    	//    [DisplayName("Credit Sea752nd Deputy")]
            [MaxLength(10, ErrorMessage = "Credit Sea752nd Deputy cannot be longer than 10 characters")]
    		public string  CreditSea752ndDeputy { get; set; }
    
    		    
    		/// <summary>
    		/// Credit WMS451st
    		/// </summary>        
    	//    [DisplayName("Credit WMS451st")]
            [MaxLength(10, ErrorMessage = "Credit WMS451st cannot be longer than 10 characters")]
    		public string  CreditWMS451st { get; set; }
    
    		    
    		/// <summary>
    		/// Credit WMS451st Deputy
    		/// </summary>        
    	//    [DisplayName("Credit WMS451st Deputy")]
            [MaxLength(10, ErrorMessage = "Credit WMS451st Deputy cannot be longer than 10 characters")]
    		public string  CreditWMS451stDeputy { get; set; }
    
    		    
    		/// <summary>
    		/// Credit WMS452nd
    		/// </summary>        
    	//    [DisplayName("Credit WMS452nd")]
            [MaxLength(10, ErrorMessage = "Credit WMS452nd cannot be longer than 10 characters")]
    		public string  CreditWMS452nd { get; set; }
    
    		    
    		/// <summary>
    		/// Credit WMS452nd Deputy
    		/// </summary>        
    	//    [DisplayName("Credit WMS452nd Deputy")]
            [MaxLength(10, ErrorMessage = "Credit WMS452nd Deputy cannot be longer than 10 characters")]
    		public string  CreditWMS452ndDeputy { get; set; }
    
    		    
    		/// <summary>
    		/// Credit WMS1st
    		/// </summary>        
    	//    [DisplayName("Credit WMS1st")]
            [MaxLength(10, ErrorMessage = "Credit WMS1st cannot be longer than 10 characters")]
    		public string  CreditWMS1st { get; set; }
    
    		    
    		/// <summary>
    		/// Credit WMS1st Deputy
    		/// </summary>        
    	//    [DisplayName("Credit WMS1st Deputy")]
            [MaxLength(10, ErrorMessage = "Credit WMS1st Deputy cannot be longer than 10 characters")]
    		public string  CreditWMS1stDeputy { get; set; }
    
    		    
    		/// <summary>
    		/// Credit WMS2nd
    		/// </summary>        
    	//    [DisplayName("Credit WMS2nd")]
            [MaxLength(10, ErrorMessage = "Credit WMS2nd cannot be longer than 10 characters")]
    		public string  CreditWMS2nd { get; set; }
    
    		    
    		/// <summary>
    		/// Credit WMS2nd Deputy
    		/// </summary>        
    	//    [DisplayName("Credit WMS2nd Deputy")]
            [MaxLength(10, ErrorMessage = "Credit WMS2nd Deputy cannot be longer than 10 characters")]
    		public string  CreditWMS2ndDeputy { get; set; }
    
    		    
    		/// <summary>
    		/// Credit WMS601st
    		/// </summary>        
    	//    [DisplayName("Credit WMS601st")]
            [MaxLength(10, ErrorMessage = "Credit WMS601st cannot be longer than 10 characters")]
    		public string  CreditWMS601st { get; set; }
    
    		    
    		/// <summary>
    		/// Credit WMS601st Deputy
    		/// </summary>        
    	//    [DisplayName("Credit WMS601st Deputy")]
            [MaxLength(10, ErrorMessage = "Credit WMS601st Deputy cannot be longer than 10 characters")]
    		public string  CreditWMS601stDeputy { get; set; }
    
    		    
    		/// <summary>
    		/// Credit WMS602nd
    		/// </summary>        
    	//    [DisplayName("Credit WMS602nd")]
            [MaxLength(10, ErrorMessage = "Credit WMS602nd cannot be longer than 10 characters")]
    		public string  CreditWMS602nd { get; set; }
    
    		    
    		/// <summary>
    		/// Credit WMS602nd Deputy
    		/// </summary>        
    	//    [DisplayName("Credit WMS602nd Deputy")]
            [MaxLength(10, ErrorMessage = "Credit WMS602nd Deputy cannot be longer than 10 characters")]
    		public string  CreditWMS602ndDeputy { get; set; }
    
    		    
    		/// <summary>
    		/// Credit WMS301st
    		/// </summary>        
    	//    [DisplayName("Credit WMS301st")]
            [MaxLength(10, ErrorMessage = "Credit WMS301st cannot be longer than 10 characters")]
    		public string  CreditWMS301st { get; set; }
    
    		    
    		/// <summary>
    		/// Credit WMS301st Deputy
    		/// </summary>        
    	//    [DisplayName("Credit WMS301st Deputy")]
            [MaxLength(10, ErrorMessage = "Credit WMS301st Deputy cannot be longer than 10 characters")]
    		public string  CreditWMS301stDeputy { get; set; }
    
    		    
    		/// <summary>
    		/// Credit WMS302nd
    		/// </summary>        
    	//    [DisplayName("Credit WMS302nd")]
            [MaxLength(10, ErrorMessage = "Credit WMS302nd cannot be longer than 10 characters")]
    		public string  CreditWMS302nd { get; set; }
    
    		    
    		/// <summary>
    		/// Credit WMS302nd Deputy
    		/// </summary>        
    	//    [DisplayName("Credit WMS302nd Deputy")]
            [MaxLength(10, ErrorMessage = "Credit WMS302nd Deputy cannot be longer than 10 characters")]
    		public string  CreditWMS302ndDeputy { get; set; }
    
    		    
    		/// <summary>
    		/// Credit DAS451st
    		/// </summary>        
    	//    [DisplayName("Credit DAS451st")]
            [MaxLength(10, ErrorMessage = "Credit DAS451st cannot be longer than 10 characters")]
    		public string  CreditDAS451st { get; set; }
    
    		    
    		/// <summary>
    		/// Credit DAS451st Deputy
    		/// </summary>        
    	//    [DisplayName("Credit DAS451st Deputy")]
            [MaxLength(10, ErrorMessage = "Credit DAS451st Deputy cannot be longer than 10 characters")]
    		public string  CreditDAS451stDeputy { get; set; }
    
    		    
    		/// <summary>
    		/// Credit DAS452nd
    		/// </summary>        
    	//    [DisplayName("Credit DAS452nd")]
            [MaxLength(10, ErrorMessage = "Credit DAS452nd cannot be longer than 10 characters")]
    		public string  CreditDAS452nd { get; set; }
    
    		    
    		/// <summary>
    		/// Credit DAS452nd Deputy
    		/// </summary>        
    	//    [DisplayName("Credit DAS452nd Deputy")]
            [MaxLength(10, ErrorMessage = "Credit DAS452nd Deputy cannot be longer than 10 characters")]
    		public string  CreditDAS452ndDeputy { get; set; }
    
    		    
    		/// <summary>
    		/// Credit DAS601st
    		/// </summary>        
    	//    [DisplayName("Credit DAS601st")]
            [MaxLength(10, ErrorMessage = "Credit DAS601st cannot be longer than 10 characters")]
    		public string  CreditDAS601st { get; set; }
    
    		    
    		/// <summary>
    		/// Credit DAS601st Deputy
    		/// </summary>        
    	//    [DisplayName("Credit DAS601st Deputy")]
            [MaxLength(10, ErrorMessage = "Credit DAS601st Deputy cannot be longer than 10 characters")]
    		public string  CreditDAS601stDeputy { get; set; }
    
    		    
    		/// <summary>
    		/// Credit DAS602nd
    		/// </summary>        
    	//    [DisplayName("Credit DAS602nd")]
            [MaxLength(10, ErrorMessage = "Credit DAS602nd cannot be longer than 10 characters")]
    		public string  CreditDAS602nd { get; set; }
    
    		    
    		/// <summary>
    		/// Credit DAS602nd Deputy
    		/// </summary>        
    	//    [DisplayName("Credit DAS602nd Deputy")]
            [MaxLength(10, ErrorMessage = "Credit DAS602nd Deputy cannot be longer than 10 characters")]
    		public string  CreditDAS602ndDeputy { get; set; }
    
    		    
    		/// <summary>
    		/// Credit DAS1st
    		/// </summary>        
    	//    [DisplayName("Credit DAS1st")]
            [MaxLength(10, ErrorMessage = "Credit DAS1st cannot be longer than 10 characters")]
    		public string  CreditDAS1st { get; set; }
    
    		    
    		/// <summary>
    		/// Credit DAS1st Deputy
    		/// </summary>        
    	//    [DisplayName("Credit DAS1st Deputy")]
            [MaxLength(10, ErrorMessage = "Credit DAS1st Deputy cannot be longer than 10 characters")]
    		public string  CreditDAS1stDeputy { get; set; }
    
    		    
    		/// <summary>
    		/// Credit DAS2nd
    		/// </summary>        
    	//    [DisplayName("Credit DAS2nd")]
            [MaxLength(10, ErrorMessage = "Credit DAS2nd cannot be longer than 10 characters")]
    		public string  CreditDAS2nd { get; set; }
    
    		    
    		/// <summary>
    		/// Credit DAS2nd Deputy
    		/// </summary>        
    	//    [DisplayName("Credit DAS2nd Deputy")]
            [MaxLength(10, ErrorMessage = "Credit DAS2nd Deputy cannot be longer than 10 characters")]
    		public string  CreditDAS2ndDeputy { get; set; }
    
    		    
    		/// <summary>
    		/// Quote1st
    		/// </summary>        
    	//    [DisplayName("Quote1st")]
            [MaxLength(10, ErrorMessage = "Quote1st cannot be longer than 10 characters")]
    		public string  Quote1st { get; set; }
    
    		    
    		/// <summary>
    		/// Quote1st Deputy
    		/// </summary>        
    	//    [DisplayName("Quote1st Deputy")]
            [MaxLength(10, ErrorMessage = "Quote1st Deputy cannot be longer than 10 characters")]
    		public string  Quote1stDeputy { get; set; }
    
    		    
    		/// <summary>
    		/// Quote2nd
    		/// </summary>        
    	//    [DisplayName("Quote2nd")]
            [MaxLength(10, ErrorMessage = "Quote2nd cannot be longer than 10 characters")]
    		public string  Quote2nd { get; set; }
    
    		    
    		/// <summary>
    		/// Quote2nd Deputy
    		/// </summary>        
    	//    [DisplayName("Quote2nd Deputy")]
            [MaxLength(10, ErrorMessage = "Quote2nd Deputy cannot be longer than 10 characters")]
    		public string  Quote2ndDeputy { get; set; }
    
    		    
    		/// <summary>
    		/// Address
    		/// </summary>        
    	//    [DisplayName("Address")]
            [MaxLength(50, ErrorMessage = "Address cannot be longer than 50 characters")]
    		public string  Address { get; set; }
    
    		    
    		/// <summary>
    		/// Phone
    		/// </summary>        
    	//    [DisplayName("Phone")]
            [MaxLength(20, ErrorMessage = "Phone cannot be longer than 20 characters")]
    		public string  Phone { get; set; }
    
    		    
    		/// <summary>
    		/// Credit1st
    		/// </summary>        
    	//    [DisplayName("Credit1st")]
            [MaxLength(10, ErrorMessage = "Credit1st cannot be longer than 10 characters")]
    		public string  Credit1st { get; set; }
    
    		    
    		/// <summary>
    		/// Credit1st Deputy
    		/// </summary>        
    	//    [DisplayName("Credit1st Deputy")]
            [MaxLength(10, ErrorMessage = "Credit1st Deputy cannot be longer than 10 characters")]
    		public string  Credit1stDeputy { get; set; }
    
    		    
    		/// <summary>
    		/// Credit2nd
    		/// </summary>        
    	//    [DisplayName("Credit2nd")]
            [MaxLength(10, ErrorMessage = "Credit2nd cannot be longer than 10 characters")]
    		public string  Credit2nd { get; set; }
    
    		    
    		/// <summary>
    		/// Credit2nd Deputy
    		/// </summary>        
    	//    [DisplayName("Credit2nd Deputy")]
            [MaxLength(10, ErrorMessage = "Credit2nd Deputy cannot be longer than 10 characters")]
    		public string  Credit2ndDeputy { get; set; }
    
    		    
    		/// <summary>
    		/// POD
    		/// </summary>        
    	//    [DisplayName("POD")]
            [MaxLength(10, ErrorMessage = "POD cannot be longer than 10 characters")]
    		public string  POD { get; set; }
    
    		    
    		/// <summary>
    		/// POL
    		/// </summary>        
    	//    [DisplayName("POL")]
            [MaxLength(10, ErrorMessage = "POL cannot be longer than 10 characters")]
    		public string  POL { get; set; }
    
    		    
    		/// <summary>
    		/// Station Level
    		/// </summary>        
    	//    [DisplayName("Station Level")]
            [MaxLength(10, ErrorMessage = "Station Level cannot be longer than 10 characters")]
    		public string  StationLevel { get; set; }
    
    		    
    		/// <summary>
    		/// No Air15
    		/// </summary>        
    	//    [DisplayName("No Air15")]
            [MaxLength(10, ErrorMessage = "No Air15 cannot be longer than 10 characters")]
    		public string  NoAir15 { get; set; }
    
    		    
    		/// <summary>
    		/// No Sea07
    		/// </summary>        
    	//    [DisplayName("No Sea07")]
            [MaxLength(10, ErrorMessage = "No Sea07 cannot be longer than 10 characters")]
    		public string  NoSea07 { get; set; }
    
    		    
    		/// <summary>
    		/// No Air30
    		/// </summary>        
    	//    [DisplayName("No Air30")]
            [MaxLength(10, ErrorMessage = "No Air30 cannot be longer than 10 characters")]
    		public string  NoAir30 { get; set; }
    
    		    
    		/// <summary>
    		/// No Sea15
    		/// </summary>        
    	//    [DisplayName("No Sea15")]
            [MaxLength(10, ErrorMessage = "No Sea15 cannot be longer than 10 characters")]
    		public string  NoSea15 { get; set; }
    
    		    
    		/// <summary>
    		/// Credit User ID
    		/// </summary>        
    	//    [DisplayName("Credit User ID")]
            [MaxLength(10, ErrorMessage = "Credit User ID cannot be longer than 10 characters")]
    		public string  CreditUserID { get; set; }
    
    		    
    		/// <summary>
    		/// Credit Date
    		/// </summary>        
    	//    [DisplayName("Credit Date")]
    		public Nullable<System.DateTime>  CreditDate { get; set; }
    
    		    
    		/// <summary>
    		/// Update User ID
    		/// </summary>        
    	//    [DisplayName("Update User ID")]
            [MaxLength(10, ErrorMessage = "Update User ID cannot be longer than 10 characters")]
    		public string  UpdateUserID { get; set; }
    
    		    
    		/// <summary>
    		/// Update Date
    		/// </summary>        
    	//    [DisplayName("Update Date")]
    		public Nullable<System.DateTime>  UpdateDate { get; set; }
    
    		    
    		/// <summary>
    		/// Station ID
    		/// </summary>        
    	//    [DisplayName("Station ID")]
            [Required(ErrorMessage = "Station ID is required")]
            [MaxLength(10, ErrorMessage = "Station ID cannot be longer than 10 characters")]
    		public string  StationID { get; set; }
    
    		    
    		/// <summary>
    		/// Object CNT
    		/// </summary>        
    	//    [DisplayName("Object CNT")]
    		public Nullable<int>  ObjectCNT { get; set; }
    
    		    
    		/// <summary>
    		/// creditair401st
    		/// </summary>        
    	//    [DisplayName("creditair401st")]
            [MaxLength(10, ErrorMessage = "creditair401st cannot be longer than 10 characters")]
    		public string  creditair401st { get; set; }
    
    		    
    		/// <summary>
    		/// creditair401st Deputy
    		/// </summary>        
    	//    [DisplayName("creditair401st Deputy")]
            [MaxLength(10, ErrorMessage = "creditair401st Deputy cannot be longer than 10 characters")]
    		public string  creditair401stDeputy { get; set; }
    
    		    
    		/// <summary>
    		/// creditair402nd
    		/// </summary>        
    	//    [DisplayName("creditair402nd")]
            [MaxLength(10, ErrorMessage = "creditair402nd cannot be longer than 10 characters")]
    		public string  creditair402nd { get; set; }
    
    		    
    		/// <summary>
    		/// creditair402nd Deputy
    		/// </summary>        
    	//    [DisplayName("creditair402nd Deputy")]
            [MaxLength(10, ErrorMessage = "creditair402nd Deputy cannot be longer than 10 characters")]
    		public string  creditair402ndDeputy { get; set; }
    
    		    
    		/// <summary>
    		/// credit Sea401st
    		/// </summary>        
    	//    [DisplayName("credit Sea401st")]
            [MaxLength(10, ErrorMessage = "credit Sea401st cannot be longer than 10 characters")]
    		public string  creditSea401st { get; set; }
    
    		    
    		/// <summary>
    		/// credit Sea401st Deputy
    		/// </summary>        
    	//    [DisplayName("credit Sea401st Deputy")]
            [MaxLength(10, ErrorMessage = "credit Sea401st Deputy cannot be longer than 10 characters")]
    		public string  creditSea401stDeputy { get; set; }
    
    		    
    		/// <summary>
    		/// credit Sea402nd
    		/// </summary>        
    	//    [DisplayName("credit Sea402nd")]
            [MaxLength(10, ErrorMessage = "credit Sea402nd cannot be longer than 10 characters")]
    		public string  creditSea402nd { get; set; }
    
    		    
    		/// <summary>
    		/// credit Sea402nd Deputy
    		/// </summary>        
    	//    [DisplayName("credit Sea402nd Deputy")]
            [MaxLength(10, ErrorMessage = "credit Sea402nd Deputy cannot be longer than 10 characters")]
    		public string  creditSea402ndDeputy { get; set; }
    
    		    
    		/// <summary>
    		/// credit Sea71st
    		/// </summary>        
    	//    [DisplayName("credit Sea71st")]
            [MaxLength(10, ErrorMessage = "credit Sea71st cannot be longer than 10 characters")]
    		public string  creditSea71st { get; set; }
    
    		    
    		/// <summary>
    		/// credit Sea71st Deputy
    		/// </summary>        
    	//    [DisplayName("credit Sea71st Deputy")]
            [MaxLength(10, ErrorMessage = "credit Sea71st Deputy cannot be longer than 10 characters")]
    		public string  creditSea71stDeputy { get; set; }
    
    		    
    		/// <summary>
    		/// credit Sea72nd
    		/// </summary>        
    	//    [DisplayName("credit Sea72nd")]
            [MaxLength(10, ErrorMessage = "credit Sea72nd cannot be longer than 10 characters")]
    		public string  creditSea72nd { get; set; }
    
    		    
    		/// <summary>
    		/// credit Sea72nd Deputy
    		/// </summary>        
    	//    [DisplayName("credit Sea72nd Deputy")]
            [MaxLength(10, ErrorMessage = "credit Sea72nd Deputy cannot be longer than 10 characters")]
    		public string  creditSea72ndDeputy { get; set; }
    
    		    
    		/// <summary>
    		/// Self App
    		/// </summary>        
    	//    [DisplayName("Self App")]
            [MaxLength(10, ErrorMessage = "Self App cannot be longer than 10 characters")]
    		public string  SelfApp { get; set; }
    
    		    
    		/// <summary>
    		/// Quote Appby PID
    		/// </summary>        
    	//    [DisplayName("Quote Appby PID")]
            [Required(ErrorMessage = "Quote Appby PID is required")]
    		public bool  QuoteAppbyPID { get; set; }
    
    		    
    		/// <summary>
    		/// Quote Type
    		/// </summary>        
    	//    [DisplayName("Quote Type")]
            [Required(ErrorMessage = "Quote Type is required")]
    		public int  QuoteType { get; set; }
    
    		    
    		/// <summary>
    		/// Credit MTS301st
    		/// </summary>        
    	//    [DisplayName("Credit MTS301st")]
            [MaxLength(10, ErrorMessage = "Credit MTS301st cannot be longer than 10 characters")]
    		public string  CreditMTS301st { get; set; }
    
    		    
    		/// <summary>
    		/// Credit MTS301st Deputy
    		/// </summary>        
    	//    [DisplayName("Credit MTS301st Deputy")]
            [MaxLength(10, ErrorMessage = "Credit MTS301st Deputy cannot be longer than 10 characters")]
    		public string  CreditMTS301stDeputy { get; set; }
    
    		    
    		/// <summary>
    		/// Credit MTS302nd
    		/// </summary>        
    	//    [DisplayName("Credit MTS302nd")]
            [MaxLength(10, ErrorMessage = "Credit MTS302nd cannot be longer than 10 characters")]
    		public string  CreditMTS302nd { get; set; }
    
    		    
    		/// <summary>
    		/// Credit MTS302nd Deputy
    		/// </summary>        
    	//    [DisplayName("Credit MTS302nd Deputy")]
            [MaxLength(10, ErrorMessage = "Credit MTS302nd Deputy cannot be longer than 10 characters")]
    		public string  CreditMTS302ndDeputy { get; set; }
    
    		    
    		/// <summary>
    		/// Credit MTS451st
    		/// </summary>        
    	//    [DisplayName("Credit MTS451st")]
            [MaxLength(10, ErrorMessage = "Credit MTS451st cannot be longer than 10 characters")]
    		public string  CreditMTS451st { get; set; }
    
    		    
    		/// <summary>
    		/// Credit MTS451st Deputy
    		/// </summary>        
    	//    [DisplayName("Credit MTS451st Deputy")]
            [MaxLength(10, ErrorMessage = "Credit MTS451st Deputy cannot be longer than 10 characters")]
    		public string  CreditMTS451stDeputy { get; set; }
    
    		    
    		/// <summary>
    		/// Credit MTS452nd
    		/// </summary>        
    	//    [DisplayName("Credit MTS452nd")]
            [MaxLength(10, ErrorMessage = "Credit MTS452nd cannot be longer than 10 characters")]
    		public string  CreditMTS452nd { get; set; }
    
    		    
    		/// <summary>
    		/// Credit MTS452nd Deputy
    		/// </summary>        
    	//    [DisplayName("Credit MTS452nd Deputy")]
            [MaxLength(10, ErrorMessage = "Credit MTS452nd Deputy cannot be longer than 10 characters")]
    		public string  CreditMTS452ndDeputy { get; set; }
    
    		    
    		/// <summary>
    		/// Credit MTS601st
    		/// </summary>        
    	//    [DisplayName("Credit MTS601st")]
            [MaxLength(10, ErrorMessage = "Credit MTS601st cannot be longer than 10 characters")]
    		public string  CreditMTS601st { get; set; }
    
    		    
    		/// <summary>
    		/// Credit MTS601st Deputy
    		/// </summary>        
    	//    [DisplayName("Credit MTS601st Deputy")]
            [MaxLength(10, ErrorMessage = "Credit MTS601st Deputy cannot be longer than 10 characters")]
    		public string  CreditMTS601stDeputy { get; set; }
    
    		    
    		/// <summary>
    		/// Credit MTS602nd
    		/// </summary>        
    	//    [DisplayName("Credit MTS602nd")]
            [MaxLength(10, ErrorMessage = "Credit MTS602nd cannot be longer than 10 characters")]
    		public string  CreditMTS602nd { get; set; }
    
    		    
    		/// <summary>
    		/// Credit MTS602nd Deputy
    		/// </summary>        
    	//    [DisplayName("Credit MTS602nd Deputy")]
            [MaxLength(10, ErrorMessage = "Credit MTS602nd Deputy cannot be longer than 10 characters")]
    		public string  CreditMTS602ndDeputy { get; set; }
    
    		    
    		/// <summary>
    		/// Credit MTS751st
    		/// </summary>        
    	//    [DisplayName("Credit MTS751st")]
            [MaxLength(10, ErrorMessage = "Credit MTS751st cannot be longer than 10 characters")]
    		public string  CreditMTS751st { get; set; }
    
    		    
    		/// <summary>
    		/// Credit MTS751st Deputy
    		/// </summary>        
    	//    [DisplayName("Credit MTS751st Deputy")]
            [MaxLength(10, ErrorMessage = "Credit MTS751st Deputy cannot be longer than 10 characters")]
    		public string  CreditMTS751stDeputy { get; set; }
    
    		    
    		/// <summary>
    		/// Credit MTS752nd
    		/// </summary>        
    	//    [DisplayName("Credit MTS752nd")]
            [MaxLength(10, ErrorMessage = "Credit MTS752nd cannot be longer than 10 characters")]
    		public string  CreditMTS752nd { get; set; }
    
    		    
    		/// <summary>
    		/// Credit MTS752nd Deputy
    		/// </summary>        
    	//    [DisplayName("Credit MTS752nd Deputy")]
            [MaxLength(10, ErrorMessage = "Credit MTS752nd Deputy cannot be longer than 10 characters")]
    		public string  CreditMTS752ndDeputy { get; set; }
    
    		    
    		/// <summary>
    		/// Credit TMS301st
    		/// </summary>        
    	//    [DisplayName("Credit TMS301st")]
            [MaxLength(10, ErrorMessage = "Credit TMS301st cannot be longer than 10 characters")]
    		public string  CreditTMS301st { get; set; }
    
    		    
    		/// <summary>
    		/// Credit TMS301st Deputy
    		/// </summary>        
    	//    [DisplayName("Credit TMS301st Deputy")]
            [MaxLength(10, ErrorMessage = "Credit TMS301st Deputy cannot be longer than 10 characters")]
    		public string  CreditTMS301stDeputy { get; set; }
    
    		    
    		/// <summary>
    		/// Credit TMS302nd
    		/// </summary>        
    	//    [DisplayName("Credit TMS302nd")]
            [MaxLength(10, ErrorMessage = "Credit TMS302nd cannot be longer than 10 characters")]
    		public string  CreditTMS302nd { get; set; }
    
    		    
    		/// <summary>
    		/// Credit TMS302nd Deputy
    		/// </summary>        
    	//    [DisplayName("Credit TMS302nd Deputy")]
            [MaxLength(10, ErrorMessage = "Credit TMS302nd Deputy cannot be longer than 10 characters")]
    		public string  CreditTMS302ndDeputy { get; set; }
    
    		    
    		/// <summary>
    		/// Credit TMS451st
    		/// </summary>        
    	//    [DisplayName("Credit TMS451st")]
            [MaxLength(10, ErrorMessage = "Credit TMS451st cannot be longer than 10 characters")]
    		public string  CreditTMS451st { get; set; }
    
    		    
    		/// <summary>
    		/// Credit TMS451st Deputy
    		/// </summary>        
    	//    [DisplayName("Credit TMS451st Deputy")]
            [MaxLength(10, ErrorMessage = "Credit TMS451st Deputy cannot be longer than 10 characters")]
    		public string  CreditTMS451stDeputy { get; set; }
    
    		    
    		/// <summary>
    		/// Credit TMS452nd
    		/// </summary>        
    	//    [DisplayName("Credit TMS452nd")]
            [MaxLength(10, ErrorMessage = "Credit TMS452nd cannot be longer than 10 characters")]
    		public string  CreditTMS452nd { get; set; }
    
    		    
    		/// <summary>
    		/// Credit TMS452nd Deputy
    		/// </summary>        
    	//    [DisplayName("Credit TMS452nd Deputy")]
            [MaxLength(10, ErrorMessage = "Credit TMS452nd Deputy cannot be longer than 10 characters")]
    		public string  CreditTMS452ndDeputy { get; set; }
    
    		    
    		/// <summary>
    		/// Credit TMS601st
    		/// </summary>        
    	//    [DisplayName("Credit TMS601st")]
            [MaxLength(10, ErrorMessage = "Credit TMS601st cannot be longer than 10 characters")]
    		public string  CreditTMS601st { get; set; }
    
    		    
    		/// <summary>
    		/// Credit TMS601st Deputy
    		/// </summary>        
    	//    [DisplayName("Credit TMS601st Deputy")]
            [MaxLength(10, ErrorMessage = "Credit TMS601st Deputy cannot be longer than 10 characters")]
    		public string  CreditTMS601stDeputy { get; set; }
    
    		    
    		/// <summary>
    		/// Credit TMS602nd
    		/// </summary>        
    	//    [DisplayName("Credit TMS602nd")]
            [MaxLength(10, ErrorMessage = "Credit TMS602nd cannot be longer than 10 characters")]
    		public string  CreditTMS602nd { get; set; }
    
    		    
    		/// <summary>
    		/// Credit TMS602nd Deputy
    		/// </summary>        
    	//    [DisplayName("Credit TMS602nd Deputy")]
            [MaxLength(10, ErrorMessage = "Credit TMS602nd Deputy cannot be longer than 10 characters")]
    		public string  CreditTMS602ndDeputy { get; set; }
    
    		    
    		/// <summary>
    		/// Credit TMS751st
    		/// </summary>        
    	//    [DisplayName("Credit TMS751st")]
            [MaxLength(10, ErrorMessage = "Credit TMS751st cannot be longer than 10 characters")]
    		public string  CreditTMS751st { get; set; }
    
    		    
    		/// <summary>
    		/// Credit TMS751st Deputy
    		/// </summary>        
    	//    [DisplayName("Credit TMS751st Deputy")]
            [MaxLength(10, ErrorMessage = "Credit TMS751st Deputy cannot be longer than 10 characters")]
    		public string  CreditTMS751stDeputy { get; set; }
    
    		    
    		/// <summary>
    		/// Credit TMS752nd
    		/// </summary>        
    	//    [DisplayName("Credit TMS752nd")]
            [MaxLength(10, ErrorMessage = "Credit TMS752nd cannot be longer than 10 characters")]
    		public string  CreditTMS752nd { get; set; }
    
    		    
    		/// <summary>
    		/// Credit TMS752nd Deputy
    		/// </summary>        
    	//    [DisplayName("Credit TMS752nd Deputy")]
            [MaxLength(10, ErrorMessage = "Credit TMS752nd Deputy cannot be longer than 10 characters")]
    		public string  CreditTMS752ndDeputy { get; set; }
    
    		    
    		/// <summary>
    		/// Updated User ID
    		/// </summary>        
    	//    [DisplayName("Updated User ID")]
            [MaxLength(10, ErrorMessage = "Updated User ID cannot be longer than 10 characters")]
    		public string  UpdatedUserID { get; set; }
    
    		    
    		/// <summary>
    		/// Updated Date
    		/// </summary>        
    	//    [DisplayName("Updated Date")]
    		public Nullable<System.DateTime>  UpdatedDate { get; set; }
    
    		    
    		/// <summary>
    		/// Credit Day
    		/// </summary>        
    	//    [DisplayName("Credit Day")]
    		public Nullable<int>  CreditDay { get; set; }
    
    		    
    		/// <summary>
    		/// Quote Lang
    		/// </summary>        
    	//    [DisplayName("Quote Lang")]
    		public Nullable<int>  QuoteLang { get; set; }
    
    		    
    	}
    //}
    
}
