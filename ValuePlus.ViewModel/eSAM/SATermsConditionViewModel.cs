using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ValuePlus.ViewModel;



namespace ValuePlus.ViewModel.eSAM
{
    
    /// <summary>
    /// SATermsCondition class
    /// </summary>
    //[MetadataType(typeof(SATermsConditionViewModel))]
    //public  partial class SATermsCondition
    //{
    
    	/// <summary>
    	/// SATermsCondition Metadata class
    	/// </summary>
    	public   class SATermsConditionViewModel:ViewModelBase
    	{
    		    
    		/// <summary>
    		/// ID
    		/// </summary>        
    	//    [DisplayName("ID")]
            [Required(ErrorMessage = "ID is required")]
    		public int  ID { get; set; }
    
    		    
    		/// <summary>
    		/// Term Order
    		/// </summary>        
    	//    [DisplayName("Term Order")]
    		public Nullable<int>  TermOrder { get; set; }
    
    		    
    		/// <summary>
    		/// Is Selected
    		/// </summary>        
    	//    [DisplayName("Is Selected")]
            [MaxLength(10, ErrorMessage = "Is Selected cannot be longer than 10 characters")]
    		public string  IsSelected { get; set; }
    
    		    
    		/// <summary>
    		/// Contents
    		/// </summary>        
    	//    [DisplayName("Contents")]
    		public string  Contents { get; set; }
    
    		    
    		/// <summary>
    		/// Dept Type
    		/// </summary>        
    	//    [DisplayName("Dept Type")]
            [MaxLength(10, ErrorMessage = "Dept Type cannot be longer than 10 characters")]
    		public string  DeptType { get; set; }
    
    		    
    		/// <summary>
    		/// Frm ID
    		/// </summary>        
    	//    [DisplayName("Frm ID")]
            [MaxLength(10, ErrorMessage = "Frm ID cannot be longer than 10 characters")]
    		public string  FrmID { get; set; }
    
    		    
    		/// <summary>
    		/// Created Date
    		/// </summary>        
    	//    [DisplayName("Created Date")]
    		public Nullable<System.DateTime>  CreatedDate { get; set; }
    
    		    
    		/// <summary>
    		/// Created User ID
    		/// </summary>        
    	//    [DisplayName("Created User ID")]
            [MaxLength(10, ErrorMessage = "Created User ID cannot be longer than 10 characters")]
    		public string  CreatedUserID { get; set; }
    
    		    
    		/// <summary>
    		/// Updated User ID
    		/// </summary>        
    	//    [DisplayName("Updated User ID")]
            [MaxLength(10, ErrorMessage = "Updated User ID cannot be longer than 10 characters")]
    		public string  UpdatedUserID { get; set; }
    
    		    
    		/// <summary>
    		/// Updated Date
    		/// </summary>        
    	//    [DisplayName("Updated Date")]
    		public Nullable<System.DateTime>  UpdatedDate { get; set; }
    
    		    
    	}
    //}
    
}
