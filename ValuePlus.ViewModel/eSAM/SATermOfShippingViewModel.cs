using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ValuePlus.ViewModel;



namespace ValuePlus.ViewModel.eSAM
{
    
    /// <summary>
    /// SATermOfShipping class
    /// </summary>
    //[MetadataType(typeof(SATermOfShippingViewModel))]
    //public  partial class SATermOfShipping
    //{
    
    	/// <summary>
    	/// SATermOfShipping Metadata class
    	/// </summary>
    	public   class SATermOfShippingViewModel:ViewModelBase
    	{
    		    
    		/// <summary>
    		/// ID
    		/// </summary>        
    	//    [DisplayName("ID")]
            [Required(ErrorMessage = "ID is required")]
    		public int  ID { get; set; }
    
    		    
    		/// <summary>
    		/// Quote ID
    		/// </summary>        
    	//    [DisplayName("Quote ID")]
            [Required(ErrorMessage = "Quote ID is required")]
    		public int  QuoteID { get; set; }
    
    		    
    		/// <summary>
    		/// Commo Dity
    		/// </summary>        
    	//    [DisplayName("Commo Dity")]
    		public string  CommoDity { get; set; }
    
    		    
    		/// <summary>
    		/// Is Prepared
    		/// </summary>        
    	//    [DisplayName("Is Prepared")]
    		public Nullable<bool>  IsPrepared { get; set; }
    
    		    
    		/// <summary>
    		/// Is Collect
    		/// </summary>        
    	//    [DisplayName("Is Collect")]
    		public Nullable<bool>  IsCollect { get; set; }
    
    		    
    		/// <summary>
    		/// Ori Trans Mode
    		/// </summary>        
    	//    [DisplayName("Ori Trans Mode")]
            [MaxLength(10, ErrorMessage = "Ori Trans Mode cannot be longer than 10 characters")]
    		public string  OriTransMode { get; set; }
    
    		    
    		/// <summary>
    		/// Dest Trans Mode
    		/// </summary>        
    	//    [DisplayName("Dest Trans Mode")]
            [MaxLength(10, ErrorMessage = "Dest Trans Mode cannot be longer than 10 characters")]
    		public string  DestTransMode { get; set; }
    
    		    
    		/// <summary>
    		/// Ori Ship Term
    		/// </summary>        
    	//    [DisplayName("Ori Ship Term")]
            [MaxLength(10, ErrorMessage = "Ori Ship Term cannot be longer than 10 characters")]
    		public string  OriShipTerm { get; set; }
    
    		    
    		/// <summary>
    		/// Dest Ship Term
    		/// </summary>        
    	//    [DisplayName("Dest Ship Term")]
            [MaxLength(10, ErrorMessage = "Dest Ship Term cannot be longer than 10 characters")]
    		public string  DestShipTerm { get; set; }
    
    		    
    		/// <summary>
    		/// Is Ori Cust Clearance
    		/// </summary>        
    	//    [DisplayName("Is Ori Cust Clearance")]
    		public Nullable<bool>  IsOriCustClearance { get; set; }
    
    		    
    		/// <summary>
    		/// Is Dest Cust Clearance
    		/// </summary>        
    	//    [DisplayName("Is Dest Cust Clearance")]
    		public Nullable<bool>  IsDestCustClearance { get; set; }
    
    		    
    		/// <summary>
    		/// Is Ori Adv Duty
    		/// </summary>        
    	//    [DisplayName("Is Ori Adv Duty")]
    		public Nullable<bool>  IsOriAdvDuty { get; set; }
    
    		    
    		/// <summary>
    		/// Is Dest Adv Duty
    		/// </summary>        
    	//    [DisplayName("Is Dest Adv Duty")]
    		public Nullable<bool>  IsDestAdvDuty { get; set; }
    
    		    
    		/// <summary>
    		/// Created User ID
    		/// </summary>        
    	//    [DisplayName("Created User ID")]
            [MaxLength(6, ErrorMessage = "Created User ID cannot be longer than 6 characters")]
    		public string  CreatedUserID { get; set; }
    
    		    
    		/// <summary>
    		/// Created Date
    		/// </summary>        
    	//    [DisplayName("Created Date")]
    		public Nullable<System.DateTime>  CreatedDate { get; set; }
    
    		    
    		/// <summary>
    		/// Updated User ID
    		/// </summary>        
    	//    [DisplayName("Updated User ID")]
            [MaxLength(6, ErrorMessage = "Updated User ID cannot be longer than 6 characters")]
    		public string  UpdatedUserID { get; set; }
    
    		    
    		/// <summary>
    		/// Updated Date
    		/// </summary>        
    	//    [DisplayName("Updated Date")]
    		public Nullable<System.DateTime>  UpdatedDate { get; set; }
    
    		    
    	}
    //}
    
}
