using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ValuePlus.ViewModel;



namespace ValuePlus.ViewModel.eSAM
{
    
    /// <summary>
    /// SASignBook class
    /// </summary>
    //[MetadataType(typeof(SASignBookViewModel))]
    //public  partial class SASignBook
    //{
    
    	/// <summary>
    	/// SASignBook Metadata class
    	/// </summary>
    	public   class SASignBookViewModel:ViewModelBase
    	{
    		    
    		/// <summary>
    		/// id
    		/// </summary>        
    	//    [DisplayName("id")]
            [Required(ErrorMessage = "id is required")]
    		public int  id { get; set; }
    
    		    
    		/// <summary>
    		/// User ID
    		/// </summary>        
    	//    [DisplayName("User ID")]
            [Required(ErrorMessage = "User ID is required")]
            [MaxLength(20, ErrorMessage = "User ID cannot be longer than 20 characters")]
    		public string  UserID { get; set; }
    
    		    
    		/// <summary>
    		/// Updated Date
    		/// </summary>        
    	//    [DisplayName("Updated Date")]
    		public Nullable<System.DateTime>  UpdatedDate { get; set; }
    
    		    
    	}
    //}
    
}
