using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ValuePlus.ViewModel;



namespace ValuePlus.ViewModel.eSAM
{
    
    /// <summary>
    /// SAUserList class
    /// </summary>
    //[MetadataType(typeof(SAUserListViewModel))]
    //public  partial class SAUserList
    //{
    
    	/// <summary>
    	/// SAUserList Metadata class
    	/// </summary>
    	public   class SAUserListViewModel:ViewModelBase
    	{
    		    
    		/// <summary>
    		/// ID
    		/// </summary>        
    	//    [DisplayName("ID")]
            [Required(ErrorMessage = "ID is required")]
    		public int  ID { get; set; }
    
    		    
    		/// <summary>
    		/// User ID
    		/// </summary>        
    	//    [DisplayName("User ID")]
            [Required(ErrorMessage = "User ID is required")]
            [MaxLength(10, ErrorMessage = "User ID cannot be longer than 10 characters")]
    		public string  UserID { get; set; }
    
    		    
    		/// <summary>
    		/// User Name
    		/// </summary>        
    	//    [DisplayName("User Name")]
            [MaxLength(50, ErrorMessage = "User Name cannot be longer than 50 characters")]
    		public string  UserName { get; set; }
    
    		    
    		/// <summary>
    		/// Station Code
    		/// </summary>        
    	//    [DisplayName("Station Code")]
            [MaxLength(50, ErrorMessage = "Station Code cannot be longer than 50 characters")]
    		public string  StationCode { get; set; }
    
    		    
    		/// <summary>
    		/// Sales Level
    		/// </summary>        
    	//    [DisplayName("Sales Level")]
            [MaxLength(5, ErrorMessage = "Sales Level cannot be longer than 5 characters")]
    		public string  SalesLevel { get; set; }
    
    		    
    		/// <summary>
    		/// Calling Card
    		/// </summary>        
    	//    [DisplayName("Calling Card")]
    		public string  CallingCard { get; set; }
    
    		    
    		/// <summary>
    		/// Mail
    		/// </summary>        
    	//    [DisplayName("Mail")]
            [MaxLength(100, ErrorMessage = "Mail cannot be longer than 100 characters")]
    		public string  Mail { get; set; }
    
    		    
    		/// <summary>
    		/// Phone
    		/// </summary>        
    	//    [DisplayName("Phone")]
            [MaxLength(20, ErrorMessage = "Phone cannot be longer than 20 characters")]
    		public string  Phone { get; set; }
    
    		    
    		/// <summary>
    		/// Ext
    		/// </summary>        
    	//    [DisplayName("Ext")]
            [MaxLength(10, ErrorMessage = "Ext cannot be longer than 10 characters")]
    		public string  Ext { get; set; }
    
    		    
    		/// <summary>
    		/// CPhone
    		/// </summary>        
    	//    [DisplayName("CPhone")]
            [MaxLength(20, ErrorMessage = "CPhone cannot be longer than 20 characters")]
    		public string  CPhone { get; set; }
    
    		    
    		/// <summary>
    		/// Job Title
    		/// </summary>        
    	//    [DisplayName("Job Title")]
            [MaxLength(20, ErrorMessage = "Job Title cannot be longer than 20 characters")]
    		public string  JobTitle { get; set; }
    
    		    
    		/// <summary>
    		/// Status
    		/// </summary>        
    	//    [DisplayName("Status")]
            [MaxLength(10, ErrorMessage = "Status cannot be longer than 10 characters")]
    		public string  Status { get; set; }
    
    		    
    		/// <summary>
    		/// Except Access
    		/// </summary>        
    	//    [DisplayName("Except Access")]
            [MaxLength(5, ErrorMessage = "Except Access cannot be longer than 5 characters")]
    		public string  ExceptAccess { get; set; }
    
    		    
    		/// <summary>
    		/// Created User ID
    		/// </summary>        
    	//    [DisplayName("Created User ID")]
            [MaxLength(10, ErrorMessage = "Created User ID cannot be longer than 10 characters")]
    		public string  CreatedUserID { get; set; }
    
    		    
    		/// <summary>
    		/// Created Date
    		/// </summary>        
    	//    [DisplayName("Created Date")]
    		public Nullable<System.DateTime>  CreatedDate { get; set; }
    
    		    
    		/// <summary>
    		/// Update User ID
    		/// </summary>        
    	//    [DisplayName("Update User ID")]
            [MaxLength(10, ErrorMessage = "Update User ID cannot be longer than 10 characters")]
    		public string  UpdateUserID { get; set; }
    
    		    
    		/// <summary>
    		/// Update Date
    		/// </summary>        
    	//    [DisplayName("Update Date")]
    		public Nullable<System.DateTime>  UpdateDate { get; set; }
    
    		    
    		/// <summary>
    		/// Station Type
    		/// </summary>        
    	//    [DisplayName("Station Type")]
            [MaxLength(10, ErrorMessage = "Station Type cannot be longer than 10 characters")]
    		public string  StationType { get; set; }
    
    		    
    		/// <summary>
    		/// Deputy
    		/// </summary>        
    	//    [DisplayName("Deputy")]
            [MaxLength(10, ErrorMessage = "Deputy cannot be longer than 10 characters")]
    		public string  Deputy { get; set; }
    
    		    
    		/// <summary>
    		/// Station ID
    		/// </summary>        
    	//    [DisplayName("Station ID")]
            [MaxLength(10, ErrorMessage = "Station ID cannot be longer than 10 characters")]
    		public string  StationID { get; set; }
    
    		    
    		/// <summary>
    		/// PL1
    		/// </summary>        
    	//    [DisplayName("PL1")]
    		public Nullable<bool>  PL1 { get; set; }
    
    		    
    		/// <summary>
    		/// PL2
    		/// </summary>        
    	//    [DisplayName("PL2")]
    		public Nullable<bool>  PL2 { get; set; }
    
    		    
    		/// <summary>
    		/// PL3
    		/// </summary>        
    	//    [DisplayName("PL3")]
    		public Nullable<bool>  PL3 { get; set; }
    
    		    
    		/// <summary>
    		/// PL4
    		/// </summary>        
    	//    [DisplayName("PL4")]
    		public Nullable<bool>  PL4 { get; set; }
    
    		    
    		/// <summary>
    		/// PL5
    		/// </summary>        
    	//    [DisplayName("PL5")]
    		public Nullable<bool>  PL5 { get; set; }
    
    		    
    		/// <summary>
    		/// PL6
    		/// </summary>        
    	//    [DisplayName("PL6")]
    		public Nullable<bool>  PL6 { get; set; }
    
    		    
    		/// <summary>
    		/// PL7
    		/// </summary>        
    	//    [DisplayName("PL7")]
    		public Nullable<bool>  PL7 { get; set; }
    
    		    
    		/// <summary>
    		/// PL8
    		/// </summary>        
    	//    [DisplayName("PL8")]
    		public Nullable<bool>  PL8 { get; set; }
    
    		    
    		/// <summary>
    		/// PL9
    		/// </summary>        
    	//    [DisplayName("PL9")]
    		public Nullable<bool>  PL9 { get; set; }
    
    		    
    		/// <summary>
    		/// PL10
    		/// </summary>        
    	//    [DisplayName("PL10")]
    		public Nullable<bool>  PL10 { get; set; }
    
    		    
    		/// <summary>
    		/// PL11
    		/// </summary>        
    	//    [DisplayName("PL11")]
    		public Nullable<bool>  PL11 { get; set; }
    
    		    
    		/// <summary>
    		/// PL12
    		/// </summary>        
    	//    [DisplayName("PL12")]
    		public Nullable<bool>  PL12 { get; set; }
    
    		    
    		/// <summary>
    		/// City Order
    		/// </summary>        
    	//    [DisplayName("City Order")]
            [MaxLength(10, ErrorMessage = "City Order cannot be longer than 10 characters")]
    		public string  CityOrder { get; set; }
    
    		    
    		/// <summary>
    		/// Excel Quote
    		/// </summary>        
    	//    [DisplayName("Excel Quote")]
            [Required(ErrorMessage = "Excel Quote is required")]
            [MaxLength(1, ErrorMessage = "Excel Quote cannot be longer than 1 characters")]
    		public string  ExcelQuote { get; set; }
    
    		    
    	}
    //}
    
}
