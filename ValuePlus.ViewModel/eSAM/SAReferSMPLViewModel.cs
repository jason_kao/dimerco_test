using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ValuePlus.ViewModel;



namespace ValuePlus.ViewModel.eSAM
{
    
    /// <summary>
    /// SAReferSMPL class
    /// </summary>
    //[MetadataType(typeof(SAReferSMPLViewModel))]
    //public  partial class SAReferSMPL
    //{
    
    	/// <summary>
    	/// SAReferSMPL Metadata class
    	/// </summary>
    	public   class SAReferSMPLViewModel:ViewModelBase
    	{
    		    
    		/// <summary>
    		/// SMHQID
    		/// </summary>        
    	//    [DisplayName("SMHQID")]
            [Required(ErrorMessage = "SMHQID is required")]
    		public int  SMHQID { get; set; }
    
    		    
    		/// <summary>
    		/// Description
    		/// </summary>        
    	//    [DisplayName("Description")]
            [MaxLength(20, ErrorMessage = "Description cannot be longer than 20 characters")]
    		public string  Description { get; set; }
    
    		    
    		/// <summary>
    		/// Status
    		/// </summary>        
    	//    [DisplayName("Status")]
            [MaxLength(10, ErrorMessage = "Status cannot be longer than 10 characters")]
    		public string  Status { get; set; }
    
    		    
    	}
    //}
    
}
