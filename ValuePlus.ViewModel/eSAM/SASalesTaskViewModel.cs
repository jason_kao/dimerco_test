using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ValuePlus.ViewModel;



namespace ValuePlus.ViewModel.eSAM
{
    
    /// <summary>
    /// SASalesTask class
    /// </summary>
    //[MetadataType(typeof(SASalesTaskViewModel))]
    //public  partial class SASalesTask
    //{
    
    	/// <summary>
    	/// SASalesTask Metadata class
    	/// </summary>
    	public   class SASalesTaskViewModel:ViewModelBase
    	{
    		    
    		/// <summary>
    		/// ID
    		/// </summary>        
    	//    [DisplayName("ID")]
            [Required(ErrorMessage = "ID is required")]
    		public int  ID { get; set; }
    
    		    
    		/// <summary>
    		/// LID
    		/// </summary>        
    	//    [DisplayName("LID")]
    		public Nullable<int>  LID { get; set; }
    
    		    
    		/// <summary>
    		/// Subject
    		/// </summary>        
    	//    [DisplayName("Subject")]
            [MaxLength(255, ErrorMessage = "Subject cannot be longer than 255 characters")]
    		public string  Subject { get; set; }
    
    		    
    		/// <summary>
    		/// Assign To
    		/// </summary>        
    	//    [DisplayName("Assign To")]
            [MaxLength(50, ErrorMessage = "Assign To cannot be longer than 50 characters")]
    		public string  AssignTo { get; set; }
    
    		    
    		/// <summary>
    		/// Attendees
    		/// </summary>        
    	//    [DisplayName("Attendees")]
            [MaxLength(255, ErrorMessage = "Attendees cannot be longer than 255 characters")]
    		public string  Attendees { get; set; }
    
    		    
    		/// <summary>
    		/// Contact
    		/// </summary>        
    	//    [DisplayName("Contact")]
            [MaxLength(50, ErrorMessage = "Contact cannot be longer than 50 characters")]
    		public string  Contact { get; set; }
    
    		    
    		/// <summary>
    		/// Sales Task Info
    		/// </summary>        
    	//    [DisplayName("Sales Task Info")]
    		public string  SalesTaskInfo { get; set; }
    
    		    
    		/// <summary>
    		/// Remarks
    		/// </summary>        
    	//    [DisplayName("Remarks")]
            [MaxLength(255, ErrorMessage = "Remarks cannot be longer than 255 characters")]
    		public string  Remarks { get; set; }
    
    		    
    		/// <summary>
    		/// Start Date
    		/// </summary>        
    	//    [DisplayName("Start Date")]
    		public Nullable<System.DateTime>  StartDate { get; set; }
    
    		    
    		/// <summary>
    		/// End Date
    		/// </summary>        
    	//    [DisplayName("End Date")]
    		public Nullable<System.DateTime>  EndDate { get; set; }
    
    		    
    		/// <summary>
    		/// Due Date
    		/// </summary>        
    	//    [DisplayName("Due Date")]
    		public Nullable<System.DateTime>  DueDate { get; set; }
    
    		    
    		/// <summary>
    		/// Status
    		/// </summary>        
    	//    [DisplayName("Status")]
            [MaxLength(10, ErrorMessage = "Status cannot be longer than 10 characters")]
    		public string  Status { get; set; }
    
    		    
    		/// <summary>
    		/// Location City
    		/// </summary>        
    	//    [DisplayName("Location City")]
            [MaxLength(50, ErrorMessage = "Location City cannot be longer than 50 characters")]
    		public string  LocationCity { get; set; }
    
    		    
    		/// <summary>
    		/// Location Address
    		/// </summary>        
    	//    [DisplayName("Location Address")]
            [MaxLength(255, ErrorMessage = "Location Address cannot be longer than 255 characters")]
    		public string  LocationAddress { get; set; }
    
    		    
    		/// <summary>
    		/// Log Type
    		/// </summary>        
    	//    [DisplayName("Log Type")]
            [MaxLength(50, ErrorMessage = "Log Type cannot be longer than 50 characters")]
    		public string  LogType { get; set; }
    
    		    
    		/// <summary>
    		/// Priority
    		/// </summary>        
    	//    [DisplayName("Priority")]
            [MaxLength(10, ErrorMessage = "Priority cannot be longer than 10 characters")]
    		public string  Priority { get; set; }
    
    		    
    		/// <summary>
    		/// Task Owner ID
    		/// </summary>        
    	//    [DisplayName("Task Owner ID")]
            [MaxLength(10, ErrorMessage = "Task Owner ID cannot be longer than 10 characters")]
    		public string  TaskOwnerID { get; set; }
    
    		    
    		/// <summary>
    		/// All Day Event
    		/// </summary>        
    	//    [DisplayName("All Day Event")]
    		public Nullable<bool>  AllDayEvent { get; set; }
    
    		    
    		/// <summary>
    		/// Attendee Contact
    		/// </summary>        
    	//    [DisplayName("Attendee Contact")]
            [MaxLength(50, ErrorMessage = "Attendee Contact cannot be longer than 50 characters")]
    		public string  AttendeeContact { get; set; }
    
    		    
    		/// <summary>
    		/// Created User ID
    		/// </summary>        
    	//    [DisplayName("Created User ID")]
            [MaxLength(10, ErrorMessage = "Created User ID cannot be longer than 10 characters")]
    		public string  CreatedUserID { get; set; }
    
    		    
    		/// <summary>
    		/// Created Date
    		/// </summary>        
    	//    [DisplayName("Created Date")]
    		public Nullable<System.DateTime>  CreatedDate { get; set; }
    
    		    
    		/// <summary>
    		/// Updated User ID
    		/// </summary>        
    	//    [DisplayName("Updated User ID")]
            [MaxLength(10, ErrorMessage = "Updated User ID cannot be longer than 10 characters")]
    		public string  UpdatedUserID { get; set; }
    
    		    
    		/// <summary>
    		/// Updated Date
    		/// </summary>        
    	//    [DisplayName("Updated Date")]
    		public Nullable<System.DateTime>  UpdatedDate { get; set; }
    
    		    
    		/// <summary>
    		/// Subject Type
    		/// </summary>        
    	//    [DisplayName("Subject Type")]
            [MaxLength(40, ErrorMessage = "Subject Type cannot be longer than 40 characters")]
    		public string  SubjectType { get; set; }
    
    		    
    		/// <summary>
    		/// is Del
    		/// </summary>        
    	//    [DisplayName("is Del")]
    		public Nullable<bool>  isDel { get; set; }
    
    		    
    		/// <summary>
    		/// notice Index
    		/// </summary>        
    	//    [DisplayName("notice Index")]
    		public Nullable<int>  noticeIndex { get; set; }
    
    		    
    		/// <summary>
    		/// notice Time
    		/// </summary>        
    	//    [DisplayName("notice Time")]
    		public Nullable<System.DateTime>  noticeTime { get; set; }
    
    		    
    		/// <summary>
    		/// Log Type ID
    		/// </summary>        
    	//    [DisplayName("Log Type ID")]
    		public Nullable<int>  LogTypeID { get; set; }
    
    		    
    		/// <summary>
    		/// Subject Type ID
    		/// </summary>        
    	//    [DisplayName("Subject Type ID")]
    		public Nullable<int>  SubjectTypeID { get; set; }
    
    		    
    	}
    //}
    
}
