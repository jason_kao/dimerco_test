using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ValuePlus.ViewModel;



namespace ValuePlus.ViewModel.eSAM
{
    
    /// <summary>
    /// SAStationRelated class
    /// </summary>
    //[MetadataType(typeof(SAStationRelatedViewModel))]
    //public  partial class SAStationRelated
    //{
    
    	/// <summary>
    	/// SAStationRelated Metadata class
    	/// </summary>
    	public   class SAStationRelatedViewModel:ViewModelBase
    	{
    		    
    		/// <summary>
    		/// ID
    		/// </summary>        
    	//    [DisplayName("ID")]
            [Required(ErrorMessage = "ID is required")]
    		public int  ID { get; set; }
    
    		    
    		/// <summary>
    		/// PStation
    		/// </summary>        
    	//    [DisplayName("PStation")]
            [MaxLength(10, ErrorMessage = "PStation cannot be longer than 10 characters")]
    		public string  PStation { get; set; }
    
    		    
    		/// <summary>
    		/// Station Code
    		/// </summary>        
    	//    [DisplayName("Station Code")]
            [MaxLength(10, ErrorMessage = "Station Code cannot be longer than 10 characters")]
    		public string  StationCode { get; set; }
    
    		    
    		/// <summary>
    		/// CStation
    		/// </summary>        
    	//    [DisplayName("CStation")]
            [MaxLength(10, ErrorMessage = "CStation cannot be longer than 10 characters")]
    		public string  CStation { get; set; }
    
    		    
    		/// <summary>
    		/// Boss Station Code
    		/// </summary>        
    	//    [DisplayName("Boss Station Code")]
            [MaxLength(10, ErrorMessage = "Boss Station Code cannot be longer than 10 characters")]
    		public string  BossStationCode { get; set; }
    
    		    
    		/// <summary>
    		/// Status
    		/// </summary>        
    	//    [DisplayName("Status")]
            [MaxLength(10, ErrorMessage = "Status cannot be longer than 10 characters")]
    		public string  Status { get; set; }
    
    		    
    	}
    //}
    
}
