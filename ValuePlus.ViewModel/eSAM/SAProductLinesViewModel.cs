using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ValuePlus.ViewModel;



namespace ValuePlus.ViewModel.eSAM
{
    
    /// <summary>
    /// SAProductLines class
    /// </summary>
    //[MetadataType(typeof(SAProductLinesViewModel))]
    //public  partial class SAProductLines
    //{
    
    	/// <summary>
    	/// SAProductLines Metadata class
    	/// </summary>
    	public   class SAProductLinesViewModel:ViewModelBase
    	{
    		    
    		/// <summary>
    		/// ID
    		/// </summary>        
    	//    [DisplayName("ID")]
            [Required(ErrorMessage = "ID is required")]
    		public int  ID { get; set; }
    
    		    
    		/// <summary>
    		/// LID
    		/// </summary>        
    	//    [DisplayName("LID")]
            [Required(ErrorMessage = "LID is required")]
    		public int  LID { get; set; }
    
    		    
    		/// <summary>
    		/// PID
    		/// </summary>        
    	//    [DisplayName("PID")]
            [Required(ErrorMessage = "PID is required")]
    		public int  PID { get; set; }
    
    		    
    		/// <summary>
    		/// Status
    		/// </summary>        
    	//    [DisplayName("Status")]
            [MaxLength(10, ErrorMessage = "Status cannot be longer than 10 characters")]
    		public string  Status { get; set; }
    
    		    
    		/// <summary>
    		/// Created Station ID
    		/// </summary>        
    	//    [DisplayName("Created Station ID")]
            [MaxLength(3, ErrorMessage = "Created Station ID cannot be longer than 3 characters")]
    		public string  CreatedStationID { get; set; }
    
    		    
    		/// <summary>
    		/// Created User ID
    		/// </summary>        
    	//    [DisplayName("Created User ID")]
            [MaxLength(10, ErrorMessage = "Created User ID cannot be longer than 10 characters")]
    		public string  CreatedUserID { get; set; }
    
    		    
    		/// <summary>
    		/// Owner Station ID
    		/// </summary>        
    	//    [DisplayName("Owner Station ID")]
            [MaxLength(3, ErrorMessage = "Owner Station ID cannot be longer than 3 characters")]
    		public string  OwnerStationID { get; set; }
    
    		    
    		/// <summary>
    		/// Owner User ID
    		/// </summary>        
    	//    [DisplayName("Owner User ID")]
            [MaxLength(10, ErrorMessage = "Owner User ID cannot be longer than 10 characters")]
    		public string  OwnerUserID { get; set; }
    
    		    
    		/// <summary>
    		/// Updated User ID
    		/// </summary>        
    	//    [DisplayName("Updated User ID")]
            [MaxLength(10, ErrorMessage = "Updated User ID cannot be longer than 10 characters")]
    		public string  UpdatedUserID { get; set; }
    
    		    
    		/// <summary>
    		/// Updated Date
    		/// </summary>        
    	//    [DisplayName("Updated Date")]
    		public Nullable<System.DateTime>  UpdatedDate { get; set; }
    
    		    
    		/// <summary>
    		/// Created Date
    		/// </summary>        
    	//    [DisplayName("Created Date")]
    		public Nullable<System.DateTime>  CreatedDate { get; set; }
    
    		    
    		/// <summary>
    		/// Secured
    		/// </summary>        
    	//    [DisplayName("Secured")]
            [Required(ErrorMessage = "Secured is required")]
            [MaxLength(1, ErrorMessage = "Secured cannot be longer than 1 characters")]
    		public string  Secured { get; set; }
    
    		    
    		/// <summary>
    		/// FLead Owner
    		/// </summary>        
    	//    [DisplayName("FLead Owner")]
            [MaxLength(6, ErrorMessage = "FLead Owner cannot be longer than 6 characters")]
    		public string  FLeadOwner { get; set; }
    
    		    
    		/// <summary>
    		/// FSecured DT
    		/// </summary>        
    	//    [DisplayName("FSecured DT")]
    		public Nullable<System.DateTime>  FSecuredDT { get; set; }
    
    		    
    		/// <summary>
    		/// Key Account
    		/// </summary>        
    	//    [DisplayName("Key Account")]
            [MaxLength(1, ErrorMessage = "Key Account cannot be longer than 1 characters")]
    		public string  KeyAccount { get; set; }
    
    		    
    	}
    //}
    
}
