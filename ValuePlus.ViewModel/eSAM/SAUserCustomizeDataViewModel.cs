using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ValuePlus.ViewModel;



namespace ValuePlus.ViewModel.eSAM
{
    
    /// <summary>
    /// SAUserCustomizeData class
    /// </summary>
    //[MetadataType(typeof(SAUserCustomizeDataViewModel))]
    //public  partial class SAUserCustomizeData
    //{
    
    	/// <summary>
    	/// SAUserCustomizeData Metadata class
    	/// </summary>
    	public   class SAUserCustomizeDataViewModel:ViewModelBase
    	{
    		    
    		/// <summary>
    		/// ID
    		/// </summary>        
    	//    [DisplayName("ID")]
            [Required(ErrorMessage = "ID is required")]
    		public int  ID { get; set; }
    
    		    
    		/// <summary>
    		/// User ID
    		/// </summary>        
    	//    [DisplayName("User ID")]
            [Required(ErrorMessage = "User ID is required")]
            [MaxLength(6, ErrorMessage = "User ID cannot be longer than 6 characters")]
    		public string  UserID { get; set; }
    
    		    
    		/// <summary>
    		/// Data Type
    		/// </summary>        
    	//    [DisplayName("Data Type")]
            [Required(ErrorMessage = "Data Type is required")]
    		public int  DataType { get; set; }
    
    		    
    		/// <summary>
    		/// Data Value
    		/// </summary>        
    	//    [DisplayName("Data Value")]
            [Required(ErrorMessage = "Data Value is required")]
            [MaxLength(50, ErrorMessage = "Data Value cannot be longer than 50 characters")]
    		public string  DataValue { get; set; }
    
    		    
    		/// <summary>
    		/// Updated By
    		/// </summary>        
    	//    [DisplayName("Updated By")]
            [Required(ErrorMessage = "Updated By is required")]
            [MaxLength(10, ErrorMessage = "Updated By cannot be longer than 10 characters")]
    		public string  UpdatedBy { get; set; }
    
    		    
    		/// <summary>
    		/// Updated Date
    		/// </summary>        
    	//    [DisplayName("Updated Date")]
            [Required(ErrorMessage = "Updated Date is required")]
    		public System.DateTime  UpdatedDate { get; set; }
    
    		    
    	}
    //}
    
}
