using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ValuePlus.ViewModel;



namespace ValuePlus.ViewModel.eSAM
{
    
    /// <summary>
    /// SAContactReportTo class
    /// </summary>
    //[MetadataType(typeof(SAContactReportToViewModel))]
    //public  partial class SAContactReportTo
    //{
    
    	/// <summary>
    	/// SAContactReportTo Metadata class
    	/// </summary>
    	public   class SAContactReportToViewModel:ViewModelBase
    	{
    		    
    		/// <summary>
    		/// Contact ID
    		/// </summary>        
    	//    [DisplayName("Contact ID")]
            [Required(ErrorMessage = "Contact ID is required")]
    		public int  ContactID { get; set; }
    
    		    
    		/// <summary>
    		/// Boss Contact ID
    		/// </summary>        
    	//    [DisplayName("Boss Contact ID")]
    		public Nullable<int>  BossContactID { get; set; }
    
    		    
    		/// <summary>
    		/// Created User ID
    		/// </summary>        
    	//    [DisplayName("Created User ID")]
            [MaxLength(10, ErrorMessage = "Created User ID cannot be longer than 10 characters")]
    		public string  CreatedUserID { get; set; }
    
    		    
    		/// <summary>
    		/// Created Date
    		/// </summary>        
    	//    [DisplayName("Created Date")]
    		public Nullable<System.DateTime>  CreatedDate { get; set; }
    
    		    
    	}
    //}
    
}
