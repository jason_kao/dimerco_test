using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ValuePlus.ViewModel;



namespace ValuePlus.ViewModel.eSAM
{
    
    /// <summary>
    /// SAMergeCheckDetail class
    /// </summary>
    //[MetadataType(typeof(SAMergeCheckDetailViewModel))]
    //public  partial class SAMergeCheckDetail
    //{
    
    	/// <summary>
    	/// SAMergeCheckDetail Metadata class
    	/// </summary>
    	public   class SAMergeCheckDetailViewModel:ViewModelBase
    	{
    		    
    		/// <summary>
    		/// ID
    		/// </summary>        
    	//    [DisplayName("ID")]
            [Required(ErrorMessage = "ID is required")]
    		public int  ID { get; set; }
    
    		    
    		/// <summary>
    		/// tocustomerid
    		/// </summary>        
    	//    [DisplayName("tocustomerid")]
    		public Nullable<int>  tocustomerid { get; set; }
    
    		    
    		/// <summary>
    		/// disposecustid
    		/// </summary>        
    	//    [DisplayName("disposecustid")]
    		public Nullable<int>  disposecustid { get; set; }
    
    		    
    		/// <summary>
    		/// stationcode
    		/// </summary>        
    	//    [DisplayName("stationcode")]
            [Required(ErrorMessage = "stationcode is required")]
            [MaxLength(6, ErrorMessage = "stationcode cannot be longer than 6 characters")]
    		public string  stationcode { get; set; }
    
    		    
    		/// <summary>
    		/// Station IP
    		/// </summary>        
    	//    [DisplayName("Station IP")]
            [MaxLength(50, ErrorMessage = "Station IP cannot be longer than 50 characters")]
    		public string  StationIP { get; set; }
    
    		    
    		/// <summary>
    		/// DBName
    		/// </summary>        
    	//    [DisplayName("DBName")]
            [MaxLength(50, ErrorMessage = "DBName cannot be longer than 50 characters")]
    		public string  DBName { get; set; }
    
    		    
    		/// <summary>
    		/// Function Name
    		/// </summary>        
    	//    [DisplayName("Function Name")]
            [MaxLength(50, ErrorMessage = "Function Name cannot be longer than 50 characters")]
    		public string  FunctionName { get; set; }
    
    		    
    		/// <summary>
    		/// Table Name
    		/// </summary>        
    	//    [DisplayName("Table Name")]
            [MaxLength(50, ErrorMessage = "Table Name cannot be longer than 50 characters")]
    		public string  TableName { get; set; }
    
    		    
    		/// <summary>
    		/// Column Name
    		/// </summary>        
    	//    [DisplayName("Column Name")]
            [MaxLength(50, ErrorMessage = "Column Name cannot be longer than 50 characters")]
    		public string  ColumnName { get; set; }
    
    		    
    		/// <summary>
    		/// Module
    		/// </summary>        
    	//    [DisplayName("Module")]
            [MaxLength(50, ErrorMessage = "Module cannot be longer than 50 characters")]
    		public string  Module { get; set; }
    
    		    
    		/// <summary>
    		/// Owner
    		/// </summary>        
    	//    [DisplayName("Owner")]
            [MaxLength(50, ErrorMessage = "Owner cannot be longer than 50 characters")]
    		public string  Owner { get; set; }
    
    		    
    		/// <summary>
    		/// Status
    		/// </summary>        
    	//    [DisplayName("Status")]
            [MaxLength(10, ErrorMessage = "Status cannot be longer than 10 characters")]
    		public string  Status { get; set; }
    
    		    
    	}
    //}
    
}
