using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ValuePlus.ViewModel;



namespace ValuePlus.ViewModel.eSAM
{
    
    /// <summary>
    /// SAMergeExcHistory class
    /// </summary>
    //[MetadataType(typeof(SAMergeExcHistoryViewModel))]
    //public  partial class SAMergeExcHistory
    //{
    
    	/// <summary>
    	/// SAMergeExcHistory Metadata class
    	/// </summary>
    	public   class SAMergeExcHistoryViewModel:ViewModelBase
    	{
    		    
    		/// <summary>
    		/// ID
    		/// </summary>        
    	//    [DisplayName("ID")]
            [Required(ErrorMessage = "ID is required")]
    		public int  ID { get; set; }
    
    		    
    		/// <summary>
    		/// Created Date
    		/// </summary>        
    	//    [DisplayName("Created Date")]
    		public Nullable<System.DateTime>  CreatedDate { get; set; }
    
    		    
    		/// <summary>
    		/// Updated Date
    		/// </summary>        
    	//    [DisplayName("Updated Date")]
    		public Nullable<System.DateTime>  UpdatedDate { get; set; }
    
    		    
    		/// <summary>
    		/// Merge ID
    		/// </summary>        
    	//    [DisplayName("Merge ID")]
    		public Nullable<int>  MergeID { get; set; }
    
    		    
    		/// <summary>
    		/// Table ID
    		/// </summary>        
    	//    [DisplayName("Table ID")]
    		public Nullable<int>  TableID { get; set; }
    
    		    
    		/// <summary>
    		/// Status
    		/// </summary>        
    	//    [DisplayName("Status")]
            [MaxLength(10, ErrorMessage = "Status cannot be longer than 10 characters")]
    		public string  Status { get; set; }
    
    		    
    		/// <summary>
    		/// Station ID
    		/// </summary>        
    	//    [DisplayName("Station ID")]
            [MaxLength(10, ErrorMessage = "Station ID cannot be longer than 10 characters")]
    		public string  StationID { get; set; }
    
    		    
    		/// <summary>
    		/// DBName
    		/// </summary>        
    	//    [DisplayName("DBName")]
            [MaxLength(30, ErrorMessage = "DBName cannot be longer than 30 characters")]
    		public string  DBName { get; set; }
    
    		    
    		/// <summary>
    		/// Station IP
    		/// </summary>        
    	//    [DisplayName("Station IP")]
            [MaxLength(30, ErrorMessage = "Station IP cannot be longer than 30 characters")]
    		public string  StationIP { get; set; }
    
    		    
    		/// <summary>
    		/// CNT
    		/// </summary>        
    	//    [DisplayName("CNT")]
    		public Nullable<int>  CNT { get; set; }
    
    		    
    	}
    //}
    
}
