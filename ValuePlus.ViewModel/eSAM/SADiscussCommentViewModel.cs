using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ValuePlus.ViewModel;



namespace ValuePlus.ViewModel.eSAM
{
    
    /// <summary>
    /// SADiscussComment class
    /// </summary>
    //[MetadataType(typeof(SADiscussCommentViewModel))]
    //public  partial class SADiscussComment
    //{
    
    	/// <summary>
    	/// SADiscussComment Metadata class
    	/// </summary>
    	public   class SADiscussCommentViewModel:ViewModelBase
    	{
    		    
    		/// <summary>
    		/// ID
    		/// </summary>        
    	//    [DisplayName("ID")]
            [Required(ErrorMessage = "ID is required")]
    		public int  ID { get; set; }
    
    		    
    		/// <summary>
    		/// Status
    		/// </summary>        
    	//    [DisplayName("Status")]
            [MaxLength(20, ErrorMessage = "Status cannot be longer than 20 characters")]
    		public string  Status { get; set; }
    
    		    
    		/// <summary>
    		/// Comments
    		/// </summary>        
    	//    [DisplayName("Comments")]
    		public string  Comments { get; set; }
    
    		    
    		/// <summary>
    		/// Reply User ID
    		/// </summary>        
    	//    [DisplayName("Reply User ID")]
            [MaxLength(10, ErrorMessage = "Reply User ID cannot be longer than 10 characters")]
    		public string  ReplyUserID { get; set; }
    
    		    
    		/// <summary>
    		/// Comment Time
    		/// </summary>        
    	//    [DisplayName("Comment Time")]
    		public Nullable<System.DateTime>  CommentTime { get; set; }
    
    		    
    		/// <summary>
    		/// Source Type
    		/// </summary>        
    	//    [DisplayName("Source Type")]
            [MaxLength(20, ErrorMessage = "Source Type cannot be longer than 20 characters")]
    		public string  SourceType { get; set; }
    
    		    
    		/// <summary>
    		/// Source ID
    		/// </summary>        
    	//    [DisplayName("Source ID")]
    		public Nullable<int>  SourceID { get; set; }
    
    		    
    		/// <summary>
    		/// Attachments
    		/// </summary>        
    	//    [DisplayName("Attachments")]
    		public string  Attachments { get; set; }
    
    		    
    	}
    //}
    
}
