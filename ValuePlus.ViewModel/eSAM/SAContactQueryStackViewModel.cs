using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ValuePlus.ViewModel;



namespace ValuePlus.ViewModel.eSAM
{
    
    /// <summary>
    /// SAContactQueryStack class
    /// </summary>
    //[MetadataType(typeof(SAContactQueryStackViewModel))]
    //public  partial class SAContactQueryStack
    //{
    
    	/// <summary>
    	/// SAContactQueryStack Metadata class
    	/// </summary>
    	public   class SAContactQueryStackViewModel:ViewModelBase
    	{
    		    
    		/// <summary>
    		/// ID
    		/// </summary>        
    	//    [DisplayName("ID")]
            [Required(ErrorMessage = "ID is required")]
    		public int  ID { get; set; }
    
    		    
    		/// <summary>
    		/// Title
    		/// </summary>        
    	//    [DisplayName("Title")]
            [MaxLength(255, ErrorMessage = "Title cannot be longer than 255 characters")]
    		public string  Title { get; set; }
    
    		    
    		/// <summary>
    		/// Total Times
    		/// </summary>        
    	//    [DisplayName("Total Times")]
            [MaxLength(255, ErrorMessage = "Total Times cannot be longer than 255 characters")]
    		public string  TotalTimes { get; set; }
    
    		    
    		/// <summary>
    		/// Query Str
    		/// </summary>        
    	//    [DisplayName("Query Str")]
            [MaxLength(255, ErrorMessage = "Query Str cannot be longer than 255 characters")]
    		public string  QueryStr { get; set; }
    
    		    
    		/// <summary>
    		/// Views
    		/// </summary>        
    	//    [DisplayName("Views")]
            [MaxLength(255, ErrorMessage = "Views cannot be longer than 255 characters")]
    		public string  Views { get; set; }
    
    		    
    		/// <summary>
    		/// Modified
    		/// </summary>        
    	//    [DisplayName("Modified")]
    		public Nullable<System.DateTime>  Modified { get; set; }
    
    		    
    		/// <summary>
    		/// Created
    		/// </summary>        
    	//    [DisplayName("Created")]
    		public Nullable<System.DateTime>  Created { get; set; }
    
    		    
    	}
    //}
    
}
