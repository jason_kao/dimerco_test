using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ValuePlus.ViewModel;



namespace ValuePlus.ViewModel.eSAM
{
    
    /// <summary>
    /// SAMail class
    /// </summary>
    //[MetadataType(typeof(SAMailViewModel))]
    //public  partial class SAMail
    //{
    
    	/// <summary>
    	/// SAMail Metadata class
    	/// </summary>
    	public   class SAMailViewModel:ViewModelBase
    	{
    		    
    		/// <summary>
    		/// ID
    		/// </summary>        
    	//    [DisplayName("ID")]
            [Required(ErrorMessage = "ID is required")]
    		public int  ID { get; set; }
    
    		    
    		/// <summary>
    		/// Frm User ID
    		/// </summary>        
    	//    [DisplayName("Frm User ID")]
            [MaxLength(10, ErrorMessage = "Frm User ID cannot be longer than 10 characters")]
    		public string  FrmUserID { get; set; }
    
    		    
    		/// <summary>
    		/// TOs
    		/// </summary>        
    	//    [DisplayName("TOs")]
            [MaxLength(255, ErrorMessage = "TOs cannot be longer than 255 characters")]
    		public string  TOs { get; set; }
    
    		    
    		/// <summary>
    		/// CCs
    		/// </summary>        
    	//    [DisplayName("CCs")]
            [MaxLength(255, ErrorMessage = "CCs cannot be longer than 255 characters")]
    		public string  CCs { get; set; }
    
    		    
    		/// <summary>
    		/// Subject
    		/// </summary>        
    	//    [DisplayName("Subject")]
    		public string  Subject { get; set; }
    
    		    
    		/// <summary>
    		/// Message
    		/// </summary>        
    	//    [DisplayName("Message")]
    		public string  Message { get; set; }
    
    		    
    		/// <summary>
    		/// Attachment
    		/// </summary>        
    	//    [DisplayName("Attachment")]
            [MaxLength(255, ErrorMessage = "Attachment cannot be longer than 255 characters")]
    		public string  Attachment { get; set; }
    
    		    
    		/// <summary>
    		/// Date
    		/// </summary>        
    	//    [DisplayName("Date")]
    		public Nullable<System.DateTime>  Date { get; set; }
    
    		    
    		/// <summary>
    		/// Source ID
    		/// </summary>        
    	//    [DisplayName("Source ID")]
    		public Nullable<int>  SourceID { get; set; }
    
    		    
    		/// <summary>
    		/// Source Type
    		/// </summary>        
    	//    [DisplayName("Source Type")]
            [MaxLength(10, ErrorMessage = "Source Type cannot be longer than 10 characters")]
    		public string  SourceType { get; set; }
    
    		    
    	}
    //}
    
}
