using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ValuePlus.ViewModel;



namespace ValuePlus.ViewModel.eSAM
{
    
    /// <summary>
    /// SAQuoteContact class
    /// </summary>
    //[MetadataType(typeof(SAQuoteContactViewModel))]
    //public  partial class SAQuoteContact
    //{
    
    	/// <summary>
    	/// SAQuoteContact Metadata class
    	/// </summary>
    	public   class SAQuoteContactViewModel:ViewModelBase
    	{
    		    
    		/// <summary>
    		/// ID
    		/// </summary>        
    	//    [DisplayName("ID")]
            [Required(ErrorMessage = "ID is required")]
    		public int  ID { get; set; }
    
    		    
    		/// <summary>
    		/// User ID
    		/// </summary>        
    	//    [DisplayName("User ID")]
            [MaxLength(10, ErrorMessage = "User ID cannot be longer than 10 characters")]
    		public string  UserID { get; set; }
    
    		    
    		/// <summary>
    		/// User Station
    		/// </summary>        
    	//    [DisplayName("User Station")]
            [MaxLength(10, ErrorMessage = "User Station cannot be longer than 10 characters")]
    		public string  UserStation { get; set; }
    
    		    
    		/// <summary>
    		/// Calling Card
    		/// </summary>        
    	//    [DisplayName("Calling Card")]
    		public string  CallingCard { get; set; }
    
    		    
    		/// <summary>
    		/// Contact Order
    		/// </summary>        
    	//    [DisplayName("Contact Order")]
            [MaxLength(10, ErrorMessage = "Contact Order cannot be longer than 10 characters")]
    		public string  ContactOrder { get; set; }
    
    		    
    		/// <summary>
    		/// s Type
    		/// </summary>        
    	//    [DisplayName("s Type")]
            [MaxLength(10, ErrorMessage = "s Type cannot be longer than 10 characters")]
    		public string  sType { get; set; }
    
    		    
    		/// <summary>
    		/// Quote ID
    		/// </summary>        
    	//    [DisplayName("Quote ID")]
    		public Nullable<int>  QuoteID { get; set; }
    
    		    
    		/// <summary>
    		/// Quote No
    		/// </summary>        
    	//    [DisplayName("Quote No")]
            [MaxLength(15, ErrorMessage = "Quote No cannot be longer than 15 characters")]
    		public string  QuoteNo { get; set; }
    
    		    
    	}
    //}
    
}
