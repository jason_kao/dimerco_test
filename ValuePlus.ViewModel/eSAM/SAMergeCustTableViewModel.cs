using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ValuePlus.ViewModel;



namespace ValuePlus.ViewModel.eSAM
{
    
    /// <summary>
    /// SAMergeCustTable class
    /// </summary>
    //[MetadataType(typeof(SAMergeCustTableViewModel))]
    //public  partial class SAMergeCustTable
    //{
    
    	/// <summary>
    	/// SAMergeCustTable Metadata class
    	/// </summary>
    	public   class SAMergeCustTableViewModel:ViewModelBase
    	{
    		    
    		/// <summary>
    		/// ID
    		/// </summary>        
    	//    [DisplayName("ID")]
            [Required(ErrorMessage = "ID is required")]
    		public int  ID { get; set; }
    
    		    
    		/// <summary>
    		/// Module
    		/// </summary>        
    	//    [DisplayName("Module")]
            [MaxLength(50, ErrorMessage = "Module cannot be longer than 50 characters")]
    		public string  Module { get; set; }
    
    		    
    		/// <summary>
    		/// Function Name
    		/// </summary>        
    	//    [DisplayName("Function Name")]
            [MaxLength(50, ErrorMessage = "Function Name cannot be longer than 50 characters")]
    		public string  FunctionName { get; set; }
    
    		    
    		/// <summary>
    		/// Table Name
    		/// </summary>        
    	//    [DisplayName("Table Name")]
            [MaxLength(50, ErrorMessage = "Table Name cannot be longer than 50 characters")]
    		public string  TableName { get; set; }
    
    		    
    		/// <summary>
    		/// Column Name
    		/// </summary>        
    	//    [DisplayName("Column Name")]
            [MaxLength(50, ErrorMessage = "Column Name cannot be longer than 50 characters")]
    		public string  ColumnName { get; set; }
    
    		    
    		/// <summary>
    		/// Owner
    		/// </summary>        
    	//    [DisplayName("Owner")]
            [MaxLength(50, ErrorMessage = "Owner cannot be longer than 50 characters")]
    		public string  Owner { get; set; }
    
    		    
    		/// <summary>
    		/// Created Date
    		/// </summary>        
    	//    [DisplayName("Created Date")]
    		public Nullable<System.DateTime>  CreatedDate { get; set; }
    
    		    
    		/// <summary>
    		/// Created BY
    		/// </summary>        
    	//    [DisplayName("Created BY")]
            [MaxLength(10, ErrorMessage = "Created BY cannot be longer than 10 characters")]
    		public string  CreatedBY { get; set; }
    
    		    
    		/// <summary>
    		/// Updated Date
    		/// </summary>        
    	//    [DisplayName("Updated Date")]
    		public Nullable<System.DateTime>  UpdatedDate { get; set; }
    
    		    
    		/// <summary>
    		/// Updated By
    		/// </summary>        
    	//    [DisplayName("Updated By")]
            [MaxLength(10, ErrorMessage = "Updated By cannot be longer than 10 characters")]
    		public string  UpdatedBy { get; set; }
    
    		    
    		/// <summary>
    		/// Status
    		/// </summary>        
    	//    [DisplayName("Status")]
            [MaxLength(10, ErrorMessage = "Status cannot be longer than 10 characters")]
    		public string  Status { get; set; }
    
    		    
    		/// <summary>
    		/// Is Re SM
    		/// </summary>        
    	//    [DisplayName("Is Re SM")]
            [MaxLength(1, ErrorMessage = "Is Re SM cannot be longer than 1 characters")]
    		public string  IsReSM { get; set; }
    
    		    
    		/// <summary>
    		/// Action
    		/// </summary>        
    	//    [DisplayName("Action")]
            [MaxLength(1, ErrorMessage = "Action cannot be longer than 1 characters")]
    		public string  Action { get; set; }
    
    		    
    		/// <summary>
    		/// Is HQ
    		/// </summary>        
    	//    [DisplayName("Is HQ")]
            [MaxLength(1, ErrorMessage = "Is HQ cannot be longer than 1 characters")]
    		public string  IsHQ { get; set; }
    
    		    
    		/// <summary>
    		/// Is Check
    		/// </summary>        
    	//    [DisplayName("Is Check")]
            [MaxLength(1, ErrorMessage = "Is Check cannot be longer than 1 characters")]
    		public string  IsCheck { get; set; }
    
    		    
    	}
    //}
    
}
