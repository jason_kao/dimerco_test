using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ValuePlus.ViewModel;



namespace ValuePlus.ViewModel.eSAM
{
    
    /// <summary>
    /// SASalesRoutes class
    /// </summary>
    //[MetadataType(typeof(SASalesRoutesViewModel))]
    //public  partial class SASalesRoutes
    //{
    
    	/// <summary>
    	/// SASalesRoutes Metadata class
    	/// </summary>
    	public   class SASalesRoutesViewModel:ViewModelBase
    	{
    		    
    		/// <summary>
    		/// ID
    		/// </summary>        
    	//    [DisplayName("ID")]
            [Required(ErrorMessage = "ID is required")]
    		public int  ID { get; set; }
    
    		    
    		/// <summary>
    		/// PID
    		/// </summary>        
    	//    [DisplayName("PID")]
    		public Nullable<int>  PID { get; set; }
    
    		    
    		/// <summary>
    		/// Origin City ID
    		/// </summary>        
    	//    [DisplayName("Origin City ID")]
    		public Nullable<int>  OriginCityID { get; set; }
    
    		    
    		/// <summary>
    		/// Destination City ID
    		/// </summary>        
    	//    [DisplayName("Destination City ID")]
    		public Nullable<int>  DestinationCityID { get; set; }
    
    		    
    		/// <summary>
    		/// Duty1
    		/// </summary>        
    	//    [DisplayName("Duty1")]
            [MaxLength(20, ErrorMessage = "Duty1 cannot be longer than 20 characters")]
    		public string  Duty1 { get; set; }
    
    		    
    		/// <summary>
    		/// Duty2
    		/// </summary>        
    	//    [DisplayName("Duty2")]
            [MaxLength(20, ErrorMessage = "Duty2 cannot be longer than 20 characters")]
    		public string  Duty2 { get; set; }
    
    		    
    		/// <summary>
    		/// Probability
    		/// </summary>        
    	//    [DisplayName("Probability")]
    		public Nullable<int>  Probability { get; set; }
    
    		    
    		/// <summary>
    		/// Created User ID
    		/// </summary>        
    	//    [DisplayName("Created User ID")]
            [MaxLength(10, ErrorMessage = "Created User ID cannot be longer than 10 characters")]
    		public string  CreatedUserID { get; set; }
    
    		    
    		/// <summary>
    		/// Created Date
    		/// </summary>        
    	//    [DisplayName("Created Date")]
    		public Nullable<System.DateTime>  CreatedDate { get; set; }
    
    		    
    		/// <summary>
    		/// Updated User ID
    		/// </summary>        
    	//    [DisplayName("Updated User ID")]
            [MaxLength(10, ErrorMessage = "Updated User ID cannot be longer than 10 characters")]
    		public string  UpdatedUserID { get; set; }
    
    		    
    		/// <summary>
    		/// Updated Date
    		/// </summary>        
    	//    [DisplayName("Updated Date")]
    		public Nullable<System.DateTime>  UpdatedDate { get; set; }
    
    		    
    	}
    //}
    
}
