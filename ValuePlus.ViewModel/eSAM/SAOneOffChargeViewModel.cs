using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ValuePlus.ViewModel;



namespace ValuePlus.ViewModel.eSAM
{
    
    /// <summary>
    /// SAOneOffCharge class
    /// </summary>
    //[MetadataType(typeof(SAOneOffChargeViewModel))]
    //public  partial class SAOneOffCharge
    //{
    
    	/// <summary>
    	/// SAOneOffCharge Metadata class
    	/// </summary>
    	public   class SAOneOffChargeViewModel:ViewModelBase
    	{
    		    
    		/// <summary>
    		/// ID
    		/// </summary>        
    	//    [DisplayName("ID")]
            [Required(ErrorMessage = "ID is required")]
    		public int  ID { get; set; }
    
    		    
    		/// <summary>
    		/// Charge Name
    		/// </summary>        
    	//    [DisplayName("Charge Name")]
            [MaxLength(200, ErrorMessage = "Charge Name cannot be longer than 200 characters")]
    		public string  ChargeName { get; set; }
    
    		    
    		/// <summary>
    		/// Currency Code
    		/// </summary>        
    	//    [DisplayName("Currency Code")]
            [MaxLength(20, ErrorMessage = "Currency Code cannot be longer than 20 characters")]
    		public string  CurrencyCode { get; set; }
    
    		    
    		/// <summary>
    		/// Price
    		/// </summary>        
    	//    [DisplayName("Price")]
            [MaxLength(200, ErrorMessage = "Price cannot be longer than 200 characters")]
    		public string  Price { get; set; }
    
    		    
    		/// <summary>
    		/// Charge Type
    		/// </summary>        
    	//    [DisplayName("Charge Type")]
    		public Nullable<int>  ChargeType { get; set; }
    
    		    
    		/// <summary>
    		/// Quote ID
    		/// </summary>        
    	//    [DisplayName("Quote ID")]
            [Required(ErrorMessage = "Quote ID is required")]
    		public int  QuoteID { get; set; }
    
    		    
    		/// <summary>
    		/// City Name
    		/// </summary>        
    	//    [DisplayName("City Name")]
            [MaxLength(200, ErrorMessage = "City Name cannot be longer than 200 characters")]
    		public string  CityName { get; set; }
    
    		    
    		/// <summary>
    		/// Created User ID
    		/// </summary>        
    	//    [DisplayName("Created User ID")]
            [MaxLength(10, ErrorMessage = "Created User ID cannot be longer than 10 characters")]
    		public string  CreatedUserID { get; set; }
    
    		    
    		/// <summary>
    		/// Created Date
    		/// </summary>        
    	//    [DisplayName("Created Date")]
    		public Nullable<System.DateTime>  CreatedDate { get; set; }
    
    		    
    		/// <summary>
    		/// Updated User ID
    		/// </summary>        
    	//    [DisplayName("Updated User ID")]
            [MaxLength(10, ErrorMessage = "Updated User ID cannot be longer than 10 characters")]
    		public string  UpdatedUserID { get; set; }
    
    		    
    		/// <summary>
    		/// Updated Date
    		/// </summary>        
    	//    [DisplayName("Updated Date")]
    		public Nullable<System.DateTime>  UpdatedDate { get; set; }
    
    		    
    	}
    //}
    
}
