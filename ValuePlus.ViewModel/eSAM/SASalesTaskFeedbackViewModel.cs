using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ValuePlus.ViewModel;



namespace ValuePlus.ViewModel.eSAM
{
    
    /// <summary>
    /// SASalesTaskFeedback class
    /// </summary>
    //[MetadataType(typeof(SASalesTaskFeedbackViewModel))]
    //public  partial class SASalesTaskFeedback
    //{
    
    	/// <summary>
    	/// SASalesTaskFeedback Metadata class
    	/// </summary>
    	public   class SASalesTaskFeedbackViewModel:ViewModelBase
    	{
    		    
    		/// <summary>
    		/// ID
    		/// </summary>        
    	//    [DisplayName("ID")]
            [Required(ErrorMessage = "ID is required")]
    		public int  ID { get; set; }
    
    		    
    		/// <summary>
    		/// task ID
    		/// </summary>        
    	//    [DisplayName("task ID")]
            [Required(ErrorMessage = "task ID is required")]
    		public int  taskID { get; set; }
    
    		    
    		/// <summary>
    		/// customer Type
    		/// </summary>        
    	//    [DisplayName("customer Type")]
            [MaxLength(2, ErrorMessage = "customer Type cannot be longer than 2 characters")]
    		public string  customerType { get; set; }
    
    		    
    		/// <summary>
    		/// Step1 Title ID
    		/// </summary>        
    	//    [DisplayName("Step1 Title ID")]
    		public Nullable<int>  Step1TitleID { get; set; }
    
    		    
    		/// <summary>
    		/// Step2 Title ID
    		/// </summary>        
    	//    [DisplayName("Step2 Title ID")]
    		public Nullable<int>  Step2TitleID { get; set; }
    
    		    
    		/// <summary>
    		/// Step3 Title ID
    		/// </summary>        
    	//    [DisplayName("Step3 Title ID")]
    		public Nullable<int>  Step3TitleID { get; set; }
    
    		    
    		/// <summary>
    		/// Step4 Title ID
    		/// </summary>        
    	//    [DisplayName("Step4 Title ID")]
    		public Nullable<int>  Step4TitleID { get; set; }
    
    		    
    		/// <summary>
    		/// Step1 Item ID
    		/// </summary>        
    	//    [DisplayName("Step1 Item ID")]
            [MaxLength(50, ErrorMessage = "Step1 Item ID cannot be longer than 50 characters")]
    		public string  Step1ItemID { get; set; }
    
    		    
    		/// <summary>
    		/// Step2 Item ID
    		/// </summary>        
    	//    [DisplayName("Step2 Item ID")]
            [MaxLength(50, ErrorMessage = "Step2 Item ID cannot be longer than 50 characters")]
    		public string  Step2ItemID { get; set; }
    
    		    
    		/// <summary>
    		/// Step3 Item ID
    		/// </summary>        
    	//    [DisplayName("Step3 Item ID")]
            [MaxLength(50, ErrorMessage = "Step3 Item ID cannot be longer than 50 characters")]
    		public string  Step3ItemID { get; set; }
    
    		    
    		/// <summary>
    		/// Step4 Item ID
    		/// </summary>        
    	//    [DisplayName("Step4 Item ID")]
            [MaxLength(50, ErrorMessage = "Step4 Item ID cannot be longer than 50 characters")]
    		public string  Step4ItemID { get; set; }
    
    		    
    		/// <summary>
    		/// item Info
    		/// </summary>        
    	//    [DisplayName("item Info")]
            [MaxLength(100, ErrorMessage = "item Info cannot be longer than 100 characters")]
    		public string  itemInfo { get; set; }
    
    		    
    		/// <summary>
    		/// Status
    		/// </summary>        
    	//    [DisplayName("Status")]
            [MaxLength(10, ErrorMessage = "Status cannot be longer than 10 characters")]
    		public string  Status { get; set; }
    
    		    
    		/// <summary>
    		/// Create By
    		/// </summary>        
    	//    [DisplayName("Create By")]
            [MaxLength(10, ErrorMessage = "Create By cannot be longer than 10 characters")]
    		public string  CreateBy { get; set; }
    
    		    
    		/// <summary>
    		/// Create Date
    		/// </summary>        
    	//    [DisplayName("Create Date")]
    		public Nullable<System.DateTime>  CreateDate { get; set; }
    
    		    
    		/// <summary>
    		/// Updated By
    		/// </summary>        
    	//    [DisplayName("Updated By")]
            [MaxLength(10, ErrorMessage = "Updated By cannot be longer than 10 characters")]
    		public string  UpdatedBy { get; set; }
    
    		    
    		/// <summary>
    		/// Updated Date
    		/// </summary>        
    	//    [DisplayName("Updated Date")]
    		public Nullable<System.DateTime>  UpdatedDate { get; set; }
    
    		    
    	}
    //}
    
}
