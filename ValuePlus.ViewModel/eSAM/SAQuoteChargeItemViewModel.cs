using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ValuePlus.ViewModel;



namespace ValuePlus.ViewModel.eSAM
{
    
    /// <summary>
    /// SAQuoteChargeItem class
    /// </summary>
    //[MetadataType(typeof(SAQuoteChargeItemViewModel))]
    //public  partial class SAQuoteChargeItem
    //{
    
    	/// <summary>
    	/// SAQuoteChargeItem Metadata class
    	/// </summary>
    	public   class SAQuoteChargeItemViewModel:ViewModelBase
    	{
    		    
    		/// <summary>
    		/// ID
    		/// </summary>        
    	//    [DisplayName("ID")]
            [Required(ErrorMessage = "ID is required")]
    		public int  ID { get; set; }
    
    		    
    		/// <summary>
    		/// Charge ID
    		/// </summary>        
    	//    [DisplayName("Charge ID")]
    		public Nullable<int>  ChargeID { get; set; }
    
    		    
    		/// <summary>
    		/// Unit
    		/// </summary>        
    	//    [DisplayName("Unit")]
            [MaxLength(20, ErrorMessage = "Unit cannot be longer than 20 characters")]
    		public string  Unit { get; set; }
    
    		    
    		/// <summary>
    		/// UOMID
    		/// </summary>        
    	//    [DisplayName("UOMID")]
            [MaxLength(20, ErrorMessage = "UOMID cannot be longer than 20 characters")]
    		public string  UOMID { get; set; }
    
    		    
    		/// <summary>
    		/// Cost
    		/// </summary>        
    	//    [DisplayName("Cost")]
            [MaxLength(20, ErrorMessage = "Cost cannot be longer than 20 characters")]
    		public string  Cost { get; set; }
    
    		    
    		/// <summary>
    		/// Profit
    		/// </summary>        
    	//    [DisplayName("Profit")]
            [MaxLength(20, ErrorMessage = "Profit cannot be longer than 20 characters")]
    		public string  Profit { get; set; }
    
    		    
    		/// <summary>
    		/// Selling
    		/// </summary>        
    	//    [DisplayName("Selling")]
            [MaxLength(20, ErrorMessage = "Selling cannot be longer than 20 characters")]
    		public string  Selling { get; set; }
    
    		    
    		/// <summary>
    		/// Created User ID
    		/// </summary>        
    	//    [DisplayName("Created User ID")]
            [MaxLength(10, ErrorMessage = "Created User ID cannot be longer than 10 characters")]
    		public string  CreatedUserID { get; set; }
    
    		    
    		/// <summary>
    		/// Created Date
    		/// </summary>        
    	//    [DisplayName("Created Date")]
    		public Nullable<System.DateTime>  CreatedDate { get; set; }
    
    		    
    		/// <summary>
    		/// Updated User ID
    		/// </summary>        
    	//    [DisplayName("Updated User ID")]
            [MaxLength(10, ErrorMessage = "Updated User ID cannot be longer than 10 characters")]
    		public string  UpdatedUserID { get; set; }
    
    		    
    		/// <summary>
    		/// Updated Date
    		/// </summary>        
    	//    [DisplayName("Updated Date")]
    		public Nullable<System.DateTime>  UpdatedDate { get; set; }
    
    		    
    		/// <summary>
    		/// FLItem
    		/// </summary>        
    	//    [DisplayName("FLItem")]
            [MaxLength(10, ErrorMessage = "FLItem cannot be longer than 10 characters")]
    		public string  FLItem { get; set; }
    
    		    
    		/// <summary>
    		/// PPCC
    		/// </summary>        
    	//    [DisplayName("PPCC")]
            [MaxLength(50, ErrorMessage = "PPCC cannot be longer than 50 characters")]
    		public string  PPCC { get; set; }
    
    		    
    		/// <summary>
    		/// Remark
    		/// </summary>        
    	//    [DisplayName("Remark")]
            [MaxLength(150, ErrorMessage = "Remark cannot be longer than 150 characters")]
    		public string  Remark { get; set; }
    
    		    
    		/// <summary>
    		/// Condition ID
    		/// </summary>        
    	//    [DisplayName("Condition ID")]
    		public Nullable<int>  ConditionID { get; set; }
    
    		    
    	}
    //}
    
}
