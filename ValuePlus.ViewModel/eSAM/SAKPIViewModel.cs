using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ValuePlus.ViewModel;



namespace ValuePlus.ViewModel.eSAM
{
    
    /// <summary>
    /// SAKPI class
    /// </summary>
    //[MetadataType(typeof(SAKPIViewModel))]
    //public  partial class SAKPI
    //{
    
    	/// <summary>
    	/// SAKPI Metadata class
    	/// </summary>
    	public   class SAKPIViewModel:ViewModelBase
    	{
    		    
    		/// <summary>
    		/// ID
    		/// </summary>        
    	//    [DisplayName("ID")]
            [Required(ErrorMessage = "ID is required")]
    		public int  ID { get; set; }
    
    		    
    		/// <summary>
    		/// Type
    		/// </summary>        
    	//    [DisplayName("Type")]
            [MaxLength(25, ErrorMessage = "Type cannot be longer than 25 characters")]
    		public string  Type { get; set; }
    
    		    
    		/// <summary>
    		/// Code
    		/// </summary>        
    	//    [DisplayName("Code")]
            [MaxLength(255, ErrorMessage = "Code cannot be longer than 255 characters")]
    		public string  Code { get; set; }
    
    		    
    		/// <summary>
    		/// Description
    		/// </summary>        
    	//    [DisplayName("Description")]
            [MaxLength(255, ErrorMessage = "Description cannot be longer than 255 characters")]
    		public string  Description { get; set; }
    
    		    
    		/// <summary>
    		/// frequency
    		/// </summary>        
    	//    [DisplayName("frequency")]
            [MaxLength(25, ErrorMessage = "frequency cannot be longer than 25 characters")]
    		public string  frequency { get; set; }
    
    		    
    		/// <summary>
    		/// Status
    		/// </summary>        
    	//    [DisplayName("Status")]
            [MaxLength(25, ErrorMessage = "Status cannot be longer than 25 characters")]
    		public string  Status { get; set; }
    
    		    
    		/// <summary>
    		/// Created User ID
    		/// </summary>        
    	//    [DisplayName("Created User ID")]
            [MaxLength(10, ErrorMessage = "Created User ID cannot be longer than 10 characters")]
    		public string  CreatedUserID { get; set; }
    
    		    
    		/// <summary>
    		/// Created Date
    		/// </summary>        
    	//    [DisplayName("Created Date")]
    		public Nullable<System.DateTime>  CreatedDate { get; set; }
    
    		    
    		/// <summary>
    		/// Update User ID
    		/// </summary>        
    	//    [DisplayName("Update User ID")]
            [MaxLength(10, ErrorMessage = "Update User ID cannot be longer than 10 characters")]
    		public string  UpdateUserID { get; set; }
    
    		    
    		/// <summary>
    		/// Update Date
    		/// </summary>        
    	//    [DisplayName("Update Date")]
    		public Nullable<System.DateTime>  UpdateDate { get; set; }
    
    		    
    	}
    //}
    
}
