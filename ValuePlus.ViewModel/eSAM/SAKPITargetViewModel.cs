using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ValuePlus.ViewModel;



namespace ValuePlus.ViewModel.eSAM
{
    
    /// <summary>
    /// SAKPITarget class
    /// </summary>
    //[MetadataType(typeof(SAKPITargetViewModel))]
    //public  partial class SAKPITarget
    //{
    
    	/// <summary>
    	/// SAKPITarget Metadata class
    	/// </summary>
    	public   class SAKPITargetViewModel:ViewModelBase
    	{
    		    
    		/// <summary>
    		/// ID
    		/// </summary>        
    	//    [DisplayName("ID")]
            [Required(ErrorMessage = "ID is required")]
    		public int  ID { get; set; }
    
    		    
    		/// <summary>
    		/// KPIID
    		/// </summary>        
    	//    [DisplayName("KPIID")]
            [Required(ErrorMessage = "KPIID is required")]
    		public int  KPIID { get; set; }
    
    		    
    		/// <summary>
    		/// Target
    		/// </summary>        
    	//    [DisplayName("Target")]
            [Required(ErrorMessage = "Target is required")]
    		public int  Target { get; set; }
    
    		    
    		/// <summary>
    		/// Status
    		/// </summary>        
    	//    [DisplayName("Status")]
            [MaxLength(10, ErrorMessage = "Status cannot be longer than 10 characters")]
    		public string  Status { get; set; }
    
    		    
    		/// <summary>
    		/// Dept Type
    		/// </summary>        
    	//    [DisplayName("Dept Type")]
            [MaxLength(20, ErrorMessage = "Dept Type cannot be longer than 20 characters")]
    		public string  DeptType { get; set; }
    
    		    
    		/// <summary>
    		/// Dept Code
    		/// </summary>        
    	//    [DisplayName("Dept Code")]
            [MaxLength(20, ErrorMessage = "Dept Code cannot be longer than 20 characters")]
    		public string  DeptCode { get; set; }
    
    		    
    		/// <summary>
    		/// Created User ID
    		/// </summary>        
    	//    [DisplayName("Created User ID")]
            [MaxLength(10, ErrorMessage = "Created User ID cannot be longer than 10 characters")]
    		public string  CreatedUserID { get; set; }
    
    		    
    		/// <summary>
    		/// Created Date
    		/// </summary>        
    	//    [DisplayName("Created Date")]
    		public Nullable<System.DateTime>  CreatedDate { get; set; }
    
    		    
    		/// <summary>
    		/// Update User ID
    		/// </summary>        
    	//    [DisplayName("Update User ID")]
            [MaxLength(10, ErrorMessage = "Update User ID cannot be longer than 10 characters")]
    		public string  UpdateUserID { get; set; }
    
    		    
    		/// <summary>
    		/// Update Date
    		/// </summary>        
    	//    [DisplayName("Update Date")]
    		public Nullable<System.DateTime>  UpdateDate { get; set; }
    
    		    
    		/// <summary>
    		/// score
    		/// </summary>        
    	//    [DisplayName("score")]
    		public Nullable<int>  score { get; set; }
    
    		    
    	}
    //}
    
}
