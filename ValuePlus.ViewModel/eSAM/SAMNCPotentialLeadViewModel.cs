using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ValuePlus.ViewModel;



namespace ValuePlus.ViewModel.eSAM
{
    
    /// <summary>
    /// SAMNCPotentialLead class
    /// </summary>
    //[MetadataType(typeof(SAMNCPotentialLeadViewModel))]
    //public  partial class SAMNCPotentialLead
    //{
    
    	/// <summary>
    	/// SAMNCPotentialLead Metadata class
    	/// </summary>
    	public   class SAMNCPotentialLeadViewModel:ViewModelBase
    	{
    		    
    		/// <summary>
    		/// ID
    		/// </summary>        
    	//    [DisplayName("ID")]
            [Required(ErrorMessage = "ID is required")]
    		public int  ID { get; set; }
    
    		    
    		/// <summary>
    		/// Project Name
    		/// </summary>        
    	//    [DisplayName("Project Name")]
            [Required(ErrorMessage = "Project Name is required")]
            [MaxLength(200, ErrorMessage = "Project Name cannot be longer than 200 characters")]
    		public string  ProjectName { get; set; }
    
    		    
    		/// <summary>
    		/// Project Code
    		/// </summary>        
    	//    [DisplayName("Project Code")]
            [MaxLength(15, ErrorMessage = "Project Code cannot be longer than 15 characters")]
    		public string  ProjectCode { get; set; }
    
    		    
    		/// <summary>
    		/// Global Code
    		/// </summary>        
    	//    [DisplayName("Global Code")]
            [MaxLength(10, ErrorMessage = "Global Code cannot be longer than 10 characters")]
    		public string  GlobalCode { get; set; }
    
    		    
    		/// <summary>
    		/// Product Line
    		/// </summary>        
    	//    [DisplayName("Product Line")]
            [Required(ErrorMessage = "Product Line is required")]
            [MaxLength(20, ErrorMessage = "Product Line cannot be longer than 20 characters")]
    		public string  ProductLine { get; set; }
    
    		    
    		/// <summary>
    		/// Reg MKT
    		/// </summary>        
    	//    [DisplayName("Reg MKT")]
            [MaxLength(6, ErrorMessage = "Reg MKT cannot be longer than 6 characters")]
    		public string  RegMKT { get; set; }
    
    		    
    		/// <summary>
    		/// Grp Rep
    		/// </summary>        
    	//    [DisplayName("Grp Rep")]
            [MaxLength(6, ErrorMessage = "Grp Rep cannot be longer than 6 characters")]
    		public string  GrpRep { get; set; }
    
    		    
    		/// <summary>
    		/// Acc Mgr
    		/// </summary>        
    	//    [DisplayName("Acc Mgr")]
            [MaxLength(6, ErrorMessage = "Acc Mgr cannot be longer than 6 characters")]
    		public string  AccMgr { get; set; }
    
    		    
    		/// <summary>
    		/// Ass Acc Mgr1
    		/// </summary>        
    	//    [DisplayName("Ass Acc Mgr1")]
            [MaxLength(6, ErrorMessage = "Ass Acc Mgr1 cannot be longer than 6 characters")]
    		public string  AssAccMgr1 { get; set; }
    
    		    
    		/// <summary>
    		/// Ass Acc Mgr2
    		/// </summary>        
    	//    [DisplayName("Ass Acc Mgr2")]
            [MaxLength(6, ErrorMessage = "Ass Acc Mgr2 cannot be longer than 6 characters")]
    		public string  AssAccMgr2 { get; set; }
    
    		    
    		/// <summary>
    		/// Ass Acc Mgr3
    		/// </summary>        
    	//    [DisplayName("Ass Acc Mgr3")]
            [MaxLength(6, ErrorMessage = "Ass Acc Mgr3 cannot be longer than 6 characters")]
    		public string  AssAccMgr3 { get; set; }
    
    		    
    		/// <summary>
    		/// Ass Acc Mgr4
    		/// </summary>        
    	//    [DisplayName("Ass Acc Mgr4")]
            [MaxLength(6, ErrorMessage = "Ass Acc Mgr4 cannot be longer than 6 characters")]
    		public string  AssAccMgr4 { get; set; }
    
    		    
    		/// <summary>
    		/// Ass Acc Mgr5
    		/// </summary>        
    	//    [DisplayName("Ass Acc Mgr5")]
            [MaxLength(6, ErrorMessage = "Ass Acc Mgr5 cannot be longer than 6 characters")]
    		public string  AssAccMgr5 { get; set; }
    
    		    
    		/// <summary>
    		/// Assistant
    		/// </summary>        
    	//    [DisplayName("Assistant")]
            [MaxLength(6, ErrorMessage = "Assistant cannot be longer than 6 characters")]
    		public string  Assistant { get; set; }
    
    		    
    		/// <summary>
    		/// Customer ID
    		/// </summary>        
    	//    [DisplayName("Customer ID")]
    		public Nullable<int>  CustomerID { get; set; }
    
    		    
    		/// <summary>
    		/// Status
    		/// </summary>        
    	//    [DisplayName("Status")]
            [MaxLength(20, ErrorMessage = "Status cannot be longer than 20 characters")]
    		public string  Status { get; set; }
    
    		    
    		/// <summary>
    		/// Send By
    		/// </summary>        
    	//    [DisplayName("Send By")]
            [MaxLength(6, ErrorMessage = "Send By cannot be longer than 6 characters")]
    		public string  SendBy { get; set; }
    
    		    
    		/// <summary>
    		/// Send Date
    		/// </summary>        
    	//    [DisplayName("Send Date")]
    		public Nullable<System.DateTime>  SendDate { get; set; }
    
    		    
    		/// <summary>
    		/// Approved By
    		/// </summary>        
    	//    [DisplayName("Approved By")]
            [MaxLength(6, ErrorMessage = "Approved By cannot be longer than 6 characters")]
    		public string  ApprovedBy { get; set; }
    
    		    
    		/// <summary>
    		/// Approved Date
    		/// </summary>        
    	//    [DisplayName("Approved Date")]
    		public Nullable<System.DateTime>  ApprovedDate { get; set; }
    
    		    
    		/// <summary>
    		/// Created By
    		/// </summary>        
    	//    [DisplayName("Created By")]
            [Required(ErrorMessage = "Created By is required")]
            [MaxLength(6, ErrorMessage = "Created By cannot be longer than 6 characters")]
    		public string  CreatedBy { get; set; }
    
    		    
    		/// <summary>
    		/// Created Date
    		/// </summary>        
    	//    [DisplayName("Created Date")]
            [Required(ErrorMessage = "Created Date is required")]
    		public System.DateTime  CreatedDate { get; set; }
    
    		    
    		/// <summary>
    		/// Updated By
    		/// </summary>        
    	//    [DisplayName("Updated By")]
            [Required(ErrorMessage = "Updated By is required")]
            [MaxLength(6, ErrorMessage = "Updated By cannot be longer than 6 characters")]
    		public string  UpdatedBy { get; set; }
    
    		    
    		/// <summary>
    		/// Updated Date
    		/// </summary>        
    	//    [DisplayName("Updated Date")]
            [Required(ErrorMessage = "Updated Date is required")]
    		public System.DateTime  UpdatedDate { get; set; }
    
    		    
    		/// <summary>
    		/// Version
    		/// </summary>        
    	//    [DisplayName("Version")]
            [Required(ErrorMessage = "Version is required")]
            [MaxLength(8, ErrorMessage = "Version cannot be longer than 8 characters")]
    		public byte[]  Version { get; set; }
    
    		    
    	}
    //}
    
}
