using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ValuePlus.ViewModel;



namespace ValuePlus.ViewModel.eSAM
{
    
    /// <summary>
    /// SAQuoteFreightFSData class
    /// </summary>
    //[MetadataType(typeof(SAQuoteFreightFSDataViewModel))]
    //public  partial class SAQuoteFreightFSData
    //{
    
    	/// <summary>
    	/// SAQuoteFreightFSData Metadata class
    	/// </summary>
    	public   class SAQuoteFreightFSDataViewModel:ViewModelBase
    	{
    		    
    		/// <summary>
    		/// Quote Freight ID
    		/// </summary>        
    	//    [DisplayName("Quote Freight ID")]
            [Required(ErrorMessage = "Quote Freight ID is required")]
    		public int  QuoteFreightID { get; set; }
    
    		    
    		/// <summary>
    		/// FSecuredate
    		/// </summary>        
    	//    [DisplayName("FSecuredate")]
    		public Nullable<System.DateTime>  FSecuredate { get; set; }
    
    		    
    		/// <summary>
    		/// FSecured Shpt
    		/// </summary>        
    	//    [DisplayName("FSecured Shpt")]
            [MaxLength(50, ErrorMessage = "FSecured Shpt cannot be longer than 50 characters")]
    		public string  FSecuredShpt { get; set; }
    
    		    
    		/// <summary>
    		/// FSecured Owner User ID
    		/// </summary>        
    	//    [DisplayName("FSecured Owner User ID")]
            [MaxLength(50, ErrorMessage = "FSecured Owner User ID cannot be longer than 50 characters")]
    		public string  FSecuredOwnerUserID { get; set; }
    
    		    
    		/// <summary>
    		/// ID
    		/// </summary>        
    	//    [DisplayName("ID")]
            [Required(ErrorMessage = "ID is required")]
    		public int  ID { get; set; }
    
    		    
    	}
    //}
    
}
