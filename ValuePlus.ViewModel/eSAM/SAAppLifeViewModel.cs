using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ValuePlus.ViewModel;



namespace ValuePlus.ViewModel.eSAM
{
    
    /// <summary>
    /// SAAppLife class
    /// </summary>
    //[MetadataType(typeof(SAAppLifeViewModel))]
    //public  partial class SAAppLife
    //{
    
    	/// <summary>
    	/// SAAppLife Metadata class
    	/// </summary>
    	public   class SAAppLifeViewModel:ViewModelBase
    	{
    		    
    		/// <summary>
    		/// ID
    		/// </summary>        
    	//    [DisplayName("ID")]
            [Required(ErrorMessage = "ID is required")]
    		public int  ID { get; set; }
    
    		    
    		/// <summary>
    		/// App Name
    		/// </summary>        
    	//    [DisplayName("App Name")]
            [MaxLength(50, ErrorMessage = "App Name cannot be longer than 50 characters")]
    		public string  AppName { get; set; }
    
    		    
    		/// <summary>
    		/// Updated Date
    		/// </summary>        
    	//    [DisplayName("Updated Date")]
    		public Nullable<System.DateTime>  UpdatedDate { get; set; }
    
    		    
    	}
    //}
    
}
