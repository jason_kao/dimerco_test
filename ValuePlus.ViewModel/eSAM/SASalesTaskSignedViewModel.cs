using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ValuePlus.ViewModel;



namespace ValuePlus.ViewModel.eSAM
{
    
    /// <summary>
    /// SASalesTaskSigned class
    /// </summary>
    //[MetadataType(typeof(SASalesTaskSignedViewModel))]
    //public  partial class SASalesTaskSigned
    //{
    
    	/// <summary>
    	/// SASalesTaskSigned Metadata class
    	/// </summary>
    	public   class SASalesTaskSignedViewModel:ViewModelBase
    	{
    		    
    		/// <summary>
    		/// ID
    		/// </summary>        
    	//    [DisplayName("ID")]
            [Required(ErrorMessage = "ID is required")]
    		public int  ID { get; set; }
    
    		    
    		/// <summary>
    		/// task ID
    		/// </summary>        
    	//    [DisplayName("task ID")]
            [Required(ErrorMessage = "task ID is required")]
    		public int  taskID { get; set; }
    
    		    
    		/// <summary>
    		/// lng
    		/// </summary>        
    	//    [DisplayName("lng")]
            [Required(ErrorMessage = "lng is required")]
    		public double  lng { get; set; }
    
    		    
    		/// <summary>
    		/// lat
    		/// </summary>        
    	//    [DisplayName("lat")]
            [Required(ErrorMessage = "lat is required")]
    		public double  lat { get; set; }
    
    		    
    		/// <summary>
    		/// Create By
    		/// </summary>        
    	//    [DisplayName("Create By")]
            [MaxLength(10, ErrorMessage = "Create By cannot be longer than 10 characters")]
    		public string  CreateBy { get; set; }
    
    		    
    		/// <summary>
    		/// Create Date
    		/// </summary>        
    	//    [DisplayName("Create Date")]
    		public Nullable<System.DateTime>  CreateDate { get; set; }
    
    		    
    		/// <summary>
    		/// Updated By
    		/// </summary>        
    	//    [DisplayName("Updated By")]
            [MaxLength(10, ErrorMessage = "Updated By cannot be longer than 10 characters")]
    		public string  UpdatedBy { get; set; }
    
    		    
    		/// <summary>
    		/// Updated Date
    		/// </summary>        
    	//    [DisplayName("Updated Date")]
    		public Nullable<System.DateTime>  UpdatedDate { get; set; }
    
    		    
    	}
    //}
    
}
