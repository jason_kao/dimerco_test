using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ValuePlus.ViewModel;



namespace ValuePlus.ViewModel.eSAM
{
    
    /// <summary>
    /// SAMNCUsed class
    /// </summary>
    //[MetadataType(typeof(SAMNCUsedViewModel))]
    //public  partial class SAMNCUsed
    //{
    
    	/// <summary>
    	/// SAMNCUsed Metadata class
    	/// </summary>
    	public   class SAMNCUsedViewModel:ViewModelBase
    	{
    		    
    		/// <summary>
    		/// ID
    		/// </summary>        
    	//    [DisplayName("ID")]
            [Required(ErrorMessage = "ID is required")]
    		public int  ID { get; set; }
    
    		    
    		/// <summary>
    		/// User ID
    		/// </summary>        
    	//    [DisplayName("User ID")]
            [MaxLength(10, ErrorMessage = "User ID cannot be longer than 10 characters")]
    		public string  UserID { get; set; }
    
    		    
    		/// <summary>
    		/// MNCID
    		/// </summary>        
    	//    [DisplayName("MNCID")]
    		public Nullable<int>  MNCID { get; set; }
    
    		    
    		/// <summary>
    		/// Updated Date
    		/// </summary>        
    	//    [DisplayName("Updated Date")]
    		public Nullable<System.DateTime>  UpdatedDate { get; set; }
    
    		    
    	}
    //}
    
}
