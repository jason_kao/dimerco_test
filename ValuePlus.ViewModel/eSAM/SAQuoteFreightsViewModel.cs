using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ValuePlus.ViewModel;



namespace ValuePlus.ViewModel.eSAM
{
    
    /// <summary>
    /// SAQuoteFreights class
    /// </summary>
    //[MetadataType(typeof(SAQuoteFreightsViewModel))]
    //public  partial class SAQuoteFreights
    //{
    
    	/// <summary>
    	/// SAQuoteFreights Metadata class
    	/// </summary>
    	public   class SAQuoteFreightsViewModel:ViewModelBase
    	{
    		    
    		/// <summary>
    		/// ID
    		/// </summary>        
    	//    [DisplayName("ID")]
            [Required(ErrorMessage = "ID is required")]
    		public int  ID { get; set; }
    
    		    
    		/// <summary>
    		/// Origin City ID
    		/// </summary>        
    	//    [DisplayName("Origin City ID")]
    		public Nullable<int>  OriginCityID { get; set; }
    
    		    
    		/// <summary>
    		/// Destination City ID
    		/// </summary>        
    	//    [DisplayName("Destination City ID")]
    		public Nullable<int>  DestinationCityID { get; set; }
    
    		    
    		/// <summary>
    		/// Carrier ID
    		/// </summary>        
    	//    [DisplayName("Carrier ID")]
    		public Nullable<int>  CarrierID { get; set; }
    
    		    
    		/// <summary>
    		/// Service Level
    		/// </summary>        
    	//    [DisplayName("Service Level")]
            [MaxLength(20, ErrorMessage = "Service Level cannot be longer than 20 characters")]
    		public string  ServiceLevel { get; set; }
    
    		    
    		/// <summary>
    		/// UOMID
    		/// </summary>        
    	//    [DisplayName("UOMID")]
    		public Nullable<int>  UOMID { get; set; }
    
    		    
    		/// <summary>
    		/// Currency ID
    		/// </summary>        
    	//    [DisplayName("Currency ID")]
    		public Nullable<int>  CurrencyID { get; set; }
    
    		    
    		/// <summary>
    		/// Weekly Allotment
    		/// </summary>        
    	//    [DisplayName("Weekly Allotment")]
            [MaxLength(20, ErrorMessage = "Weekly Allotment cannot be longer than 20 characters")]
    		public string  WeeklyAllotment { get; set; }
    
    		    
    		/// <summary>
    		/// Frequency
    		/// </summary>        
    	//    [DisplayName("Frequency")]
            [MaxLength(20, ErrorMessage = "Frequency cannot be longer than 20 characters")]
    		public string  Frequency { get; set; }
    
    		    
    		/// <summary>
    		/// Transit Time
    		/// </summary>        
    	//    [DisplayName("Transit Time")]
            [MaxLength(20, ErrorMessage = "Transit Time cannot be longer than 20 characters")]
    		public string  TransitTime { get; set; }
    
    		    
    		/// <summary>
    		/// Remark
    		/// </summary>        
    	//    [DisplayName("Remark")]
            [MaxLength(200, ErrorMessage = "Remark cannot be longer than 200 characters")]
    		public string  Remark { get; set; }
    
    		    
    		/// <summary>
    		/// Quote ID
    		/// </summary>        
    	//    [DisplayName("Quote ID")]
    		public Nullable<int>  QuoteID { get; set; }
    
    		    
    		/// <summary>
    		/// Selling Rate1
    		/// </summary>        
    	//    [DisplayName("Selling Rate1")]
            [MaxLength(20, ErrorMessage = "Selling Rate1 cannot be longer than 20 characters")]
    		public string  SellingRate1 { get; set; }
    
    		    
    		/// <summary>
    		/// Selling Rate2
    		/// </summary>        
    	//    [DisplayName("Selling Rate2")]
            [MaxLength(20, ErrorMessage = "Selling Rate2 cannot be longer than 20 characters")]
    		public string  SellingRate2 { get; set; }
    
    		    
    		/// <summary>
    		/// Selling Rate3
    		/// </summary>        
    	//    [DisplayName("Selling Rate3")]
            [MaxLength(20, ErrorMessage = "Selling Rate3 cannot be longer than 20 characters")]
    		public string  SellingRate3 { get; set; }
    
    		    
    		/// <summary>
    		/// Selling Rate4
    		/// </summary>        
    	//    [DisplayName("Selling Rate4")]
            [MaxLength(20, ErrorMessage = "Selling Rate4 cannot be longer than 20 characters")]
    		public string  SellingRate4 { get; set; }
    
    		    
    		/// <summary>
    		/// Selling Rate5
    		/// </summary>        
    	//    [DisplayName("Selling Rate5")]
            [MaxLength(20, ErrorMessage = "Selling Rate5 cannot be longer than 20 characters")]
    		public string  SellingRate5 { get; set; }
    
    		    
    		/// <summary>
    		/// Selling Rate6
    		/// </summary>        
    	//    [DisplayName("Selling Rate6")]
            [MaxLength(20, ErrorMessage = "Selling Rate6 cannot be longer than 20 characters")]
    		public string  SellingRate6 { get; set; }
    
    		    
    		/// <summary>
    		/// Selling Rate7
    		/// </summary>        
    	//    [DisplayName("Selling Rate7")]
            [MaxLength(20, ErrorMessage = "Selling Rate7 cannot be longer than 20 characters")]
    		public string  SellingRate7 { get; set; }
    
    		    
    		/// <summary>
    		/// Selling Rate8
    		/// </summary>        
    	//    [DisplayName("Selling Rate8")]
            [MaxLength(20, ErrorMessage = "Selling Rate8 cannot be longer than 20 characters")]
    		public string  SellingRate8 { get; set; }
    
    		    
    		/// <summary>
    		/// Selling Rate9
    		/// </summary>        
    	//    [DisplayName("Selling Rate9")]
            [MaxLength(20, ErrorMessage = "Selling Rate9 cannot be longer than 20 characters")]
    		public string  SellingRate9 { get; set; }
    
    		    
    		/// <summary>
    		/// Selling Rate10
    		/// </summary>        
    	//    [DisplayName("Selling Rate10")]
            [MaxLength(20, ErrorMessage = "Selling Rate10 cannot be longer than 20 characters")]
    		public string  SellingRate10 { get; set; }
    
    		    
    		/// <summary>
    		/// Commodity
    		/// </summary>        
    	//    [DisplayName("Commodity")]
            [MaxLength(50, ErrorMessage = "Commodity cannot be longer than 50 characters")]
    		public string  Commodity { get; set; }
    
    		    
    		/// <summary>
    		/// Ploading Port ID
    		/// </summary>        
    	//    [DisplayName("Ploading Port ID")]
    		public Nullable<int>  PloadingPortID { get; set; }
    
    		    
    		/// <summary>
    		/// PDischarge Port ID
    		/// </summary>        
    	//    [DisplayName("PDischarge Port ID")]
    		public Nullable<int>  PDischargePortID { get; set; }
    
    		    
    		/// <summary>
    		/// Created User ID
    		/// </summary>        
    	//    [DisplayName("Created User ID")]
            [MaxLength(10, ErrorMessage = "Created User ID cannot be longer than 10 characters")]
    		public string  CreatedUserID { get; set; }
    
    		    
    		/// <summary>
    		/// Created Date
    		/// </summary>        
    	//    [DisplayName("Created Date")]
    		public Nullable<System.DateTime>  CreatedDate { get; set; }
    
    		    
    		/// <summary>
    		/// Updated User ID
    		/// </summary>        
    	//    [DisplayName("Updated User ID")]
            [MaxLength(10, ErrorMessage = "Updated User ID cannot be longer than 10 characters")]
    		public string  UpdatedUserID { get; set; }
    
    		    
    		/// <summary>
    		/// Updated Date
    		/// </summary>        
    	//    [DisplayName("Updated Date")]
    		public Nullable<System.DateTime>  UpdatedDate { get; set; }
    
    		    
    		/// <summary>
    		/// Other Value1
    		/// </summary>        
    	//    [DisplayName("Other Value1")]
            [MaxLength(100, ErrorMessage = "Other Value1 cannot be longer than 100 characters")]
    		public string  OtherValue1 { get; set; }
    
    		    
    		/// <summary>
    		/// Other Value2
    		/// </summary>        
    	//    [DisplayName("Other Value2")]
            [MaxLength(100, ErrorMessage = "Other Value2 cannot be longer than 100 characters")]
    		public string  OtherValue2 { get; set; }
    
    		    
    		/// <summary>
    		/// Other Value3
    		/// </summary>        
    	//    [DisplayName("Other Value3")]
            [MaxLength(100, ErrorMessage = "Other Value3 cannot be longer than 100 characters")]
    		public string  OtherValue3 { get; set; }
    
    		    
    		/// <summary>
    		/// EIType
    		/// </summary>        
    	//    [DisplayName("EIType")]
            [MaxLength(10, ErrorMessage = "EIType cannot be longer than 10 characters")]
    		public string  EIType { get; set; }
    
    		    
    		/// <summary>
    		/// PPCC
    		/// </summary>        
    	//    [DisplayName("PPCC")]
            [MaxLength(5, ErrorMessage = "PPCC cannot be longer than 5 characters")]
    		public string  PPCC { get; set; }
    
    		    
    		/// <summary>
    		/// Nature Of Goods
    		/// </summary>        
    	//    [DisplayName("Nature Of Goods")]
            [MaxLength(50, ErrorMessage = "Nature Of Goods cannot be longer than 50 characters")]
    		public string  NatureOfGoods { get; set; }
    
    		    
    		/// <summary>
    		/// Freight Pty
    		/// </summary>        
    	//    [DisplayName("Freight Pty")]
    		public Nullable<int>  FreightPty { get; set; }
    
    		    
    		/// <summary>
    		/// Origin Pty
    		/// </summary>        
    	//    [DisplayName("Origin Pty")]
    		public Nullable<int>  OriginPty { get; set; }
    
    		    
    		/// <summary>
    		/// Dest Pty
    		/// </summary>        
    	//    [DisplayName("Dest Pty")]
    		public Nullable<int>  DestPty { get; set; }
    
    		    
    		/// <summary>
    		/// FLead Owner
    		/// </summary>        
    	//    [DisplayName("FLead Owner")]
            [MaxLength(6, ErrorMessage = "FLead Owner cannot be longer than 6 characters")]
    		public string  FLeadOwner { get; set; }
    
    		    
    		/// <summary>
    		/// FSecured DT
    		/// </summary>        
    	//    [DisplayName("FSecured DT")]
    		public Nullable<System.DateTime>  FSecuredDT { get; set; }
    
    		    
    		/// <summary>
    		/// Est Tonnage
    		/// </summary>        
    	//    [DisplayName("Est Tonnage")]
            [MaxLength(20, ErrorMessage = "Est Tonnage cannot be longer than 20 characters")]
    		public string  EstTonnage { get; set; }
    
    		    
    		/// <summary>
    		/// Est Revenue
    		/// </summary>        
    	//    [DisplayName("Est Revenue")]
            [MaxLength(20, ErrorMessage = "Est Revenue cannot be longer than 20 characters")]
    		public string  EstRevenue { get; set; }
    
    		    
    		/// <summary>
    		/// Est GP
    		/// </summary>        
    	//    [DisplayName("Est GP")]
            [MaxLength(20, ErrorMessage = "Est GP cannot be longer than 20 characters")]
    		public string  EstGP { get; set; }
    
    		    
    		/// <summary>
    		/// Group ID
    		/// </summary>        
    	//    [DisplayName("Group ID")]
    		public Nullable<int>  GroupID { get; set; }
    
    		    
    		/// <summary>
    		/// FShpt No
    		/// </summary>        
    	//    [DisplayName("FShpt No")]
            [MaxLength(20, ErrorMessage = "FShpt No cannot be longer than 20 characters")]
    		public string  FShptNo { get; set; }
    
    		    
    		/// <summary>
    		/// FStation Id
    		/// </summary>        
    	//    [DisplayName("FStation Id")]
            [MaxLength(10, ErrorMessage = "FStation Id cannot be longer than 10 characters")]
    		public string  FStationId { get; set; }
    
    		    
    		/// <summary>
    		/// FLot No
    		/// </summary>        
    	//    [DisplayName("FLot No")]
            [MaxLength(20, ErrorMessage = "FLot No cannot be longer than 20 characters")]
    		public string  FLotNo { get; set; }
    
    		    
    		/// <summary>
    		/// City Group ID
    		/// </summary>        
    	//    [DisplayName("City Group ID")]
    		public Nullable<int>  CityGroupID { get; set; }
    
    		    
    		/// <summary>
    		/// CBMTo WT
    		/// </summary>        
    	//    [DisplayName("CBMTo WT")]
            [MaxLength(20, ErrorMessage = "CBMTo WT cannot be longer than 20 characters")]
    		public string  CBMToWT { get; set; }
    
    		    
    		/// <summary>
    		/// CBMTo WTUOMID
    		/// </summary>        
    	//    [DisplayName("CBMTo WTUOMID")]
    		public Nullable<int>  CBMToWTUOMID { get; set; }
    
    		    
    		/// <summary>
    		/// LSecured DT
    		/// </summary>        
    	//    [DisplayName("LSecured DT")]
    		public Nullable<System.DateTime>  LSecuredDT { get; set; }
    
    		    
    	}
    //}
    
}
