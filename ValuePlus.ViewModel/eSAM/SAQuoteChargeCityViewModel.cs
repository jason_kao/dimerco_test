using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ValuePlus.ViewModel;



namespace ValuePlus.ViewModel.eSAM
{
    
    /// <summary>
    /// SAQuoteChargeCity class
    /// </summary>
    //[MetadataType(typeof(SAQuoteChargeCityViewModel))]
    //public  partial class SAQuoteChargeCity
    //{
    
    	/// <summary>
    	/// SAQuoteChargeCity Metadata class
    	/// </summary>
    	public   class SAQuoteChargeCityViewModel:ViewModelBase
    	{
    		    
    		/// <summary>
    		/// ID
    		/// </summary>        
    	//    [DisplayName("ID")]
            [Required(ErrorMessage = "ID is required")]
    		public int  ID { get; set; }
    
    		    
    		/// <summary>
    		/// City ID
    		/// </summary>        
    	//    [DisplayName("City ID")]
    		public Nullable<int>  CityID { get; set; }
    
    		    
    		/// <summary>
    		/// Quote ID
    		/// </summary>        
    	//    [DisplayName("Quote ID")]
    		public Nullable<int>  QuoteID { get; set; }
    
    		    
    		/// <summary>
    		/// Currency ID
    		/// </summary>        
    	//    [DisplayName("Currency ID")]
    		public Nullable<int>  CurrencyID { get; set; }
    
    		    
    		/// <summary>
    		/// UOMID
    		/// </summary>        
    	//    [DisplayName("UOMID")]
    		public Nullable<int>  UOMID { get; set; }
    
    		    
    		/// <summary>
    		/// City Type
    		/// </summary>        
    	//    [DisplayName("City Type")]
            [MaxLength(10, ErrorMessage = "City Type cannot be longer than 10 characters")]
    		public string  CityType { get; set; }
    
    		    
    		/// <summary>
    		/// Charge Type
    		/// </summary>        
    	//    [DisplayName("Charge Type")]
            [MaxLength(10, ErrorMessage = "Charge Type cannot be longer than 10 characters")]
    		public string  ChargeType { get; set; }
    
    		    
    		/// <summary>
    		/// Created User ID
    		/// </summary>        
    	//    [DisplayName("Created User ID")]
            [MaxLength(10, ErrorMessage = "Created User ID cannot be longer than 10 characters")]
    		public string  CreatedUserID { get; set; }
    
    		    
    		/// <summary>
    		/// Created Date
    		/// </summary>        
    	//    [DisplayName("Created Date")]
    		public Nullable<System.DateTime>  CreatedDate { get; set; }
    
    		    
    		/// <summary>
    		/// Updated User ID
    		/// </summary>        
    	//    [DisplayName("Updated User ID")]
            [MaxLength(10, ErrorMessage = "Updated User ID cannot be longer than 10 characters")]
    		public string  UpdatedUserID { get; set; }
    
    		    
    		/// <summary>
    		/// Updated Date
    		/// </summary>        
    	//    [DisplayName("Updated Date")]
    		public Nullable<System.DateTime>  UpdatedDate { get; set; }
    
    		    
    		/// <summary>
    		/// s Type
    		/// </summary>        
    	//    [DisplayName("s Type")]
            [MaxLength(20, ErrorMessage = "s Type cannot be longer than 20 characters")]
    		public string  sType { get; set; }
    
    		    
    		/// <summary>
    		/// Origin City ID
    		/// </summary>        
    	//    [DisplayName("Origin City ID")]
    		public Nullable<int>  OriginCityID { get; set; }
    
    		    
    		/// <summary>
    		/// Destination City ID
    		/// </summary>        
    	//    [DisplayName("Destination City ID")]
    		public Nullable<int>  DestinationCityID { get; set; }
    
    		    
    		/// <summary>
    		/// POL
    		/// </summary>        
    	//    [DisplayName("POL")]
    		public Nullable<int>  POL { get; set; }
    
    		    
    		/// <summary>
    		/// POD
    		/// </summary>        
    	//    [DisplayName("POD")]
    		public Nullable<int>  POD { get; set; }
    
    		    
    		/// <summary>
    		/// Method
    		/// </summary>        
    	//    [DisplayName("Method")]
            [MaxLength(50, ErrorMessage = "Method cannot be longer than 50 characters")]
    		public string  Method { get; set; }
    
    		    
    		/// <summary>
    		/// Group ID
    		/// </summary>        
    	//    [DisplayName("Group ID")]
    		public Nullable<int>  GroupID { get; set; }
    
    		    
    	}
    //}
    
}
