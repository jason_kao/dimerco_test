using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ValuePlus.ViewModel;



namespace ValuePlus.ViewModel.eSAM
{
    
    /// <summary>
    /// SASCItems class
    /// </summary>
    //[MetadataType(typeof(SASCItemsViewModel))]
    //public  partial class SASCItems
    //{
    
    	/// <summary>
    	/// SASCItems Metadata class
    	/// </summary>
    	public   class SASCItemsViewModel:ViewModelBase
    	{
    		    
    		/// <summary>
    		/// HQID
    		/// </summary>        
    	//    [DisplayName("HQID")]
            [Required(ErrorMessage = "HQID is required")]
    		public int  HQID { get; set; }
    
    		    
    		/// <summary>
    		/// Group ID
    		/// </summary>        
    	//    [DisplayName("Group ID")]
    		public Nullable<int>  GroupID { get; set; }
    
    		    
    		/// <summary>
    		/// Item Category ID
    		/// </summary>        
    	//    [DisplayName("Item Category ID")]
    		public Nullable<int>  ItemCategoryID { get; set; }
    
    		    
    		/// <summary>
    		/// Caculate Type ID
    		/// </summary>        
    	//    [DisplayName("Caculate Type ID")]
    		public Nullable<int>  CaculateTypeID { get; set; }
    
    		    
    		/// <summary>
    		/// Item Type ID
    		/// </summary>        
    	//    [DisplayName("Item Type ID")]
    		public Nullable<int>  ItemTypeID { get; set; }
    
    		    
    		/// <summary>
    		/// Customer Type ID
    		/// </summary>        
    	//    [DisplayName("Customer Type ID")]
    		public Nullable<int>  CustomerTypeID { get; set; }
    
    		    
    		/// <summary>
    		/// Mode Type
    		/// </summary>        
    	//    [DisplayName("Mode Type")]
            [MaxLength(10, ErrorMessage = "Mode Type cannot be longer than 10 characters")]
    		public string  ModeType { get; set; }
    
    		    
    		/// <summary>
    		/// Item Name
    		/// </summary>        
    	//    [DisplayName("Item Name")]
            [MaxLength(50, ErrorMessage = "Item Name cannot be longer than 50 characters")]
    		public string  ItemName { get; set; }
    
    		    
    		/// <summary>
    		/// Score
    		/// </summary>        
    	//    [DisplayName("Score")]
    		public Nullable<double>  Score { get; set; }
    
    		    
    		/// <summary>
    		/// Target
    		/// </summary>        
    	//    [DisplayName("Target")]
    		public Nullable<double>  Target { get; set; }
    
    		    
    		/// <summary>
    		/// is Month Target
    		/// </summary>        
    	//    [DisplayName("is Month Target")]
    		public Nullable<bool>  isMonthTarget { get; set; }
    
    		    
    		/// <summary>
    		/// Month Target1
    		/// </summary>        
    	//    [DisplayName("Month Target1")]
    		public Nullable<double>  MonthTarget1 { get; set; }
    
    		    
    		/// <summary>
    		/// Month Target2
    		/// </summary>        
    	//    [DisplayName("Month Target2")]
    		public Nullable<double>  MonthTarget2 { get; set; }
    
    		    
    		/// <summary>
    		/// Month Target3
    		/// </summary>        
    	//    [DisplayName("Month Target3")]
    		public Nullable<double>  MonthTarget3 { get; set; }
    
    		    
    		/// <summary>
    		/// Month Target4
    		/// </summary>        
    	//    [DisplayName("Month Target4")]
    		public Nullable<double>  MonthTarget4 { get; set; }
    
    		    
    		/// <summary>
    		/// Month Target5
    		/// </summary>        
    	//    [DisplayName("Month Target5")]
    		public Nullable<double>  MonthTarget5 { get; set; }
    
    		    
    		/// <summary>
    		/// Month Target6
    		/// </summary>        
    	//    [DisplayName("Month Target6")]
    		public Nullable<double>  MonthTarget6 { get; set; }
    
    		    
    		/// <summary>
    		/// Month Target7
    		/// </summary>        
    	//    [DisplayName("Month Target7")]
    		public Nullable<double>  MonthTarget7 { get; set; }
    
    		    
    		/// <summary>
    		/// Month Target8
    		/// </summary>        
    	//    [DisplayName("Month Target8")]
    		public Nullable<double>  MonthTarget8 { get; set; }
    
    		    
    		/// <summary>
    		/// Month Target9
    		/// </summary>        
    	//    [DisplayName("Month Target9")]
    		public Nullable<double>  MonthTarget9 { get; set; }
    
    		    
    		/// <summary>
    		/// Month Target10
    		/// </summary>        
    	//    [DisplayName("Month Target10")]
    		public Nullable<double>  MonthTarget10 { get; set; }
    
    		    
    		/// <summary>
    		/// Month Target11
    		/// </summary>        
    	//    [DisplayName("Month Target11")]
    		public Nullable<double>  MonthTarget11 { get; set; }
    
    		    
    		/// <summary>
    		/// Month Target12
    		/// </summary>        
    	//    [DisplayName("Month Target12")]
    		public Nullable<double>  MonthTarget12 { get; set; }
    
    		    
    		/// <summary>
    		/// is Score Rules
    		/// </summary>        
    	//    [DisplayName("is Score Rules")]
    		public Nullable<bool>  isScoreRules { get; set; }
    
    		    
    		/// <summary>
    		/// Score Rules ID
    		/// </summary>        
    	//    [DisplayName("Score Rules ID")]
    		public Nullable<int>  ScoreRulesID { get; set; }
    
    		    
    		/// <summary>
    		/// Status
    		/// </summary>        
    	//    [DisplayName("Status")]
            [MaxLength(10, ErrorMessage = "Status cannot be longer than 10 characters")]
    		public string  Status { get; set; }
    
    		    
    		/// <summary>
    		/// Created Date
    		/// </summary>        
    	//    [DisplayName("Created Date")]
    		public Nullable<System.DateTime>  CreatedDate { get; set; }
    
    		    
    		/// <summary>
    		/// Created By
    		/// </summary>        
    	//    [DisplayName("Created By")]
            [MaxLength(10, ErrorMessage = "Created By cannot be longer than 10 characters")]
    		public string  CreatedBy { get; set; }
    
    		    
    		/// <summary>
    		/// Updated Date
    		/// </summary>        
    	//    [DisplayName("Updated Date")]
    		public Nullable<System.DateTime>  UpdatedDate { get; set; }
    
    		    
    		/// <summary>
    		/// Updated By
    		/// </summary>        
    	//    [DisplayName("Updated By")]
            [MaxLength(10, ErrorMessage = "Updated By cannot be longer than 10 characters")]
    		public string  UpdatedBy { get; set; }
    
    		    
    	}
    //}
    
}
