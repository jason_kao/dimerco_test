using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ValuePlus.ViewModel;



namespace ValuePlus.ViewModel.eSAM
{
    
    /// <summary>
    /// SASerialNumber class
    /// </summary>
    //[MetadataType(typeof(SASerialNumberViewModel))]
    //public  partial class SASerialNumber
    //{
    
    	/// <summary>
    	/// SASerialNumber Metadata class
    	/// </summary>
    	public   class SASerialNumberViewModel:ViewModelBase
    	{
    		    
    		/// <summary>
    		/// Station Code
    		/// </summary>        
    	//    [DisplayName("Station Code")]
            [Required(ErrorMessage = "Station Code is required")]
            [MaxLength(10, ErrorMessage = "Station Code cannot be longer than 10 characters")]
    		public string  StationCode { get; set; }
    
    		    
    		/// <summary>
    		/// Next AIRCredit No
    		/// </summary>        
    	//    [DisplayName("Next AIRCredit No")]
    		public Nullable<int>  NextAIRCreditNo { get; set; }
    
    		    
    		/// <summary>
    		/// Next AIRQuote No
    		/// </summary>        
    	//    [DisplayName("Next AIRQuote No")]
    		public Nullable<int>  NextAIRQuoteNo { get; set; }
    
    		    
    		/// <summary>
    		/// Next SEACredit No
    		/// </summary>        
    	//    [DisplayName("Next SEACredit No")]
    		public Nullable<int>  NextSEACreditNo { get; set; }
    
    		    
    		/// <summary>
    		/// Next SEAQuote No
    		/// </summary>        
    	//    [DisplayName("Next SEAQuote No")]
    		public Nullable<int>  NextSEAQuoteNo { get; set; }
    
    		    
    		/// <summary>
    		/// Next WMSCredit No
    		/// </summary>        
    	//    [DisplayName("Next WMSCredit No")]
    		public Nullable<int>  NextWMSCreditNo { get; set; }
    
    		    
    		/// <summary>
    		/// Next WMSQuote No
    		/// </summary>        
    	//    [DisplayName("Next WMSQuote No")]
    		public Nullable<int>  NextWMSQuoteNo { get; set; }
    
    		    
    		/// <summary>
    		/// Next DASCredit No
    		/// </summary>        
    	//    [DisplayName("Next DASCredit No")]
    		public Nullable<int>  NextDASCreditNo { get; set; }
    
    		    
    		/// <summary>
    		/// Next DASQuote No
    		/// </summary>        
    	//    [DisplayName("Next DASQuote No")]
    		public Nullable<int>  NextDASQuoteNo { get; set; }
    
    		    
    		/// <summary>
    		/// Created User ID
    		/// </summary>        
    	//    [DisplayName("Created User ID")]
            [MaxLength(10, ErrorMessage = "Created User ID cannot be longer than 10 characters")]
    		public string  CreatedUserID { get; set; }
    
    		    
    		/// <summary>
    		/// Created Date
    		/// </summary>        
    	//    [DisplayName("Created Date")]
    		public Nullable<System.DateTime>  CreatedDate { get; set; }
    
    		    
    		/// <summary>
    		/// Updated User ID
    		/// </summary>        
    	//    [DisplayName("Updated User ID")]
            [MaxLength(10, ErrorMessage = "Updated User ID cannot be longer than 10 characters")]
    		public string  UpdatedUserID { get; set; }
    
    		    
    		/// <summary>
    		/// Updated Date
    		/// </summary>        
    	//    [DisplayName("Updated Date")]
    		public Nullable<System.DateTime>  UpdatedDate { get; set; }
    
    		    
    		/// <summary>
    		/// Next MTSCredit No
    		/// </summary>        
    	//    [DisplayName("Next MTSCredit No")]
    		public Nullable<int>  NextMTSCreditNo { get; set; }
    
    		    
    		/// <summary>
    		/// Next MTSQuote No
    		/// </summary>        
    	//    [DisplayName("Next MTSQuote No")]
    		public Nullable<int>  NextMTSQuoteNo { get; set; }
    
    		    
    		/// <summary>
    		/// Next TMSCredit No
    		/// </summary>        
    	//    [DisplayName("Next TMSCredit No")]
    		public Nullable<int>  NextTMSCreditNo { get; set; }
    
    		    
    		/// <summary>
    		/// Next TMSQuote No
    		/// </summary>        
    	//    [DisplayName("Next TMSQuote No")]
    		public Nullable<int>  NextTMSQuoteNo { get; set; }
    
    		    
    	}
    //}
    
}
