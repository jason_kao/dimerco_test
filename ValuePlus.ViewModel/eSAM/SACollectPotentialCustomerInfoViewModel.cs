using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ValuePlus.ViewModel;



namespace ValuePlus.ViewModel.eSAM
{
    
    /// <summary>
    /// SACollectPotentialCustomerInfo class
    /// </summary>
    //[MetadataType(typeof(SACollectPotentialCustomerInfoViewModel))]
    //public  partial class SACollectPotentialCustomerInfo
    //{
    
    	/// <summary>
    	/// SACollectPotentialCustomerInfo Metadata class
    	/// </summary>
    	public   class SACollectPotentialCustomerInfoViewModel:ViewModelBase
    	{
    		    
    		/// <summary>
    		/// ID
    		/// </summary>        
    	//    [DisplayName("ID")]
            [Required(ErrorMessage = "ID is required")]
    		public int  ID { get; set; }
    
    		    
    		/// <summary>
    		/// Type
    		/// </summary>        
    	//    [DisplayName("Type")]
            [MaxLength(255, ErrorMessage = "Type cannot be longer than 255 characters")]
    		public string  Type { get; set; }
    
    		    
    		/// <summary>
    		/// Company
    		/// </summary>        
    	//    [DisplayName("Company")]
            [MaxLength(255, ErrorMessage = "Company cannot be longer than 255 characters")]
    		public string  Company { get; set; }
    
    		    
    		/// <summary>
    		/// Contact Name
    		/// </summary>        
    	//    [DisplayName("Contact Name")]
            [MaxLength(255, ErrorMessage = "Contact Name cannot be longer than 255 characters")]
    		public string  ContactName { get; set; }
    
    		    
    		/// <summary>
    		/// Contact Job Title
    		/// </summary>        
    	//    [DisplayName("Contact Job Title")]
            [MaxLength(255, ErrorMessage = "Contact Job Title cannot be longer than 255 characters")]
    		public string  ContactJobTitle { get; set; }
    
    		    
    		/// <summary>
    		/// Country
    		/// </summary>        
    	//    [DisplayName("Country")]
    		public Nullable<int>  Country { get; set; }
    
    		    
    		/// <summary>
    		/// State
    		/// </summary>        
    	//    [DisplayName("State")]
    		public Nullable<int>  State { get; set; }
    
    		    
    		/// <summary>
    		/// City
    		/// </summary>        
    	//    [DisplayName("City")]
    		public Nullable<int>  City { get; set; }
    
    		    
    		/// <summary>
    		/// Zip
    		/// </summary>        
    	//    [DisplayName("Zip")]
    		public Nullable<int>  Zip { get; set; }
    
    		    
    		/// <summary>
    		/// Address
    		/// </summary>        
    	//    [DisplayName("Address")]
            [MaxLength(255, ErrorMessage = "Address cannot be longer than 255 characters")]
    		public string  Address { get; set; }
    
    		    
    		/// <summary>
    		/// Contact Email
    		/// </summary>        
    	//    [DisplayName("Contact Email")]
            [MaxLength(255, ErrorMessage = "Contact Email cannot be longer than 255 characters")]
    		public string  ContactEmail { get; set; }
    
    		    
    		/// <summary>
    		/// Contact Phone
    		/// </summary>        
    	//    [DisplayName("Contact Phone")]
            [MaxLength(255, ErrorMessage = "Contact Phone cannot be longer than 255 characters")]
    		public string  ContactPhone { get; set; }
    
    		    
    		/// <summary>
    		/// Contact Fax
    		/// </summary>        
    	//    [DisplayName("Contact Fax")]
            [MaxLength(255, ErrorMessage = "Contact Fax cannot be longer than 255 characters")]
    		public string  ContactFax { get; set; }
    
    		    
    		/// <summary>
    		/// Industry ID
    		/// </summary>        
    	//    [DisplayName("Industry ID")]
    		public Nullable<int>  IndustryID { get; set; }
    
    		    
    		/// <summary>
    		/// Service Type
    		/// </summary>        
    	//    [DisplayName("Service Type")]
            [MaxLength(255, ErrorMessage = "Service Type cannot be longer than 255 characters")]
    		public string  ServiceType { get; set; }
    
    		    
    		/// <summary>
    		/// Service Source
    		/// </summary>        
    	//    [DisplayName("Service Source")]
            [MaxLength(255, ErrorMessage = "Service Source cannot be longer than 255 characters")]
    		public string  ServiceSource { get; set; }
    
    		    
    		/// <summary>
    		/// Contact Subject
    		/// </summary>        
    	//    [DisplayName("Contact Subject")]
            [MaxLength(255, ErrorMessage = "Contact Subject cannot be longer than 255 characters")]
    		public string  ContactSubject { get; set; }
    
    		    
    		/// <summary>
    		/// Comments
    		/// </summary>        
    	//    [DisplayName("Comments")]
    		public string  Comments { get; set; }
    
    		    
    		/// <summary>
    		/// Created By
    		/// </summary>        
    	//    [DisplayName("Created By")]
            [MaxLength(255, ErrorMessage = "Created By cannot be longer than 255 characters")]
    		public string  CreatedBy { get; set; }
    
    		    
    		/// <summary>
    		/// Created Date
    		/// </summary>        
    	//    [DisplayName("Created Date")]
    		public Nullable<System.DateTime>  CreatedDate { get; set; }
    
    		    
    		/// <summary>
    		/// Updated By
    		/// </summary>        
    	//    [DisplayName("Updated By")]
            [MaxLength(255, ErrorMessage = "Updated By cannot be longer than 255 characters")]
    		public string  UpdatedBy { get; set; }
    
    		    
    		/// <summary>
    		/// Updated Date
    		/// </summary>        
    	//    [DisplayName("Updated Date")]
    		public Nullable<System.DateTime>  UpdatedDate { get; set; }
    
    		    
    		/// <summary>
    		/// PLID
    		/// </summary>        
    	//    [DisplayName("PLID")]
    		public Nullable<int>  PLID { get; set; }
    
    		    
    		/// <summary>
    		/// is Feed
    		/// </summary>        
    	//    [DisplayName("is Feed")]
            [MaxLength(1, ErrorMessage = "is Feed cannot be longer than 1 characters")]
    		public string  isFeed { get; set; }
    
    		    
    	}
    //}
    
}
