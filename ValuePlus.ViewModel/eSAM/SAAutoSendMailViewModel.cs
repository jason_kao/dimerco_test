using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ValuePlus.ViewModel;



namespace ValuePlus.ViewModel.eSAM
{
    
    /// <summary>
    /// SAAutoSendMail class
    /// </summary>
    //[MetadataType(typeof(SAAutoSendMailViewModel))]
    //public  partial class SAAutoSendMail
    //{
    
    	/// <summary>
    	/// SAAutoSendMail Metadata class
    	/// </summary>
    	public   class SAAutoSendMailViewModel:ViewModelBase
    	{
    		    
    		/// <summary>
    		/// ID
    		/// </summary>        
    	//    [DisplayName("ID")]
            [Required(ErrorMessage = "ID is required")]
    		public int  ID { get; set; }
    
    		    
    		/// <summary>
    		/// Sales Task ID
    		/// </summary>        
    	//    [DisplayName("Sales Task ID")]
    		public Nullable<int>  SalesTaskID { get; set; }
    
    		    
    		/// <summary>
    		/// Created Date
    		/// </summary>        
    	//    [DisplayName("Created Date")]
    		public Nullable<System.DateTime>  CreatedDate { get; set; }
    
    		    
    	}
    //}
    
}
