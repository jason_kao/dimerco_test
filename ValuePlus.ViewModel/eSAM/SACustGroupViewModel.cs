using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ValuePlus.ViewModel;



namespace ValuePlus.ViewModel.eSAM
{
    
    /// <summary>
    /// SACustGroup class
    /// </summary>
    //[MetadataType(typeof(SACustGroupViewModel))]
    //public  partial class SACustGroup
    //{
    
    	/// <summary>
    	/// SACustGroup Metadata class
    	/// </summary>
    	public   class SACustGroupViewModel:ViewModelBase
    	{
    		    
    		/// <summary>
    		/// ID
    		/// </summary>        
    	//    [DisplayName("ID")]
            [Required(ErrorMessage = "ID is required")]
    		public int  ID { get; set; }
    
    		    
    		/// <summary>
    		/// Parent ID
    		/// </summary>        
    	//    [DisplayName("Parent ID")]
    		public Nullable<int>  ParentID { get; set; }
    
    		    
    		/// <summary>
    		/// Customer ID
    		/// </summary>        
    	//    [DisplayName("Customer ID")]
    		public Nullable<int>  CustomerID { get; set; }
    
    		    
    		/// <summary>
    		/// Depttype
    		/// </summary>        
    	//    [DisplayName("Depttype")]
            [Required(ErrorMessage = "Depttype is required")]
            [MaxLength(10, ErrorMessage = "Depttype cannot be longer than 10 characters")]
    		public string  Depttype { get; set; }
    
    		    
    		/// <summary>
    		/// Created By
    		/// </summary>        
    	//    [DisplayName("Created By")]
            [MaxLength(10, ErrorMessage = "Created By cannot be longer than 10 characters")]
    		public string  CreatedBy { get; set; }
    
    		    
    		/// <summary>
    		/// Created Date
    		/// </summary>        
    	//    [DisplayName("Created Date")]
    		public Nullable<System.DateTime>  CreatedDate { get; set; }
    
    		    
    		/// <summary>
    		/// Updated By
    		/// </summary>        
    	//    [DisplayName("Updated By")]
            [MaxLength(10, ErrorMessage = "Updated By cannot be longer than 10 characters")]
    		public string  UpdatedBy { get; set; }
    
    		    
    		/// <summary>
    		/// Updated Date
    		/// </summary>        
    	//    [DisplayName("Updated Date")]
    		public Nullable<System.DateTime>  UpdatedDate { get; set; }
    
    		    
    		/// <summary>
    		/// s Type
    		/// </summary>        
    	//    [DisplayName("s Type")]
            [MaxLength(10, ErrorMessage = "s Type cannot be longer than 10 characters")]
    		public string  sType { get; set; }
    
    		    
    	}
    //}
    
}
