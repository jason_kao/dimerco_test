using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ValuePlus.ViewModel;



namespace ValuePlus.ViewModel.eSAM
{
    
    /// <summary>
    /// SAQuoteTerms class
    /// </summary>
    //[MetadataType(typeof(SAQuoteTermsViewModel))]
    //public  partial class SAQuoteTerms
    //{
    
    	/// <summary>
    	/// SAQuoteTerms Metadata class
    	/// </summary>
    	public   class SAQuoteTermsViewModel:ViewModelBase
    	{
    		    
    		/// <summary>
    		/// ID
    		/// </summary>        
    	//    [DisplayName("ID")]
            [Required(ErrorMessage = "ID is required")]
    		public int  ID { get; set; }
    
    		    
    		/// <summary>
    		/// Title
    		/// </summary>        
    	//    [DisplayName("Title")]
            [MaxLength(255, ErrorMessage = "Title cannot be longer than 255 characters")]
    		public string  Title { get; set; }
    
    		    
    		/// <summary>
    		/// Contents
    		/// </summary>        
    	//    [DisplayName("Contents")]
    		public string  Contents { get; set; }
    
    		    
    		/// <summary>
    		/// Is Selected
    		/// </summary>        
    	//    [DisplayName("Is Selected")]
            [MaxLength(10, ErrorMessage = "Is Selected cannot be longer than 10 characters")]
    		public string  IsSelected { get; set; }
    
    		    
    		/// <summary>
    		/// Quote No
    		/// </summary>        
    	//    [DisplayName("Quote No")]
            [MaxLength(20, ErrorMessage = "Quote No cannot be longer than 20 characters")]
    		public string  QuoteNo { get; set; }
    
    		    
    		/// <summary>
    		/// Attachments
    		/// </summary>        
    	//    [DisplayName("Attachments")]
    		public string  Attachments { get; set; }
    
    		    
    		/// <summary>
    		/// Quote ID
    		/// </summary>        
    	//    [DisplayName("Quote ID")]
    		public Nullable<int>  QuoteID { get; set; }
    
    		    
    		/// <summary>
    		/// Term Order
    		/// </summary>        
    	//    [DisplayName("Term Order")]
    		public Nullable<int>  TermOrder { get; set; }
    
    		    
    	}
    //}
    
}
