using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ValuePlus.ViewModel;



namespace ValuePlus.ViewModel.eSAM
{
    
    /// <summary>
    /// SAOneOffShipmentInfo class
    /// </summary>
    //[MetadataType(typeof(SAOneOffShipmentInfoViewModel))]
    //public  partial class SAOneOffShipmentInfo
    //{
    
    	/// <summary>
    	/// SAOneOffShipmentInfo Metadata class
    	/// </summary>
    	public   class SAOneOffShipmentInfoViewModel:ViewModelBase
    	{
    		    
    		/// <summary>
    		/// ID
    		/// </summary>        
    	//    [DisplayName("ID")]
            [Required(ErrorMessage = "ID is required")]
    		public int  ID { get; set; }
    
    		    
    		/// <summary>
    		/// GWT
    		/// </summary>        
    	//    [DisplayName("GWT")]
            [MaxLength(50, ErrorMessage = "GWT cannot be longer than 50 characters")]
    		public string  GWT { get; set; }
    
    		    
    		/// <summary>
    		/// VWT
    		/// </summary>        
    	//    [DisplayName("VWT")]
            [MaxLength(50, ErrorMessage = "VWT cannot be longer than 50 characters")]
    		public string  VWT { get; set; }
    
    		    
    		/// <summary>
    		/// CWT
    		/// </summary>        
    	//    [DisplayName("CWT")]
            [MaxLength(50, ErrorMessage = "CWT cannot be longer than 50 characters")]
    		public string  CWT { get; set; }
    
    		    
    		/// <summary>
    		/// PCS
    		/// </summary>        
    	//    [DisplayName("PCS")]
            [MaxLength(50, ErrorMessage = "PCS cannot be longer than 50 characters")]
    		public string  PCS { get; set; }
    
    		    
    		/// <summary>
    		/// Pick Addr
    		/// </summary>        
    	//    [DisplayName("Pick Addr")]
            [MaxLength(200, ErrorMessage = "Pick Addr cannot be longer than 200 characters")]
    		public string  PickAddr { get; set; }
    
    		    
    		/// <summary>
    		/// Dev Addr
    		/// </summary>        
    	//    [DisplayName("Dev Addr")]
            [MaxLength(200, ErrorMessage = "Dev Addr cannot be longer than 200 characters")]
    		public string  DevAddr { get; set; }
    
    		    
    		/// <summary>
    		/// Dimension
    		/// </summary>        
    	//    [DisplayName("Dimension")]
    		public string  Dimension { get; set; }
    
    		    
    		/// <summary>
    		/// Quote ID
    		/// </summary>        
    	//    [DisplayName("Quote ID")]
            [Required(ErrorMessage = "Quote ID is required")]
    		public int  QuoteID { get; set; }
    
    		    
    		/// <summary>
    		/// Created User ID
    		/// </summary>        
    	//    [DisplayName("Created User ID")]
            [MaxLength(10, ErrorMessage = "Created User ID cannot be longer than 10 characters")]
    		public string  CreatedUserID { get; set; }
    
    		    
    		/// <summary>
    		/// Created Date
    		/// </summary>        
    	//    [DisplayName("Created Date")]
    		public Nullable<System.DateTime>  CreatedDate { get; set; }
    
    		    
    		/// <summary>
    		/// Updated User ID
    		/// </summary>        
    	//    [DisplayName("Updated User ID")]
            [MaxLength(10, ErrorMessage = "Updated User ID cannot be longer than 10 characters")]
    		public string  UpdatedUserID { get; set; }
    
    		    
    		/// <summary>
    		/// Updated Date
    		/// </summary>        
    	//    [DisplayName("Updated Date")]
    		public Nullable<System.DateTime>  UpdatedDate { get; set; }
    
    		    
    		/// <summary>
    		/// Customer Ref
    		/// </summary>        
    	//    [DisplayName("Customer Ref")]
            [MaxLength(200, ErrorMessage = "Customer Ref cannot be longer than 200 characters")]
    		public string  CustomerRef { get; set; }
    
    		    
    		/// <summary>
    		/// Lane Segment
    		/// </summary>        
    	//    [DisplayName("Lane Segment")]
            [MaxLength(200, ErrorMessage = "Lane Segment cannot be longer than 200 characters")]
    		public string  LaneSegment { get; set; }
    
    		    
    	}
    //}
    
}
