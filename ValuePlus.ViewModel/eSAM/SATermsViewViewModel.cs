using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ValuePlus.ViewModel;



namespace ValuePlus.ViewModel.eSAM
{
    
    /// <summary>
    /// SATermsView class
    /// </summary>
    //[MetadataType(typeof(SATermsViewViewModel))]
    //public  partial class SATermsView
    //{
    
    	/// <summary>
    	/// SATermsView Metadata class
    	/// </summary>
    	public   class SATermsViewViewModel:ViewModelBase
    	{
    		    
    		/// <summary>
    		/// Term ID
    		/// </summary>        
    	//    [DisplayName("Term ID")]
            [Required(ErrorMessage = "Term ID is required")]
    		public int  TermID { get; set; }
    
    		    
    		/// <summary>
    		/// Product Line
    		/// </summary>        
    	//    [DisplayName("Product Line")]
            [MaxLength(50, ErrorMessage = "Product Line cannot be longer than 50 characters")]
    		public string  ProductLine { get; set; }
    
    		    
    		/// <summary>
    		/// Todepts
    		/// </summary>        
    	//    [DisplayName("Todepts")]
            [MaxLength(50, ErrorMessage = "Todepts cannot be longer than 50 characters")]
    		public string  Todepts { get; set; }
    
    		    
    		/// <summary>
    		/// VPID
    		/// </summary>        
    	//    [DisplayName("VPID")]
            [MaxLength(50, ErrorMessage = "VPID cannot be longer than 50 characters")]
    		public string  VPID { get; set; }
    
    		    
    	}
    //}
    
}
