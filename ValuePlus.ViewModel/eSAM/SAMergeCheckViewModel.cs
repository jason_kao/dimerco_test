using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ValuePlus.ViewModel;



namespace ValuePlus.ViewModel.eSAM
{
    
    /// <summary>
    /// SAMergeCheck class
    /// </summary>
    //[MetadataType(typeof(SAMergeCheckViewModel))]
    //public  partial class SAMergeCheck
    //{
    
    	/// <summary>
    	/// SAMergeCheck Metadata class
    	/// </summary>
    	public   class SAMergeCheckViewModel:ViewModelBase
    	{
    		    
    		/// <summary>
    		/// ID
    		/// </summary>        
    	//    [DisplayName("ID")]
            [Required(ErrorMessage = "ID is required")]
    		public int  ID { get; set; }
    
    		    
    		/// <summary>
    		/// Station IP
    		/// </summary>        
    	//    [DisplayName("Station IP")]
            [MaxLength(20, ErrorMessage = "Station IP cannot be longer than 20 characters")]
    		public string  StationIP { get; set; }
    
    		    
    		/// <summary>
    		/// DBName
    		/// </summary>        
    	//    [DisplayName("DBName")]
            [MaxLength(20, ErrorMessage = "DBName cannot be longer than 20 characters")]
    		public string  DBName { get; set; }
    
    		    
    		/// <summary>
    		/// Merge Table ID
    		/// </summary>        
    	//    [DisplayName("Merge Table ID")]
    		public Nullable<int>  MergeTableID { get; set; }
    
    		    
    		/// <summary>
    		/// Is Check
    		/// </summary>        
    	//    [DisplayName("Is Check")]
            [MaxLength(1, ErrorMessage = "Is Check cannot be longer than 1 characters")]
    		public string  IsCheck { get; set; }
    
    		    
    		/// <summary>
    		/// Created Date
    		/// </summary>        
    	//    [DisplayName("Created Date")]
    		public Nullable<System.DateTime>  CreatedDate { get; set; }
    
    		    
    	}
    //}
    
}
