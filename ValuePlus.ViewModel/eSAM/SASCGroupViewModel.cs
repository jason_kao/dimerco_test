using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ValuePlus.ViewModel;



namespace ValuePlus.ViewModel.eSAM
{
    
    /// <summary>
    /// SASCGroup class
    /// </summary>
    //[MetadataType(typeof(SASCGroupViewModel))]
    //public  partial class SASCGroup
    //{
    
    	/// <summary>
    	/// SASCGroup Metadata class
    	/// </summary>
    	public   class SASCGroupViewModel:ViewModelBase
    	{
    		    
    		/// <summary>
    		/// HQID
    		/// </summary>        
    	//    [DisplayName("HQID")]
            [Required(ErrorMessage = "HQID is required")]
    		public int  HQID { get; set; }
    
    		    
    		/// <summary>
    		/// Group Lvl
    		/// </summary>        
    	//    [DisplayName("Group Lvl")]
    		public Nullable<int>  GroupLvl { get; set; }
    
    		    
    		/// <summary>
    		/// Group Type
    		/// </summary>        
    	//    [DisplayName("Group Type")]
            [MaxLength(50, ErrorMessage = "Group Type cannot be longer than 50 characters")]
    		public string  GroupType { get; set; }
    
    		    
    		/// <summary>
    		/// Group Name
    		/// </summary>        
    	//    [DisplayName("Group Name")]
            [MaxLength(50, ErrorMessage = "Group Name cannot be longer than 50 characters")]
    		public string  GroupName { get; set; }
    
    		    
    		/// <summary>
    		/// Group Code
    		/// </summary>        
    	//    [DisplayName("Group Code")]
            [MaxLength(50, ErrorMessage = "Group Code cannot be longer than 50 characters")]
    		public string  GroupCode { get; set; }
    
    		    
    		/// <summary>
    		/// Group Source ID
    		/// </summary>        
    	//    [DisplayName("Group Source ID")]
            [MaxLength(50, ErrorMessage = "Group Source ID cannot be longer than 50 characters")]
    		public string  GroupSourceID { get; set; }
    
    		    
    		/// <summary>
    		/// Group PSource ID
    		/// </summary>        
    	//    [DisplayName("Group PSource ID")]
            [MaxLength(50, ErrorMessage = "Group PSource ID cannot be longer than 50 characters")]
    		public string  GroupPSourceID { get; set; }
    
    		    
    		/// <summary>
    		/// Status
    		/// </summary>        
    	//    [DisplayName("Status")]
            [MaxLength(10, ErrorMessage = "Status cannot be longer than 10 characters")]
    		public string  Status { get; set; }
    
    		    
    		/// <summary>
    		/// Created Date
    		/// </summary>        
    	//    [DisplayName("Created Date")]
    		public Nullable<System.DateTime>  CreatedDate { get; set; }
    
    		    
    		/// <summary>
    		/// Created By
    		/// </summary>        
    	//    [DisplayName("Created By")]
            [MaxLength(10, ErrorMessage = "Created By cannot be longer than 10 characters")]
    		public string  CreatedBy { get; set; }
    
    		    
    		/// <summary>
    		/// Updated Date
    		/// </summary>        
    	//    [DisplayName("Updated Date")]
    		public Nullable<System.DateTime>  UpdatedDate { get; set; }
    
    		    
    		/// <summary>
    		/// Updated By
    		/// </summary>        
    	//    [DisplayName("Updated By")]
            [MaxLength(10, ErrorMessage = "Updated By cannot be longer than 10 characters")]
    		public string  UpdatedBy { get; set; }
    
    		    
    	}
    //}
    
}
