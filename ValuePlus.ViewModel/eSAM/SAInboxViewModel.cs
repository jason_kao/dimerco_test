using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ValuePlus.ViewModel;



namespace ValuePlus.ViewModel.eSAM
{
    
    /// <summary>
    /// SAInbox class
    /// </summary>
    //[MetadataType(typeof(SAInboxViewModel))]
    //public  partial class SAInbox
    //{
    
    	/// <summary>
    	/// SAInbox Metadata class
    	/// </summary>
    	public   class SAInboxViewModel:ViewModelBase
    	{
    		    
    		/// <summary>
    		/// ID
    		/// </summary>        
    	//    [DisplayName("ID")]
            [Required(ErrorMessage = "ID is required")]
    		public int  ID { get; set; }
    
    		    
    		/// <summary>
    		/// Source Type
    		/// </summary>        
    	//    [DisplayName("Source Type")]
            [MaxLength(10, ErrorMessage = "Source Type cannot be longer than 10 characters")]
    		public string  SourceType { get; set; }
    
    		    
    		/// <summary>
    		/// Source ID
    		/// </summary>        
    	//    [DisplayName("Source ID")]
    		public Nullable<int>  SourceID { get; set; }
    
    		    
    		/// <summary>
    		/// Comments
    		/// </summary>        
    	//    [DisplayName("Comments")]
    		public string  Comments { get; set; }
    
    		    
    		/// <summary>
    		/// Status
    		/// </summary>        
    	//    [DisplayName("Status")]
            [MaxLength(30, ErrorMessage = "Status cannot be longer than 30 characters")]
    		public string  Status { get; set; }
    
    		    
    		/// <summary>
    		/// To User ID
    		/// </summary>        
    	//    [DisplayName("To User ID")]
            [MaxLength(10, ErrorMessage = "To User ID cannot be longer than 10 characters")]
    		public string  ToUserID { get; set; }
    
    		    
    		/// <summary>
    		/// Created User ID
    		/// </summary>        
    	//    [DisplayName("Created User ID")]
            [MaxLength(10, ErrorMessage = "Created User ID cannot be longer than 10 characters")]
    		public string  CreatedUserID { get; set; }
    
    		    
    		/// <summary>
    		/// Updated User ID
    		/// </summary>        
    	//    [DisplayName("Updated User ID")]
            [MaxLength(10, ErrorMessage = "Updated User ID cannot be longer than 10 characters")]
    		public string  UpdatedUserID { get; set; }
    
    		    
    		/// <summary>
    		/// Created Date
    		/// </summary>        
    	//    [DisplayName("Created Date")]
    		public Nullable<System.DateTime>  CreatedDate { get; set; }
    
    		    
    		/// <summary>
    		/// Updated Date
    		/// </summary>        
    	//    [DisplayName("Updated Date")]
    		public Nullable<System.DateTime>  UpdatedDate { get; set; }
    
    		    
    		/// <summary>
    		/// Approval SN
    		/// </summary>        
    	//    [DisplayName("Approval SN")]
            [MaxLength(30, ErrorMessage = "Approval SN cannot be longer than 30 characters")]
    		public string  ApprovalSN { get; set; }
    
    		    
    		/// <summary>
    		/// App Lvl
    		/// </summary>        
    	//    [DisplayName("App Lvl")]
            [MaxLength(1, ErrorMessage = "App Lvl cannot be longer than 1 characters")]
    		public string  AppLvl { get; set; }
    
    		    
    		/// <summary>
    		/// Stage Lvl
    		/// </summary>        
    	//    [DisplayName("Stage Lvl")]
            [MaxLength(1, ErrorMessage = "Stage Lvl cannot be longer than 1 characters")]
    		public string  StageLvl { get; set; }
    
    		    
    	}
    //}
    
}
