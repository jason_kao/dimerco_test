using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ValuePlus.ViewModel;



namespace ValuePlus.ViewModel.eSAM
{
    
    /// <summary>
    /// SAQueryStack class
    /// </summary>
    //[MetadataType(typeof(SAQueryStackViewModel))]
    //public  partial class SAQueryStack
    //{
    
    	/// <summary>
    	/// SAQueryStack Metadata class
    	/// </summary>
    	public   class SAQueryStackViewModel:ViewModelBase
    	{
    		    
    		/// <summary>
    		/// ID
    		/// </summary>        
    	//    [DisplayName("ID")]
            [Required(ErrorMessage = "ID is required")]
    		public int  ID { get; set; }
    
    		    
    		/// <summary>
    		/// User ID
    		/// </summary>        
    	//    [DisplayName("User ID")]
            [MaxLength(10, ErrorMessage = "User ID cannot be longer than 10 characters")]
    		public string  UserID { get; set; }
    
    		    
    		/// <summary>
    		/// Total Times
    		/// </summary>        
    	//    [DisplayName("Total Times")]
    		public Nullable<int>  TotalTimes { get; set; }
    
    		    
    		/// <summary>
    		/// Query Str
    		/// </summary>        
    	//    [DisplayName("Query Str")]
            [MaxLength(255, ErrorMessage = "Query Str cannot be longer than 255 characters")]
    		public string  QueryStr { get; set; }
    
    		    
    		/// <summary>
    		/// Views
    		/// </summary>        
    	//    [DisplayName("Views")]
            [MaxLength(20, ErrorMessage = "Views cannot be longer than 20 characters")]
    		public string  Views { get; set; }
    
    		    
    		/// <summary>
    		/// Types
    		/// </summary>        
    	//    [DisplayName("Types")]
            [MaxLength(20, ErrorMessage = "Types cannot be longer than 20 characters")]
    		public string  Types { get; set; }
    
    		    
    	}
    //}
    
}
