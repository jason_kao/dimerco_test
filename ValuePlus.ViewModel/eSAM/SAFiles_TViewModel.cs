using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ValuePlus.ViewModel;



namespace ValuePlus.ViewModel.eSAM
{
    
    /// <summary>
    /// SAFiles_T class
    /// </summary>
    //[MetadataType(typeof(SAFiles_TViewModel))]
    //public  partial class SAFiles_T
    //{
    
    	/// <summary>
    	/// SAFiles_T Metadata class
    	/// </summary>
    	public   class SAFiles_TViewModel:ViewModelBase
    	{
    		    
    		/// <summary>
    		/// ID
    		/// </summary>        
    	//    [DisplayName("ID")]
            [Required(ErrorMessage = "ID is required")]
    		public int  ID { get; set; }
    
    		    
    		/// <summary>
    		/// File Name
    		/// </summary>        
    	//    [DisplayName("File Name")]
            [MaxLength(100, ErrorMessage = "File Name cannot be longer than 100 characters")]
    		public string  FileName { get; set; }
    
    		    
    		/// <summary>
    		/// File Type
    		/// </summary>        
    	//    [DisplayName("File Type")]
            [MaxLength(100, ErrorMessage = "File Type cannot be longer than 100 characters")]
    		public string  FileType { get; set; }
    
    		    
    		/// <summary>
    		/// Contents
    		/// </summary>        
    	//    [DisplayName("Contents")]
            [MaxLength(100, ErrorMessage = "Contents cannot be longer than 100 characters")]
    		public string  Contents { get; set; }
    
    		    
    		/// <summary>
    		/// Show Name
    		/// </summary>        
    	//    [DisplayName("Show Name")]
            [MaxLength(255, ErrorMessage = "Show Name cannot be longer than 255 characters")]
    		public string  ShowName { get; set; }
    
    		    
    		/// <summary>
    		/// Size
    		/// </summary>        
    	//    [DisplayName("Size")]
            [MaxLength(10, ErrorMessage = "Size cannot be longer than 10 characters")]
    		public string  Size { get; set; }
    
    		    
    		/// <summary>
    		/// Position
    		/// </summary>        
    	//    [DisplayName("Position")]
            [MaxLength(100, ErrorMessage = "Position cannot be longer than 100 characters")]
    		public string  Position { get; set; }
    
    		    
    		/// <summary>
    		/// Created Date
    		/// </summary>        
    	//    [DisplayName("Created Date")]
    		public Nullable<System.DateTime>  CreatedDate { get; set; }
    
    		    
    		/// <summary>
    		/// Created User ID
    		/// </summary>        
    	//    [DisplayName("Created User ID")]
            [MaxLength(10, ErrorMessage = "Created User ID cannot be longer than 10 characters")]
    		public string  CreatedUserID { get; set; }
    
    		    
    		/// <summary>
    		/// Status
    		/// </summary>        
    	//    [DisplayName("Status")]
            [MaxLength(10, ErrorMessage = "Status cannot be longer than 10 characters")]
    		public string  Status { get; set; }
    
    		    
    		/// <summary>
    		/// Updated User ID
    		/// </summary>        
    	//    [DisplayName("Updated User ID")]
            [MaxLength(10, ErrorMessage = "Updated User ID cannot be longer than 10 characters")]
    		public string  UpdatedUserID { get; set; }
    
    		    
    		/// <summary>
    		/// Updated Date
    		/// </summary>        
    	//    [DisplayName("Updated Date")]
    		public Nullable<System.DateTime>  UpdatedDate { get; set; }
    
    		    
    		/// <summary>
    		/// Source ID
    		/// </summary>        
    	//    [DisplayName("Source ID")]
            [MaxLength(10, ErrorMessage = "Source ID cannot be longer than 10 characters")]
    		public string  SourceID { get; set; }
    
    		    
    		/// <summary>
    		/// Source Type
    		/// </summary>        
    	//    [DisplayName("Source Type")]
            [MaxLength(10, ErrorMessage = "Source Type cannot be longer than 10 characters")]
    		public string  SourceType { get; set; }
    
    		    
    		/// <summary>
    		/// Sub File Name
    		/// </summary>        
    	//    [DisplayName("Sub File Name")]
            [MaxLength(10, ErrorMessage = "Sub File Name cannot be longer than 10 characters")]
    		public string  SubFileName { get; set; }
    
    		    
    		/// <summary>
    		/// Upload
    		/// </summary>        
    	//    [DisplayName("Upload")]
            [MaxLength(200, ErrorMessage = "Upload cannot be longer than 200 characters")]
    		public string  Upload { get; set; }
    
    		    
    	}
    //}
    
}
