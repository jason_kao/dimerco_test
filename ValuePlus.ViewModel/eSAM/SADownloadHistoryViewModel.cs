using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ValuePlus.ViewModel;



namespace ValuePlus.ViewModel.eSAM
{
    
    /// <summary>
    /// SADownloadHistory class
    /// </summary>
    //[MetadataType(typeof(SADownloadHistoryViewModel))]
    //public  partial class SADownloadHistory
    //{
    
    	/// <summary>
    	/// SADownloadHistory Metadata class
    	/// </summary>
    	public   class SADownloadHistoryViewModel:ViewModelBase
    	{
    		    
    		/// <summary>
    		/// ID
    		/// </summary>        
    	//    [DisplayName("ID")]
            [Required(ErrorMessage = "ID is required")]
    		public int  ID { get; set; }
    
    		    
    		/// <summary>
    		/// User ID
    		/// </summary>        
    	//    [DisplayName("User ID")]
            [MaxLength(10, ErrorMessage = "User ID cannot be longer than 10 characters")]
    		public string  UserID { get; set; }
    
    		    
    		/// <summary>
    		/// Mode
    		/// </summary>        
    	//    [DisplayName("Mode")]
            [MaxLength(20, ErrorMessage = "Mode cannot be longer than 20 characters")]
    		public string  Mode { get; set; }
    
    		    
    		/// <summary>
    		/// Created Date
    		/// </summary>        
    	//    [DisplayName("Created Date")]
    		public Nullable<System.DateTime>  CreatedDate { get; set; }
    
    		    
    		/// <summary>
    		/// Condition
    		/// </summary>        
    	//    [DisplayName("Condition")]
            [MaxLength(300, ErrorMessage = "Condition cannot be longer than 300 characters")]
    		public string  Condition { get; set; }
    
    		    
    		/// <summary>
    		/// File Name
    		/// </summary>        
    	//    [DisplayName("File Name")]
            [MaxLength(50, ErrorMessage = "File Name cannot be longer than 50 characters")]
    		public string  FileName { get; set; }
    
    		    
    	}
    //}
    
}
