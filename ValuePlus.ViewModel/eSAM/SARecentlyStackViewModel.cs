using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ValuePlus.ViewModel;



namespace ValuePlus.ViewModel.eSAM
{
    
    /// <summary>
    /// SARecentlyStack class
    /// </summary>
    //[MetadataType(typeof(SARecentlyStackViewModel))]
    //public  partial class SARecentlyStack
    //{
    
    	/// <summary>
    	/// SARecentlyStack Metadata class
    	/// </summary>
    	public   class SARecentlyStackViewModel:ViewModelBase
    	{
    		    
    		/// <summary>
    		/// ID
    		/// </summary>        
    	//    [DisplayName("ID")]
            [Required(ErrorMessage = "ID is required")]
    		public int  ID { get; set; }
    
    		    
    		/// <summary>
    		/// Created Userid
    		/// </summary>        
    	//    [DisplayName("Created Userid")]
            [MaxLength(10, ErrorMessage = "Created Userid cannot be longer than 10 characters")]
    		public string  CreatedUserid { get; set; }
    
    		    
    		/// <summary>
    		/// Created Date
    		/// </summary>        
    	//    [DisplayName("Created Date")]
    		public Nullable<System.DateTime>  CreatedDate { get; set; }
    
    		    
    		/// <summary>
    		/// Source Type
    		/// </summary>        
    	//    [DisplayName("Source Type")]
            [MaxLength(20, ErrorMessage = "Source Type cannot be longer than 20 characters")]
    		public string  SourceType { get; set; }
    
    		    
    		/// <summary>
    		/// Source ID
    		/// </summary>        
    	//    [DisplayName("Source ID")]
    		public Nullable<int>  SourceID { get; set; }
    
    		    
    	}
    //}
    
}
