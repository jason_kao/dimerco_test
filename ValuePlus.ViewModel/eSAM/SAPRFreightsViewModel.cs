using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ValuePlus.ViewModel;



namespace ValuePlus.ViewModel.eSAM
{
    
    /// <summary>
    /// SAPRFreights class
    /// </summary>
    //[MetadataType(typeof(SAPRFreightsViewModel))]
    //public  partial class SAPRFreights
    //{
    
    	/// <summary>
    	/// SAPRFreights Metadata class
    	/// </summary>
    	public   class SAPRFreightsViewModel:ViewModelBase
    	{
    		    
    		/// <summary>
    		/// ID
    		/// </summary>        
    	//    [DisplayName("ID")]
            [Required(ErrorMessage = "ID is required")]
    		public int  ID { get; set; }
    
    		    
    		/// <summary>
    		/// PID
    		/// </summary>        
    	//    [DisplayName("PID")]
    		public Nullable<int>  PID { get; set; }
    
    		    
    		/// <summary>
    		/// Status
    		/// </summary>        
    	//    [DisplayName("Status")]
            [MaxLength(10, ErrorMessage = "Status cannot be longer than 10 characters")]
    		public string  Status { get; set; }
    
    		    
    		/// <summary>
    		/// Type
    		/// </summary>        
    	//    [DisplayName("Type")]
            [MaxLength(10, ErrorMessage = "Type cannot be longer than 10 characters")]
    		public string  Type { get; set; }
    
    		    
    		/// <summary>
    		/// Orig City ID
    		/// </summary>        
    	//    [DisplayName("Orig City ID")]
    		public Nullable<int>  OrigCityID { get; set; }
    
    		    
    		/// <summary>
    		/// Dest City ID
    		/// </summary>        
    	//    [DisplayName("Dest City ID")]
    		public Nullable<int>  DestCityID { get; set; }
    
    		    
    		/// <summary>
    		/// PLoading Port ID
    		/// </summary>        
    	//    [DisplayName("PLoading Port ID")]
    		public Nullable<int>  PLoadingPortID { get; set; }
    
    		    
    		/// <summary>
    		/// PDischarge Port ID
    		/// </summary>        
    	//    [DisplayName("PDischarge Port ID")]
    		public Nullable<int>  PDischargePortID { get; set; }
    
    		    
    		/// <summary>
    		/// Currency ID
    		/// </summary>        
    	//    [DisplayName("Currency ID")]
    		public Nullable<int>  CurrencyID { get; set; }
    
    		    
    		/// <summary>
    		/// Carrier ID
    		/// </summary>        
    	//    [DisplayName("Carrier ID")]
    		public Nullable<int>  CarrierID { get; set; }
    
    		    
    		/// <summary>
    		/// Frequency
    		/// </summary>        
    	//    [DisplayName("Frequency")]
            [MaxLength(20, ErrorMessage = "Frequency cannot be longer than 20 characters")]
    		public string  Frequency { get; set; }
    
    		    
    		/// <summary>
    		/// Transit Time
    		/// </summary>        
    	//    [DisplayName("Transit Time")]
            [MaxLength(20, ErrorMessage = "Transit Time cannot be longer than 20 characters")]
    		public string  TransitTime { get; set; }
    
    		    
    		/// <summary>
    		/// Effective Date
    		/// </summary>        
    	//    [DisplayName("Effective Date")]
    		public Nullable<System.DateTime>  EffectiveDate { get; set; }
    
    		    
    		/// <summary>
    		/// Promotion Rate
    		/// </summary>        
    	//    [DisplayName("Promotion Rate")]
    		public string  PromotionRate { get; set; }
    
    		    
    		/// <summary>
    		/// Remark
    		/// </summary>        
    	//    [DisplayName("Remark")]
    		public string  Remark { get; set; }
    
    		    
    		/// <summary>
    		/// Created User ID
    		/// </summary>        
    	//    [DisplayName("Created User ID")]
            [MaxLength(10, ErrorMessage = "Created User ID cannot be longer than 10 characters")]
    		public string  CreatedUserID { get; set; }
    
    		    
    		/// <summary>
    		/// Created Date
    		/// </summary>        
    	//    [DisplayName("Created Date")]
    		public Nullable<System.DateTime>  CreatedDate { get; set; }
    
    		    
    		/// <summary>
    		/// Updated User ID
    		/// </summary>        
    	//    [DisplayName("Updated User ID")]
            [MaxLength(10, ErrorMessage = "Updated User ID cannot be longer than 10 characters")]
    		public string  UpdatedUserID { get; set; }
    
    		    
    		/// <summary>
    		/// Updated Date
    		/// </summary>        
    	//    [DisplayName("Updated Date")]
    		public Nullable<System.DateTime>  UpdatedDate { get; set; }
    
    		    
    	}
    //}
    
}
