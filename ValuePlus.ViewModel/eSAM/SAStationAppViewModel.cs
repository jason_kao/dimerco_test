using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ValuePlus.ViewModel;



namespace ValuePlus.ViewModel.eSAM
{
    
    /// <summary>
    /// SAStationApp class
    /// </summary>
    //[MetadataType(typeof(SAStationAppViewModel))]
    //public  partial class SAStationApp
    //{
    
    	/// <summary>
    	/// SAStationApp Metadata class
    	/// </summary>
    	public   class SAStationAppViewModel:ViewModelBase
    	{
    		    
    		/// <summary>
    		/// ID
    		/// </summary>        
    	//    [DisplayName("ID")]
            [Required(ErrorMessage = "ID is required")]
    		public int  ID { get; set; }
    
    		    
    		/// <summary>
    		/// Station ID
    		/// </summary>        
    	//    [DisplayName("Station ID")]
            [MaxLength(10, ErrorMessage = "Station ID cannot be longer than 10 characters")]
    		public string  StationID { get; set; }
    
    		    
    		/// <summary>
    		/// PID
    		/// </summary>        
    	//    [DisplayName("PID")]
            [MaxLength(2, ErrorMessage = "PID cannot be longer than 2 characters")]
    		public string  PID { get; set; }
    
    		    
    		/// <summary>
    		/// Mode
    		/// </summary>        
    	//    [DisplayName("Mode")]
            [MaxLength(2, ErrorMessage = "Mode cannot be longer than 2 characters")]
    		public string  Mode { get; set; }
    
    		    
    		/// <summary>
    		/// Lvl
    		/// </summary>        
    	//    [DisplayName("Lvl")]
            [MaxLength(2, ErrorMessage = "Lvl cannot be longer than 2 characters")]
    		public string  Lvl { get; set; }
    
    		    
    		/// <summary>
    		/// A1st
    		/// </summary>        
    	//    [DisplayName("A1st")]
            [MaxLength(10, ErrorMessage = "A1st cannot be longer than 10 characters")]
    		public string  A1st { get; set; }
    
    		    
    		/// <summary>
    		/// A1st D
    		/// </summary>        
    	//    [DisplayName("A1st D")]
            [MaxLength(10, ErrorMessage = "A1st D cannot be longer than 10 characters")]
    		public string  A1stD { get; set; }
    
    		    
    		/// <summary>
    		/// A2nd
    		/// </summary>        
    	//    [DisplayName("A2nd")]
            [MaxLength(10, ErrorMessage = "A2nd cannot be longer than 10 characters")]
    		public string  A2nd { get; set; }
    
    		    
    		/// <summary>
    		/// A2nd D
    		/// </summary>        
    	//    [DisplayName("A2nd D")]
            [MaxLength(10, ErrorMessage = "A2nd D cannot be longer than 10 characters")]
    		public string  A2ndD { get; set; }
    
    		    
    		/// <summary>
    		/// Created DT
    		/// </summary>        
    	//    [DisplayName("Created DT")]
    		public Nullable<System.DateTime>  CreatedDT { get; set; }
    
    		    
    		/// <summary>
    		/// Created By
    		/// </summary>        
    	//    [DisplayName("Created By")]
            [MaxLength(10, ErrorMessage = "Created By cannot be longer than 10 characters")]
    		public string  CreatedBy { get; set; }
    
    		    
    		/// <summary>
    		/// Updated DT
    		/// </summary>        
    	//    [DisplayName("Updated DT")]
    		public Nullable<System.DateTime>  UpdatedDT { get; set; }
    
    		    
    		/// <summary>
    		/// Updated By
    		/// </summary>        
    	//    [DisplayName("Updated By")]
            [MaxLength(10, ErrorMessage = "Updated By cannot be longer than 10 characters")]
    		public string  UpdatedBy { get; set; }
    
    		    
    		/// <summary>
    		/// Version
    		/// </summary>        
    	//    [DisplayName("Version")]
            [MaxLength(8, ErrorMessage = "Version cannot be longer than 8 characters")]
    		public byte[]  Version { get; set; }
    
    		    
    	}
    //}
    
}
