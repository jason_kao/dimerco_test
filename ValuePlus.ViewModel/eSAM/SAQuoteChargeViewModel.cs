using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ValuePlus.ViewModel;



namespace ValuePlus.ViewModel.eSAM
{
    
    /// <summary>
    /// SAQuoteCharge class
    /// </summary>
    //[MetadataType(typeof(SAQuoteChargeViewModel))]
    //public  partial class SAQuoteCharge
    //{
    
    	/// <summary>
    	/// SAQuoteCharge Metadata class
    	/// </summary>
    	public   class SAQuoteChargeViewModel:ViewModelBase
    	{
    		    
    		/// <summary>
    		/// ID
    		/// </summary>        
    	//    [DisplayName("ID")]
            [Required(ErrorMessage = "ID is required")]
    		public int  ID { get; set; }
    
    		    
    		/// <summary>
    		/// Quote City ID
    		/// </summary>        
    	//    [DisplayName("Quote City ID")]
    		public Nullable<int>  QuoteCityID { get; set; }
    
    		    
    		/// <summary>
    		/// Order No
    		/// </summary>        
    	//    [DisplayName("Order No")]
            [MaxLength(10, ErrorMessage = "Order No cannot be longer than 10 characters")]
    		public string  OrderNo { get; set; }
    
    		    
    		/// <summary>
    		/// Charge Code Desc
    		/// </summary>        
    	//    [DisplayName("Charge Code Desc")]
            [MaxLength(50, ErrorMessage = "Charge Code Desc cannot be longer than 50 characters")]
    		public string  ChargeCodeDesc { get; set; }
    
    		    
    		/// <summary>
    		/// Charge Code ID
    		/// </summary>        
    	//    [DisplayName("Charge Code ID")]
    		public Nullable<int>  ChargeCodeID { get; set; }
    
    		    
    		/// <summary>
    		/// Remark
    		/// </summary>        
    	//    [DisplayName("Remark")]
            [MaxLength(255, ErrorMessage = "Remark cannot be longer than 255 characters")]
    		public string  Remark { get; set; }
    
    		    
    		/// <summary>
    		/// Created User ID
    		/// </summary>        
    	//    [DisplayName("Created User ID")]
            [MaxLength(20, ErrorMessage = "Created User ID cannot be longer than 20 characters")]
    		public string  CreatedUserID { get; set; }
    
    		    
    		/// <summary>
    		/// Created Date
    		/// </summary>        
    	//    [DisplayName("Created Date")]
    		public Nullable<System.DateTime>  CreatedDate { get; set; }
    
    		    
    		/// <summary>
    		/// Updated User ID
    		/// </summary>        
    	//    [DisplayName("Updated User ID")]
            [MaxLength(20, ErrorMessage = "Updated User ID cannot be longer than 20 characters")]
    		public string  UpdatedUserID { get; set; }
    
    		    
    		/// <summary>
    		/// Updated Date
    		/// </summary>        
    	//    [DisplayName("Updated Date")]
    		public Nullable<System.DateTime>  UpdatedDate { get; set; }
    
    		    
    		/// <summary>
    		/// Min Cost
    		/// </summary>        
    	//    [DisplayName("Min Cost")]
            [MaxLength(20, ErrorMessage = "Min Cost cannot be longer than 20 characters")]
    		public string  MinCost { get; set; }
    
    		    
    		/// <summary>
    		/// Min Profit
    		/// </summary>        
    	//    [DisplayName("Min Profit")]
            [MaxLength(20, ErrorMessage = "Min Profit cannot be longer than 20 characters")]
    		public string  MinProfit { get; set; }
    
    		    
    		/// <summary>
    		/// Min Selling
    		/// </summary>        
    	//    [DisplayName("Min Selling")]
            [MaxLength(20, ErrorMessage = "Min Selling cannot be longer than 20 characters")]
    		public string  MinSelling { get; set; }
    
    		    
    		/// <summary>
    		/// Flat Cost
    		/// </summary>        
    	//    [DisplayName("Flat Cost")]
            [MaxLength(20, ErrorMessage = "Flat Cost cannot be longer than 20 characters")]
    		public string  FlatCost { get; set; }
    
    		    
    		/// <summary>
    		/// Flat Profit
    		/// </summary>        
    	//    [DisplayName("Flat Profit")]
            [MaxLength(20, ErrorMessage = "Flat Profit cannot be longer than 20 characters")]
    		public string  FlatProfit { get; set; }
    
    		    
    		/// <summary>
    		/// Flat Selling
    		/// </summary>        
    	//    [DisplayName("Flat Selling")]
            [MaxLength(20, ErrorMessage = "Flat Selling cannot be longer than 20 characters")]
    		public string  FlatSelling { get; set; }
    
    		    
    		/// <summary>
    		/// Min UOM
    		/// </summary>        
    	//    [DisplayName("Min UOM")]
    		public Nullable<int>  MinUOM { get; set; }
    
    		    
    		/// <summary>
    		/// Flat UOM
    		/// </summary>        
    	//    [DisplayName("Flat UOM")]
    		public Nullable<int>  FlatUOM { get; set; }
    
    		    
    		/// <summary>
    		/// FLType
    		/// </summary>        
    	//    [DisplayName("FLType")]
            [MaxLength(10, ErrorMessage = "FLType cannot be longer than 10 characters")]
    		public string  FLType { get; set; }
    
    		    
    		/// <summary>
    		/// PPCC
    		/// </summary>        
    	//    [DisplayName("PPCC")]
            [MaxLength(10, ErrorMessage = "PPCC cannot be longer than 10 characters")]
    		public string  PPCC { get; set; }
    
    		    
    		/// <summary>
    		/// Remark2
    		/// </summary>        
    	//    [DisplayName("Remark2")]
            [MaxLength(1000, ErrorMessage = "Remark2 cannot be longer than 1000 characters")]
    		public string  Remark2 { get; set; }
    
    		    
    		/// <summary>
    		/// Method
    		/// </summary>        
    	//    [DisplayName("Method")]
            [MaxLength(50, ErrorMessage = "Method cannot be longer than 50 characters")]
    		public string  Method { get; set; }
    
    		    
    		/// <summary>
    		/// Currency ID
    		/// </summary>        
    	//    [DisplayName("Currency ID")]
    		public Nullable<int>  CurrencyID { get; set; }
    
    		    
    		/// <summary>
    		/// Group ID
    		/// </summary>        
    	//    [DisplayName("Group ID")]
    		public Nullable<int>  GroupID { get; set; }
    
    		    
    		/// <summary>
    		/// Sales Remark
    		/// </summary>        
    	//    [DisplayName("Sales Remark")]
            [MaxLength(1000, ErrorMessage = "Sales Remark cannot be longer than 1000 characters")]
    		public string  SalesRemark { get; set; }
    
    		    
    	}
    //}
    
}
