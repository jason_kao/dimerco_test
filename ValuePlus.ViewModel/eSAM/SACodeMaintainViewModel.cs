using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ValuePlus.ViewModel;



namespace ValuePlus.ViewModel.eSAM
{
    
    /// <summary>
    /// SACodeMaintain class
    /// </summary>
    //[MetadataType(typeof(SACodeMaintainViewModel))]
    //public  partial class SACodeMaintain
    //{
    
    	/// <summary>
    	/// SACodeMaintain Metadata class
    	/// </summary>
    	public   class SACodeMaintainViewModel:ViewModelBase
    	{
    		    
    		/// <summary>
    		/// ID
    		/// </summary>        
    	//    [DisplayName("ID")]
            [Required(ErrorMessage = "ID is required")]
    		public int  ID { get; set; }
    
    		    
    		/// <summary>
    		/// Title
    		/// </summary>        
    	//    [DisplayName("Title")]
            [MaxLength(50, ErrorMessage = "Title cannot be longer than 50 characters")]
    		public string  Title { get; set; }
    
    		    
    		/// <summary>
    		/// Class
    		/// </summary>        
    	//    [DisplayName("Class")]
            [MaxLength(50, ErrorMessage = "Class cannot be longer than 50 characters")]
    		public string  Class { get; set; }
    
    		    
    		/// <summary>
    		/// Contents
    		/// </summary>        
    	//    [DisplayName("Contents")]
            [MaxLength(50, ErrorMessage = "Contents cannot be longer than 50 characters")]
    		public string  Contents { get; set; }
    
    		    
    		/// <summary>
    		/// Remark1
    		/// </summary>        
    	//    [DisplayName("Remark1")]
            [MaxLength(60, ErrorMessage = "Remark1 cannot be longer than 60 characters")]
    		public string  Remark1 { get; set; }
    
    		    
    		/// <summary>
    		/// Remark2
    		/// </summary>        
    	//    [DisplayName("Remark2")]
            [MaxLength(60, ErrorMessage = "Remark2 cannot be longer than 60 characters")]
    		public string  Remark2 { get; set; }
    
    		    
    		/// <summary>
    		/// Item
    		/// </summary>        
    	//    [DisplayName("Item")]
            [MaxLength(50, ErrorMessage = "Item cannot be longer than 50 characters")]
    		public string  Item { get; set; }
    
    		    
    		/// <summary>
    		/// Order No
    		/// </summary>        
    	//    [DisplayName("Order No")]
            [MaxLength(10, ErrorMessage = "Order No cannot be longer than 10 characters")]
    		public string  OrderNo { get; set; }
    
    		    
    		/// <summary>
    		/// Created User ID
    		/// </summary>        
    	//    [DisplayName("Created User ID")]
            [MaxLength(10, ErrorMessage = "Created User ID cannot be longer than 10 characters")]
    		public string  CreatedUserID { get; set; }
    
    		    
    		/// <summary>
    		/// Created Date
    		/// </summary>        
    	//    [DisplayName("Created Date")]
    		public Nullable<System.DateTime>  CreatedDate { get; set; }
    
    		    
    		/// <summary>
    		/// Update User ID
    		/// </summary>        
    	//    [DisplayName("Update User ID")]
            [MaxLength(10, ErrorMessage = "Update User ID cannot be longer than 10 characters")]
    		public string  UpdateUserID { get; set; }
    
    		    
    		/// <summary>
    		/// Update Date
    		/// </summary>        
    	//    [DisplayName("Update Date")]
    		public Nullable<System.DateTime>  UpdateDate { get; set; }
    
    		    
    	}
    //}
    
}
