using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ValuePlus.ViewModel;



namespace ValuePlus.ViewModel.eSAM
{
    
    /// <summary>
    /// SAQFMaster class
    /// </summary>
    //[MetadataType(typeof(SAQFMasterViewModel))]
    //public  partial class SAQFMaster
    //{
    
    	/// <summary>
    	/// SAQFMaster Metadata class
    	/// </summary>
    	public   class SAQFMasterViewModel:ViewModelBase
    	{
    		    
    		/// <summary>
    		/// ID
    		/// </summary>        
    	//    [DisplayName("ID")]
            [Required(ErrorMessage = "ID is required")]
    		public int  ID { get; set; }
    
    		    
    		/// <summary>
    		/// Carrier ID
    		/// </summary>        
    	//    [DisplayName("Carrier ID")]
    		public Nullable<int>  CarrierID { get; set; }
    
    		    
    		/// <summary>
    		/// Starting Of Period
    		/// </summary>        
    	//    [DisplayName("Starting Of Period")]
    		public Nullable<System.DateTime>  StartingOfPeriod { get; set; }
    
    		    
    		/// <summary>
    		/// End Of Period
    		/// </summary>        
    	//    [DisplayName("End Of Period")]
    		public Nullable<System.DateTime>  EndOfPeriod { get; set; }
    
    		    
    		/// <summary>
    		/// MQC
    		/// </summary>        
    	//    [DisplayName("MQC")]
    		public Nullable<int>  MQC { get; set; }
    
    		    
    		/// <summary>
    		/// Unit
    		/// </summary>        
    	//    [DisplayName("Unit")]
            [MaxLength(10, ErrorMessage = "Unit cannot be longer than 10 characters")]
    		public string  Unit { get; set; }
    
    		    
    		/// <summary>
    		/// Created By
    		/// </summary>        
    	//    [DisplayName("Created By")]
            [Required(ErrorMessage = "Created By is required")]
            [MaxLength(6, ErrorMessage = "Created By cannot be longer than 6 characters")]
    		public string  CreatedBy { get; set; }
    
    		    
    		/// <summary>
    		/// Created Date
    		/// </summary>        
    	//    [DisplayName("Created Date")]
    		public Nullable<System.DateTime>  CreatedDate { get; set; }
    
    		    
    		/// <summary>
    		/// Updated By
    		/// </summary>        
    	//    [DisplayName("Updated By")]
            [MaxLength(6, ErrorMessage = "Updated By cannot be longer than 6 characters")]
    		public string  UpdatedBy { get; set; }
    
    		    
    		/// <summary>
    		/// Updated Date
    		/// </summary>        
    	//    [DisplayName("Updated Date")]
    		public Nullable<System.DateTime>  UpdatedDate { get; set; }
    
    		    
    	}
    //}
    
}
