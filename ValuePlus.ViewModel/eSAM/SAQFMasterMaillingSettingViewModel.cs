using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ValuePlus.ViewModel;



namespace ValuePlus.ViewModel.eSAM
{
    
    /// <summary>
    /// SAQFMasterMaillingSetting class
    /// </summary>
    //[MetadataType(typeof(SAQFMasterMaillingSettingViewModel))]
    //public  partial class SAQFMasterMaillingSetting
    //{
    
    	/// <summary>
    	/// SAQFMasterMaillingSetting Metadata class
    	/// </summary>
    	public   class SAQFMasterMaillingSettingViewModel:ViewModelBase
    	{
    		    
    		/// <summary>
    		/// ID
    		/// </summary>        
    	//    [DisplayName("ID")]
            [Required(ErrorMessage = "ID is required")]
    		public int  ID { get; set; }
    
    		    
    		/// <summary>
    		/// Master ID
    		/// </summary>        
    	//    [DisplayName("Master ID")]
    		public Nullable<int>  MasterID { get; set; }
    
    		    
    		/// <summary>
    		/// SVID
    		/// </summary>        
    	//    [DisplayName("SVID")]
    		public Nullable<int>  SVID { get; set; }
    
    		    
    		/// <summary>
    		/// Type
    		/// </summary>        
    	//    [DisplayName("Type")]
            [MaxLength(1, ErrorMessage = "Type cannot be longer than 1 characters")]
    		public string  Type { get; set; }
    
    		    
    		/// <summary>
    		/// User ID
    		/// </summary>        
    	//    [DisplayName("User ID")]
            [MaxLength(6, ErrorMessage = "User ID cannot be longer than 6 characters")]
    		public string  UserID { get; set; }
    
    		    
    		/// <summary>
    		/// Created By
    		/// </summary>        
    	//    [DisplayName("Created By")]
            [Required(ErrorMessage = "Created By is required")]
            [MaxLength(6, ErrorMessage = "Created By cannot be longer than 6 characters")]
    		public string  CreatedBy { get; set; }
    
    		    
    		/// <summary>
    		/// Created Date
    		/// </summary>        
    	//    [DisplayName("Created Date")]
            [Required(ErrorMessage = "Created Date is required")]
    		public System.DateTime  CreatedDate { get; set; }
    
    		    
    		/// <summary>
    		/// Updated By
    		/// </summary>        
    	//    [DisplayName("Updated By")]
            [MaxLength(6, ErrorMessage = "Updated By cannot be longer than 6 characters")]
    		public string  UpdatedBy { get; set; }
    
    		    
    		/// <summary>
    		/// Updated Date
    		/// </summary>        
    	//    [DisplayName("Updated Date")]
    		public Nullable<System.DateTime>  UpdatedDate { get; set; }
    
    		    
    	}
    //}
    
}
