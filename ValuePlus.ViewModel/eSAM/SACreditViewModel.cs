using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ValuePlus.ViewModel;



namespace ValuePlus.ViewModel.eSAM
{
    
    /// <summary>
    /// SACredit class
    /// </summary>
    //[MetadataType(typeof(SACreditViewModel))]
    //public  partial class SACredit
    //{
    
    	/// <summary>
    	/// SACredit Metadata class
    	/// </summary>
    	public   class SACreditViewModel:ViewModelBase
    	{
    		    
    		/// <summary>
    		/// ID
    		/// </summary>        
    	//    [DisplayName("ID")]
            [Required(ErrorMessage = "ID is required")]
    		public int  ID { get; set; }
    
    		    
    		/// <summary>
    		/// LID
    		/// </summary>        
    	//    [DisplayName("LID")]
            [Required(ErrorMessage = "LID is required")]
    		public int  LID { get; set; }
    
    		    
    		/// <summary>
    		/// PID
    		/// </summary>        
    	//    [DisplayName("PID")]
            [Required(ErrorMessage = "PID is required")]
    		public int  PID { get; set; }
    
    		    
    		/// <summary>
    		/// Credit Ref
    		/// </summary>        
    	//    [DisplayName("Credit Ref")]
            [MaxLength(20, ErrorMessage = "Credit Ref cannot be longer than 20 characters")]
    		public string  CreditRef { get; set; }
    
    		    
    		/// <summary>
    		/// Received Date
    		/// </summary>        
    	//    [DisplayName("Received Date")]
            [MaxLength(10, ErrorMessage = "Received Date cannot be longer than 10 characters")]
    		public string  ReceivedDate { get; set; }
    
    		    
    		/// <summary>
    		/// Busi Liense
    		/// </summary>        
    	//    [DisplayName("Busi Liense")]
            [MaxLength(50, ErrorMessage = "Busi Liense cannot be longer than 50 characters")]
    		public string  BusiLiense { get; set; }
    
    		    
    		/// <summary>
    		/// Major Commodity
    		/// </summary>        
    	//    [DisplayName("Major Commodity")]
            [MaxLength(50, ErrorMessage = "Major Commodity cannot be longer than 50 characters")]
    		public string  MajorCommodity { get; set; }
    
    		    
    		/// <summary>
    		/// Day Num
    		/// </summary>        
    	//    [DisplayName("Day Num")]
    		public Nullable<int>  DayNum { get; set; }
    
    		    
    		/// <summary>
    		/// Major Bank Info
    		/// </summary>        
    	//    [DisplayName("Major Bank Info")]
            [MaxLength(50, ErrorMessage = "Major Bank Info cannot be longer than 50 characters")]
    		public string  MajorBankInfo { get; set; }
    
    		    
    		/// <summary>
    		/// Capital Amount
    		/// </summary>        
    	//    [DisplayName("Capital Amount")]
            [MaxLength(50, ErrorMessage = "Capital Amount cannot be longer than 50 characters")]
    		public string  CapitalAmount { get; set; }
    
    		    
    		/// <summary>
    		/// Established Date
    		/// </summary>        
    	//    [DisplayName("Established Date")]
            [MaxLength(50, ErrorMessage = "Established Date cannot be longer than 50 characters")]
    		public string  EstablishedDate { get; set; }
    
    		    
    		/// <summary>
    		/// Noof Employee
    		/// </summary>        
    	//    [DisplayName("Noof Employee")]
            [MaxLength(50, ErrorMessage = "Noof Employee cannot be longer than 50 characters")]
    		public string  NoofEmployee { get; set; }
    
    		    
    		/// <summary>
    		/// Annual Revenue
    		/// </summary>        
    	//    [DisplayName("Annual Revenue")]
            [MaxLength(50, ErrorMessage = "Annual Revenue cannot be longer than 50 characters")]
    		public string  AnnualRevenue { get; set; }
    
    		    
    		/// <summary>
    		/// Credit Term ID
    		/// </summary>        
    	//    [DisplayName("Credit Term ID")]
    		public Nullable<int>  CreditTermID { get; set; }
    
    		    
    		/// <summary>
    		/// Currency ID
    		/// </summary>        
    	//    [DisplayName("Currency ID")]
    		public Nullable<int>  CurrencyID { get; set; }
    
    		    
    		/// <summary>
    		/// Status
    		/// </summary>        
    	//    [DisplayName("Status")]
            [MaxLength(10, ErrorMessage = "Status cannot be longer than 10 characters")]
    		public string  Status { get; set; }
    
    		    
    		/// <summary>
    		/// Amount
    		/// </summary>        
    	//    [DisplayName("Amount")]
            [MaxLength(20, ErrorMessage = "Amount cannot be longer than 20 characters")]
    		public string  Amount { get; set; }
    
    		    
    		/// <summary>
    		/// Created User ID
    		/// </summary>        
    	//    [DisplayName("Created User ID")]
            [MaxLength(10, ErrorMessage = "Created User ID cannot be longer than 10 characters")]
    		public string  CreatedUserID { get; set; }
    
    		    
    		/// <summary>
    		/// Updated User ID
    		/// </summary>        
    	//    [DisplayName("Updated User ID")]
            [MaxLength(10, ErrorMessage = "Updated User ID cannot be longer than 10 characters")]
    		public string  UpdatedUserID { get; set; }
    
    		    
    		/// <summary>
    		/// Updated Date
    		/// </summary>        
    	//    [DisplayName("Updated Date")]
    		public Nullable<System.DateTime>  UpdatedDate { get; set; }
    
    		    
    		/// <summary>
    		/// Credit Term Type
    		/// </summary>        
    	//    [DisplayName("Credit Term Type")]
            [MaxLength(10, ErrorMessage = "Credit Term Type cannot be longer than 10 characters")]
    		public string  CreditTermType { get; set; }
    
    		    
    		/// <summary>
    		/// PLRemark
    		/// </summary>        
    	//    [DisplayName("PLRemark")]
            [MaxLength(255, ErrorMessage = "PLRemark cannot be longer than 255 characters")]
    		public string  PLRemark { get; set; }
    
    		    
    		/// <summary>
    		/// Due Date
    		/// </summary>        
    	//    [DisplayName("Due Date")]
            [MaxLength(50, ErrorMessage = "Due Date cannot be longer than 50 characters")]
    		public string  DueDate { get; set; }
    
    		    
    		/// <summary>
    		/// Created Date
    		/// </summary>        
    	//    [DisplayName("Created Date")]
    		public Nullable<System.DateTime>  CreatedDate { get; set; }
    
    		    
    		/// <summary>
    		/// Capital Currency ID
    		/// </summary>        
    	//    [DisplayName("Capital Currency ID")]
    		public Nullable<int>  CapitalCurrencyID { get; set; }
    
    		    
    		/// <summary>
    		/// Export
    		/// </summary>        
    	//    [DisplayName("Export")]
    		public Nullable<bool>  Export { get; set; }
    
    		    
    		/// <summary>
    		/// Import
    		/// </summary>        
    	//    [DisplayName("Import")]
    		public Nullable<bool>  Import { get; set; }
    
    		    
    		/// <summary>
    		/// DC
    		/// </summary>        
    	//    [DisplayName("DC")]
    		public Nullable<bool>  DC { get; set; }
    
    		    
    		/// <summary>
    		/// SL
    		/// </summary>        
    	//    [DisplayName("SL")]
    		public Nullable<bool>  SL { get; set; }
    
    		    
    		/// <summary>
    		/// VM
    		/// </summary>        
    	//    [DisplayName("VM")]
    		public Nullable<bool>  VM { get; set; }
    
    		    
    		/// <summary>
    		/// TR
    		/// </summary>        
    	//    [DisplayName("TR")]
    		public Nullable<bool>  TR { get; set; }
    
    		    
    		/// <summary>
    		/// Payable Day
    		/// </summary>        
    	//    [DisplayName("Payable Day")]
            [Required(ErrorMessage = "Payable Day is required")]
    		public int  PayableDay { get; set; }
    
    		    
    		/// <summary>
    		/// Payment Date1
    		/// </summary>        
    	//    [DisplayName("Payment Date1")]
    		public Nullable<int>  PaymentDate1 { get; set; }
    
    		    
    		/// <summary>
    		/// Payment Date2
    		/// </summary>        
    	//    [DisplayName("Payment Date2")]
    		public Nullable<int>  PaymentDate2 { get; set; }
    
    		    
    		/// <summary>
    		/// Station ID
    		/// </summary>        
    	//    [DisplayName("Station ID")]
            [MaxLength(3, ErrorMessage = "Station ID cannot be longer than 3 characters")]
    		public string  StationID { get; set; }
    
    		    
    		/// <summary>
    		/// Approved Date
    		/// </summary>        
    	//    [DisplayName("Approved Date")]
    		public Nullable<System.DateTime>  ApprovedDate { get; set; }
    
    		    
    		/// <summary>
    		/// Expired Date
    		/// </summary>        
    	//    [DisplayName("Expired Date")]
    		public Nullable<System.DateTime>  ExpiredDate { get; set; }
    
    		    
    		/// <summary>
    		/// Approval Lvl
    		/// </summary>        
    	//    [DisplayName("Approval Lvl")]
            [MaxLength(1, ErrorMessage = "Approval Lvl cannot be longer than 1 characters")]
    		public string  ApprovalLvl { get; set; }
    
    		    
    		/// <summary>
    		/// is Added Req Info
    		/// </summary>        
    	//    [DisplayName("is Added Req Info")]
            [MaxLength(1, ErrorMessage = "is Added Req Info cannot be longer than 1 characters")]
    		public string  isAddedReqInfo { get; set; }
    
    		    
    		/// <summary>
    		/// Closing Date
    		/// </summary>        
    	//    [DisplayName("Closing Date")]
    		public Nullable<int>  ClosingDate { get; set; }
    
    		    
    	}
    //}
    
}
