using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ValuePlus.ViewModel;



namespace ValuePlus.ViewModel.eSAM
{
    
    /// <summary>
    /// SAQuoteToolLocalChargeCity class
    /// </summary>
    //[MetadataType(typeof(SAQuoteToolLocalChargeCityViewModel))]
    //public  partial class SAQuoteToolLocalChargeCity
    //{
    
    	/// <summary>
    	/// SAQuoteToolLocalChargeCity Metadata class
    	/// </summary>
    	public   class SAQuoteToolLocalChargeCityViewModel:ViewModelBase
    	{
    		    
    		/// <summary>
    		/// ID
    		/// </summary>        
    	//    [DisplayName("ID")]
            [Required(ErrorMessage = "ID is required")]
    		public int  ID { get; set; }
    
    		    
    		/// <summary>
    		/// Quote ID
    		/// </summary>        
    	//    [DisplayName("Quote ID")]
            [Required(ErrorMessage = "Quote ID is required")]
    		public int  QuoteID { get; set; }
    
    		    
    		/// <summary>
    		/// City Codes
    		/// </summary>        
    	//    [DisplayName("City Codes")]
            [Required(ErrorMessage = "City Codes is required")]
            [MaxLength(255, ErrorMessage = "City Codes cannot be longer than 255 characters")]
    		public string  CityCodes { get; set; }
    
    		    
    		/// <summary>
    		/// Curency Code
    		/// </summary>        
    	//    [DisplayName("Curency Code")]
            [Required(ErrorMessage = "Curency Code is required")]
            [MaxLength(3, ErrorMessage = "Curency Code cannot be longer than 3 characters")]
    		public string  CurencyCode { get; set; }
    
    		    
    		/// <summary>
    		/// UOMCode
    		/// </summary>        
    	//    [DisplayName("UOMCode")]
            [Required(ErrorMessage = "UOMCode is required")]
            [MaxLength(3, ErrorMessage = "UOMCode cannot be longer than 3 characters")]
    		public string  UOMCode { get; set; }
    
    		    
    		/// <summary>
    		/// Mode
    		/// </summary>        
    	//    [DisplayName("Mode")]
            [Required(ErrorMessage = "Mode is required")]
            [MaxLength(10, ErrorMessage = "Mode cannot be longer than 10 characters")]
    		public string  Mode { get; set; }
    
    		    
    		/// <summary>
    		/// Weight Break1
    		/// </summary>        
    	//    [DisplayName("Weight Break1")]
            [MaxLength(20, ErrorMessage = "Weight Break1 cannot be longer than 20 characters")]
    		public string  WeightBreak1 { get; set; }
    
    		    
    		/// <summary>
    		/// Weight Break2
    		/// </summary>        
    	//    [DisplayName("Weight Break2")]
            [MaxLength(20, ErrorMessage = "Weight Break2 cannot be longer than 20 characters")]
    		public string  WeightBreak2 { get; set; }
    
    		    
    		/// <summary>
    		/// Weight Break3
    		/// </summary>        
    	//    [DisplayName("Weight Break3")]
            [MaxLength(20, ErrorMessage = "Weight Break3 cannot be longer than 20 characters")]
    		public string  WeightBreak3 { get; set; }
    
    		    
    		/// <summary>
    		/// Weight Break4
    		/// </summary>        
    	//    [DisplayName("Weight Break4")]
            [MaxLength(20, ErrorMessage = "Weight Break4 cannot be longer than 20 characters")]
    		public string  WeightBreak4 { get; set; }
    
    		    
    		/// <summary>
    		/// Weight Break5
    		/// </summary>        
    	//    [DisplayName("Weight Break5")]
            [MaxLength(20, ErrorMessage = "Weight Break5 cannot be longer than 20 characters")]
    		public string  WeightBreak5 { get; set; }
    
    		    
    		/// <summary>
    		/// Weight Break6
    		/// </summary>        
    	//    [DisplayName("Weight Break6")]
            [MaxLength(20, ErrorMessage = "Weight Break6 cannot be longer than 20 characters")]
    		public string  WeightBreak6 { get; set; }
    
    		    
    		/// <summary>
    		/// Weight Break7
    		/// </summary>        
    	//    [DisplayName("Weight Break7")]
            [MaxLength(20, ErrorMessage = "Weight Break7 cannot be longer than 20 characters")]
    		public string  WeightBreak7 { get; set; }
    
    		    
    		/// <summary>
    		/// Weight Break8
    		/// </summary>        
    	//    [DisplayName("Weight Break8")]
            [MaxLength(20, ErrorMessage = "Weight Break8 cannot be longer than 20 characters")]
    		public string  WeightBreak8 { get; set; }
    
    		    
    		/// <summary>
    		/// Weight Break9
    		/// </summary>        
    	//    [DisplayName("Weight Break9")]
            [MaxLength(20, ErrorMessage = "Weight Break9 cannot be longer than 20 characters")]
    		public string  WeightBreak9 { get; set; }
    
    		    
    		/// <summary>
    		/// Weight Break10
    		/// </summary>        
    	//    [DisplayName("Weight Break10")]
            [MaxLength(20, ErrorMessage = "Weight Break10 cannot be longer than 20 characters")]
    		public string  WeightBreak10 { get; set; }
    
    		    
    		/// <summary>
    		/// UOM1
    		/// </summary>        
    	//    [DisplayName("UOM1")]
    		public Nullable<int>  UOM1 { get; set; }
    
    		    
    		/// <summary>
    		/// UOM2
    		/// </summary>        
    	//    [DisplayName("UOM2")]
    		public Nullable<int>  UOM2 { get; set; }
    
    		    
    		/// <summary>
    		/// UOM3
    		/// </summary>        
    	//    [DisplayName("UOM3")]
    		public Nullable<int>  UOM3 { get; set; }
    
    		    
    		/// <summary>
    		/// UOM4
    		/// </summary>        
    	//    [DisplayName("UOM4")]
    		public Nullable<int>  UOM4 { get; set; }
    
    		    
    		/// <summary>
    		/// UOM5
    		/// </summary>        
    	//    [DisplayName("UOM5")]
    		public Nullable<int>  UOM5 { get; set; }
    
    		    
    		/// <summary>
    		/// UOM6
    		/// </summary>        
    	//    [DisplayName("UOM6")]
    		public Nullable<int>  UOM6 { get; set; }
    
    		    
    		/// <summary>
    		/// UOM7
    		/// </summary>        
    	//    [DisplayName("UOM7")]
    		public Nullable<int>  UOM7 { get; set; }
    
    		    
    		/// <summary>
    		/// UOM8
    		/// </summary>        
    	//    [DisplayName("UOM8")]
    		public Nullable<int>  UOM8 { get; set; }
    
    		    
    		/// <summary>
    		/// UOM9
    		/// </summary>        
    	//    [DisplayName("UOM9")]
    		public Nullable<int>  UOM9 { get; set; }
    
    		    
    		/// <summary>
    		/// UOM10
    		/// </summary>        
    	//    [DisplayName("UOM10")]
    		public Nullable<int>  UOM10 { get; set; }
    
    		    
    		/// <summary>
    		/// Updated Date
    		/// </summary>        
    	//    [DisplayName("Updated Date")]
    		public Nullable<System.DateTime>  UpdatedDate { get; set; }
    
    		    
    		/// <summary>
    		/// Updated By
    		/// </summary>        
    	//    [DisplayName("Updated By")]
            [MaxLength(10, ErrorMessage = "Updated By cannot be longer than 10 characters")]
    		public string  UpdatedBy { get; set; }
    
    		    
    		/// <summary>
    		/// Created Date
    		/// </summary>        
    	//    [DisplayName("Created Date")]
    		public Nullable<System.DateTime>  CreatedDate { get; set; }
    
    		    
    		/// <summary>
    		/// Created By
    		/// </summary>        
    	//    [DisplayName("Created By")]
            [MaxLength(10, ErrorMessage = "Created By cannot be longer than 10 characters")]
    		public string  CreatedBy { get; set; }
    
    		    
    	}
    //}
    
}
