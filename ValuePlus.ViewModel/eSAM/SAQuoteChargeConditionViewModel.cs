using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ValuePlus.ViewModel;



namespace ValuePlus.ViewModel.eSAM
{
    
    /// <summary>
    /// SAQuoteChargeCondition class
    /// </summary>
    //[MetadataType(typeof(SAQuoteChargeConditionViewModel))]
    //public  partial class SAQuoteChargeCondition
    //{
    
    	/// <summary>
    	/// SAQuoteChargeCondition Metadata class
    	/// </summary>
    	public   class SAQuoteChargeConditionViewModel:ViewModelBase
    	{
    		    
    		/// <summary>
    		/// ID
    		/// </summary>        
    	//    [DisplayName("ID")]
            [Required(ErrorMessage = "ID is required")]
    		public int  ID { get; set; }
    
    		    
    		/// <summary>
    		/// Condition
    		/// </summary>        
    	//    [DisplayName("Condition")]
            [MaxLength(50, ErrorMessage = "Condition cannot be longer than 50 characters")]
    		public string  Condition { get; set; }
    
    		    
    		/// <summary>
    		/// Created User ID
    		/// </summary>        
    	//    [DisplayName("Created User ID")]
            [MaxLength(10, ErrorMessage = "Created User ID cannot be longer than 10 characters")]
    		public string  CreatedUserID { get; set; }
    
    		    
    		/// <summary>
    		/// Created Date
    		/// </summary>        
    	//    [DisplayName("Created Date")]
    		public Nullable<System.DateTime>  CreatedDate { get; set; }
    
    		    
    		/// <summary>
    		/// SACharge ID
    		/// </summary>        
    	//    [DisplayName("SACharge ID")]
    		public Nullable<int>  SAChargeID { get; set; }
    
    		    
    		/// <summary>
    		/// Updated User ID
    		/// </summary>        
    	//    [DisplayName("Updated User ID")]
            [MaxLength(10, ErrorMessage = "Updated User ID cannot be longer than 10 characters")]
    		public string  UpdatedUserID { get; set; }
    
    		    
    		/// <summary>
    		/// UPdated Date
    		/// </summary>        
    	//    [DisplayName("UPdated Date")]
    		public Nullable<System.DateTime>  UPdatedDate { get; set; }
    
    		    
    		/// <summary>
    		/// Remark
    		/// </summary>        
    	//    [DisplayName("Remark")]
            [MaxLength(1000, ErrorMessage = "Remark cannot be longer than 1000 characters")]
    		public string  Remark { get; set; }
    
    		    
    	}
    //}
    
}
