using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ValuePlus.ViewModel;



namespace ValuePlus.ViewModel.eSAM
{
    
    /// <summary>
    /// SAFilesUpload class
    /// </summary>
    //[MetadataType(typeof(SAFilesUploadViewModel))]
    //public  partial class SAFilesUpload
    //{
    
    	/// <summary>
    	/// SAFilesUpload Metadata class
    	/// </summary>
    	public   class SAFilesUploadViewModel:ViewModelBase
    	{
    		    
    		/// <summary>
    		/// ID
    		/// </summary>        
    	//    [DisplayName("ID")]
            [Required(ErrorMessage = "ID is required")]
    		public int  ID { get; set; }
    
    		    
    		/// <summary>
    		/// File ID
    		/// </summary>        
    	//    [DisplayName("File ID")]
            [Required(ErrorMessage = "File ID is required")]
    		public int  FileID { get; set; }
    
    		    
    		/// <summary>
    		/// Status
    		/// </summary>        
    	//    [DisplayName("Status")]
            [Required(ErrorMessage = "Status is required")]
            [MaxLength(50, ErrorMessage = "Status cannot be longer than 50 characters")]
    		public string  Status { get; set; }
    
    		    
    		/// <summary>
    		/// Error Msg
    		/// </summary>        
    	//    [DisplayName("Error Msg")]
            [MaxLength(255, ErrorMessage = "Error Msg cannot be longer than 255 characters")]
    		public string  ErrorMsg { get; set; }
    
    		    
    	}
    //}
    
}
