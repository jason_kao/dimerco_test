using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ValuePlus.ViewModel;



namespace ValuePlus.ViewModel.eSAM
{
    
    /// <summary>
    /// SAQuoteToolSurcharges class
    /// </summary>
    //[MetadataType(typeof(SAQuoteToolSurchargesViewModel))]
    //public  partial class SAQuoteToolSurcharges
    //{
    
    	/// <summary>
    	/// SAQuoteToolSurcharges Metadata class
    	/// </summary>
    	public   class SAQuoteToolSurchargesViewModel:ViewModelBase
    	{
    		    
    		/// <summary>
    		/// ID
    		/// </summary>        
    	//    [DisplayName("ID")]
            [Required(ErrorMessage = "ID is required")]
    		public int  ID { get; set; }
    
    		    
    		/// <summary>
    		/// Quote ID
    		/// </summary>        
    	//    [DisplayName("Quote ID")]
    		public Nullable<int>  QuoteID { get; set; }
    
    		    
    		/// <summary>
    		/// Freight ID
    		/// </summary>        
    	//    [DisplayName("Freight ID")]
    		public Nullable<int>  FreightID { get; set; }
    
    		    
    		/// <summary>
    		/// MIN
    		/// </summary>        
    	//    [DisplayName("MIN")]
            [MaxLength(20, ErrorMessage = "MIN cannot be longer than 20 characters")]
    		public string  MIN { get; set; }
    
    		    
    		/// <summary>
    		/// CBM
    		/// </summary>        
    	//    [DisplayName("CBM")]
            [MaxLength(20, ErrorMessage = "CBM cannot be longer than 20 characters")]
    		public string  CBM { get; set; }
    
    		    
    		/// <summary>
    		/// TON
    		/// </summary>        
    	//    [DisplayName("TON")]
            [MaxLength(20, ErrorMessage = "TON cannot be longer than 20 characters")]
    		public string  TON { get; set; }
    
    		    
    		/// <summary>
    		/// Set Filing
    		/// </summary>        
    	//    [DisplayName("Set Filing")]
            [MaxLength(20, ErrorMessage = "Set Filing cannot be longer than 20 characters")]
    		public string  SetFiling { get; set; }
    
    		    
    		/// <summary>
    		/// Selling Rate1
    		/// </summary>        
    	//    [DisplayName("Selling Rate1")]
            [MaxLength(20, ErrorMessage = "Selling Rate1 cannot be longer than 20 characters")]
    		public string  SellingRate1 { get; set; }
    
    		    
    		/// <summary>
    		/// Selling Rate2
    		/// </summary>        
    	//    [DisplayName("Selling Rate2")]
            [MaxLength(20, ErrorMessage = "Selling Rate2 cannot be longer than 20 characters")]
    		public string  SellingRate2 { get; set; }
    
    		    
    		/// <summary>
    		/// Selling Rate3
    		/// </summary>        
    	//    [DisplayName("Selling Rate3")]
            [MaxLength(20, ErrorMessage = "Selling Rate3 cannot be longer than 20 characters")]
    		public string  SellingRate3 { get; set; }
    
    		    
    		/// <summary>
    		/// Selling Rate4
    		/// </summary>        
    	//    [DisplayName("Selling Rate4")]
            [MaxLength(20, ErrorMessage = "Selling Rate4 cannot be longer than 20 characters")]
    		public string  SellingRate4 { get; set; }
    
    		    
    		/// <summary>
    		/// Selling Rate5
    		/// </summary>        
    	//    [DisplayName("Selling Rate5")]
            [MaxLength(20, ErrorMessage = "Selling Rate5 cannot be longer than 20 characters")]
    		public string  SellingRate5 { get; set; }
    
    		    
    		/// <summary>
    		/// Selling Rate6
    		/// </summary>        
    	//    [DisplayName("Selling Rate6")]
            [MaxLength(20, ErrorMessage = "Selling Rate6 cannot be longer than 20 characters")]
    		public string  SellingRate6 { get; set; }
    
    		    
    		/// <summary>
    		/// Selling Rate7
    		/// </summary>        
    	//    [DisplayName("Selling Rate7")]
            [MaxLength(20, ErrorMessage = "Selling Rate7 cannot be longer than 20 characters")]
    		public string  SellingRate7 { get; set; }
    
    		    
    		/// <summary>
    		/// Selling Rate8
    		/// </summary>        
    	//    [DisplayName("Selling Rate8")]
            [MaxLength(20, ErrorMessage = "Selling Rate8 cannot be longer than 20 characters")]
    		public string  SellingRate8 { get; set; }
    
    		    
    		/// <summary>
    		/// Selling Rate9
    		/// </summary>        
    	//    [DisplayName("Selling Rate9")]
            [MaxLength(20, ErrorMessage = "Selling Rate9 cannot be longer than 20 characters")]
    		public string  SellingRate9 { get; set; }
    
    		    
    		/// <summary>
    		/// Selling Rate10
    		/// </summary>        
    	//    [DisplayName("Selling Rate10")]
            [MaxLength(20, ErrorMessage = "Selling Rate10 cannot be longer than 20 characters")]
    		public string  SellingRate10 { get; set; }
    
    		    
    		/// <summary>
    		/// Created User ID
    		/// </summary>        
    	//    [DisplayName("Created User ID")]
            [MaxLength(10, ErrorMessage = "Created User ID cannot be longer than 10 characters")]
    		public string  CreatedUserID { get; set; }
    
    		    
    		/// <summary>
    		/// Created Date
    		/// </summary>        
    	//    [DisplayName("Created Date")]
    		public Nullable<System.DateTime>  CreatedDate { get; set; }
    
    		    
    		/// <summary>
    		/// Updated User ID
    		/// </summary>        
    	//    [DisplayName("Updated User ID")]
            [MaxLength(10, ErrorMessage = "Updated User ID cannot be longer than 10 characters")]
    		public string  UpdatedUserID { get; set; }
    
    		    
    		/// <summary>
    		/// Updated Date
    		/// </summary>        
    	//    [DisplayName("Updated Date")]
    		public Nullable<System.DateTime>  UpdatedDate { get; set; }
    
    		    
    		/// <summary>
    		/// s Type
    		/// </summary>        
    	//    [DisplayName("s Type")]
            [MaxLength(20, ErrorMessage = "s Type cannot be longer than 20 characters")]
    		public string  sType { get; set; }
    
    		    
    		/// <summary>
    		/// Remarks
    		/// </summary>        
    	//    [DisplayName("Remarks")]
            [MaxLength(200, ErrorMessage = "Remarks cannot be longer than 200 characters")]
    		public string  Remarks { get; set; }
    
    		    
    		/// <summary>
    		/// Lane Segment
    		/// </summary>        
    	//    [DisplayName("Lane Segment")]
            [MaxLength(500, ErrorMessage = "Lane Segment cannot be longer than 500 characters")]
    		public string  LaneSegment { get; set; }
    
    		    
    		/// <summary>
    		/// Charge Code
    		/// </summary>        
    	//    [DisplayName("Charge Code")]
            [MaxLength(255, ErrorMessage = "Charge Code cannot be longer than 255 characters")]
    		public string  ChargeCode { get; set; }
    
    		    
    		/// <summary>
    		/// Currency Code
    		/// </summary>        
    	//    [DisplayName("Currency Code")]
            [MaxLength(50, ErrorMessage = "Currency Code cannot be longer than 50 characters")]
    		public string  CurrencyCode { get; set; }
    
    		    
    		/// <summary>
    		/// Sub Charge Code
    		/// </summary>        
    	//    [DisplayName("Sub Charge Code")]
            [MaxLength(255, ErrorMessage = "Sub Charge Code cannot be longer than 255 characters")]
    		public string  SubChargeCode { get; set; }
    
    		    
    		/// <summary>
    		/// Charge Code ID
    		/// </summary>        
    	//    [DisplayName("Charge Code ID")]
    		public Nullable<int>  ChargeCodeID { get; set; }
    
    		    
    		/// <summary>
    		/// Sub Charge Code ID
    		/// </summary>        
    	//    [DisplayName("Sub Charge Code ID")]
    		public Nullable<int>  SubChargeCodeID { get; set; }
    
    		    
    	}
    //}
    
}
