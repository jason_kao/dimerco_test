using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ValuePlus.ViewModel;



namespace ValuePlus.ViewModel.eSAM
{
    
    /// <summary>
    /// SASecuredDataTmp class
    /// </summary>
    //[MetadataType(typeof(SASecuredDataTmpViewModel))]
    //public  partial class SASecuredDataTmp
    //{
    
    	/// <summary>
    	/// SASecuredDataTmp Metadata class
    	/// </summary>
    	public   class SASecuredDataTmpViewModel:ViewModelBase
    	{
    		    
    		/// <summary>
    		/// Confirm Id
    		/// </summary>        
    	//    [DisplayName("Confirm Id")]
            [MaxLength(100, ErrorMessage = "Confirm Id cannot be longer than 100 characters")]
    		public string  ConfirmId { get; set; }
    
    		    
    		/// <summary>
    		/// Issue Date
    		/// </summary>        
    	//    [DisplayName("Issue Date")]
    		public Nullable<System.DateTime>  IssueDate { get; set; }
    
    		    
    		/// <summary>
    		/// Shpt No
    		/// </summary>        
    	//    [DisplayName("Shpt No")]
            [MaxLength(50, ErrorMessage = "Shpt No cannot be longer than 50 characters")]
    		public string  ShptNo { get; set; }
    
    		    
    		/// <summary>
    		/// Lot No
    		/// </summary>        
    	//    [DisplayName("Lot No")]
            [MaxLength(20, ErrorMessage = "Lot No cannot be longer than 20 characters")]
    		public string  LotNo { get; set; }
    
    		    
    		/// <summary>
    		/// Station ID
    		/// </summary>        
    	//    [DisplayName("Station ID")]
            [Required(ErrorMessage = "Station ID is required")]
            [MaxLength(10, ErrorMessage = "Station ID cannot be longer than 10 characters")]
    		public string  StationID { get; set; }
    
    		    
    		/// <summary>
    		/// Rnk
    		/// </summary>        
    	//    [DisplayName("Rnk")]
    		public Nullable<long>  Rnk { get; set; }
    
    		    
    	}
    //}
    
}
