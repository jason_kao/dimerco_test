using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ValuePlus.ViewModel;



namespace ValuePlus.ViewModel.eSAM
{
    
    /// <summary>
    /// SAUserRole class
    /// </summary>
    //[MetadataType(typeof(SAUserRoleViewModel))]
    //public  partial class SAUserRole
    //{
    
    	/// <summary>
    	/// SAUserRole Metadata class
    	/// </summary>
    	public   class SAUserRoleViewModel:ViewModelBase
    	{
    		    
    		/// <summary>
    		/// ID
    		/// </summary>        
    	//    [DisplayName("ID")]
            [Required(ErrorMessage = "ID is required")]
    		public int  ID { get; set; }
    
    		    
    		/// <summary>
    		/// Role Name
    		/// </summary>        
    	//    [DisplayName("Role Name")]
            [MaxLength(50, ErrorMessage = "Role Name cannot be longer than 50 characters")]
    		public string  RoleName { get; set; }
    
    		    
    		/// <summary>
    		/// User ID
    		/// </summary>        
    	//    [DisplayName("User ID")]
            [MaxLength(10, ErrorMessage = "User ID cannot be longer than 10 characters")]
    		public string  UserID { get; set; }
    
    		    
    		/// <summary>
    		/// Status
    		/// </summary>        
    	//    [DisplayName("Status")]
            [MaxLength(10, ErrorMessage = "Status cannot be longer than 10 characters")]
    		public string  Status { get; set; }
    
    		    
    		/// <summary>
    		/// Dept Type
    		/// </summary>        
    	//    [DisplayName("Dept Type")]
            [MaxLength(20, ErrorMessage = "Dept Type cannot be longer than 20 characters")]
    		public string  DeptType { get; set; }
    
    		    
    		/// <summary>
    		/// Dept Code
    		/// </summary>        
    	//    [DisplayName("Dept Code")]
            [MaxLength(20, ErrorMessage = "Dept Code cannot be longer than 20 characters")]
    		public string  DeptCode { get; set; }
    
    		    
    		/// <summary>
    		/// Item
    		/// </summary>        
    	//    [DisplayName("Item")]
            [MaxLength(20, ErrorMessage = "Item cannot be longer than 20 characters")]
    		public string  Item { get; set; }
    
    		    
    		/// <summary>
    		/// Item Name
    		/// </summary>        
    	//    [DisplayName("Item Name")]
            [MaxLength(20, ErrorMessage = "Item Name cannot be longer than 20 characters")]
    		public string  ItemName { get; set; }
    
    		    
    		/// <summary>
    		/// Created User ID
    		/// </summary>        
    	//    [DisplayName("Created User ID")]
            [MaxLength(10, ErrorMessage = "Created User ID cannot be longer than 10 characters")]
    		public string  CreatedUserID { get; set; }
    
    		    
    		/// <summary>
    		/// Created Date
    		/// </summary>        
    	//    [DisplayName("Created Date")]
    		public Nullable<System.DateTime>  CreatedDate { get; set; }
    
    		    
    		/// <summary>
    		/// Update User ID
    		/// </summary>        
    	//    [DisplayName("Update User ID")]
            [MaxLength(10, ErrorMessage = "Update User ID cannot be longer than 10 characters")]
    		public string  UpdateUserID { get; set; }
    
    		    
    		/// <summary>
    		/// Update Date
    		/// </summary>        
    	//    [DisplayName("Update Date")]
    		public Nullable<System.DateTime>  UpdateDate { get; set; }
    
    		    
    	}
    //}
    
}
