using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ValuePlus.ViewModel;



namespace ValuePlus.ViewModel.eSAM
{
    
    /// <summary>
    /// SADelRelativeRight class
    /// </summary>
    //[MetadataType(typeof(SADelRelativeRightViewModel))]
    //public  partial class SADelRelativeRight
    //{
    
    	/// <summary>
    	/// SADelRelativeRight Metadata class
    	/// </summary>
    	public   class SADelRelativeRightViewModel:ViewModelBase
    	{
    		    
    		/// <summary>
    		/// ID
    		/// </summary>        
    	//    [DisplayName("ID")]
            [Required(ErrorMessage = "ID is required")]
    		public int  ID { get; set; }
    
    		    
    		/// <summary>
    		/// Station ID
    		/// </summary>        
    	//    [DisplayName("Station ID")]
            [MaxLength(3, ErrorMessage = "Station ID cannot be longer than 3 characters")]
    		public string  StationID { get; set; }
    
    		    
    		/// <summary>
    		/// Created Date
    		/// </summary>        
    	//    [DisplayName("Created Date")]
    		public Nullable<System.DateTime>  CreatedDate { get; set; }
    
    		    
    		/// <summary>
    		/// Status
    		/// </summary>        
    	//    [DisplayName("Status")]
    		public Nullable<int>  Status { get; set; }
    
    		    
    	}
    //}
    
}
