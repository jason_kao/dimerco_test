using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ValuePlus.ViewModel;



namespace ValuePlus.ViewModel.eSAM
{
    
    /// <summary>
    /// SASCScoreRules class
    /// </summary>
    //[MetadataType(typeof(SASCScoreRulesViewModel))]
    //public  partial class SASCScoreRules
    //{
    
    	/// <summary>
    	/// SASCScoreRules Metadata class
    	/// </summary>
    	public   class SASCScoreRulesViewModel:ViewModelBase
    	{
    		    
    		/// <summary>
    		/// HQID
    		/// </summary>        
    	//    [DisplayName("HQID")]
            [Required(ErrorMessage = "HQID is required")]
    		public int  HQID { get; set; }
    
    		    
    		/// <summary>
    		/// Item ID
    		/// </summary>        
    	//    [DisplayName("Item ID")]
    		public Nullable<int>  ItemID { get; set; }
    
    		    
    		/// <summary>
    		/// Sort ID
    		/// </summary>        
    	//    [DisplayName("Sort ID")]
    		public Nullable<int>  SortID { get; set; }
    
    		    
    		/// <summary>
    		/// Start PCT
    		/// </summary>        
    	//    [DisplayName("Start PCT")]
    		public Nullable<int>  StartPCT { get; set; }
    
    		    
    		/// <summary>
    		/// End PCT
    		/// </summary>        
    	//    [DisplayName("End PCT")]
    		public Nullable<int>  EndPCT { get; set; }
    
    		    
    		/// <summary>
    		/// is By PCT
    		/// </summary>        
    	//    [DisplayName("is By PCT")]
    		public Nullable<bool>  isByPCT { get; set; }
    
    		    
    		/// <summary>
    		/// Fixed Score
    		/// </summary>        
    	//    [DisplayName("Fixed Score")]
    		public Nullable<double>  FixedScore { get; set; }
    
    		    
    		/// <summary>
    		/// Status
    		/// </summary>        
    	//    [DisplayName("Status")]
            [MaxLength(10, ErrorMessage = "Status cannot be longer than 10 characters")]
    		public string  Status { get; set; }
    
    		    
    		/// <summary>
    		/// Created Date
    		/// </summary>        
    	//    [DisplayName("Created Date")]
    		public Nullable<System.DateTime>  CreatedDate { get; set; }
    
    		    
    		/// <summary>
    		/// Created By
    		/// </summary>        
    	//    [DisplayName("Created By")]
            [MaxLength(10, ErrorMessage = "Created By cannot be longer than 10 characters")]
    		public string  CreatedBy { get; set; }
    
    		    
    		/// <summary>
    		/// Updated Date
    		/// </summary>        
    	//    [DisplayName("Updated Date")]
    		public Nullable<System.DateTime>  UpdatedDate { get; set; }
    
    		    
    		/// <summary>
    		/// Updated By
    		/// </summary>        
    	//    [DisplayName("Updated By")]
            [MaxLength(10, ErrorMessage = "Updated By cannot be longer than 10 characters")]
    		public string  UpdatedBy { get; set; }
    
    		    
    	}
    //}
    
}
