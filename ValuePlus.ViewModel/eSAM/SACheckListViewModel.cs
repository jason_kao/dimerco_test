using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ValuePlus.ViewModel;



namespace ValuePlus.ViewModel.eSAM
{
    
    /// <summary>
    /// SACheckList class
    /// </summary>
    //[MetadataType(typeof(SACheckListViewModel))]
    //public  partial class SACheckList
    //{
    
    	/// <summary>
    	/// SACheckList Metadata class
    	/// </summary>
    	public   class SACheckListViewModel:ViewModelBase
    	{
    		    
    		/// <summary>
    		/// ID
    		/// </summary>        
    	//    [DisplayName("ID")]
            [Required(ErrorMessage = "ID is required")]
    		public int  ID { get; set; }
    
    		    
    		/// <summary>
    		/// Station IP
    		/// </summary>        
    	//    [DisplayName("Station IP")]
            [MaxLength(20, ErrorMessage = "Station IP cannot be longer than 20 characters")]
    		public string  StationIP { get; set; }
    
    		    
    		/// <summary>
    		/// Server Name
    		/// </summary>        
    	//    [DisplayName("Server Name")]
            [MaxLength(30, ErrorMessage = "Server Name cannot be longer than 30 characters")]
    		public string  ServerName { get; set; }
    
    		    
    		/// <summary>
    		/// PLID
    		/// </summary>        
    	//    [DisplayName("PLID")]
    		public Nullable<int>  PLID { get; set; }
    
    		    
    		/// <summary>
    		/// Status
    		/// </summary>        
    	//    [DisplayName("Status")]
            [MaxLength(20, ErrorMessage = "Status cannot be longer than 20 characters")]
    		public string  Status { get; set; }
    
    		    
    		/// <summary>
    		/// Cmd
    		/// </summary>        
    	//    [DisplayName("Cmd")]
            [MaxLength(255, ErrorMessage = "Cmd cannot be longer than 255 characters")]
    		public string  Cmd { get; set; }
    
    		    
    		/// <summary>
    		/// Created Date
    		/// </summary>        
    	//    [DisplayName("Created Date")]
    		public Nullable<System.DateTime>  CreatedDate { get; set; }
    
    		    
    		/// <summary>
    		/// Updated Date
    		/// </summary>        
    	//    [DisplayName("Updated Date")]
    		public Nullable<System.DateTime>  UpdatedDate { get; set; }
    
    		    
    		/// <summary>
    		/// Source ID
    		/// </summary>        
    	//    [DisplayName("Source ID")]
    		public Nullable<int>  SourceID { get; set; }
    
    		    
    		/// <summary>
    		/// Station Code
    		/// </summary>        
    	//    [DisplayName("Station Code")]
            [MaxLength(10, ErrorMessage = "Station Code cannot be longer than 10 characters")]
    		public string  StationCode { get; set; }
    
    		    
    		/// <summary>
    		/// Customer Name
    		/// </summary>        
    	//    [DisplayName("Customer Name")]
            [MaxLength(100, ErrorMessage = "Customer Name cannot be longer than 100 characters")]
    		public string  CustomerName { get; set; }
    
    		    
    	}
    //}
    
}
