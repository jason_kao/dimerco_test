using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ValuePlus.ViewModel;



namespace ValuePlus.ViewModel.eSAM
{
    
    /// <summary>
    /// SAQuoteFreightHistoricalRate class
    /// </summary>
    //[MetadataType(typeof(SAQuoteFreightHistoricalRateViewModel))]
    //public  partial class SAQuoteFreightHistoricalRate
    //{
    
    	/// <summary>
    	/// SAQuoteFreightHistoricalRate Metadata class
    	/// </summary>
    	public   class SAQuoteFreightHistoricalRateViewModel:ViewModelBase
    	{
    		    
    		/// <summary>
    		/// HQID
    		/// </summary>        
    	//    [DisplayName("HQID")]
            [Required(ErrorMessage = "HQID is required")]
    		public int  HQID { get; set; }
    
    		    
    		/// <summary>
    		/// LID
    		/// </summary>        
    	//    [DisplayName("LID")]
    		public Nullable<int>  LID { get; set; }
    
    		    
    		/// <summary>
    		/// Origin City ID
    		/// </summary>        
    	//    [DisplayName("Origin City ID")]
    		public Nullable<int>  OriginCityID { get; set; }
    
    		    
    		/// <summary>
    		/// Destination City ID
    		/// </summary>        
    	//    [DisplayName("Destination City ID")]
    		public Nullable<int>  DestinationCityID { get; set; }
    
    		    
    		/// <summary>
    		/// Ploading Port ID
    		/// </summary>        
    	//    [DisplayName("Ploading Port ID")]
    		public Nullable<int>  PloadingPortID { get; set; }
    
    		    
    		/// <summary>
    		/// PDischarge Port ID
    		/// </summary>        
    	//    [DisplayName("PDischarge Port ID")]
    		public Nullable<int>  PDischargePortID { get; set; }
    
    		    
    		/// <summary>
    		/// Carrier ID
    		/// </summary>        
    	//    [DisplayName("Carrier ID")]
    		public Nullable<int>  CarrierID { get; set; }
    
    		    
    		/// <summary>
    		/// Charge Item ID
    		/// </summary>        
    	//    [DisplayName("Charge Item ID")]
    		public Nullable<int>  ChargeItemID { get; set; }
    
    		    
    		/// <summary>
    		/// Min Selling Rate
    		/// </summary>        
    	//    [DisplayName("Min Selling Rate")]
    		public Nullable<decimal>  MinSellingRate { get; set; }
    
    		    
    		/// <summary>
    		/// Max Selling Rate
    		/// </summary>        
    	//    [DisplayName("Max Selling Rate")]
    		public Nullable<decimal>  MaxSellingRate { get; set; }
    
    		    
    		/// <summary>
    		/// Last Year Selling Rate
    		/// </summary>        
    	//    [DisplayName("Last Year Selling Rate")]
    		public Nullable<decimal>  LastYearSellingRate { get; set; }
    
    		    
    		/// <summary>
    		/// Last Two Year Selling Rate
    		/// </summary>        
    	//    [DisplayName("Last Two Year Selling Rate")]
    		public Nullable<decimal>  LastTwoYearSellingRate { get; set; }
    
    		    
    		/// <summary>
    		/// Last Month Selling Rate
    		/// </summary>        
    	//    [DisplayName("Last Month Selling Rate")]
    		public Nullable<decimal>  LastMonthSellingRate { get; set; }
    
    		    
    		/// <summary>
    		/// Last Two Month Selling Rate
    		/// </summary>        
    	//    [DisplayName("Last Two Month Selling Rate")]
    		public Nullable<decimal>  LastTwoMonthSellingRate { get; set; }
    
    		    
    		/// <summary>
    		/// is All Quote
    		/// </summary>        
    	//    [DisplayName("is All Quote")]
            [MaxLength(1, ErrorMessage = "is All Quote cannot be longer than 1 characters")]
    		public string  isAllQuote { get; set; }
    
    		    
    		/// <summary>
    		/// Created Date
    		/// </summary>        
    	//    [DisplayName("Created Date")]
    		public Nullable<System.DateTime>  CreatedDate { get; set; }
    
    		    
    	}
    //}
    
}
