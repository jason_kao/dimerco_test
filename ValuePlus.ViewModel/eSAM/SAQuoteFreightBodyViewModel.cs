using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ValuePlus.ViewModel;



namespace ValuePlus.ViewModel.eSAM
{
    
    /// <summary>
    /// SAQuoteFreightBody class
    /// </summary>
    //[MetadataType(typeof(SAQuoteFreightBodyViewModel))]
    //public  partial class SAQuoteFreightBody
    //{
    
    	/// <summary>
    	/// SAQuoteFreightBody Metadata class
    	/// </summary>
    	public   class SAQuoteFreightBodyViewModel:ViewModelBase
    	{
    		    
    		/// <summary>
    		/// ID
    		/// </summary>        
    	//    [DisplayName("ID")]
            [Required(ErrorMessage = "ID is required")]
    		public int  ID { get; set; }
    
    		    
    		/// <summary>
    		/// FID
    		/// </summary>        
    	//    [DisplayName("FID")]
    		public Nullable<int>  FID { get; set; }
    
    		    
    		/// <summary>
    		/// Cost1
    		/// </summary>        
    	//    [DisplayName("Cost1")]
            [MaxLength(10, ErrorMessage = "Cost1 cannot be longer than 10 characters")]
    		public string  Cost1 { get; set; }
    
    		    
    		/// <summary>
    		/// Cost2
    		/// </summary>        
    	//    [DisplayName("Cost2")]
            [MaxLength(10, ErrorMessage = "Cost2 cannot be longer than 10 characters")]
    		public string  Cost2 { get; set; }
    
    		    
    		/// <summary>
    		/// Cost3
    		/// </summary>        
    	//    [DisplayName("Cost3")]
            [MaxLength(10, ErrorMessage = "Cost3 cannot be longer than 10 characters")]
    		public string  Cost3 { get; set; }
    
    		    
    		/// <summary>
    		/// Cost4
    		/// </summary>        
    	//    [DisplayName("Cost4")]
            [MaxLength(10, ErrorMessage = "Cost4 cannot be longer than 10 characters")]
    		public string  Cost4 { get; set; }
    
    		    
    		/// <summary>
    		/// Cost5
    		/// </summary>        
    	//    [DisplayName("Cost5")]
            [MaxLength(10, ErrorMessage = "Cost5 cannot be longer than 10 characters")]
    		public string  Cost5 { get; set; }
    
    		    
    		/// <summary>
    		/// Cost6
    		/// </summary>        
    	//    [DisplayName("Cost6")]
            [MaxLength(10, ErrorMessage = "Cost6 cannot be longer than 10 characters")]
    		public string  Cost6 { get; set; }
    
    		    
    		/// <summary>
    		/// Cost7
    		/// </summary>        
    	//    [DisplayName("Cost7")]
            [MaxLength(10, ErrorMessage = "Cost7 cannot be longer than 10 characters")]
    		public string  Cost7 { get; set; }
    
    		    
    		/// <summary>
    		/// Cost8
    		/// </summary>        
    	//    [DisplayName("Cost8")]
            [MaxLength(10, ErrorMessage = "Cost8 cannot be longer than 10 characters")]
    		public string  Cost8 { get; set; }
    
    		    
    		/// <summary>
    		/// Cost9
    		/// </summary>        
    	//    [DisplayName("Cost9")]
            [MaxLength(10, ErrorMessage = "Cost9 cannot be longer than 10 characters")]
    		public string  Cost9 { get; set; }
    
    		    
    		/// <summary>
    		/// Cost10
    		/// </summary>        
    	//    [DisplayName("Cost10")]
            [MaxLength(10, ErrorMessage = "Cost10 cannot be longer than 10 characters")]
    		public string  Cost10 { get; set; }
    
    		    
    		/// <summary>
    		/// Profit1
    		/// </summary>        
    	//    [DisplayName("Profit1")]
            [MaxLength(10, ErrorMessage = "Profit1 cannot be longer than 10 characters")]
    		public string  Profit1 { get; set; }
    
    		    
    		/// <summary>
    		/// Profit2
    		/// </summary>        
    	//    [DisplayName("Profit2")]
            [MaxLength(10, ErrorMessage = "Profit2 cannot be longer than 10 characters")]
    		public string  Profit2 { get; set; }
    
    		    
    		/// <summary>
    		/// Profit3
    		/// </summary>        
    	//    [DisplayName("Profit3")]
            [MaxLength(10, ErrorMessage = "Profit3 cannot be longer than 10 characters")]
    		public string  Profit3 { get; set; }
    
    		    
    		/// <summary>
    		/// Profit4
    		/// </summary>        
    	//    [DisplayName("Profit4")]
            [MaxLength(10, ErrorMessage = "Profit4 cannot be longer than 10 characters")]
    		public string  Profit4 { get; set; }
    
    		    
    		/// <summary>
    		/// Profit5
    		/// </summary>        
    	//    [DisplayName("Profit5")]
            [MaxLength(10, ErrorMessage = "Profit5 cannot be longer than 10 characters")]
    		public string  Profit5 { get; set; }
    
    		    
    		/// <summary>
    		/// Profit6
    		/// </summary>        
    	//    [DisplayName("Profit6")]
            [MaxLength(10, ErrorMessage = "Profit6 cannot be longer than 10 characters")]
    		public string  Profit6 { get; set; }
    
    		    
    		/// <summary>
    		/// Profit7
    		/// </summary>        
    	//    [DisplayName("Profit7")]
            [MaxLength(10, ErrorMessage = "Profit7 cannot be longer than 10 characters")]
    		public string  Profit7 { get; set; }
    
    		    
    		/// <summary>
    		/// Profit8
    		/// </summary>        
    	//    [DisplayName("Profit8")]
            [MaxLength(10, ErrorMessage = "Profit8 cannot be longer than 10 characters")]
    		public string  Profit8 { get; set; }
    
    		    
    		/// <summary>
    		/// Profit9
    		/// </summary>        
    	//    [DisplayName("Profit9")]
            [MaxLength(10, ErrorMessage = "Profit9 cannot be longer than 10 characters")]
    		public string  Profit9 { get; set; }
    
    		    
    		/// <summary>
    		/// Profit10
    		/// </summary>        
    	//    [DisplayName("Profit10")]
            [MaxLength(10, ErrorMessage = "Profit10 cannot be longer than 10 characters")]
    		public string  Profit10 { get; set; }
    
    		    
    	}
    //}
    
}
