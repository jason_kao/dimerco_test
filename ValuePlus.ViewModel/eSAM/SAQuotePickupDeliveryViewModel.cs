using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ValuePlus.ViewModel;



namespace ValuePlus.ViewModel.eSAM
{
    
    /// <summary>
    /// SAQuotePickupDelivery class
    /// </summary>
    //[MetadataType(typeof(SAQuotePickupDeliveryViewModel))]
    //public  partial class SAQuotePickupDelivery
    //{
    
    	/// <summary>
    	/// SAQuotePickupDelivery Metadata class
    	/// </summary>
    	public   class SAQuotePickupDeliveryViewModel:ViewModelBase
    	{
    		    
    		/// <summary>
    		/// ID
    		/// </summary>        
    	//    [DisplayName("ID")]
            [Required(ErrorMessage = "ID is required")]
    		public int  ID { get; set; }
    
    		    
    		/// <summary>
    		/// Quote ID
    		/// </summary>        
    	//    [DisplayName("Quote ID")]
    		public Nullable<int>  QuoteID { get; set; }
    
    		    
    		/// <summary>
    		/// Customer ID
    		/// </summary>        
    	//    [DisplayName("Customer ID")]
    		public Nullable<int>  CustomerID { get; set; }
    
    		    
    		/// <summary>
    		/// Contact Name
    		/// </summary>        
    	//    [DisplayName("Contact Name")]
            [MaxLength(50, ErrorMessage = "Contact Name cannot be longer than 50 characters")]
    		public string  ContactName { get; set; }
    
    		    
    		/// <summary>
    		/// Contact Phone
    		/// </summary>        
    	//    [DisplayName("Contact Phone")]
            [MaxLength(50, ErrorMessage = "Contact Phone cannot be longer than 50 characters")]
    		public string  ContactPhone { get; set; }
    
    		    
    		/// <summary>
    		/// Country
    		/// </summary>        
    	//    [DisplayName("Country")]
            [MaxLength(255, ErrorMessage = "Country cannot be longer than 255 characters")]
    		public string  Country { get; set; }
    
    		    
    		/// <summary>
    		/// State
    		/// </summary>        
    	//    [DisplayName("State")]
            [MaxLength(255, ErrorMessage = "State cannot be longer than 255 characters")]
    		public string  State { get; set; }
    
    		    
    		/// <summary>
    		/// City
    		/// </summary>        
    	//    [DisplayName("City")]
            [MaxLength(255, ErrorMessage = "City cannot be longer than 255 characters")]
    		public string  City { get; set; }
    
    		    
    		/// <summary>
    		/// Zip
    		/// </summary>        
    	//    [DisplayName("Zip")]
            [MaxLength(50, ErrorMessage = "Zip cannot be longer than 50 characters")]
    		public string  Zip { get; set; }
    
    		    
    		/// <summary>
    		/// Address
    		/// </summary>        
    	//    [DisplayName("Address")]
            [MaxLength(255, ErrorMessage = "Address cannot be longer than 255 characters")]
    		public string  Address { get; set; }
    
    		    
    		/// <summary>
    		/// PD
    		/// </summary>        
    	//    [DisplayName("PD")]
            [MaxLength(10, ErrorMessage = "PD cannot be longer than 10 characters")]
    		public string  PD { get; set; }
    
    		    
    	}
    //}
    
}
