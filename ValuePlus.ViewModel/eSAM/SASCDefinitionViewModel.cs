using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ValuePlus.ViewModel;



namespace ValuePlus.ViewModel.eSAM
{
    
    /// <summary>
    /// SASCDefinition class
    /// </summary>
    //[MetadataType(typeof(SASCDefinitionViewModel))]
    //public  partial class SASCDefinition
    //{
    
    	/// <summary>
    	/// SASCDefinition Metadata class
    	/// </summary>
    	public   class SASCDefinitionViewModel:ViewModelBase
    	{
    		    
    		/// <summary>
    		/// HQID
    		/// </summary>        
    	//    [DisplayName("HQID")]
            [Required(ErrorMessage = "HQID is required")]
    		public int  HQID { get; set; }
    
    		    
    		/// <summary>
    		/// Party Lvl
    		/// </summary>        
    	//    [DisplayName("Party Lvl")]
    		public Nullable<int>  PartyLvl { get; set; }
    
    		    
    		/// <summary>
    		/// Party Type
    		/// </summary>        
    	//    [DisplayName("Party Type")]
            [MaxLength(20, ErrorMessage = "Party Type cannot be longer than 20 characters")]
    		public string  PartyType { get; set; }
    
    		    
    		/// <summary>
    		/// Party Code
    		/// </summary>        
    	//    [DisplayName("Party Code")]
            [MaxLength(20, ErrorMessage = "Party Code cannot be longer than 20 characters")]
    		public string  PartyCode { get; set; }
    
    		    
    		/// <summary>
    		/// Party ID
    		/// </summary>        
    	//    [DisplayName("Party ID")]
            [MaxLength(20, ErrorMessage = "Party ID cannot be longer than 20 characters")]
    		public string  PartyID { get; set; }
    
    		    
    		/// <summary>
    		/// Definition Type ID
    		/// </summary>        
    	//    [DisplayName("Definition Type ID")]
    		public Nullable<int>  DefinitionTypeID { get; set; }
    
    		    
    		/// <summary>
    		/// Value
    		/// </summary>        
    	//    [DisplayName("Value")]
    		public Nullable<int>  Value { get; set; }
    
    		    
    		/// <summary>
    		/// Value Unit
    		/// </summary>        
    	//    [DisplayName("Value Unit")]
            [MaxLength(50, ErrorMessage = "Value Unit cannot be longer than 50 characters")]
    		public string  ValueUnit { get; set; }
    
    		    
    		/// <summary>
    		/// Status
    		/// </summary>        
    	//    [DisplayName("Status")]
            [MaxLength(10, ErrorMessage = "Status cannot be longer than 10 characters")]
    		public string  Status { get; set; }
    
    		    
    		/// <summary>
    		/// Created Date
    		/// </summary>        
    	//    [DisplayName("Created Date")]
    		public Nullable<System.DateTime>  CreatedDate { get; set; }
    
    		    
    		/// <summary>
    		/// Created By
    		/// </summary>        
    	//    [DisplayName("Created By")]
            [MaxLength(10, ErrorMessage = "Created By cannot be longer than 10 characters")]
    		public string  CreatedBy { get; set; }
    
    		    
    		/// <summary>
    		/// Updated Date
    		/// </summary>        
    	//    [DisplayName("Updated Date")]
    		public Nullable<System.DateTime>  UpdatedDate { get; set; }
    
    		    
    		/// <summary>
    		/// Updated By
    		/// </summary>        
    	//    [DisplayName("Updated By")]
            [MaxLength(10, ErrorMessage = "Updated By cannot be longer than 10 characters")]
    		public string  UpdatedBy { get; set; }
    
    		    
    	}
    //}
    
}
