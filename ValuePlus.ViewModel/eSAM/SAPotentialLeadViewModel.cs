using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ValuePlus.ViewModel;



namespace ValuePlus.ViewModel.eSAM
{
    
    /// <summary>
    /// SAPotentialLead class
    /// </summary>
    //[MetadataType(typeof(SAPotentialLeadViewModel))]
    //public  partial class SAPotentialLead
    //{
    
    	/// <summary>
    	/// SAPotentialLead Metadata class
    	/// </summary>
    	public   class SAPotentialLeadViewModel:ViewModelBase
    	{
    		    
    		/// <summary>
    		/// HQID
    		/// </summary>        
    	//    [DisplayName("HQID")]
            [Required(ErrorMessage = "HQID is required")]
    		public int  HQID { get; set; }
    
    		    
    		/// <summary>
    		/// NAME
    		/// </summary>        
    	//    [DisplayName("NAME")]
            [MaxLength(255, ErrorMessage = "NAME cannot be longer than 255 characters")]
    		public string  NAME { get; set; }
    
    		    
    		/// <summary>
    		/// ADDRESS1
    		/// </summary>        
    	//    [DisplayName("ADDRESS1")]
            [MaxLength(255, ErrorMessage = "ADDRESS1 cannot be longer than 255 characters")]
    		public string  ADDRESS1 { get; set; }
    
    		    
    		/// <summary>
    		/// CITYNAME
    		/// </summary>        
    	//    [DisplayName("CITYNAME")]
            [MaxLength(255, ErrorMessage = "CITYNAME cannot be longer than 255 characters")]
    		public string  CITYNAME { get; set; }
    
    		    
    		/// <summary>
    		/// City ID
    		/// </summary>        
    	//    [DisplayName("City ID")]
    		public Nullable<int>  CityID { get; set; }
    
    		    
    		/// <summary>
    		/// STATEPROVINCECODE
    		/// </summary>        
    	//    [DisplayName("STATEPROVINCECODE")]
            [MaxLength(255, ErrorMessage = "STATEPROVINCECODE cannot be longer than 255 characters")]
    		public string  STATEPROVINCECODE { get; set; }
    
    		    
    		/// <summary>
    		/// State ID
    		/// </summary>        
    	//    [DisplayName("State ID")]
            [MaxLength(20, ErrorMessage = "State ID cannot be longer than 20 characters")]
    		public string  StateID { get; set; }
    
    		    
    		/// <summary>
    		/// POSTALCODE
    		/// </summary>        
    	//    [DisplayName("POSTALCODE")]
            [MaxLength(10, ErrorMessage = "POSTALCODE cannot be longer than 10 characters")]
    		public string  POSTALCODE { get; set; }
    
    		    
    		/// <summary>
    		/// REGIONNAME
    		/// </summary>        
    	//    [DisplayName("REGIONNAME")]
            [MaxLength(255, ErrorMessage = "REGIONNAME cannot be longer than 255 characters")]
    		public string  REGIONNAME { get; set; }
    
    		    
    		/// <summary>
    		/// COUNTRYNAME
    		/// </summary>        
    	//    [DisplayName("COUNTRYNAME")]
            [MaxLength(255, ErrorMessage = "COUNTRYNAME cannot be longer than 255 characters")]
    		public string  COUNTRYNAME { get; set; }
    
    		    
    		/// <summary>
    		/// Country ID
    		/// </summary>        
    	//    [DisplayName("Country ID")]
            [MaxLength(20, ErrorMessage = "Country ID cannot be longer than 20 characters")]
    		public string  CountryID { get; set; }
    
    		    
    		/// <summary>
    		/// PHONENUMBER
    		/// </summary>        
    	//    [DisplayName("PHONENUMBER")]
            [MaxLength(50, ErrorMessage = "PHONENUMBER cannot be longer than 50 characters")]
    		public string  PHONENUMBER { get; set; }
    
    		    
    		/// <summary>
    		/// Phone Ext
    		/// </summary>        
    	//    [DisplayName("Phone Ext")]
            [MaxLength(10, ErrorMessage = "Phone Ext cannot be longer than 10 characters")]
    		public string  PhoneExt { get; set; }
    
    		    
    		/// <summary>
    		/// FAXNUMBER
    		/// </summary>        
    	//    [DisplayName("FAXNUMBER")]
            [MaxLength(50, ErrorMessage = "FAXNUMBER cannot be longer than 50 characters")]
    		public string  FAXNUMBER { get; set; }
    
    		    
    		/// <summary>
    		/// Fax Ext
    		/// </summary>        
    	//    [DisplayName("Fax Ext")]
            [MaxLength(10, ErrorMessage = "Fax Ext cannot be longer than 10 characters")]
    		public string  FaxExt { get; set; }
    
    		    
    		/// <summary>
    		/// WEBADDRESS1
    		/// </summary>        
    	//    [DisplayName("WEBADDRESS1")]
            [MaxLength(255, ErrorMessage = "WEBADDRESS1 cannot be longer than 255 characters")]
    		public string  WEBADDRESS1 { get; set; }
    
    		    
    		/// <summary>
    		/// WEBADDRESS2
    		/// </summary>        
    	//    [DisplayName("WEBADDRESS2")]
            [MaxLength(255, ErrorMessage = "WEBADDRESS2 cannot be longer than 255 characters")]
    		public string  WEBADDRESS2 { get; set; }
    
    		    
    		/// <summary>
    		/// DUNSNUMBER
    		/// </summary>        
    	//    [DisplayName("DUNSNUMBER")]
            [MaxLength(50, ErrorMessage = "DUNSNUMBER cannot be longer than 50 characters")]
    		public string  DUNSNUMBER { get; set; }
    
    		    
    		/// <summary>
    		/// PARENTDUNS
    		/// </summary>        
    	//    [DisplayName("PARENTDUNS")]
            [MaxLength(255, ErrorMessage = "PARENTDUNS cannot be longer than 255 characters")]
    		public string  PARENTDUNS { get; set; }
    
    		    
    		/// <summary>
    		/// TOPPARENTDUNS
    		/// </summary>        
    	//    [DisplayName("TOPPARENTDUNS")]
            [MaxLength(255, ErrorMessage = "TOPPARENTDUNS cannot be longer than 255 characters")]
    		public string  TOPPARENTDUNS { get; set; }
    
    		    
    		/// <summary>
    		/// SALES
    		/// </summary>        
    	//    [DisplayName("SALES")]
            [MaxLength(50, ErrorMessage = "SALES cannot be longer than 50 characters")]
    		public string  SALES { get; set; }
    
    		    
    		/// <summary>
    		/// FOREIGNTRADE
    		/// </summary>        
    	//    [DisplayName("FOREIGNTRADE")]
            [MaxLength(255, ErrorMessage = "FOREIGNTRADE cannot be longer than 255 characters")]
    		public string  FOREIGNTRADE { get; set; }
    
    		    
    		/// <summary>
    		/// STOCKTICKER
    		/// </summary>        
    	//    [DisplayName("STOCKTICKER")]
            [MaxLength(255, ErrorMessage = "STOCKTICKER cannot be longer than 255 characters")]
    		public string  STOCKTICKER { get; set; }
    
    		    
    		/// <summary>
    		/// TOTALEMPLOYEES
    		/// </summary>        
    	//    [DisplayName("TOTALEMPLOYEES")]
            [MaxLength(50, ErrorMessage = "TOTALEMPLOYEES cannot be longer than 50 characters")]
    		public string  TOTALEMPLOYEES { get; set; }
    
    		    
    		/// <summary>
    		/// HQSTATUSDESCRIPTION
    		/// </summary>        
    	//    [DisplayName("HQSTATUSDESCRIPTION")]
            [MaxLength(255, ErrorMessage = "HQSTATUSDESCRIPTION cannot be longer than 255 characters")]
    		public string  HQSTATUSDESCRIPTION { get; set; }
    
    		    
    		/// <summary>
    		/// OWNERSHIPDESCRIPTION
    		/// </summary>        
    	//    [DisplayName("OWNERSHIPDESCRIPTION")]
            [MaxLength(255, ErrorMessage = "OWNERSHIPDESCRIPTION cannot be longer than 255 characters")]
    		public string  OWNERSHIPDESCRIPTION { get; set; }
    
    		    
    		/// <summary>
    		/// YEARESTABLISHED
    		/// </summary>        
    	//    [DisplayName("YEARESTABLISHED")]
            [MaxLength(50, ErrorMessage = "YEARESTABLISHED cannot be longer than 50 characters")]
    		public string  YEARESTABLISHED { get; set; }
    
    		    
    		/// <summary>
    		/// SALESSCORE
    		/// </summary>        
    	//    [DisplayName("SALESSCORE")]
            [MaxLength(255, ErrorMessage = "SALESSCORE cannot be longer than 255 characters")]
    		public string  SALESSCORE { get; set; }
    
    		    
    		/// <summary>
    		/// IMPORTPCNT
    		/// </summary>        
    	//    [DisplayName("IMPORTPCNT")]
            [MaxLength(50, ErrorMessage = "IMPORTPCNT cannot be longer than 50 characters")]
    		public string  IMPORTPCNT { get; set; }
    
    		    
    		/// <summary>
    		/// IMPORTVALUE
    		/// </summary>        
    	//    [DisplayName("IMPORTVALUE")]
            [MaxLength(50, ErrorMessage = "IMPORTVALUE cannot be longer than 50 characters")]
    		public string  IMPORTVALUE { get; set; }
    
    		    
    		/// <summary>
    		/// IMPORTTONNAGE
    		/// </summary>        
    	//    [DisplayName("IMPORTTONNAGE")]
            [MaxLength(50, ErrorMessage = "IMPORTTONNAGE cannot be longer than 50 characters")]
    		public string  IMPORTTONNAGE { get; set; }
    
    		    
    		/// <summary>
    		/// IMPORTTEUS
    		/// </summary>        
    	//    [DisplayName("IMPORTTEUS")]
            [MaxLength(50, ErrorMessage = "IMPORTTEUS cannot be longer than 50 characters")]
    		public string  IMPORTTEUS { get; set; }
    
    		    
    		/// <summary>
    		/// IMPORTSHIPMENTS
    		/// </summary>        
    	//    [DisplayName("IMPORTSHIPMENTS")]
            [MaxLength(50, ErrorMessage = "IMPORTSHIPMENTS cannot be longer than 50 characters")]
    		public string  IMPORTSHIPMENTS { get; set; }
    
    		    
    		/// <summary>
    		/// IMPORTCOUNTRY1
    		/// </summary>        
    	//    [DisplayName("IMPORTCOUNTRY1")]
            [MaxLength(255, ErrorMessage = "IMPORTCOUNTRY1 cannot be longer than 255 characters")]
    		public string  IMPORTCOUNTRY1 { get; set; }
    
    		    
    		/// <summary>
    		/// IMPORTCOUNTRYSHIPMENTS1
    		/// </summary>        
    	//    [DisplayName("IMPORTCOUNTRYSHIPMENTS1")]
            [MaxLength(50, ErrorMessage = "IMPORTCOUNTRYSHIPMENTS1 cannot be longer than 50 characters")]
    		public string  IMPORTCOUNTRYSHIPMENTS1 { get; set; }
    
    		    
    		/// <summary>
    		/// IMPORTCOUNTRY2
    		/// </summary>        
    	//    [DisplayName("IMPORTCOUNTRY2")]
            [MaxLength(255, ErrorMessage = "IMPORTCOUNTRY2 cannot be longer than 255 characters")]
    		public string  IMPORTCOUNTRY2 { get; set; }
    
    		    
    		/// <summary>
    		/// IMPORTCOUNTRYSHIPMENTS2
    		/// </summary>        
    	//    [DisplayName("IMPORTCOUNTRYSHIPMENTS2")]
            [MaxLength(50, ErrorMessage = "IMPORTCOUNTRYSHIPMENTS2 cannot be longer than 50 characters")]
    		public string  IMPORTCOUNTRYSHIPMENTS2 { get; set; }
    
    		    
    		/// <summary>
    		/// IMPORTCOUNTRY3
    		/// </summary>        
    	//    [DisplayName("IMPORTCOUNTRY3")]
            [MaxLength(255, ErrorMessage = "IMPORTCOUNTRY3 cannot be longer than 255 characters")]
    		public string  IMPORTCOUNTRY3 { get; set; }
    
    		    
    		/// <summary>
    		/// IMPORTCOUNTRYSHIPMENTS3
    		/// </summary>        
    	//    [DisplayName("IMPORTCOUNTRYSHIPMENTS3")]
            [MaxLength(50, ErrorMessage = "IMPORTCOUNTRYSHIPMENTS3 cannot be longer than 50 characters")]
    		public string  IMPORTCOUNTRYSHIPMENTS3 { get; set; }
    
    		    
    		/// <summary>
    		/// IMPORTCOUNTRY4
    		/// </summary>        
    	//    [DisplayName("IMPORTCOUNTRY4")]
            [MaxLength(255, ErrorMessage = "IMPORTCOUNTRY4 cannot be longer than 255 characters")]
    		public string  IMPORTCOUNTRY4 { get; set; }
    
    		    
    		/// <summary>
    		/// IMPORTCOUNTRYSHIPMENTS4
    		/// </summary>        
    	//    [DisplayName("IMPORTCOUNTRYSHIPMENTS4")]
            [MaxLength(50, ErrorMessage = "IMPORTCOUNTRYSHIPMENTS4 cannot be longer than 50 characters")]
    		public string  IMPORTCOUNTRYSHIPMENTS4 { get; set; }
    
    		    
    		/// <summary>
    		/// IMPORTCOUNTRY5
    		/// </summary>        
    	//    [DisplayName("IMPORTCOUNTRY5")]
            [MaxLength(255, ErrorMessage = "IMPORTCOUNTRY5 cannot be longer than 255 characters")]
    		public string  IMPORTCOUNTRY5 { get; set; }
    
    		    
    		/// <summary>
    		/// IMPORTCOUNTRYSHIPMENTS5
    		/// </summary>        
    	//    [DisplayName("IMPORTCOUNTRYSHIPMENTS5")]
            [MaxLength(50, ErrorMessage = "IMPORTCOUNTRYSHIPMENTS5 cannot be longer than 50 characters")]
    		public string  IMPORTCOUNTRYSHIPMENTS5 { get; set; }
    
    		    
    		/// <summary>
    		/// IMPORTPORT1
    		/// </summary>        
    	//    [DisplayName("IMPORTPORT1")]
            [MaxLength(255, ErrorMessage = "IMPORTPORT1 cannot be longer than 255 characters")]
    		public string  IMPORTPORT1 { get; set; }
    
    		    
    		/// <summary>
    		/// IMPORTPORTSHIPMENTS1
    		/// </summary>        
    	//    [DisplayName("IMPORTPORTSHIPMENTS1")]
            [MaxLength(50, ErrorMessage = "IMPORTPORTSHIPMENTS1 cannot be longer than 50 characters")]
    		public string  IMPORTPORTSHIPMENTS1 { get; set; }
    
    		    
    		/// <summary>
    		/// IMPORTPORT2
    		/// </summary>        
    	//    [DisplayName("IMPORTPORT2")]
            [MaxLength(255, ErrorMessage = "IMPORTPORT2 cannot be longer than 255 characters")]
    		public string  IMPORTPORT2 { get; set; }
    
    		    
    		/// <summary>
    		/// IMPORTPORTSHIPMENTS2
    		/// </summary>        
    	//    [DisplayName("IMPORTPORTSHIPMENTS2")]
            [MaxLength(50, ErrorMessage = "IMPORTPORTSHIPMENTS2 cannot be longer than 50 characters")]
    		public string  IMPORTPORTSHIPMENTS2 { get; set; }
    
    		    
    		/// <summary>
    		/// IMPORTPORT3
    		/// </summary>        
    	//    [DisplayName("IMPORTPORT3")]
            [MaxLength(255, ErrorMessage = "IMPORTPORT3 cannot be longer than 255 characters")]
    		public string  IMPORTPORT3 { get; set; }
    
    		    
    		/// <summary>
    		/// IMPORTPORTSHIPMENTS3
    		/// </summary>        
    	//    [DisplayName("IMPORTPORTSHIPMENTS3")]
            [MaxLength(50, ErrorMessage = "IMPORTPORTSHIPMENTS3 cannot be longer than 50 characters")]
    		public string  IMPORTPORTSHIPMENTS3 { get; set; }
    
    		    
    		/// <summary>
    		/// IMPORTPORT4
    		/// </summary>        
    	//    [DisplayName("IMPORTPORT4")]
            [MaxLength(255, ErrorMessage = "IMPORTPORT4 cannot be longer than 255 characters")]
    		public string  IMPORTPORT4 { get; set; }
    
    		    
    		/// <summary>
    		/// IMPORTPORTSHIPMENTS4
    		/// </summary>        
    	//    [DisplayName("IMPORTPORTSHIPMENTS4")]
            [MaxLength(50, ErrorMessage = "IMPORTPORTSHIPMENTS4 cannot be longer than 50 characters")]
    		public string  IMPORTPORTSHIPMENTS4 { get; set; }
    
    		    
    		/// <summary>
    		/// IMPORTPORT5
    		/// </summary>        
    	//    [DisplayName("IMPORTPORT5")]
            [MaxLength(255, ErrorMessage = "IMPORTPORT5 cannot be longer than 255 characters")]
    		public string  IMPORTPORT5 { get; set; }
    
    		    
    		/// <summary>
    		/// IMPORTPORTSHIPMENTS5
    		/// </summary>        
    	//    [DisplayName("IMPORTPORTSHIPMENTS5")]
            [MaxLength(50, ErrorMessage = "IMPORTPORTSHIPMENTS5 cannot be longer than 50 characters")]
    		public string  IMPORTPORTSHIPMENTS5 { get; set; }
    
    		    
    		/// <summary>
    		/// IMPORTCOMMODITY1
    		/// </summary>        
    	//    [DisplayName("IMPORTCOMMODITY1")]
            [MaxLength(255, ErrorMessage = "IMPORTCOMMODITY1 cannot be longer than 255 characters")]
    		public string  IMPORTCOMMODITY1 { get; set; }
    
    		    
    		/// <summary>
    		/// IMPORTCOMMODITYSHIPMENTS1
    		/// </summary>        
    	//    [DisplayName("IMPORTCOMMODITYSHIPMENTS1")]
            [MaxLength(50, ErrorMessage = "IMPORTCOMMODITYSHIPMENTS1 cannot be longer than 50 characters")]
    		public string  IMPORTCOMMODITYSHIPMENTS1 { get; set; }
    
    		    
    		/// <summary>
    		/// IMPORTCOMMODITY2
    		/// </summary>        
    	//    [DisplayName("IMPORTCOMMODITY2")]
            [MaxLength(255, ErrorMessage = "IMPORTCOMMODITY2 cannot be longer than 255 characters")]
    		public string  IMPORTCOMMODITY2 { get; set; }
    
    		    
    		/// <summary>
    		/// IMPORTCOMMODITYSHIPMENTS2
    		/// </summary>        
    	//    [DisplayName("IMPORTCOMMODITYSHIPMENTS2")]
            [MaxLength(50, ErrorMessage = "IMPORTCOMMODITYSHIPMENTS2 cannot be longer than 50 characters")]
    		public string  IMPORTCOMMODITYSHIPMENTS2 { get; set; }
    
    		    
    		/// <summary>
    		/// IMPORTCOMMODITY3
    		/// </summary>        
    	//    [DisplayName("IMPORTCOMMODITY3")]
            [MaxLength(255, ErrorMessage = "IMPORTCOMMODITY3 cannot be longer than 255 characters")]
    		public string  IMPORTCOMMODITY3 { get; set; }
    
    		    
    		/// <summary>
    		/// IMPORTCOMMODITYSHIPMENTS3
    		/// </summary>        
    	//    [DisplayName("IMPORTCOMMODITYSHIPMENTS3")]
            [MaxLength(50, ErrorMessage = "IMPORTCOMMODITYSHIPMENTS3 cannot be longer than 50 characters")]
    		public string  IMPORTCOMMODITYSHIPMENTS3 { get; set; }
    
    		    
    		/// <summary>
    		/// IMPORTCOMMODITY4
    		/// </summary>        
    	//    [DisplayName("IMPORTCOMMODITY4")]
            [MaxLength(255, ErrorMessage = "IMPORTCOMMODITY4 cannot be longer than 255 characters")]
    		public string  IMPORTCOMMODITY4 { get; set; }
    
    		    
    		/// <summary>
    		/// IMPORTCOMMODITYSHIPMENTS4
    		/// </summary>        
    	//    [DisplayName("IMPORTCOMMODITYSHIPMENTS4")]
            [MaxLength(50, ErrorMessage = "IMPORTCOMMODITYSHIPMENTS4 cannot be longer than 50 characters")]
    		public string  IMPORTCOMMODITYSHIPMENTS4 { get; set; }
    
    		    
    		/// <summary>
    		/// IMPORTCOMMODITY5
    		/// </summary>        
    	//    [DisplayName("IMPORTCOMMODITY5")]
            [MaxLength(255, ErrorMessage = "IMPORTCOMMODITY5 cannot be longer than 255 characters")]
    		public string  IMPORTCOMMODITY5 { get; set; }
    
    		    
    		/// <summary>
    		/// IMPORTCOMMODITYSHIPMENTS5
    		/// </summary>        
    	//    [DisplayName("IMPORTCOMMODITYSHIPMENTS5")]
            [MaxLength(50, ErrorMessage = "IMPORTCOMMODITYSHIPMENTS5 cannot be longer than 50 characters")]
    		public string  IMPORTCOMMODITYSHIPMENTS5 { get; set; }
    
    		    
    		/// <summary>
    		/// EXPORTPCNT
    		/// </summary>        
    	//    [DisplayName("EXPORTPCNT")]
            [MaxLength(50, ErrorMessage = "EXPORTPCNT cannot be longer than 50 characters")]
    		public string  EXPORTPCNT { get; set; }
    
    		    
    		/// <summary>
    		/// EXPORTVALUE
    		/// </summary>        
    	//    [DisplayName("EXPORTVALUE")]
            [MaxLength(50, ErrorMessage = "EXPORTVALUE cannot be longer than 50 characters")]
    		public string  EXPORTVALUE { get; set; }
    
    		    
    		/// <summary>
    		/// EXPORTTONNAGE
    		/// </summary>        
    	//    [DisplayName("EXPORTTONNAGE")]
            [MaxLength(50, ErrorMessage = "EXPORTTONNAGE cannot be longer than 50 characters")]
    		public string  EXPORTTONNAGE { get; set; }
    
    		    
    		/// <summary>
    		/// EXPORTTEUS
    		/// </summary>        
    	//    [DisplayName("EXPORTTEUS")]
            [MaxLength(50, ErrorMessage = "EXPORTTEUS cannot be longer than 50 characters")]
    		public string  EXPORTTEUS { get; set; }
    
    		    
    		/// <summary>
    		/// EXPORTSHIPMENTS
    		/// </summary>        
    	//    [DisplayName("EXPORTSHIPMENTS")]
            [MaxLength(50, ErrorMessage = "EXPORTSHIPMENTS cannot be longer than 50 characters")]
    		public string  EXPORTSHIPMENTS { get; set; }
    
    		    
    		/// <summary>
    		/// EXPORTCOUNTRY1
    		/// </summary>        
    	//    [DisplayName("EXPORTCOUNTRY1")]
            [MaxLength(255, ErrorMessage = "EXPORTCOUNTRY1 cannot be longer than 255 characters")]
    		public string  EXPORTCOUNTRY1 { get; set; }
    
    		    
    		/// <summary>
    		/// EXPORTCOUNTRYSHIPMENTS1
    		/// </summary>        
    	//    [DisplayName("EXPORTCOUNTRYSHIPMENTS1")]
            [MaxLength(50, ErrorMessage = "EXPORTCOUNTRYSHIPMENTS1 cannot be longer than 50 characters")]
    		public string  EXPORTCOUNTRYSHIPMENTS1 { get; set; }
    
    		    
    		/// <summary>
    		/// EXPORTCOUNTRY2
    		/// </summary>        
    	//    [DisplayName("EXPORTCOUNTRY2")]
            [MaxLength(255, ErrorMessage = "EXPORTCOUNTRY2 cannot be longer than 255 characters")]
    		public string  EXPORTCOUNTRY2 { get; set; }
    
    		    
    		/// <summary>
    		/// EXPORTCOUNTRYSHIPMENTS2
    		/// </summary>        
    	//    [DisplayName("EXPORTCOUNTRYSHIPMENTS2")]
            [MaxLength(50, ErrorMessage = "EXPORTCOUNTRYSHIPMENTS2 cannot be longer than 50 characters")]
    		public string  EXPORTCOUNTRYSHIPMENTS2 { get; set; }
    
    		    
    		/// <summary>
    		/// EXPORTCOUNTRY3
    		/// </summary>        
    	//    [DisplayName("EXPORTCOUNTRY3")]
            [MaxLength(255, ErrorMessage = "EXPORTCOUNTRY3 cannot be longer than 255 characters")]
    		public string  EXPORTCOUNTRY3 { get; set; }
    
    		    
    		/// <summary>
    		/// EXPORTCOUNTRYSHIPMENTS3
    		/// </summary>        
    	//    [DisplayName("EXPORTCOUNTRYSHIPMENTS3")]
            [MaxLength(50, ErrorMessage = "EXPORTCOUNTRYSHIPMENTS3 cannot be longer than 50 characters")]
    		public string  EXPORTCOUNTRYSHIPMENTS3 { get; set; }
    
    		    
    		/// <summary>
    		/// EXPORTCOUNTRY4
    		/// </summary>        
    	//    [DisplayName("EXPORTCOUNTRY4")]
            [MaxLength(255, ErrorMessage = "EXPORTCOUNTRY4 cannot be longer than 255 characters")]
    		public string  EXPORTCOUNTRY4 { get; set; }
    
    		    
    		/// <summary>
    		/// EXPORTCOUNTRYSHIPMENTS4
    		/// </summary>        
    	//    [DisplayName("EXPORTCOUNTRYSHIPMENTS4")]
            [MaxLength(50, ErrorMessage = "EXPORTCOUNTRYSHIPMENTS4 cannot be longer than 50 characters")]
    		public string  EXPORTCOUNTRYSHIPMENTS4 { get; set; }
    
    		    
    		/// <summary>
    		/// EXPORTCOUNTRY5
    		/// </summary>        
    	//    [DisplayName("EXPORTCOUNTRY5")]
            [MaxLength(255, ErrorMessage = "EXPORTCOUNTRY5 cannot be longer than 255 characters")]
    		public string  EXPORTCOUNTRY5 { get; set; }
    
    		    
    		/// <summary>
    		/// EXPORTCOUNTRYSHIPMENTS5
    		/// </summary>        
    	//    [DisplayName("EXPORTCOUNTRYSHIPMENTS5")]
            [MaxLength(50, ErrorMessage = "EXPORTCOUNTRYSHIPMENTS5 cannot be longer than 50 characters")]
    		public string  EXPORTCOUNTRYSHIPMENTS5 { get; set; }
    
    		    
    		/// <summary>
    		/// EXPORTPORT1
    		/// </summary>        
    	//    [DisplayName("EXPORTPORT1")]
            [MaxLength(255, ErrorMessage = "EXPORTPORT1 cannot be longer than 255 characters")]
    		public string  EXPORTPORT1 { get; set; }
    
    		    
    		/// <summary>
    		/// EXPORTPORTSHIPMENTS1
    		/// </summary>        
    	//    [DisplayName("EXPORTPORTSHIPMENTS1")]
            [MaxLength(50, ErrorMessage = "EXPORTPORTSHIPMENTS1 cannot be longer than 50 characters")]
    		public string  EXPORTPORTSHIPMENTS1 { get; set; }
    
    		    
    		/// <summary>
    		/// EXPORTPORT2
    		/// </summary>        
    	//    [DisplayName("EXPORTPORT2")]
            [MaxLength(255, ErrorMessage = "EXPORTPORT2 cannot be longer than 255 characters")]
    		public string  EXPORTPORT2 { get; set; }
    
    		    
    		/// <summary>
    		/// EXPORTPORTSHIPMENTS2
    		/// </summary>        
    	//    [DisplayName("EXPORTPORTSHIPMENTS2")]
            [MaxLength(50, ErrorMessage = "EXPORTPORTSHIPMENTS2 cannot be longer than 50 characters")]
    		public string  EXPORTPORTSHIPMENTS2 { get; set; }
    
    		    
    		/// <summary>
    		/// EXPORTPORT3
    		/// </summary>        
    	//    [DisplayName("EXPORTPORT3")]
            [MaxLength(255, ErrorMessage = "EXPORTPORT3 cannot be longer than 255 characters")]
    		public string  EXPORTPORT3 { get; set; }
    
    		    
    		/// <summary>
    		/// EXPORTPORTSHIPMENTS3
    		/// </summary>        
    	//    [DisplayName("EXPORTPORTSHIPMENTS3")]
            [MaxLength(50, ErrorMessage = "EXPORTPORTSHIPMENTS3 cannot be longer than 50 characters")]
    		public string  EXPORTPORTSHIPMENTS3 { get; set; }
    
    		    
    		/// <summary>
    		/// EXPORTPORT4
    		/// </summary>        
    	//    [DisplayName("EXPORTPORT4")]
            [MaxLength(255, ErrorMessage = "EXPORTPORT4 cannot be longer than 255 characters")]
    		public string  EXPORTPORT4 { get; set; }
    
    		    
    		/// <summary>
    		/// EXPORTPORTSHIPMENTS4
    		/// </summary>        
    	//    [DisplayName("EXPORTPORTSHIPMENTS4")]
            [MaxLength(50, ErrorMessage = "EXPORTPORTSHIPMENTS4 cannot be longer than 50 characters")]
    		public string  EXPORTPORTSHIPMENTS4 { get; set; }
    
    		    
    		/// <summary>
    		/// EXPORTPORT5
    		/// </summary>        
    	//    [DisplayName("EXPORTPORT5")]
            [MaxLength(255, ErrorMessage = "EXPORTPORT5 cannot be longer than 255 characters")]
    		public string  EXPORTPORT5 { get; set; }
    
    		    
    		/// <summary>
    		/// EXPORTPORTSHIPMENTS5
    		/// </summary>        
    	//    [DisplayName("EXPORTPORTSHIPMENTS5")]
            [MaxLength(50, ErrorMessage = "EXPORTPORTSHIPMENTS5 cannot be longer than 50 characters")]
    		public string  EXPORTPORTSHIPMENTS5 { get; set; }
    
    		    
    		/// <summary>
    		/// EXPORTCOMMODITY1
    		/// </summary>        
    	//    [DisplayName("EXPORTCOMMODITY1")]
            [MaxLength(255, ErrorMessage = "EXPORTCOMMODITY1 cannot be longer than 255 characters")]
    		public string  EXPORTCOMMODITY1 { get; set; }
    
    		    
    		/// <summary>
    		/// EXPORTCOMMODITYSHIPMENTS1
    		/// </summary>        
    	//    [DisplayName("EXPORTCOMMODITYSHIPMENTS1")]
            [MaxLength(50, ErrorMessage = "EXPORTCOMMODITYSHIPMENTS1 cannot be longer than 50 characters")]
    		public string  EXPORTCOMMODITYSHIPMENTS1 { get; set; }
    
    		    
    		/// <summary>
    		/// EXPORTCOMMODITY2
    		/// </summary>        
    	//    [DisplayName("EXPORTCOMMODITY2")]
            [MaxLength(255, ErrorMessage = "EXPORTCOMMODITY2 cannot be longer than 255 characters")]
    		public string  EXPORTCOMMODITY2 { get; set; }
    
    		    
    		/// <summary>
    		/// EXPORTCOMMODITYSHIPMENTS2
    		/// </summary>        
    	//    [DisplayName("EXPORTCOMMODITYSHIPMENTS2")]
            [MaxLength(50, ErrorMessage = "EXPORTCOMMODITYSHIPMENTS2 cannot be longer than 50 characters")]
    		public string  EXPORTCOMMODITYSHIPMENTS2 { get; set; }
    
    		    
    		/// <summary>
    		/// EXPORTCOMMODITY3
    		/// </summary>        
    	//    [DisplayName("EXPORTCOMMODITY3")]
            [MaxLength(255, ErrorMessage = "EXPORTCOMMODITY3 cannot be longer than 255 characters")]
    		public string  EXPORTCOMMODITY3 { get; set; }
    
    		    
    		/// <summary>
    		/// EXPORTCOMMODITYSHIPMENTS3
    		/// </summary>        
    	//    [DisplayName("EXPORTCOMMODITYSHIPMENTS3")]
            [MaxLength(50, ErrorMessage = "EXPORTCOMMODITYSHIPMENTS3 cannot be longer than 50 characters")]
    		public string  EXPORTCOMMODITYSHIPMENTS3 { get; set; }
    
    		    
    		/// <summary>
    		/// EXPORTCOMMODITY4
    		/// </summary>        
    	//    [DisplayName("EXPORTCOMMODITY4")]
            [MaxLength(255, ErrorMessage = "EXPORTCOMMODITY4 cannot be longer than 255 characters")]
    		public string  EXPORTCOMMODITY4 { get; set; }
    
    		    
    		/// <summary>
    		/// EXPORTCOMMODITYSHIPMENTS4
    		/// </summary>        
    	//    [DisplayName("EXPORTCOMMODITYSHIPMENTS4")]
            [MaxLength(50, ErrorMessage = "EXPORTCOMMODITYSHIPMENTS4 cannot be longer than 50 characters")]
    		public string  EXPORTCOMMODITYSHIPMENTS4 { get; set; }
    
    		    
    		/// <summary>
    		/// EXPORTCOMMODITY5
    		/// </summary>        
    	//    [DisplayName("EXPORTCOMMODITY5")]
            [MaxLength(255, ErrorMessage = "EXPORTCOMMODITY5 cannot be longer than 255 characters")]
    		public string  EXPORTCOMMODITY5 { get; set; }
    
    		    
    		/// <summary>
    		/// EXPORTCOMMODITYSHIPMENTS5
    		/// </summary>        
    	//    [DisplayName("EXPORTCOMMODITYSHIPMENTS5")]
            [MaxLength(50, ErrorMessage = "EXPORTCOMMODITYSHIPMENTS5 cannot be longer than 50 characters")]
    		public string  EXPORTCOMMODITYSHIPMENTS5 { get; set; }
    
    		    
    		/// <summary>
    		/// MAJORITYTRADE
    		/// </summary>        
    	//    [DisplayName("MAJORITYTRADE")]
            [MaxLength(255, ErrorMessage = "MAJORITYTRADE cannot be longer than 255 characters")]
    		public string  MAJORITYTRADE { get; set; }
    
    		    
    		/// <summary>
    		/// PRIMARYSIC
    		/// </summary>        
    	//    [DisplayName("PRIMARYSIC")]
            [MaxLength(50, ErrorMessage = "PRIMARYSIC cannot be longer than 50 characters")]
    		public string  PRIMARYSIC { get; set; }
    
    		    
    		/// <summary>
    		/// PRIMARYSICDESCRIPTION
    		/// </summary>        
    	//    [DisplayName("PRIMARYSICDESCRIPTION")]
            [MaxLength(255, ErrorMessage = "PRIMARYSICDESCRIPTION cannot be longer than 255 characters")]
    		public string  PRIMARYSICDESCRIPTION { get; set; }
    
    		    
    		/// <summary>
    		/// SIC2
    		/// </summary>        
    	//    [DisplayName("SIC2")]
            [MaxLength(255, ErrorMessage = "SIC2 cannot be longer than 255 characters")]
    		public string  SIC2 { get; set; }
    
    		    
    		/// <summary>
    		/// SIC2 DESCRIPTION
    		/// </summary>        
    	//    [DisplayName("SIC2 DESCRIPTION")]
            [MaxLength(255, ErrorMessage = "SIC2 DESCRIPTION cannot be longer than 255 characters")]
    		public string  SIC2DESCRIPTION { get; set; }
    
    		    
    		/// <summary>
    		/// SIC3
    		/// </summary>        
    	//    [DisplayName("SIC3")]
            [MaxLength(255, ErrorMessage = "SIC3 cannot be longer than 255 characters")]
    		public string  SIC3 { get; set; }
    
    		    
    		/// <summary>
    		/// SIC3 DESCRIPTION
    		/// </summary>        
    	//    [DisplayName("SIC3 DESCRIPTION")]
            [MaxLength(255, ErrorMessage = "SIC3 DESCRIPTION cannot be longer than 255 characters")]
    		public string  SIC3DESCRIPTION { get; set; }
    
    		    
    		/// <summary>
    		/// PRIMARYNAICS
    		/// </summary>        
    	//    [DisplayName("PRIMARYNAICS")]
            [MaxLength(255, ErrorMessage = "PRIMARYNAICS cannot be longer than 255 characters")]
    		public string  PRIMARYNAICS { get; set; }
    
    		    
    		/// <summary>
    		/// PRIMARYNAICSDESCRIPTION
    		/// </summary>        
    	//    [DisplayName("PRIMARYNAICSDESCRIPTION")]
            [MaxLength(255, ErrorMessage = "PRIMARYNAICSDESCRIPTION cannot be longer than 255 characters")]
    		public string  PRIMARYNAICSDESCRIPTION { get; set; }
    
    		    
    		/// <summary>
    		/// NAICS2
    		/// </summary>        
    	//    [DisplayName("NAICS2")]
            [MaxLength(255, ErrorMessage = "NAICS2 cannot be longer than 255 characters")]
    		public string  NAICS2 { get; set; }
    
    		    
    		/// <summary>
    		/// NAICS2 DESCRIPTION
    		/// </summary>        
    	//    [DisplayName("NAICS2 DESCRIPTION")]
            [MaxLength(255, ErrorMessage = "NAICS2 DESCRIPTION cannot be longer than 255 characters")]
    		public string  NAICS2DESCRIPTION { get; set; }
    
    		    
    		/// <summary>
    		/// NAICS3
    		/// </summary>        
    	//    [DisplayName("NAICS3")]
            [MaxLength(255, ErrorMessage = "NAICS3 cannot be longer than 255 characters")]
    		public string  NAICS3 { get; set; }
    
    		    
    		/// <summary>
    		/// NAICS3 DESCRIPTION
    		/// </summary>        
    	//    [DisplayName("NAICS3 DESCRIPTION")]
            [MaxLength(255, ErrorMessage = "NAICS3 DESCRIPTION cannot be longer than 255 characters")]
    		public string  NAICS3DESCRIPTION { get; set; }
    
    		    
    		/// <summary>
    		/// Industry ID
    		/// </summary>        
    	//    [DisplayName("Industry ID")]
    		public Nullable<int>  IndustryID { get; set; }
    
    		    
    		/// <summary>
    		/// Industry Group ID
    		/// </summary>        
    	//    [DisplayName("Industry Group ID")]
    		public Nullable<int>  IndustryGroupID { get; set; }
    
    		    
    		/// <summary>
    		/// Product Line
    		/// </summary>        
    	//    [DisplayName("Product Line")]
            [MaxLength(255, ErrorMessage = "Product Line cannot be longer than 255 characters")]
    		public string  ProductLine { get; set; }
    
    		    
    		/// <summary>
    		/// SAFiles ID
    		/// </summary>        
    	//    [DisplayName("SAFiles ID")]
    		public Nullable<int>  SAFilesID { get; set; }
    
    		    
    		/// <summary>
    		/// Lead HQID
    		/// </summary>        
    	//    [DisplayName("Lead HQID")]
    		public Nullable<int>  LeadHQID { get; set; }
    
    		    
    		/// <summary>
    		/// Status
    		/// </summary>        
    	//    [DisplayName("Status")]
            [Required(ErrorMessage = "Status is required")]
            [MaxLength(10, ErrorMessage = "Status cannot be longer than 10 characters")]
    		public string  Status { get; set; }
    
    		    
    		/// <summary>
    		/// Station ID
    		/// </summary>        
    	//    [DisplayName("Station ID")]
            [Required(ErrorMessage = "Station ID is required")]
            [MaxLength(10, ErrorMessage = "Station ID cannot be longer than 10 characters")]
    		public string  StationID { get; set; }
    
    		    
    		/// <summary>
    		/// Owner ID
    		/// </summary>        
    	//    [DisplayName("Owner ID")]
            [MaxLength(6, ErrorMessage = "Owner ID cannot be longer than 6 characters")]
    		public string  OwnerID { get; set; }
    
    		    
    		/// <summary>
    		/// Created By
    		/// </summary>        
    	//    [DisplayName("Created By")]
            [MaxLength(6, ErrorMessage = "Created By cannot be longer than 6 characters")]
    		public string  CreatedBy { get; set; }
    
    		    
    		/// <summary>
    		/// Created Date
    		/// </summary>        
    	//    [DisplayName("Created Date")]
            [Required(ErrorMessage = "Created Date is required")]
    		public System.DateTime  CreatedDate { get; set; }
    
    		    
    		/// <summary>
    		/// Updated By
    		/// </summary>        
    	//    [DisplayName("Updated By")]
            [MaxLength(6, ErrorMessage = "Updated By cannot be longer than 6 characters")]
    		public string  UpdatedBy { get; set; }
    
    		    
    		/// <summary>
    		/// Updated Date
    		/// </summary>        
    	//    [DisplayName("Updated Date")]
    		public Nullable<System.DateTime>  UpdatedDate { get; set; }
    
    		    
    		/// <summary>
    		/// Assigned By
    		/// </summary>        
    	//    [DisplayName("Assigned By")]
            [MaxLength(6, ErrorMessage = "Assigned By cannot be longer than 6 characters")]
    		public string  AssignedBy { get; set; }
    
    		    
    		/// <summary>
    		/// Assigned Date
    		/// </summary>        
    	//    [DisplayName("Assigned Date")]
    		public Nullable<System.DateTime>  AssignedDate { get; set; }
    
    		    
    	}
    //}
    
}
