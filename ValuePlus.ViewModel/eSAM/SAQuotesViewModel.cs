using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ValuePlus.ViewModel;



namespace ValuePlus.ViewModel.eSAM
{
    
    /// <summary>
    /// SAQuotes class
    /// </summary>
    //[MetadataType(typeof(SAQuotesViewModel))]
    //public  partial class SAQuotes
    //{
    
    	/// <summary>
    	/// SAQuotes Metadata class
    	/// </summary>
    	public   class SAQuotesViewModel:ViewModelBase
    	{
    		    
    		/// <summary>
    		/// ID
    		/// </summary>        
    	//    [DisplayName("ID")]
            [Required(ErrorMessage = "ID is required")]
    		public int  ID { get; set; }
    
    		    
    		/// <summary>
    		/// LID
    		/// </summary>        
    	//    [DisplayName("LID")]
    		public Nullable<int>  LID { get; set; }
    
    		    
    		/// <summary>
    		/// PID
    		/// </summary>        
    	//    [DisplayName("PID")]
    		public Nullable<int>  PID { get; set; }
    
    		    
    		/// <summary>
    		/// Quote No
    		/// </summary>        
    	//    [DisplayName("Quote No")]
            [MaxLength(15, ErrorMessage = "Quote No cannot be longer than 15 characters")]
    		public string  QuoteNo { get; set; }
    
    		    
    		/// <summary>
    		/// Status
    		/// </summary>        
    	//    [DisplayName("Status")]
            [MaxLength(10, ErrorMessage = "Status cannot be longer than 10 characters")]
    		public string  Status { get; set; }
    
    		    
    		/// <summary>
    		/// Attention To
    		/// </summary>        
    	//    [DisplayName("Attention To")]
            [MaxLength(50, ErrorMessage = "Attention To cannot be longer than 50 characters")]
    		public string  AttentionTo { get; set; }
    
    		    
    		/// <summary>
    		/// Effective Date
    		/// </summary>        
    	//    [DisplayName("Effective Date")]
    		public Nullable<System.DateTime>  EffectiveDate { get; set; }
    
    		    
    		/// <summary>
    		/// Expired Date
    		/// </summary>        
    	//    [DisplayName("Expired Date")]
    		public Nullable<System.DateTime>  ExpiredDate { get; set; }
    
    		    
    		/// <summary>
    		/// Trade Term ID
    		/// </summary>        
    	//    [DisplayName("Trade Term ID")]
    		public Nullable<int>  TradeTermID { get; set; }
    
    		    
    		/// <summary>
    		/// Credit Term ID
    		/// </summary>        
    	//    [DisplayName("Credit Term ID")]
    		public Nullable<int>  CreditTermID { get; set; }
    
    		    
    		/// <summary>
    		/// Credit Limit Amount
    		/// </summary>        
    	//    [DisplayName("Credit Limit Amount")]
            [MaxLength(20, ErrorMessage = "Credit Limit Amount cannot be longer than 20 characters")]
    		public string  CreditLimitAmount { get; set; }
    
    		    
    		/// <summary>
    		/// Credit Limit Curr ID
    		/// </summary>        
    	//    [DisplayName("Credit Limit Curr ID")]
    		public Nullable<int>  CreditLimitCurrID { get; set; }
    
    		    
    		/// <summary>
    		/// Remark
    		/// </summary>        
    	//    [DisplayName("Remark")]
    		public string  Remark { get; set; }
    
    		    
    		/// <summary>
    		/// Book Mark
    		/// </summary>        
    	//    [DisplayName("Book Mark")]
            [MaxLength(10, ErrorMessage = "Book Mark cannot be longer than 10 characters")]
    		public string  BookMark { get; set; }
    
    		    
    		/// <summary>
    		/// FRHeader Body
    		/// </summary>        
    	//    [DisplayName("FRHeader Body")]
            [MaxLength(100, ErrorMessage = "FRHeader Body cannot be longer than 100 characters")]
    		public string  FRHeaderBody { get; set; }
    
    		    
    		/// <summary>
    		/// Approval Info
    		/// </summary>        
    	//    [DisplayName("Approval Info")]
            [MaxLength(10, ErrorMessage = "Approval Info cannot be longer than 10 characters")]
    		public string  ApprovalInfo { get; set; }
    
    		    
    		/// <summary>
    		/// Accept Info
    		/// </summary>        
    	//    [DisplayName("Accept Info")]
            [MaxLength(10, ErrorMessage = "Accept Info cannot be longer than 10 characters")]
    		public string  AcceptInfo { get; set; }
    
    		    
    		/// <summary>
    		/// Created User ID
    		/// </summary>        
    	//    [DisplayName("Created User ID")]
            [MaxLength(10, ErrorMessage = "Created User ID cannot be longer than 10 characters")]
    		public string  CreatedUserID { get; set; }
    
    		    
    		/// <summary>
    		/// Created Date
    		/// </summary>        
    	//    [DisplayName("Created Date")]
    		public Nullable<System.DateTime>  CreatedDate { get; set; }
    
    		    
    		/// <summary>
    		/// Updated User ID
    		/// </summary>        
    	//    [DisplayName("Updated User ID")]
            [MaxLength(10, ErrorMessage = "Updated User ID cannot be longer than 10 characters")]
    		public string  UpdatedUserID { get; set; }
    
    		    
    		/// <summary>
    		/// Updated Date
    		/// </summary>        
    	//    [DisplayName("Updated Date")]
    		public Nullable<System.DateTime>  UpdatedDate { get; set; }
    
    		    
    		/// <summary>
    		/// Approval By
    		/// </summary>        
    	//    [DisplayName("Approval By")]
            [MaxLength(50, ErrorMessage = "Approval By cannot be longer than 50 characters")]
    		public string  ApprovalBy { get; set; }
    
    		    
    		/// <summary>
    		/// Approval Date
    		/// </summary>        
    	//    [DisplayName("Approval Date")]
    		public Nullable<System.DateTime>  ApprovalDate { get; set; }
    
    		    
    		/// <summary>
    		/// Accept By
    		/// </summary>        
    	//    [DisplayName("Accept By")]
            [MaxLength(50, ErrorMessage = "Accept By cannot be longer than 50 characters")]
    		public string  AcceptBy { get; set; }
    
    		    
    		/// <summary>
    		/// Accept Date
    		/// </summary>        
    	//    [DisplayName("Accept Date")]
    		public Nullable<System.DateTime>  AcceptDate { get; set; }
    
    		    
    		/// <summary>
    		/// s Type
    		/// </summary>        
    	//    [DisplayName("s Type")]
            [MaxLength(10, ErrorMessage = "s Type cannot be longer than 10 characters")]
    		public string  sType { get; set; }
    
    		    
    		/// <summary>
    		/// One PWD
    		/// </summary>        
    	//    [DisplayName("One PWD")]
            [MaxLength(10, ErrorMessage = "One PWD cannot be longer than 10 characters")]
    		public string  OnePWD { get; set; }
    
    		    
    		/// <summary>
    		/// Title1
    		/// </summary>        
    	//    [DisplayName("Title1")]
            [MaxLength(10, ErrorMessage = "Title1 cannot be longer than 10 characters")]
    		public string  Title1 { get; set; }
    
    		    
    		/// <summary>
    		/// Title2
    		/// </summary>        
    	//    [DisplayName("Title2")]
            [MaxLength(10, ErrorMessage = "Title2 cannot be longer than 10 characters")]
    		public string  Title2 { get; set; }
    
    		    
    		/// <summary>
    		/// Title3
    		/// </summary>        
    	//    [DisplayName("Title3")]
            [MaxLength(10, ErrorMessage = "Title3 cannot be longer than 10 characters")]
    		public string  Title3 { get; set; }
    
    		    
    		/// <summary>
    		/// Title4
    		/// </summary>        
    	//    [DisplayName("Title4")]
            [MaxLength(10, ErrorMessage = "Title4 cannot be longer than 10 characters")]
    		public string  Title4 { get; set; }
    
    		    
    		/// <summary>
    		/// Title5
    		/// </summary>        
    	//    [DisplayName("Title5")]
            [MaxLength(10, ErrorMessage = "Title5 cannot be longer than 10 characters")]
    		public string  Title5 { get; set; }
    
    		    
    		/// <summary>
    		/// Title6
    		/// </summary>        
    	//    [DisplayName("Title6")]
            [MaxLength(10, ErrorMessage = "Title6 cannot be longer than 10 characters")]
    		public string  Title6 { get; set; }
    
    		    
    		/// <summary>
    		/// Title7
    		/// </summary>        
    	//    [DisplayName("Title7")]
            [MaxLength(10, ErrorMessage = "Title7 cannot be longer than 10 characters")]
    		public string  Title7 { get; set; }
    
    		    
    		/// <summary>
    		/// Title8
    		/// </summary>        
    	//    [DisplayName("Title8")]
            [MaxLength(10, ErrorMessage = "Title8 cannot be longer than 10 characters")]
    		public string  Title8 { get; set; }
    
    		    
    		/// <summary>
    		/// Title9
    		/// </summary>        
    	//    [DisplayName("Title9")]
            [MaxLength(10, ErrorMessage = "Title9 cannot be longer than 10 characters")]
    		public string  Title9 { get; set; }
    
    		    
    		/// <summary>
    		/// Title10
    		/// </summary>        
    	//    [DisplayName("Title10")]
            [MaxLength(10, ErrorMessage = "Title10 cannot be longer than 10 characters")]
    		public string  Title10 { get; set; }
    
    		    
    		/// <summary>
    		/// BKDescription
    		/// </summary>        
    	//    [DisplayName("BKDescription")]
            [MaxLength(50, ErrorMessage = "BKDescription cannot be longer than 50 characters")]
    		public string  BKDescription { get; set; }
    
    		    
    		/// <summary>
    		/// Is Generate
    		/// </summary>        
    	//    [DisplayName("Is Generate")]
    		public Nullable<bool>  IsGenerate { get; set; }
    
    		    
    		/// <summary>
    		/// Project Explanation
    		/// </summary>        
    	//    [DisplayName("Project Explanation")]
            [MaxLength(255, ErrorMessage = "Project Explanation cannot be longer than 255 characters")]
    		public string  ProjectExplanation { get; set; }
    
    		    
    		/// <summary>
    		/// Is MNC
    		/// </summary>        
    	//    [DisplayName("Is MNC")]
    		public Nullable<bool>  IsMNC { get; set; }
    
    		    
    		/// <summary>
    		/// Prevail Rate
    		/// </summary>        
    	//    [DisplayName("Prevail Rate")]
            [MaxLength(10, ErrorMessage = "Prevail Rate cannot be longer than 10 characters")]
    		public string  PrevailRate { get; set; }
    
    		    
    		/// <summary>
    		/// Prevail Rate Flag
    		/// </summary>        
    	//    [DisplayName("Prevail Rate Flag")]
            [MaxLength(10, ErrorMessage = "Prevail Rate Flag cannot be longer than 10 characters")]
    		public string  PrevailRateFlag { get; set; }
    
    		    
    		/// <summary>
    		/// DTSTrade Mode
    		/// </summary>        
    	//    [DisplayName("DTSTrade Mode")]
            [MaxLength(20, ErrorMessage = "DTSTrade Mode cannot be longer than 20 characters")]
    		public string  DTSTradeMode { get; set; }
    
    		    
    		/// <summary>
    		/// Vendor ID
    		/// </summary>        
    	//    [DisplayName("Vendor ID")]
    		public Nullable<int>  VendorID { get; set; }
    
    		    
    		/// <summary>
    		/// Project ID
    		/// </summary>        
    	//    [DisplayName("Project ID")]
    		public Nullable<int>  ProjectID { get; set; }
    
    		    
    		/// <summary>
    		/// Station ID
    		/// </summary>        
    	//    [DisplayName("Station ID")]
            [MaxLength(5, ErrorMessage = "Station ID cannot be longer than 5 characters")]
    		public string  StationID { get; set; }
    
    		    
    		/// <summary>
    		/// SF
    		/// </summary>        
    	//    [DisplayName("SF")]
            [Required(ErrorMessage = "SF is required")]
    		public int  SF { get; set; }
    
    		    
    		/// <summary>
    		/// Payable Day
    		/// </summary>        
    	//    [DisplayName("Payable Day")]
            [Required(ErrorMessage = "Payable Day is required")]
    		public int  PayableDay { get; set; }
    
    		    
    		/// <summary>
    		/// NRA
    		/// </summary>        
    	//    [DisplayName("NRA")]
            [Required(ErrorMessage = "NRA is required")]
            [MaxLength(1, ErrorMessage = "NRA cannot be longer than 1 characters")]
    		public string  NRA { get; set; }
    
    		    
    		/// <summary>
    		/// Is Accessor
    		/// </summary>        
    	//    [DisplayName("Is Accessor")]
    		public Nullable<bool>  IsAccessor { get; set; }
    
    		    
    		/// <summary>
    		/// Move Type
    		/// </summary>        
    	//    [DisplayName("Move Type")]
            [MaxLength(10, ErrorMessage = "Move Type cannot be longer than 10 characters")]
    		public string  MoveType { get; set; }
    
    		    
    	}
    //}
    
}
