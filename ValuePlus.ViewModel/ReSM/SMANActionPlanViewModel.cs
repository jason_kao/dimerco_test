using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ValuePlus.ViewModel;



namespace ValuePlus.ViewModel.ReSM
{
    
    /// <summary>
    /// SMANActionPlan class
    /// </summary>
    //[MetadataType(typeof(SMANActionPlanViewModel))]
    //public  partial class SMANActionPlan
    //{
    
    	/// <summary>
    	/// SMANActionPlan Metadata class
    	/// </summary>
    	public   class SMANActionPlanViewModel:ViewModelBase
    	{
    		    
    		/// <summary>
    		/// HQID
    		/// </summary>        
    	//    [DisplayName("HQID")]
            [Required(ErrorMessage = "HQID is required")]
    		public int  HQID { get; set; }
    
    		    
    		/// <summary>
    		/// ANAction Code
    		/// </summary>        
    	//    [DisplayName("ANAction Code")]
            [Required(ErrorMessage = "ANAction Code is required")]
            [MaxLength(2, ErrorMessage = "ANAction Code cannot be longer than 2 characters")]
    		public string  ANActionCode { get; set; }
    
    		    
    		/// <summary>
    		/// ANAction Description
    		/// </summary>        
    	//    [DisplayName("ANAction Description")]
            [Required(ErrorMessage = "ANAction Description is required")]
            [MaxLength(30, ErrorMessage = "ANAction Description cannot be longer than 30 characters")]
    		public string  ANActionDescription { get; set; }
    
    		    
    		/// <summary>
    		/// Created By
    		/// </summary>        
    	//    [DisplayName("Created By")]
            [Required(ErrorMessage = "Created By is required")]
            [MaxLength(6, ErrorMessage = "Created By cannot be longer than 6 characters")]
    		public string  CreatedBy { get; set; }
    
    		    
    		/// <summary>
    		/// Created Date
    		/// </summary>        
    	//    [DisplayName("Created Date")]
            [Required(ErrorMessage = "Created Date is required")]
    		public System.DateTime  CreatedDate { get; set; }
    
    		    
    		/// <summary>
    		/// Updated By
    		/// </summary>        
    	//    [DisplayName("Updated By")]
            [MaxLength(6, ErrorMessage = "Updated By cannot be longer than 6 characters")]
    		public string  UpdatedBy { get; set; }
    
    		    
    		/// <summary>
    		/// Updated Date
    		/// </summary>        
    	//    [DisplayName("Updated Date")]
    		public Nullable<System.DateTime>  UpdatedDate { get; set; }
    
    		    
    	}
    //}
    
}
