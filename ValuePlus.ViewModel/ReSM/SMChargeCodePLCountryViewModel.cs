using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ValuePlus.ViewModel;



namespace ValuePlus.ViewModel.ReSM
{
    
    /// <summary>
    /// SMChargeCodePLCountry class
    /// </summary>
    //[MetadataType(typeof(SMChargeCodePLCountryViewModel))]
    //public  partial class SMChargeCodePLCountry
    //{
    
    	/// <summary>
    	/// SMChargeCodePLCountry Metadata class
    	/// </summary>
    	public   class SMChargeCodePLCountryViewModel:ViewModelBase
    	{
    		    
    		/// <summary>
    		/// Charge Code ID
    		/// </summary>        
    	//    [DisplayName("Charge Code ID")]
            [Required(ErrorMessage = "Charge Code ID is required")]
    		public int  ChargeCodeID { get; set; }
    
    		    
    		/// <summary>
    		/// Product Line ID
    		/// </summary>        
    	//    [DisplayName("Product Line ID")]
            [Required(ErrorMessage = "Product Line ID is required")]
    		public int  ProductLineID { get; set; }
    
    		    
    		/// <summary>
    		/// Country ID
    		/// </summary>        
    	//    [DisplayName("Country ID")]
            [Required(ErrorMessage = "Country ID is required")]
    		public int  CountryID { get; set; }
    
    		    
    		/// <summary>
    		/// VATPP
    		/// </summary>        
    	//    [DisplayName("VATPP")]
            [MaxLength(2, ErrorMessage = "VATPP cannot be longer than 2 characters")]
    		public string  VATPP { get; set; }
    
    		    
    		/// <summary>
    		/// VATCC
    		/// </summary>        
    	//    [DisplayName("VATCC")]
            [MaxLength(2, ErrorMessage = "VATCC cannot be longer than 2 characters")]
    		public string  VATCC { get; set; }
    
    		    
    		/// <summary>
    		/// VATCost
    		/// </summary>        
    	//    [DisplayName("VATCost")]
            [MaxLength(2, ErrorMessage = "VATCost cannot be longer than 2 characters")]
    		public string  VATCost { get; set; }
    
    		    
    		/// <summary>
    		/// Status
    		/// </summary>        
    	//    [DisplayName("Status")]
            [Required(ErrorMessage = "Status is required")]
            [MaxLength(10, ErrorMessage = "Status cannot be longer than 10 characters")]
    		public string  Status { get; set; }
    
    		    
    		/// <summary>
    		/// Created By
    		/// </summary>        
    	//    [DisplayName("Created By")]
            [MaxLength(6, ErrorMessage = "Created By cannot be longer than 6 characters")]
    		public string  CreatedBy { get; set; }
    
    		    
    		/// <summary>
    		/// Created Date
    		/// </summary>        
    	//    [DisplayName("Created Date")]
            [Required(ErrorMessage = "Created Date is required")]
    		public System.DateTime  CreatedDate { get; set; }
    
    		    
    		/// <summary>
    		/// Updated By
    		/// </summary>        
    	//    [DisplayName("Updated By")]
            [MaxLength(6, ErrorMessage = "Updated By cannot be longer than 6 characters")]
    		public string  UpdatedBy { get; set; }
    
    		    
    		/// <summary>
    		/// Updated Date
    		/// </summary>        
    	//    [DisplayName("Updated Date")]
    		public Nullable<System.DateTime>  UpdatedDate { get; set; }
    
    		    
    		/// <summary>
    		/// Version
    		/// </summary>        
    	//    [DisplayName("Version")]
            [MaxLength(8, ErrorMessage = "Version cannot be longer than 8 characters")]
    		public byte[]  Version { get; set; }
    
    		    
    	}
    //}
    
}
