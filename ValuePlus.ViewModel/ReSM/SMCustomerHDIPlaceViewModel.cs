using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ValuePlus.ViewModel;



namespace ValuePlus.ViewModel.ReSM
{
    
    /// <summary>
    /// SMCustomerHDIPlace class
    /// </summary>
    //[MetadataType(typeof(SMCustomerHDIPlaceViewModel))]
    //public  partial class SMCustomerHDIPlace
    //{
    
    	/// <summary>
    	/// SMCustomerHDIPlace Metadata class
    	/// </summary>
    	public   class SMCustomerHDIPlaceViewModel:ViewModelBase
    	{
    		    
    		/// <summary>
    		/// HQID
    		/// </summary>        
    	//    [DisplayName("HQID")]
            [Required(ErrorMessage = "HQID is required")]
    		public int  HQID { get; set; }
    
    		    
    		/// <summary>
    		/// HDIID
    		/// </summary>        
    	//    [DisplayName("HDIID")]
            [Required(ErrorMessage = "HDIID is required")]
    		public int  HDIID { get; set; }
    
    		    
    		/// <summary>
    		/// From Place Type
    		/// </summary>        
    	//    [DisplayName("From Place Type")]
            [MaxLength(50, ErrorMessage = "From Place Type cannot be longer than 50 characters")]
    		public string  FromPlaceType { get; set; }
    
    		    
    		/// <summary>
    		/// From Place ID
    		/// </summary>        
    	//    [DisplayName("From Place ID")]
    		public Nullable<int>  FromPlaceID { get; set; }
    
    		    
    		/// <summary>
    		/// To Place Type
    		/// </summary>        
    	//    [DisplayName("To Place Type")]
            [MaxLength(50, ErrorMessage = "To Place Type cannot be longer than 50 characters")]
    		public string  ToPlaceType { get; set; }
    
    		    
    		/// <summary>
    		/// To Place ID
    		/// </summary>        
    	//    [DisplayName("To Place ID")]
    		public Nullable<int>  ToPlaceID { get; set; }
    
    		    
    		/// <summary>
    		/// Created By
    		/// </summary>        
    	//    [DisplayName("Created By")]
            [MaxLength(50, ErrorMessage = "Created By cannot be longer than 50 characters")]
    		public string  CreatedBy { get; set; }
    
    		    
    		/// <summary>
    		/// Created Date
    		/// </summary>        
    	//    [DisplayName("Created Date")]
    		public Nullable<System.DateTime>  CreatedDate { get; set; }
    
    		    
    		/// <summary>
    		/// Updated By
    		/// </summary>        
    	//    [DisplayName("Updated By")]
            [MaxLength(50, ErrorMessage = "Updated By cannot be longer than 50 characters")]
    		public string  UpdatedBy { get; set; }
    
    		    
    		/// <summary>
    		/// Updated Date
    		/// </summary>        
    	//    [DisplayName("Updated Date")]
    		public Nullable<System.DateTime>  UpdatedDate { get; set; }
    
    		    
    		/// <summary>
    		/// Flag
    		/// </summary>        
    	//    [DisplayName("Flag")]
    		public Nullable<bool>  Flag { get; set; }
    
    		    
    	}
    //}
    
}
