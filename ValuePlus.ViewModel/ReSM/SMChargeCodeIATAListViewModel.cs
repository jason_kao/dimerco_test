using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ValuePlus.ViewModel;



namespace ValuePlus.ViewModel.ReSM
{
    
    /// <summary>
    /// SMChargeCodeIATAList class
    /// </summary>
    //[MetadataType(typeof(SMChargeCodeIATAListViewModel))]
    //public  partial class SMChargeCodeIATAList
    //{
    
    	/// <summary>
    	/// SMChargeCodeIATAList Metadata class
    	/// </summary>
    	public   class SMChargeCodeIATAListViewModel:ViewModelBase
    	{
    		    
    		/// <summary>
    		/// HQID
    		/// </summary>        
    	//    [DisplayName("HQID")]
            [Required(ErrorMessage = "HQID is required")]
    		public int  HQID { get; set; }
    
    		    
    		/// <summary>
    		/// IATACharge Code
    		/// </summary>        
    	//    [DisplayName("IATACharge Code")]
            [MaxLength(5, ErrorMessage = "IATACharge Code cannot be longer than 5 characters")]
    		public string  IATAChargeCode { get; set; }
    
    		    
    		/// <summary>
    		/// IATACharge Code Desc
    		/// </summary>        
    	//    [DisplayName("IATACharge Code Desc")]
            [MaxLength(500, ErrorMessage = "IATACharge Code Desc cannot be longer than 500 characters")]
    		public string  IATAChargeCodeDesc { get; set; }
    
    		    
    		/// <summary>
    		/// Ref
    		/// </summary>        
    	//    [DisplayName("Ref")]
            [MaxLength(50, ErrorMessage = "Ref cannot be longer than 50 characters")]
    		public string  Ref { get; set; }
    
    		    
    	}
    //}
    
}
