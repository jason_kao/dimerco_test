using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ValuePlus.ViewModel;



namespace ValuePlus.ViewModel.ReSM
{
    
    /// <summary>
    /// SMLELevel2 class
    /// </summary>
    //[MetadataType(typeof(SMLELevel2ViewModel))]
    //public  partial class SMLELevel2
    //{
    
    	/// <summary>
    	/// SMLELevel2 Metadata class
    	/// </summary>
    	public   class SMLELevel2ViewModel:ViewModelBase
    	{
    		    
    		/// <summary>
    		/// HQID
    		/// </summary>        
    	//    [DisplayName("HQID")]
            [Required(ErrorMessage = "HQID is required")]
    		public int  HQID { get; set; }
    
    		    
    		/// <summary>
    		/// Level2 Code
    		/// </summary>        
    	//    [DisplayName("Level2 Code")]
            [Required(ErrorMessage = "Level2 Code is required")]
            [MaxLength(10, ErrorMessage = "Level2 Code cannot be longer than 10 characters")]
    		public string  Level2Code { get; set; }
    
    		    
    		/// <summary>
    		/// Level1 ID
    		/// </summary>        
    	//    [DisplayName("Level1 ID")]
            [Required(ErrorMessage = "Level1 ID is required")]
    		public int  Level1ID { get; set; }
    
    		    
    		/// <summary>
    		/// Level2 Name
    		/// </summary>        
    	//    [DisplayName("Level2 Name")]
            [MaxLength(255, ErrorMessage = "Level2 Name cannot be longer than 255 characters")]
    		public string  Level2Name { get; set; }
    
    		    
    		/// <summary>
    		/// Status
    		/// </summary>        
    	//    [DisplayName("Status")]
            [Required(ErrorMessage = "Status is required")]
            [MaxLength(10, ErrorMessage = "Status cannot be longer than 10 characters")]
    		public string  Status { get; set; }
    
    		    
    		/// <summary>
    		/// Created By
    		/// </summary>        
    	//    [DisplayName("Created By")]
            [MaxLength(6, ErrorMessage = "Created By cannot be longer than 6 characters")]
    		public string  CreatedBy { get; set; }
    
    		    
    		/// <summary>
    		/// Created Date
    		/// </summary>        
    	//    [DisplayName("Created Date")]
            [Required(ErrorMessage = "Created Date is required")]
    		public System.DateTime  CreatedDate { get; set; }
    
    		    
    		/// <summary>
    		/// Updated By
    		/// </summary>        
    	//    [DisplayName("Updated By")]
            [MaxLength(6, ErrorMessage = "Updated By cannot be longer than 6 characters")]
    		public string  UpdatedBy { get; set; }
    
    		    
    		/// <summary>
    		/// Updated Date
    		/// </summary>        
    	//    [DisplayName("Updated Date")]
    		public Nullable<System.DateTime>  UpdatedDate { get; set; }
    
    		    
    		/// <summary>
    		/// Version
    		/// </summary>        
    	//    [DisplayName("Version")]
            [Required(ErrorMessage = "Version is required")]
            [MaxLength(8, ErrorMessage = "Version cannot be longer than 8 characters")]
    		public byte[]  Version { get; set; }
    
    		    
    	}
    //}
    
}
