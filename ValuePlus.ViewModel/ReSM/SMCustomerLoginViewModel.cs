using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ValuePlus.ViewModel;



namespace ValuePlus.ViewModel.ReSM
{
    
    /// <summary>
    /// SMCustomerLogin class
    /// </summary>
    //[MetadataType(typeof(SMCustomerLoginViewModel))]
    //public  partial class SMCustomerLogin
    //{
    
    	/// <summary>
    	/// SMCustomerLogin Metadata class
    	/// </summary>
    	public   class SMCustomerLoginViewModel:ViewModelBase
    	{
    		    
    		/// <summary>
    		/// HQID
    		/// </summary>        
    	//    [DisplayName("HQID")]
            [Required(ErrorMessage = "HQID is required")]
    		public int  HQID { get; set; }
    
    		    
    		/// <summary>
    		/// Map Type
    		/// </summary>        
    	//    [DisplayName("Map Type")]
            [Required(ErrorMessage = "Map Type is required")]
            [MaxLength(50, ErrorMessage = "Map Type cannot be longer than 50 characters")]
    		public string  MapType { get; set; }
    
    		    
    		/// <summary>
    		/// Map Seq
    		/// </summary>        
    	//    [DisplayName("Map Seq")]
            [Required(ErrorMessage = "Map Seq is required")]
            [MaxLength(50, ErrorMessage = "Map Seq cannot be longer than 50 characters")]
    		public string  MapSeq { get; set; }
    
    		    
    		/// <summary>
    		/// Customer ID
    		/// </summary>        
    	//    [DisplayName("Customer ID")]
            [Required(ErrorMessage = "Customer ID is required")]
    		public int  CustomerID { get; set; }
    
    		    
    		/// <summary>
    		/// User ID
    		/// </summary>        
    	//    [DisplayName("User ID")]
            [MaxLength(50, ErrorMessage = "User ID cannot be longer than 50 characters")]
    		public string  UserID { get; set; }
    
    		    
    		/// <summary>
    		/// PWD
    		/// </summary>        
    	//    [DisplayName("PWD")]
            [MaxLength(50, ErrorMessage = "PWD cannot be longer than 50 characters")]
    		public string  PWD { get; set; }
    
    		    
    		/// <summary>
    		/// e Mail
    		/// </summary>        
    	//    [DisplayName("e Mail")]
            [MaxLength(50, ErrorMessage = "e Mail cannot be longer than 50 characters")]
    		public string  eMail { get; set; }
    
    		    
    		/// <summary>
    		/// Created By
    		/// </summary>        
    	//    [DisplayName("Created By")]
            [MaxLength(10, ErrorMessage = "Created By cannot be longer than 10 characters")]
    		public string  CreatedBy { get; set; }
    
    		    
    		/// <summary>
    		/// Created Date
    		/// </summary>        
    	//    [DisplayName("Created Date")]
    		public Nullable<System.DateTime>  CreatedDate { get; set; }
    
    		    
    		/// <summary>
    		/// Updated By
    		/// </summary>        
    	//    [DisplayName("Updated By")]
            [MaxLength(10, ErrorMessage = "Updated By cannot be longer than 10 characters")]
    		public string  UpdatedBy { get; set; }
    
    		    
    		/// <summary>
    		/// Updated Date
    		/// </summary>        
    	//    [DisplayName("Updated Date")]
    		public Nullable<System.DateTime>  UpdatedDate { get; set; }
    
    		    
    	}
    //}
    
}
