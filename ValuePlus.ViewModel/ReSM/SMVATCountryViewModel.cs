using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ValuePlus.ViewModel;



namespace ValuePlus.ViewModel.ReSM
{
    
    /// <summary>
    /// SMVATCountry class
    /// </summary>
    //[MetadataType(typeof(SMVATCountryViewModel))]
    //public  partial class SMVATCountry
    //{
    
    	/// <summary>
    	/// SMVATCountry Metadata class
    	/// </summary>
    	public   class SMVATCountryViewModel:ViewModelBase
    	{
    		    
    		/// <summary>
    		/// HQID
    		/// </summary>        
    	//    [DisplayName("HQID")]
            [Required(ErrorMessage = "HQID is required")]
    		public int  HQID { get; set; }
    
    		    
    		/// <summary>
    		/// Country ID
    		/// </summary>        
    	//    [DisplayName("Country ID")]
            [Required(ErrorMessage = "Country ID is required")]
    		public int  CountryID { get; set; }
    
    		    
    		/// <summary>
    		/// VATCode
    		/// </summary>        
    	//    [DisplayName("VATCode")]
            [Required(ErrorMessage = "VATCode is required")]
            [MaxLength(2, ErrorMessage = "VATCode cannot be longer than 2 characters")]
    		public string  VATCode { get; set; }
    
    		    
    		/// <summary>
    		/// VATName
    		/// </summary>        
    	//    [DisplayName("VATName")]
            [MaxLength(255, ErrorMessage = "VATName cannot be longer than 255 characters")]
    		public string  VATName { get; set; }
    
    		    
    		/// <summary>
    		/// VATRate
    		/// </summary>        
    	//    [DisplayName("VATRate")]
    		public Nullable<double>  VATRate { get; set; }
    
    		    
    		/// <summary>
    		/// Status
    		/// </summary>        
    	//    [DisplayName("Status")]
            [Required(ErrorMessage = "Status is required")]
            [MaxLength(10, ErrorMessage = "Status cannot be longer than 10 characters")]
    		public string  Status { get; set; }
    
    		    
    		/// <summary>
    		/// Created By
    		/// </summary>        
    	//    [DisplayName("Created By")]
            [MaxLength(6, ErrorMessage = "Created By cannot be longer than 6 characters")]
    		public string  CreatedBy { get; set; }
    
    		    
    		/// <summary>
    		/// Created Date
    		/// </summary>        
    	//    [DisplayName("Created Date")]
            [Required(ErrorMessage = "Created Date is required")]
    		public System.DateTime  CreatedDate { get; set; }
    
    		    
    		/// <summary>
    		/// Updated By
    		/// </summary>        
    	//    [DisplayName("Updated By")]
            [MaxLength(6, ErrorMessage = "Updated By cannot be longer than 6 characters")]
    		public string  UpdatedBy { get; set; }
    
    		    
    		/// <summary>
    		/// Updated Date
    		/// </summary>        
    	//    [DisplayName("Updated Date")]
    		public Nullable<System.DateTime>  UpdatedDate { get; set; }
    
    		    
    		/// <summary>
    		/// Version
    		/// </summary>        
    	//    [DisplayName("Version")]
            [MaxLength(8, ErrorMessage = "Version cannot be longer than 8 characters")]
    		public byte[]  Version { get; set; }
    
    		    
    	}
    //}
    
}
