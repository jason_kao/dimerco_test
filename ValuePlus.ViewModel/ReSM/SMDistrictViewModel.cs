using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ValuePlus.ViewModel;



namespace ValuePlus.ViewModel.ReSM
{
    
    /// <summary>
    /// SMDistrict class
    /// </summary>
    //[MetadataType(typeof(SMDistrictViewModel))]
    //public  partial class SMDistrict
    //{
    
    	/// <summary>
    	/// SMDistrict Metadata class
    	/// </summary>
    	public   class SMDistrictViewModel:ViewModelBase
    	{
    		    
    		/// <summary>
    		/// HQID
    		/// </summary>        
    	//    [DisplayName("HQID")]
            [Required(ErrorMessage = "HQID is required")]
    		public int  HQID { get; set; }
    
    		    
    		/// <summary>
    		/// District Code
    		/// </summary>        
    	//    [DisplayName("District Code")]
            [Required(ErrorMessage = "District Code is required")]
            [MaxLength(20, ErrorMessage = "District Code cannot be longer than 20 characters")]
    		public string  DistrictCode { get; set; }
    
    		    
    		/// <summary>
    		/// Country ID
    		/// </summary>        
    	//    [DisplayName("Country ID")]
            [Required(ErrorMessage = "Country ID is required")]
    		public int  CountryID { get; set; }
    
    		    
    		/// <summary>
    		/// District Name
    		/// </summary>        
    	//    [DisplayName("District Name")]
            [MaxLength(255, ErrorMessage = "District Name cannot be longer than 255 characters")]
    		public string  DistrictName { get; set; }
    
    		    
    		/// <summary>
    		/// Status
    		/// </summary>        
    	//    [DisplayName("Status")]
            [Required(ErrorMessage = "Status is required")]
            [MaxLength(10, ErrorMessage = "Status cannot be longer than 10 characters")]
    		public string  Status { get; set; }
    
    		    
    		/// <summary>
    		/// Created By
    		/// </summary>        
    	//    [DisplayName("Created By")]
            [MaxLength(6, ErrorMessage = "Created By cannot be longer than 6 characters")]
    		public string  CreatedBy { get; set; }
    
    		    
    		/// <summary>
    		/// Created Date
    		/// </summary>        
    	//    [DisplayName("Created Date")]
            [Required(ErrorMessage = "Created Date is required")]
    		public System.DateTime  CreatedDate { get; set; }
    
    		    
    		/// <summary>
    		/// Updated By
    		/// </summary>        
    	//    [DisplayName("Updated By")]
            [MaxLength(6, ErrorMessage = "Updated By cannot be longer than 6 characters")]
    		public string  UpdatedBy { get; set; }
    
    		    
    		/// <summary>
    		/// Updated Date
    		/// </summary>        
    	//    [DisplayName("Updated Date")]
    		public Nullable<System.DateTime>  UpdatedDate { get; set; }
    
    		    
    		/// <summary>
    		/// Version
    		/// </summary>        
    	//    [DisplayName("Version")]
            [Required(ErrorMessage = "Version is required")]
            [MaxLength(8, ErrorMessage = "Version cannot be longer than 8 characters")]
    		public byte[]  Version { get; set; }
    
    		    
    	}
    //}
    
}
