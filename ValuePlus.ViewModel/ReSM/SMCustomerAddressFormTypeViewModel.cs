using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ValuePlus.ViewModel;



namespace ValuePlus.ViewModel.ReSM
{
    
    /// <summary>
    /// SMCustomerAddressFormType class
    /// </summary>
    //[MetadataType(typeof(SMCustomerAddressFormTypeViewModel))]
    //public  partial class SMCustomerAddressFormType
    //{
    
    	/// <summary>
    	/// SMCustomerAddressFormType Metadata class
    	/// </summary>
    	public   class SMCustomerAddressFormTypeViewModel:ViewModelBase
    	{
    		    
    		/// <summary>
    		/// HQID
    		/// </summary>        
    	//    [DisplayName("HQID")]
            [Required(ErrorMessage = "HQID is required")]
    		public int  HQID { get; set; }
    
    		    
    		/// <summary>
    		/// Form Type Code
    		/// </summary>        
    	//    [DisplayName("Form Type Code")]
            [Required(ErrorMessage = "Form Type Code is required")]
            [MaxLength(10, ErrorMessage = "Form Type Code cannot be longer than 10 characters")]
    		public string  FormTypeCode { get; set; }
    
    		    
    		/// <summary>
    		/// Form Type Name
    		/// </summary>        
    	//    [DisplayName("Form Type Name")]
            [MaxLength(50, ErrorMessage = "Form Type Name cannot be longer than 50 characters")]
    		public string  FormTypeName { get; set; }
    
    		    
    		/// <summary>
    		/// Status
    		/// </summary>        
    	//    [DisplayName("Status")]
            [Required(ErrorMessage = "Status is required")]
            [MaxLength(10, ErrorMessage = "Status cannot be longer than 10 characters")]
    		public string  Status { get; set; }
    
    		    
    		/// <summary>
    		/// Seq
    		/// </summary>        
    	//    [DisplayName("Seq")]
    		public Nullable<int>  Seq { get; set; }
    
    		    
    		/// <summary>
    		/// Show
    		/// </summary>        
    	//    [DisplayName("Show")]
    		public Nullable<bool>  Show { get; set; }
    
    		    
    	}
    //}
    
}
