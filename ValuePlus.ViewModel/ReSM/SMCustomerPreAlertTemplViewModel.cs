using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ValuePlus.ViewModel;



namespace ValuePlus.ViewModel.ReSM
{
    
    /// <summary>
    /// SMCustomerPreAlertTempl class
    /// </summary>
    //[MetadataType(typeof(SMCustomerPreAlertTemplViewModel))]
    //public  partial class SMCustomerPreAlertTempl
    //{
    
    	/// <summary>
    	/// SMCustomerPreAlertTempl Metadata class
    	/// </summary>
    	public   class SMCustomerPreAlertTemplViewModel:ViewModelBase
    	{
    		    
    		/// <summary>
    		/// ID
    		/// </summary>        
    	//    [DisplayName("ID")]
            [Required(ErrorMessage = "ID is required")]
    		public int  ID { get; set; }
    
    		    
    		/// <summary>
    		/// Customer ID
    		/// </summary>        
    	//    [DisplayName("Customer ID")]
            [Required(ErrorMessage = "Customer ID is required")]
    		public int  CustomerID { get; set; }
    
    		    
    		/// <summary>
    		/// Product Line ID
    		/// </summary>        
    	//    [DisplayName("Product Line ID")]
            [Required(ErrorMessage = "Product Line ID is required")]
    		public int  ProductLineID { get; set; }
    
    		    
    		/// <summary>
    		/// AWBType
    		/// </summary>        
    	//    [DisplayName("AWBType")]
            [MaxLength(20, ErrorMessage = "AWBType cannot be longer than 20 characters")]
    		public string  AWBType { get; set; }
    
    		    
    		/// <summary>
    		/// Form Type
    		/// </summary>        
    	//    [DisplayName("Form Type")]
    		public Nullable<int>  FormType { get; set; }
    
    		    
    		/// <summary>
    		/// Subject
    		/// </summary>        
    	//    [DisplayName("Subject")]
            [MaxLength(255, ErrorMessage = "Subject cannot be longer than 255 characters")]
    		public string  Subject { get; set; }
    
    		    
    		/// <summary>
    		/// Need Master
    		/// </summary>        
    	//    [DisplayName("Need Master")]
            [MaxLength(1, ErrorMessage = "Need Master cannot be longer than 1 characters")]
    		public string  NeedMaster { get; set; }
    
    		    
    		/// <summary>
    		/// Need House
    		/// </summary>        
    	//    [DisplayName("Need House")]
            [MaxLength(1, ErrorMessage = "Need House cannot be longer than 1 characters")]
    		public string  NeedHouse { get; set; }
    
    		    
    		/// <summary>
    		/// Need Debit Note
    		/// </summary>        
    	//    [DisplayName("Need Debit Note")]
            [MaxLength(1, ErrorMessage = "Need Debit Note cannot be longer than 1 characters")]
    		public string  NeedDebitNote { get; set; }
    
    		    
    		/// <summary>
    		/// Need Arrival Notice
    		/// </summary>        
    	//    [DisplayName("Need Arrival Notice")]
            [MaxLength(1, ErrorMessage = "Need Arrival Notice cannot be longer than 1 characters")]
    		public string  NeedArrivalNotice { get; set; }
    
    		    
    		/// <summary>
    		/// Mail Content
    		/// </summary>        
    	//    [DisplayName("Mail Content")]
    		public string  MailContent { get; set; }
    
    		    
    		/// <summary>
    		/// Status
    		/// </summary>        
    	//    [DisplayName("Status")]
            [Required(ErrorMessage = "Status is required")]
            [MaxLength(50, ErrorMessage = "Status cannot be longer than 50 characters")]
    		public string  Status { get; set; }
    
    		    
    		/// <summary>
    		/// Created By
    		/// </summary>        
    	//    [DisplayName("Created By")]
            [MaxLength(6, ErrorMessage = "Created By cannot be longer than 6 characters")]
    		public string  CreatedBy { get; set; }
    
    		    
    		/// <summary>
    		/// Created Date
    		/// </summary>        
    	//    [DisplayName("Created Date")]
            [Required(ErrorMessage = "Created Date is required")]
    		public System.DateTime  CreatedDate { get; set; }
    
    		    
    		/// <summary>
    		/// Updated By
    		/// </summary>        
    	//    [DisplayName("Updated By")]
            [MaxLength(6, ErrorMessage = "Updated By cannot be longer than 6 characters")]
    		public string  UpdatedBy { get; set; }
    
    		    
    		/// <summary>
    		/// Updated Date
    		/// </summary>        
    	//    [DisplayName("Updated Date")]
    		public Nullable<System.DateTime>  UpdatedDate { get; set; }
    
    		    
    		/// <summary>
    		/// Version
    		/// </summary>        
    	//    [DisplayName("Version")]
            [MaxLength(8, ErrorMessage = "Version cannot be longer than 8 characters")]
    		public byte[]  Version { get; set; }
    
    		    
    		/// <summary>
    		/// Need Comm Inv
    		/// </summary>        
    	//    [DisplayName("Need Comm Inv")]
            [MaxLength(1, ErrorMessage = "Need Comm Inv cannot be longer than 1 characters")]
    		public string  NeedCommInv { get; set; }
    
    		    
    		/// <summary>
    		/// Need Packing List
    		/// </summary>        
    	//    [DisplayName("Need Packing List")]
            [MaxLength(1, ErrorMessage = "Need Packing List cannot be longer than 1 characters")]
    		public string  NeedPackingList { get; set; }
    
    		    
    		/// <summary>
    		/// Send Customer
    		/// </summary>        
    	//    [DisplayName("Send Customer")]
            [MaxLength(1, ErrorMessage = "Send Customer cannot be longer than 1 characters")]
    		public string  SendCustomer { get; set; }
    
    		    
    		/// <summary>
    		/// Send Shipper
    		/// </summary>        
    	//    [DisplayName("Send Shipper")]
            [MaxLength(1, ErrorMessage = "Send Shipper cannot be longer than 1 characters")]
    		public string  SendShipper { get; set; }
    
    		    
    		/// <summary>
    		/// Send CNEE
    		/// </summary>        
    	//    [DisplayName("Send CNEE")]
            [MaxLength(1, ErrorMessage = "Send CNEE cannot be longer than 1 characters")]
    		public string  SendCNEE { get; set; }
    
    		    
    		/// <summary>
    		/// Send Notify
    		/// </summary>        
    	//    [DisplayName("Send Notify")]
            [MaxLength(1, ErrorMessage = "Send Notify cannot be longer than 1 characters")]
    		public string  SendNotify { get; set; }
    
    		    
    		/// <summary>
    		/// Send BBAgent
    		/// </summary>        
    	//    [DisplayName("Send BBAgent")]
            [MaxLength(1, ErrorMessage = "Send BBAgent cannot be longer than 1 characters")]
    		public string  SendBBAgent { get; set; }
    
    		    
    		/// <summary>
    		/// Send Thirty Party
    		/// </summary>        
    	//    [DisplayName("Send Thirty Party")]
            [MaxLength(1, ErrorMessage = "Send Thirty Party cannot be longer than 1 characters")]
    		public string  SendThirtyParty { get; set; }
    
    		    
    		/// <summary>
    		/// Send Final Import
    		/// </summary>        
    	//    [DisplayName("Send Final Import")]
            [MaxLength(1, ErrorMessage = "Send Final Import cannot be longer than 1 characters")]
    		public string  SendFinalImport { get; set; }
    
    		    
    		/// <summary>
    		/// Send Dest Agent
    		/// </summary>        
    	//    [DisplayName("Send Dest Agent")]
            [MaxLength(1, ErrorMessage = "Send Dest Agent cannot be longer than 1 characters")]
    		public string  SendDestAgent { get; set; }
    
    		    
    	}
    //}
    
}
