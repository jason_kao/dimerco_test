using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ValuePlus.ViewModel;



namespace ValuePlus.ViewModel.ReSM
{
    
    /// <summary>
    /// SMSeaPort class
    /// </summary>
    //[MetadataType(typeof(SMSeaPortViewModel))]
    //public  partial class SMSeaPort
    //{
    
    	/// <summary>
    	/// SMSeaPort Metadata class
    	/// </summary>
    	public   class SMSeaPortViewModel:ViewModelBase
    	{
    		    
    		/// <summary>
    		/// HQID
    		/// </summary>        
    	//    [DisplayName("HQID")]
            [Required(ErrorMessage = "HQID is required")]
    		public int  HQID { get; set; }
    
    		    
    		/// <summary>
    		/// Sea Port Code
    		/// </summary>        
    	//    [DisplayName("Sea Port Code")]
            [Required(ErrorMessage = "Sea Port Code is required")]
            [MaxLength(5, ErrorMessage = "Sea Port Code cannot be longer than 5 characters")]
    		public string  SeaPortCode { get; set; }
    
    		    
    		/// <summary>
    		/// Sea Port Name
    		/// </summary>        
    	//    [DisplayName("Sea Port Name")]
            [Required(ErrorMessage = "Sea Port Name is required")]
            [MaxLength(255, ErrorMessage = "Sea Port Name cannot be longer than 255 characters")]
    		public string  SeaPortName { get; set; }
    
    		    
    		/// <summary>
    		/// City ID
    		/// </summary>        
    	//    [DisplayName("City ID")]
    		public Nullable<int>  CityID { get; set; }
    
    		    
    		/// <summary>
    		/// Status
    		/// </summary>        
    	//    [DisplayName("Status")]
            [Required(ErrorMessage = "Status is required")]
            [MaxLength(10, ErrorMessage = "Status cannot be longer than 10 characters")]
    		public string  Status { get; set; }
    
    		    
    		/// <summary>
    		/// Created By
    		/// </summary>        
    	//    [DisplayName("Created By")]
            [MaxLength(6, ErrorMessage = "Created By cannot be longer than 6 characters")]
    		public string  CreatedBy { get; set; }
    
    		    
    		/// <summary>
    		/// Created Date
    		/// </summary>        
    	//    [DisplayName("Created Date")]
            [Required(ErrorMessage = "Created Date is required")]
    		public System.DateTime  CreatedDate { get; set; }
    
    		    
    		/// <summary>
    		/// Updated By
    		/// </summary>        
    	//    [DisplayName("Updated By")]
            [MaxLength(6, ErrorMessage = "Updated By cannot be longer than 6 characters")]
    		public string  UpdatedBy { get; set; }
    
    		    
    		/// <summary>
    		/// Updated Date
    		/// </summary>        
    	//    [DisplayName("Updated Date")]
    		public Nullable<System.DateTime>  UpdatedDate { get; set; }
    
    		    
    		/// <summary>
    		/// Version
    		/// </summary>        
    	//    [DisplayName("Version")]
            [Required(ErrorMessage = "Version is required")]
            [MaxLength(8, ErrorMessage = "Version cannot be longer than 8 characters")]
    		public byte[]  Version { get; set; }
    
    		    
    		/// <summary>
    		/// AMSSea Port Code
    		/// </summary>        
    	//    [DisplayName("AMSSea Port Code")]
            [MaxLength(10, ErrorMessage = "AMSSea Port Code cannot be longer than 10 characters")]
    		public string  AMSSeaPortCode { get; set; }
    
    		    
    		/// <summary>
    		/// ISF5 Port
    		/// </summary>        
    	//    [DisplayName("ISF5 Port")]
            [MaxLength(50, ErrorMessage = "ISF5 Port cannot be longer than 50 characters")]
    		public string  ISF5Port { get; set; }
    
    		    
    		/// <summary>
    		/// UNLocation Code
    		/// </summary>        
    	//    [DisplayName("UNLocation Code")]
            [MaxLength(20, ErrorMessage = "UNLocation Code cannot be longer than 20 characters")]
    		public string  UNLocationCode { get; set; }
    
    		    
    	}
    //}
    
}
