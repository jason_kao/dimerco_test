using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ValuePlus.ViewModel;



namespace ValuePlus.ViewModel.ReSM
{
    
    /// <summary>
    /// SMCustomerHDI class
    /// </summary>
    //[MetadataType(typeof(SMCustomerHDIViewModel))]
    //public  partial class SMCustomerHDI
    //{
    
    	/// <summary>
    	/// SMCustomerHDI Metadata class
    	/// </summary>
    	public   class SMCustomerHDIViewModel:ViewModelBase
    	{
    		    
    		/// <summary>
    		/// HQID
    		/// </summary>        
    	//    [DisplayName("HQID")]
            [Required(ErrorMessage = "HQID is required")]
    		public int  HQID { get; set; }
    
    		    
    		/// <summary>
    		/// Customer ID
    		/// </summary>        
    	//    [DisplayName("Customer ID")]
    		public Nullable<int>  CustomerID { get; set; }
    
    		    
    		/// <summary>
    		/// HDIID
    		/// </summary>        
    	//    [DisplayName("HDIID")]
    		public Nullable<int>  HDIID { get; set; }
    
    		    
    		/// <summary>
    		/// HDIDescription
    		/// </summary>        
    	//    [DisplayName("HDIDescription")]
            [MaxLength(2000, ErrorMessage = "HDIDescription cannot be longer than 2000 characters")]
    		public string  HDIDescription { get; set; }
    
    		    
    		/// <summary>
    		/// Mode
    		/// </summary>        
    	//    [DisplayName("Mode")]
            [MaxLength(50, ErrorMessage = "Mode cannot be longer than 50 characters")]
    		public string  Mode { get; set; }
    
    		    
    		/// <summary>
    		/// Forms
    		/// </summary>        
    	//    [DisplayName("Forms")]
            [MaxLength(500, ErrorMessage = "Forms cannot be longer than 500 characters")]
    		public string  Forms { get; set; }
    
    		    
    		/// <summary>
    		/// Status
    		/// </summary>        
    	//    [DisplayName("Status")]
            [Required(ErrorMessage = "Status is required")]
            [MaxLength(10, ErrorMessage = "Status cannot be longer than 10 characters")]
    		public string  Status { get; set; }
    
    		    
    		/// <summary>
    		/// Created By
    		/// </summary>        
    	//    [DisplayName("Created By")]
            [MaxLength(50, ErrorMessage = "Created By cannot be longer than 50 characters")]
    		public string  CreatedBy { get; set; }
    
    		    
    		/// <summary>
    		/// Created Date
    		/// </summary>        
    	//    [DisplayName("Created Date")]
    		public Nullable<System.DateTime>  CreatedDate { get; set; }
    
    		    
    		/// <summary>
    		/// Updated By
    		/// </summary>        
    	//    [DisplayName("Updated By")]
            [MaxLength(50, ErrorMessage = "Updated By cannot be longer than 50 characters")]
    		public string  UpdatedBy { get; set; }
    
    		    
    		/// <summary>
    		/// Updated Date
    		/// </summary>        
    	//    [DisplayName("Updated Date")]
    		public Nullable<System.DateTime>  UpdatedDate { get; set; }
    
    		    
    		/// <summary>
    		/// Version
    		/// </summary>        
    	//    [DisplayName("Version")]
            [MaxLength(8, ErrorMessage = "Version cannot be longer than 8 characters")]
    		public byte[]  Version { get; set; }
    
    		    
    		/// <summary>
    		/// Origin HDIID
    		/// </summary>        
    	//    [DisplayName("Origin HDIID")]
    		public Nullable<int>  OriginHDIID { get; set; }
    
    		    
    		/// <summary>
    		/// HDIType
    		/// </summary>        
    	//    [DisplayName("HDIType")]
            [MaxLength(50, ErrorMessage = "HDIType cannot be longer than 50 characters")]
    		public string  HDIType { get; set; }
    
    		    
    	}
    //}
    
}
