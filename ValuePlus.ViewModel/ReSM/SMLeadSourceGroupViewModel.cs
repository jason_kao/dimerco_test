using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ValuePlus.ViewModel;



namespace ValuePlus.ViewModel.ReSM
{
    
    /// <summary>
    /// SMLeadSourceGroup class
    /// </summary>
    //[MetadataType(typeof(SMLeadSourceGroupViewModel))]
    //public  partial class SMLeadSourceGroup
    //{
    
    	/// <summary>
    	/// SMLeadSourceGroup Metadata class
    	/// </summary>
    	public   class SMLeadSourceGroupViewModel:ViewModelBase
    	{
    		    
    		/// <summary>
    		/// HQID
    		/// </summary>        
    	//    [DisplayName("HQID")]
            [Required(ErrorMessage = "HQID is required")]
    		public int  HQID { get; set; }
    
    		    
    		/// <summary>
    		/// Lead Source Group Code
    		/// </summary>        
    	//    [DisplayName("Lead Source Group Code")]
            [Required(ErrorMessage = "Lead Source Group Code is required")]
            [MaxLength(10, ErrorMessage = "Lead Source Group Code cannot be longer than 10 characters")]
    		public string  LeadSourceGroupCode { get; set; }
    
    		    
    		/// <summary>
    		/// Lead Source Group Name
    		/// </summary>        
    	//    [DisplayName("Lead Source Group Name")]
            [Required(ErrorMessage = "Lead Source Group Name is required")]
            [MaxLength(255, ErrorMessage = "Lead Source Group Name cannot be longer than 255 characters")]
    		public string  LeadSourceGroupName { get; set; }
    
    		    
    		/// <summary>
    		/// Status
    		/// </summary>        
    	//    [DisplayName("Status")]
            [Required(ErrorMessage = "Status is required")]
            [MaxLength(50, ErrorMessage = "Status cannot be longer than 50 characters")]
    		public string  Status { get; set; }
    
    		    
    		/// <summary>
    		/// Created By
    		/// </summary>        
    	//    [DisplayName("Created By")]
            [MaxLength(6, ErrorMessage = "Created By cannot be longer than 6 characters")]
    		public string  CreatedBy { get; set; }
    
    		    
    		/// <summary>
    		/// Created Date
    		/// </summary>        
    	//    [DisplayName("Created Date")]
            [Required(ErrorMessage = "Created Date is required")]
    		public System.DateTime  CreatedDate { get; set; }
    
    		    
    		/// <summary>
    		/// Updated By
    		/// </summary>        
    	//    [DisplayName("Updated By")]
            [MaxLength(6, ErrorMessage = "Updated By cannot be longer than 6 characters")]
    		public string  UpdatedBy { get; set; }
    
    		    
    		/// <summary>
    		/// Updated Date
    		/// </summary>        
    	//    [DisplayName("Updated Date")]
    		public Nullable<System.DateTime>  UpdatedDate { get; set; }
    
    		    
    	}
    //}
    
}
