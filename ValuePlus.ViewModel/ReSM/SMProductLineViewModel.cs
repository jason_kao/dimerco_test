using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ValuePlus.ViewModel;



namespace ValuePlus.ViewModel.ReSM
{
    
    /// <summary>
    /// SMProductLine class
    /// </summary>
    //[MetadataType(typeof(SMProductLineViewModel))]
    //public  partial class SMProductLine
    //{
    
    	/// <summary>
    	/// SMProductLine Metadata class
    	/// </summary>
    	public   class SMProductLineViewModel:ViewModelBase
    	{
    		    
    		/// <summary>
    		/// HQID
    		/// </summary>        
    	//    [DisplayName("HQID")]
            [Required(ErrorMessage = "HQID is required")]
    		public int  HQID { get; set; }
    
    		    
    		/// <summary>
    		/// Product Line Code
    		/// </summary>        
    	//    [DisplayName("Product Line Code")]
            [Required(ErrorMessage = "Product Line Code is required")]
            [MaxLength(5, ErrorMessage = "Product Line Code cannot be longer than 5 characters")]
    		public string  ProductLineCode { get; set; }
    
    		    
    		/// <summary>
    		/// Product Line Name
    		/// </summary>        
    	//    [DisplayName("Product Line Name")]
            [MaxLength(255, ErrorMessage = "Product Line Name cannot be longer than 255 characters")]
    		public string  ProductLineName { get; set; }
    
    		    
    		/// <summary>
    		/// Expor Imp
    		/// </summary>        
    	//    [DisplayName("Expor Imp")]
            [MaxLength(1, ErrorMessage = "Expor Imp cannot be longer than 1 characters")]
    		public string  ExporImp { get; set; }
    
    		    
    		/// <summary>
    		/// Status
    		/// </summary>        
    	//    [DisplayName("Status")]
            [Required(ErrorMessage = "Status is required")]
            [MaxLength(10, ErrorMessage = "Status cannot be longer than 10 characters")]
    		public string  Status { get; set; }
    
    		    
    		/// <summary>
    		/// Created By
    		/// </summary>        
    	//    [DisplayName("Created By")]
            [MaxLength(6, ErrorMessage = "Created By cannot be longer than 6 characters")]
    		public string  CreatedBy { get; set; }
    
    		    
    		/// <summary>
    		/// Created Date
    		/// </summary>        
    	//    [DisplayName("Created Date")]
            [Required(ErrorMessage = "Created Date is required")]
    		public System.DateTime  CreatedDate { get; set; }
    
    		    
    		/// <summary>
    		/// Updated By
    		/// </summary>        
    	//    [DisplayName("Updated By")]
            [MaxLength(6, ErrorMessage = "Updated By cannot be longer than 6 characters")]
    		public string  UpdatedBy { get; set; }
    
    		    
    		/// <summary>
    		/// Updated Date
    		/// </summary>        
    	//    [DisplayName("Updated Date")]
    		public Nullable<System.DateTime>  UpdatedDate { get; set; }
    
    		    
    		/// <summary>
    		/// Version
    		/// </summary>        
    	//    [DisplayName("Version")]
            [Required(ErrorMessage = "Version is required")]
            [MaxLength(8, ErrorMessage = "Version cannot be longer than 8 characters")]
    		public byte[]  Version { get; set; }
    
    		    
    		/// <summary>
    		/// Is Gate Way
    		/// </summary>        
    	//    [DisplayName("Is Gate Way")]
    		public Nullable<bool>  IsGateWay { get; set; }
    
    		    
    		/// <summary>
    		/// Charge Code UOMType
    		/// </summary>        
    	//    [DisplayName("Charge Code UOMType")]
            [MaxLength(50, ErrorMessage = "Charge Code UOMType cannot be longer than 50 characters")]
    		public string  ChargeCodeUOMType { get; set; }
    
    		    
    		/// <summary>
    		/// Charge Code UOMType2
    		/// </summary>        
    	//    [DisplayName("Charge Code UOMType2")]
            [MaxLength(50, ErrorMessage = "Charge Code UOMType2 cannot be longer than 50 characters")]
    		public string  ChargeCodeUOMType2 { get; set; }
    
    		    
    		/// <summary>
    		/// Product Line Show Code
    		/// </summary>        
    	//    [DisplayName("Product Line Show Code")]
            [MaxLength(10, ErrorMessage = "Product Line Show Code cannot be longer than 10 characters")]
    		public string  ProductLineShowCode { get; set; }
    
    		    
    		/// <summary>
    		/// Is Customer From SAM
    		/// </summary>        
    	//    [DisplayName("Is Customer From SAM")]
    		public Nullable<bool>  IsCustomerFromSAM { get; set; }
    
    		    
    		/// <summary>
    		/// Is Used In PL
    		/// </summary>        
    	//    [DisplayName("Is Used In PL")]
            [Required(ErrorMessage = "Is Used In PL is required")]
    		public bool  IsUsedInPL { get; set; }
    
    		    
    		/// <summary>
    		/// Is Used In Agent Control
    		/// </summary>        
    	//    [DisplayName("Is Used In Agent Control")]
    		public Nullable<bool>  IsUsedInAgentControl { get; set; }
    
    		    
    	}
    //}
    
}
