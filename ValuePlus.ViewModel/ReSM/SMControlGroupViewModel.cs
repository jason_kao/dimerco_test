using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ValuePlus.ViewModel;



namespace ValuePlus.ViewModel.ReSM
{
    
    /// <summary>
    /// SMControlGroup class
    /// </summary>
    //[MetadataType(typeof(SMControlGroupViewModel))]
    //public  partial class SMControlGroup
    //{
    
    	/// <summary>
    	/// SMControlGroup Metadata class
    	/// </summary>
    	public   class SMControlGroupViewModel:ViewModelBase
    	{
    		    
    		/// <summary>
    		/// HQID
    		/// </summary>        
    	//    [DisplayName("HQID")]
            [Required(ErrorMessage = "HQID is required")]
    		public int  HQID { get; set; }
    
    		    
    		/// <summary>
    		/// Field Type
    		/// </summary>        
    	//    [DisplayName("Field Type")]
            [MaxLength(10, ErrorMessage = "Field Type cannot be longer than 10 characters")]
    		public string  FieldType { get; set; }
    
    		    
    		/// <summary>
    		/// CSPProject ID
    		/// </summary>        
    	//    [DisplayName("CSPProject ID")]
    		public Nullable<int>  CSPProjectID { get; set; }
    
    		    
    		/// <summary>
    		/// Customer ID
    		/// </summary>        
    	//    [DisplayName("Customer ID")]
    		public Nullable<int>  CustomerID { get; set; }
    
    		    
    		/// <summary>
    		/// Prodcut Line
    		/// </summary>        
    	//    [DisplayName("Prodcut Line")]
    		public Nullable<int>  ProdcutLine { get; set; }
    
    		    
    		/// <summary>
    		/// Status
    		/// </summary>        
    	//    [DisplayName("Status")]
            [MaxLength(10, ErrorMessage = "Status cannot be longer than 10 characters")]
    		public string  Status { get; set; }
    
    		    
    		/// <summary>
    		/// Created By
    		/// </summary>        
    	//    [DisplayName("Created By")]
            [MaxLength(10, ErrorMessage = "Created By cannot be longer than 10 characters")]
    		public string  CreatedBy { get; set; }
    
    		    
    		/// <summary>
    		/// Created Date
    		/// </summary>        
    	//    [DisplayName("Created Date")]
    		public Nullable<System.DateTime>  CreatedDate { get; set; }
    
    		    
    		/// <summary>
    		/// Updated By
    		/// </summary>        
    	//    [DisplayName("Updated By")]
            [MaxLength(10, ErrorMessage = "Updated By cannot be longer than 10 characters")]
    		public string  UpdatedBy { get; set; }
    
    		    
    		/// <summary>
    		/// Updated Date
    		/// </summary>        
    	//    [DisplayName("Updated Date")]
    		public Nullable<System.DateTime>  UpdatedDate { get; set; }
    
    		    
    		/// <summary>
    		/// Partial As Ful Send
    		/// </summary>        
    	//    [DisplayName("Partial As Ful Send")]
    		public Nullable<bool>  PartialAsFulSend { get; set; }
    
    		    
    		/// <summary>
    		/// Need Reconfirm
    		/// </summary>        
    	//    [DisplayName("Need Reconfirm")]
    		public Nullable<bool>  NeedReconfirm { get; set; }
    
    		    
    		/// <summary>
    		/// Reconfirm By
    		/// </summary>        
    	//    [DisplayName("Reconfirm By")]
            [MaxLength(50, ErrorMessage = "Reconfirm By cannot be longer than 50 characters")]
    		public string  ReconfirmBy { get; set; }
    
    		    
    		/// <summary>
    		/// Shipment Come From PO
    		/// </summary>        
    	//    [DisplayName("Shipment Come From PO")]
    		public Nullable<bool>  ShipmentComeFromPO { get; set; }
    
    		    
    	}
    //}
    
}
