using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ValuePlus.ViewModel;



namespace ValuePlus.ViewModel.ReSM
{
    
    /// <summary>
    /// SMUserRight class
    /// </summary>
    //[MetadataType(typeof(SMUserRightViewModel))]
    //public  partial class SMUserRight
    //{
    
    	/// <summary>
    	/// SMUserRight Metadata class
    	/// </summary>
    	public   class SMUserRightViewModel:ViewModelBase
    	{
    		    
    		/// <summary>
    		/// HQID
    		/// </summary>        
    	//    [DisplayName("HQID")]
            [Required(ErrorMessage = "HQID is required")]
    		public int  HQID { get; set; }
    
    		    
    		/// <summary>
    		/// Function Code
    		/// </summary>        
    	//    [DisplayName("Function Code")]
            [Required(ErrorMessage = "Function Code is required")]
            [MaxLength(50, ErrorMessage = "Function Code cannot be longer than 50 characters")]
    		public string  FunctionCode { get; set; }
    
    		    
    		/// <summary>
    		/// User ID
    		/// </summary>        
    	//    [DisplayName("User ID")]
            [Required(ErrorMessage = "User ID is required")]
            [MaxLength(50, ErrorMessage = "User ID cannot be longer than 50 characters")]
    		public string  UserID { get; set; }
    
    		    
    		/// <summary>
    		/// User Right
    		/// </summary>        
    	//    [DisplayName("User Right")]
    		public Nullable<int>  UserRight { get; set; }
    
    		    
    		/// <summary>
    		/// Created By
    		/// </summary>        
    	//    [DisplayName("Created By")]
            [MaxLength(50, ErrorMessage = "Created By cannot be longer than 50 characters")]
    		public string  CreatedBy { get; set; }
    
    		    
    		/// <summary>
    		/// Created Date
    		/// </summary>        
    	//    [DisplayName("Created Date")]
    		public Nullable<System.DateTime>  CreatedDate { get; set; }
    
    		    
    		/// <summary>
    		/// Updated By
    		/// </summary>        
    	//    [DisplayName("Updated By")]
            [MaxLength(50, ErrorMessage = "Updated By cannot be longer than 50 characters")]
    		public string  UpdatedBy { get; set; }
    
    		    
    		/// <summary>
    		/// Updated Date
    		/// </summary>        
    	//    [DisplayName("Updated Date")]
    		public Nullable<System.DateTime>  UpdatedDate { get; set; }
    
    		    
    	}
    //}
    
}
