using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ValuePlus.ViewModel;



namespace ValuePlus.ViewModel.ReSM
{
    
    /// <summary>
    /// SMPODTemplate class
    /// </summary>
    //[MetadataType(typeof(SMPODTemplateViewModel))]
    //public  partial class SMPODTemplate
    //{
    
    	/// <summary>
    	/// SMPODTemplate Metadata class
    	/// </summary>
    	public   class SMPODTemplateViewModel:ViewModelBase
    	{
    		    
    		/// <summary>
    		/// HQID
    		/// </summary>        
    	//    [DisplayName("HQID")]
            [Required(ErrorMessage = "HQID is required")]
    		public int  HQID { get; set; }
    
    		    
    		/// <summary>
    		/// Customer ID
    		/// </summary>        
    	//    [DisplayName("Customer ID")]
            [Required(ErrorMessage = "Customer ID is required")]
    		public int  CustomerID { get; set; }
    
    		    
    		/// <summary>
    		/// DEPT
    		/// </summary>        
    	//    [DisplayName("DEPT")]
    		public Nullable<int>  DEPT { get; set; }
    
    		    
    		/// <summary>
    		/// DSTN
    		/// </summary>        
    	//    [DisplayName("DSTN")]
    		public Nullable<int>  DSTN { get; set; }
    
    		    
    		/// <summary>
    		/// PODTEMPLDesc
    		/// </summary>        
    	//    [DisplayName("PODTEMPLDesc")]
            [MaxLength(100, ErrorMessage = "PODTEMPLDesc cannot be longer than 100 characters")]
    		public string  PODTEMPLDesc { get; set; }
    
    		    
    		/// <summary>
    		/// GLobal Code
    		/// </summary>        
    	//    [DisplayName("GLobal Code")]
            [MaxLength(10, ErrorMessage = "GLobal Code cannot be longer than 10 characters")]
    		public string  GLobalCode { get; set; }
    
    		    
    		/// <summary>
    		/// Created By
    		/// </summary>        
    	//    [DisplayName("Created By")]
            [MaxLength(10, ErrorMessage = "Created By cannot be longer than 10 characters")]
    		public string  CreatedBy { get; set; }
    
    		    
    		/// <summary>
    		/// Created Date
    		/// </summary>        
    	//    [DisplayName("Created Date")]
    		public Nullable<System.DateTime>  CreatedDate { get; set; }
    
    		    
    		/// <summary>
    		/// Updated By
    		/// </summary>        
    	//    [DisplayName("Updated By")]
            [MaxLength(10, ErrorMessage = "Updated By cannot be longer than 10 characters")]
    		public string  UpdatedBy { get; set; }
    
    		    
    		/// <summary>
    		/// Updated Date
    		/// </summary>        
    	//    [DisplayName("Updated Date")]
    		public Nullable<System.DateTime>  UpdatedDate { get; set; }
    
    		    
    		/// <summary>
    		/// Trade Term
    		/// </summary>        
    	//    [DisplayName("Trade Term")]
            [MaxLength(50, ErrorMessage = "Trade Term cannot be longer than 50 characters")]
    		public string  TradeTerm { get; set; }
    
    		    
    	}
    //}
    
}
