using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ValuePlus.ViewModel;



namespace ValuePlus.ViewModel.ReSM
{
    
    /// <summary>
    /// SMANActionRule class
    /// </summary>
    //[MetadataType(typeof(SMANActionRuleViewModel))]
    //public  partial class SMANActionRule
    //{
    
    	/// <summary>
    	/// SMANActionRule Metadata class
    	/// </summary>
    	public   class SMANActionRuleViewModel:ViewModelBase
    	{
    		    
    		/// <summary>
    		/// HQID
    		/// </summary>        
    	//    [DisplayName("HQID")]
            [Required(ErrorMessage = "HQID is required")]
    		public int  HQID { get; set; }
    
    		    
    		/// <summary>
    		/// ANFunction Code
    		/// </summary>        
    	//    [DisplayName("ANFunction Code")]
            [Required(ErrorMessage = "ANFunction Code is required")]
            [MaxLength(4, ErrorMessage = "ANFunction Code cannot be longer than 4 characters")]
    		public string  ANFunctionCode { get; set; }
    
    		    
    		/// <summary>
    		/// Type1 Level
    		/// </summary>        
    	//    [DisplayName("Type1 Level")]
            [Required(ErrorMessage = "Type1 Level is required")]
            [MaxLength(3, ErrorMessage = "Type1 Level cannot be longer than 3 characters")]
    		public string  Type1Level { get; set; }
    
    		    
    		/// <summary>
    		/// Type2 Level
    		/// </summary>        
    	//    [DisplayName("Type2 Level")]
            [Required(ErrorMessage = "Type2 Level is required")]
            [MaxLength(3, ErrorMessage = "Type2 Level cannot be longer than 3 characters")]
    		public string  Type2Level { get; set; }
    
    		    
    		/// <summary>
    		/// Type3 Level
    		/// </summary>        
    	//    [DisplayName("Type3 Level")]
            [MaxLength(3, ErrorMessage = "Type3 Level cannot be longer than 3 characters")]
    		public string  Type3Level { get; set; }
    
    		    
    		/// <summary>
    		/// ANAction Code
    		/// </summary>        
    	//    [DisplayName("ANAction Code")]
            [Required(ErrorMessage = "ANAction Code is required")]
            [MaxLength(2, ErrorMessage = "ANAction Code cannot be longer than 2 characters")]
    		public string  ANActionCode { get; set; }
    
    		    
    		/// <summary>
    		/// Created By
    		/// </summary>        
    	//    [DisplayName("Created By")]
            [Required(ErrorMessage = "Created By is required")]
            [MaxLength(6, ErrorMessage = "Created By cannot be longer than 6 characters")]
    		public string  CreatedBy { get; set; }
    
    		    
    		/// <summary>
    		/// Created Date
    		/// </summary>        
    	//    [DisplayName("Created Date")]
            [Required(ErrorMessage = "Created Date is required")]
    		public System.DateTime  CreatedDate { get; set; }
    
    		    
    		/// <summary>
    		/// Updated By
    		/// </summary>        
    	//    [DisplayName("Updated By")]
            [MaxLength(6, ErrorMessage = "Updated By cannot be longer than 6 characters")]
    		public string  UpdatedBy { get; set; }
    
    		    
    		/// <summary>
    		/// Updated Date
    		/// </summary>        
    	//    [DisplayName("Updated Date")]
    		public Nullable<System.DateTime>  UpdatedDate { get; set; }
    
    		    
    		/// <summary>
    		/// Mixed Label
    		/// </summary>        
    	//    [DisplayName("Mixed Label")]
            [MaxLength(50, ErrorMessage = "Mixed Label cannot be longer than 50 characters")]
    		public string  MixedLabel { get; set; }
    
    		    
    	}
    //}
    
}
