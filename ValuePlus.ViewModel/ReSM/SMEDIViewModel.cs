using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ValuePlus.ViewModel;



namespace ValuePlus.ViewModel.ReSM
{
    
    /// <summary>
    /// SMEDI class
    /// </summary>
    //[MetadataType(typeof(SMEDIViewModel))]
    //public  partial class SMEDI
    //{
    
    	/// <summary>
    	/// SMEDI Metadata class
    	/// </summary>
    	public   class SMEDIViewModel:ViewModelBase
    	{
    		    
    		/// <summary>
    		/// HQID
    		/// </summary>        
    	//    [DisplayName("HQID")]
            [Required(ErrorMessage = "HQID is required")]
    		public int  HQID { get; set; }
    
    		    
    		/// <summary>
    		/// Station ID
    		/// </summary>        
    	//    [DisplayName("Station ID")]
            [Required(ErrorMessage = "Station ID is required")]
            [MaxLength(50, ErrorMessage = "Station ID cannot be longer than 50 characters")]
    		public string  StationID { get; set; }
    
    		    
    		/// <summary>
    		/// EDIType ID
    		/// </summary>        
    	//    [DisplayName("EDIType ID")]
            [Required(ErrorMessage = "EDIType ID is required")]
    		public int  EDITypeID { get; set; }
    
    		    
    		/// <summary>
    		/// Created By
    		/// </summary>        
    	//    [DisplayName("Created By")]
            [MaxLength(50, ErrorMessage = "Created By cannot be longer than 50 characters")]
    		public string  CreatedBy { get; set; }
    
    		    
    		/// <summary>
    		/// Created Date
    		/// </summary>        
    	//    [DisplayName("Created Date")]
    		public Nullable<System.DateTime>  CreatedDate { get; set; }
    
    		    
    		/// <summary>
    		/// Updated By
    		/// </summary>        
    	//    [DisplayName("Updated By")]
            [MaxLength(50, ErrorMessage = "Updated By cannot be longer than 50 characters")]
    		public string  UpdatedBy { get; set; }
    
    		    
    		/// <summary>
    		/// Updated Date
    		/// </summary>        
    	//    [DisplayName("Updated Date")]
            [MaxLength(10, ErrorMessage = "Updated Date cannot be longer than 10 characters")]
    		public string  UpdatedDate { get; set; }
    
    		    
    		/// <summary>
    		/// Edi Value
    		/// </summary>        
    	//    [DisplayName("Edi Value")]
    		public Nullable<int>  EdiValue { get; set; }
    
    		    
    		/// <summary>
    		/// Remark
    		/// </summary>        
    	//    [DisplayName("Remark")]
            [MaxLength(50, ErrorMessage = "Remark cannot be longer than 50 characters")]
    		public string  Remark { get; set; }
    
    		    
    	}
    //}
    
}
