using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ValuePlus.ViewModel;



namespace ValuePlus.ViewModel.ReSM
{
    
    /// <summary>
    /// SMDepartment class
    /// </summary>
    //[MetadataType(typeof(SMDepartmentViewModel))]
    //public  partial class SMDepartment
    //{
    
    	/// <summary>
    	/// SMDepartment Metadata class
    	/// </summary>
    	public   class SMDepartmentViewModel:ViewModelBase
    	{
    		    
    		/// <summary>
    		/// HQID
    		/// </summary>        
    	//    [DisplayName("HQID")]
            [Required(ErrorMessage = "HQID is required")]
    		public int  HQID { get; set; }
    
    		    
    		/// <summary>
    		/// Department Code
    		/// </summary>        
    	//    [DisplayName("Department Code")]
            [Required(ErrorMessage = "Department Code is required")]
            [MaxLength(10, ErrorMessage = "Department Code cannot be longer than 10 characters")]
    		public string  DepartmentCode { get; set; }
    
    		    
    		/// <summary>
    		/// Department Name
    		/// </summary>        
    	//    [DisplayName("Department Name")]
            [MaxLength(255, ErrorMessage = "Department Name cannot be longer than 255 characters")]
    		public string  DepartmentName { get; set; }
    
    		    
    		/// <summary>
    		/// Station ID
    		/// </summary>        
    	//    [DisplayName("Station ID")]
            [Required(ErrorMessage = "Station ID is required")]
            [MaxLength(3, ErrorMessage = "Station ID cannot be longer than 3 characters")]
    		public string  StationID { get; set; }
    
    		    
    		/// <summary>
    		/// Status
    		/// </summary>        
    	//    [DisplayName("Status")]
            [Required(ErrorMessage = "Status is required")]
            [MaxLength(10, ErrorMessage = "Status cannot be longer than 10 characters")]
    		public string  Status { get; set; }
    
    		    
    		/// <summary>
    		/// Created By
    		/// </summary>        
    	//    [DisplayName("Created By")]
            [MaxLength(6, ErrorMessage = "Created By cannot be longer than 6 characters")]
    		public string  CreatedBy { get; set; }
    
    		    
    		/// <summary>
    		/// Created Date
    		/// </summary>        
    	//    [DisplayName("Created Date")]
            [Required(ErrorMessage = "Created Date is required")]
    		public System.DateTime  CreatedDate { get; set; }
    
    		    
    		/// <summary>
    		/// Updated By
    		/// </summary>        
    	//    [DisplayName("Updated By")]
            [MaxLength(6, ErrorMessage = "Updated By cannot be longer than 6 characters")]
    		public string  UpdatedBy { get; set; }
    
    		    
    		/// <summary>
    		/// Updated Date
    		/// </summary>        
    	//    [DisplayName("Updated Date")]
    		public Nullable<System.DateTime>  UpdatedDate { get; set; }
    
    		    
    		/// <summary>
    		/// Version
    		/// </summary>        
    	//    [DisplayName("Version")]
            [Required(ErrorMessage = "Version is required")]
            [MaxLength(8, ErrorMessage = "Version cannot be longer than 8 characters")]
    		public byte[]  Version { get; set; }
    
    		    
    		/// <summary>
    		/// CRPDepartment Code
    		/// </summary>        
    	//    [DisplayName("CRPDepartment Code")]
            [MaxLength(10, ErrorMessage = "CRPDepartment Code cannot be longer than 10 characters")]
    		public string  CRPDepartmentCode { get; set; }
    
    		    
    	}
    //}
    
}
