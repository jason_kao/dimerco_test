using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ValuePlus.ViewModel;



namespace ValuePlus.ViewModel.ReSM
{
    
    /// <summary>
    /// SMDIMDistrict class
    /// </summary>
    //[MetadataType(typeof(SMDIMDistrictViewModel))]
    //public  partial class SMDIMDistrict
    //{
    
    	/// <summary>
    	/// SMDIMDistrict Metadata class
    	/// </summary>
    	public   class SMDIMDistrictViewModel:ViewModelBase
    	{
    		    
    		/// <summary>
    		/// HQID
    		/// </summary>        
    	//    [DisplayName("HQID")]
            [Required(ErrorMessage = "HQID is required")]
    		public int  HQID { get; set; }
    
    		    
    		/// <summary>
    		/// Dim District Code
    		/// </summary>        
    	//    [DisplayName("Dim District Code")]
            [Required(ErrorMessage = "Dim District Code is required")]
            [MaxLength(10, ErrorMessage = "Dim District Code cannot be longer than 10 characters")]
    		public string  DimDistrictCode { get; set; }
    
    		    
    		/// <summary>
    		/// Dim Region ID
    		/// </summary>        
    	//    [DisplayName("Dim Region ID")]
            [Required(ErrorMessage = "Dim Region ID is required")]
    		public int  DimRegionID { get; set; }
    
    		    
    		/// <summary>
    		/// Dim District Name
    		/// </summary>        
    	//    [DisplayName("Dim District Name")]
            [MaxLength(255, ErrorMessage = "Dim District Name cannot be longer than 255 characters")]
    		public string  DimDistrictName { get; set; }
    
    		    
    		/// <summary>
    		/// Status
    		/// </summary>        
    	//    [DisplayName("Status")]
            [Required(ErrorMessage = "Status is required")]
            [MaxLength(10, ErrorMessage = "Status cannot be longer than 10 characters")]
    		public string  Status { get; set; }
    
    		    
    		/// <summary>
    		/// Created By
    		/// </summary>        
    	//    [DisplayName("Created By")]
            [MaxLength(6, ErrorMessage = "Created By cannot be longer than 6 characters")]
    		public string  CreatedBy { get; set; }
    
    		    
    		/// <summary>
    		/// Created Date
    		/// </summary>        
    	//    [DisplayName("Created Date")]
            [Required(ErrorMessage = "Created Date is required")]
    		public System.DateTime  CreatedDate { get; set; }
    
    		    
    		/// <summary>
    		/// Updated By
    		/// </summary>        
    	//    [DisplayName("Updated By")]
            [MaxLength(6, ErrorMessage = "Updated By cannot be longer than 6 characters")]
    		public string  UpdatedBy { get; set; }
    
    		    
    		/// <summary>
    		/// Updated Date
    		/// </summary>        
    	//    [DisplayName("Updated Date")]
    		public Nullable<System.DateTime>  UpdatedDate { get; set; }
    
    		    
    		/// <summary>
    		/// Version
    		/// </summary>        
    	//    [DisplayName("Version")]
            [Required(ErrorMessage = "Version is required")]
            [MaxLength(8, ErrorMessage = "Version cannot be longer than 8 characters")]
    		public byte[]  Version { get; set; }
    
    		    
    	}
    //}
    
}
