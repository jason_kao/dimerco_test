using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ValuePlus.ViewModel;



namespace ValuePlus.ViewModel.ReSM
{
    
    /// <summary>
    /// SMGroupMilestone class
    /// </summary>
    //[MetadataType(typeof(SMGroupMilestoneViewModel))]
    //public  partial class SMGroupMilestone
    //{
    
    	/// <summary>
    	/// SMGroupMilestone Metadata class
    	/// </summary>
    	public   class SMGroupMilestoneViewModel:ViewModelBase
    	{
    		    
    		/// <summary>
    		/// Group ID
    		/// </summary>        
    	//    [DisplayName("Group ID")]
            [Required(ErrorMessage = "Group ID is required")]
    		public int  GroupID { get; set; }
    
    		    
    		/// <summary>
    		/// Milestone ID
    		/// </summary>        
    	//    [DisplayName("Milestone ID")]
            [Required(ErrorMessage = "Milestone ID is required")]
    		public int  MilestoneID { get; set; }
    
    		    
    		/// <summary>
    		/// Milestone Name
    		/// </summary>        
    	//    [DisplayName("Milestone Name")]
            [MaxLength(100, ErrorMessage = "Milestone Name cannot be longer than 100 characters")]
    		public string  MilestoneName { get; set; }
    
    		    
    		/// <summary>
    		/// Input Type
    		/// </summary>        
    	//    [DisplayName("Input Type")]
            [MaxLength(1, ErrorMessage = "Input Type cannot be longer than 1 characters")]
    		public string  InputType { get; set; }
    
    		    
    		/// <summary>
    		/// Description
    		/// </summary>        
    	//    [DisplayName("Description")]
            [MaxLength(100, ErrorMessage = "Description cannot be longer than 100 characters")]
    		public string  Description { get; set; }
    
    		    
    		/// <summary>
    		/// Seq Num
    		/// </summary>        
    	//    [DisplayName("Seq Num")]
    		public Nullable<int>  SeqNum { get; set; }
    
    		    
    		/// <summary>
    		/// Usage
    		/// </summary>        
    	//    [DisplayName("Usage")]
            [MaxLength(1, ErrorMessage = "Usage cannot be longer than 1 characters")]
    		public string  Usage { get; set; }
    
    		    
    	}
    //}
    
}
