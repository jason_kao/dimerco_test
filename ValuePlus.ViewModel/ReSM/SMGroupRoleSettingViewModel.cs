using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ValuePlus.ViewModel;



namespace ValuePlus.ViewModel.ReSM
{
    
    /// <summary>
    /// SMGroupRoleSetting class
    /// </summary>
    //[MetadataType(typeof(SMGroupRoleSettingViewModel))]
    //public  partial class SMGroupRoleSetting
    //{
    
    	/// <summary>
    	/// SMGroupRoleSetting Metadata class
    	/// </summary>
    	public   class SMGroupRoleSettingViewModel:ViewModelBase
    	{
    		    
    		/// <summary>
    		/// ID
    		/// </summary>        
    	//    [DisplayName("ID")]
            [Required(ErrorMessage = "ID is required")]
    		public int  ID { get; set; }
    
    		    
    		/// <summary>
    		/// Role No
    		/// </summary>        
    	//    [DisplayName("Role No")]
            [MaxLength(10, ErrorMessage = "Role No cannot be longer than 10 characters")]
    		public string  RoleNo { get; set; }
    
    		    
    		/// <summary>
    		/// Role Type
    		/// </summary>        
    	//    [DisplayName("Role Type")]
            [MaxLength(10, ErrorMessage = "Role Type cannot be longer than 10 characters")]
    		public string  RoleType { get; set; }
    
    		    
    		/// <summary>
    		/// Role ID
    		/// </summary>        
    	//    [DisplayName("Role ID")]
    		public Nullable<int>  RoleID { get; set; }
    
    		    
    		/// <summary>
    		/// User ID
    		/// </summary>        
    	//    [DisplayName("User ID")]
            [MaxLength(6, ErrorMessage = "User ID cannot be longer than 6 characters")]
    		public string  UserID { get; set; }
    
    		    
    		/// <summary>
    		/// Apply Mode
    		/// </summary>        
    	//    [DisplayName("Apply Mode")]
            [MaxLength(50, ErrorMessage = "Apply Mode cannot be longer than 50 characters")]
    		public string  ApplyMode { get; set; }
    
    		    
    		/// <summary>
    		/// Status
    		/// </summary>        
    	//    [DisplayName("Status")]
            [MaxLength(10, ErrorMessage = "Status cannot be longer than 10 characters")]
    		public string  Status { get; set; }
    
    		    
    		/// <summary>
    		/// Created By
    		/// </summary>        
    	//    [DisplayName("Created By")]
            [MaxLength(6, ErrorMessage = "Created By cannot be longer than 6 characters")]
    		public string  CreatedBy { get; set; }
    
    		    
    		/// <summary>
    		/// Created Date
    		/// </summary>        
    	//    [DisplayName("Created Date")]
    		public Nullable<System.DateTime>  CreatedDate { get; set; }
    
    		    
    		/// <summary>
    		/// Updated By
    		/// </summary>        
    	//    [DisplayName("Updated By")]
            [MaxLength(6, ErrorMessage = "Updated By cannot be longer than 6 characters")]
    		public string  UpdatedBy { get; set; }
    
    		    
    		/// <summary>
    		/// UPdated Date
    		/// </summary>        
    	//    [DisplayName("UPdated Date")]
    		public Nullable<System.DateTime>  UPdatedDate { get; set; }
    
    		    
    	}
    //}
    
}
