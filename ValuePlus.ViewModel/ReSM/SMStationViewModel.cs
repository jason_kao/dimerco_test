using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ValuePlus.ViewModel;



namespace ValuePlus.ViewModel.ReSM
{
    
    /// <summary>
    /// SMStation class
    /// </summary>
    //[MetadataType(typeof(SMStationViewModel))]
    //public  partial class SMStation
    //{
    
    	/// <summary>
    	/// SMStation Metadata class
    	/// </summary>
    	public   class SMStationViewModel:ViewModelBase
    	{
    		    
    		/// <summary>
    		/// HQID
    		/// </summary>        
    	//    [DisplayName("HQID")]
            [Required(ErrorMessage = "HQID is required")]
    		public int  HQID { get; set; }
    
    		    
    		/// <summary>
    		/// Station ID
    		/// </summary>        
    	//    [DisplayName("Station ID")]
            [Required(ErrorMessage = "Station ID is required")]
            [MaxLength(3, ErrorMessage = "Station ID cannot be longer than 3 characters")]
    		public string  StationID { get; set; }
    
    		    
    		/// <summary>
    		/// Customer ID
    		/// </summary>        
    	//    [DisplayName("Customer ID")]
            [Required(ErrorMessage = "Customer ID is required")]
    		public int  CustomerID { get; set; }
    
    		    
    		/// <summary>
    		/// Station Code
    		/// </summary>        
    	//    [DisplayName("Station Code")]
            [Required(ErrorMessage = "Station Code is required")]
            [MaxLength(6, ErrorMessage = "Station Code cannot be longer than 6 characters")]
    		public string  StationCode { get; set; }
    
    		    
    		/// <summary>
    		/// Station Name
    		/// </summary>        
    	//    [DisplayName("Station Name")]
            [MaxLength(255, ErrorMessage = "Station Name cannot be longer than 255 characters")]
    		public string  StationName { get; set; }
    
    		    
    		/// <summary>
    		/// City ID
    		/// </summary>        
    	//    [DisplayName("City ID")]
    		public Nullable<int>  CityID { get; set; }
    
    		    
    		/// <summary>
    		/// Dim Region ID
    		/// </summary>        
    	//    [DisplayName("Dim Region ID")]
    		public Nullable<int>  DimRegionID { get; set; }
    
    		    
    		/// <summary>
    		/// Dim District ID
    		/// </summary>        
    	//    [DisplayName("Dim District ID")]
    		public Nullable<int>  DimDistrictID { get; set; }
    
    		    
    		/// <summary>
    		/// Dim Country ID
    		/// </summary>        
    	//    [DisplayName("Dim Country ID")]
    		public Nullable<int>  DimCountryID { get; set; }
    
    		    
    		/// <summary>
    		/// Station Type
    		/// </summary>        
    	//    [DisplayName("Station Type")]
            [MaxLength(50, ErrorMessage = "Station Type cannot be longer than 50 characters")]
    		public string  StationType { get; set; }
    
    		    
    		/// <summary>
    		/// Station Name1
    		/// </summary>        
    	//    [DisplayName("Station Name1")]
            [MaxLength(255, ErrorMessage = "Station Name1 cannot be longer than 255 characters")]
    		public string  StationName1 { get; set; }
    
    		    
    		/// <summary>
    		/// Station Address1
    		/// </summary>        
    	//    [DisplayName("Station Address1")]
            [MaxLength(50, ErrorMessage = "Station Address1 cannot be longer than 50 characters")]
    		public string  StationAddress1 { get; set; }
    
    		    
    		/// <summary>
    		/// Station Address2
    		/// </summary>        
    	//    [DisplayName("Station Address2")]
            [MaxLength(50, ErrorMessage = "Station Address2 cannot be longer than 50 characters")]
    		public string  StationAddress2 { get; set; }
    
    		    
    		/// <summary>
    		/// Station Address3
    		/// </summary>        
    	//    [DisplayName("Station Address3")]
            [MaxLength(50, ErrorMessage = "Station Address3 cannot be longer than 50 characters")]
    		public string  StationAddress3 { get; set; }
    
    		    
    		/// <summary>
    		/// Station Address4
    		/// </summary>        
    	//    [DisplayName("Station Address4")]
            [MaxLength(50, ErrorMessage = "Station Address4 cannot be longer than 50 characters")]
    		public string  StationAddress4 { get; set; }
    
    		    
    		/// <summary>
    		/// Station Address5
    		/// </summary>        
    	//    [DisplayName("Station Address5")]
            [MaxLength(50, ErrorMessage = "Station Address5 cannot be longer than 50 characters")]
    		public string  StationAddress5 { get; set; }
    
    		    
    		/// <summary>
    		/// Home Currency
    		/// </summary>        
    	//    [DisplayName("Home Currency")]
            [MaxLength(10, ErrorMessage = "Home Currency cannot be longer than 10 characters")]
    		public string  HomeCurrency { get; set; }
    
    		    
    		/// <summary>
    		/// Home Weight UOMID
    		/// </summary>        
    	//    [DisplayName("Home Weight UOMID")]
    		public Nullable<int>  HomeWeightUOMID { get; set; }
    
    		    
    		/// <summary>
    		/// Home Dimension UOMID
    		/// </summary>        
    	//    [DisplayName("Home Dimension UOMID")]
    		public Nullable<int>  HomeDimensionUOMID { get; set; }
    
    		    
    		/// <summary>
    		/// Report Currency
    		/// </summary>        
    	//    [DisplayName("Report Currency")]
            [MaxLength(10, ErrorMessage = "Report Currency cannot be longer than 10 characters")]
    		public string  ReportCurrency { get; set; }
    
    		    
    		/// <summary>
    		/// Report Weight UOMID
    		/// </summary>        
    	//    [DisplayName("Report Weight UOMID")]
    		public Nullable<int>  ReportWeightUOMID { get; set; }
    
    		    
    		/// <summary>
    		/// Station Phone
    		/// </summary>        
    	//    [DisplayName("Station Phone")]
            [MaxLength(50, ErrorMessage = "Station Phone cannot be longer than 50 characters")]
    		public string  StationPhone { get; set; }
    
    		    
    		/// <summary>
    		/// Station Phone Ext
    		/// </summary>        
    	//    [DisplayName("Station Phone Ext")]
            [MaxLength(10, ErrorMessage = "Station Phone Ext cannot be longer than 10 characters")]
    		public string  StationPhoneExt { get; set; }
    
    		    
    		/// <summary>
    		/// Station Fax
    		/// </summary>        
    	//    [DisplayName("Station Fax")]
            [MaxLength(50, ErrorMessage = "Station Fax cannot be longer than 50 characters")]
    		public string  StationFax { get; set; }
    
    		    
    		/// <summary>
    		/// Station Fax Ext
    		/// </summary>        
    	//    [DisplayName("Station Fax Ext")]
            [MaxLength(10, ErrorMessage = "Station Fax Ext cannot be longer than 10 characters")]
    		public string  StationFaxExt { get; set; }
    
    		    
    		/// <summary>
    		/// Station ZIP
    		/// </summary>        
    	//    [DisplayName("Station ZIP")]
            [MaxLength(10, ErrorMessage = "Station ZIP cannot be longer than 10 characters")]
    		public string  StationZIP { get; set; }
    
    		    
    		/// <summary>
    		/// Start Date
    		/// </summary>        
    	//    [DisplayName("Start Date")]
    		public Nullable<System.DateTime>  StartDate { get; set; }
    
    		    
    		/// <summary>
    		/// Month From
    		/// </summary>        
    	//    [DisplayName("Month From")]
    		public Nullable<decimal>  MonthFrom { get; set; }
    
    		    
    		/// <summary>
    		/// ARDate
    		/// </summary>        
    	//    [DisplayName("ARDate")]
    		public Nullable<System.DateTime>  ARDate { get; set; }
    
    		    
    		/// <summary>
    		/// APDate
    		/// </summary>        
    	//    [DisplayName("APDate")]
    		public Nullable<System.DateTime>  APDate { get; set; }
    
    		    
    		/// <summary>
    		/// GLDate
    		/// </summary>        
    	//    [DisplayName("GLDate")]
    		public Nullable<System.DateTime>  GLDate { get; set; }
    
    		    
    		/// <summary>
    		/// GLDate2
    		/// </summary>        
    	//    [DisplayName("GLDate2")]
    		public Nullable<System.DateTime>  GLDate2 { get; set; }
    
    		    
    		/// <summary>
    		/// GLNOARBank
    		/// </summary>        
    	//    [DisplayName("GLNOARBank")]
            [MaxLength(50, ErrorMessage = "GLNOARBank cannot be longer than 50 characters")]
    		public string  GLNOARBank { get; set; }
    
    		    
    		/// <summary>
    		/// GLNOAPBank
    		/// </summary>        
    	//    [DisplayName("GLNOAPBank")]
            [MaxLength(50, ErrorMessage = "GLNOAPBank cannot be longer than 50 characters")]
    		public string  GLNOAPBank { get; set; }
    
    		    
    		/// <summary>
    		/// GLNOExch
    		/// </summary>        
    	//    [DisplayName("GLNOExch")]
            [MaxLength(50, ErrorMessage = "GLNOExch cannot be longer than 50 characters")]
    		public string  GLNOExch { get; set; }
    
    		    
    		/// <summary>
    		/// GLNOUnexch
    		/// </summary>        
    	//    [DisplayName("GLNOUnexch")]
            [MaxLength(50, ErrorMessage = "GLNOUnexch cannot be longer than 50 characters")]
    		public string  GLNOUnexch { get; set; }
    
    		    
    		/// <summary>
    		/// GLNOYE
    		/// </summary>        
    	//    [DisplayName("GLNOYE")]
            [MaxLength(50, ErrorMessage = "GLNOYE cannot be longer than 50 characters")]
    		public string  GLNOYE { get; set; }
    
    		    
    		/// <summary>
    		/// GLDate YE
    		/// </summary>        
    	//    [DisplayName("GLDate YE")]
    		public Nullable<System.DateTime>  GLDateYE { get; set; }
    
    		    
    		/// <summary>
    		/// Site URL
    		/// </summary>        
    	//    [DisplayName("Site URL")]
            [MaxLength(255, ErrorMessage = "Site URL cannot be longer than 255 characters")]
    		public string  SiteURL { get; set; }
    
    		    
    		/// <summary>
    		/// IATACode
    		/// </summary>        
    	//    [DisplayName("IATACode")]
            [MaxLength(50, ErrorMessage = "IATACode cannot be longer than 50 characters")]
    		public string  IATACode { get; set; }
    
    		    
    		/// <summary>
    		/// Registration1
    		/// </summary>        
    	//    [DisplayName("Registration1")]
            [MaxLength(50, ErrorMessage = "Registration1 cannot be longer than 50 characters")]
    		public string  Registration1 { get; set; }
    
    		    
    		/// <summary>
    		/// Registration Name1
    		/// </summary>        
    	//    [DisplayName("Registration Name1")]
            [MaxLength(50, ErrorMessage = "Registration Name1 cannot be longer than 50 characters")]
    		public string  RegistrationName1 { get; set; }
    
    		    
    		/// <summary>
    		/// Registration2
    		/// </summary>        
    	//    [DisplayName("Registration2")]
            [MaxLength(50, ErrorMessage = "Registration2 cannot be longer than 50 characters")]
    		public string  Registration2 { get; set; }
    
    		    
    		/// <summary>
    		/// Registration Name2
    		/// </summary>        
    	//    [DisplayName("Registration Name2")]
            [MaxLength(50, ErrorMessage = "Registration Name2 cannot be longer than 50 characters")]
    		public string  RegistrationName2 { get; set; }
    
    		    
    		/// <summary>
    		/// Registration3
    		/// </summary>        
    	//    [DisplayName("Registration3")]
            [MaxLength(50, ErrorMessage = "Registration3 cannot be longer than 50 characters")]
    		public string  Registration3 { get; set; }
    
    		    
    		/// <summary>
    		/// Registration Name3
    		/// </summary>        
    	//    [DisplayName("Registration Name3")]
            [MaxLength(50, ErrorMessage = "Registration Name3 cannot be longer than 50 characters")]
    		public string  RegistrationName3 { get; set; }
    
    		    
    		/// <summary>
    		/// Registration4
    		/// </summary>        
    	//    [DisplayName("Registration4")]
            [MaxLength(50, ErrorMessage = "Registration4 cannot be longer than 50 characters")]
    		public string  Registration4 { get; set; }
    
    		    
    		/// <summary>
    		/// Registration Name4
    		/// </summary>        
    	//    [DisplayName("Registration Name4")]
            [MaxLength(50, ErrorMessage = "Registration Name4 cannot be longer than 50 characters")]
    		public string  RegistrationName4 { get; set; }
    
    		    
    		/// <summary>
    		/// Registration5
    		/// </summary>        
    	//    [DisplayName("Registration5")]
            [MaxLength(50, ErrorMessage = "Registration5 cannot be longer than 50 characters")]
    		public string  Registration5 { get; set; }
    
    		    
    		/// <summary>
    		/// Registration Name5
    		/// </summary>        
    	//    [DisplayName("Registration Name5")]
            [MaxLength(50, ErrorMessage = "Registration Name5 cannot be longer than 50 characters")]
    		public string  RegistrationName5 { get; set; }
    
    		    
    		/// <summary>
    		/// Status
    		/// </summary>        
    	//    [DisplayName("Status")]
            [Required(ErrorMessage = "Status is required")]
            [MaxLength(10, ErrorMessage = "Status cannot be longer than 10 characters")]
    		public string  Status { get; set; }
    
    		    
    		/// <summary>
    		/// Created By
    		/// </summary>        
    	//    [DisplayName("Created By")]
            [MaxLength(6, ErrorMessage = "Created By cannot be longer than 6 characters")]
    		public string  CreatedBy { get; set; }
    
    		    
    		/// <summary>
    		/// Created Date
    		/// </summary>        
    	//    [DisplayName("Created Date")]
            [Required(ErrorMessage = "Created Date is required")]
    		public System.DateTime  CreatedDate { get; set; }
    
    		    
    		/// <summary>
    		/// Updated By
    		/// </summary>        
    	//    [DisplayName("Updated By")]
            [MaxLength(6, ErrorMessage = "Updated By cannot be longer than 6 characters")]
    		public string  UpdatedBy { get; set; }
    
    		    
    		/// <summary>
    		/// Updated Date
    		/// </summary>        
    	//    [DisplayName("Updated Date")]
    		public Nullable<System.DateTime>  UpdatedDate { get; set; }
    
    		    
    		/// <summary>
    		/// Version
    		/// </summary>        
    	//    [DisplayName("Version")]
            [Required(ErrorMessage = "Version is required")]
            [MaxLength(8, ErrorMessage = "Version cannot be longer than 8 characters")]
    		public byte[]  Version { get; set; }
    
    		    
    		/// <summary>
    		/// DBName
    		/// </summary>        
    	//    [DisplayName("DBName")]
            [MaxLength(50, ErrorMessage = "DBName cannot be longer than 50 characters")]
    		public string  DBName { get; set; }
    
    		    
    		/// <summary>
    		/// Station IP
    		/// </summary>        
    	//    [DisplayName("Station IP")]
            [MaxLength(50, ErrorMessage = "Station IP cannot be longer than 50 characters")]
    		public string  StationIP { get; set; }
    
    		    
    		/// <summary>
    		/// Station Level
    		/// </summary>        
    	//    [DisplayName("Station Level")]
            [MaxLength(10, ErrorMessage = "Station Level cannot be longer than 10 characters")]
    		public string  StationLevel { get; set; }
    
    		    
    		/// <summary>
    		/// Parent Station ID
    		/// </summary>        
    	//    [DisplayName("Parent Station ID")]
            [MaxLength(3, ErrorMessage = "Parent Station ID cannot be longer than 3 characters")]
    		public string  ParentStationID { get; set; }
    
    		    
    		/// <summary>
    		/// Longitute
    		/// </summary>        
    	//    [DisplayName("Longitute")]
            [MaxLength(30, ErrorMessage = "Longitute cannot be longer than 30 characters")]
    		public string  Longitute { get; set; }
    
    		    
    		/// <summary>
    		/// Latitude
    		/// </summary>        
    	//    [DisplayName("Latitude")]
            [MaxLength(30, ErrorMessage = "Latitude cannot be longer than 30 characters")]
    		public string  Latitude { get; set; }
    
    		    
    		/// <summary>
    		/// Time Zone ID
    		/// </summary>        
    	//    [DisplayName("Time Zone ID")]
    		public Nullable<int>  TimeZoneID { get; set; }
    
    		    
    		/// <summary>
    		/// First Seaport ID
    		/// </summary>        
    	//    [DisplayName("First Seaport ID")]
    		public Nullable<int>  FirstSeaportID { get; set; }
    
    		    
    		/// <summary>
    		/// Second Seaport ID
    		/// </summary>        
    	//    [DisplayName("Second Seaport ID")]
    		public Nullable<int>  SecondSeaportID { get; set; }
    
    		    
    		/// <summary>
    		/// Third Seaport ID
    		/// </summary>        
    	//    [DisplayName("Third Seaport ID")]
    		public Nullable<int>  ThirdSeaportID { get; set; }
    
    		    
    		/// <summary>
    		/// Invoice Remark
    		/// </summary>        
    	//    [DisplayName("Invoice Remark")]
    		public string  InvoiceRemark { get; set; }
    
    		    
    		/// <summary>
    		/// Inbox Name
    		/// </summary>        
    	//    [DisplayName("Inbox Name")]
            [MaxLength(50, ErrorMessage = "Inbox Name cannot be longer than 50 characters")]
    		public string  InboxName { get; set; }
    
    		    
    		/// <summary>
    		/// e Mail Acct
    		/// </summary>        
    	//    [DisplayName("e Mail Acct")]
            [MaxLength(50, ErrorMessage = "e Mail Acct cannot be longer than 50 characters")]
    		public string  eMailAcct { get; set; }
    
    		    
    		/// <summary>
    		/// Product Line
    		/// </summary>        
    	//    [DisplayName("Product Line")]
            [MaxLength(100, ErrorMessage = "Product Line cannot be longer than 100 characters")]
    		public string  ProductLine { get; set; }
    
    		    
    		/// <summary>
    		/// Is WMS
    		/// </summary>        
    	//    [DisplayName("Is WMS")]
    		public Nullable<int>  IsWMS { get; set; }
    
    		    
    		/// <summary>
    		/// Cube DBName
    		/// </summary>        
    	//    [DisplayName("Cube DBName")]
            [MaxLength(50, ErrorMessage = "Cube DBName cannot be longer than 50 characters")]
    		public string  CubeDBName { get; set; }
    
    		    
    		/// <summary>
    		/// Overdue Service
    		/// </summary>        
    	//    [DisplayName("Overdue Service")]
            [MaxLength(500, ErrorMessage = "Overdue Service cannot be longer than 500 characters")]
    		public string  OverdueService { get; set; }
    
    		    
    		/// <summary>
    		/// Terms Conditions
    		/// </summary>        
    	//    [DisplayName("Terms Conditions")]
            [MaxLength(500, ErrorMessage = "Terms Conditions cannot be longer than 500 characters")]
    		public string  TermsConditions { get; set; }
    
    		    
    		/// <summary>
    		/// Is Print Contact
    		/// </summary>        
    	//    [DisplayName("Is Print Contact")]
    		public Nullable<bool>  IsPrintContact { get; set; }
    
    		    
    		/// <summary>
    		/// Gate Way City ID
    		/// </summary>        
    	//    [DisplayName("Gate Way City ID")]
    		public Nullable<int>  GateWayCityID { get; set; }
    
    		    
    		/// <summary>
    		/// Arr Notice Stand Remark
    		/// </summary>        
    	//    [DisplayName("Arr Notice Stand Remark")]
            [MaxLength(500, ErrorMessage = "Arr Notice Stand Remark cannot be longer than 500 characters")]
    		public string  ArrNoticeStandRemark { get; set; }
    
    		    
    		/// <summary>
    		/// Arr Notice User Remark
    		/// </summary>        
    	//    [DisplayName("Arr Notice User Remark")]
            [MaxLength(500, ErrorMessage = "Arr Notice User Remark cannot be longer than 500 characters")]
    		public string  ArrNoticeUserRemark { get; set; }
    
    		    
    		/// <summary>
    		/// Used WMSSYS
    		/// </summary>        
    	//    [DisplayName("Used WMSSYS")]
            [Required(ErrorMessage = "Used WMSSYS is required")]
            [MaxLength(1, ErrorMessage = "Used WMSSYS cannot be longer than 1 characters")]
    		public string  UsedWMSSYS { get; set; }
    
    		    
    		/// <summary>
    		/// Used TMSSYS
    		/// </summary>        
    	//    [DisplayName("Used TMSSYS")]
            [Required(ErrorMessage = "Used TMSSYS is required")]
            [MaxLength(1, ErrorMessage = "Used TMSSYS cannot be longer than 1 characters")]
    		public string  UsedTMSSYS { get; set; }
    
    		    
    		/// <summary>
    		/// Local Web URL
    		/// </summary>        
    	//    [DisplayName("Local Web URL")]
            [MaxLength(500, ErrorMessage = "Local Web URL cannot be longer than 500 characters")]
    		public string  LocalWebURL { get; set; }
    
    		    
    		/// <summary>
    		/// HBLRemarks
    		/// </summary>        
    	//    [DisplayName("HBLRemarks")]
            [MaxLength(500, ErrorMessage = "HBLRemarks cannot be longer than 500 characters")]
    		public string  HBLRemarks { get; set; }
    
    		    
    		/// <summary>
    		/// TMSLanguage
    		/// </summary>        
    	//    [DisplayName("TMSLanguage")]
            [Required(ErrorMessage = "TMSLanguage is required")]
            [MaxLength(1, ErrorMessage = "TMSLanguage cannot be longer than 1 characters")]
    		public string  TMSLanguage { get; set; }
    
    		    
    		/// <summary>
    		/// Handling Code
    		/// </summary>        
    	//    [DisplayName("Handling Code")]
    		public Nullable<bool>  HandlingCode { get; set; }
    
    		    
    		/// <summary>
    		/// Local Web Port
    		/// </summary>        
    	//    [DisplayName("Local Web Port")]
            [MaxLength(10, ErrorMessage = "Local Web Port cannot be longer than 10 characters")]
    		public string  LocalWebPort { get; set; }
    
    		    
    		/// <summary>
    		/// FTPRoot
    		/// </summary>        
    	//    [DisplayName("FTPRoot")]
            [Required(ErrorMessage = "FTPRoot is required")]
            [MaxLength(50, ErrorMessage = "FTPRoot cannot be longer than 50 characters")]
    		public string  FTPRoot { get; set; }
    
    		    
    		/// <summary>
    		/// Show Origin Currency
    		/// </summary>        
    	//    [DisplayName("Show Origin Currency")]
    		public Nullable<bool>  ShowOriginCurrency { get; set; }
    
    		    
    		/// <summary>
    		/// Local Web URL2
    		/// </summary>        
    	//    [DisplayName("Local Web URL2")]
            [MaxLength(100, ErrorMessage = "Local Web URL2 cannot be longer than 100 characters")]
    		public string  LocalWebURL2 { get; set; }
    
    		    
    		/// <summary>
    		/// Local Web Port2
    		/// </summary>        
    	//    [DisplayName("Local Web Port2")]
            [MaxLength(10, ErrorMessage = "Local Web Port2 cannot be longer than 10 characters")]
    		public string  LocalWebPort2 { get; set; }
    
    		    
    		/// <summary>
    		/// Local Web URL3
    		/// </summary>        
    	//    [DisplayName("Local Web URL3")]
            [MaxLength(100, ErrorMessage = "Local Web URL3 cannot be longer than 100 characters")]
    		public string  LocalWebURL3 { get; set; }
    
    		    
    		/// <summary>
    		/// Local Web Port3
    		/// </summary>        
    	//    [DisplayName("Local Web Port3")]
            [MaxLength(10, ErrorMessage = "Local Web Port3 cannot be longer than 10 characters")]
    		public string  LocalWebPort3 { get; set; }
    
    		    
    		/// <summary>
    		/// Local Web URL4
    		/// </summary>        
    	//    [DisplayName("Local Web URL4")]
            [MaxLength(100, ErrorMessage = "Local Web URL4 cannot be longer than 100 characters")]
    		public string  LocalWebURL4 { get; set; }
    
    		    
    		/// <summary>
    		/// Local Web Port4
    		/// </summary>        
    	//    [DisplayName("Local Web Port4")]
            [MaxLength(10, ErrorMessage = "Local Web Port4 cannot be longer than 10 characters")]
    		public string  LocalWebPort4 { get; set; }
    
    		    
    		/// <summary>
    		/// Replicated ID
    		/// </summary>        
    	//    [DisplayName("Replicated ID")]
    		public Nullable<int>  ReplicatedID { get; set; }
    
    		    
    		/// <summary>
    		/// LERegion
    		/// </summary>        
    	//    [DisplayName("LERegion")]
    		public Nullable<int>  LERegion { get; set; }
    
    		    
    		/// <summary>
    		/// LELevel1
    		/// </summary>        
    	//    [DisplayName("LELevel1")]
    		public Nullable<int>  LELevel1 { get; set; }
    
    		    
    		/// <summary>
    		/// LELevel2
    		/// </summary>        
    	//    [DisplayName("LELevel2")]
    		public Nullable<int>  LELevel2 { get; set; }
    
    		    
    		/// <summary>
    		/// Is Used Taxed
    		/// </summary>        
    	//    [DisplayName("Is Used Taxed")]
    		public Nullable<int>  IsUsedTaxed { get; set; }
    
    		    
    		/// <summary>
    		/// Is Used Draft Invoice
    		/// </summary>        
    	//    [DisplayName("Is Used Draft Invoice")]
    		public Nullable<int>  IsUsedDraftInvoice { get; set; }
    
    		    
    		/// <summary>
    		/// Credit Rule
    		/// </summary>        
    	//    [DisplayName("Credit Rule")]
    		public Nullable<int>  CreditRule { get; set; }
    
    		    
    		/// <summary>
    		/// Is Duty Exception
    		/// </summary>        
    	//    [DisplayName("Is Duty Exception")]
    		public Nullable<int>  IsDutyException { get; set; }
    
    		    
    	}
    //}
    
}
