using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ValuePlus.ViewModel;



namespace ValuePlus.ViewModel.ReSM
{
    
    /// <summary>
    /// SMCity class
    /// </summary>
    //[MetadataType(typeof(SMCityViewModel))]
    //public  partial class SMCity
    //{
    
    	/// <summary>
    	/// SMCity Metadata class
    	/// </summary>
    	public   class SMCityViewModel:ViewModelBase
    	{
    		    
    		/// <summary>
    		/// HQID
    		/// </summary>        
    	//    [DisplayName("HQID")]
            [Required(ErrorMessage = "HQID is required")]
    		public int  HQID { get; set; }
    
    		    
    		/// <summary>
    		/// City Code
    		/// </summary>        
    	//    [DisplayName("City Code")]
            [Required(ErrorMessage = "City Code is required")]
            [MaxLength(5, ErrorMessage = "City Code cannot be longer than 5 characters")]
    		public string  CityCode { get; set; }
    
    		    
    		/// <summary>
    		/// Country ID
    		/// </summary>        
    	//    [DisplayName("Country ID")]
            [Required(ErrorMessage = "Country ID is required")]
    		public int  CountryID { get; set; }
    
    		    
    		/// <summary>
    		/// District ID
    		/// </summary>        
    	//    [DisplayName("District ID")]
    		public Nullable<int>  DistrictID { get; set; }
    
    		    
    		/// <summary>
    		/// State ID
    		/// </summary>        
    	//    [DisplayName("State ID")]
            [Required(ErrorMessage = "State ID is required")]
    		public int  StateID { get; set; }
    
    		    
    		/// <summary>
    		/// City Name
    		/// </summary>        
    	//    [DisplayName("City Name")]
            [MaxLength(255, ErrorMessage = "City Name cannot be longer than 255 characters")]
    		public string  CityName { get; set; }
    
    		    
    		/// <summary>
    		/// City Local Name
    		/// </summary>        
    	//    [DisplayName("City Local Name")]
            [MaxLength(255, ErrorMessage = "City Local Name cannot be longer than 255 characters")]
    		public string  CityLocalName { get; set; }
    
    		    
    		/// <summary>
    		/// City Phone
    		/// </summary>        
    	//    [DisplayName("City Phone")]
            [MaxLength(50, ErrorMessage = "City Phone cannot be longer than 50 characters")]
    		public string  CityPhone { get; set; }
    
    		    
    		/// <summary>
    		/// Area ID
    		/// </summary>        
    	//    [DisplayName("Area ID")]
    		public Nullable<int>  AreaID { get; set; }
    
    		    
    		/// <summary>
    		/// Status
    		/// </summary>        
    	//    [DisplayName("Status")]
            [Required(ErrorMessage = "Status is required")]
            [MaxLength(10, ErrorMessage = "Status cannot be longer than 10 characters")]
    		public string  Status { get; set; }
    
    		    
    		/// <summary>
    		/// Created By
    		/// </summary>        
    	//    [DisplayName("Created By")]
            [MaxLength(6, ErrorMessage = "Created By cannot be longer than 6 characters")]
    		public string  CreatedBy { get; set; }
    
    		    
    		/// <summary>
    		/// Created Date
    		/// </summary>        
    	//    [DisplayName("Created Date")]
            [Required(ErrorMessage = "Created Date is required")]
    		public System.DateTime  CreatedDate { get; set; }
    
    		    
    		/// <summary>
    		/// Updated By
    		/// </summary>        
    	//    [DisplayName("Updated By")]
            [MaxLength(6, ErrorMessage = "Updated By cannot be longer than 6 characters")]
    		public string  UpdatedBy { get; set; }
    
    		    
    		/// <summary>
    		/// Updated Date
    		/// </summary>        
    	//    [DisplayName("Updated Date")]
    		public Nullable<System.DateTime>  UpdatedDate { get; set; }
    
    		    
    		/// <summary>
    		/// Version
    		/// </summary>        
    	//    [DisplayName("Version")]
            [Required(ErrorMessage = "Version is required")]
            [MaxLength(8, ErrorMessage = "Version cannot be longer than 8 characters")]
    		public byte[]  Version { get; set; }
    
    		    
    		/// <summary>
    		/// IATACode
    		/// </summary>        
    	//    [DisplayName("IATACode")]
            [MaxLength(5, ErrorMessage = "IATACode cannot be longer than 5 characters")]
    		public string  IATACode { get; set; }
    
    		    
    		/// <summary>
    		/// CALoc Code
    		/// </summary>        
    	//    [DisplayName("CALoc Code")]
            [MaxLength(50, ErrorMessage = "CALoc Code cannot be longer than 50 characters")]
    		public string  CALocCode { get; set; }
    
    		    
    		/// <summary>
    		/// UNCity Code
    		/// </summary>        
    	//    [DisplayName("UNCity Code")]
            [MaxLength(10, ErrorMessage = "UNCity Code cannot be longer than 10 characters")]
    		public string  UNCityCode { get; set; }
    
    		    
    	}
    //}
    
}
