using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ValuePlus.ViewModel;



namespace ValuePlus.ViewModel.ReSM
{
    
    /// <summary>
    /// SMOceanCarrier class
    /// </summary>
    //[MetadataType(typeof(SMOceanCarrierViewModel))]
    //public  partial class SMOceanCarrier
    //{
    
    	/// <summary>
    	/// SMOceanCarrier Metadata class
    	/// </summary>
    	public   class SMOceanCarrierViewModel:ViewModelBase
    	{
    		    
    		/// <summary>
    		/// HQID
    		/// </summary>        
    	//    [DisplayName("HQID")]
            [Required(ErrorMessage = "HQID is required")]
    		public int  HQID { get; set; }
    
    		    
    		/// <summary>
    		/// Station ID
    		/// </summary>        
    	//    [DisplayName("Station ID")]
            [Required(ErrorMessage = "Station ID is required")]
            [MaxLength(3, ErrorMessage = "Station ID cannot be longer than 3 characters")]
    		public string  StationID { get; set; }
    
    		    
    		/// <summary>
    		/// Ocean Carrier Code
    		/// </summary>        
    	//    [DisplayName("Ocean Carrier Code")]
            [Required(ErrorMessage = "Ocean Carrier Code is required")]
            [MaxLength(10, ErrorMessage = "Ocean Carrier Code cannot be longer than 10 characters")]
    		public string  OceanCarrierCode { get; set; }
    
    		    
    		/// <summary>
    		/// Customer ID
    		/// </summary>        
    	//    [DisplayName("Customer ID")]
    		public Nullable<int>  CustomerID { get; set; }
    
    		    
    		/// <summary>
    		/// City ID
    		/// </summary>        
    	//    [DisplayName("City ID")]
    		public Nullable<int>  CityID { get; set; }
    
    		    
    		/// <summary>
    		/// Ocean Carrier Name
    		/// </summary>        
    	//    [DisplayName("Ocean Carrier Name")]
            [Required(ErrorMessage = "Ocean Carrier Name is required")]
            [MaxLength(255, ErrorMessage = "Ocean Carrier Name cannot be longer than 255 characters")]
    		public string  OceanCarrierName { get; set; }
    
    		    
    		/// <summary>
    		/// Ocean Carrier Name1
    		/// </summary>        
    	//    [DisplayName("Ocean Carrier Name1")]
            [MaxLength(255, ErrorMessage = "Ocean Carrier Name1 cannot be longer than 255 characters")]
    		public string  OceanCarrierName1 { get; set; }
    
    		    
    		/// <summary>
    		/// Ocean Carrier Address1
    		/// </summary>        
    	//    [DisplayName("Ocean Carrier Address1")]
            [MaxLength(255, ErrorMessage = "Ocean Carrier Address1 cannot be longer than 255 characters")]
    		public string  OceanCarrierAddress1 { get; set; }
    
    		    
    		/// <summary>
    		/// Ocean Carrier Address2
    		/// </summary>        
    	//    [DisplayName("Ocean Carrier Address2")]
            [MaxLength(255, ErrorMessage = "Ocean Carrier Address2 cannot be longer than 255 characters")]
    		public string  OceanCarrierAddress2 { get; set; }
    
    		    
    		/// <summary>
    		/// Ocean Carrier Address3
    		/// </summary>        
    	//    [DisplayName("Ocean Carrier Address3")]
            [MaxLength(255, ErrorMessage = "Ocean Carrier Address3 cannot be longer than 255 characters")]
    		public string  OceanCarrierAddress3 { get; set; }
    
    		    
    		/// <summary>
    		/// Ocean Carrier Address4
    		/// </summary>        
    	//    [DisplayName("Ocean Carrier Address4")]
            [MaxLength(255, ErrorMessage = "Ocean Carrier Address4 cannot be longer than 255 characters")]
    		public string  OceanCarrierAddress4 { get; set; }
    
    		    
    		/// <summary>
    		/// Ocean Carrier Address5
    		/// </summary>        
    	//    [DisplayName("Ocean Carrier Address5")]
            [MaxLength(255, ErrorMessage = "Ocean Carrier Address5 cannot be longer than 255 characters")]
    		public string  OceanCarrierAddress5 { get; set; }
    
    		    
    		/// <summary>
    		/// Ocean Carrier Phone
    		/// </summary>        
    	//    [DisplayName("Ocean Carrier Phone")]
            [MaxLength(50, ErrorMessage = "Ocean Carrier Phone cannot be longer than 50 characters")]
    		public string  OceanCarrierPhone { get; set; }
    
    		    
    		/// <summary>
    		/// Ocean Carrier Phone Ext
    		/// </summary>        
    	//    [DisplayName("Ocean Carrier Phone Ext")]
            [MaxLength(10, ErrorMessage = "Ocean Carrier Phone Ext cannot be longer than 10 characters")]
    		public string  OceanCarrierPhoneExt { get; set; }
    
    		    
    		/// <summary>
    		/// Ocean Carrier Fax
    		/// </summary>        
    	//    [DisplayName("Ocean Carrier Fax")]
            [MaxLength(50, ErrorMessage = "Ocean Carrier Fax cannot be longer than 50 characters")]
    		public string  OceanCarrierFax { get; set; }
    
    		    
    		/// <summary>
    		/// Ocean Carrier Fax Ext
    		/// </summary>        
    	//    [DisplayName("Ocean Carrier Fax Ext")]
            [MaxLength(10, ErrorMessage = "Ocean Carrier Fax Ext cannot be longer than 10 characters")]
    		public string  OceanCarrierFaxExt { get; set; }
    
    		    
    		/// <summary>
    		/// Ocean Carrier Zip
    		/// </summary>        
    	//    [DisplayName("Ocean Carrier Zip")]
            [MaxLength(10, ErrorMessage = "Ocean Carrier Zip cannot be longer than 10 characters")]
    		public string  OceanCarrierZip { get; set; }
    
    		    
    		/// <summary>
    		/// Ocean Carrier Web Site
    		/// </summary>        
    	//    [DisplayName("Ocean Carrier Web Site")]
            [MaxLength(255, ErrorMessage = "Ocean Carrier Web Site cannot be longer than 255 characters")]
    		public string  OceanCarrierWebSite { get; set; }
    
    		    
    		/// <summary>
    		/// Status
    		/// </summary>        
    	//    [DisplayName("Status")]
            [Required(ErrorMessage = "Status is required")]
            [MaxLength(10, ErrorMessage = "Status cannot be longer than 10 characters")]
    		public string  Status { get; set; }
    
    		    
    		/// <summary>
    		/// Created By
    		/// </summary>        
    	//    [DisplayName("Created By")]
            [MaxLength(6, ErrorMessage = "Created By cannot be longer than 6 characters")]
    		public string  CreatedBy { get; set; }
    
    		    
    		/// <summary>
    		/// Created Date
    		/// </summary>        
    	//    [DisplayName("Created Date")]
            [Required(ErrorMessage = "Created Date is required")]
    		public System.DateTime  CreatedDate { get; set; }
    
    		    
    		/// <summary>
    		/// Updated By
    		/// </summary>        
    	//    [DisplayName("Updated By")]
            [MaxLength(6, ErrorMessage = "Updated By cannot be longer than 6 characters")]
    		public string  UpdatedBy { get; set; }
    
    		    
    		/// <summary>
    		/// Updated Date
    		/// </summary>        
    	//    [DisplayName("Updated Date")]
    		public Nullable<System.DateTime>  UpdatedDate { get; set; }
    
    		    
    		/// <summary>
    		/// Version
    		/// </summary>        
    	//    [DisplayName("Version")]
            [Required(ErrorMessage = "Version is required")]
            [MaxLength(8, ErrorMessage = "Version cannot be longer than 8 characters")]
    		public byte[]  Version { get; set; }
    
    		    
    		/// <summary>
    		/// SCAC
    		/// </summary>        
    	//    [DisplayName("SCAC")]
            [MaxLength(50, ErrorMessage = "SCAC cannot be longer than 50 characters")]
    		public string  SCAC { get; set; }
    
    		    
    	}
    //}
    
}
