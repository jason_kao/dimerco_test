using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ValuePlus.ViewModel;



namespace ValuePlus.ViewModel.ReSM
{
    
    /// <summary>
    /// SMLELevel1 class
    /// </summary>
    //[MetadataType(typeof(SMLELevel1ViewModel))]
    //public  partial class SMLELevel1
    //{
    
    	/// <summary>
    	/// SMLELevel1 Metadata class
    	/// </summary>
    	public   class SMLELevel1ViewModel:ViewModelBase
    	{
    		    
    		/// <summary>
    		/// HQID
    		/// </summary>        
    	//    [DisplayName("HQID")]
            [Required(ErrorMessage = "HQID is required")]
    		public int  HQID { get; set; }
    
    		    
    		/// <summary>
    		/// Level1 Code
    		/// </summary>        
    	//    [DisplayName("Level1 Code")]
            [Required(ErrorMessage = "Level1 Code is required")]
            [MaxLength(10, ErrorMessage = "Level1 Code cannot be longer than 10 characters")]
    		public string  Level1Code { get; set; }
    
    		    
    		/// <summary>
    		/// LERegion ID
    		/// </summary>        
    	//    [DisplayName("LERegion ID")]
            [Required(ErrorMessage = "LERegion ID is required")]
    		public int  LERegionID { get; set; }
    
    		    
    		/// <summary>
    		/// Level1 Name
    		/// </summary>        
    	//    [DisplayName("Level1 Name")]
            [MaxLength(255, ErrorMessage = "Level1 Name cannot be longer than 255 characters")]
    		public string  Level1Name { get; set; }
    
    		    
    		/// <summary>
    		/// Status
    		/// </summary>        
    	//    [DisplayName("Status")]
            [Required(ErrorMessage = "Status is required")]
            [MaxLength(10, ErrorMessage = "Status cannot be longer than 10 characters")]
    		public string  Status { get; set; }
    
    		    
    		/// <summary>
    		/// Created By
    		/// </summary>        
    	//    [DisplayName("Created By")]
            [MaxLength(6, ErrorMessage = "Created By cannot be longer than 6 characters")]
    		public string  CreatedBy { get; set; }
    
    		    
    		/// <summary>
    		/// Created Date
    		/// </summary>        
    	//    [DisplayName("Created Date")]
            [Required(ErrorMessage = "Created Date is required")]
    		public System.DateTime  CreatedDate { get; set; }
    
    		    
    		/// <summary>
    		/// Updated By
    		/// </summary>        
    	//    [DisplayName("Updated By")]
            [MaxLength(6, ErrorMessage = "Updated By cannot be longer than 6 characters")]
    		public string  UpdatedBy { get; set; }
    
    		    
    		/// <summary>
    		/// Updated Date
    		/// </summary>        
    	//    [DisplayName("Updated Date")]
    		public Nullable<System.DateTime>  UpdatedDate { get; set; }
    
    		    
    		/// <summary>
    		/// Version
    		/// </summary>        
    	//    [DisplayName("Version")]
            [Required(ErrorMessage = "Version is required")]
            [MaxLength(8, ErrorMessage = "Version cannot be longer than 8 characters")]
    		public byte[]  Version { get; set; }
    
    		    
    	}
    //}
    
}
