using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ValuePlus.ViewModel;



namespace ValuePlus.ViewModel.ReSM
{
    
    /// <summary>
    /// SMCreditTerm class
    /// </summary>
    //[MetadataType(typeof(SMCreditTermViewModel))]
    //public  partial class SMCreditTerm
    //{
    
    	/// <summary>
    	/// SMCreditTerm Metadata class
    	/// </summary>
    	public   class SMCreditTermViewModel:ViewModelBase
    	{
    		    
    		/// <summary>
    		/// HQID
    		/// </summary>        
    	//    [DisplayName("HQID")]
            [Required(ErrorMessage = "HQID is required")]
    		public int  HQID { get; set; }
    
    		    
    		/// <summary>
    		/// Credit Term Code
    		/// </summary>        
    	//    [DisplayName("Credit Term Code")]
            [Required(ErrorMessage = "Credit Term Code is required")]
            [MaxLength(10, ErrorMessage = "Credit Term Code cannot be longer than 10 characters")]
    		public string  CreditTermCode { get; set; }
    
    		    
    		/// <summary>
    		/// Credit Term Name
    		/// </summary>        
    	//    [DisplayName("Credit Term Name")]
            [MaxLength(255, ErrorMessage = "Credit Term Name cannot be longer than 255 characters")]
    		public string  CreditTermName { get; set; }
    
    		    
    		/// <summary>
    		/// Days
    		/// </summary>        
    	//    [DisplayName("Days")]
    		public Nullable<int>  Days { get; set; }
    
    		    
    		/// <summary>
    		/// Status
    		/// </summary>        
    	//    [DisplayName("Status")]
            [Required(ErrorMessage = "Status is required")]
            [MaxLength(10, ErrorMessage = "Status cannot be longer than 10 characters")]
    		public string  Status { get; set; }
    
    		    
    		/// <summary>
    		/// Created By
    		/// </summary>        
    	//    [DisplayName("Created By")]
            [MaxLength(6, ErrorMessage = "Created By cannot be longer than 6 characters")]
    		public string  CreatedBy { get; set; }
    
    		    
    		/// <summary>
    		/// Created Date
    		/// </summary>        
    	//    [DisplayName("Created Date")]
            [Required(ErrorMessage = "Created Date is required")]
    		public System.DateTime  CreatedDate { get; set; }
    
    		    
    		/// <summary>
    		/// Updated By
    		/// </summary>        
    	//    [DisplayName("Updated By")]
            [MaxLength(6, ErrorMessage = "Updated By cannot be longer than 6 characters")]
    		public string  UpdatedBy { get; set; }
    
    		    
    		/// <summary>
    		/// Updated Date
    		/// </summary>        
    	//    [DisplayName("Updated Date")]
    		public Nullable<System.DateTime>  UpdatedDate { get; set; }
    
    		    
    		/// <summary>
    		/// Version
    		/// </summary>        
    	//    [DisplayName("Version")]
            [Required(ErrorMessage = "Version is required")]
            [MaxLength(8, ErrorMessage = "Version cannot be longer than 8 characters")]
    		public byte[]  Version { get; set; }
    
    		    
    	}
    //}
    
}
