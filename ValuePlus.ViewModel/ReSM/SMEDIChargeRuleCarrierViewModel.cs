using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ValuePlus.ViewModel;



namespace ValuePlus.ViewModel.ReSM
{
    
    /// <summary>
    /// SMEDIChargeRuleCarrier class
    /// </summary>
    //[MetadataType(typeof(SMEDIChargeRuleCarrierViewModel))]
    //public  partial class SMEDIChargeRuleCarrier
    //{
    
    	/// <summary>
    	/// SMEDIChargeRuleCarrier Metadata class
    	/// </summary>
    	public   class SMEDIChargeRuleCarrierViewModel:ViewModelBase
    	{
    		    
    		/// <summary>
    		/// HQID
    		/// </summary>        
    	//    [DisplayName("HQID")]
            [Required(ErrorMessage = "HQID is required")]
    		public int  HQID { get; set; }
    
    		    
    		/// <summary>
    		/// Rule ID
    		/// </summary>        
    	//    [DisplayName("Rule ID")]
    		public Nullable<int>  RuleID { get; set; }
    
    		    
    		/// <summary>
    		/// Air Line ID
    		/// </summary>        
    	//    [DisplayName("Air Line ID")]
    		public Nullable<int>  AirLineID { get; set; }
    
    		    
    		/// <summary>
    		/// Ail Line Code
    		/// </summary>        
    	//    [DisplayName("Ail Line Code")]
            [MaxLength(10, ErrorMessage = "Ail Line Code cannot be longer than 10 characters")]
    		public string  AilLineCode { get; set; }
    
    		    
    		/// <summary>
    		/// Status
    		/// </summary>        
    	//    [DisplayName("Status")]
            [MaxLength(50, ErrorMessage = "Status cannot be longer than 50 characters")]
    		public string  Status { get; set; }
    
    		    
    		/// <summary>
    		/// Created By
    		/// </summary>        
    	//    [DisplayName("Created By")]
            [MaxLength(10, ErrorMessage = "Created By cannot be longer than 10 characters")]
    		public string  CreatedBy { get; set; }
    
    		    
    		/// <summary>
    		/// Created Date
    		/// </summary>        
    	//    [DisplayName("Created Date")]
    		public Nullable<System.DateTime>  CreatedDate { get; set; }
    
    		    
    		/// <summary>
    		/// Updated By
    		/// </summary>        
    	//    [DisplayName("Updated By")]
            [MaxLength(10, ErrorMessage = "Updated By cannot be longer than 10 characters")]
    		public string  UpdatedBy { get; set; }
    
    		    
    		/// <summary>
    		/// Updated Date
    		/// </summary>        
    	//    [DisplayName("Updated Date")]
    		public Nullable<System.DateTime>  UpdatedDate { get; set; }
    
    		    
    	}
    //}
    
}
