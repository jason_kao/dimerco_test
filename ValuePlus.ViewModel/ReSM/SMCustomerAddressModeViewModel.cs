using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ValuePlus.ViewModel;



namespace ValuePlus.ViewModel.ReSM
{
    
    /// <summary>
    /// SMCustomerAddressMode class
    /// </summary>
    //[MetadataType(typeof(SMCustomerAddressModeViewModel))]
    //public  partial class SMCustomerAddressMode
    //{
    
    	/// <summary>
    	/// SMCustomerAddressMode Metadata class
    	/// </summary>
    	public   class SMCustomerAddressModeViewModel:ViewModelBase
    	{
    		    
    		/// <summary>
    		/// HQID
    		/// </summary>        
    	//    [DisplayName("HQID")]
            [Required(ErrorMessage = "HQID is required")]
    		public int  HQID { get; set; }
    
    		    
    		/// <summary>
    		/// Customer Address ID
    		/// </summary>        
    	//    [DisplayName("Customer Address ID")]
            [Required(ErrorMessage = "Customer Address ID is required")]
    		public int  CustomerAddressID { get; set; }
    
    		    
    		/// <summary>
    		/// Mode ID
    		/// </summary>        
    	//    [DisplayName("Mode ID")]
            [Required(ErrorMessage = "Mode ID is required")]
    		public int  ModeID { get; set; }
    
    		    
    		/// <summary>
    		/// Used
    		/// </summary>        
    	//    [DisplayName("Used")]
    		public Nullable<bool>  Used { get; set; }
    
    		    
    		/// <summary>
    		/// Status
    		/// </summary>        
    	//    [DisplayName("Status")]
            [Required(ErrorMessage = "Status is required")]
            [MaxLength(50, ErrorMessage = "Status cannot be longer than 50 characters")]
    		public string  Status { get; set; }
    
    		    
    		/// <summary>
    		/// Created By
    		/// </summary>        
    	//    [DisplayName("Created By")]
            [MaxLength(6, ErrorMessage = "Created By cannot be longer than 6 characters")]
    		public string  CreatedBy { get; set; }
    
    		    
    		/// <summary>
    		/// Created Date
    		/// </summary>        
    	//    [DisplayName("Created Date")]
            [Required(ErrorMessage = "Created Date is required")]
    		public System.DateTime  CreatedDate { get; set; }
    
    		    
    		/// <summary>
    		/// Updated By
    		/// </summary>        
    	//    [DisplayName("Updated By")]
            [MaxLength(6, ErrorMessage = "Updated By cannot be longer than 6 characters")]
    		public string  UpdatedBy { get; set; }
    
    		    
    		/// <summary>
    		/// Updated Date
    		/// </summary>        
    	//    [DisplayName("Updated Date")]
    		public Nullable<System.DateTime>  UpdatedDate { get; set; }
    
    		    
    	}
    //}
    
}
