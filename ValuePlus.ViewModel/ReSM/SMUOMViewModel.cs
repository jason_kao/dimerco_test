using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ValuePlus.ViewModel;



namespace ValuePlus.ViewModel.ReSM
{
    
    /// <summary>
    /// SMUOM class
    /// </summary>
    //[MetadataType(typeof(SMUOMViewModel))]
    //public  partial class SMUOM
    //{
    
    	/// <summary>
    	/// SMUOM Metadata class
    	/// </summary>
    	public   class SMUOMViewModel:ViewModelBase
    	{
    		    
    		/// <summary>
    		/// HQID
    		/// </summary>        
    	//    [DisplayName("HQID")]
            [Required(ErrorMessage = "HQID is required")]
    		public int  HQID { get; set; }
    
    		    
    		/// <summary>
    		/// UOMCode
    		/// </summary>        
    	//    [DisplayName("UOMCode")]
            [Required(ErrorMessage = "UOMCode is required")]
            [MaxLength(10, ErrorMessage = "UOMCode cannot be longer than 10 characters")]
    		public string  UOMCode { get; set; }
    
    		    
    		/// <summary>
    		/// UOMName
    		/// </summary>        
    	//    [DisplayName("UOMName")]
            [MaxLength(255, ErrorMessage = "UOMName cannot be longer than 255 characters")]
    		public string  UOMName { get; set; }
    
    		    
    		/// <summary>
    		/// Decimal
    		/// </summary>        
    	//    [DisplayName("Decimal")]
            [MaxLength(50, ErrorMessage = "Decimal cannot be longer than 50 characters")]
    		public string  Decimal { get; set; }
    
    		    
    		/// <summary>
    		/// Rounding
    		/// </summary>        
    	//    [DisplayName("Rounding")]
            [MaxLength(50, ErrorMessage = "Rounding cannot be longer than 50 characters")]
    		public string  Rounding { get; set; }
    
    		    
    		/// <summary>
    		/// UOMType
    		/// </summary>        
    	//    [DisplayName("UOMType")]
            [Required(ErrorMessage = "UOMType is required")]
            [MaxLength(20, ErrorMessage = "UOMType cannot be longer than 20 characters")]
    		public string  UOMType { get; set; }
    
    		    
    		/// <summary>
    		/// Status
    		/// </summary>        
    	//    [DisplayName("Status")]
            [MaxLength(10, ErrorMessage = "Status cannot be longer than 10 characters")]
    		public string  Status { get; set; }
    
    		    
    		/// <summary>
    		/// Created By
    		/// </summary>        
    	//    [DisplayName("Created By")]
            [MaxLength(6, ErrorMessage = "Created By cannot be longer than 6 characters")]
    		public string  CreatedBy { get; set; }
    
    		    
    		/// <summary>
    		/// Created Date
    		/// </summary>        
    	//    [DisplayName("Created Date")]
            [Required(ErrorMessage = "Created Date is required")]
    		public System.DateTime  CreatedDate { get; set; }
    
    		    
    		/// <summary>
    		/// Updated By
    		/// </summary>        
    	//    [DisplayName("Updated By")]
            [MaxLength(6, ErrorMessage = "Updated By cannot be longer than 6 characters")]
    		public string  UpdatedBy { get; set; }
    
    		    
    		/// <summary>
    		/// Updated Date
    		/// </summary>        
    	//    [DisplayName("Updated Date")]
    		public Nullable<System.DateTime>  UpdatedDate { get; set; }
    
    		    
    		/// <summary>
    		/// Version
    		/// </summary>        
    	//    [DisplayName("Version")]
            [Required(ErrorMessage = "Version is required")]
            [MaxLength(8, ErrorMessage = "Version cannot be longer than 8 characters")]
    		public byte[]  Version { get; set; }
    
    		    
    		/// <summary>
    		/// Height
    		/// </summary>        
    	//    [DisplayName("Height")]
    		public Nullable<decimal>  Height { get; set; }
    
    		    
    		/// <summary>
    		/// Length
    		/// </summary>        
    	//    [DisplayName("Length")]
    		public Nullable<decimal>  Length { get; set; }
    
    		    
    		/// <summary>
    		/// Width
    		/// </summary>        
    	//    [DisplayName("Width")]
    		public Nullable<decimal>  Width { get; set; }
    
    		    
    		/// <summary>
    		/// Gross Weight
    		/// </summary>        
    	//    [DisplayName("Gross Weight")]
    		public Nullable<decimal>  GrossWeight { get; set; }
    
    		    
    		/// <summary>
    		/// Tare Weight
    		/// </summary>        
    	//    [DisplayName("Tare Weight")]
    		public Nullable<decimal>  TareWeight { get; set; }
    
    		    
    	}
    //}
    
}
