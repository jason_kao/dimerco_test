using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ValuePlus.ViewModel;



namespace ValuePlus.ViewModel.ReSM
{
    
    /// <summary>
    /// SMChargeCodeGLCode class
    /// </summary>
    //[MetadataType(typeof(SMChargeCodeGLCodeViewModel))]
    //public  partial class SMChargeCodeGLCode
    //{
    
    	/// <summary>
    	/// SMChargeCodeGLCode Metadata class
    	/// </summary>
    	public   class SMChargeCodeGLCodeViewModel:ViewModelBase
    	{
    		    
    		/// <summary>
    		/// HQID
    		/// </summary>        
    	//    [DisplayName("HQID")]
            [Required(ErrorMessage = "HQID is required")]
    		public int  HQID { get; set; }
    
    		    
    		/// <summary>
    		/// Charge Code ID
    		/// </summary>        
    	//    [DisplayName("Charge Code ID")]
    		public Nullable<int>  ChargeCodeID { get; set; }
    
    		    
    		/// <summary>
    		/// Mode Code
    		/// </summary>        
    	//    [DisplayName("Mode Code")]
            [MaxLength(2, ErrorMessage = "Mode Code cannot be longer than 2 characters")]
    		public string  ModeCode { get; set; }
    
    		    
    		/// <summary>
    		/// SGLCode
    		/// </summary>        
    	//    [DisplayName("SGLCode")]
            [MaxLength(10, ErrorMessage = "SGLCode cannot be longer than 10 characters")]
    		public string  SGLCode { get; set; }
    
    		    
    		/// <summary>
    		/// CGLCode
    		/// </summary>        
    	//    [DisplayName("CGLCode")]
            [MaxLength(10, ErrorMessage = "CGLCode cannot be longer than 10 characters")]
    		public string  CGLCode { get; set; }
    
    		    
    		/// <summary>
    		/// Created By
    		/// </summary>        
    	//    [DisplayName("Created By")]
            [MaxLength(10, ErrorMessage = "Created By cannot be longer than 10 characters")]
    		public string  CreatedBy { get; set; }
    
    		    
    		/// <summary>
    		/// Created Date
    		/// </summary>        
    	//    [DisplayName("Created Date")]
    		public Nullable<System.DateTime>  CreatedDate { get; set; }
    
    		    
    		/// <summary>
    		/// Updated By
    		/// </summary>        
    	//    [DisplayName("Updated By")]
            [MaxLength(10, ErrorMessage = "Updated By cannot be longer than 10 characters")]
    		public string  UpdatedBy { get; set; }
    
    		    
    		/// <summary>
    		/// Updated Date
    		/// </summary>        
    	//    [DisplayName("Updated Date")]
    		public Nullable<System.DateTime>  UpdatedDate { get; set; }
    
    		    
    		/// <summary>
    		/// Version
    		/// </summary>        
    	//    [DisplayName("Version")]
            [MaxLength(8, ErrorMessage = "Version cannot be longer than 8 characters")]
    		public byte[]  Version { get; set; }
    
    		    
    	}
    //}
    
}
