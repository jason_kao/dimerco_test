using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ValuePlus.ViewModel;



namespace ValuePlus.ViewModel.ReSM
{
    
    /// <summary>
    /// SMReminderRule class
    /// </summary>
    //[MetadataType(typeof(SMReminderRuleViewModel))]
    //public  partial class SMReminderRule
    //{
    
    	/// <summary>
    	/// SMReminderRule Metadata class
    	/// </summary>
    	public   class SMReminderRuleViewModel:ViewModelBase
    	{
    		    
    		/// <summary>
    		/// HQID
    		/// </summary>        
    	//    [DisplayName("HQID")]
            [Required(ErrorMessage = "HQID is required")]
    		public int  HQID { get; set; }
    
    		    
    		/// <summary>
    		/// Name
    		/// </summary>        
    	//    [DisplayName("Name")]
            [MaxLength(50, ErrorMessage = "Name cannot be longer than 50 characters")]
    		public string  Name { get; set; }
    
    		    
    		/// <summary>
    		/// Code
    		/// </summary>        
    	//    [DisplayName("Code")]
            [MaxLength(10, ErrorMessage = "Code cannot be longer than 10 characters")]
    		public string  Code { get; set; }
    
    		    
    		/// <summary>
    		/// Type
    		/// </summary>        
    	//    [DisplayName("Type")]
            [MaxLength(1, ErrorMessage = "Type cannot be longer than 1 characters")]
    		public string  Type { get; set; }
    
    		    
    		/// <summary>
    		/// Description
    		/// </summary>        
    	//    [DisplayName("Description")]
            [MaxLength(100, ErrorMessage = "Description cannot be longer than 100 characters")]
    		public string  Description { get; set; }
    
    		    
    		/// <summary>
    		/// Category
    		/// </summary>        
    	//    [DisplayName("Category")]
    		public Nullable<int>  Category { get; set; }
    
    		    
    		/// <summary>
    		/// Severity
    		/// </summary>        
    	//    [DisplayName("Severity")]
    		public Nullable<int>  Severity { get; set; }
    
    		    
    		/// <summary>
    		/// PID
    		/// </summary>        
    	//    [DisplayName("PID")]
    		public Nullable<int>  PID { get; set; }
    
    		    
    		/// <summary>
    		/// Mode
    		/// </summary>        
    	//    [DisplayName("Mode")]
            [MaxLength(2, ErrorMessage = "Mode cannot be longer than 2 characters")]
    		public string  Mode { get; set; }
    
    		    
    		/// <summary>
    		/// Created By
    		/// </summary>        
    	//    [DisplayName("Created By")]
            [MaxLength(10, ErrorMessage = "Created By cannot be longer than 10 characters")]
    		public string  CreatedBy { get; set; }
    
    		    
    		/// <summary>
    		/// Created Date
    		/// </summary>        
    	//    [DisplayName("Created Date")]
    		public Nullable<System.DateTime>  CreatedDate { get; set; }
    
    		    
    		/// <summary>
    		/// Updated By
    		/// </summary>        
    	//    [DisplayName("Updated By")]
            [MaxLength(10, ErrorMessage = "Updated By cannot be longer than 10 characters")]
    		public string  UpdatedBy { get; set; }
    
    		    
    		/// <summary>
    		/// Updated Date
    		/// </summary>        
    	//    [DisplayName("Updated Date")]
    		public Nullable<System.DateTime>  UpdatedDate { get; set; }
    
    		    
    		/// <summary>
    		/// Status
    		/// </summary>        
    	//    [DisplayName("Status")]
            [MaxLength(20, ErrorMessage = "Status cannot be longer than 20 characters")]
    		public string  Status { get; set; }
    
    		    
    	}
    //}
    
}
