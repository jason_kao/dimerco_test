using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ValuePlus.ViewModel;



namespace ValuePlus.ViewModel.ReSM
{
    
    /// <summary>
    /// SMCurrency class
    /// </summary>
    //[MetadataType(typeof(SMCurrencyViewModel))]
    //public  partial class SMCurrency
    //{
    
    	/// <summary>
    	/// SMCurrency Metadata class
    	/// </summary>
    	public   class SMCurrencyViewModel:ViewModelBase
    	{
    		    
    		/// <summary>
    		/// HQID
    		/// </summary>        
    	//    [DisplayName("HQID")]
            [Required(ErrorMessage = "HQID is required")]
    		public int  HQID { get; set; }
    
    		    
    		/// <summary>
    		/// Currency Code
    		/// </summary>        
    	//    [DisplayName("Currency Code")]
            [Required(ErrorMessage = "Currency Code is required")]
            [MaxLength(5, ErrorMessage = "Currency Code cannot be longer than 5 characters")]
    		public string  CurrencyCode { get; set; }
    
    		    
    		/// <summary>
    		/// Currency Name
    		/// </summary>        
    	//    [DisplayName("Currency Name")]
            [Required(ErrorMessage = "Currency Name is required")]
            [MaxLength(255, ErrorMessage = "Currency Name cannot be longer than 255 characters")]
    		public string  CurrencyName { get; set; }
    
    		    
    		/// <summary>
    		/// Decimal
    		/// </summary>        
    	//    [DisplayName("Decimal")]
    		public Nullable<decimal>  Decimal { get; set; }
    
    		    
    		/// <summary>
    		/// Round Unit
    		/// </summary>        
    	//    [DisplayName("Round Unit")]
    		public Nullable<decimal>  RoundUnit { get; set; }
    
    		    
    		/// <summary>
    		/// Min Amount
    		/// </summary>        
    	//    [DisplayName("Min Amount")]
    		public Nullable<decimal>  MinAmount { get; set; }
    
    		    
    		/// <summary>
    		/// Round Up
    		/// </summary>        
    	//    [DisplayName("Round Up")]
    		public Nullable<decimal>  RoundUp { get; set; }
    
    		    
    		/// <summary>
    		/// Min Round Unit
    		/// </summary>        
    	//    [DisplayName("Min Round Unit")]
            [MaxLength(50, ErrorMessage = "Min Round Unit cannot be longer than 50 characters")]
    		public string  MinRoundUnit { get; set; }
    
    		    
    		/// <summary>
    		/// AMTDecimal
    		/// </summary>        
    	//    [DisplayName("AMTDecimal")]
            [MaxLength(50, ErrorMessage = "AMTDecimal cannot be longer than 50 characters")]
    		public string  AMTDecimal { get; set; }
    
    		    
    		/// <summary>
    		/// VATDecimal
    		/// </summary>        
    	//    [DisplayName("VATDecimal")]
            [MaxLength(50, ErrorMessage = "VATDecimal cannot be longer than 50 characters")]
    		public string  VATDecimal { get; set; }
    
    		    
    		/// <summary>
    		/// Currency Flag
    		/// </summary>        
    	//    [DisplayName("Currency Flag")]
            [MaxLength(50, ErrorMessage = "Currency Flag cannot be longer than 50 characters")]
    		public string  CurrencyFlag { get; set; }
    
    		    
    		/// <summary>
    		/// Status
    		/// </summary>        
    	//    [DisplayName("Status")]
            [Required(ErrorMessage = "Status is required")]
            [MaxLength(10, ErrorMessage = "Status cannot be longer than 10 characters")]
    		public string  Status { get; set; }
    
    		    
    		/// <summary>
    		/// Created By
    		/// </summary>        
    	//    [DisplayName("Created By")]
            [MaxLength(6, ErrorMessage = "Created By cannot be longer than 6 characters")]
    		public string  CreatedBy { get; set; }
    
    		    
    		/// <summary>
    		/// Created Date
    		/// </summary>        
    	//    [DisplayName("Created Date")]
            [Required(ErrorMessage = "Created Date is required")]
    		public System.DateTime  CreatedDate { get; set; }
    
    		    
    		/// <summary>
    		/// Updated By
    		/// </summary>        
    	//    [DisplayName("Updated By")]
            [MaxLength(6, ErrorMessage = "Updated By cannot be longer than 6 characters")]
    		public string  UpdatedBy { get; set; }
    
    		    
    		/// <summary>
    		/// Updated Date
    		/// </summary>        
    	//    [DisplayName("Updated Date")]
    		public Nullable<System.DateTime>  UpdatedDate { get; set; }
    
    		    
    		/// <summary>
    		/// Version
    		/// </summary>        
    	//    [DisplayName("Version")]
            [Required(ErrorMessage = "Version is required")]
            [MaxLength(8, ErrorMessage = "Version cannot be longer than 8 characters")]
    		public byte[]  Version { get; set; }
    
    		    
    		/// <summary>
    		/// Global Seq
    		/// </summary>        
    	//    [DisplayName("Global Seq")]
    		public Nullable<int>  GlobalSeq { get; set; }
    
    		    
    	}
    //}
    
}
