using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ValuePlus.ViewModel;



namespace ValuePlus.ViewModel.ReSM
{
    
    /// <summary>
    /// SMControlGroupDetail class
    /// </summary>
    //[MetadataType(typeof(SMControlGroupDetailViewModel))]
    //public  partial class SMControlGroupDetail
    //{
    
    	/// <summary>
    	/// SMControlGroupDetail Metadata class
    	/// </summary>
    	public   class SMControlGroupDetailViewModel:ViewModelBase
    	{
    		    
    		/// <summary>
    		/// HQID
    		/// </summary>        
    	//    [DisplayName("HQID")]
            [Required(ErrorMessage = "HQID is required")]
    		public int  HQID { get; set; }
    
    		    
    		/// <summary>
    		/// Control Group ID
    		/// </summary>        
    	//    [DisplayName("Control Group ID")]
    		public Nullable<int>  ControlGroupID { get; set; }
    
    		    
    		/// <summary>
    		/// Field ID
    		/// </summary>        
    	//    [DisplayName("Field ID")]
    		public Nullable<int>  FieldID { get; set; }
    
    		    
    		/// <summary>
    		/// Is Mandatory
    		/// </summary>        
    	//    [DisplayName("Is Mandatory")]
    		public Nullable<bool>  IsMandatory { get; set; }
    
    		    
    		/// <summary>
    		/// Map Name
    		/// </summary>        
    	//    [DisplayName("Map Name")]
            [MaxLength(50, ErrorMessage = "Map Name cannot be longer than 50 characters")]
    		public string  MapName { get; set; }
    
    		    
    		/// <summary>
    		/// Pick Up Data
    		/// </summary>        
    	//    [DisplayName("Pick Up Data")]
            [MaxLength(100, ErrorMessage = "Pick Up Data cannot be longer than 100 characters")]
    		public string  PickUpData { get; set; }
    
    		    
    		/// <summary>
    		/// Created By
    		/// </summary>        
    	//    [DisplayName("Created By")]
            [MaxLength(10, ErrorMessage = "Created By cannot be longer than 10 characters")]
    		public string  CreatedBy { get; set; }
    
    		    
    		/// <summary>
    		/// Created Date
    		/// </summary>        
    	//    [DisplayName("Created Date")]
    		public Nullable<System.DateTime>  CreatedDate { get; set; }
    
    		    
    		/// <summary>
    		/// Updated By
    		/// </summary>        
    	//    [DisplayName("Updated By")]
            [MaxLength(10, ErrorMessage = "Updated By cannot be longer than 10 characters")]
    		public string  UpdatedBy { get; set; }
    
    		    
    		/// <summary>
    		/// Updated Date
    		/// </summary>        
    	//    [DisplayName("Updated Date")]
    		public Nullable<System.DateTime>  UpdatedDate { get; set; }
    
    		    
    		/// <summary>
    		/// Issue Station
    		/// </summary>        
    	//    [DisplayName("Issue Station")]
            [MaxLength(100, ErrorMessage = "Issue Station cannot be longer than 100 characters")]
    		public string  IssueStation { get; set; }
    
    		    
    		/// <summary>
    		/// Bill To
    		/// </summary>        
    	//    [DisplayName("Bill To")]
            [MaxLength(100, ErrorMessage = "Bill To cannot be longer than 100 characters")]
    		public string  BillTo { get; set; }
    
    		    
    		/// <summary>
    		/// Charge Code
    		/// </summary>        
    	//    [DisplayName("Charge Code")]
            [MaxLength(100, ErrorMessage = "Charge Code cannot be longer than 100 characters")]
    		public string  ChargeCode { get; set; }
    
    		    
    		/// <summary>
    		/// is Include VAT
    		/// </summary>        
    	//    [DisplayName("is Include VAT")]
    		public Nullable<bool>  isIncludeVAT { get; set; }
    
    		    
    		/// <summary>
    		/// Description
    		/// </summary>        
    	//    [DisplayName("Description")]
            [MaxLength(250, ErrorMessage = "Description cannot be longer than 250 characters")]
    		public string  Description { get; set; }
    
    		    
    		/// <summary>
    		/// Show On Search
    		/// </summary>        
    	//    [DisplayName("Show On Search")]
    		public Nullable<bool>  ShowOnSearch { get; set; }
    
    		    
    		/// <summary>
    		/// Search Result Order
    		/// </summary>        
    	//    [DisplayName("Search Result Order")]
    		public Nullable<int>  SearchResultOrder { get; set; }
    
    		    
    	}
    //}
    
}
