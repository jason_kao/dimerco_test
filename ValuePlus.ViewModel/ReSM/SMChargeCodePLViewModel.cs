using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ValuePlus.ViewModel;



namespace ValuePlus.ViewModel.ReSM
{
    
    /// <summary>
    /// SMChargeCodePL class
    /// </summary>
    //[MetadataType(typeof(SMChargeCodePLViewModel))]
    //public  partial class SMChargeCodePL
    //{
    
    	/// <summary>
    	/// SMChargeCodePL Metadata class
    	/// </summary>
    	public   class SMChargeCodePLViewModel:ViewModelBase
    	{
    		    
    		/// <summary>
    		/// HQID
    		/// </summary>        
    	//    [DisplayName("HQID")]
            [Required(ErrorMessage = "HQID is required")]
    		public int  HQID { get; set; }
    
    		    
    		/// <summary>
    		/// Charge Code ID
    		/// </summary>        
    	//    [DisplayName("Charge Code ID")]
            [Required(ErrorMessage = "Charge Code ID is required")]
    		public int  ChargeCodeID { get; set; }
    
    		    
    		/// <summary>
    		/// Product Line ID
    		/// </summary>        
    	//    [DisplayName("Product Line ID")]
            [Required(ErrorMessage = "Product Line ID is required")]
    		public int  ProductLineID { get; set; }
    
    		    
    		/// <summary>
    		/// Export Type
    		/// </summary>        
    	//    [DisplayName("Export Type")]
            [MaxLength(50, ErrorMessage = "Export Type cannot be longer than 50 characters")]
    		public string  ExportType { get; set; }
    
    		    
    		/// <summary>
    		/// Import Type
    		/// </summary>        
    	//    [DisplayName("Import Type")]
            [MaxLength(50, ErrorMessage = "Import Type cannot be longer than 50 characters")]
    		public string  ImportType { get; set; }
    
    		    
    		/// <summary>
    		/// Status
    		/// </summary>        
    	//    [DisplayName("Status")]
            [Required(ErrorMessage = "Status is required")]
            [MaxLength(10, ErrorMessage = "Status cannot be longer than 10 characters")]
    		public string  Status { get; set; }
    
    		    
    		/// <summary>
    		/// Created By
    		/// </summary>        
    	//    [DisplayName("Created By")]
            [MaxLength(6, ErrorMessage = "Created By cannot be longer than 6 characters")]
    		public string  CreatedBy { get; set; }
    
    		    
    		/// <summary>
    		/// Created Date
    		/// </summary>        
    	//    [DisplayName("Created Date")]
            [Required(ErrorMessage = "Created Date is required")]
    		public System.DateTime  CreatedDate { get; set; }
    
    		    
    		/// <summary>
    		/// Updated By
    		/// </summary>        
    	//    [DisplayName("Updated By")]
            [MaxLength(6, ErrorMessage = "Updated By cannot be longer than 6 characters")]
    		public string  UpdatedBy { get; set; }
    
    		    
    		/// <summary>
    		/// Updated Date
    		/// </summary>        
    	//    [DisplayName("Updated Date")]
    		public Nullable<System.DateTime>  UpdatedDate { get; set; }
    
    		    
    		/// <summary>
    		/// Version
    		/// </summary>        
    	//    [DisplayName("Version")]
            [MaxLength(8, ErrorMessage = "Version cannot be longer than 8 characters")]
    		public byte[]  Version { get; set; }
    
    		    
    		/// <summary>
    		/// VATPP
    		/// </summary>        
    	//    [DisplayName("VATPP")]
            [MaxLength(2, ErrorMessage = "VATPP cannot be longer than 2 characters")]
    		public string  VATPP { get; set; }
    
    		    
    		/// <summary>
    		/// VATCC
    		/// </summary>        
    	//    [DisplayName("VATCC")]
            [MaxLength(2, ErrorMessage = "VATCC cannot be longer than 2 characters")]
    		public string  VATCC { get; set; }
    
    		    
    		/// <summary>
    		/// Vat Cost
    		/// </summary>        
    	//    [DisplayName("Vat Cost")]
            [MaxLength(2, ErrorMessage = "Vat Cost cannot be longer than 2 characters")]
    		public string  VatCost { get; set; }
    
    		    
    		/// <summary>
    		/// SGLCode
    		/// </summary>        
    	//    [DisplayName("SGLCode")]
            [MaxLength(10, ErrorMessage = "SGLCode cannot be longer than 10 characters")]
    		public string  SGLCode { get; set; }
    
    		    
    		/// <summary>
    		/// CGLCode
    		/// </summary>        
    	//    [DisplayName("CGLCode")]
            [MaxLength(10, ErrorMessage = "CGLCode cannot be longer than 10 characters")]
    		public string  CGLCode { get; set; }
    
    		    
    		/// <summary>
    		/// UOMType
    		/// </summary>        
    	//    [DisplayName("UOMType")]
            [MaxLength(50, ErrorMessage = "UOMType cannot be longer than 50 characters")]
    		public string  UOMType { get; set; }
    
    		    
    		/// <summary>
    		/// Is Used
    		/// </summary>        
    	//    [DisplayName("Is Used")]
    		public Nullable<bool>  IsUsed { get; set; }
    
    		    
    		/// <summary>
    		/// UOMType2
    		/// </summary>        
    	//    [DisplayName("UOMType2")]
            [MaxLength(50, ErrorMessage = "UOMType2 cannot be longer than 50 characters")]
    		public string  UOMType2 { get; set; }
    
    		    
    	}
    //}
    
}
