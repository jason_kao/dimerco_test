using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ValuePlus.ViewModel;



namespace ValuePlus.ViewModel.ReSM
{
    
    /// <summary>
    /// SMMilestoneHistory class
    /// </summary>
    //[MetadataType(typeof(SMMilestoneHistoryViewModel))]
    //public  partial class SMMilestoneHistory
    //{
    
    	/// <summary>
    	/// SMMilestoneHistory Metadata class
    	/// </summary>
    	public   class SMMilestoneHistoryViewModel:ViewModelBase
    	{
    		    
    		/// <summary>
    		/// ID
    		/// </summary>        
    	//    [DisplayName("ID")]
            [Required(ErrorMessage = "ID is required")]
    		public int  ID { get; set; }
    
    		    
    		/// <summary>
    		/// Station ID
    		/// </summary>        
    	//    [DisplayName("Station ID")]
            [Required(ErrorMessage = "Station ID is required")]
            [MaxLength(6, ErrorMessage = "Station ID cannot be longer than 6 characters")]
    		public string  StationID { get; set; }
    
    		    
    		/// <summary>
    		/// key Value
    		/// </summary>        
    	//    [DisplayName("key Value")]
            [MaxLength(20, ErrorMessage = "key Value cannot be longer than 20 characters")]
    		public string  keyValue { get; set; }
    
    		    
    		/// <summary>
    		/// Productline
    		/// </summary>        
    	//    [DisplayName("Productline")]
    		public Nullable<int>  Productline { get; set; }
    
    		    
    		/// <summary>
    		/// Milestone ID
    		/// </summary>        
    	//    [DisplayName("Milestone ID")]
    		public Nullable<int>  MilestoneID { get; set; }
    
    		    
    		/// <summary>
    		/// Milestone Time
    		/// </summary>        
    	//    [DisplayName("Milestone Time")]
    		public Nullable<System.DateTime>  MilestoneTime { get; set; }
    
    		    
    		/// <summary>
    		/// Irr Reason
    		/// </summary>        
    	//    [DisplayName("Irr Reason")]
            [MaxLength(255, ErrorMessage = "Irr Reason cannot be longer than 255 characters")]
    		public string  IrrReason { get; set; }
    
    		    
    		/// <summary>
    		/// Approved By
    		/// </summary>        
    	//    [DisplayName("Approved By")]
            [MaxLength(5, ErrorMessage = "Approved By cannot be longer than 5 characters")]
    		public string  ApprovedBy { get; set; }
    
    		    
    		/// <summary>
    		/// Extra Value1
    		/// </summary>        
    	//    [DisplayName("Extra Value1")]
            [MaxLength(50, ErrorMessage = "Extra Value1 cannot be longer than 50 characters")]
    		public string  ExtraValue1 { get; set; }
    
    		    
    		/// <summary>
    		/// Extra Value2
    		/// </summary>        
    	//    [DisplayName("Extra Value2")]
            [MaxLength(50, ErrorMessage = "Extra Value2 cannot be longer than 50 characters")]
    		public string  ExtraValue2 { get; set; }
    
    		    
    		/// <summary>
    		/// UTCOff Set
    		/// </summary>        
    	//    [DisplayName("UTCOff Set")]
    		public Nullable<int>  UTCOffSet { get; set; }
    
    		    
    		/// <summary>
    		/// Status
    		/// </summary>        
    	//    [DisplayName("Status")]
    		public Nullable<int>  Status { get; set; }
    
    		    
    		/// <summary>
    		/// Created DT
    		/// </summary>        
    	//    [DisplayName("Created DT")]
    		public Nullable<System.DateTime>  CreatedDT { get; set; }
    
    		    
    		/// <summary>
    		/// Created By
    		/// </summary>        
    	//    [DisplayName("Created By")]
            [MaxLength(5, ErrorMessage = "Created By cannot be longer than 5 characters")]
    		public string  CreatedBy { get; set; }
    
    		    
    	}
    //}
    
}
