using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ValuePlus.ViewModel;



namespace ValuePlus.ViewModel.ReSM
{
    
    /// <summary>
    /// SMeAWBAddressSetting class
    /// </summary>
    //[MetadataType(typeof(SMeAWBAddressSettingViewModel))]
    //public  partial class SMeAWBAddressSetting
    //{
    
    	/// <summary>
    	/// SMeAWBAddressSetting Metadata class
    	/// </summary>
    	public   class SMeAWBAddressSettingViewModel:ViewModelBase
    	{
    		    
    		/// <summary>
    		/// HQID
    		/// </summary>        
    	//    [DisplayName("HQID")]
            [Required(ErrorMessage = "HQID is required")]
    		public int  HQID { get; set; }
    
    		    
    		/// <summary>
    		/// From Place Type
    		/// </summary>        
    	//    [DisplayName("From Place Type")]
            [Required(ErrorMessage = "From Place Type is required")]
            [MaxLength(10, ErrorMessage = "From Place Type cannot be longer than 10 characters")]
    		public string  FromPlaceType { get; set; }
    
    		    
    		/// <summary>
    		/// To Place Type
    		/// </summary>        
    	//    [DisplayName("To Place Type")]
            [Required(ErrorMessage = "To Place Type is required")]
            [MaxLength(10, ErrorMessage = "To Place Type cannot be longer than 10 characters")]
    		public string  ToPlaceType { get; set; }
    
    		    
    		/// <summary>
    		/// From Station
    		/// </summary>        
    	//    [DisplayName("From Station")]
            [Required(ErrorMessage = "From Station is required")]
            [MaxLength(6, ErrorMessage = "From Station cannot be longer than 6 characters")]
    		public string  FromStation { get; set; }
    
    		    
    		/// <summary>
    		/// To Station
    		/// </summary>        
    	//    [DisplayName("To Station")]
            [Required(ErrorMessage = "To Station is required")]
            [MaxLength(6, ErrorMessage = "To Station cannot be longer than 6 characters")]
    		public string  ToStation { get; set; }
    
    		    
    		/// <summary>
    		/// Relate Air Line
    		/// </summary>        
    	//    [DisplayName("Relate Air Line")]
            [Required(ErrorMessage = "Relate Air Line is required")]
            [MaxLength(500, ErrorMessage = "Relate Air Line cannot be longer than 500 characters")]
    		public string  RelateAirLine { get; set; }
    
    		    
    		/// <summary>
    		/// Status
    		/// </summary>        
    	//    [DisplayName("Status")]
            [Required(ErrorMessage = "Status is required")]
            [MaxLength(10, ErrorMessage = "Status cannot be longer than 10 characters")]
    		public string  Status { get; set; }
    
    		    
    		/// <summary>
    		/// Created By
    		/// </summary>        
    	//    [DisplayName("Created By")]
            [MaxLength(6, ErrorMessage = "Created By cannot be longer than 6 characters")]
    		public string  CreatedBy { get; set; }
    
    		    
    		/// <summary>
    		/// Created Date
    		/// </summary>        
    	//    [DisplayName("Created Date")]
            [Required(ErrorMessage = "Created Date is required")]
    		public System.DateTime  CreatedDate { get; set; }
    
    		    
    		/// <summary>
    		/// Updated By
    		/// </summary>        
    	//    [DisplayName("Updated By")]
            [MaxLength(6, ErrorMessage = "Updated By cannot be longer than 6 characters")]
    		public string  UpdatedBy { get; set; }
    
    		    
    		/// <summary>
    		/// Updated Date
    		/// </summary>        
    	//    [DisplayName("Updated Date")]
    		public Nullable<System.DateTime>  UpdatedDate { get; set; }
    
    		    
    	}
    //}
    
}
