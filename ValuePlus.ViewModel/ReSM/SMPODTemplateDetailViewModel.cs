using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ValuePlus.ViewModel;



namespace ValuePlus.ViewModel.ReSM
{
    
    /// <summary>
    /// SMPODTemplateDetail class
    /// </summary>
    //[MetadataType(typeof(SMPODTemplateDetailViewModel))]
    //public  partial class SMPODTemplateDetail
    //{
    
    	/// <summary>
    	/// SMPODTemplateDetail Metadata class
    	/// </summary>
    	public   class SMPODTemplateDetailViewModel:ViewModelBase
    	{
    		    
    		/// <summary>
    		/// HQID
    		/// </summary>        
    	//    [DisplayName("HQID")]
            [Required(ErrorMessage = "HQID is required")]
    		public int  HQID { get; set; }
    
    		    
    		/// <summary>
    		/// PODTEMPLID
    		/// </summary>        
    	//    [DisplayName("PODTEMPLID")]
            [Required(ErrorMessage = "PODTEMPLID is required")]
    		public int  PODTEMPLID { get; set; }
    
    		    
    		/// <summary>
    		/// Mode
    		/// </summary>        
    	//    [DisplayName("Mode")]
            [MaxLength(5, ErrorMessage = "Mode cannot be longer than 5 characters")]
    		public string  Mode { get; set; }
    
    		    
    		/// <summary>
    		/// Sequence
    		/// </summary>        
    	//    [DisplayName("Sequence")]
            [Required(ErrorMessage = "Sequence is required")]
    		public int  Sequence { get; set; }
    
    		    
    		/// <summary>
    		/// Milestone
    		/// </summary>        
    	//    [DisplayName("Milestone")]
            [MaxLength(20, ErrorMessage = "Milestone cannot be longer than 20 characters")]
    		public string  Milestone { get; set; }
    
    		    
    		/// <summary>
    		/// Milestone Desc
    		/// </summary>        
    	//    [DisplayName("Milestone Desc")]
            [MaxLength(50, ErrorMessage = "Milestone Desc cannot be longer than 50 characters")]
    		public string  MilestoneDesc { get; set; }
    
    		    
    		/// <summary>
    		/// Reason Desc
    		/// </summary>        
    	//    [DisplayName("Reason Desc")]
            [MaxLength(50, ErrorMessage = "Reason Desc cannot be longer than 50 characters")]
    		public string  ReasonDesc { get; set; }
    
    		    
    		/// <summary>
    		/// Date Table
    		/// </summary>        
    	//    [DisplayName("Date Table")]
            [MaxLength(50, ErrorMessage = "Date Table cannot be longer than 50 characters")]
    		public string  DateTable { get; set; }
    
    		    
    		/// <summary>
    		/// Date Field
    		/// </summary>        
    	//    [DisplayName("Date Field")]
            [MaxLength(50, ErrorMessage = "Date Field cannot be longer than 50 characters")]
    		public string  DateField { get; set; }
    
    		    
    		/// <summary>
    		/// Party Table
    		/// </summary>        
    	//    [DisplayName("Party Table")]
            [MaxLength(50, ErrorMessage = "Party Table cannot be longer than 50 characters")]
    		public string  PartyTable { get; set; }
    
    		    
    		/// <summary>
    		/// Party Field
    		/// </summary>        
    	//    [DisplayName("Party Field")]
            [MaxLength(50, ErrorMessage = "Party Field cannot be longer than 50 characters")]
    		public string  PartyField { get; set; }
    
    		    
    		/// <summary>
    		/// FRequired
    		/// </summary>        
    	//    [DisplayName("FRequired")]
            [Required(ErrorMessage = "FRequired is required")]
    		public bool  FRequired { get; set; }
    
    		    
    		/// <summary>
    		/// FDate
    		/// </summary>        
    	//    [DisplayName("FDate")]
            [Required(ErrorMessage = "FDate is required")]
    		public bool  FDate { get; set; }
    
    		    
    		/// <summary>
    		/// FParty
    		/// </summary>        
    	//    [DisplayName("FParty")]
            [Required(ErrorMessage = "FParty is required")]
    		public bool  FParty { get; set; }
    
    		    
    		/// <summary>
    		/// FReason
    		/// </summary>        
    	//    [DisplayName("FReason")]
            [Required(ErrorMessage = "FReason is required")]
    		public bool  FReason { get; set; }
    
    		    
    		/// <summary>
    		/// Created By
    		/// </summary>        
    	//    [DisplayName("Created By")]
            [MaxLength(10, ErrorMessage = "Created By cannot be longer than 10 characters")]
    		public string  CreatedBy { get; set; }
    
    		    
    		/// <summary>
    		/// Created Date
    		/// </summary>        
    	//    [DisplayName("Created Date")]
    		public Nullable<System.DateTime>  CreatedDate { get; set; }
    
    		    
    		/// <summary>
    		/// Updated By
    		/// </summary>        
    	//    [DisplayName("Updated By")]
            [MaxLength(10, ErrorMessage = "Updated By cannot be longer than 10 characters")]
    		public string  UpdatedBy { get; set; }
    
    		    
    		/// <summary>
    		/// Updated Date
    		/// </summary>        
    	//    [DisplayName("Updated Date")]
    		public Nullable<System.DateTime>  UpdatedDate { get; set; }
    
    		    
    	}
    //}
    
}
