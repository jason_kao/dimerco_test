using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ValuePlus.ViewModel;



namespace ValuePlus.ViewModel.ReSM
{
    
    /// <summary>
    /// SMANMailReceivedList class
    /// </summary>
    //[MetadataType(typeof(SMANMailReceivedListViewModel))]
    //public  partial class SMANMailReceivedList
    //{
    
    	/// <summary>
    	/// SMANMailReceivedList Metadata class
    	/// </summary>
    	public   class SMANMailReceivedListViewModel:ViewModelBase
    	{
    		    
    		/// <summary>
    		/// HQID
    		/// </summary>        
    	//    [DisplayName("HQID")]
            [Required(ErrorMessage = "HQID is required")]
    		public int  HQID { get; set; }
    
    		    
    		/// <summary>
    		/// ANFunction Code
    		/// </summary>        
    	//    [DisplayName("ANFunction Code")]
            [Required(ErrorMessage = "ANFunction Code is required")]
            [MaxLength(4, ErrorMessage = "ANFunction Code cannot be longer than 4 characters")]
    		public string  ANFunctionCode { get; set; }
    
    		    
    		/// <summary>
    		/// Party Type
    		/// </summary>        
    	//    [DisplayName("Party Type")]
            [Required(ErrorMessage = "Party Type is required")]
            [MaxLength(1, ErrorMessage = "Party Type cannot be longer than 1 characters")]
    		public string  PartyType { get; set; }
    
    		    
    		/// <summary>
    		/// Party ID
    		/// </summary>        
    	//    [DisplayName("Party ID")]
            [Required(ErrorMessage = "Party ID is required")]
    		public int  PartyID { get; set; }
    
    		    
    		/// <summary>
    		/// Mail Type
    		/// </summary>        
    	//    [DisplayName("Mail Type")]
            [Required(ErrorMessage = "Mail Type is required")]
            [MaxLength(1, ErrorMessage = "Mail Type cannot be longer than 1 characters")]
    		public string  MailType { get; set; }
    
    		    
    		/// <summary>
    		/// User ID
    		/// </summary>        
    	//    [DisplayName("User ID")]
            [Required(ErrorMessage = "User ID is required")]
            [MaxLength(10, ErrorMessage = "User ID cannot be longer than 10 characters")]
    		public string  UserID { get; set; }
    
    		    
    		/// <summary>
    		/// Status
    		/// </summary>        
    	//    [DisplayName("Status")]
            [Required(ErrorMessage = "Status is required")]
            [MaxLength(10, ErrorMessage = "Status cannot be longer than 10 characters")]
    		public string  Status { get; set; }
    
    		    
    		/// <summary>
    		/// Created By
    		/// </summary>        
    	//    [DisplayName("Created By")]
            [Required(ErrorMessage = "Created By is required")]
            [MaxLength(6, ErrorMessage = "Created By cannot be longer than 6 characters")]
    		public string  CreatedBy { get; set; }
    
    		    
    		/// <summary>
    		/// Created Date
    		/// </summary>        
    	//    [DisplayName("Created Date")]
            [Required(ErrorMessage = "Created Date is required")]
    		public System.DateTime  CreatedDate { get; set; }
    
    		    
    		/// <summary>
    		/// Updated By
    		/// </summary>        
    	//    [DisplayName("Updated By")]
            [MaxLength(6, ErrorMessage = "Updated By cannot be longer than 6 characters")]
    		public string  UpdatedBy { get; set; }
    
    		    
    		/// <summary>
    		/// Updated Date
    		/// </summary>        
    	//    [DisplayName("Updated Date")]
    		public Nullable<System.DateTime>  UpdatedDate { get; set; }
    
    		    
    	}
    //}
    
}
