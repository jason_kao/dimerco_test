using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ValuePlus.ViewModel;



namespace ValuePlus.ViewModel.ReSM
{
    
    /// <summary>
    /// SMCustomer class
    /// </summary>
    //[MetadataType(typeof(SMCustomerViewModel))]
    //public  partial class SMCustomer
    //{
    
    	/// <summary>
    	/// SMCustomer Metadata class
    	/// </summary>
    	public   class SMCustomerViewModel:ViewModelBase
    	{
    		    
    		/// <summary>
    		/// HQID
    		/// </summary>        
    	//    [DisplayName("HQID")]
            [Required(ErrorMessage = "HQID is required")]
    		public int  HQID { get; set; }
    
    		    
    		/// <summary>
    		/// Station ID
    		/// </summary>        
    	//    [DisplayName("Station ID")]
            [Required(ErrorMessage = "Station ID is required")]
            [MaxLength(3, ErrorMessage = "Station ID cannot be longer than 3 characters")]
    		public string  StationID { get; set; }
    
    		    
    		/// <summary>
    		/// Customer Code
    		/// </summary>        
    	//    [DisplayName("Customer Code")]
            [Required(ErrorMessage = "Customer Code is required")]
            [MaxLength(15, ErrorMessage = "Customer Code cannot be longer than 15 characters")]
    		public string  CustomerCode { get; set; }
    
    		    
    		/// <summary>
    		/// Customer Name
    		/// </summary>        
    	//    [DisplayName("Customer Name")]
            [MaxLength(255, ErrorMessage = "Customer Name cannot be longer than 255 characters")]
    		public string  CustomerName { get; set; }
    
    		    
    		/// <summary>
    		/// City ID
    		/// </summary>        
    	//    [DisplayName("City ID")]
    		public Nullable<int>  CityID { get; set; }
    
    		    
    		/// <summary>
    		/// Industry ID
    		/// </summary>        
    	//    [DisplayName("Industry ID")]
    		public Nullable<int>  IndustryID { get; set; }
    
    		    
    		/// <summary>
    		/// Global Code
    		/// </summary>        
    	//    [DisplayName("Global Code")]
            [MaxLength(10, ErrorMessage = "Global Code cannot be longer than 10 characters")]
    		public string  GlobalCode { get; set; }
    
    		    
    		/// <summary>
    		/// Customer Name1
    		/// </summary>        
    	//    [DisplayName("Customer Name1")]
            [MaxLength(255, ErrorMessage = "Customer Name1 cannot be longer than 255 characters")]
    		public string  CustomerName1 { get; set; }
    
    		    
    		/// <summary>
    		/// Customer Address1
    		/// </summary>        
    	//    [DisplayName("Customer Address1")]
            [MaxLength(255, ErrorMessage = "Customer Address1 cannot be longer than 255 characters")]
    		public string  CustomerAddress1 { get; set; }
    
    		    
    		/// <summary>
    		/// Customer Address2
    		/// </summary>        
    	//    [DisplayName("Customer Address2")]
            [MaxLength(255, ErrorMessage = "Customer Address2 cannot be longer than 255 characters")]
    		public string  CustomerAddress2 { get; set; }
    
    		    
    		/// <summary>
    		/// Customer Address3
    		/// </summary>        
    	//    [DisplayName("Customer Address3")]
            [MaxLength(255, ErrorMessage = "Customer Address3 cannot be longer than 255 characters")]
    		public string  CustomerAddress3 { get; set; }
    
    		    
    		/// <summary>
    		/// Customer Address4
    		/// </summary>        
    	//    [DisplayName("Customer Address4")]
            [MaxLength(255, ErrorMessage = "Customer Address4 cannot be longer than 255 characters")]
    		public string  CustomerAddress4 { get; set; }
    
    		    
    		/// <summary>
    		/// Customer Address5
    		/// </summary>        
    	//    [DisplayName("Customer Address5")]
            [MaxLength(255, ErrorMessage = "Customer Address5 cannot be longer than 255 characters")]
    		public string  CustomerAddress5 { get; set; }
    
    		    
    		/// <summary>
    		/// Phone
    		/// </summary>        
    	//    [DisplayName("Phone")]
            [MaxLength(50, ErrorMessage = "Phone cannot be longer than 50 characters")]
    		public string  Phone { get; set; }
    
    		    
    		/// <summary>
    		/// Phone Ext
    		/// </summary>        
    	//    [DisplayName("Phone Ext")]
            [MaxLength(10, ErrorMessage = "Phone Ext cannot be longer than 10 characters")]
    		public string  PhoneExt { get; set; }
    
    		    
    		/// <summary>
    		/// Fax
    		/// </summary>        
    	//    [DisplayName("Fax")]
            [MaxLength(50, ErrorMessage = "Fax cannot be longer than 50 characters")]
    		public string  Fax { get; set; }
    
    		    
    		/// <summary>
    		/// Fax Ext
    		/// </summary>        
    	//    [DisplayName("Fax Ext")]
            [MaxLength(10, ErrorMessage = "Fax Ext cannot be longer than 10 characters")]
    		public string  FaxExt { get; set; }
    
    		    
    		/// <summary>
    		/// Zip
    		/// </summary>        
    	//    [DisplayName("Zip")]
            [MaxLength(10, ErrorMessage = "Zip cannot be longer than 10 characters")]
    		public string  Zip { get; set; }
    
    		    
    		/// <summary>
    		/// Web Site
    		/// </summary>        
    	//    [DisplayName("Web Site")]
            [MaxLength(255, ErrorMessage = "Web Site cannot be longer than 255 characters")]
    		public string  WebSite { get; set; }
    
    		    
    		/// <summary>
    		/// Trade Term
    		/// </summary>        
    	//    [DisplayName("Trade Term")]
            [MaxLength(10, ErrorMessage = "Trade Term cannot be longer than 10 characters")]
    		public string  TradeTerm { get; set; }
    
    		    
    		/// <summary>
    		/// Shipment Type
    		/// </summary>        
    	//    [DisplayName("Shipment Type")]
            [MaxLength(10, ErrorMessage = "Shipment Type cannot be longer than 10 characters")]
    		public string  ShipmentType { get; set; }
    
    		    
    		/// <summary>
    		/// Service Type
    		/// </summary>        
    	//    [DisplayName("Service Type")]
            [MaxLength(10, ErrorMessage = "Service Type cannot be longer than 10 characters")]
    		public string  ServiceType { get; set; }
    
    		    
    		/// <summary>
    		/// Air Move
    		/// </summary>        
    	//    [DisplayName("Air Move")]
            [MaxLength(10, ErrorMessage = "Air Move cannot be longer than 10 characters")]
    		public string  AirMove { get; set; }
    
    		    
    		/// <summary>
    		/// Ocean Move
    		/// </summary>        
    	//    [DisplayName("Ocean Move")]
            [MaxLength(10, ErrorMessage = "Ocean Move cannot be longer than 10 characters")]
    		public string  OceanMove { get; set; }
    
    		    
    		/// <summary>
    		/// Air Line Code
    		/// </summary>        
    	//    [DisplayName("Air Line Code")]
            [MaxLength(10, ErrorMessage = "Air Line Code cannot be longer than 10 characters")]
    		public string  AirLineCode { get; set; }
    
    		    
    		/// <summary>
    		/// TPLetter Code
    		/// </summary>        
    	//    [DisplayName("TPLetter Code")]
            [MaxLength(10, ErrorMessage = "TPLetter Code cannot be longer than 10 characters")]
    		public string  TPLetterCode { get; set; }
    
    		    
    		/// <summary>
    		/// Marks1
    		/// </summary>        
    	//    [DisplayName("Marks1")]
            [MaxLength(255, ErrorMessage = "Marks1 cannot be longer than 255 characters")]
    		public string  Marks1 { get; set; }
    
    		    
    		/// <summary>
    		/// Marks2
    		/// </summary>        
    	//    [DisplayName("Marks2")]
            [MaxLength(255, ErrorMessage = "Marks2 cannot be longer than 255 characters")]
    		public string  Marks2 { get; set; }
    
    		    
    		/// <summary>
    		/// Marks3
    		/// </summary>        
    	//    [DisplayName("Marks3")]
            [MaxLength(255, ErrorMessage = "Marks3 cannot be longer than 255 characters")]
    		public string  Marks3 { get; set; }
    
    		    
    		/// <summary>
    		/// Marks4
    		/// </summary>        
    	//    [DisplayName("Marks4")]
            [MaxLength(255, ErrorMessage = "Marks4 cannot be longer than 255 characters")]
    		public string  Marks4 { get; set; }
    
    		    
    		/// <summary>
    		/// Marks5
    		/// </summary>        
    	//    [DisplayName("Marks5")]
            [MaxLength(255, ErrorMessage = "Marks5 cannot be longer than 255 characters")]
    		public string  Marks5 { get; set; }
    
    		    
    		/// <summary>
    		/// Natureof Goods1
    		/// </summary>        
    	//    [DisplayName("Natureof Goods1")]
            [MaxLength(255, ErrorMessage = "Natureof Goods1 cannot be longer than 255 characters")]
    		public string  NatureofGoods1 { get; set; }
    
    		    
    		/// <summary>
    		/// Natureof Goods2
    		/// </summary>        
    	//    [DisplayName("Natureof Goods2")]
            [MaxLength(255, ErrorMessage = "Natureof Goods2 cannot be longer than 255 characters")]
    		public string  NatureofGoods2 { get; set; }
    
    		    
    		/// <summary>
    		/// Commodity
    		/// </summary>        
    	//    [DisplayName("Commodity")]
            [MaxLength(255, ErrorMessage = "Commodity cannot be longer than 255 characters")]
    		public string  Commodity { get; set; }
    
    		    
    		/// <summary>
    		/// VAT
    		/// </summary>        
    	//    [DisplayName("VAT")]
            [MaxLength(10, ErrorMessage = "VAT cannot be longer than 10 characters")]
    		public string  VAT { get; set; }
    
    		    
    		/// <summary>
    		/// Status
    		/// </summary>        
    	//    [DisplayName("Status")]
            [Required(ErrorMessage = "Status is required")]
            [MaxLength(10, ErrorMessage = "Status cannot be longer than 10 characters")]
    		public string  Status { get; set; }
    
    		    
    		/// <summary>
    		/// Created By
    		/// </summary>        
    	//    [DisplayName("Created By")]
            [MaxLength(6, ErrorMessage = "Created By cannot be longer than 6 characters")]
    		public string  CreatedBy { get; set; }
    
    		    
    		/// <summary>
    		/// Created Date
    		/// </summary>        
    	//    [DisplayName("Created Date")]
            [Required(ErrorMessage = "Created Date is required")]
    		public System.DateTime  CreatedDate { get; set; }
    
    		    
    		/// <summary>
    		/// Updated By
    		/// </summary>        
    	//    [DisplayName("Updated By")]
            [MaxLength(6, ErrorMessage = "Updated By cannot be longer than 6 characters")]
    		public string  UpdatedBy { get; set; }
    
    		    
    		/// <summary>
    		/// Updated Date
    		/// </summary>        
    	//    [DisplayName("Updated Date")]
    		public Nullable<System.DateTime>  UpdatedDate { get; set; }
    
    		    
    		/// <summary>
    		/// Version
    		/// </summary>        
    	//    [DisplayName("Version")]
            [Required(ErrorMessage = "Version is required")]
            [MaxLength(8, ErrorMessage = "Version cannot be longer than 8 characters")]
    		public byte[]  Version { get; set; }
    
    		    
    		/// <summary>
    		/// Is To ACS
    		/// </summary>        
    	//    [DisplayName("Is To ACS")]
            [Required(ErrorMessage = "Is To ACS is required")]
    		public bool  IsToACS { get; set; }
    
    		    
    		/// <summary>
    		/// City
    		/// </summary>        
    	//    [DisplayName("City")]
            [MaxLength(20, ErrorMessage = "City cannot be longer than 20 characters")]
    		public string  City { get; set; }
    
    		    
    		/// <summary>
    		/// State
    		/// </summary>        
    	//    [DisplayName("State")]
            [MaxLength(20, ErrorMessage = "State cannot be longer than 20 characters")]
    		public string  State { get; set; }
    
    		    
    		/// <summary>
    		/// Exist Customer
    		/// </summary>        
    	//    [DisplayName("Exist Customer")]
    		public Nullable<bool>  ExistCustomer { get; set; }
    
    		    
    		/// <summary>
    		/// Created Station ID
    		/// </summary>        
    	//    [DisplayName("Created Station ID")]
            [MaxLength(3, ErrorMessage = "Created Station ID cannot be longer than 3 characters")]
    		public string  CreatedStationID { get; set; }
    
    		    
    		/// <summary>
    		/// Vendor Posting GLCode
    		/// </summary>        
    	//    [DisplayName("Vendor Posting GLCode")]
            [MaxLength(10, ErrorMessage = "Vendor Posting GLCode cannot be longer than 10 characters")]
    		public string  VendorPostingGLCode { get; set; }
    
    		    
    		/// <summary>
    		/// Local Name
    		/// </summary>        
    	//    [DisplayName("Local Name")]
            [MaxLength(255, ErrorMessage = "Local Name cannot be longer than 255 characters")]
    		public string  LocalName { get; set; }
    
    		    
    		/// <summary>
    		/// Lead Source ID
    		/// </summary>        
    	//    [DisplayName("Lead Source ID")]
    		public Nullable<int>  LeadSourceID { get; set; }
    
    		    
    		/// <summary>
    		/// Country
    		/// </summary>        
    	//    [DisplayName("Country")]
            [MaxLength(50, ErrorMessage = "Country cannot be longer than 50 characters")]
    		public string  Country { get; set; }
    
    		    
    		/// <summary>
    		/// Industry Group ID
    		/// </summary>        
    	//    [DisplayName("Industry Group ID")]
    		public Nullable<int>  IndustryGroupID { get; set; }
    
    		    
    		/// <summary>
    		/// Pay Term ID
    		/// </summary>        
    	//    [DisplayName("Pay Term ID")]
    		public Nullable<int>  PayTermID { get; set; }
    
    		    
    		/// <summary>
    		/// Agent ID
    		/// </summary>        
    	//    [DisplayName("Agent ID")]
    		public Nullable<int>  AgentID { get; set; }
    
    		    
    		/// <summary>
    		/// Bill To Party
    		/// </summary>        
    	//    [DisplayName("Bill To Party")]
    		public Nullable<int>  BillToParty { get; set; }
    
    		    
    		/// <summary>
    		/// PODFlag
    		/// </summary>        
    	//    [DisplayName("PODFlag")]
    		public Nullable<bool>  PODFlag { get; set; }
    
    		    
    		/// <summary>
    		/// Freight Location
    		/// </summary>        
    	//    [DisplayName("Freight Location")]
            [MaxLength(50, ErrorMessage = "Freight Location cannot be longer than 50 characters")]
    		public string  FreightLocation { get; set; }
    
    		    
    		/// <summary>
    		/// Is MNC
    		/// </summary>        
    	//    [DisplayName("Is MNC")]
    		public Nullable<bool>  IsMNC { get; set; }
    
    		    
    		/// <summary>
    		/// Cust Type
    		/// </summary>        
    	//    [DisplayName("Cust Type")]
            [MaxLength(2, ErrorMessage = "Cust Type cannot be longer than 2 characters")]
    		public string  CustType { get; set; }
    
    		    
    		/// <summary>
    		/// Capital Currency ID
    		/// </summary>        
    	//    [DisplayName("Capital Currency ID")]
    		public Nullable<int>  CapitalCurrencyID { get; set; }
    
    		    
    		/// <summary>
    		/// Capital Amount
    		/// </summary>        
    	//    [DisplayName("Capital Amount")]
            [MaxLength(50, ErrorMessage = "Capital Amount cannot be longer than 50 characters")]
    		public string  CapitalAmount { get; set; }
    
    		    
    		/// <summary>
    		/// Established Date
    		/// </summary>        
    	//    [DisplayName("Established Date")]
            [MaxLength(50, ErrorMessage = "Established Date cannot be longer than 50 characters")]
    		public string  EstablishedDate { get; set; }
    
    		    
    		/// <summary>
    		/// Annual Revenue
    		/// </summary>        
    	//    [DisplayName("Annual Revenue")]
            [MaxLength(50, ErrorMessage = "Annual Revenue cannot be longer than 50 characters")]
    		public string  AnnualRevenue { get; set; }
    
    		    
    		/// <summary>
    		/// Is Vendor
    		/// </summary>        
    	//    [DisplayName("Is Vendor")]
    		public Nullable<bool>  IsVendor { get; set; }
    
    		    
    		/// <summary>
    		/// Customer Level
    		/// </summary>        
    	//    [DisplayName("Customer Level")]
            [MaxLength(20, ErrorMessage = "Customer Level cannot be longer than 20 characters")]
    		public string  CustomerLevel { get; set; }
    
    		    
    		/// <summary>
    		/// lat
    		/// </summary>        
    	//    [DisplayName("lat")]
    		public Nullable<double>  lat { get; set; }
    
    		    
    		/// <summary>
    		/// lng
    		/// </summary>        
    	//    [DisplayName("lng")]
    		public Nullable<double>  lng { get; set; }
    
    		    
    		/// <summary>
    		/// SAFiles ID
    		/// </summary>        
    	//    [DisplayName("SAFiles ID")]
    		public Nullable<int>  SAFilesID { get; set; }
    
    		    
    	}
    //}
    
}
