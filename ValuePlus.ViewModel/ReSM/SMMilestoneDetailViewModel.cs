using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ValuePlus.ViewModel;



namespace ValuePlus.ViewModel.ReSM
{
    
    /// <summary>
    /// SMMilestoneDetail class
    /// </summary>
    //[MetadataType(typeof(SMMilestoneDetailViewModel))]
    //public  partial class SMMilestoneDetail
    //{
    
    	/// <summary>
    	/// SMMilestoneDetail Metadata class
    	/// </summary>
    	public   class SMMilestoneDetailViewModel:ViewModelBase
    	{
    		    
    		/// <summary>
    		/// ID
    		/// </summary>        
    	//    [DisplayName("ID")]
            [Required(ErrorMessage = "ID is required")]
    		public int  ID { get; set; }
    
    		    
    		/// <summary>
    		/// Station ID
    		/// </summary>        
    	//    [DisplayName("Station ID")]
            [Required(ErrorMessage = "Station ID is required")]
            [MaxLength(6, ErrorMessage = "Station ID cannot be longer than 6 characters")]
    		public string  StationID { get; set; }
    
    		    
    		/// <summary>
    		/// Parentkey Value
    		/// </summary>        
    	//    [DisplayName("Parentkey Value")]
            [MaxLength(20, ErrorMessage = "Parentkey Value cannot be longer than 20 characters")]
    		public string  ParentkeyValue { get; set; }
    
    		    
    		/// <summary>
    		/// Container Key
    		/// </summary>        
    	//    [DisplayName("Container Key")]
            [MaxLength(20, ErrorMessage = "Container Key cannot be longer than 20 characters")]
    		public string  ContainerKey { get; set; }
    
    		    
    		/// <summary>
    		/// POKey
    		/// </summary>        
    	//    [DisplayName("POKey")]
            [MaxLength(20, ErrorMessage = "POKey cannot be longer than 20 characters")]
    		public string  POKey { get; set; }
    
    		    
    		/// <summary>
    		/// SOKey
    		/// </summary>        
    	//    [DisplayName("SOKey")]
            [MaxLength(20, ErrorMessage = "SOKey cannot be longer than 20 characters")]
    		public string  SOKey { get; set; }
    
    		    
    		/// <summary>
    		/// Product Line
    		/// </summary>        
    	//    [DisplayName("Product Line")]
    		public Nullable<int>  ProductLine { get; set; }
    
    		    
    		/// <summary>
    		/// Milestone ID
    		/// </summary>        
    	//    [DisplayName("Milestone ID")]
    		public Nullable<int>  MilestoneID { get; set; }
    
    		    
    		/// <summary>
    		/// Milestone Time
    		/// </summary>        
    	//    [DisplayName("Milestone Time")]
    		public Nullable<System.DateTime>  MilestoneTime { get; set; }
    
    		    
    		/// <summary>
    		/// Extra Value1
    		/// </summary>        
    	//    [DisplayName("Extra Value1")]
            [MaxLength(50, ErrorMessage = "Extra Value1 cannot be longer than 50 characters")]
    		public string  ExtraValue1 { get; set; }
    
    		    
    		/// <summary>
    		/// Extra Value2
    		/// </summary>        
    	//    [DisplayName("Extra Value2")]
            [MaxLength(50, ErrorMessage = "Extra Value2 cannot be longer than 50 characters")]
    		public string  ExtraValue2 { get; set; }
    
    		    
    		/// <summary>
    		/// UTCOffset
    		/// </summary>        
    	//    [DisplayName("UTCOffset")]
    		public Nullable<int>  UTCOffset { get; set; }
    
    		    
    		/// <summary>
    		/// Status
    		/// </summary>        
    	//    [DisplayName("Status")]
    		public Nullable<int>  Status { get; set; }
    
    		    
    		/// <summary>
    		/// Created DT
    		/// </summary>        
    	//    [DisplayName("Created DT")]
    		public Nullable<System.DateTime>  CreatedDT { get; set; }
    
    		    
    		/// <summary>
    		/// Created By
    		/// </summary>        
    	//    [DisplayName("Created By")]
            [MaxLength(5, ErrorMessage = "Created By cannot be longer than 5 characters")]
    		public string  CreatedBy { get; set; }
    
    		    
    	}
    //}
    
}
