using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ValuePlus.ViewModel;



namespace ValuePlus.ViewModel.ReSM
{
    
    /// <summary>
    /// SMMailSetting class
    /// </summary>
    //[MetadataType(typeof(SMMailSettingViewModel))]
    //public  partial class SMMailSetting
    //{
    
    	/// <summary>
    	/// SMMailSetting Metadata class
    	/// </summary>
    	public   class SMMailSettingViewModel:ViewModelBase
    	{
    		    
    		/// <summary>
    		/// Title
    		/// </summary>        
    	//    [DisplayName("Title")]
            [Required(ErrorMessage = "Title is required")]
            [MaxLength(50, ErrorMessage = "Title cannot be longer than 50 characters")]
    		public string  Title { get; set; }
    
    		    
    		/// <summary>
    		/// SMTPServer
    		/// </summary>        
    	//    [DisplayName("SMTPServer")]
            [MaxLength(50, ErrorMessage = "SMTPServer cannot be longer than 50 characters")]
    		public string  SMTPServer { get; set; }
    
    		    
    		/// <summary>
    		/// Encode
    		/// </summary>        
    	//    [DisplayName("Encode")]
            [MaxLength(50, ErrorMessage = "Encode cannot be longer than 50 characters")]
    		public string  Encode { get; set; }
    
    		    
    		/// <summary>
    		/// Account
    		/// </summary>        
    	//    [DisplayName("Account")]
            [MaxLength(50, ErrorMessage = "Account cannot be longer than 50 characters")]
    		public string  Account { get; set; }
    
    		    
    		/// <summary>
    		/// Password
    		/// </summary>        
    	//    [DisplayName("Password")]
            [MaxLength(50, ErrorMessage = "Password cannot be longer than 50 characters")]
    		public string  Password { get; set; }
    
    		    
    		/// <summary>
    		/// Account Mail
    		/// </summary>        
    	//    [DisplayName("Account Mail")]
            [MaxLength(50, ErrorMessage = "Account Mail cannot be longer than 50 characters")]
    		public string  AccountMail { get; set; }
    
    		    
    		/// <summary>
    		/// Display Name
    		/// </summary>        
    	//    [DisplayName("Display Name")]
            [MaxLength(50, ErrorMessage = "Display Name cannot be longer than 50 characters")]
    		public string  DisplayName { get; set; }
    
    		    
    		/// <summary>
    		/// Status
    		/// </summary>        
    	//    [DisplayName("Status")]
            [Required(ErrorMessage = "Status is required")]
            [MaxLength(10, ErrorMessage = "Status cannot be longer than 10 characters")]
    		public string  Status { get; set; }
    
    		    
    		/// <summary>
    		/// Created By
    		/// </summary>        
    	//    [DisplayName("Created By")]
            [MaxLength(50, ErrorMessage = "Created By cannot be longer than 50 characters")]
    		public string  CreatedBy { get; set; }
    
    		    
    		/// <summary>
    		/// Created Date
    		/// </summary>        
    	//    [DisplayName("Created Date")]
            [Required(ErrorMessage = "Created Date is required")]
    		public System.DateTime  CreatedDate { get; set; }
    
    		    
    		/// <summary>
    		/// Updated By
    		/// </summary>        
    	//    [DisplayName("Updated By")]
            [MaxLength(50, ErrorMessage = "Updated By cannot be longer than 50 characters")]
    		public string  UpdatedBy { get; set; }
    
    		    
    		/// <summary>
    		/// Updated Date
    		/// </summary>        
    	//    [DisplayName("Updated Date")]
    		public Nullable<System.DateTime>  UpdatedDate { get; set; }
    
    		    
    		/// <summary>
    		/// Version
    		/// </summary>        
    	//    [DisplayName("Version")]
            [Required(ErrorMessage = "Version is required")]
            [MaxLength(8, ErrorMessage = "Version cannot be longer than 8 characters")]
    		public byte[]  Version { get; set; }
    
    		    
    	}
    //}
    
}
