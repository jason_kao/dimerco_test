using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ValuePlus.ViewModel;



namespace ValuePlus.ViewModel.ReSM
{
    
    /// <summary>
    /// SMCustomerForms class
    /// </summary>
    //[MetadataType(typeof(SMCustomerFormsViewModel))]
    //public  partial class SMCustomerForms
    //{
    
    	/// <summary>
    	/// SMCustomerForms Metadata class
    	/// </summary>
    	public   class SMCustomerFormsViewModel:ViewModelBase
    	{
    		    
    		/// <summary>
    		/// HQID
    		/// </summary>        
    	//    [DisplayName("HQID")]
            [Required(ErrorMessage = "HQID is required")]
    		public int  HQID { get; set; }
    
    		    
    		/// <summary>
    		/// Customer ID
    		/// </summary>        
    	//    [DisplayName("Customer ID")]
    		public Nullable<int>  CustomerID { get; set; }
    
    		    
    		/// <summary>
    		/// e Form Name
    		/// </summary>        
    	//    [DisplayName("e Form Name")]
            [MaxLength(50, ErrorMessage = "e Form Name cannot be longer than 50 characters")]
    		public string  eFormName { get; set; }
    
    		    
    		/// <summary>
    		/// Mode
    		/// </summary>        
    	//    [DisplayName("Mode")]
            [MaxLength(50, ErrorMessage = "Mode cannot be longer than 50 characters")]
    		public string  Mode { get; set; }
    
    		    
    		/// <summary>
    		/// VIA
    		/// </summary>        
    	//    [DisplayName("VIA")]
            [MaxLength(50, ErrorMessage = "VIA cannot be longer than 50 characters")]
    		public string  VIA { get; set; }
    
    		    
    		/// <summary>
    		/// File Format
    		/// </summary>        
    	//    [DisplayName("File Format")]
            [MaxLength(50, ErrorMessage = "File Format cannot be longer than 50 characters")]
    		public string  FileFormat { get; set; }
    
    		    
    		/// <summary>
    		/// Status
    		/// </summary>        
    	//    [DisplayName("Status")]
            [Required(ErrorMessage = "Status is required")]
            [MaxLength(10, ErrorMessage = "Status cannot be longer than 10 characters")]
    		public string  Status { get; set; }
    
    		    
    		/// <summary>
    		/// Created By
    		/// </summary>        
    	//    [DisplayName("Created By")]
            [MaxLength(50, ErrorMessage = "Created By cannot be longer than 50 characters")]
    		public string  CreatedBy { get; set; }
    
    		    
    		/// <summary>
    		/// Created Date
    		/// </summary>        
    	//    [DisplayName("Created Date")]
    		public Nullable<System.DateTime>  CreatedDate { get; set; }
    
    		    
    		/// <summary>
    		/// Updated By
    		/// </summary>        
    	//    [DisplayName("Updated By")]
            [MaxLength(50, ErrorMessage = "Updated By cannot be longer than 50 characters")]
    		public string  UpdatedBy { get; set; }
    
    		    
    		/// <summary>
    		/// Updated Date
    		/// </summary>        
    	//    [DisplayName("Updated Date")]
    		public Nullable<System.DateTime>  UpdatedDate { get; set; }
    
    		    
    		/// <summary>
    		/// Version
    		/// </summary>        
    	//    [DisplayName("Version")]
            [MaxLength(8, ErrorMessage = "Version cannot be longer than 8 characters")]
    		public byte[]  Version { get; set; }
    
    		    
    		/// <summary>
    		/// Zip File
    		/// </summary>        
    	//    [DisplayName("Zip File")]
    		public Nullable<int>  ZipFile { get; set; }
    
    		    
    	}
    //}
    
}
