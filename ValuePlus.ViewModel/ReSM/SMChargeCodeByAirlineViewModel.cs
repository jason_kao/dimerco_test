using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ValuePlus.ViewModel;



namespace ValuePlus.ViewModel.ReSM
{
    
    /// <summary>
    /// SMChargeCodeByAirline class
    /// </summary>
    //[MetadataType(typeof(SMChargeCodeByAirlineViewModel))]
    //public  partial class SMChargeCodeByAirline
    //{
    
    	/// <summary>
    	/// SMChargeCodeByAirline Metadata class
    	/// </summary>
    	public   class SMChargeCodeByAirlineViewModel:ViewModelBase
    	{
    		    
    		/// <summary>
    		/// ID
    		/// </summary>        
    	//    [DisplayName("ID")]
            [Required(ErrorMessage = "ID is required")]
    		public int  ID { get; set; }
    
    		    
    		/// <summary>
    		/// Station ID
    		/// </summary>        
    	//    [DisplayName("Station ID")]
            [MaxLength(3, ErrorMessage = "Station ID cannot be longer than 3 characters")]
    		public string  StationID { get; set; }
    
    		    
    		/// <summary>
    		/// Airline HQID
    		/// </summary>        
    	//    [DisplayName("Airline HQID")]
    		public Nullable<int>  AirlineHQID { get; set; }
    
    		    
    		/// <summary>
    		/// Sub Charge ID
    		/// </summary>        
    	//    [DisplayName("Sub Charge ID")]
    		public Nullable<int>  SubChargeID { get; set; }
    
    		    
    		/// <summary>
    		/// Charge ID
    		/// </summary>        
    	//    [DisplayName("Charge ID")]
    		public Nullable<int>  ChargeID { get; set; }
    
    		    
    		/// <summary>
    		/// EDICharge ID
    		/// </summary>        
    	//    [DisplayName("EDICharge ID")]
    		public Nullable<int>  EDIChargeID { get; set; }
    
    		    
    		/// <summary>
    		/// Createdby
    		/// </summary>        
    	//    [DisplayName("Createdby")]
            [MaxLength(10, ErrorMessage = "Createdby cannot be longer than 10 characters")]
    		public string  Createdby { get; set; }
    
    		    
    		/// <summary>
    		/// Created Date
    		/// </summary>        
    	//    [DisplayName("Created Date")]
    		public Nullable<System.DateTime>  CreatedDate { get; set; }
    
    		    
    		/// <summary>
    		/// Updatedby
    		/// </summary>        
    	//    [DisplayName("Updatedby")]
            [MaxLength(10, ErrorMessage = "Updatedby cannot be longer than 10 characters")]
    		public string  Updatedby { get; set; }
    
    		    
    		/// <summary>
    		/// Updated Date
    		/// </summary>        
    	//    [DisplayName("Updated Date")]
    		public Nullable<System.DateTime>  UpdatedDate { get; set; }
    
    		    
    	}
    //}
    
}
