using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ValuePlus.ViewModel;



namespace ValuePlus.ViewModel.ReSM
{
    
    /// <summary>
    /// SMUser class
    /// </summary>
    //[MetadataType(typeof(SMUserViewModel))]
    //public  partial class SMUser
    //{
    
    	/// <summary>
    	/// SMUser Metadata class
    	/// </summary>
    	public   class SMUserViewModel:ViewModelBase
    	{
    		    
    		/// <summary>
    		/// HQID
    		/// </summary>        
    	//    [DisplayName("HQID")]
            [Required(ErrorMessage = "HQID is required")]
    		public int  HQID { get; set; }
    
    		    
    		/// <summary>
    		/// User ID
    		/// </summary>        
    	//    [DisplayName("User ID")]
            [MaxLength(6, ErrorMessage = "User ID cannot be longer than 6 characters")]
    		public string  UserID { get; set; }
    
    		    
    		/// <summary>
    		/// First Name
    		/// </summary>        
    	//    [DisplayName("First Name")]
            [MaxLength(50, ErrorMessage = "First Name cannot be longer than 50 characters")]
    		public string  FirstName { get; set; }
    
    		    
    		/// <summary>
    		/// Last Name
    		/// </summary>        
    	//    [DisplayName("Last Name")]
            [MaxLength(50, ErrorMessage = "Last Name cannot be longer than 50 characters")]
    		public string  LastName { get; set; }
    
    		    
    		/// <summary>
    		/// Full Name
    		/// </summary>        
    	//    [DisplayName("Full Name")]
            [MaxLength(100, ErrorMessage = "Full Name cannot be longer than 100 characters")]
    		public string  FullName { get; set; }
    
    		    
    		/// <summary>
    		/// Department ID
    		/// </summary>        
    	//    [DisplayName("Department ID")]
            [MaxLength(50, ErrorMessage = "Department ID cannot be longer than 50 characters")]
    		public string  DepartmentID { get; set; }
    
    		    
    		/// <summary>
    		/// Station ID
    		/// </summary>        
    	//    [DisplayName("Station ID")]
            [MaxLength(3, ErrorMessage = "Station ID cannot be longer than 3 characters")]
    		public string  StationID { get; set; }
    
    		    
    		/// <summary>
    		/// Hire Date
    		/// </summary>        
    	//    [DisplayName("Hire Date")]
    		public Nullable<System.DateTime>  HireDate { get; set; }
    
    		    
    		/// <summary>
    		/// Birthday
    		/// </summary>        
    	//    [DisplayName("Birthday")]
    		public Nullable<System.DateTime>  Birthday { get; set; }
    
    		    
    		/// <summary>
    		/// Password
    		/// </summary>        
    	//    [DisplayName("Password")]
            [MaxLength(50, ErrorMessage = "Password cannot be longer than 50 characters")]
    		public string  Password { get; set; }
    
    		    
    		/// <summary>
    		/// Password Last Changed
    		/// </summary>        
    	//    [DisplayName("Password Last Changed")]
    		public Nullable<System.DateTime>  PasswordLastChanged { get; set; }
    
    		    
    		/// <summary>
    		/// Email
    		/// </summary>        
    	//    [DisplayName("Email")]
            [MaxLength(50, ErrorMessage = "Email cannot be longer than 50 characters")]
    		public string  Email { get; set; }
    
    		    
    		/// <summary>
    		/// Report To
    		/// </summary>        
    	//    [DisplayName("Report To")]
    		public Nullable<int>  ReportTo { get; set; }
    
    		    
    		/// <summary>
    		/// Title
    		/// </summary>        
    	//    [DisplayName("Title")]
            [MaxLength(50, ErrorMessage = "Title cannot be longer than 50 characters")]
    		public string  Title { get; set; }
    
    		    
    		/// <summary>
    		/// Status
    		/// </summary>        
    	//    [DisplayName("Status")]
            [Required(ErrorMessage = "Status is required")]
            [MaxLength(10, ErrorMessage = "Status cannot be longer than 10 characters")]
    		public string  Status { get; set; }
    
    		    
    		/// <summary>
    		/// Created By
    		/// </summary>        
    	//    [DisplayName("Created By")]
            [MaxLength(6, ErrorMessage = "Created By cannot be longer than 6 characters")]
    		public string  CreatedBy { get; set; }
    
    		    
    		/// <summary>
    		/// Created Date
    		/// </summary>        
    	//    [DisplayName("Created Date")]
            [Required(ErrorMessage = "Created Date is required")]
    		public System.DateTime  CreatedDate { get; set; }
    
    		    
    		/// <summary>
    		/// Updated By
    		/// </summary>        
    	//    [DisplayName("Updated By")]
            [MaxLength(6, ErrorMessage = "Updated By cannot be longer than 6 characters")]
    		public string  UpdatedBy { get; set; }
    
    		    
    		/// <summary>
    		/// Updated Date
    		/// </summary>        
    	//    [DisplayName("Updated Date")]
    		public Nullable<System.DateTime>  UpdatedDate { get; set; }
    
    		    
    		/// <summary>
    		/// Version
    		/// </summary>        
    	//    [DisplayName("Version")]
            [MaxLength(8, ErrorMessage = "Version cannot be longer than 8 characters")]
    		public byte[]  Version { get; set; }
    
    		    
    		/// <summary>
    		/// Tel
    		/// </summary>        
    	//    [DisplayName("Tel")]
            [MaxLength(60, ErrorMessage = "Tel cannot be longer than 60 characters")]
    		public string  Tel { get; set; }
    
    		    
    		/// <summary>
    		/// Fax
    		/// </summary>        
    	//    [DisplayName("Fax")]
            [MaxLength(60, ErrorMessage = "Fax cannot be longer than 60 characters")]
    		public string  Fax { get; set; }
    
    		    
    		/// <summary>
    		/// Mobile Phone
    		/// </summary>        
    	//    [DisplayName("Mobile Phone")]
            [MaxLength(60, ErrorMessage = "Mobile Phone cannot be longer than 60 characters")]
    		public string  MobilePhone { get; set; }
    
    		    
    		/// <summary>
    		/// Skype
    		/// </summary>        
    	//    [DisplayName("Skype")]
            [MaxLength(60, ErrorMessage = "Skype cannot be longer than 60 characters")]
    		public string  Skype { get; set; }
    
    		    
    		/// <summary>
    		/// Last Login DT
    		/// </summary>        
    	//    [DisplayName("Last Login DT")]
    		public Nullable<System.DateTime>  LastLoginDT { get; set; }
    
    		    
    		/// <summary>
    		/// Last Login IP
    		/// </summary>        
    	//    [DisplayName("Last Login IP")]
            [MaxLength(20, ErrorMessage = "Last Login IP cannot be longer than 20 characters")]
    		public string  LastLoginIP { get; set; }
    
    		    
    		/// <summary>
    		/// Admin
    		/// </summary>        
    	//    [DisplayName("Admin")]
            [MaxLength(10, ErrorMessage = "Admin cannot be longer than 10 characters")]
    		public string  Admin { get; set; }
    
    		    
    		/// <summary>
    		/// CRPDepartment ID
    		/// </summary>        
    	//    [DisplayName("CRPDepartment ID")]
    		public Nullable<int>  CRPDepartmentID { get; set; }
    
    		    
    		/// <summary>
    		/// CRPDivision ID
    		/// </summary>        
    	//    [DisplayName("CRPDivision ID")]
    		public Nullable<int>  CRPDivisionID { get; set; }
    
    		    
    		/// <summary>
    		/// Alias Login ID
    		/// </summary>        
    	//    [DisplayName("Alias Login ID")]
            [MaxLength(50, ErrorMessage = "Alias Login ID cannot be longer than 50 characters")]
    		public string  AliasLoginID { get; set; }
    
    		    
    		/// <summary>
    		/// Cost Center1
    		/// </summary>        
    	//    [DisplayName("Cost Center1")]
            [MaxLength(3, ErrorMessage = "Cost Center1 cannot be longer than 3 characters")]
    		public string  CostCenter1 { get; set; }
    
    		    
    		/// <summary>
    		/// Cost Center2
    		/// </summary>        
    	//    [DisplayName("Cost Center2")]
            [MaxLength(3, ErrorMessage = "Cost Center2 cannot be longer than 3 characters")]
    		public string  CostCenter2 { get; set; }
    
    		    
    		/// <summary>
    		/// Cost Center3
    		/// </summary>        
    	//    [DisplayName("Cost Center3")]
            [MaxLength(3, ErrorMessage = "Cost Center3 cannot be longer than 3 characters")]
    		public string  CostCenter3 { get; set; }
    
    		    
    		/// <summary>
    		/// IATANo
    		/// </summary>        
    	//    [DisplayName("IATANo")]
            [MaxLength(20, ErrorMessage = "IATANo cannot be longer than 20 characters")]
    		public string  IATANo { get; set; }
    
    		    
    	}
    //}
    
}
