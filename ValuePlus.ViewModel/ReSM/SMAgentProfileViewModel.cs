using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ValuePlus.ViewModel;



namespace ValuePlus.ViewModel.ReSM
{
    
    /// <summary>
    /// SMAgentProfile class
    /// </summary>
    //[MetadataType(typeof(SMAgentProfileViewModel))]
    //public  partial class SMAgentProfile
    //{
    
    	/// <summary>
    	/// SMAgentProfile Metadata class
    	/// </summary>
    	public   class SMAgentProfileViewModel:ViewModelBase
    	{
    		    
    		/// <summary>
    		/// HQID
    		/// </summary>        
    	//    [DisplayName("HQID")]
            [Required(ErrorMessage = "HQID is required")]
    		public int  HQID { get; set; }
    
    		    
    		/// <summary>
    		/// Agent Profile Code
    		/// </summary>        
    	//    [DisplayName("Agent Profile Code")]
            [Required(ErrorMessage = "Agent Profile Code is required")]
            [MaxLength(10, ErrorMessage = "Agent Profile Code cannot be longer than 10 characters")]
    		public string  AgentProfileCode { get; set; }
    
    		    
    		/// <summary>
    		/// Agent Profile Name
    		/// </summary>        
    	//    [DisplayName("Agent Profile Name")]
            [MaxLength(255, ErrorMessage = "Agent Profile Name cannot be longer than 255 characters")]
    		public string  AgentProfileName { get; set; }
    
    		    
    		/// <summary>
    		/// Form Of Agreement
    		/// </summary>        
    	//    [DisplayName("Form Of Agreement")]
            [MaxLength(50, ErrorMessage = "Form Of Agreement cannot be longer than 50 characters")]
    		public string  FormOfAgreement { get; set; }
    
    		    
    		/// <summary>
    		/// Effective Date
    		/// </summary>        
    	//    [DisplayName("Effective Date")]
    		public Nullable<System.DateTime>  EffectiveDate { get; set; }
    
    		    
    		/// <summary>
    		/// Expired Date
    		/// </summary>        
    	//    [DisplayName("Expired Date")]
    		public Nullable<System.DateTime>  ExpiredDate { get; set; }
    
    		    
    		/// <summary>
    		/// Status
    		/// </summary>        
    	//    [DisplayName("Status")]
            [Required(ErrorMessage = "Status is required")]
            [MaxLength(10, ErrorMessage = "Status cannot be longer than 10 characters")]
    		public string  Status { get; set; }
    
    		    
    		/// <summary>
    		/// Contract Party
    		/// </summary>        
    	//    [DisplayName("Contract Party")]
    		public string  ContractParty { get; set; }
    
    		    
    		/// <summary>
    		/// handling
    		/// </summary>        
    	//    [DisplayName("handling")]
    		public string  handling { get; set; }
    
    		    
    		/// <summary>
    		/// Profit Share
    		/// </summary>        
    	//    [DisplayName("Profit Share")]
    		public string  ProfitShare { get; set; }
    
    		    
    		/// <summary>
    		/// Payment Term
    		/// </summary>        
    	//    [DisplayName("Payment Term")]
    		public string  PaymentTerm { get; set; }
    
    		    
    		/// <summary>
    		/// Remittance Method
    		/// </summary>        
    	//    [DisplayName("Remittance Method")]
    		public string  RemittanceMethod { get; set; }
    
    		    
    		/// <summary>
    		/// Insurance
    		/// </summary>        
    	//    [DisplayName("Insurance")]
    		public string  Insurance { get; set; }
    
    		    
    		/// <summary>
    		/// Other Information
    		/// </summary>        
    	//    [DisplayName("Other Information")]
    		public string  OtherInformation { get; set; }
    
    		    
    		/// <summary>
    		/// Settlement
    		/// </summary>        
    	//    [DisplayName("Settlement")]
            [MaxLength(50, ErrorMessage = "Settlement cannot be longer than 50 characters")]
    		public string  Settlement { get; set; }
    
    		    
    		/// <summary>
    		/// Start Date
    		/// </summary>        
    	//    [DisplayName("Start Date")]
    		public Nullable<System.DateTime>  StartDate { get; set; }
    
    		    
    		/// <summary>
    		/// End Date
    		/// </summary>        
    	//    [DisplayName("End Date")]
    		public Nullable<System.DateTime>  EndDate { get; set; }
    
    		    
    		/// <summary>
    		/// Remark
    		/// </summary>        
    	//    [DisplayName("Remark")]
            [MaxLength(200, ErrorMessage = "Remark cannot be longer than 200 characters")]
    		public string  Remark { get; set; }
    
    		    
    		/// <summary>
    		/// Agent ID
    		/// </summary>        
    	//    [DisplayName("Agent ID")]
    		public Nullable<int>  AgentID { get; set; }
    
    		    
    		/// <summary>
    		/// Created By
    		/// </summary>        
    	//    [DisplayName("Created By")]
            [MaxLength(6, ErrorMessage = "Created By cannot be longer than 6 characters")]
    		public string  CreatedBy { get; set; }
    
    		    
    		/// <summary>
    		/// Created Date
    		/// </summary>        
    	//    [DisplayName("Created Date")]
            [Required(ErrorMessage = "Created Date is required")]
    		public System.DateTime  CreatedDate { get; set; }
    
    		    
    		/// <summary>
    		/// Updated By
    		/// </summary>        
    	//    [DisplayName("Updated By")]
            [MaxLength(6, ErrorMessage = "Updated By cannot be longer than 6 characters")]
    		public string  UpdatedBy { get; set; }
    
    		    
    		/// <summary>
    		/// Updated Date
    		/// </summary>        
    	//    [DisplayName("Updated Date")]
    		public Nullable<System.DateTime>  UpdatedDate { get; set; }
    
    		    
    		/// <summary>
    		/// Version
    		/// </summary>        
    	//    [DisplayName("Version")]
            [Required(ErrorMessage = "Version is required")]
            [MaxLength(8, ErrorMessage = "Version cannot be longer than 8 characters")]
    		public byte[]  Version { get; set; }
    
    		    
    		/// <summary>
    		/// Licenses
    		/// </summary>        
    	//    [DisplayName("Licenses")]
            [MaxLength(500, ErrorMessage = "Licenses cannot be longer than 500 characters")]
    		public string  Licenses { get; set; }
    
    		    
    	}
    //}
    
}
