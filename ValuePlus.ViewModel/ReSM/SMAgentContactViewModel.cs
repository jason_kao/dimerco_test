using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ValuePlus.ViewModel;



namespace ValuePlus.ViewModel.ReSM
{
    
    /// <summary>
    /// SMAgentContact class
    /// </summary>
    //[MetadataType(typeof(SMAgentContactViewModel))]
    //public  partial class SMAgentContact
    //{
    
    	/// <summary>
    	/// SMAgentContact Metadata class
    	/// </summary>
    	public   class SMAgentContactViewModel:ViewModelBase
    	{
    		    
    		/// <summary>
    		/// HQID
    		/// </summary>        
    	//    [DisplayName("HQID")]
            [Required(ErrorMessage = "HQID is required")]
    		public int  HQID { get; set; }
    
    		    
    		/// <summary>
    		/// Agentid
    		/// </summary>        
    	//    [DisplayName("Agentid")]
    		public Nullable<int>  Agentid { get; set; }
    
    		    
    		/// <summary>
    		/// Password
    		/// </summary>        
    	//    [DisplayName("Password")]
            [MaxLength(50, ErrorMessage = "Password cannot be longer than 50 characters")]
    		public string  Password { get; set; }
    
    		    
    		/// <summary>
    		/// Title
    		/// </summary>        
    	//    [DisplayName("Title")]
            [MaxLength(50, ErrorMessage = "Title cannot be longer than 50 characters")]
    		public string  Title { get; set; }
    
    		    
    		/// <summary>
    		/// Contact Person
    		/// </summary>        
    	//    [DisplayName("Contact Person")]
            [MaxLength(200, ErrorMessage = "Contact Person cannot be longer than 200 characters")]
    		public string  ContactPerson { get; set; }
    
    		    
    		/// <summary>
    		/// Job Title
    		/// </summary>        
    	//    [DisplayName("Job Title")]
            [MaxLength(200, ErrorMessage = "Job Title cannot be longer than 200 characters")]
    		public string  JobTitle { get; set; }
    
    		    
    		/// <summary>
    		/// Depart
    		/// </summary>        
    	//    [DisplayName("Depart")]
            [MaxLength(50, ErrorMessage = "Depart cannot be longer than 50 characters")]
    		public string  Depart { get; set; }
    
    		    
    		/// <summary>
    		/// Tel Phone
    		/// </summary>        
    	//    [DisplayName("Tel Phone")]
            [MaxLength(200, ErrorMessage = "Tel Phone cannot be longer than 200 characters")]
    		public string  TelPhone { get; set; }
    
    		    
    		/// <summary>
    		/// e Mail
    		/// </summary>        
    	//    [DisplayName("e Mail")]
            [MaxLength(200, ErrorMessage = "e Mail cannot be longer than 200 characters")]
    		public string  eMail { get; set; }
    
    		    
    		/// <summary>
    		/// Mobile
    		/// </summary>        
    	//    [DisplayName("Mobile")]
            [MaxLength(200, ErrorMessage = "Mobile cannot be longer than 200 characters")]
    		public string  Mobile { get; set; }
    
    		    
    		/// <summary>
    		/// Ext Show
    		/// </summary>        
    	//    [DisplayName("Ext Show")]
            [MaxLength(10, ErrorMessage = "Ext Show cannot be longer than 10 characters")]
    		public string  ExtShow { get; set; }
    
    		    
    		/// <summary>
    		/// Seq
    		/// </summary>        
    	//    [DisplayName("Seq")]
    		public Nullable<int>  Seq { get; set; }
    
    		    
    		/// <summary>
    		/// Admin
    		/// </summary>        
    	//    [DisplayName("Admin")]
            [MaxLength(1, ErrorMessage = "Admin cannot be longer than 1 characters")]
    		public string  Admin { get; set; }
    
    		    
    		/// <summary>
    		/// Status
    		/// </summary>        
    	//    [DisplayName("Status")]
            [Required(ErrorMessage = "Status is required")]
            [MaxLength(10, ErrorMessage = "Status cannot be longer than 10 characters")]
    		public string  Status { get; set; }
    
    		    
    		/// <summary>
    		/// Ext Phone
    		/// </summary>        
    	//    [DisplayName("Ext Phone")]
            [MaxLength(10, ErrorMessage = "Ext Phone cannot be longer than 10 characters")]
    		public string  ExtPhone { get; set; }
    
    		    
    		/// <summary>
    		/// Fax No
    		/// </summary>        
    	//    [DisplayName("Fax No")]
            [MaxLength(100, ErrorMessage = "Fax No cannot be longer than 100 characters")]
    		public string  FaxNo { get; set; }
    
    		    
    		/// <summary>
    		/// Created By
    		/// </summary>        
    	//    [DisplayName("Created By")]
            [MaxLength(50, ErrorMessage = "Created By cannot be longer than 50 characters")]
    		public string  CreatedBy { get; set; }
    
    		    
    		/// <summary>
    		/// Created Date
    		/// </summary>        
    	//    [DisplayName("Created Date")]
    		public Nullable<System.DateTime>  CreatedDate { get; set; }
    
    		    
    		/// <summary>
    		/// Updated By
    		/// </summary>        
    	//    [DisplayName("Updated By")]
            [MaxLength(50, ErrorMessage = "Updated By cannot be longer than 50 characters")]
    		public string  UpdatedBy { get; set; }
    
    		    
    		/// <summary>
    		/// Updated Date
    		/// </summary>        
    	//    [DisplayName("Updated Date")]
    		public Nullable<System.DateTime>  UpdatedDate { get; set; }
    
    		    
    	}
    //}
    
}
