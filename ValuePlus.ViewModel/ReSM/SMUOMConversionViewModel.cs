using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ValuePlus.ViewModel;



namespace ValuePlus.ViewModel.ReSM
{
    
    /// <summary>
    /// SMUOMConversion class
    /// </summary>
    //[MetadataType(typeof(SMUOMConversionViewModel))]
    //public  partial class SMUOMConversion
    //{
    
    	/// <summary>
    	/// SMUOMConversion Metadata class
    	/// </summary>
    	public   class SMUOMConversionViewModel:ViewModelBase
    	{
    		    
    		/// <summary>
    		/// HQID
    		/// </summary>        
    	//    [DisplayName("HQID")]
            [Required(ErrorMessage = "HQID is required")]
    		public int  HQID { get; set; }
    
    		    
    		/// <summary>
    		/// UOMFrom ID
    		/// </summary>        
    	//    [DisplayName("UOMFrom ID")]
            [Required(ErrorMessage = "UOMFrom ID is required")]
    		public int  UOMFromID { get; set; }
    
    		    
    		/// <summary>
    		/// UOMTo ID
    		/// </summary>        
    	//    [DisplayName("UOMTo ID")]
            [Required(ErrorMessage = "UOMTo ID is required")]
    		public int  UOMToID { get; set; }
    
    		    
    		/// <summary>
    		/// Product Line ID
    		/// </summary>        
    	//    [DisplayName("Product Line ID")]
            [Required(ErrorMessage = "Product Line ID is required")]
    		public int  ProductLineID { get; set; }
    
    		    
    		/// <summary>
    		/// Factor
    		/// </summary>        
    	//    [DisplayName("Factor")]
            [Required(ErrorMessage = "Factor is required")]
    		public decimal  Factor { get; set; }
    
    		    
    		/// <summary>
    		/// Decimal
    		/// </summary>        
    	//    [DisplayName("Decimal")]
    		public Nullable<decimal>  Decimal { get; set; }
    
    		    
    		/// <summary>
    		/// Rounding
    		/// </summary>        
    	//    [DisplayName("Rounding")]
    		public Nullable<decimal>  Rounding { get; set; }
    
    		    
    		/// <summary>
    		/// Status
    		/// </summary>        
    	//    [DisplayName("Status")]
            [Required(ErrorMessage = "Status is required")]
            [MaxLength(10, ErrorMessage = "Status cannot be longer than 10 characters")]
    		public string  Status { get; set; }
    
    		    
    		/// <summary>
    		/// Created By
    		/// </summary>        
    	//    [DisplayName("Created By")]
            [MaxLength(6, ErrorMessage = "Created By cannot be longer than 6 characters")]
    		public string  CreatedBy { get; set; }
    
    		    
    		/// <summary>
    		/// Created Date
    		/// </summary>        
    	//    [DisplayName("Created Date")]
            [Required(ErrorMessage = "Created Date is required")]
    		public System.DateTime  CreatedDate { get; set; }
    
    		    
    		/// <summary>
    		/// Updated By
    		/// </summary>        
    	//    [DisplayName("Updated By")]
            [MaxLength(6, ErrorMessage = "Updated By cannot be longer than 6 characters")]
    		public string  UpdatedBy { get; set; }
    
    		    
    		/// <summary>
    		/// Updated Date
    		/// </summary>        
    	//    [DisplayName("Updated Date")]
    		public Nullable<System.DateTime>  UpdatedDate { get; set; }
    
    		    
    		/// <summary>
    		/// Version
    		/// </summary>        
    	//    [DisplayName("Version")]
            [Required(ErrorMessage = "Version is required")]
            [MaxLength(8, ErrorMessage = "Version cannot be longer than 8 characters")]
    		public byte[]  Version { get; set; }
    
    		    
    	}
    //}
    
}
