using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ValuePlus.ViewModel;



namespace ValuePlus.ViewModel.ReSM
{
    
    /// <summary>
    /// SMANLevelRange class
    /// </summary>
    //[MetadataType(typeof(SMANLevelRangeViewModel))]
    //public  partial class SMANLevelRange
    //{
    
    	/// <summary>
    	/// SMANLevelRange Metadata class
    	/// </summary>
    	public   class SMANLevelRangeViewModel:ViewModelBase
    	{
    		    
    		/// <summary>
    		/// HQID
    		/// </summary>        
    	//    [DisplayName("HQID")]
            [Required(ErrorMessage = "HQID is required")]
    		public int  HQID { get; set; }
    
    		    
    		/// <summary>
    		/// ANDIMCode
    		/// </summary>        
    	//    [DisplayName("ANDIMCode")]
            [Required(ErrorMessage = "ANDIMCode is required")]
            [MaxLength(4, ErrorMessage = "ANDIMCode cannot be longer than 4 characters")]
    		public string  ANDIMCode { get; set; }
    
    		    
    		/// <summary>
    		/// Level Code
    		/// </summary>        
    	//    [DisplayName("Level Code")]
            [Required(ErrorMessage = "Level Code is required")]
            [MaxLength(3, ErrorMessage = "Level Code cannot be longer than 3 characters")]
    		public string  LevelCode { get; set; }
    
    		    
    		/// <summary>
    		/// Range Start
    		/// </summary>        
    	//    [DisplayName("Range Start")]
            [Required(ErrorMessage = "Range Start is required")]
    		public decimal  RangeStart { get; set; }
    
    		    
    		/// <summary>
    		/// Range End
    		/// </summary>        
    	//    [DisplayName("Range End")]
            [Required(ErrorMessage = "Range End is required")]
    		public decimal  RangeEnd { get; set; }
    
    		    
    		/// <summary>
    		/// Created By
    		/// </summary>        
    	//    [DisplayName("Created By")]
            [Required(ErrorMessage = "Created By is required")]
            [MaxLength(6, ErrorMessage = "Created By cannot be longer than 6 characters")]
    		public string  CreatedBy { get; set; }
    
    		    
    		/// <summary>
    		/// Created Date
    		/// </summary>        
    	//    [DisplayName("Created Date")]
            [Required(ErrorMessage = "Created Date is required")]
    		public System.DateTime  CreatedDate { get; set; }
    
    		    
    		/// <summary>
    		/// Updated By
    		/// </summary>        
    	//    [DisplayName("Updated By")]
            [MaxLength(6, ErrorMessage = "Updated By cannot be longer than 6 characters")]
    		public string  UpdatedBy { get; set; }
    
    		    
    		/// <summary>
    		/// Updated Date
    		/// </summary>        
    	//    [DisplayName("Updated Date")]
    		public Nullable<System.DateTime>  UpdatedDate { get; set; }
    
    		    
    	}
    //}
    
}
