using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ValuePlus.ViewModel;



namespace ValuePlus.ViewModel.ReSM
{
    
    /// <summary>
    /// SMCustomerAddress class
    /// </summary>
    //[MetadataType(typeof(SMCustomerAddressViewModel))]
    //public  partial class SMCustomerAddress
    //{
    
    	/// <summary>
    	/// SMCustomerAddress Metadata class
    	/// </summary>
    	public   class SMCustomerAddressViewModel:ViewModelBase
    	{
    		    
    		/// <summary>
    		/// HQID
    		/// </summary>        
    	//    [DisplayName("HQID")]
            [Required(ErrorMessage = "HQID is required")]
    		public int  HQID { get; set; }
    
    		    
    		/// <summary>
    		/// Customer ID
    		/// </summary>        
    	//    [DisplayName("Customer ID")]
            [Required(ErrorMessage = "Customer ID is required")]
    		public int  CustomerID { get; set; }
    
    		    
    		/// <summary>
    		/// Form Type
    		/// </summary>        
    	//    [DisplayName("Form Type")]
            [MaxLength(500, ErrorMessage = "Form Type cannot be longer than 500 characters")]
    		public string  FormType { get; set; }
    
    		    
    		/// <summary>
    		/// Mode
    		/// </summary>        
    	//    [DisplayName("Mode")]
            [MaxLength(500, ErrorMessage = "Mode cannot be longer than 500 characters")]
    		public string  Mode { get; set; }
    
    		    
    		/// <summary>
    		/// Customer Name
    		/// </summary>        
    	//    [DisplayName("Customer Name")]
            [MaxLength(255, ErrorMessage = "Customer Name cannot be longer than 255 characters")]
    		public string  CustomerName { get; set; }
    
    		    
    		/// <summary>
    		/// Customer Address1
    		/// </summary>        
    	//    [DisplayName("Customer Address1")]
            [MaxLength(255, ErrorMessage = "Customer Address1 cannot be longer than 255 characters")]
    		public string  CustomerAddress1 { get; set; }
    
    		    
    		/// <summary>
    		/// Customer Address2
    		/// </summary>        
    	//    [DisplayName("Customer Address2")]
            [MaxLength(255, ErrorMessage = "Customer Address2 cannot be longer than 255 characters")]
    		public string  CustomerAddress2 { get; set; }
    
    		    
    		/// <summary>
    		/// Customer Address3
    		/// </summary>        
    	//    [DisplayName("Customer Address3")]
            [MaxLength(255, ErrorMessage = "Customer Address3 cannot be longer than 255 characters")]
    		public string  CustomerAddress3 { get; set; }
    
    		    
    		/// <summary>
    		/// Customer Address4
    		/// </summary>        
    	//    [DisplayName("Customer Address4")]
            [MaxLength(255, ErrorMessage = "Customer Address4 cannot be longer than 255 characters")]
    		public string  CustomerAddress4 { get; set; }
    
    		    
    		/// <summary>
    		/// Customer Address5
    		/// </summary>        
    	//    [DisplayName("Customer Address5")]
            [MaxLength(255, ErrorMessage = "Customer Address5 cannot be longer than 255 characters")]
    		public string  CustomerAddress5 { get; set; }
    
    		    
    		/// <summary>
    		/// Customer Phone
    		/// </summary>        
    	//    [DisplayName("Customer Phone")]
            [MaxLength(50, ErrorMessage = "Customer Phone cannot be longer than 50 characters")]
    		public string  CustomerPhone { get; set; }
    
    		    
    		/// <summary>
    		/// Customer Fax
    		/// </summary>        
    	//    [DisplayName("Customer Fax")]
            [MaxLength(50, ErrorMessage = "Customer Fax cannot be longer than 50 characters")]
    		public string  CustomerFax { get; set; }
    
    		    
    		/// <summary>
    		/// Customer Email
    		/// </summary>        
    	//    [DisplayName("Customer Email")]
            [MaxLength(50, ErrorMessage = "Customer Email cannot be longer than 50 characters")]
    		public string  CustomerEmail { get; set; }
    
    		    
    		/// <summary>
    		/// Status
    		/// </summary>        
    	//    [DisplayName("Status")]
            [Required(ErrorMessage = "Status is required")]
            [MaxLength(50, ErrorMessage = "Status cannot be longer than 50 characters")]
    		public string  Status { get; set; }
    
    		    
    		/// <summary>
    		/// Created By
    		/// </summary>        
    	//    [DisplayName("Created By")]
            [MaxLength(6, ErrorMessage = "Created By cannot be longer than 6 characters")]
    		public string  CreatedBy { get; set; }
    
    		    
    		/// <summary>
    		/// Created Date
    		/// </summary>        
    	//    [DisplayName("Created Date")]
            [Required(ErrorMessage = "Created Date is required")]
    		public System.DateTime  CreatedDate { get; set; }
    
    		    
    		/// <summary>
    		/// Updated By
    		/// </summary>        
    	//    [DisplayName("Updated By")]
            [MaxLength(6, ErrorMessage = "Updated By cannot be longer than 6 characters")]
    		public string  UpdatedBy { get; set; }
    
    		    
    		/// <summary>
    		/// Updated Date
    		/// </summary>        
    	//    [DisplayName("Updated Date")]
    		public Nullable<System.DateTime>  UpdatedDate { get; set; }
    
    		    
    		/// <summary>
    		/// ZIP
    		/// </summary>        
    	//    [DisplayName("ZIP")]
            [MaxLength(50, ErrorMessage = "ZIP cannot be longer than 50 characters")]
    		public string  ZIP { get; set; }
    
    		    
    		/// <summary>
    		/// Country ID
    		/// </summary>        
    	//    [DisplayName("Country ID")]
    		public Nullable<int>  CountryID { get; set; }
    
    		    
    		/// <summary>
    		/// State ID
    		/// </summary>        
    	//    [DisplayName("State ID")]
    		public Nullable<int>  StateID { get; set; }
    
    		    
    		/// <summary>
    		/// City ID
    		/// </summary>        
    	//    [DisplayName("City ID")]
    		public Nullable<int>  CityID { get; set; }
    
    		    
    		/// <summary>
    		/// City Name
    		/// </summary>        
    	//    [DisplayName("City Name")]
            [MaxLength(50, ErrorMessage = "City Name cannot be longer than 50 characters")]
    		public string  CityName { get; set; }
    
    		    
    		/// <summary>
    		/// Area Code
    		/// </summary>        
    	//    [DisplayName("Area Code")]
            [MaxLength(50, ErrorMessage = "Area Code cannot be longer than 50 characters")]
    		public string  AreaCode { get; set; }
    
    		    
    	}
    //}
    
}
