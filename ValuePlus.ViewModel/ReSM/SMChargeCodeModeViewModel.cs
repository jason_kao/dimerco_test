using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ValuePlus.ViewModel;



namespace ValuePlus.ViewModel.ReSM
{
    
    /// <summary>
    /// SMChargeCodeMode class
    /// </summary>
    //[MetadataType(typeof(SMChargeCodeModeViewModel))]
    //public  partial class SMChargeCodeMode
    //{
    
    	/// <summary>
    	/// SMChargeCodeMode Metadata class
    	/// </summary>
    	public   class SMChargeCodeModeViewModel:ViewModelBase
    	{
    		    
    		/// <summary>
    		/// HQID
    		/// </summary>        
    	//    [DisplayName("HQID")]
            [Required(ErrorMessage = "HQID is required")]
    		public int  HQID { get; set; }
    
    		    
    		/// <summary>
    		/// Charge Code ID
    		/// </summary>        
    	//    [DisplayName("Charge Code ID")]
            [Required(ErrorMessage = "Charge Code ID is required")]
    		public int  ChargeCodeID { get; set; }
    
    		    
    		/// <summary>
    		/// Mode ID
    		/// </summary>        
    	//    [DisplayName("Mode ID")]
            [Required(ErrorMessage = "Mode ID is required")]
    		public int  ModeID { get; set; }
    
    		    
    		/// <summary>
    		/// Is Used
    		/// </summary>        
    	//    [DisplayName("Is Used")]
    		public Nullable<bool>  IsUsed { get; set; }
    
    		    
    		/// <summary>
    		/// CSType
    		/// </summary>        
    	//    [DisplayName("CSType")]
            [MaxLength(50, ErrorMessage = "CSType cannot be longer than 50 characters")]
    		public string  CSType { get; set; }
    
    		    
    		/// <summary>
    		/// Status
    		/// </summary>        
    	//    [DisplayName("Status")]
            [Required(ErrorMessage = "Status is required")]
            [MaxLength(10, ErrorMessage = "Status cannot be longer than 10 characters")]
    		public string  Status { get; set; }
    
    		    
    		/// <summary>
    		/// Created By
    		/// </summary>        
    	//    [DisplayName("Created By")]
            [MaxLength(6, ErrorMessage = "Created By cannot be longer than 6 characters")]
    		public string  CreatedBy { get; set; }
    
    		    
    		/// <summary>
    		/// Created Date
    		/// </summary>        
    	//    [DisplayName("Created Date")]
            [Required(ErrorMessage = "Created Date is required")]
    		public System.DateTime  CreatedDate { get; set; }
    
    		    
    		/// <summary>
    		/// Updated By
    		/// </summary>        
    	//    [DisplayName("Updated By")]
            [MaxLength(6, ErrorMessage = "Updated By cannot be longer than 6 characters")]
    		public string  UpdatedBy { get; set; }
    
    		    
    		/// <summary>
    		/// Updated Date
    		/// </summary>        
    	//    [DisplayName("Updated Date")]
    		public Nullable<System.DateTime>  UpdatedDate { get; set; }
    
    		    
    		/// <summary>
    		/// Version
    		/// </summary>        
    	//    [DisplayName("Version")]
            [MaxLength(8, ErrorMessage = "Version cannot be longer than 8 characters")]
    		public byte[]  Version { get; set; }
    
    		    
    		/// <summary>
    		/// UOMType
    		/// </summary>        
    	//    [DisplayName("UOMType")]
            [MaxLength(50, ErrorMessage = "UOMType cannot be longer than 50 characters")]
    		public string  UOMType { get; set; }
    
    		    
    	}
    //}
    
}
