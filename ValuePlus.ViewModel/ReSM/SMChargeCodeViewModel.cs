using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ValuePlus.ViewModel;



namespace ValuePlus.ViewModel.ReSM
{
    
    /// <summary>
    /// SMChargeCode class
    /// </summary>
    //[MetadataType(typeof(SMChargeCodeViewModel))]
    //public  partial class SMChargeCode
    //{
    
    	/// <summary>
    	/// SMChargeCode Metadata class
    	/// </summary>
    	public   class SMChargeCodeViewModel:ViewModelBase
    	{
    		    
    		/// <summary>
    		/// HQID
    		/// </summary>        
    	//    [DisplayName("HQID")]
            [Required(ErrorMessage = "HQID is required")]
    		public int  HQID { get; set; }
    
    		    
    		/// <summary>
    		/// Charge Code
    		/// </summary>        
    	//    [DisplayName("Charge Code")]
            [Required(ErrorMessage = "Charge Code is required")]
            [MaxLength(10, ErrorMessage = "Charge Code cannot be longer than 10 characters")]
    		public string  ChargeCode { get; set; }
    
    		    
    		/// <summary>
    		/// Sub Category ID
    		/// </summary>        
    	//    [DisplayName("Sub Category ID")]
            [Required(ErrorMessage = "Sub Category ID is required")]
    		public int  SubCategoryID { get; set; }
    
    		    
    		/// <summary>
    		/// Charge Code Name
    		/// </summary>        
    	//    [DisplayName("Charge Code Name")]
            [MaxLength(255, ErrorMessage = "Charge Code Name cannot be longer than 255 characters")]
    		public string  ChargeCodeName { get; set; }
    
    		    
    		/// <summary>
    		/// GLSales
    		/// </summary>        
    	//    [DisplayName("GLSales")]
            [MaxLength(50, ErrorMessage = "GLSales cannot be longer than 50 characters")]
    		public string  GLSales { get; set; }
    
    		    
    		/// <summary>
    		/// GLCost
    		/// </summary>        
    	//    [DisplayName("GLCost")]
            [MaxLength(50, ErrorMessage = "GLCost cannot be longer than 50 characters")]
    		public string  GLCost { get; set; }
    
    		    
    		/// <summary>
    		/// GLExpense
    		/// </summary>        
    	//    [DisplayName("GLExpense")]
            [MaxLength(50, ErrorMessage = "GLExpense cannot be longer than 50 characters")]
    		public string  GLExpense { get; set; }
    
    		    
    		/// <summary>
    		/// Due To
    		/// </summary>        
    	//    [DisplayName("Due To")]
            [MaxLength(50, ErrorMessage = "Due To cannot be longer than 50 characters")]
    		public string  DueTo { get; set; }
    
    		    
    		/// <summary>
    		/// Apply To
    		/// </summary>        
    	//    [DisplayName("Apply To")]
            [MaxLength(50, ErrorMessage = "Apply To cannot be longer than 50 characters")]
    		public string  ApplyTo { get; set; }
    
    		    
    		/// <summary>
    		/// Apply To Code
    		/// </summary>        
    	//    [DisplayName("Apply To Code")]
            [MaxLength(255, ErrorMessage = "Apply To Code cannot be longer than 255 characters")]
    		public string  ApplyToCode { get; set; }
    
    		    
    		/// <summary>
    		/// UOM
    		/// </summary>        
    	//    [DisplayName("UOM")]
            [MaxLength(255, ErrorMessage = "UOM cannot be longer than 255 characters")]
    		public string  UOM { get; set; }
    
    		    
    		/// <summary>
    		/// Status
    		/// </summary>        
    	//    [DisplayName("Status")]
            [Required(ErrorMessage = "Status is required")]
            [MaxLength(10, ErrorMessage = "Status cannot be longer than 10 characters")]
    		public string  Status { get; set; }
    
    		    
    		/// <summary>
    		/// Created By
    		/// </summary>        
    	//    [DisplayName("Created By")]
            [MaxLength(6, ErrorMessage = "Created By cannot be longer than 6 characters")]
    		public string  CreatedBy { get; set; }
    
    		    
    		/// <summary>
    		/// Created Date
    		/// </summary>        
    	//    [DisplayName("Created Date")]
            [Required(ErrorMessage = "Created Date is required")]
    		public System.DateTime  CreatedDate { get; set; }
    
    		    
    		/// <summary>
    		/// Updated By
    		/// </summary>        
    	//    [DisplayName("Updated By")]
            [MaxLength(6, ErrorMessage = "Updated By cannot be longer than 6 characters")]
    		public string  UpdatedBy { get; set; }
    
    		    
    		/// <summary>
    		/// Updated Date
    		/// </summary>        
    	//    [DisplayName("Updated Date")]
    		public Nullable<System.DateTime>  UpdatedDate { get; set; }
    
    		    
    		/// <summary>
    		/// Version
    		/// </summary>        
    	//    [DisplayName("Version")]
            [Required(ErrorMessage = "Version is required")]
            [MaxLength(8, ErrorMessage = "Version cannot be longer than 8 characters")]
    		public byte[]  Version { get; set; }
    
    		    
    		/// <summary>
    		/// DAWBDue To
    		/// </summary>        
    	//    [DisplayName("DAWBDue To")]
            [MaxLength(10, ErrorMessage = "DAWBDue To cannot be longer than 10 characters")]
    		public string  DAWBDueTo { get; set; }
    
    		    
    		/// <summary>
    		/// MBLDue To
    		/// </summary>        
    	//    [DisplayName("MBLDue To")]
            [MaxLength(10, ErrorMessage = "MBLDue To cannot be longer than 10 characters")]
    		public string  MBLDueTo { get; set; }
    
    		    
    		/// <summary>
    		/// Sale Cost Type
    		/// </summary>        
    	//    [DisplayName("Sale Cost Type")]
            [MaxLength(20, ErrorMessage = "Sale Cost Type cannot be longer than 20 characters")]
    		public string  SaleCostType { get; set; }
    
    		    
    		/// <summary>
    		/// Sale Cost Type2
    		/// </summary>        
    	//    [DisplayName("Sale Cost Type2")]
            [MaxLength(20, ErrorMessage = "Sale Cost Type2 cannot be longer than 20 characters")]
    		public string  SaleCostType2 { get; set; }
    
    		    
    		/// <summary>
    		/// Remark
    		/// </summary>        
    	//    [DisplayName("Remark")]
    		public string  Remark { get; set; }
    
    		    
    		/// <summary>
    		/// Is Profit Share
    		/// </summary>        
    	//    [DisplayName("Is Profit Share")]
            [Required(ErrorMessage = "Is Profit Share is required")]
    		public bool  IsProfitShare { get; set; }
    
    		    
    		/// <summary>
    		/// Invoice Type
    		/// </summary>        
    	//    [DisplayName("Invoice Type")]
    		public Nullable<int>  InvoiceType { get; set; }
    
    		    
    	}
    //}
    
}
