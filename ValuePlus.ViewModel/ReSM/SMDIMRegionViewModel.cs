using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ValuePlus.ViewModel;



namespace ValuePlus.ViewModel.ReSM
{
    
    /// <summary>
    /// SMDIMRegion class
    /// </summary>
    //[MetadataType(typeof(SMDIMRegionViewModel))]
    //public  partial class SMDIMRegion
    //{
    
    	/// <summary>
    	/// SMDIMRegion Metadata class
    	/// </summary>
    	public   class SMDIMRegionViewModel:ViewModelBase
    	{
    		    
    		/// <summary>
    		/// HQID
    		/// </summary>        
    	//    [DisplayName("HQID")]
            [Required(ErrorMessage = "HQID is required")]
    		public int  HQID { get; set; }
    
    		    
    		/// <summary>
    		/// Dim Region Code
    		/// </summary>        
    	//    [DisplayName("Dim Region Code")]
            [Required(ErrorMessage = "Dim Region Code is required")]
            [MaxLength(10, ErrorMessage = "Dim Region Code cannot be longer than 10 characters")]
    		public string  DimRegionCode { get; set; }
    
    		    
    		/// <summary>
    		/// Dim Region Name
    		/// </summary>        
    	//    [DisplayName("Dim Region Name")]
            [MaxLength(255, ErrorMessage = "Dim Region Name cannot be longer than 255 characters")]
    		public string  DimRegionName { get; set; }
    
    		    
    		/// <summary>
    		/// Status
    		/// </summary>        
    	//    [DisplayName("Status")]
            [Required(ErrorMessage = "Status is required")]
            [MaxLength(10, ErrorMessage = "Status cannot be longer than 10 characters")]
    		public string  Status { get; set; }
    
    		    
    		/// <summary>
    		/// Created By
    		/// </summary>        
    	//    [DisplayName("Created By")]
            [MaxLength(6, ErrorMessage = "Created By cannot be longer than 6 characters")]
    		public string  CreatedBy { get; set; }
    
    		    
    		/// <summary>
    		/// Created Date
    		/// </summary>        
    	//    [DisplayName("Created Date")]
            [Required(ErrorMessage = "Created Date is required")]
    		public System.DateTime  CreatedDate { get; set; }
    
    		    
    		/// <summary>
    		/// Updated By
    		/// </summary>        
    	//    [DisplayName("Updated By")]
            [MaxLength(6, ErrorMessage = "Updated By cannot be longer than 6 characters")]
    		public string  UpdatedBy { get; set; }
    
    		    
    		/// <summary>
    		/// Updated Date
    		/// </summary>        
    	//    [DisplayName("Updated Date")]
    		public Nullable<System.DateTime>  UpdatedDate { get; set; }
    
    		    
    		/// <summary>
    		/// Version
    		/// </summary>        
    	//    [DisplayName("Version")]
            [Required(ErrorMessage = "Version is required")]
            [MaxLength(8, ErrorMessage = "Version cannot be longer than 8 characters")]
    		public byte[]  Version { get; set; }
    
    		    
    		/// <summary>
    		/// Is Block Shpt
    		/// </summary>        
    	//    [DisplayName("Is Block Shpt")]
            [MaxLength(1, ErrorMessage = "Is Block Shpt cannot be longer than 1 characters")]
    		public string  IsBlockShpt { get; set; }
    
    		    
    	}
    //}
    
}
