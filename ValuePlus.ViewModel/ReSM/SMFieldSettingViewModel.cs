using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ValuePlus.ViewModel;



namespace ValuePlus.ViewModel.ReSM
{
    
    /// <summary>
    /// SMFieldSetting class
    /// </summary>
    //[MetadataType(typeof(SMFieldSettingViewModel))]
    //public  partial class SMFieldSetting
    //{
    
    	/// <summary>
    	/// SMFieldSetting Metadata class
    	/// </summary>
    	public   class SMFieldSettingViewModel:ViewModelBase
    	{
    		    
    		/// <summary>
    		/// HQID
    		/// </summary>        
    	//    [DisplayName("HQID")]
            [Required(ErrorMessage = "HQID is required")]
    		public int  HQID { get; set; }
    
    		    
    		/// <summary>
    		/// Field Type
    		/// </summary>        
    	//    [DisplayName("Field Type")]
            [MaxLength(10, ErrorMessage = "Field Type cannot be longer than 10 characters")]
    		public string  FieldType { get; set; }
    
    		    
    		/// <summary>
    		/// Product Line
    		/// </summary>        
    	//    [DisplayName("Product Line")]
    		public Nullable<int>  ProductLine { get; set; }
    
    		    
    		/// <summary>
    		/// Field Name
    		/// </summary>        
    	//    [DisplayName("Field Name")]
            [MaxLength(50, ErrorMessage = "Field Name cannot be longer than 50 characters")]
    		public string  FieldName { get; set; }
    
    		    
    		/// <summary>
    		/// Show Name
    		/// </summary>        
    	//    [DisplayName("Show Name")]
            [MaxLength(50, ErrorMessage = "Show Name cannot be longer than 50 characters")]
    		public string  ShowName { get; set; }
    
    		    
    		/// <summary>
    		/// Is Extra
    		/// </summary>        
    	//    [DisplayName("Is Extra")]
    		public Nullable<bool>  IsExtra { get; set; }
    
    		    
    		/// <summary>
    		/// Created By
    		/// </summary>        
    	//    [DisplayName("Created By")]
            [MaxLength(10, ErrorMessage = "Created By cannot be longer than 10 characters")]
    		public string  CreatedBy { get; set; }
    
    		    
    		/// <summary>
    		/// Created Date
    		/// </summary>        
    	//    [DisplayName("Created Date")]
    		public Nullable<System.DateTime>  CreatedDate { get; set; }
    
    		    
    		/// <summary>
    		/// Updated By
    		/// </summary>        
    	//    [DisplayName("Updated By")]
            [MaxLength(10, ErrorMessage = "Updated By cannot be longer than 10 characters")]
    		public string  UpdatedBy { get; set; }
    
    		    
    		/// <summary>
    		/// Updated Date
    		/// </summary>        
    	//    [DisplayName("Updated Date")]
    		public Nullable<System.DateTime>  UpdatedDate { get; set; }
    
    		    
    	}
    //}
    
}
