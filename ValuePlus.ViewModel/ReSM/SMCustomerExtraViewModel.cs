using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ValuePlus.ViewModel;



namespace ValuePlus.ViewModel.ReSM
{
    
    /// <summary>
    /// SMCustomerExtra class
    /// </summary>
    //[MetadataType(typeof(SMCustomerExtraViewModel))]
    //public  partial class SMCustomerExtra
    //{
    
    	/// <summary>
    	/// SMCustomerExtra Metadata class
    	/// </summary>
    	public   class SMCustomerExtraViewModel:ViewModelBase
    	{
    		    
    		/// <summary>
    		/// HQID
    		/// </summary>        
    	//    [DisplayName("HQID")]
            [Required(ErrorMessage = "HQID is required")]
    		public int  HQID { get; set; }
    
    		    
    		/// <summary>
    		/// Customer ID
    		/// </summary>        
    	//    [DisplayName("Customer ID")]
            [Required(ErrorMessage = "Customer ID is required")]
    		public int  CustomerID { get; set; }
    
    		    
    		/// <summary>
    		/// Global Code
    		/// </summary>        
    	//    [DisplayName("Global Code")]
            [MaxLength(10, ErrorMessage = "Global Code cannot be longer than 10 characters")]
    		public string  GlobalCode { get; set; }
    
    		    
    		/// <summary>
    		/// Extra Type
    		/// </summary>        
    	//    [DisplayName("Extra Type")]
            [Required(ErrorMessage = "Extra Type is required")]
            [MaxLength(10, ErrorMessage = "Extra Type cannot be longer than 10 characters")]
    		public string  ExtraType { get; set; }
    
    		    
    		/// <summary>
    		/// Field Name
    		/// </summary>        
    	//    [DisplayName("Field Name")]
            [Required(ErrorMessage = "Field Name is required")]
            [MaxLength(50, ErrorMessage = "Field Name cannot be longer than 50 characters")]
    		public string  FieldName { get; set; }
    
    		    
    		/// <summary>
    		/// Map Name
    		/// </summary>        
    	//    [DisplayName("Map Name")]
            [MaxLength(50, ErrorMessage = "Map Name cannot be longer than 50 characters")]
    		public string  MapName { get; set; }
    
    		    
    		/// <summary>
    		/// Mandatory
    		/// </summary>        
    	//    [DisplayName("Mandatory")]
            [MaxLength(1, ErrorMessage = "Mandatory cannot be longer than 1 characters")]
    		public string  Mandatory { get; set; }
    
    		    
    		/// <summary>
    		/// Pickup Data
    		/// </summary>        
    	//    [DisplayName("Pickup Data")]
            [MaxLength(1000, ErrorMessage = "Pickup Data cannot be longer than 1000 characters")]
    		public string  PickupData { get; set; }
    
    		    
    		/// <summary>
    		/// Status
    		/// </summary>        
    	//    [DisplayName("Status")]
            [MaxLength(10, ErrorMessage = "Status cannot be longer than 10 characters")]
    		public string  Status { get; set; }
    
    		    
    		/// <summary>
    		/// Product Line
    		/// </summary>        
    	//    [DisplayName("Product Line")]
            [MaxLength(3, ErrorMessage = "Product Line cannot be longer than 3 characters")]
    		public string  ProductLine { get; set; }
    
    		    
    		/// <summary>
    		/// Mode Code
    		/// </summary>        
    	//    [DisplayName("Mode Code")]
            [MaxLength(2, ErrorMessage = "Mode Code cannot be longer than 2 characters")]
    		public string  ModeCode { get; set; }
    
    		    
    		/// <summary>
    		/// Created By
    		/// </summary>        
    	//    [DisplayName("Created By")]
            [MaxLength(10, ErrorMessage = "Created By cannot be longer than 10 characters")]
    		public string  CreatedBy { get; set; }
    
    		    
    		/// <summary>
    		/// Created Date
    		/// </summary>        
    	//    [DisplayName("Created Date")]
    		public Nullable<System.DateTime>  CreatedDate { get; set; }
    
    		    
    		/// <summary>
    		/// Updated By
    		/// </summary>        
    	//    [DisplayName("Updated By")]
            [MaxLength(10, ErrorMessage = "Updated By cannot be longer than 10 characters")]
    		public string  UpdatedBy { get; set; }
    
    		    
    		/// <summary>
    		/// Updated Date
    		/// </summary>        
    	//    [DisplayName("Updated Date")]
    		public Nullable<System.DateTime>  UpdatedDate { get; set; }
    
    		    
    	}
    //}
    
}
