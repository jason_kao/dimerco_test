using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ValuePlus.ViewModel;



namespace ValuePlus.ViewModel.ReSM
{
    
    /// <summary>
    /// SMANDimension class
    /// </summary>
    //[MetadataType(typeof(SMANDimensionViewModel))]
    //public  partial class SMANDimension
    //{
    
    	/// <summary>
    	/// SMANDimension Metadata class
    	/// </summary>
    	public   class SMANDimensionViewModel:ViewModelBase
    	{
    		    
    		/// <summary>
    		/// HQID
    		/// </summary>        
    	//    [DisplayName("HQID")]
            [Required(ErrorMessage = "HQID is required")]
    		public int  HQID { get; set; }
    
    		    
    		/// <summary>
    		/// ANDIMCode
    		/// </summary>        
    	//    [DisplayName("ANDIMCode")]
            [Required(ErrorMessage = "ANDIMCode is required")]
            [MaxLength(3, ErrorMessage = "ANDIMCode cannot be longer than 3 characters")]
    		public string  ANDIMCode { get; set; }
    
    		    
    		/// <summary>
    		/// ANDIMDescription
    		/// </summary>        
    	//    [DisplayName("ANDIMDescription")]
            [Required(ErrorMessage = "ANDIMDescription is required")]
            [MaxLength(30, ErrorMessage = "ANDIMDescription cannot be longer than 30 characters")]
    		public string  ANDIMDescription { get; set; }
    
    		    
    		/// <summary>
    		/// Created By
    		/// </summary>        
    	//    [DisplayName("Created By")]
            [Required(ErrorMessage = "Created By is required")]
            [MaxLength(6, ErrorMessage = "Created By cannot be longer than 6 characters")]
    		public string  CreatedBy { get; set; }
    
    		    
    		/// <summary>
    		/// Created Date
    		/// </summary>        
    	//    [DisplayName("Created Date")]
            [Required(ErrorMessage = "Created Date is required")]
    		public System.DateTime  CreatedDate { get; set; }
    
    		    
    		/// <summary>
    		/// Updated By
    		/// </summary>        
    	//    [DisplayName("Updated By")]
            [MaxLength(6, ErrorMessage = "Updated By cannot be longer than 6 characters")]
    		public string  UpdatedBy { get; set; }
    
    		    
    		/// <summary>
    		/// Updated Date
    		/// </summary>        
    	//    [DisplayName("Updated Date")]
    		public Nullable<System.DateTime>  UpdatedDate { get; set; }
    
    		    
    	}
    //}
    
}
