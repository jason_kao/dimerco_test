using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ValuePlus.ViewModel;



namespace ValuePlus.ViewModel.ReSM
{
    
    /// <summary>
    /// SMGlobalRate class
    /// </summary>
    //[MetadataType(typeof(SMGlobalRateViewModel))]
    //public  partial class SMGlobalRate
    //{
    
    	/// <summary>
    	/// SMGlobalRate Metadata class
    	/// </summary>
    	public   class SMGlobalRateViewModel:ViewModelBase
    	{
    		    
    		/// <summary>
    		/// HQID
    		/// </summary>        
    	//    [DisplayName("HQID")]
            [Required(ErrorMessage = "HQID is required")]
    		public int  HQID { get; set; }
    
    		    
    		/// <summary>
    		/// From Currency
    		/// </summary>        
    	//    [DisplayName("From Currency")]
            [MaxLength(3, ErrorMessage = "From Currency cannot be longer than 3 characters")]
    		public string  FromCurrency { get; set; }
    
    		    
    		/// <summary>
    		/// To Currency
    		/// </summary>        
    	//    [DisplayName("To Currency")]
            [MaxLength(3, ErrorMessage = "To Currency cannot be longer than 3 characters")]
    		public string  ToCurrency { get; set; }
    
    		    
    		/// <summary>
    		/// Convert Rate
    		/// </summary>        
    	//    [DisplayName("Convert Rate")]
    		public Nullable<decimal>  ConvertRate { get; set; }
    
    		    
    		/// <summary>
    		/// Rate Date
    		/// </summary>        
    	//    [DisplayName("Rate Date")]
    		public Nullable<System.DateTime>  RateDate { get; set; }
    
    		    
    	}
    //}
    
}
