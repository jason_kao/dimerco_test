using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ValuePlus.ViewModel;



namespace ValuePlus.ViewModel.ReSM
{
    
    /// <summary>
    /// SMAgentControlPL class
    /// </summary>
    //[MetadataType(typeof(SMAgentControlPLViewModel))]
    //public  partial class SMAgentControlPL
    //{
    
    	/// <summary>
    	/// SMAgentControlPL Metadata class
    	/// </summary>
    	public   class SMAgentControlPLViewModel:ViewModelBase
    	{
    		    
    		/// <summary>
    		/// HQID
    		/// </summary>        
    	//    [DisplayName("HQID")]
            [Required(ErrorMessage = "HQID is required")]
    		public int  HQID { get; set; }
    
    		    
    		/// <summary>
    		/// Vendor ID
    		/// </summary>        
    	//    [DisplayName("Vendor ID")]
    		public Nullable<int>  VendorID { get; set; }
    
    		    
    		/// <summary>
    		/// PLID
    		/// </summary>        
    	//    [DisplayName("PLID")]
    		public Nullable<int>  PLID { get; set; }
    
    		    
    		/// <summary>
    		/// Specialized Market
    		/// </summary>        
    	//    [DisplayName("Specialized Market")]
            [MaxLength(200, ErrorMessage = "Specialized Market cannot be longer than 200 characters")]
    		public string  SpecializedMarket { get; set; }
    
    		    
    		/// <summary>
    		/// Network
    		/// </summary>        
    	//    [DisplayName("Network")]
            [MaxLength(200, ErrorMessage = "Network cannot be longer than 200 characters")]
    		public string  Network { get; set; }
    
    		    
    		/// <summary>
    		/// PICPersonal
    		/// </summary>        
    	//    [DisplayName("PICPersonal")]
            [MaxLength(50, ErrorMessage = "PICPersonal cannot be longer than 50 characters")]
    		public string  PICPersonal { get; set; }
    
    		    
    		/// <summary>
    		/// Others Info
    		/// </summary>        
    	//    [DisplayName("Others Info")]
            [MaxLength(200, ErrorMessage = "Others Info cannot be longer than 200 characters")]
    		public string  OthersInfo { get; set; }
    
    		    
    		/// <summary>
    		/// Reason
    		/// </summary>        
    	//    [DisplayName("Reason")]
            [MaxLength(500, ErrorMessage = "Reason cannot be longer than 500 characters")]
    		public string  Reason { get; set; }
    
    		    
    		/// <summary>
    		/// has Coload
    		/// </summary>        
    	//    [DisplayName("has Coload")]
    		public Nullable<bool>  hasCoload { get; set; }
    
    		    
    		/// <summary>
    		/// has Borrow In
    		/// </summary>        
    	//    [DisplayName("has Borrow In")]
    		public Nullable<bool>  hasBorrowIn { get; set; }
    
    		    
    		/// <summary>
    		/// Status
    		/// </summary>        
    	//    [DisplayName("Status")]
            [MaxLength(1, ErrorMessage = "Status cannot be longer than 1 characters")]
    		public string  Status { get; set; }
    
    		    
    		/// <summary>
    		/// Create By
    		/// </summary>        
    	//    [DisplayName("Create By")]
            [MaxLength(50, ErrorMessage = "Create By cannot be longer than 50 characters")]
    		public string  CreateBy { get; set; }
    
    		    
    		/// <summary>
    		/// Create Date
    		/// </summary>        
    	//    [DisplayName("Create Date")]
    		public Nullable<System.DateTime>  CreateDate { get; set; }
    
    		    
    		/// <summary>
    		/// Update By
    		/// </summary>        
    	//    [DisplayName("Update By")]
            [MaxLength(50, ErrorMessage = "Update By cannot be longer than 50 characters")]
    		public string  UpdateBy { get; set; }
    
    		    
    		/// <summary>
    		/// Update Date
    		/// </summary>        
    	//    [DisplayName("Update Date")]
    		public Nullable<System.DateTime>  UpdateDate { get; set; }
    
    		    
    	}
    //}
    
}
