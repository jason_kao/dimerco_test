using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ValuePlus.ViewModel;



namespace ValuePlus.ViewModel.ReSM
{
    
    /// <summary>
    /// SMChargeCodePLMethod class
    /// </summary>
    //[MetadataType(typeof(SMChargeCodePLMethodViewModel))]
    //public  partial class SMChargeCodePLMethod
    //{
    
    	/// <summary>
    	/// SMChargeCodePLMethod Metadata class
    	/// </summary>
    	public   class SMChargeCodePLMethodViewModel:ViewModelBase
    	{
    		    
    		/// <summary>
    		/// HQID
    		/// </summary>        
    	//    [DisplayName("HQID")]
            [Required(ErrorMessage = "HQID is required")]
    		public int  HQID { get; set; }
    
    		    
    		/// <summary>
    		/// Charge Code ID
    		/// </summary>        
    	//    [DisplayName("Charge Code ID")]
            [Required(ErrorMessage = "Charge Code ID is required")]
    		public int  ChargeCodeID { get; set; }
    
    		    
    		/// <summary>
    		/// Product Line ID
    		/// </summary>        
    	//    [DisplayName("Product Line ID")]
            [Required(ErrorMessage = "Product Line ID is required")]
    		public int  ProductLineID { get; set; }
    
    		    
    		/// <summary>
    		/// Charge Method
    		/// </summary>        
    	//    [DisplayName("Charge Method")]
            [Required(ErrorMessage = "Charge Method is required")]
            [MaxLength(50, ErrorMessage = "Charge Method cannot be longer than 50 characters")]
    		public string  ChargeMethod { get; set; }
    
    		    
    		/// <summary>
    		/// Charge UOM
    		/// </summary>        
    	//    [DisplayName("Charge UOM")]
            [MaxLength(500, ErrorMessage = "Charge UOM cannot be longer than 500 characters")]
    		public string  ChargeUOM { get; set; }
    
    		    
    		/// <summary>
    		/// UOMPara
    		/// </summary>        
    	//    [DisplayName("UOMPara")]
    		public Nullable<int>  UOMPara { get; set; }
    
    		    
    		/// <summary>
    		/// Multi Entry
    		/// </summary>        
    	//    [DisplayName("Multi Entry")]
    		public Nullable<bool>  MultiEntry { get; set; }
    
    		    
    		/// <summary>
    		/// Calculation
    		/// </summary>        
    	//    [DisplayName("Calculation")]
            [MaxLength(500, ErrorMessage = "Calculation cannot be longer than 500 characters")]
    		public string  Calculation { get; set; }
    
    		    
    		/// <summary>
    		/// Created By
    		/// </summary>        
    	//    [DisplayName("Created By")]
            [MaxLength(50, ErrorMessage = "Created By cannot be longer than 50 characters")]
    		public string  CreatedBy { get; set; }
    
    		    
    		/// <summary>
    		/// Created Date
    		/// </summary>        
    	//    [DisplayName("Created Date")]
    		public Nullable<System.DateTime>  CreatedDate { get; set; }
    
    		    
    		/// <summary>
    		/// Updated By
    		/// </summary>        
    	//    [DisplayName("Updated By")]
            [MaxLength(50, ErrorMessage = "Updated By cannot be longer than 50 characters")]
    		public string  UpdatedBy { get; set; }
    
    		    
    		/// <summary>
    		/// Updated Date
    		/// </summary>        
    	//    [DisplayName("Updated Date")]
    		public Nullable<System.DateTime>  UpdatedDate { get; set; }
    
    		    
    		/// <summary>
    		/// Status
    		/// </summary>        
    	//    [DisplayName("Status")]
            [MaxLength(10, ErrorMessage = "Status cannot be longer than 10 characters")]
    		public string  Status { get; set; }
    
    		    
    		/// <summary>
    		/// Remark
    		/// </summary>        
    	//    [DisplayName("Remark")]
            [MaxLength(500, ErrorMessage = "Remark cannot be longer than 500 characters")]
    		public string  Remark { get; set; }
    
    		    
    	}
    //}
    
}
