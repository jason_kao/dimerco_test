using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ValuePlus.ViewModel;



namespace ValuePlus.ViewModel.ReSM
{
    
    /// <summary>
    /// SMAirPort class
    /// </summary>
    //[MetadataType(typeof(SMAirPortViewModel))]
    //public  partial class SMAirPort
    //{
    
    	/// <summary>
    	/// SMAirPort Metadata class
    	/// </summary>
    	public   class SMAirPortViewModel:ViewModelBase
    	{
    		    
    		/// <summary>
    		/// HQID
    		/// </summary>        
    	//    [DisplayName("HQID")]
            [Required(ErrorMessage = "HQID is required")]
    		public int  HQID { get; set; }
    
    		    
    		/// <summary>
    		/// Air Port Code
    		/// </summary>        
    	//    [DisplayName("Air Port Code")]
            [Required(ErrorMessage = "Air Port Code is required")]
            [MaxLength(5, ErrorMessage = "Air Port Code cannot be longer than 5 characters")]
    		public string  AirPortCode { get; set; }
    
    		    
    		/// <summary>
    		/// Air Port Name
    		/// </summary>        
    	//    [DisplayName("Air Port Name")]
            [MaxLength(255, ErrorMessage = "Air Port Name cannot be longer than 255 characters")]
    		public string  AirPortName { get; set; }
    
    		    
    		/// <summary>
    		/// City ID
    		/// </summary>        
    	//    [DisplayName("City ID")]
    		public Nullable<int>  CityID { get; set; }
    
    		    
    		/// <summary>
    		/// Status
    		/// </summary>        
    	//    [DisplayName("Status")]
            [Required(ErrorMessage = "Status is required")]
            [MaxLength(10, ErrorMessage = "Status cannot be longer than 10 characters")]
    		public string  Status { get; set; }
    
    		    
    		/// <summary>
    		/// Created By
    		/// </summary>        
    	//    [DisplayName("Created By")]
            [MaxLength(6, ErrorMessage = "Created By cannot be longer than 6 characters")]
    		public string  CreatedBy { get; set; }
    
    		    
    		/// <summary>
    		/// Created Date
    		/// </summary>        
    	//    [DisplayName("Created Date")]
            [Required(ErrorMessage = "Created Date is required")]
    		public System.DateTime  CreatedDate { get; set; }
    
    		    
    		/// <summary>
    		/// Updated By
    		/// </summary>        
    	//    [DisplayName("Updated By")]
            [MaxLength(6, ErrorMessage = "Updated By cannot be longer than 6 characters")]
    		public string  UpdatedBy { get; set; }
    
    		    
    		/// <summary>
    		/// Updated Date
    		/// </summary>        
    	//    [DisplayName("Updated Date")]
    		public Nullable<System.DateTime>  UpdatedDate { get; set; }
    
    		    
    		/// <summary>
    		/// Version
    		/// </summary>        
    	//    [DisplayName("Version")]
            [Required(ErrorMessage = "Version is required")]
            [MaxLength(8, ErrorMessage = "Version cannot be longer than 8 characters")]
    		public byte[]  Version { get; set; }
    
    		    
    		/// <summary>
    		/// Is International
    		/// </summary>        
    	//    [DisplayName("Is International")]
    		public Nullable<bool>  IsInternational { get; set; }
    
    		    
    		/// <summary>
    		/// Is Domestic
    		/// </summary>        
    	//    [DisplayName("Is Domestic")]
    		public Nullable<bool>  IsDomestic { get; set; }
    
    		    
    	}
    //}
    
}
