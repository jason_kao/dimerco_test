using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ValuePlus.ViewModel;



namespace ValuePlus.ViewModel.ReSM
{
    
    /// <summary>
    /// SMChartofAccount class
    /// </summary>
    //[MetadataType(typeof(SMChartofAccountViewModel))]
    //public  partial class SMChartofAccount
    //{
    
    	/// <summary>
    	/// SMChartofAccount Metadata class
    	/// </summary>
    	public   class SMChartofAccountViewModel:ViewModelBase
    	{
    		    
    		/// <summary>
    		/// HQID
    		/// </summary>        
    	//    [DisplayName("HQID")]
            [Required(ErrorMessage = "HQID is required")]
    		public int  HQID { get; set; }
    
    		    
    		/// <summary>
    		/// GLCode
    		/// </summary>        
    	//    [DisplayName("GLCode")]
            [Required(ErrorMessage = "GLCode is required")]
            [MaxLength(10, ErrorMessage = "GLCode cannot be longer than 10 characters")]
    		public string  GLCode { get; set; }
    
    		    
    		/// <summary>
    		/// GLName
    		/// </summary>        
    	//    [DisplayName("GLName")]
            [MaxLength(255, ErrorMessage = "GLName cannot be longer than 255 characters")]
    		public string  GLName { get; set; }
    
    		    
    		/// <summary>
    		/// Description
    		/// </summary>        
    	//    [DisplayName("Description")]
            [MaxLength(255, ErrorMessage = "Description cannot be longer than 255 characters")]
    		public string  Description { get; set; }
    
    		    
    		/// <summary>
    		/// Notes
    		/// </summary>        
    	//    [DisplayName("Notes")]
            [MaxLength(255, ErrorMessage = "Notes cannot be longer than 255 characters")]
    		public string  Notes { get; set; }
    
    		    
    		/// <summary>
    		/// Account Type
    		/// </summary>        
    	//    [DisplayName("Account Type")]
            [MaxLength(4, ErrorMessage = "Account Type cannot be longer than 4 characters")]
    		public string  AccountType { get; set; }
    
    		    
    		/// <summary>
    		/// Currency
    		/// </summary>        
    	//    [DisplayName("Currency")]
            [MaxLength(4, ErrorMessage = "Currency cannot be longer than 4 characters")]
    		public string  Currency { get; set; }
    
    		    
    		/// <summary>
    		/// Level
    		/// </summary>        
    	//    [DisplayName("Level")]
            [MaxLength(1, ErrorMessage = "Level cannot be longer than 1 characters")]
    		public string  Level { get; set; }
    
    		    
    		/// <summary>
    		/// Charge Code
    		/// </summary>        
    	//    [DisplayName("Charge Code")]
            [MaxLength(3, ErrorMessage = "Charge Code cannot be longer than 3 characters")]
    		public string  ChargeCode { get; set; }
    
    		    
    		/// <summary>
    		/// Status
    		/// </summary>        
    	//    [DisplayName("Status")]
            [Required(ErrorMessage = "Status is required")]
            [MaxLength(10, ErrorMessage = "Status cannot be longer than 10 characters")]
    		public string  Status { get; set; }
    
    		    
    		/// <summary>
    		/// Created By
    		/// </summary>        
    	//    [DisplayName("Created By")]
            [MaxLength(6, ErrorMessage = "Created By cannot be longer than 6 characters")]
    		public string  CreatedBy { get; set; }
    
    		    
    		/// <summary>
    		/// Created Date
    		/// </summary>        
    	//    [DisplayName("Created Date")]
            [Required(ErrorMessage = "Created Date is required")]
    		public System.DateTime  CreatedDate { get; set; }
    
    		    
    		/// <summary>
    		/// Updated By
    		/// </summary>        
    	//    [DisplayName("Updated By")]
            [MaxLength(6, ErrorMessage = "Updated By cannot be longer than 6 characters")]
    		public string  UpdatedBy { get; set; }
    
    		    
    		/// <summary>
    		/// Updated Date
    		/// </summary>        
    	//    [DisplayName("Updated Date")]
    		public Nullable<System.DateTime>  UpdatedDate { get; set; }
    
    		    
    		/// <summary>
    		/// Version
    		/// </summary>        
    	//    [DisplayName("Version")]
            [MaxLength(8, ErrorMessage = "Version cannot be longer than 8 characters")]
    		public byte[]  Version { get; set; }
    
    		    
    		/// <summary>
    		/// Dr
    		/// </summary>        
    	//    [DisplayName("Dr")]
            [MaxLength(1, ErrorMessage = "Dr cannot be longer than 1 characters")]
    		public string  Dr { get; set; }
    
    		    
    	}
    //}
    
}
