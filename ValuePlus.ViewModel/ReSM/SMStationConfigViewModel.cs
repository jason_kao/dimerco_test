using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ValuePlus.ViewModel;



namespace ValuePlus.ViewModel.ReSM
{
    
    /// <summary>
    /// SMStationConfig class
    /// </summary>
    //[MetadataType(typeof(SMStationConfigViewModel))]
    //public  partial class SMStationConfig
    //{
    
    	/// <summary>
    	/// SMStationConfig Metadata class
    	/// </summary>
    	public   class SMStationConfigViewModel:ViewModelBase
    	{
    		    
    		/// <summary>
    		/// Station ID
    		/// </summary>        
    	//    [DisplayName("Station ID")]
            [Required(ErrorMessage = "Station ID is required")]
            [MaxLength(3, ErrorMessage = "Station ID cannot be longer than 3 characters")]
    		public string  StationID { get; set; }
    
    		    
    		/// <summary>
    		/// Inbox Name
    		/// </summary>        
    	//    [DisplayName("Inbox Name")]
            [MaxLength(50, ErrorMessage = "Inbox Name cannot be longer than 50 characters")]
    		public string  InboxName { get; set; }
    
    		    
    		/// <summary>
    		/// e Mail Acct
    		/// </summary>        
    	//    [DisplayName("e Mail Acct")]
            [Required(ErrorMessage = "e Mail Acct is required")]
            [MaxLength(50, ErrorMessage = "e Mail Acct cannot be longer than 50 characters")]
    		public string  eMailAcct { get; set; }
    
    		    
    		/// <summary>
    		/// Station Code
    		/// </summary>        
    	//    [DisplayName("Station Code")]
            [MaxLength(6, ErrorMessage = "Station Code cannot be longer than 6 characters")]
    		public string  StationCode { get; set; }
    
    		    
    	}
    //}
    
}
