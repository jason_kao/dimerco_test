using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ValuePlus.ViewModel;



namespace ValuePlus.ViewModel.ReSM
{
    
    /// <summary>
    /// SMCustomerPreAlertTemplMap class
    /// </summary>
    //[MetadataType(typeof(SMCustomerPreAlertTemplMapViewModel))]
    //public  partial class SMCustomerPreAlertTemplMap
    //{
    
    	/// <summary>
    	/// SMCustomerPreAlertTemplMap Metadata class
    	/// </summary>
    	public   class SMCustomerPreAlertTemplMapViewModel:ViewModelBase
    	{
    		    
    		/// <summary>
    		/// ID
    		/// </summary>        
    	//    [DisplayName("ID")]
            [Required(ErrorMessage = "ID is required")]
    		public int  ID { get; set; }
    
    		    
    		/// <summary>
    		/// Product Line ID
    		/// </summary>        
    	//    [DisplayName("Product Line ID")]
            [Required(ErrorMessage = "Product Line ID is required")]
    		public int  ProductLineID { get; set; }
    
    		    
    		/// <summary>
    		/// AWBType
    		/// </summary>        
    	//    [DisplayName("AWBType")]
            [MaxLength(20, ErrorMessage = "AWBType cannot be longer than 20 characters")]
    		public string  AWBType { get; set; }
    
    		    
    		/// <summary>
    		/// Form Type
    		/// </summary>        
    	//    [DisplayName("Form Type")]
    		public Nullable<int>  FormType { get; set; }
    
    		    
    		/// <summary>
    		/// Alias Code
    		/// </summary>        
    	//    [DisplayName("Alias Code")]
            [MaxLength(100, ErrorMessage = "Alias Code cannot be longer than 100 characters")]
    		public string  AliasCode { get; set; }
    
    		    
    		/// <summary>
    		/// Map Name
    		/// </summary>        
    	//    [DisplayName("Map Name")]
            [MaxLength(255, ErrorMessage = "Map Name cannot be longer than 255 characters")]
    		public string  MapName { get; set; }
    
    		    
    		/// <summary>
    		/// Show Header
    		/// </summary>        
    	//    [DisplayName("Show Header")]
            [MaxLength(1, ErrorMessage = "Show Header cannot be longer than 1 characters")]
    		public string  ShowHeader { get; set; }
    
    		    
    		/// <summary>
    		/// Show Content
    		/// </summary>        
    	//    [DisplayName("Show Content")]
            [MaxLength(1, ErrorMessage = "Show Content cannot be longer than 1 characters")]
    		public string  ShowContent { get; set; }
    
    		    
    		/// <summary>
    		/// Status
    		/// </summary>        
    	//    [DisplayName("Status")]
            [Required(ErrorMessage = "Status is required")]
            [MaxLength(50, ErrorMessage = "Status cannot be longer than 50 characters")]
    		public string  Status { get; set; }
    
    		    
    		/// <summary>
    		/// Created By
    		/// </summary>        
    	//    [DisplayName("Created By")]
            [MaxLength(6, ErrorMessage = "Created By cannot be longer than 6 characters")]
    		public string  CreatedBy { get; set; }
    
    		    
    		/// <summary>
    		/// Created Date
    		/// </summary>        
    	//    [DisplayName("Created Date")]
            [Required(ErrorMessage = "Created Date is required")]
    		public System.DateTime  CreatedDate { get; set; }
    
    		    
    		/// <summary>
    		/// Updated By
    		/// </summary>        
    	//    [DisplayName("Updated By")]
            [MaxLength(6, ErrorMessage = "Updated By cannot be longer than 6 characters")]
    		public string  UpdatedBy { get; set; }
    
    		    
    		/// <summary>
    		/// Updated Date
    		/// </summary>        
    	//    [DisplayName("Updated Date")]
    		public Nullable<System.DateTime>  UpdatedDate { get; set; }
    
    		    
    	}
    //}
    
}
