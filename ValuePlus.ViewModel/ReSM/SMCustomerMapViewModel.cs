using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ValuePlus.ViewModel;



namespace ValuePlus.ViewModel.ReSM
{
    
    /// <summary>
    /// SMCustomerMap class
    /// </summary>
    //[MetadataType(typeof(SMCustomerMapViewModel))]
    //public  partial class SMCustomerMap
    //{
    
    	/// <summary>
    	/// SMCustomerMap Metadata class
    	/// </summary>
    	public   class SMCustomerMapViewModel:ViewModelBase
    	{
    		    
    		/// <summary>
    		/// HQID
    		/// </summary>        
    	//    [DisplayName("HQID")]
            [Required(ErrorMessage = "HQID is required")]
    		public int  HQID { get; set; }
    
    		    
    		/// <summary>
    		/// Map Type
    		/// </summary>        
    	//    [DisplayName("Map Type")]
            [Required(ErrorMessage = "Map Type is required")]
            [MaxLength(50, ErrorMessage = "Map Type cannot be longer than 50 characters")]
    		public string  MapType { get; set; }
    
    		    
    		/// <summary>
    		/// Customer ID
    		/// </summary>        
    	//    [DisplayName("Customer ID")]
            [Required(ErrorMessage = "Customer ID is required")]
    		public int  CustomerID { get; set; }
    
    		    
    		/// <summary>
    		/// Station ID
    		/// </summary>        
    	//    [DisplayName("Station ID")]
            [Required(ErrorMessage = "Station ID is required")]
            [MaxLength(10, ErrorMessage = "Station ID cannot be longer than 10 characters")]
    		public string  StationID { get; set; }
    
    		    
    		/// <summary>
    		/// Created By
    		/// </summary>        
    	//    [DisplayName("Created By")]
            [MaxLength(10, ErrorMessage = "Created By cannot be longer than 10 characters")]
    		public string  CreatedBy { get; set; }
    
    		    
    		/// <summary>
    		/// Created Date
    		/// </summary>        
    	//    [DisplayName("Created Date")]
    		public Nullable<System.DateTime>  CreatedDate { get; set; }
    
    		    
    		/// <summary>
    		/// Updated By
    		/// </summary>        
    	//    [DisplayName("Updated By")]
            [MaxLength(10, ErrorMessage = "Updated By cannot be longer than 10 characters")]
    		public string  UpdatedBy { get; set; }
    
    		    
    		/// <summary>
    		/// Updated Date
    		/// </summary>        
    	//    [DisplayName("Updated Date")]
    		public Nullable<System.DateTime>  UpdatedDate { get; set; }
    
    		    
    		/// <summary>
    		/// Status
    		/// </summary>        
    	//    [DisplayName("Status")]
            [MaxLength(10, ErrorMessage = "Status cannot be longer than 10 characters")]
    		public string  Status { get; set; }
    
    		    
    		/// <summary>
    		/// Login ID
    		/// </summary>        
    	//    [DisplayName("Login ID")]
            [MaxLength(20, ErrorMessage = "Login ID cannot be longer than 20 characters")]
    		public string  LoginID { get; set; }
    
    		    
    		/// <summary>
    		/// PWD
    		/// </summary>        
    	//    [DisplayName("PWD")]
            [MaxLength(50, ErrorMessage = "PWD cannot be longer than 50 characters")]
    		public string  PWD { get; set; }
    
    		    
    		/// <summary>
    		/// e Mail1
    		/// </summary>        
    	//    [DisplayName("e Mail1")]
            [MaxLength(50, ErrorMessage = "e Mail1 cannot be longer than 50 characters")]
    		public string  eMail1 { get; set; }
    
    		    
    		/// <summary>
    		/// e Mail2
    		/// </summary>        
    	//    [DisplayName("e Mail2")]
            [MaxLength(50, ErrorMessage = "e Mail2 cannot be longer than 50 characters")]
    		public string  eMail2 { get; set; }
    
    		    
    	}
    //}
    
}
