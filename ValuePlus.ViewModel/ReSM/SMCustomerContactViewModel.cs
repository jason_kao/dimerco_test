using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ValuePlus.ViewModel;



namespace ValuePlus.ViewModel.ReSM
{
    
    /// <summary>
    /// SMCustomerContact class
    /// </summary>
    //[MetadataType(typeof(SMCustomerContactViewModel))]
    //public  partial class SMCustomerContact
    //{
    
    	/// <summary>
    	/// SMCustomerContact Metadata class
    	/// </summary>
    	public   class SMCustomerContactViewModel:ViewModelBase
    	{
    		    
    		/// <summary>
    		/// HQID
    		/// </summary>        
    	//    [DisplayName("HQID")]
            [Required(ErrorMessage = "HQID is required")]
    		public int  HQID { get; set; }
    
    		    
    		/// <summary>
    		/// Customer ID
    		/// </summary>        
    	//    [DisplayName("Customer ID")]
            [Required(ErrorMessage = "Customer ID is required")]
    		public int  CustomerID { get; set; }
    
    		    
    		/// <summary>
    		/// First Name
    		/// </summary>        
    	//    [DisplayName("First Name")]
            [MaxLength(50, ErrorMessage = "First Name cannot be longer than 50 characters")]
    		public string  FirstName { get; set; }
    
    		    
    		/// <summary>
    		/// Last Name
    		/// </summary>        
    	//    [DisplayName("Last Name")]
            [MaxLength(50, ErrorMessage = "Last Name cannot be longer than 50 characters")]
    		public string  LastName { get; set; }
    
    		    
    		/// <summary>
    		/// Full Name
    		/// </summary>        
    	//    [DisplayName("Full Name")]
            [MaxLength(50, ErrorMessage = "Full Name cannot be longer than 50 characters")]
    		public string  FullName { get; set; }
    
    		    
    		/// <summary>
    		/// Tel_ H
    		/// </summary>        
    	//    [DisplayName("Tel_ H")]
            [MaxLength(50, ErrorMessage = "Tel_ H cannot be longer than 50 characters")]
    		public string  Tel_H { get; set; }
    
    		    
    		/// <summary>
    		/// Tel_ M
    		/// </summary>        
    	//    [DisplayName("Tel_ M")]
            [MaxLength(50, ErrorMessage = "Tel_ M cannot be longer than 50 characters")]
    		public string  Tel_M { get; set; }
    
    		    
    		/// <summary>
    		/// Tel_ O
    		/// </summary>        
    	//    [DisplayName("Tel_ O")]
            [MaxLength(50, ErrorMessage = "Tel_ O cannot be longer than 50 characters")]
    		public string  Tel_O { get; set; }
    
    		    
    		/// <summary>
    		/// Tel_ OExt
    		/// </summary>        
    	//    [DisplayName("Tel_ OExt")]
            [MaxLength(10, ErrorMessage = "Tel_ OExt cannot be longer than 10 characters")]
    		public string  Tel_OExt { get; set; }
    
    		    
    		/// <summary>
    		/// Fax
    		/// </summary>        
    	//    [DisplayName("Fax")]
            [MaxLength(50, ErrorMessage = "Fax cannot be longer than 50 characters")]
    		public string  Fax { get; set; }
    
    		    
    		/// <summary>
    		/// Birthday
    		/// </summary>        
    	//    [DisplayName("Birthday")]
            [MaxLength(50, ErrorMessage = "Birthday cannot be longer than 50 characters")]
    		public string  Birthday { get; set; }
    
    		    
    		/// <summary>
    		/// Email
    		/// </summary>        
    	//    [DisplayName("Email")]
            [MaxLength(100, ErrorMessage = "Email cannot be longer than 100 characters")]
    		public string  Email { get; set; }
    
    		    
    		/// <summary>
    		/// Title
    		/// </summary>        
    	//    [DisplayName("Title")]
            [MaxLength(50, ErrorMessage = "Title cannot be longer than 50 characters")]
    		public string  Title { get; set; }
    
    		    
    		/// <summary>
    		/// Status
    		/// </summary>        
    	//    [DisplayName("Status")]
            [Required(ErrorMessage = "Status is required")]
            [MaxLength(10, ErrorMessage = "Status cannot be longer than 10 characters")]
    		public string  Status { get; set; }
    
    		    
    		/// <summary>
    		/// Created By
    		/// </summary>        
    	//    [DisplayName("Created By")]
            [MaxLength(6, ErrorMessage = "Created By cannot be longer than 6 characters")]
    		public string  CreatedBy { get; set; }
    
    		    
    		/// <summary>
    		/// Created Date
    		/// </summary>        
    	//    [DisplayName("Created Date")]
            [Required(ErrorMessage = "Created Date is required")]
    		public System.DateTime  CreatedDate { get; set; }
    
    		    
    		/// <summary>
    		/// Updated By
    		/// </summary>        
    	//    [DisplayName("Updated By")]
            [MaxLength(6, ErrorMessage = "Updated By cannot be longer than 6 characters")]
    		public string  UpdatedBy { get; set; }
    
    		    
    		/// <summary>
    		/// Updated Date
    		/// </summary>        
    	//    [DisplayName("Updated Date")]
    		public Nullable<System.DateTime>  UpdatedDate { get; set; }
    
    		    
    		/// <summary>
    		/// Version
    		/// </summary>        
    	//    [DisplayName("Version")]
            [Required(ErrorMessage = "Version is required")]
            [MaxLength(8, ErrorMessage = "Version cannot be longer than 8 characters")]
    		public byte[]  Version { get; set; }
    
    		    
    		/// <summary>
    		/// Role
    		/// </summary>        
    	//    [DisplayName("Role")]
            [MaxLength(50, ErrorMessage = "Role cannot be longer than 50 characters")]
    		public string  Role { get; set; }
    
    		    
    		/// <summary>
    		/// Department
    		/// </summary>        
    	//    [DisplayName("Department")]
            [MaxLength(50, ErrorMessage = "Department cannot be longer than 50 characters")]
    		public string  Department { get; set; }
    
    		    
    		/// <summary>
    		/// Location
    		/// </summary>        
    	//    [DisplayName("Location")]
            [MaxLength(50, ErrorMessage = "Location cannot be longer than 50 characters")]
    		public string  Location { get; set; }
    
    		    
    		/// <summary>
    		/// Boss
    		/// </summary>        
    	//    [DisplayName("Boss")]
            [MaxLength(50, ErrorMessage = "Boss cannot be longer than 50 characters")]
    		public string  Boss { get; set; }
    
    		    
    		/// <summary>
    		/// Assistant
    		/// </summary>        
    	//    [DisplayName("Assistant")]
            [MaxLength(50, ErrorMessage = "Assistant cannot be longer than 50 characters")]
    		public string  Assistant { get; set; }
    
    		    
    		/// <summary>
    		/// Language
    		/// </summary>        
    	//    [DisplayName("Language")]
            [MaxLength(50, ErrorMessage = "Language cannot be longer than 50 characters")]
    		public string  Language { get; set; }
    
    		    
    		/// <summary>
    		/// Joined Company
    		/// </summary>        
    	//    [DisplayName("Joined Company")]
            [MaxLength(50, ErrorMessage = "Joined Company cannot be longer than 50 characters")]
    		public string  JoinedCompany { get; set; }
    
    		    
    		/// <summary>
    		/// Joined Industry
    		/// </summary>        
    	//    [DisplayName("Joined Industry")]
            [MaxLength(50, ErrorMessage = "Joined Industry cannot be longer than 50 characters")]
    		public string  JoinedIndustry { get; set; }
    
    		    
    		/// <summary>
    		/// Hobby
    		/// </summary>        
    	//    [DisplayName("Hobby")]
            [MaxLength(50, ErrorMessage = "Hobby cannot be longer than 50 characters")]
    		public string  Hobby { get; set; }
    
    		    
    		/// <summary>
    		/// Address
    		/// </summary>        
    	//    [DisplayName("Address")]
            [MaxLength(50, ErrorMessage = "Address cannot be longer than 50 characters")]
    		public string  Address { get; set; }
    
    		    
    		/// <summary>
    		/// City
    		/// </summary>        
    	//    [DisplayName("City")]
            [MaxLength(50, ErrorMessage = "City cannot be longer than 50 characters")]
    		public string  City { get; set; }
    
    		    
    		/// <summary>
    		/// State
    		/// </summary>        
    	//    [DisplayName("State")]
            [MaxLength(50, ErrorMessage = "State cannot be longer than 50 characters")]
    		public string  State { get; set; }
    
    		    
    		/// <summary>
    		/// Country
    		/// </summary>        
    	//    [DisplayName("Country")]
            [MaxLength(50, ErrorMessage = "Country cannot be longer than 50 characters")]
    		public string  Country { get; set; }
    
    		    
    		/// <summary>
    		/// ZIP
    		/// </summary>        
    	//    [DisplayName("ZIP")]
            [MaxLength(50, ErrorMessage = "ZIP cannot be longer than 50 characters")]
    		public string  ZIP { get; set; }
    
    		    
    		/// <summary>
    		/// Personal Information
    		/// </summary>        
    	//    [DisplayName("Personal Information")]
            [MaxLength(50, ErrorMessage = "Personal Information cannot be longer than 50 characters")]
    		public string  PersonalInformation { get; set; }
    
    		    
    		/// <summary>
    		/// tr Content
    		/// </summary>        
    	//    [DisplayName("tr Content")]
            [MaxLength(50, ErrorMessage = "tr Content cannot be longer than 50 characters")]
    		public string  trContent { get; set; }
    
    		    
    		/// <summary>
    		/// Family Members
    		/// </summary>        
    	//    [DisplayName("Family Members")]
            [MaxLength(50, ErrorMessage = "Family Members cannot be longer than 50 characters")]
    		public string  FamilyMembers { get; set; }
    
    		    
    		/// <summary>
    		/// Names
    		/// </summary>        
    	//    [DisplayName("Names")]
            [MaxLength(50, ErrorMessage = "Names cannot be longer than 50 characters")]
    		public string  Names { get; set; }
    
    		    
    		/// <summary>
    		/// Titles
    		/// </summary>        
    	//    [DisplayName("Titles")]
            [MaxLength(50, ErrorMessage = "Titles cannot be longer than 50 characters")]
    		public string  Titles { get; set; }
    
    		    
    		/// <summary>
    		/// Last Contact Date
    		/// </summary>        
    	//    [DisplayName("Last Contact Date")]
    		public Nullable<System.DateTime>  LastContactDate { get; set; }
    
    		    
    		/// <summary>
    		/// Last Contact
    		/// </summary>        
    	//    [DisplayName("Last Contact")]
            [MaxLength(50, ErrorMessage = "Last Contact cannot be longer than 50 characters")]
    		public string  LastContact { get; set; }
    
    		    
    		/// <summary>
    		/// VIP
    		/// </summary>        
    	//    [DisplayName("VIP")]
            [MaxLength(50, ErrorMessage = "VIP cannot be longer than 50 characters")]
    		public string  VIP { get; set; }
    
    		    
    		/// <summary>
    		/// Local Name
    		/// </summary>        
    	//    [DisplayName("Local Name")]
            [MaxLength(50, ErrorMessage = "Local Name cannot be longer than 50 characters")]
    		public string  LocalName { get; set; }
    
    		    
    		/// <summary>
    		/// Fax2
    		/// </summary>        
    	//    [DisplayName("Fax2")]
            [MaxLength(50, ErrorMessage = "Fax2 cannot be longer than 50 characters")]
    		public string  Fax2 { get; set; }
    
    		    
    		/// <summary>
    		/// Image ID
    		/// </summary>        
    	//    [DisplayName("Image ID")]
    		public Nullable<int>  ImageID { get; set; }
    
    		    
    	}
    //}
    
}
