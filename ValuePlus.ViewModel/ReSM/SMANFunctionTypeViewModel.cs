using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ValuePlus.ViewModel;



namespace ValuePlus.ViewModel.ReSM
{
    
    /// <summary>
    /// SMANFunctionType class
    /// </summary>
    //[MetadataType(typeof(SMANFunctionTypeViewModel))]
    //public  partial class SMANFunctionType
    //{
    
    	/// <summary>
    	/// SMANFunctionType Metadata class
    	/// </summary>
    	public   class SMANFunctionTypeViewModel:ViewModelBase
    	{
    		    
    		/// <summary>
    		/// HQID
    		/// </summary>        
    	//    [DisplayName("HQID")]
            [Required(ErrorMessage = "HQID is required")]
    		public int  HQID { get; set; }
    
    		    
    		/// <summary>
    		/// ANFunction Code
    		/// </summary>        
    	//    [DisplayName("ANFunction Code")]
            [MaxLength(4, ErrorMessage = "ANFunction Code cannot be longer than 4 characters")]
    		public string  ANFunctionCode { get; set; }
    
    		    
    		/// <summary>
    		/// ANFunction Name
    		/// </summary>        
    	//    [DisplayName("ANFunction Name")]
            [MaxLength(30, ErrorMessage = "ANFunction Name cannot be longer than 30 characters")]
    		public string  ANFunctionName { get; set; }
    
    		    
    		/// <summary>
    		/// ANDIMCode1
    		/// </summary>        
    	//    [DisplayName("ANDIMCode1")]
            [Required(ErrorMessage = "ANDIMCode1 is required")]
            [MaxLength(3, ErrorMessage = "ANDIMCode1 cannot be longer than 3 characters")]
    		public string  ANDIMCode1 { get; set; }
    
    		    
    		/// <summary>
    		/// ANDIMCode2
    		/// </summary>        
    	//    [DisplayName("ANDIMCode2")]
            [Required(ErrorMessage = "ANDIMCode2 is required")]
            [MaxLength(3, ErrorMessage = "ANDIMCode2 cannot be longer than 3 characters")]
    		public string  ANDIMCode2 { get; set; }
    
    		    
    		/// <summary>
    		/// ANDIMCode3
    		/// </summary>        
    	//    [DisplayName("ANDIMCode3")]
            [MaxLength(3, ErrorMessage = "ANDIMCode3 cannot be longer than 3 characters")]
    		public string  ANDIMCode3 { get; set; }
    
    		    
    		/// <summary>
    		/// Created By
    		/// </summary>        
    	//    [DisplayName("Created By")]
            [Required(ErrorMessage = "Created By is required")]
            [MaxLength(6, ErrorMessage = "Created By cannot be longer than 6 characters")]
    		public string  CreatedBy { get; set; }
    
    		    
    		/// <summary>
    		/// Created Date
    		/// </summary>        
    	//    [DisplayName("Created Date")]
            [Required(ErrorMessage = "Created Date is required")]
    		public System.DateTime  CreatedDate { get; set; }
    
    		    
    		/// <summary>
    		/// Updated By
    		/// </summary>        
    	//    [DisplayName("Updated By")]
            [MaxLength(6, ErrorMessage = "Updated By cannot be longer than 6 characters")]
    		public string  UpdatedBy { get; set; }
    
    		    
    		/// <summary>
    		/// Updated Date
    		/// </summary>        
    	//    [DisplayName("Updated Date")]
    		public Nullable<System.DateTime>  UpdatedDate { get; set; }
    
    		    
    		/// <summary>
    		/// Repeter
    		/// </summary>        
    	//    [DisplayName("Repeter")]
            [MaxLength(1, ErrorMessage = "Repeter cannot be longer than 1 characters")]
    		public string  Repeter { get; set; }
    
    		    
    		/// <summary>
    		/// Pattern
    		/// </summary>        
    	//    [DisplayName("Pattern")]
            [MaxLength(30, ErrorMessage = "Pattern cannot be longer than 30 characters")]
    		public string  Pattern { get; set; }
    
    		    
    		/// <summary>
    		/// Status
    		/// </summary>        
    	//    [DisplayName("Status")]
            [MaxLength(10, ErrorMessage = "Status cannot be longer than 10 characters")]
    		public string  Status { get; set; }
    
    		    
    	}
    //}
    
}
