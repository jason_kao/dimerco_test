using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ValuePlus.ViewModel;



namespace ValuePlus.ViewModel.ReSM
{
    
    /// <summary>
    /// SMAirLine class
    /// </summary>
    //[MetadataType(typeof(SMAirLineViewModel))]
    //public  partial class SMAirLine
    //{
    
    	/// <summary>
    	/// SMAirLine Metadata class
    	/// </summary>
    	public   class SMAirLineViewModel:ViewModelBase
    	{
    		    
    		/// <summary>
    		/// HQID
    		/// </summary>        
    	//    [DisplayName("HQID")]
            [Required(ErrorMessage = "HQID is required")]
    		public int  HQID { get; set; }
    
    		    
    		/// <summary>
    		/// Station ID
    		/// </summary>        
    	//    [DisplayName("Station ID")]
            [Required(ErrorMessage = "Station ID is required")]
            [MaxLength(3, ErrorMessage = "Station ID cannot be longer than 3 characters")]
    		public string  StationID { get; set; }
    
    		    
    		/// <summary>
    		/// Air Line Code
    		/// </summary>        
    	//    [DisplayName("Air Line Code")]
            [Required(ErrorMessage = "Air Line Code is required")]
            [MaxLength(10, ErrorMessage = "Air Line Code cannot be longer than 10 characters")]
    		public string  AirLineCode { get; set; }
    
    		    
    		/// <summary>
    		/// Customer ID
    		/// </summary>        
    	//    [DisplayName("Customer ID")]
    		public Nullable<int>  CustomerID { get; set; }
    
    		    
    		/// <summary>
    		/// City ID
    		/// </summary>        
    	//    [DisplayName("City ID")]
    		public Nullable<int>  CityID { get; set; }
    
    		    
    		/// <summary>
    		/// Air Line Name
    		/// </summary>        
    	//    [DisplayName("Air Line Name")]
            [Required(ErrorMessage = "Air Line Name is required")]
            [MaxLength(255, ErrorMessage = "Air Line Name cannot be longer than 255 characters")]
    		public string  AirLineName { get; set; }
    
    		    
    		/// <summary>
    		/// Air Line Name1
    		/// </summary>        
    	//    [DisplayName("Air Line Name1")]
            [MaxLength(255, ErrorMessage = "Air Line Name1 cannot be longer than 255 characters")]
    		public string  AirLineName1 { get; set; }
    
    		    
    		/// <summary>
    		/// Air Line Address1
    		/// </summary>        
    	//    [DisplayName("Air Line Address1")]
            [MaxLength(255, ErrorMessage = "Air Line Address1 cannot be longer than 255 characters")]
    		public string  AirLineAddress1 { get; set; }
    
    		    
    		/// <summary>
    		/// Air Line Address2
    		/// </summary>        
    	//    [DisplayName("Air Line Address2")]
            [MaxLength(255, ErrorMessage = "Air Line Address2 cannot be longer than 255 characters")]
    		public string  AirLineAddress2 { get; set; }
    
    		    
    		/// <summary>
    		/// Air Line Address3
    		/// </summary>        
    	//    [DisplayName("Air Line Address3")]
            [MaxLength(255, ErrorMessage = "Air Line Address3 cannot be longer than 255 characters")]
    		public string  AirLineAddress3 { get; set; }
    
    		    
    		/// <summary>
    		/// Air Line Address4
    		/// </summary>        
    	//    [DisplayName("Air Line Address4")]
            [MaxLength(255, ErrorMessage = "Air Line Address4 cannot be longer than 255 characters")]
    		public string  AirLineAddress4 { get; set; }
    
    		    
    		/// <summary>
    		/// Air Line Address5
    		/// </summary>        
    	//    [DisplayName("Air Line Address5")]
            [MaxLength(255, ErrorMessage = "Air Line Address5 cannot be longer than 255 characters")]
    		public string  AirLineAddress5 { get; set; }
    
    		    
    		/// <summary>
    		/// Air Line Phone
    		/// </summary>        
    	//    [DisplayName("Air Line Phone")]
            [MaxLength(50, ErrorMessage = "Air Line Phone cannot be longer than 50 characters")]
    		public string  AirLinePhone { get; set; }
    
    		    
    		/// <summary>
    		/// Air Line Phone Ext
    		/// </summary>        
    	//    [DisplayName("Air Line Phone Ext")]
            [MaxLength(10, ErrorMessage = "Air Line Phone Ext cannot be longer than 10 characters")]
    		public string  AirLinePhoneExt { get; set; }
    
    		    
    		/// <summary>
    		/// Air Line Fax
    		/// </summary>        
    	//    [DisplayName("Air Line Fax")]
            [MaxLength(50, ErrorMessage = "Air Line Fax cannot be longer than 50 characters")]
    		public string  AirLineFax { get; set; }
    
    		    
    		/// <summary>
    		/// Air Line Fax Ext
    		/// </summary>        
    	//    [DisplayName("Air Line Fax Ext")]
            [MaxLength(10, ErrorMessage = "Air Line Fax Ext cannot be longer than 10 characters")]
    		public string  AirLineFaxExt { get; set; }
    
    		    
    		/// <summary>
    		/// Air Line Zip
    		/// </summary>        
    	//    [DisplayName("Air Line Zip")]
            [MaxLength(10, ErrorMessage = "Air Line Zip cannot be longer than 10 characters")]
    		public string  AirLineZip { get; set; }
    
    		    
    		/// <summary>
    		/// Air Line Web Site
    		/// </summary>        
    	//    [DisplayName("Air Line Web Site")]
            [MaxLength(255, ErrorMessage = "Air Line Web Site cannot be longer than 255 characters")]
    		public string  AirLineWebSite { get; set; }
    
    		    
    		/// <summary>
    		/// Status
    		/// </summary>        
    	//    [DisplayName("Status")]
            [Required(ErrorMessage = "Status is required")]
            [MaxLength(10, ErrorMessage = "Status cannot be longer than 10 characters")]
    		public string  Status { get; set; }
    
    		    
    		/// <summary>
    		/// Created By
    		/// </summary>        
    	//    [DisplayName("Created By")]
            [MaxLength(6, ErrorMessage = "Created By cannot be longer than 6 characters")]
    		public string  CreatedBy { get; set; }
    
    		    
    		/// <summary>
    		/// Created Date
    		/// </summary>        
    	//    [DisplayName("Created Date")]
            [Required(ErrorMessage = "Created Date is required")]
    		public System.DateTime  CreatedDate { get; set; }
    
    		    
    		/// <summary>
    		/// Updated By
    		/// </summary>        
    	//    [DisplayName("Updated By")]
            [MaxLength(6, ErrorMessage = "Updated By cannot be longer than 6 characters")]
    		public string  UpdatedBy { get; set; }
    
    		    
    		/// <summary>
    		/// Updated Date
    		/// </summary>        
    	//    [DisplayName("Updated Date")]
    		public Nullable<System.DateTime>  UpdatedDate { get; set; }
    
    		    
    		/// <summary>
    		/// Version
    		/// </summary>        
    	//    [DisplayName("Version")]
            [Required(ErrorMessage = "Version is required")]
            [MaxLength(8, ErrorMessage = "Version cannot be longer than 8 characters")]
    		public byte[]  Version { get; set; }
    
    		    
    		/// <summary>
    		/// Is International
    		/// </summary>        
    	//    [DisplayName("Is International")]
    		public Nullable<bool>  IsInternational { get; set; }
    
    		    
    		/// <summary>
    		/// Is Domestic
    		/// </summary>        
    	//    [DisplayName("Is Domestic")]
    		public Nullable<bool>  IsDomestic { get; set; }
    
    		    
    	}
    //}
    
}
