using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ValuePlus.ViewModel;



namespace ValuePlus.ViewModel.ReSM
{
    
    /// <summary>
    /// SMCodeSetting class
    /// </summary>
    //[MetadataType(typeof(SMCodeSettingViewModel))]
    //public  partial class SMCodeSetting
    //{
    
    	/// <summary>
    	/// SMCodeSetting Metadata class
    	/// </summary>
    	public   class SMCodeSettingViewModel:ViewModelBase
    	{
    		    
    		/// <summary>
    		/// ID
    		/// </summary>        
    	//    [DisplayName("ID")]
            [Required(ErrorMessage = "ID is required")]
    		public int  ID { get; set; }
    
    		    
    		/// <summary>
    		/// Code Group
    		/// </summary>        
    	//    [DisplayName("Code Group")]
            [Required(ErrorMessage = "Code Group is required")]
            [MaxLength(50, ErrorMessage = "Code Group cannot be longer than 50 characters")]
    		public string  CodeGroup { get; set; }
    
    		    
    		/// <summary>
    		/// Code
    		/// </summary>        
    	//    [DisplayName("Code")]
            [Required(ErrorMessage = "Code is required")]
            [MaxLength(50, ErrorMessage = "Code cannot be longer than 50 characters")]
    		public string  Code { get; set; }
    
    		    
    		/// <summary>
    		/// Name
    		/// </summary>        
    	//    [DisplayName("Name")]
            [Required(ErrorMessage = "Name is required")]
            [MaxLength(255, ErrorMessage = "Name cannot be longer than 255 characters")]
    		public string  Name { get; set; }
    
    		    
    		/// <summary>
    		/// Seq
    		/// </summary>        
    	//    [DisplayName("Seq")]
            [Required(ErrorMessage = "Seq is required")]
    		public int  Seq { get; set; }
    
    		    
    		/// <summary>
    		/// Status
    		/// </summary>        
    	//    [DisplayName("Status")]
            [Required(ErrorMessage = "Status is required")]
            [MaxLength(1, ErrorMessage = "Status cannot be longer than 1 characters")]
    		public string  Status { get; set; }
    
    		    
    	}
    //}
    
}
