using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ValuePlus.ViewModel;



namespace ValuePlus.ViewModel.ReSM
{
    
    /// <summary>
    /// SMTimeZone class
    /// </summary>
    //[MetadataType(typeof(SMTimeZoneViewModel))]
    //public  partial class SMTimeZone
    //{
    
    	/// <summary>
    	/// SMTimeZone Metadata class
    	/// </summary>
    	public   class SMTimeZoneViewModel:ViewModelBase
    	{
    		    
    		/// <summary>
    		/// HQID
    		/// </summary>        
    	//    [DisplayName("HQID")]
            [Required(ErrorMessage = "HQID is required")]
    		public int  HQID { get; set; }
    
    		    
    		/// <summary>
    		/// Time Zone Name
    		/// </summary>        
    	//    [DisplayName("Time Zone Name")]
            [Required(ErrorMessage = "Time Zone Name is required")]
            [MaxLength(50, ErrorMessage = "Time Zone Name cannot be longer than 50 characters")]
    		public string  TimeZoneName { get; set; }
    
    		    
    		/// <summary>
    		/// Display Name
    		/// </summary>        
    	//    [DisplayName("Display Name")]
            [Required(ErrorMessage = "Display Name is required")]
            [MaxLength(100, ErrorMessage = "Display Name cannot be longer than 100 characters")]
    		public string  DisplayName { get; set; }
    
    		    
    		/// <summary>
    		/// Standard Name
    		/// </summary>        
    	//    [DisplayName("Standard Name")]
            [Required(ErrorMessage = "Standard Name is required")]
            [MaxLength(50, ErrorMessage = "Standard Name cannot be longer than 50 characters")]
    		public string  StandardName { get; set; }
    
    		    
    		/// <summary>
    		/// Daylight Name
    		/// </summary>        
    	//    [DisplayName("Daylight Name")]
            [Required(ErrorMessage = "Daylight Name is required")]
            [MaxLength(50, ErrorMessage = "Daylight Name cannot be longer than 50 characters")]
    		public string  DaylightName { get; set; }
    
    		    
    		/// <summary>
    		/// UTCOff Set
    		/// </summary>        
    	//    [DisplayName("UTCOff Set")]
            [Required(ErrorMessage = "UTCOff Set is required")]
    		public int  UTCOffSet { get; set; }
    
    		    
    	}
    //}
    
}
