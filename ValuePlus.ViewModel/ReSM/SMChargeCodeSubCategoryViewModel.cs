using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ValuePlus.ViewModel;



namespace ValuePlus.ViewModel.ReSM
{
    
    /// <summary>
    /// SMChargeCodeSubCategory class
    /// </summary>
    //[MetadataType(typeof(SMChargeCodeSubCategoryViewModel))]
    //public  partial class SMChargeCodeSubCategory
    //{
    
    	/// <summary>
    	/// SMChargeCodeSubCategory Metadata class
    	/// </summary>
    	public   class SMChargeCodeSubCategoryViewModel:ViewModelBase
    	{
    		    
    		/// <summary>
    		/// HQID
    		/// </summary>        
    	//    [DisplayName("HQID")]
            [Required(ErrorMessage = "HQID is required")]
    		public int  HQID { get; set; }
    
    		    
    		/// <summary>
    		/// Sub Category Code
    		/// </summary>        
    	//    [DisplayName("Sub Category Code")]
            [Required(ErrorMessage = "Sub Category Code is required")]
            [MaxLength(10, ErrorMessage = "Sub Category Code cannot be longer than 10 characters")]
    		public string  SubCategoryCode { get; set; }
    
    		    
    		/// <summary>
    		/// Category ID
    		/// </summary>        
    	//    [DisplayName("Category ID")]
            [Required(ErrorMessage = "Category ID is required")]
    		public int  CategoryID { get; set; }
    
    		    
    		/// <summary>
    		/// Sub Category Name
    		/// </summary>        
    	//    [DisplayName("Sub Category Name")]
            [MaxLength(255, ErrorMessage = "Sub Category Name cannot be longer than 255 characters")]
    		public string  SubCategoryName { get; set; }
    
    		    
    		/// <summary>
    		/// Status
    		/// </summary>        
    	//    [DisplayName("Status")]
            [Required(ErrorMessage = "Status is required")]
            [MaxLength(10, ErrorMessage = "Status cannot be longer than 10 characters")]
    		public string  Status { get; set; }
    
    		    
    		/// <summary>
    		/// Created By
    		/// </summary>        
    	//    [DisplayName("Created By")]
            [MaxLength(6, ErrorMessage = "Created By cannot be longer than 6 characters")]
    		public string  CreatedBy { get; set; }
    
    		    
    		/// <summary>
    		/// Created Date
    		/// </summary>        
    	//    [DisplayName("Created Date")]
            [Required(ErrorMessage = "Created Date is required")]
    		public System.DateTime  CreatedDate { get; set; }
    
    		    
    		/// <summary>
    		/// Updated By
    		/// </summary>        
    	//    [DisplayName("Updated By")]
            [MaxLength(6, ErrorMessage = "Updated By cannot be longer than 6 characters")]
    		public string  UpdatedBy { get; set; }
    
    		    
    		/// <summary>
    		/// Updated Date
    		/// </summary>        
    	//    [DisplayName("Updated Date")]
    		public Nullable<System.DateTime>  UpdatedDate { get; set; }
    
    		    
    		/// <summary>
    		/// Version
    		/// </summary>        
    	//    [DisplayName("Version")]
            [Required(ErrorMessage = "Version is required")]
            [MaxLength(8, ErrorMessage = "Version cannot be longer than 8 characters")]
    		public byte[]  Version { get; set; }
    
    		    
    	}
    //}
    
}
