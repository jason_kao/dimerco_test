using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ValuePlus.ViewModel;



namespace ValuePlus.ViewModel.ReSM
{
    
    /// <summary>
    /// SMMilestone class
    /// </summary>
    //[MetadataType(typeof(SMMilestoneViewModel))]
    //public  partial class SMMilestone
    //{
    
    	/// <summary>
    	/// SMMilestone Metadata class
    	/// </summary>
    	public   class SMMilestoneViewModel:ViewModelBase
    	{
    		    
    		/// <summary>
    		/// HQID
    		/// </summary>        
    	//    [DisplayName("HQID")]
            [Required(ErrorMessage = "HQID is required")]
    		public int  HQID { get; set; }
    
    		    
    		/// <summary>
    		/// Mode
    		/// </summary>        
    	//    [DisplayName("Mode")]
            [MaxLength(2, ErrorMessage = "Mode cannot be longer than 2 characters")]
    		public string  Mode { get; set; }
    
    		    
    		/// <summary>
    		/// Milestone Name
    		/// </summary>        
    	//    [DisplayName("Milestone Name")]
            [Required(ErrorMessage = "Milestone Name is required")]
            [MaxLength(100, ErrorMessage = "Milestone Name cannot be longer than 100 characters")]
    		public string  MilestoneName { get; set; }
    
    		    
    		/// <summary>
    		/// Input Type
    		/// </summary>        
    	//    [DisplayName("Input Type")]
            [MaxLength(1, ErrorMessage = "Input Type cannot be longer than 1 characters")]
    		public string  InputType { get; set; }
    
    		    
    		/// <summary>
    		/// Entry Control
    		/// </summary>        
    	//    [DisplayName("Entry Control")]
            [MaxLength(1, ErrorMessage = "Entry Control cannot be longer than 1 characters")]
    		public string  EntryControl { get; set; }
    
    		    
    		/// <summary>
    		/// Description
    		/// </summary>        
    	//    [DisplayName("Description")]
            [MaxLength(100, ErrorMessage = "Description cannot be longer than 100 characters")]
    		public string  Description { get; set; }
    
    		    
    		/// <summary>
    		/// Seq Num
    		/// </summary>        
    	//    [DisplayName("Seq Num")]
    		public Nullable<int>  SeqNum { get; set; }
    
    		    
    		/// <summary>
    		/// Status
    		/// </summary>        
    	//    [DisplayName("Status")]
            [MaxLength(10, ErrorMessage = "Status cannot be longer than 10 characters")]
    		public string  Status { get; set; }
    
    		    
    		/// <summary>
    		/// IMPStatus
    		/// </summary>        
    	//    [DisplayName("IMPStatus")]
            [MaxLength(1, ErrorMessage = "IMPStatus cannot be longer than 1 characters")]
    		public string  IMPStatus { get; set; }
    
    		    
    		/// <summary>
    		/// View Type
    		/// </summary>        
    	//    [DisplayName("View Type")]
            [MaxLength(1, ErrorMessage = "View Type cannot be longer than 1 characters")]
    		public string  ViewType { get; set; }
    
    		    
    		/// <summary>
    		/// Est Act
    		/// </summary>        
    	//    [DisplayName("Est Act")]
            [MaxLength(1, ErrorMessage = "Est Act cannot be longer than 1 characters")]
    		public string  EstAct { get; set; }
    
    		    
    		/// <summary>
    		/// Created Date
    		/// </summary>        
    	//    [DisplayName("Created Date")]
    		public Nullable<System.DateTime>  CreatedDate { get; set; }
    
    		    
    	}
    //}
    
}
