using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ValuePlus.ViewModel;



namespace ValuePlus.ViewModel.ReSM
{
    
    /// <summary>
    /// SMState class
    /// </summary>
    //[MetadataType(typeof(SMStateViewModel))]
    //public  partial class SMState
    //{
    
    	/// <summary>
    	/// SMState Metadata class
    	/// </summary>
    	public   class SMStateViewModel:ViewModelBase
    	{
    		    
    		/// <summary>
    		/// HQID
    		/// </summary>        
    	//    [DisplayName("HQID")]
            [Required(ErrorMessage = "HQID is required")]
    		public int  HQID { get; set; }
    
    		    
    		/// <summary>
    		/// State Code
    		/// </summary>        
    	//    [DisplayName("State Code")]
            [Required(ErrorMessage = "State Code is required")]
            [MaxLength(5, ErrorMessage = "State Code cannot be longer than 5 characters")]
    		public string  StateCode { get; set; }
    
    		    
    		/// <summary>
    		/// Country ID
    		/// </summary>        
    	//    [DisplayName("Country ID")]
            [Required(ErrorMessage = "Country ID is required")]
    		public int  CountryID { get; set; }
    
    		    
    		/// <summary>
    		/// State Name
    		/// </summary>        
    	//    [DisplayName("State Name")]
            [MaxLength(255, ErrorMessage = "State Name cannot be longer than 255 characters")]
    		public string  StateName { get; set; }
    
    		    
    		/// <summary>
    		/// Status
    		/// </summary>        
    	//    [DisplayName("Status")]
            [Required(ErrorMessage = "Status is required")]
            [MaxLength(10, ErrorMessage = "Status cannot be longer than 10 characters")]
    		public string  Status { get; set; }
    
    		    
    		/// <summary>
    		/// Created By
    		/// </summary>        
    	//    [DisplayName("Created By")]
            [MaxLength(6, ErrorMessage = "Created By cannot be longer than 6 characters")]
    		public string  CreatedBy { get; set; }
    
    		    
    		/// <summary>
    		/// Created Date
    		/// </summary>        
    	//    [DisplayName("Created Date")]
            [Required(ErrorMessage = "Created Date is required")]
    		public System.DateTime  CreatedDate { get; set; }
    
    		    
    		/// <summary>
    		/// Updated By
    		/// </summary>        
    	//    [DisplayName("Updated By")]
            [MaxLength(6, ErrorMessage = "Updated By cannot be longer than 6 characters")]
    		public string  UpdatedBy { get; set; }
    
    		    
    		/// <summary>
    		/// Updated Date
    		/// </summary>        
    	//    [DisplayName("Updated Date")]
    		public Nullable<System.DateTime>  UpdatedDate { get; set; }
    
    		    
    		/// <summary>
    		/// Version
    		/// </summary>        
    	//    [DisplayName("Version")]
            [Required(ErrorMessage = "Version is required")]
            [MaxLength(8, ErrorMessage = "Version cannot be longer than 8 characters")]
    		public byte[]  Version { get; set; }
    
    		    
    		/// <summary>
    		/// Local State Name
    		/// </summary>        
    	//    [DisplayName("Local State Name")]
            [MaxLength(255, ErrorMessage = "Local State Name cannot be longer than 255 characters")]
    		public string  LocalStateName { get; set; }
    
    		    
    	}
    //}
    
}
