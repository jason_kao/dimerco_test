using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ValuePlus.ViewModel;



namespace ValuePlus.ViewModel.ReSM
{
    
    /// <summary>
    /// SMISFData class
    /// </summary>
    //[MetadataType(typeof(SMISFDataViewModel))]
    //public  partial class SMISFData
    //{
    
    	/// <summary>
    	/// SMISFData Metadata class
    	/// </summary>
    	public   class SMISFDataViewModel:ViewModelBase
    	{
    		    
    		/// <summary>
    		/// HQID
    		/// </summary>        
    	//    [DisplayName("HQID")]
            [Required(ErrorMessage = "HQID is required")]
    		public int  HQID { get; set; }
    
    		    
    		/// <summary>
    		/// Customer ID
    		/// </summary>        
    	//    [DisplayName("Customer ID")]
            [Required(ErrorMessage = "Customer ID is required")]
    		public int  CustomerID { get; set; }
    
    		    
    		/// <summary>
    		/// Importer Type
    		/// </summary>        
    	//    [DisplayName("Importer Type")]
            [Required(ErrorMessage = "Importer Type is required")]
            [MaxLength(20, ErrorMessage = "Importer Type cannot be longer than 20 characters")]
    		public string  ImporterType { get; set; }
    
    		    
    		/// <summary>
    		/// Importer IDType
    		/// </summary>        
    	//    [DisplayName("Importer IDType")]
            [Required(ErrorMessage = "Importer IDType is required")]
            [MaxLength(10, ErrorMessage = "Importer IDType cannot be longer than 10 characters")]
    		public string  ImporterIDType { get; set; }
    
    		    
    		/// <summary>
    		/// Importer ID
    		/// </summary>        
    	//    [DisplayName("Importer ID")]
            [MaxLength(15, ErrorMessage = "Importer ID cannot be longer than 15 characters")]
    		public string  ImporterID { get; set; }
    
    		    
    		/// <summary>
    		/// Importer Bond No
    		/// </summary>        
    	//    [DisplayName("Importer Bond No")]
            [MaxLength(50, ErrorMessage = "Importer Bond No cannot be longer than 50 characters")]
    		public string  ImporterBondNo { get; set; }
    
    		    
    		/// <summary>
    		/// ISFBond Activity
    		/// </summary>        
    	//    [DisplayName("ISFBond Activity")]
            [MaxLength(50, ErrorMessage = "ISFBond Activity cannot be longer than 50 characters")]
    		public string  ISFBondActivity { get; set; }
    
    		    
    		/// <summary>
    		/// ISFBond Type
    		/// </summary>        
    	//    [DisplayName("ISFBond Type")]
            [MaxLength(50, ErrorMessage = "ISFBond Type cannot be longer than 50 characters")]
    		public string  ISFBondType { get; set; }
    
    		    
    		/// <summary>
    		/// Surety ID
    		/// </summary>        
    	//    [DisplayName("Surety ID")]
            [MaxLength(50, ErrorMessage = "Surety ID cannot be longer than 50 characters")]
    		public string  SuretyID { get; set; }
    
    		    
    		/// <summary>
    		/// Bond Reference No
    		/// </summary>        
    	//    [DisplayName("Bond Reference No")]
            [MaxLength(50, ErrorMessage = "Bond Reference No cannot be longer than 50 characters")]
    		public string  BondReferenceNo { get; set; }
    
    		    
    		/// <summary>
    		/// Importer Bond Holder ID
    		/// </summary>        
    	//    [DisplayName("Importer Bond Holder ID")]
            [MaxLength(50, ErrorMessage = "Importer Bond Holder ID cannot be longer than 50 characters")]
    		public string  ImporterBondHolderID { get; set; }
    
    		    
    		/// <summary>
    		/// POASigned Date
    		/// </summary>        
    	//    [DisplayName("POASigned Date")]
    		public Nullable<System.DateTime>  POASignedDate { get; set; }
    
    		    
    		/// <summary>
    		/// POASinged Sheet
    		/// </summary>        
    	//    [DisplayName("POASinged Sheet")]
            [MaxLength(100, ErrorMessage = "POASinged Sheet cannot be longer than 100 characters")]
    		public string  POASingedSheet { get; set; }
    
    		    
    		/// <summary>
    		/// Status
    		/// </summary>        
    	//    [DisplayName("Status")]
            [MaxLength(50, ErrorMessage = "Status cannot be longer than 50 characters")]
    		public string  Status { get; set; }
    
    		    
    		/// <summary>
    		/// Created By
    		/// </summary>        
    	//    [DisplayName("Created By")]
            [MaxLength(10, ErrorMessage = "Created By cannot be longer than 10 characters")]
    		public string  CreatedBy { get; set; }
    
    		    
    		/// <summary>
    		/// Created Date
    		/// </summary>        
    	//    [DisplayName("Created Date")]
    		public Nullable<System.DateTime>  CreatedDate { get; set; }
    
    		    
    		/// <summary>
    		/// Updated By
    		/// </summary>        
    	//    [DisplayName("Updated By")]
            [MaxLength(10, ErrorMessage = "Updated By cannot be longer than 10 characters")]
    		public string  UpdatedBy { get; set; }
    
    		    
    		/// <summary>
    		/// Updated Date
    		/// </summary>        
    	//    [DisplayName("Updated Date")]
    		public Nullable<System.DateTime>  UpdatedDate { get; set; }
    
    		    
    		/// <summary>
    		/// Importer Name
    		/// </summary>        
    	//    [DisplayName("Importer Name")]
            [MaxLength(200, ErrorMessage = "Importer Name cannot be longer than 200 characters")]
    		public string  ImporterName { get; set; }
    
    		    
    	}
    //}
    
}
