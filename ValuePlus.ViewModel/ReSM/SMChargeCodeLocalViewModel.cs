using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ValuePlus.ViewModel;



namespace ValuePlus.ViewModel.ReSM
{
    
    /// <summary>
    /// SMChargeCodeLocal class
    /// </summary>
    //[MetadataType(typeof(SMChargeCodeLocalViewModel))]
    //public  partial class SMChargeCodeLocal
    //{
    
    	/// <summary>
    	/// SMChargeCodeLocal Metadata class
    	/// </summary>
    	public   class SMChargeCodeLocalViewModel:ViewModelBase
    	{
    		    
    		/// <summary>
    		/// HQID
    		/// </summary>        
    	//    [DisplayName("HQID")]
            [Required(ErrorMessage = "HQID is required")]
    		public int  HQID { get; set; }
    
    		    
    		/// <summary>
    		/// Station ID
    		/// </summary>        
    	//    [DisplayName("Station ID")]
            [Required(ErrorMessage = "Station ID is required")]
            [MaxLength(50, ErrorMessage = "Station ID cannot be longer than 50 characters")]
    		public string  StationID { get; set; }
    
    		    
    		/// <summary>
    		/// Charge Code ID
    		/// </summary>        
    	//    [DisplayName("Charge Code ID")]
            [Required(ErrorMessage = "Charge Code ID is required")]
    		public int  ChargeCodeID { get; set; }
    
    		    
    		/// <summary>
    		/// Local Name
    		/// </summary>        
    	//    [DisplayName("Local Name")]
            [MaxLength(255, ErrorMessage = "Local Name cannot be longer than 255 characters")]
    		public string  LocalName { get; set; }
    
    		    
    	}
    //}
    
}
