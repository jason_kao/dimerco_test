using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ValuePlus.ViewModel;



namespace ValuePlus.ViewModel.ReSM
{
    
    /// <summary>
    /// SMAgentFormPlace class
    /// </summary>
    //[MetadataType(typeof(SMAgentFormPlaceViewModel))]
    //public  partial class SMAgentFormPlace
    //{
    
    	/// <summary>
    	/// SMAgentFormPlace Metadata class
    	/// </summary>
    	public   class SMAgentFormPlaceViewModel:ViewModelBase
    	{
    		    
    		/// <summary>
    		/// HQID
    		/// </summary>        
    	//    [DisplayName("HQID")]
            [Required(ErrorMessage = "HQID is required")]
    		public int  HQID { get; set; }
    
    		    
    		/// <summary>
    		/// Vendor PLID
    		/// </summary>        
    	//    [DisplayName("Vendor PLID")]
            [Required(ErrorMessage = "Vendor PLID is required")]
    		public int  VendorPLID { get; set; }
    
    		    
    		/// <summary>
    		/// From Place Type
    		/// </summary>        
    	//    [DisplayName("From Place Type")]
            [MaxLength(50, ErrorMessage = "From Place Type cannot be longer than 50 characters")]
    		public string  FromPlaceType { get; set; }
    
    		    
    		/// <summary>
    		/// From Place ID
    		/// </summary>        
    	//    [DisplayName("From Place ID")]
    		public Nullable<int>  FromPlaceID { get; set; }
    
    		    
    		/// <summary>
    		/// To Place Type
    		/// </summary>        
    	//    [DisplayName("To Place Type")]
            [MaxLength(50, ErrorMessage = "To Place Type cannot be longer than 50 characters")]
    		public string  ToPlaceType { get; set; }
    
    		    
    		/// <summary>
    		/// To Place ID
    		/// </summary>        
    	//    [DisplayName("To Place ID")]
    		public Nullable<int>  ToPlaceID { get; set; }
    
    		    
    		/// <summary>
    		/// Createby
    		/// </summary>        
    	//    [DisplayName("Createby")]
            [MaxLength(10, ErrorMessage = "Createby cannot be longer than 10 characters")]
    		public string  Createby { get; set; }
    
    		    
    		/// <summary>
    		/// Createdate
    		/// </summary>        
    	//    [DisplayName("Createdate")]
    		public Nullable<System.DateTime>  Createdate { get; set; }
    
    		    
    		/// <summary>
    		/// Updateby
    		/// </summary>        
    	//    [DisplayName("Updateby")]
            [MaxLength(10, ErrorMessage = "Updateby cannot be longer than 10 characters")]
    		public string  Updateby { get; set; }
    
    		    
    		/// <summary>
    		/// Updatedate
    		/// </summary>        
    	//    [DisplayName("Updatedate")]
    		public Nullable<System.DateTime>  Updatedate { get; set; }
    
    		    
    	}
    //}
    
}
