using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ValuePlus.ViewModel;



namespace ValuePlus.ViewModel.ReSM
{
    
    /// <summary>
    /// SMVesselName class
    /// </summary>
    //[MetadataType(typeof(SMVesselNameViewModel))]
    //public  partial class SMVesselName
    //{
    
    	/// <summary>
    	/// SMVesselName Metadata class
    	/// </summary>
    	public   class SMVesselNameViewModel:ViewModelBase
    	{
    		    
    		/// <summary>
    		/// HQID
    		/// </summary>        
    	//    [DisplayName("HQID")]
            [Required(ErrorMessage = "HQID is required")]
    		public int  HQID { get; set; }
    
    		    
    		/// <summary>
    		/// Vessel Code
    		/// </summary>        
    	//    [DisplayName("Vessel Code")]
            [Required(ErrorMessage = "Vessel Code is required")]
            [MaxLength(50, ErrorMessage = "Vessel Code cannot be longer than 50 characters")]
    		public string  VesselCode { get; set; }
    
    		    
    		/// <summary>
    		/// Vessel Name
    		/// </summary>        
    	//    [DisplayName("Vessel Name")]
            [MaxLength(100, ErrorMessage = "Vessel Name cannot be longer than 100 characters")]
    		public string  VesselName { get; set; }
    
    		    
    		/// <summary>
    		/// Vessel Flag
    		/// </summary>        
    	//    [DisplayName("Vessel Flag")]
            [MaxLength(50, ErrorMessage = "Vessel Flag cannot be longer than 50 characters")]
    		public string  VesselFlag { get; set; }
    
    		    
    		/// <summary>
    		/// Status
    		/// </summary>        
    	//    [DisplayName("Status")]
            [MaxLength(50, ErrorMessage = "Status cannot be longer than 50 characters")]
    		public string  Status { get; set; }
    
    		    
    		/// <summary>
    		/// Created By
    		/// </summary>        
    	//    [DisplayName("Created By")]
            [MaxLength(50, ErrorMessage = "Created By cannot be longer than 50 characters")]
    		public string  CreatedBy { get; set; }
    
    		    
    		/// <summary>
    		/// Created Date
    		/// </summary>        
    	//    [DisplayName("Created Date")]
    		public Nullable<System.DateTime>  CreatedDate { get; set; }
    
    		    
    		/// <summary>
    		/// Updated By
    		/// </summary>        
    	//    [DisplayName("Updated By")]
            [MaxLength(50, ErrorMessage = "Updated By cannot be longer than 50 characters")]
    		public string  UpdatedBy { get; set; }
    
    		    
    		/// <summary>
    		/// Updated Date
    		/// </summary>        
    	//    [DisplayName("Updated Date")]
    		public Nullable<System.DateTime>  UpdatedDate { get; set; }
    
    		    
    	}
    //}
    
}
