using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ValuePlus.ViewModel;



namespace ValuePlus.ViewModel.ReSM
{
    
    /// <summary>
    /// SMAgent class
    /// </summary>
    //[MetadataType(typeof(SMAgentViewModel))]
    //public  partial class SMAgent
    //{
    
    	/// <summary>
    	/// SMAgent Metadata class
    	/// </summary>
    	public   class SMAgentViewModel:ViewModelBase
    	{
    		    
    		/// <summary>
    		/// HQID
    		/// </summary>        
    	//    [DisplayName("HQID")]
            [Required(ErrorMessage = "HQID is required")]
    		public int  HQID { get; set; }
    
    		    
    		/// <summary>
    		/// Station ID
    		/// </summary>        
    	//    [DisplayName("Station ID")]
            [Required(ErrorMessage = "Station ID is required")]
            [MaxLength(3, ErrorMessage = "Station ID cannot be longer than 3 characters")]
    		public string  StationID { get; set; }
    
    		    
    		/// <summary>
    		/// Agent Code
    		/// </summary>        
    	//    [DisplayName("Agent Code")]
            [Required(ErrorMessage = "Agent Code is required")]
            [MaxLength(10, ErrorMessage = "Agent Code cannot be longer than 10 characters")]
    		public string  AgentCode { get; set; }
    
    		    
    		/// <summary>
    		/// Agent Profile ID
    		/// </summary>        
    	//    [DisplayName("Agent Profile ID")]
    		public Nullable<int>  AgentProfileID { get; set; }
    
    		    
    		/// <summary>
    		/// Customer ID
    		/// </summary>        
    	//    [DisplayName("Customer ID")]
    		public Nullable<int>  CustomerID { get; set; }
    
    		    
    		/// <summary>
    		/// City ID
    		/// </summary>        
    	//    [DisplayName("City ID")]
    		public Nullable<int>  CityID { get; set; }
    
    		    
    		/// <summary>
    		/// Agent Name
    		/// </summary>        
    	//    [DisplayName("Agent Name")]
            [Required(ErrorMessage = "Agent Name is required")]
            [MaxLength(255, ErrorMessage = "Agent Name cannot be longer than 255 characters")]
    		public string  AgentName { get; set; }
    
    		    
    		/// <summary>
    		/// Agent Name1
    		/// </summary>        
    	//    [DisplayName("Agent Name1")]
            [MaxLength(255, ErrorMessage = "Agent Name1 cannot be longer than 255 characters")]
    		public string  AgentName1 { get; set; }
    
    		    
    		/// <summary>
    		/// Agent Address1
    		/// </summary>        
    	//    [DisplayName("Agent Address1")]
            [MaxLength(255, ErrorMessage = "Agent Address1 cannot be longer than 255 characters")]
    		public string  AgentAddress1 { get; set; }
    
    		    
    		/// <summary>
    		/// Agent Address2
    		/// </summary>        
    	//    [DisplayName("Agent Address2")]
            [MaxLength(255, ErrorMessage = "Agent Address2 cannot be longer than 255 characters")]
    		public string  AgentAddress2 { get; set; }
    
    		    
    		/// <summary>
    		/// Agent Address3
    		/// </summary>        
    	//    [DisplayName("Agent Address3")]
            [MaxLength(255, ErrorMessage = "Agent Address3 cannot be longer than 255 characters")]
    		public string  AgentAddress3 { get; set; }
    
    		    
    		/// <summary>
    		/// Agent Address4
    		/// </summary>        
    	//    [DisplayName("Agent Address4")]
            [MaxLength(255, ErrorMessage = "Agent Address4 cannot be longer than 255 characters")]
    		public string  AgentAddress4 { get; set; }
    
    		    
    		/// <summary>
    		/// Agent Address5
    		/// </summary>        
    	//    [DisplayName("Agent Address5")]
            [MaxLength(255, ErrorMessage = "Agent Address5 cannot be longer than 255 characters")]
    		public string  AgentAddress5 { get; set; }
    
    		    
    		/// <summary>
    		/// Agent Phone
    		/// </summary>        
    	//    [DisplayName("Agent Phone")]
            [MaxLength(50, ErrorMessage = "Agent Phone cannot be longer than 50 characters")]
    		public string  AgentPhone { get; set; }
    
    		    
    		/// <summary>
    		/// Agent Phone Ext
    		/// </summary>        
    	//    [DisplayName("Agent Phone Ext")]
            [MaxLength(10, ErrorMessage = "Agent Phone Ext cannot be longer than 10 characters")]
    		public string  AgentPhoneExt { get; set; }
    
    		    
    		/// <summary>
    		/// Agent Fax
    		/// </summary>        
    	//    [DisplayName("Agent Fax")]
            [MaxLength(50, ErrorMessage = "Agent Fax cannot be longer than 50 characters")]
    		public string  AgentFax { get; set; }
    
    		    
    		/// <summary>
    		/// Agent Fax Ext
    		/// </summary>        
    	//    [DisplayName("Agent Fax Ext")]
            [MaxLength(10, ErrorMessage = "Agent Fax Ext cannot be longer than 10 characters")]
    		public string  AgentFaxExt { get; set; }
    
    		    
    		/// <summary>
    		/// Agent Zip
    		/// </summary>        
    	//    [DisplayName("Agent Zip")]
            [MaxLength(10, ErrorMessage = "Agent Zip cannot be longer than 10 characters")]
    		public string  AgentZip { get; set; }
    
    		    
    		/// <summary>
    		/// Agent Web Site
    		/// </summary>        
    	//    [DisplayName("Agent Web Site")]
            [MaxLength(255, ErrorMessage = "Agent Web Site cannot be longer than 255 characters")]
    		public string  AgentWebSite { get; set; }
    
    		    
    		/// <summary>
    		/// Agent ID
    		/// </summary>        
    	//    [DisplayName("Agent ID")]
    		public Nullable<int>  AgentID { get; set; }
    
    		    
    		/// <summary>
    		/// Status
    		/// </summary>        
    	//    [DisplayName("Status")]
            [Required(ErrorMessage = "Status is required")]
            [MaxLength(10, ErrorMessage = "Status cannot be longer than 10 characters")]
    		public string  Status { get; set; }
    
    		    
    		/// <summary>
    		/// Created By
    		/// </summary>        
    	//    [DisplayName("Created By")]
            [MaxLength(6, ErrorMessage = "Created By cannot be longer than 6 characters")]
    		public string  CreatedBy { get; set; }
    
    		    
    		/// <summary>
    		/// Created Date
    		/// </summary>        
    	//    [DisplayName("Created Date")]
            [Required(ErrorMessage = "Created Date is required")]
    		public System.DateTime  CreatedDate { get; set; }
    
    		    
    		/// <summary>
    		/// Updated By
    		/// </summary>        
    	//    [DisplayName("Updated By")]
            [MaxLength(6, ErrorMessage = "Updated By cannot be longer than 6 characters")]
    		public string  UpdatedBy { get; set; }
    
    		    
    		/// <summary>
    		/// Updated Date
    		/// </summary>        
    	//    [DisplayName("Updated Date")]
    		public Nullable<System.DateTime>  UpdatedDate { get; set; }
    
    		    
    		/// <summary>
    		/// Version
    		/// </summary>        
    	//    [DisplayName("Version")]
            [MaxLength(8, ErrorMessage = "Version cannot be longer than 8 characters")]
    		public byte[]  Version { get; set; }
    
    		    
    		/// <summary>
    		/// Longitute
    		/// </summary>        
    	//    [DisplayName("Longitute")]
            [MaxLength(50, ErrorMessage = "Longitute cannot be longer than 50 characters")]
    		public string  Longitute { get; set; }
    
    		    
    		/// <summary>
    		/// Latitude
    		/// </summary>        
    	//    [DisplayName("Latitude")]
            [MaxLength(50, ErrorMessage = "Latitude cannot be longer than 50 characters")]
    		public string  Latitude { get; set; }
    
    		    
    		/// <summary>
    		/// Product Line
    		/// </summary>        
    	//    [DisplayName("Product Line")]
            [MaxLength(100, ErrorMessage = "Product Line cannot be longer than 100 characters")]
    		public string  ProductLine { get; set; }
    
    		    
    		/// <summary>
    		/// Branch
    		/// </summary>        
    	//    [DisplayName("Branch")]
            [MaxLength(200, ErrorMessage = "Branch cannot be longer than 200 characters")]
    		public string  Branch { get; set; }
    
    		    
    		/// <summary>
    		/// Continent
    		/// </summary>        
    	//    [DisplayName("Continent")]
            [MaxLength(50, ErrorMessage = "Continent cannot be longer than 50 characters")]
    		public string  Continent { get; set; }
    
    		    
    		/// <summary>
    		/// ext Show
    		/// </summary>        
    	//    [DisplayName("ext Show")]
            [MaxLength(10, ErrorMessage = "ext Show cannot be longer than 10 characters")]
    		public string  extShow { get; set; }
    
    		    
    		/// <summary>
    		/// e Mail
    		/// </summary>        
    	//    [DisplayName("e Mail")]
            [MaxLength(500, ErrorMessage = "e Mail cannot be longer than 500 characters")]
    		public string  eMail { get; set; }
    
    		    
    	}
    //}
    
}
