using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ValuePlus.ViewModel;



namespace ValuePlus.ViewModel.ReSM
{
    
    /// <summary>
    /// SMCountry class
    /// </summary>
    //[MetadataType(typeof(SMCountryViewModel))]
    //public  partial class SMCountry
    //{
    
    	/// <summary>
    	/// SMCountry Metadata class
    	/// </summary>
    	public   class SMCountryViewModel:ViewModelBase
    	{
    		    
    		/// <summary>
    		/// HQID
    		/// </summary>        
    	//    [DisplayName("HQID")]
            [Required(ErrorMessage = "HQID is required")]
    		public int  HQID { get; set; }
    
    		    
    		/// <summary>
    		/// Country Code
    		/// </summary>        
    	//    [DisplayName("Country Code")]
            [Required(ErrorMessage = "Country Code is required")]
            [MaxLength(5, ErrorMessage = "Country Code cannot be longer than 5 characters")]
    		public string  CountryCode { get; set; }
    
    		    
    		/// <summary>
    		/// Country Name
    		/// </summary>        
    	//    [DisplayName("Country Name")]
            [MaxLength(255, ErrorMessage = "Country Name cannot be longer than 255 characters")]
    		public string  CountryName { get; set; }
    
    		    
    		/// <summary>
    		/// Area ID
    		/// </summary>        
    	//    [DisplayName("Area ID")]
    		public Nullable<int>  AreaID { get; set; }
    
    		    
    		/// <summary>
    		/// GRegion ID
    		/// </summary>        
    	//    [DisplayName("GRegion ID")]
    		public Nullable<int>  GRegionID { get; set; }
    
    		    
    		/// <summary>
    		/// Prefix Number
    		/// </summary>        
    	//    [DisplayName("Prefix Number")]
            [MaxLength(10, ErrorMessage = "Prefix Number cannot be longer than 10 characters")]
    		public string  PrefixNumber { get; set; }
    
    		    
    		/// <summary>
    		/// Status
    		/// </summary>        
    	//    [DisplayName("Status")]
            [Required(ErrorMessage = "Status is required")]
            [MaxLength(10, ErrorMessage = "Status cannot be longer than 10 characters")]
    		public string  Status { get; set; }
    
    		    
    		/// <summary>
    		/// Created By
    		/// </summary>        
    	//    [DisplayName("Created By")]
            [MaxLength(6, ErrorMessage = "Created By cannot be longer than 6 characters")]
    		public string  CreatedBy { get; set; }
    
    		    
    		/// <summary>
    		/// Created Date
    		/// </summary>        
    	//    [DisplayName("Created Date")]
            [Required(ErrorMessage = "Created Date is required")]
    		public System.DateTime  CreatedDate { get; set; }
    
    		    
    		/// <summary>
    		/// Updated By
    		/// </summary>        
    	//    [DisplayName("Updated By")]
            [MaxLength(6, ErrorMessage = "Updated By cannot be longer than 6 characters")]
    		public string  UpdatedBy { get; set; }
    
    		    
    		/// <summary>
    		/// Updated Date
    		/// </summary>        
    	//    [DisplayName("Updated Date")]
    		public Nullable<System.DateTime>  UpdatedDate { get; set; }
    
    		    
    		/// <summary>
    		/// Version
    		/// </summary>        
    	//    [DisplayName("Version")]
            [Required(ErrorMessage = "Version is required")]
            [MaxLength(8, ErrorMessage = "Version cannot be longer than 8 characters")]
    		public byte[]  Version { get; set; }
    
    		    
    		/// <summary>
    		/// Currency
    		/// </summary>        
    	//    [DisplayName("Currency")]
            [MaxLength(3, ErrorMessage = "Currency cannot be longer than 3 characters")]
    		public string  Currency { get; set; }
    
    		    
    		/// <summary>
    		/// Length UOMID
    		/// </summary>        
    	//    [DisplayName("Length UOMID")]
    		public Nullable<int>  LengthUOMID { get; set; }
    
    		    
    		/// <summary>
    		/// Weight UOMID
    		/// </summary>        
    	//    [DisplayName("Weight UOMID")]
    		public Nullable<int>  WeightUOMID { get; set; }
    
    		    
    		/// <summary>
    		/// Volume UOMID
    		/// </summary>        
    	//    [DisplayName("Volume UOMID")]
    		public Nullable<int>  VolumeUOMID { get; set; }
    
    		    
    		/// <summary>
    		/// Show State
    		/// </summary>        
    	//    [DisplayName("Show State")]
            [MaxLength(20, ErrorMessage = "Show State cannot be longer than 20 characters")]
    		public string  ShowState { get; set; }
    
    		    
    		/// <summary>
    		/// Mandatory State
    		/// </summary>        
    	//    [DisplayName("Mandatory State")]
    		public Nullable<bool>  MandatoryState { get; set; }
    
    		    
    		/// <summary>
    		/// Show Zip
    		/// </summary>        
    	//    [DisplayName("Show Zip")]
            [MaxLength(20, ErrorMessage = "Show Zip cannot be longer than 20 characters")]
    		public string  ShowZip { get; set; }
    
    		    
    		/// <summary>
    		/// Mandatory Zip
    		/// </summary>        
    	//    [DisplayName("Mandatory Zip")]
    		public Nullable<bool>  MandatoryZip { get; set; }
    
    		    
    		/// <summary>
    		/// VATName
    		/// </summary>        
    	//    [DisplayName("VATName")]
            [Required(ErrorMessage = "VATName is required")]
            [MaxLength(50, ErrorMessage = "VATName cannot be longer than 50 characters")]
    		public string  VATName { get; set; }
    
    		    
    		/// <summary>
    		/// Local County Name
    		/// </summary>        
    	//    [DisplayName("Local County Name")]
            [MaxLength(255, ErrorMessage = "Local County Name cannot be longer than 255 characters")]
    		public string  LocalCountyName { get; set; }
    
    		    
    		/// <summary>
    		/// Mandatory AMSZip
    		/// </summary>        
    	//    [DisplayName("Mandatory AMSZip")]
    		public Nullable<bool>  MandatoryAMSZip { get; set; }
    
    		    
    	}
    //}
    
}
