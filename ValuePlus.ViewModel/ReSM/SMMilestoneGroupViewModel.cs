using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ValuePlus.ViewModel;



namespace ValuePlus.ViewModel.ReSM
{
    
    /// <summary>
    /// SMMilestoneGroup class
    /// </summary>
    //[MetadataType(typeof(SMMilestoneGroupViewModel))]
    //public  partial class SMMilestoneGroup
    //{
    
    	/// <summary>
    	/// SMMilestoneGroup Metadata class
    	/// </summary>
    	public   class SMMilestoneGroupViewModel:ViewModelBase
    	{
    		    
    		/// <summary>
    		/// Customer ID
    		/// </summary>        
    	//    [DisplayName("Customer ID")]
            [Required(ErrorMessage = "Customer ID is required")]
    		public int  CustomerID { get; set; }
    
    		    
    		/// <summary>
    		/// Milestone Group ID
    		/// </summary>        
    	//    [DisplayName("Milestone Group ID")]
            [Required(ErrorMessage = "Milestone Group ID is required")]
    		public int  MilestoneGroupID { get; set; }
    
    		    
    		/// <summary>
    		/// Created By
    		/// </summary>        
    	//    [DisplayName("Created By")]
            [MaxLength(10, ErrorMessage = "Created By cannot be longer than 10 characters")]
    		public string  CreatedBy { get; set; }
    
    		    
    		/// <summary>
    		/// Created Date
    		/// </summary>        
    	//    [DisplayName("Created Date")]
    		public Nullable<System.DateTime>  CreatedDate { get; set; }
    
    		    
    		/// <summary>
    		/// Updated By
    		/// </summary>        
    	//    [DisplayName("Updated By")]
            [MaxLength(10, ErrorMessage = "Updated By cannot be longer than 10 characters")]
    		public string  UpdatedBy { get; set; }
    
    		    
    		/// <summary>
    		/// Updated Date
    		/// </summary>        
    	//    [DisplayName("Updated Date")]
    		public Nullable<System.DateTime>  UpdatedDate { get; set; }
    
    		    
    		/// <summary>
    		/// Productline
    		/// </summary>        
    	//    [DisplayName("Productline")]
    		public Nullable<int>  Productline { get; set; }
    
    		    
    		/// <summary>
    		/// Global Code
    		/// </summary>        
    	//    [DisplayName("Global Code")]
            [MaxLength(10, ErrorMessage = "Global Code cannot be longer than 10 characters")]
    		public string  GlobalCode { get; set; }
    
    		    
    		/// <summary>
    		/// Move Code
    		/// </summary>        
    	//    [DisplayName("Move Code")]
            [MaxLength(3, ErrorMessage = "Move Code cannot be longer than 3 characters")]
    		public string  MoveCode { get; set; }
    
    		    
    		/// <summary>
    		/// Project ID
    		/// </summary>        
    	//    [DisplayName("Project ID")]
    		public Nullable<int>  ProjectID { get; set; }
    
    		    
    		/// <summary>
    		/// SHPTTYPE
    		/// </summary>        
    	//    [DisplayName("SHPTTYPE")]
            [MaxLength(3, ErrorMessage = "SHPTTYPE cannot be longer than 3 characters")]
    		public string  SHPTTYPE { get; set; }
    
    		    
    	}
    //}
    
}
