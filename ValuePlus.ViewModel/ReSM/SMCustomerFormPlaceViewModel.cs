using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ValuePlus.ViewModel;



namespace ValuePlus.ViewModel.ReSM
{
    
    /// <summary>
    /// SMCustomerFormPlace class
    /// </summary>
    //[MetadataType(typeof(SMCustomerFormPlaceViewModel))]
    //public  partial class SMCustomerFormPlace
    //{
    
    	/// <summary>
    	/// SMCustomerFormPlace Metadata class
    	/// </summary>
    	public   class SMCustomerFormPlaceViewModel:ViewModelBase
    	{
    		    
    		/// <summary>
    		/// HQID
    		/// </summary>        
    	//    [DisplayName("HQID")]
            [Required(ErrorMessage = "HQID is required")]
    		public int  HQID { get; set; }
    
    		    
    		/// <summary>
    		/// Form ID
    		/// </summary>        
    	//    [DisplayName("Form ID")]
            [Required(ErrorMessage = "Form ID is required")]
    		public int  FormID { get; set; }
    
    		    
    		/// <summary>
    		/// From Place Type
    		/// </summary>        
    	//    [DisplayName("From Place Type")]
            [MaxLength(50, ErrorMessage = "From Place Type cannot be longer than 50 characters")]
    		public string  FromPlaceType { get; set; }
    
    		    
    		/// <summary>
    		/// From Place ID
    		/// </summary>        
    	//    [DisplayName("From Place ID")]
    		public Nullable<int>  FromPlaceID { get; set; }
    
    		    
    		/// <summary>
    		/// To Place Type
    		/// </summary>        
    	//    [DisplayName("To Place Type")]
            [MaxLength(50, ErrorMessage = "To Place Type cannot be longer than 50 characters")]
    		public string  ToPlaceType { get; set; }
    
    		    
    		/// <summary>
    		/// To Place ID
    		/// </summary>        
    	//    [DisplayName("To Place ID")]
    		public Nullable<int>  ToPlaceID { get; set; }
    
    		    
    	}
    //}
    
}
