using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ValuePlus.ViewModel;



namespace ValuePlus.ViewModel.ReSM
{
    
    /// <summary>
    /// SMTradeTerm class
    /// </summary>
    //[MetadataType(typeof(SMTradeTermViewModel))]
    //public  partial class SMTradeTerm
    //{
    
    	/// <summary>
    	/// SMTradeTerm Metadata class
    	/// </summary>
    	public   class SMTradeTermViewModel:ViewModelBase
    	{
    		    
    		/// <summary>
    		/// HQID
    		/// </summary>        
    	//    [DisplayName("HQID")]
            [Required(ErrorMessage = "HQID is required")]
    		public int  HQID { get; set; }
    
    		    
    		/// <summary>
    		/// Trade Term Code
    		/// </summary>        
    	//    [DisplayName("Trade Term Code")]
            [Required(ErrorMessage = "Trade Term Code is required")]
            [MaxLength(10, ErrorMessage = "Trade Term Code cannot be longer than 10 characters")]
    		public string  TradeTermCode { get; set; }
    
    		    
    		/// <summary>
    		/// Trade Term Name
    		/// </summary>        
    	//    [DisplayName("Trade Term Name")]
            [MaxLength(255, ErrorMessage = "Trade Term Name cannot be longer than 255 characters")]
    		public string  TradeTermName { get; set; }
    
    		    
    		/// <summary>
    		/// Status
    		/// </summary>        
    	//    [DisplayName("Status")]
            [Required(ErrorMessage = "Status is required")]
            [MaxLength(10, ErrorMessage = "Status cannot be longer than 10 characters")]
    		public string  Status { get; set; }
    
    		    
    		/// <summary>
    		/// Created By
    		/// </summary>        
    	//    [DisplayName("Created By")]
            [MaxLength(6, ErrorMessage = "Created By cannot be longer than 6 characters")]
    		public string  CreatedBy { get; set; }
    
    		    
    		/// <summary>
    		/// Created Date
    		/// </summary>        
    	//    [DisplayName("Created Date")]
            [Required(ErrorMessage = "Created Date is required")]
    		public System.DateTime  CreatedDate { get; set; }
    
    		    
    		/// <summary>
    		/// Updated By
    		/// </summary>        
    	//    [DisplayName("Updated By")]
            [MaxLength(6, ErrorMessage = "Updated By cannot be longer than 6 characters")]
    		public string  UpdatedBy { get; set; }
    
    		    
    		/// <summary>
    		/// Updated Date
    		/// </summary>        
    	//    [DisplayName("Updated Date")]
    		public Nullable<System.DateTime>  UpdatedDate { get; set; }
    
    		    
    		/// <summary>
    		/// Version
    		/// </summary>        
    	//    [DisplayName("Version")]
            [Required(ErrorMessage = "Version is required")]
            [MaxLength(8, ErrorMessage = "Version cannot be longer than 8 characters")]
    		public byte[]  Version { get; set; }
    
    		    
    		/// <summary>
    		/// FRT
    		/// </summary>        
    	//    [DisplayName("FRT")]
            [MaxLength(10, ErrorMessage = "FRT cannot be longer than 10 characters")]
    		public string  FRT { get; set; }
    
    		    
    		/// <summary>
    		/// Other
    		/// </summary>        
    	//    [DisplayName("Other")]
            [MaxLength(10, ErrorMessage = "Other cannot be longer than 10 characters")]
    		public string  Other { get; set; }
    
    		    
    		/// <summary>
    		/// Move Code
    		/// </summary>        
    	//    [DisplayName("Move Code")]
            [MaxLength(10, ErrorMessage = "Move Code cannot be longer than 10 characters")]
    		public string  MoveCode { get; set; }
    
    		    
    	}
    //}
    
}
