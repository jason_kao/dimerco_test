using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ValuePlus.ViewModel;



namespace ValuePlus.ViewModel.ReSM
{
    
    /// <summary>
    /// SMCustomerPL class
    /// </summary>
    //[MetadataType(typeof(SMCustomerPLViewModel))]
    //public  partial class SMCustomerPL
    //{
    
    	/// <summary>
    	/// SMCustomerPL Metadata class
    	/// </summary>
    	public   class SMCustomerPLViewModel:ViewModelBase
    	{
    		    
    		/// <summary>
    		/// HQID
    		/// </summary>        
    	//    [DisplayName("HQID")]
            [Required(ErrorMessage = "HQID is required")]
    		public int  HQID { get; set; }
    
    		    
    		/// <summary>
    		/// Customer ID
    		/// </summary>        
    	//    [DisplayName("Customer ID")]
            [Required(ErrorMessage = "Customer ID is required")]
    		public int  CustomerID { get; set; }
    
    		    
    		/// <summary>
    		/// Product Line ID
    		/// </summary>        
    	//    [DisplayName("Product Line ID")]
            [Required(ErrorMessage = "Product Line ID is required")]
    		public int  ProductLineID { get; set; }
    
    		    
    		/// <summary>
    		/// Is Station
    		/// </summary>        
    	//    [DisplayName("Is Station")]
    		public Nullable<bool>  IsStation { get; set; }
    
    		    
    		/// <summary>
    		/// Is Foreign Agent
    		/// </summary>        
    	//    [DisplayName("Is Foreign Agent")]
    		public Nullable<bool>  IsForeignAgent { get; set; }
    
    		    
    		/// <summary>
    		/// Is Local Agent
    		/// </summary>        
    	//    [DisplayName("Is Local Agent")]
    		public Nullable<bool>  IsLocalAgent { get; set; }
    
    		    
    		/// <summary>
    		/// Is Air Line
    		/// </summary>        
    	//    [DisplayName("Is Air Line")]
    		public Nullable<bool>  IsAirLine { get; set; }
    
    		    
    		/// <summary>
    		/// Is Ocean Carrier
    		/// </summary>        
    	//    [DisplayName("Is Ocean Carrier")]
    		public Nullable<bool>  IsOceanCarrier { get; set; }
    
    		    
    		/// <summary>
    		/// Is Customer
    		/// </summary>        
    	//    [DisplayName("Is Customer")]
    		public Nullable<bool>  IsCustomer { get; set; }
    
    		    
    		/// <summary>
    		/// Is Shipper
    		/// </summary>        
    	//    [DisplayName("Is Shipper")]
    		public Nullable<bool>  IsShipper { get; set; }
    
    		    
    		/// <summary>
    		/// Is CNEE
    		/// </summary>        
    	//    [DisplayName("Is CNEE")]
    		public Nullable<bool>  IsCNEE { get; set; }
    
    		    
    		/// <summary>
    		/// Is3rd Party
    		/// </summary>        
    	//    [DisplayName("Is3rd Party")]
    		public Nullable<bool>  Is3rdParty { get; set; }
    
    		    
    		/// <summary>
    		/// Is Notify
    		/// </summary>        
    	//    [DisplayName("Is Notify")]
    		public Nullable<bool>  IsNotify { get; set; }
    
    		    
    		/// <summary>
    		/// Is Broker
    		/// </summary>        
    	//    [DisplayName("Is Broker")]
    		public Nullable<bool>  IsBroker { get; set; }
    
    		    
    		/// <summary>
    		/// DASCarrier Country ID
    		/// </summary>        
    	//    [DisplayName("DASCarrier Country ID")]
    		public Nullable<int>  DASCarrierCountryID { get; set; }
    
    		    
    		/// <summary>
    		/// Status
    		/// </summary>        
    	//    [DisplayName("Status")]
            [Required(ErrorMessage = "Status is required")]
            [MaxLength(50, ErrorMessage = "Status cannot be longer than 50 characters")]
    		public string  Status { get; set; }
    
    		    
    		/// <summary>
    		/// Created By
    		/// </summary>        
    	//    [DisplayName("Created By")]
            [MaxLength(6, ErrorMessage = "Created By cannot be longer than 6 characters")]
    		public string  CreatedBy { get; set; }
    
    		    
    		/// <summary>
    		/// Created Date
    		/// </summary>        
    	//    [DisplayName("Created Date")]
            [Required(ErrorMessage = "Created Date is required")]
    		public System.DateTime  CreatedDate { get; set; }
    
    		    
    		/// <summary>
    		/// Updated By
    		/// </summary>        
    	//    [DisplayName("Updated By")]
            [MaxLength(6, ErrorMessage = "Updated By cannot be longer than 6 characters")]
    		public string  UpdatedBy { get; set; }
    
    		    
    		/// <summary>
    		/// Updated Date
    		/// </summary>        
    	//    [DisplayName("Updated Date")]
    		public Nullable<System.DateTime>  UpdatedDate { get; set; }
    
    		    
    		/// <summary>
    		/// Version
    		/// </summary>        
    	//    [DisplayName("Version")]
            [MaxLength(8, ErrorMessage = "Version cannot be longer than 8 characters")]
    		public byte[]  Version { get; set; }
    
    		    
    		/// <summary>
    		/// Is Forward
    		/// </summary>        
    	//    [DisplayName("Is Forward")]
    		public Nullable<bool>  IsForward { get; set; }
    
    		    
    		/// <summary>
    		/// Is Trucker
    		/// </summary>        
    	//    [DisplayName("Is Trucker")]
    		public Nullable<bool>  IsTrucker { get; set; }
    
    		    
    		/// <summary>
    		/// Is Vendor
    		/// </summary>        
    	//    [DisplayName("Is Vendor")]
    		public Nullable<bool>  IsVendor { get; set; }
    
    		    
    		/// <summary>
    		/// Is Bill To
    		/// </summary>        
    	//    [DisplayName("Is Bill To")]
    		public Nullable<bool>  IsBillTo { get; set; }
    
    		    
    		/// <summary>
    		/// Sales Person ID
    		/// </summary>        
    	//    [DisplayName("Sales Person ID")]
    		public Nullable<int>  SalesPersonID { get; set; }
    
    		    
    		/// <summary>
    		/// Customer Service ID
    		/// </summary>        
    	//    [DisplayName("Customer Service ID")]
    		public Nullable<int>  CustomerServiceID { get; set; }
    
    		    
    		/// <summary>
    		/// Is Warehouse
    		/// </summary>        
    	//    [DisplayName("Is Warehouse")]
    		public Nullable<bool>  IsWarehouse { get; set; }
    
    		    
    		/// <summary>
    		/// Language
    		/// </summary>        
    	//    [DisplayName("Language")]
            [Required(ErrorMessage = "Language is required")]
            [MaxLength(1, ErrorMessage = "Language cannot be longer than 1 characters")]
    		public string  Language { get; set; }
    
    		    
    		/// <summary>
    		/// Contact Person
    		/// </summary>        
    	//    [DisplayName("Contact Person")]
            [MaxLength(500, ErrorMessage = "Contact Person cannot be longer than 500 characters")]
    		public string  ContactPerson { get; set; }
    
    		    
    		/// <summary>
    		/// Is MNC
    		/// </summary>        
    	//    [DisplayName("Is MNC")]
    		public Nullable<bool>  IsMNC { get; set; }
    
    		    
    		/// <summary>
    		/// Need Book Confirm
    		/// </summary>        
    	//    [DisplayName("Need Book Confirm")]
            [MaxLength(1, ErrorMessage = "Need Book Confirm cannot be longer than 1 characters")]
    		public string  NeedBookConfirm { get; set; }
    
    		    
    		/// <summary>
    		/// POA
    		/// </summary>        
    	//    [DisplayName("POA")]
            [MaxLength(1, ErrorMessage = "POA cannot be longer than 1 characters")]
    		public string  POA { get; set; }
    
    		    
    		/// <summary>
    		/// Is Co Load
    		/// </summary>        
    	//    [DisplayName("Is Co Load")]
    		public Nullable<bool>  IsCoLoad { get; set; }
    
    		    
    		/// <summary>
    		/// Is Brrowin
    		/// </summary>        
    	//    [DisplayName("Is Brrowin")]
    		public Nullable<bool>  IsBrrowin { get; set; }
    
    		    
    		/// <summary>
    		/// Allow Co Load Out
    		/// </summary>        
    	//    [DisplayName("Allow Co Load Out")]
    		public Nullable<bool>  AllowCoLoadOut { get; set; }
    
    		    
    		/// <summary>
    		/// Is Borrowin
    		/// </summary>        
    	//    [DisplayName("Is Borrowin")]
    		public Nullable<bool>  IsBorrowin { get; set; }
    
    		    
    	}
    //}
    
}
