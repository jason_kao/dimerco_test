using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ValuePlus.ViewModel;



namespace ValuePlus.ViewModel.ReSM
{
    
    /// <summary>
    /// SMLeadSource class
    /// </summary>
    //[MetadataType(typeof(SMLeadSourceViewModel))]
    //public  partial class SMLeadSource
    //{
    
    	/// <summary>
    	/// SMLeadSource Metadata class
    	/// </summary>
    	public   class SMLeadSourceViewModel:ViewModelBase
    	{
    		    
    		/// <summary>
    		/// HQID
    		/// </summary>        
    	//    [DisplayName("HQID")]
            [Required(ErrorMessage = "HQID is required")]
    		public int  HQID { get; set; }
    
    		    
    		/// <summary>
    		/// Lead Source Code
    		/// </summary>        
    	//    [DisplayName("Lead Source Code")]
            [Required(ErrorMessage = "Lead Source Code is required")]
            [MaxLength(10, ErrorMessage = "Lead Source Code cannot be longer than 10 characters")]
    		public string  LeadSourceCode { get; set; }
    
    		    
    		/// <summary>
    		/// Lead Source Name
    		/// </summary>        
    	//    [DisplayName("Lead Source Name")]
            [Required(ErrorMessage = "Lead Source Name is required")]
            [MaxLength(255, ErrorMessage = "Lead Source Name cannot be longer than 255 characters")]
    		public string  LeadSourceName { get; set; }
    
    		    
    		/// <summary>
    		/// Lead Source Group ID
    		/// </summary>        
    	//    [DisplayName("Lead Source Group ID")]
            [Required(ErrorMessage = "Lead Source Group ID is required")]
    		public int  LeadSourceGroupID { get; set; }
    
    		    
    		/// <summary>
    		/// Status
    		/// </summary>        
    	//    [DisplayName("Status")]
            [Required(ErrorMessage = "Status is required")]
            [MaxLength(50, ErrorMessage = "Status cannot be longer than 50 characters")]
    		public string  Status { get; set; }
    
    		    
    		/// <summary>
    		/// Created By
    		/// </summary>        
    	//    [DisplayName("Created By")]
            [MaxLength(6, ErrorMessage = "Created By cannot be longer than 6 characters")]
    		public string  CreatedBy { get; set; }
    
    		    
    		/// <summary>
    		/// Created Date
    		/// </summary>        
    	//    [DisplayName("Created Date")]
            [Required(ErrorMessage = "Created Date is required")]
    		public System.DateTime  CreatedDate { get; set; }
    
    		    
    		/// <summary>
    		/// Updated By
    		/// </summary>        
    	//    [DisplayName("Updated By")]
            [MaxLength(6, ErrorMessage = "Updated By cannot be longer than 6 characters")]
    		public string  UpdatedBy { get; set; }
    
    		    
    		/// <summary>
    		/// Updated Date
    		/// </summary>        
    	//    [DisplayName("Updated Date")]
    		public Nullable<System.DateTime>  UpdatedDate { get; set; }
    
    		    
    	}
    //}
    
}
