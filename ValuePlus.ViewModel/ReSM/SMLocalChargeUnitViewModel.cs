using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ValuePlus.ViewModel;



namespace ValuePlus.ViewModel.ReSM
{
    
    /// <summary>
    /// SMLocalChargeUnit class
    /// </summary>
    //[MetadataType(typeof(SMLocalChargeUnitViewModel))]
    //public  partial class SMLocalChargeUnit
    //{
    
    	/// <summary>
    	/// SMLocalChargeUnit Metadata class
    	/// </summary>
    	public   class SMLocalChargeUnitViewModel:ViewModelBase
    	{
    		    
    		/// <summary>
    		/// HQID
    		/// </summary>        
    	//    [DisplayName("HQID")]
            [Required(ErrorMessage = "HQID is required")]
    		public int  HQID { get; set; }
    
    		    
    		/// <summary>
    		/// Product Line ID
    		/// </summary>        
    	//    [DisplayName("Product Line ID")]
            [Required(ErrorMessage = "Product Line ID is required")]
    		public int  ProductLineID { get; set; }
    
    		    
    		/// <summary>
    		/// Mode ID
    		/// </summary>        
    	//    [DisplayName("Mode ID")]
    		public Nullable<int>  ModeID { get; set; }
    
    		    
    		/// <summary>
    		/// Unit Code
    		/// </summary>        
    	//    [DisplayName("Unit Code")]
            [Required(ErrorMessage = "Unit Code is required")]
            [MaxLength(50, ErrorMessage = "Unit Code cannot be longer than 50 characters")]
    		public string  UnitCode { get; set; }
    
    		    
    		/// <summary>
    		/// Unit Name
    		/// </summary>        
    	//    [DisplayName("Unit Name")]
            [Required(ErrorMessage = "Unit Name is required")]
            [MaxLength(50, ErrorMessage = "Unit Name cannot be longer than 50 characters")]
    		public string  UnitName { get; set; }
    
    		    
    	}
    //}
    
}
