using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ValuePlus.ViewModel;



namespace ValuePlus.ViewModel.ReSM
{
    
    /// <summary>
    /// SMCountryHual class
    /// </summary>
    //[MetadataType(typeof(SMCountryHualViewModel))]
    //public  partial class SMCountryHual
    //{
    
    	/// <summary>
    	/// SMCountryHual Metadata class
    	/// </summary>
    	public   class SMCountryHualViewModel:ViewModelBase
    	{
    		    
    		/// <summary>
    		/// HQID
    		/// </summary>        
    	//    [DisplayName("HQID")]
            [Required(ErrorMessage = "HQID is required")]
    		public int  HQID { get; set; }
    
    		    
    		/// <summary>
    		/// From Country ID
    		/// </summary>        
    	//    [DisplayName("From Country ID")]
            [Required(ErrorMessage = "From Country ID is required")]
    		public int  FromCountryID { get; set; }
    
    		    
    		/// <summary>
    		/// To Country ID
    		/// </summary>        
    	//    [DisplayName("To Country ID")]
            [Required(ErrorMessage = "To Country ID is required")]
    		public int  ToCountryID { get; set; }
    
    		    
    		/// <summary>
    		/// Hual Method
    		/// </summary>        
    	//    [DisplayName("Hual Method")]
            [MaxLength(50, ErrorMessage = "Hual Method cannot be longer than 50 characters")]
    		public string  HualMethod { get; set; }
    
    		    
    		/// <summary>
    		/// Status
    		/// </summary>        
    	//    [DisplayName("Status")]
            [MaxLength(10, ErrorMessage = "Status cannot be longer than 10 characters")]
    		public string  Status { get; set; }
    
    		    
    		/// <summary>
    		/// Created By
    		/// </summary>        
    	//    [DisplayName("Created By")]
            [MaxLength(10, ErrorMessage = "Created By cannot be longer than 10 characters")]
    		public string  CreatedBy { get; set; }
    
    		    
    		/// <summary>
    		/// Created Date
    		/// </summary>        
    	//    [DisplayName("Created Date")]
            [Required(ErrorMessage = "Created Date is required")]
    		public System.DateTime  CreatedDate { get; set; }
    
    		    
    		/// <summary>
    		/// Updated By
    		/// </summary>        
    	//    [DisplayName("Updated By")]
            [MaxLength(10, ErrorMessage = "Updated By cannot be longer than 10 characters")]
    		public string  UpdatedBy { get; set; }
    
    		    
    		/// <summary>
    		/// Updated Date
    		/// </summary>        
    	//    [DisplayName("Updated Date")]
    		public Nullable<System.DateTime>  UpdatedDate { get; set; }
    
    		    
    		/// <summary>
    		/// Version
    		/// </summary>        
    	//    [DisplayName("Version")]
            [Required(ErrorMessage = "Version is required")]
            [MaxLength(8, ErrorMessage = "Version cannot be longer than 8 characters")]
    		public byte[]  Version { get; set; }
    
    		    
    	}
    //}
    
}
