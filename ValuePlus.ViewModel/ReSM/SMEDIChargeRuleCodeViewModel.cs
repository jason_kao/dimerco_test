using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ValuePlus.ViewModel;



namespace ValuePlus.ViewModel.ReSM
{
    
    /// <summary>
    /// SMEDIChargeRuleCode class
    /// </summary>
    //[MetadataType(typeof(SMEDIChargeRuleCodeViewModel))]
    //public  partial class SMEDIChargeRuleCode
    //{
    
    	/// <summary>
    	/// SMEDIChargeRuleCode Metadata class
    	/// </summary>
    	public   class SMEDIChargeRuleCodeViewModel:ViewModelBase
    	{
    		    
    		/// <summary>
    		/// HQID
    		/// </summary>        
    	//    [DisplayName("HQID")]
            [Required(ErrorMessage = "HQID is required")]
    		public int  HQID { get; set; }
    
    		    
    		/// <summary>
    		/// Rule ID
    		/// </summary>        
    	//    [DisplayName("Rule ID")]
    		public Nullable<int>  RuleID { get; set; }
    
    		    
    		/// <summary>
    		/// Charge Code
    		/// </summary>        
    	//    [DisplayName("Charge Code")]
            [MaxLength(10, ErrorMessage = "Charge Code cannot be longer than 10 characters")]
    		public string  ChargeCode { get; set; }
    
    		    
    		/// <summary>
    		/// Charge Meaning
    		/// </summary>        
    	//    [DisplayName("Charge Meaning")]
            [MaxLength(100, ErrorMessage = "Charge Meaning cannot be longer than 100 characters")]
    		public string  ChargeMeaning { get; set; }
    
    		    
    		/// <summary>
    		/// DIMCharge ID
    		/// </summary>        
    	//    [DisplayName("DIMCharge ID")]
    		public Nullable<int>  DIMChargeID { get; set; }
    
    		    
    		/// <summary>
    		/// DIMCharge Code
    		/// </summary>        
    	//    [DisplayName("DIMCharge Code")]
            [MaxLength(10, ErrorMessage = "DIMCharge Code cannot be longer than 10 characters")]
    		public string  DIMChargeCode { get; set; }
    
    		    
    		/// <summary>
    		/// Status
    		/// </summary>        
    	//    [DisplayName("Status")]
            [MaxLength(10, ErrorMessage = "Status cannot be longer than 10 characters")]
    		public string  Status { get; set; }
    
    		    
    		/// <summary>
    		/// Created By
    		/// </summary>        
    	//    [DisplayName("Created By")]
            [MaxLength(10, ErrorMessage = "Created By cannot be longer than 10 characters")]
    		public string  CreatedBy { get; set; }
    
    		    
    		/// <summary>
    		/// Created Date
    		/// </summary>        
    	//    [DisplayName("Created Date")]
    		public Nullable<System.DateTime>  CreatedDate { get; set; }
    
    		    
    		/// <summary>
    		/// Updated By
    		/// </summary>        
    	//    [DisplayName("Updated By")]
            [MaxLength(10, ErrorMessage = "Updated By cannot be longer than 10 characters")]
    		public string  UpdatedBy { get; set; }
    
    		    
    		/// <summary>
    		/// Updated Date
    		/// </summary>        
    	//    [DisplayName("Updated Date")]
    		public Nullable<System.DateTime>  UpdatedDate { get; set; }
    
    		    
    	}
    //}
    
}
