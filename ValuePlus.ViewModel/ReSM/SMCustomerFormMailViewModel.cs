using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ValuePlus.ViewModel;



namespace ValuePlus.ViewModel.ReSM
{
    
    /// <summary>
    /// SMCustomerFormMail class
    /// </summary>
    //[MetadataType(typeof(SMCustomerFormMailViewModel))]
    //public  partial class SMCustomerFormMail
    //{
    
    	/// <summary>
    	/// SMCustomerFormMail Metadata class
    	/// </summary>
    	public   class SMCustomerFormMailViewModel:ViewModelBase
    	{
    		    
    		/// <summary>
    		/// HQID
    		/// </summary>        
    	//    [DisplayName("HQID")]
            [Required(ErrorMessage = "HQID is required")]
    		public int  HQID { get; set; }
    
    		    
    		/// <summary>
    		/// Customer Form ID
    		/// </summary>        
    	//    [DisplayName("Customer Form ID")]
            [Required(ErrorMessage = "Customer Form ID is required")]
    		public int  CustomerFormID { get; set; }
    
    		    
    		/// <summary>
    		/// User Type
    		/// </summary>        
    	//    [DisplayName("User Type")]
            [Required(ErrorMessage = "User Type is required")]
            [MaxLength(50, ErrorMessage = "User Type cannot be longer than 50 characters")]
    		public string  UserType { get; set; }
    
    		    
    		/// <summary>
    		/// User ID
    		/// </summary>        
    	//    [DisplayName("User ID")]
            [Required(ErrorMessage = "User ID is required")]
            [MaxLength(50, ErrorMessage = "User ID cannot be longer than 50 characters")]
    		public string  UserID { get; set; }
    
    		    
    		/// <summary>
    		/// Status
    		/// </summary>        
    	//    [DisplayName("Status")]
            [MaxLength(50, ErrorMessage = "Status cannot be longer than 50 characters")]
    		public string  Status { get; set; }
    
    		    
    		/// <summary>
    		/// Created By
    		/// </summary>        
    	//    [DisplayName("Created By")]
            [MaxLength(50, ErrorMessage = "Created By cannot be longer than 50 characters")]
    		public string  CreatedBy { get; set; }
    
    		    
    		/// <summary>
    		/// Created Date
    		/// </summary>        
    	//    [DisplayName("Created Date")]
    		public Nullable<System.DateTime>  CreatedDate { get; set; }
    
    		    
    		/// <summary>
    		/// Updated By
    		/// </summary>        
    	//    [DisplayName("Updated By")]
            [MaxLength(50, ErrorMessage = "Updated By cannot be longer than 50 characters")]
    		public string  UpdatedBy { get; set; }
    
    		    
    		/// <summary>
    		/// Updated Date
    		/// </summary>        
    	//    [DisplayName("Updated Date")]
    		public Nullable<System.DateTime>  UpdatedDate { get; set; }
    
    		    
    		/// <summary>
    		/// SID
    		/// </summary>        
    	//    [DisplayName("SID")]
            [MaxLength(200, ErrorMessage = "SID cannot be longer than 200 characters")]
    		public string  SID { get; set; }
    
    		    
    	}
    //}
    
}
