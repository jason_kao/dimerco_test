using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ValuePlus.ViewModel;



namespace ValuePlus.ViewModel.ReSM
{
    
    /// <summary>
    /// SMUserStation class
    /// </summary>
    //[MetadataType(typeof(SMUserStationViewModel))]
    //public  partial class SMUserStation
    //{
    
    	/// <summary>
    	/// SMUserStation Metadata class
    	/// </summary>
    	public   class SMUserStationViewModel:ViewModelBase
    	{
    		    
    		/// <summary>
    		/// HQID
    		/// </summary>        
    	//    [DisplayName("HQID")]
            [Required(ErrorMessage = "HQID is required")]
    		public int  HQID { get; set; }
    
    		    
    		/// <summary>
    		/// User ID
    		/// </summary>        
    	//    [DisplayName("User ID")]
            [Required(ErrorMessage = "User ID is required")]
            [MaxLength(10, ErrorMessage = "User ID cannot be longer than 10 characters")]
    		public string  UserID { get; set; }
    
    		    
    		/// <summary>
    		/// Station ID
    		/// </summary>        
    	//    [DisplayName("Station ID")]
            [Required(ErrorMessage = "Station ID is required")]
            [MaxLength(10, ErrorMessage = "Station ID cannot be longer than 10 characters")]
    		public string  StationID { get; set; }
    
    		    
    		/// <summary>
    		/// Status
    		/// </summary>        
    	//    [DisplayName("Status")]
            [MaxLength(10, ErrorMessage = "Status cannot be longer than 10 characters")]
    		public string  Status { get; set; }
    
    		    
    	}
    //}
    
}
