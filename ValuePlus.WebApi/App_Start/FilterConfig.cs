﻿using ValuePlus.WebApi.App_Infrastructure;

namespace ValuePlus.WebApi.App_Start
{
    public class FilterConfig
    {
        public static void Configure(System.Web.Mvc.GlobalFilterCollection filters)
        {
            filters.Add(new LogActionActionAttribute());
        }
    }
}
