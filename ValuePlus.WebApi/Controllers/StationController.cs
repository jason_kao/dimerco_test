﻿using Newtonsoft.Json;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web.Mvc;
using ValuePlus.Domain.eSM;
using ValuePlus.Service;
using ValuePlus.ViewModel;

namespace ValuePlus.WebApi.Controllers
{
    public class StationController : ControllerBase
    {
        private readonly IStationSearchService _stationSearchService;
        private readonly ICountryService _countryService;
        private readonly ICityService _cityService;
        private readonly IStateService _stateService;
        public StationController(IStationSearchService stationSearchService, 
            ICountryService countryService, ICityService cityService, 
            IStateService stateService)
        {
            _stationSearchService = stationSearchService;
            _countryService = countryService;
            _cityService = cityService;
            _stateService = stateService;
        }

        [HttpGet]
        public JsonResult GetSearchList()
        {
            var ret = _stationSearchService.GetList();
            return Json(ret, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public JsonResult GetData(int hqId)
        {
            var ret = _stationSearchService.GetData(hqId);
            return Json(ret, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public JsonResult GetCountries()
        {
            var ret = _countryService.GetList();
            return Json(ret, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public JsonResult GetStates(int countryId)
        {
            var ret = _stateService.GetList(countryId);
            return Json(ret, JsonRequestBehavior.AllowGet);
        }


        [HttpGet]
        public JsonResult GetCities(int countryId,int stateId)
        {
            var ret = _cityService.GetList(countryId, stateId);
            return Json(ret, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public JsonResult GetCitiesByText(string text)
        {
            var ret = _cityService.GetListByText(text);
            return Json(ret, JsonRequestBehavior.AllowGet);
        }
        

        [HttpPost]
        public JsonResult Save(StationSearchViewModel viewModel)
        {
            if (ModelState.IsValid)
            {
                _stationSearchService.Save(viewModel);
            }
            else
            {
                var errorList = ModelState.ToDictionary(kvp => kvp.Key,kvp => kvp.Value.Errors.Select(e => e.ErrorMessage).ToArray()).Where(m => m.Value.Count() > 0); ;
                foreach (var error in errorList)
                {
                    viewModel.Errors.Add(error.Key, error.Value.First());
                }
            }
            return Json(viewModel, JsonRequestBehavior.AllowGet);
        }
    }
}