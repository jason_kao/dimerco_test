﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ValuePlus.Service;

namespace ValuePlus.WebApi.Controllers
{
    public class LogController : ControllerBase
    {
        private readonly ILogService _logService;
        public LogController(ILogService logService)
        {
            _logService = logService;
        }

        [HttpPost]
        public ActionResult Post(string stationId, Guid pageId, string url, string type, float loadSeconds, DateTime createTime)
        {
            _logService.LogDetail(stationId, pageId, url, type, loadSeconds, createTime);

            return Json(new { isSuccess = true });
        }


        [HttpGet]
        public ActionResult GetPageId()
        {
            var id = Guid.NewGuid();

            return Json(new { id = id }, JsonRequestBehavior.AllowGet);
        }
    }
}