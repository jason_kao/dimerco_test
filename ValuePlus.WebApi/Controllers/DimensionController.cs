﻿using Newtonsoft.Json;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web.Mvc;
using ValuePlus.Domain;
using ValuePlus.Service;
using ValuePlus.Service.eAMS.Utility;
using ValuePlus.ViewModel;

namespace ValuePlus.WebApi.Controllers
{
    public class DimensionController : ControllerBase
    {
        private readonly IDimensionService _dimensionService;
        public DimensionController(IDimensionService dimensionService)
        {
            _dimensionService = dimensionService;
        }

        [HttpGet]
        public JsonResult GetSearchList(string Mode, string SourceID)
        {
            var ret = _dimensionService.GetList("AEH", "82363");
            return Json(ret, JsonRequestBehavior.AllowGet);
        }
        
    }
}