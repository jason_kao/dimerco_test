﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web.Mvc;
using ValuePlus.Domain;
using ValuePlus.Service;
using ValuePlus.ViewModel;

namespace ValuePlus.WebApi.Controllers
{
    public class TestStationController : ControllerBase
    { 
            private readonly IControlBankService _dimensionService;
            public TestStationController(IControlBankService dimensionService)
            {
                _dimensionService = dimensionService;
            }
            [HttpPost]
            public JsonResult GetSeachList(string Number, string txtNumber, string ddlDateFT, DateTime DateFrom, DateTime DateTo)
            {
                var ret = _dimensionService.GetList(Number, txtNumber, ddlDateFT, DateFrom, DateTo);
                return Json(ret, JsonRequestBehavior.AllowGet);
            }
    }
}