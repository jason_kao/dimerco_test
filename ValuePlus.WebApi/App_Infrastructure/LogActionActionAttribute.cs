﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ValuePlus.Service;

namespace ValuePlus.WebApi.App_Infrastructure
{
    public class LogActionActionAttribute: ActionFilterAttribute
    {
        private IBOLogService boLogService;
        private Stopwatch watch;
        private string parameters;

        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            boLogService = new BOLogService();
            if (filterContext.ActionParameters.Count() > 0)
            {
                var list = new List<string>();
                foreach (var param in filterContext.ActionParameters)
                {
                    list.Add(string.Format("{0}={1}", param.Key, param.Value));
                }
                parameters = string.Join(",", list);
            }

            watch = Stopwatch.StartNew();
            base.OnActionExecuting(filterContext);
        }

        public override void OnActionExecuted(ActionExecutedContext filterContext)
        {
            base.OnActionExecuted(filterContext);
            var executionSeconds = Math.Round((decimal)watch.ElapsedMilliseconds / 1000,2);
            var className = filterContext.Controller.GetType().Name;
            var functionName = filterContext.ActionDescriptor.ActionName;

            if (filterContext.Exception == null)
            {
                boLogService.LogAction(className, functionName, parameters, null, executionSeconds);
            }
            else
            {
                var exception = string.Format("Message={0},Source={1},StackTrace={2},InnerException={3}", filterContext.Exception.Message, filterContext.Exception.Source, filterContext.Exception.StackTrace, filterContext.Exception.InnerException);
                boLogService.LogException(className, functionName, parameters, null, executionSeconds, exception);
            }

           
        }
    }
}