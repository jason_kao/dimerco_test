﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Web;
using ValuePlus.Service;

namespace ValuePlus.WebApi.App_Infrastructure
{
    public class MonitoringInterceptor : IInterceptor
    {

        public MonitoringInterceptor()
        {
        }

        public void Intercept(IInvocation invocation)
        {
            var boLogService = new BOLogService();
            var watch = Stopwatch.StartNew();
            
            //Proceed
            invocation.Proceed();


            var executionSeconds = Math.Round((decimal)watch.ElapsedMilliseconds / 1000,2);
            var className = invocation.InvocationTarget.GetType().Name;
            var functionName = invocation.GetConcreteMethod().Name;
            string parameters = null;
            if (invocation.Arguments.Count() > 0)
            {
                var list = new List<string>();
                foreach(var param in invocation.Arguments)
                {
                    list.Add(param.ToString());
                }
                parameters = string.Join(",", list);
            }

            boLogService.LogService(className, functionName, parameters, null, executionSeconds);
        }
    }
}