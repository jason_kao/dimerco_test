namespace ValuePlus.Domain.eSM
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("SMGroupRoleSetting")]
    public partial class SMGroupRoleSetting
    {
        public int ID { get; set; }

        [StringLength(10)]
        public string RoleNo { get; set; }

        [StringLength(10)]
        public string RoleType { get; set; }

        public int? RoleID { get; set; }

        [StringLength(6)]
        public string UserID { get; set; }

        [StringLength(50)]
        public string ApplyMode { get; set; }

        [StringLength(10)]
        public string Status { get; set; }

        [StringLength(6)]
        public string CreatedBy { get; set; }

        public DateTime? CreatedDate { get; set; }

        [StringLength(6)]
        public string UpdatedBy { get; set; }

        public DateTime? UPdatedDate { get; set; }
    }
}
