namespace ValuePlus.Domain.eSM
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("SMISFData")]
    public partial class SMISFData
    {
        [Key]
        public int HQID { get; set; }

        public int CustomerID { get; set; }

        [Required]
        [StringLength(20)]
        public string ImporterType { get; set; }

        [Required]
        [StringLength(10)]
        public string ImporterIDType { get; set; }

        [StringLength(15)]
        public string ImporterID { get; set; }

        [StringLength(50)]
        public string ImporterBondNo { get; set; }

        [StringLength(50)]
        public string ISFBondActivity { get; set; }

        [StringLength(50)]
        public string ISFBondType { get; set; }

        [StringLength(50)]
        public string SuretyID { get; set; }

        [StringLength(50)]
        public string BondReferenceNo { get; set; }

        [StringLength(50)]
        public string ImporterBondHolderID { get; set; }

        public DateTime? POASignedDate { get; set; }

        [StringLength(100)]
        public string POASingedSheet { get; set; }

        [StringLength(50)]
        public string Status { get; set; }

        [StringLength(10)]
        public string CreatedBy { get; set; }

        public DateTime? CreatedDate { get; set; }

        [StringLength(10)]
        public string UpdatedBy { get; set; }

        public DateTime? UpdatedDate { get; set; }

        [StringLength(200)]
        public string ImporterName { get; set; }
    }
}
