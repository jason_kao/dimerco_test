namespace ValuePlus.Domain.eSM
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("SMProductLine")]
    public partial class SMProductLine
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public SMProductLine()
        {
            SMChargeCodePL = new HashSet<SMChargeCodePL>();
        }

        [Key]
        public int HQID { get; set; }

        [Required]
        [StringLength(5)]
        public string ProductLineCode { get; set; }

        [StringLength(255)]
        public string ProductLineName { get; set; }

        [StringLength(1)]
        public string ExporImp { get; set; }

        [Required]
        [StringLength(10)]
        public string Status { get; set; }

        [StringLength(6)]
        public string CreatedBy { get; set; }

        public DateTime CreatedDate { get; set; }

        [StringLength(6)]
        public string UpdatedBy { get; set; }

        public DateTime? UpdatedDate { get; set; }

        [Column(TypeName = "timestamp")]
        [MaxLength(8)]
        [Timestamp]
        public byte[] Version { get; set; }

        public bool? IsGateWay { get; set; }

        [StringLength(50)]
        public string ChargeCodeUOMType { get; set; }

        [StringLength(50)]
        public string ChargeCodeUOMType2 { get; set; }

        [StringLength(10)]
        public string ProductLineShowCode { get; set; }

        public bool? IsCustomerFromSAM { get; set; }

        public bool IsUsedInPL { get; set; }

        public bool? IsUsedInAgentControl { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<SMChargeCodePL> SMChargeCodePL { get; set; }
    }
}
