namespace ValuePlus.Domain.eSM
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("SMeAWBAddressSetting")]
    public partial class SMeAWBAddressSetting
    {
        [Key]
        public int HQID { get; set; }

        [Required]
        [StringLength(10)]
        public string FromPlaceType { get; set; }

        [Required]
        [StringLength(10)]
        public string ToPlaceType { get; set; }

        [Required]
        [StringLength(6)]
        public string FromStation { get; set; }

        [Required]
        [StringLength(6)]
        public string ToStation { get; set; }

        [Required]
        [StringLength(500)]
        public string RelateAirLine { get; set; }

        [Required]
        [StringLength(10)]
        public string Status { get; set; }

        [StringLength(6)]
        public string CreatedBy { get; set; }

        public DateTime CreatedDate { get; set; }

        [StringLength(6)]
        public string UpdatedBy { get; set; }

        public DateTime? UpdatedDate { get; set; }
    }
}
