namespace ValuePlus.Domain.eSM
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("SMANMailReceivedList")]
    public partial class SMANMailReceivedList
    {
        [Key]
        public int HQID { get; set; }

        [Required]
        [StringLength(4)]
        public string ANFunctionCode { get; set; }

        [Required]
        [StringLength(1)]
        public string PartyType { get; set; }

        public int PartyID { get; set; }

        [Required]
        [StringLength(1)]
        public string MailType { get; set; }

        [Required]
        [StringLength(10)]
        public string UserID { get; set; }

        [Required]
        [StringLength(10)]
        public string Status { get; set; }

        [Required]
        [StringLength(6)]
        public string CreatedBy { get; set; }

        public DateTime CreatedDate { get; set; }

        [StringLength(6)]
        public string UpdatedBy { get; set; }

        public DateTime? UpdatedDate { get; set; }
    }
}
