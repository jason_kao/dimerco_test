namespace ValuePlus.Domain.eSM
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("SMControlGroupDetail")]
    public partial class SMControlGroupDetail
    {
        [Key]
        public int HQID { get; set; }

        public int? ControlGroupID { get; set; }

        public int? FieldID { get; set; }

        public bool? IsMandatory { get; set; }

        [StringLength(50)]
        public string MapName { get; set; }

        [StringLength(100)]
        public string PickUpData { get; set; }

        [StringLength(10)]
        public string CreatedBy { get; set; }

        public DateTime? CreatedDate { get; set; }

        [StringLength(10)]
        public string UpdatedBy { get; set; }

        public DateTime? UpdatedDate { get; set; }

        [StringLength(100)]
        public string IssueStation { get; set; }

        [StringLength(100)]
        public string BillTo { get; set; }

        [StringLength(100)]
        public string ChargeCode { get; set; }

        public bool? isIncludeVAT { get; set; }

        [StringLength(250)]
        public string Description { get; set; }

        public bool? ShowOnSearch { get; set; }

        public int? SearchResultOrder { get; set; }
    }
}
