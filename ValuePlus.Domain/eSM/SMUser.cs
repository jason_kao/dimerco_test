namespace ValuePlus.Domain.eSM
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("SMUser")]
    public partial class SMUser
    {
        [Key]
        public int HQID { get; set; }

        [StringLength(6)]
        public string UserID { get; set; }

        [StringLength(50)]
        public string FirstName { get; set; }

        [StringLength(50)]
        public string LastName { get; set; }

        [StringLength(100)]
        public string FullName { get; set; }

        [StringLength(50)]
        public string DepartmentID { get; set; }

        [StringLength(3)]
        public string StationID { get; set; }

        public DateTime? HireDate { get; set; }

        public DateTime? Birthday { get; set; }

        [StringLength(50)]
        public string Password { get; set; }

        public DateTime? PasswordLastChanged { get; set; }

        [StringLength(50)]
        public string Email { get; set; }

        public int? ReportTo { get; set; }

        [StringLength(50)]
        public string Title { get; set; }

        [Required]
        [StringLength(10)]
        public string Status { get; set; }

        [StringLength(6)]
        public string CreatedBy { get; set; }

        public DateTime CreatedDate { get; set; }

        [StringLength(6)]
        public string UpdatedBy { get; set; }

        public DateTime? UpdatedDate { get; set; }

        [Column(TypeName = "timestamp")]
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        [MaxLength(8)]
        public byte[] Version { get; set; }

        [StringLength(60)]
        public string Tel { get; set; }

        [StringLength(60)]
        public string Fax { get; set; }

        [StringLength(60)]
        public string MobilePhone { get; set; }

        [StringLength(60)]
        public string Skype { get; set; }

        public DateTime? LastLoginDT { get; set; }

        [StringLength(20)]
        public string LastLoginIP { get; set; }

        [StringLength(10)]
        public string Admin { get; set; }

        public int? CRPDepartmentID { get; set; }

        public int? CRPDivisionID { get; set; }

        [StringLength(50)]
        public string AliasLoginID { get; set; }

        [StringLength(3)]
        public string CostCenter1 { get; set; }

        [StringLength(3)]
        public string CostCenter2 { get; set; }

        [StringLength(3)]
        public string CostCenter3 { get; set; }

        [StringLength(20)]
        public string IATANo { get; set; }
    }
}
