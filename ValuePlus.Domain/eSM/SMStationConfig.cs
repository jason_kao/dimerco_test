namespace ValuePlus.Domain.eSM
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("SMStationConfig")]
    public partial class SMStationConfig
    {
        [Key]
        [StringLength(3)]
        public string StationID { get; set; }

        [StringLength(50)]
        public string InboxName { get; set; }

        [Required]
        [StringLength(50)]
        public string eMailAcct { get; set; }

        [StringLength(6)]
        public string StationCode { get; set; }
    }
}
