namespace ValuePlus.Domain.eSM
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("SMChartofAccount")]
    public partial class SMChartofAccount
    {
        [Key]
        public int HQID { get; set; }

        [Required]
        [StringLength(10)]
        public string GLCode { get; set; }

        [StringLength(255)]
        public string GLName { get; set; }

        [StringLength(255)]
        public string Description { get; set; }

        [StringLength(255)]
        public string Notes { get; set; }

        [StringLength(4)]
        public string AccountType { get; set; }

        [StringLength(4)]
        public string Currency { get; set; }

        [StringLength(1)]
        public string Level { get; set; }

        [StringLength(3)]
        public string ChargeCode { get; set; }

        [Required]
        [StringLength(10)]
        public string Status { get; set; }

        [StringLength(6)]
        public string CreatedBy { get; set; }

        public DateTime CreatedDate { get; set; }

        [StringLength(6)]
        public string UpdatedBy { get; set; }

        public DateTime? UpdatedDate { get; set; }

        [Column(TypeName = "timestamp")]
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        [MaxLength(8)]
        public byte[] Version { get; set; }

        [StringLength(1)]
        public string Dr { get; set; }
    }
}
