namespace ValuePlus.Domain.eSM
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("SMChargeCodeGLCode")]
    public partial class SMChargeCodeGLCode
    {
        [Key]
        public int HQID { get; set; }

        public int? ChargeCodeID { get; set; }

        [StringLength(2)]
        public string ModeCode { get; set; }

        [StringLength(10)]
        public string SGLCode { get; set; }

        [StringLength(10)]
        public string CGLCode { get; set; }

        [StringLength(10)]
        public string CreatedBy { get; set; }

        public DateTime? CreatedDate { get; set; }

        [StringLength(10)]
        public string UpdatedBy { get; set; }

        public DateTime? UpdatedDate { get; set; }

        [Column(TypeName = "timestamp")]
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        [MaxLength(8)]
        public byte[] Version { get; set; }

        public virtual SMChargeCode SMChargeCode { get; set; }
    }
}
