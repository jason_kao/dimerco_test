namespace ValuePlus.Domain.eSM
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("SMCustomerPreAlertTemplMap")]
    public partial class SMCustomerPreAlertTemplMap
    {
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int ID { get; set; }

        public int ProductLineID { get; set; }

        [StringLength(20)]
        public string AWBType { get; set; }

        public int? FormType { get; set; }

        [StringLength(100)]
        public string AliasCode { get; set; }

        [StringLength(255)]
        public string MapName { get; set; }

        [StringLength(1)]
        public string ShowHeader { get; set; }

        [StringLength(1)]
        public string ShowContent { get; set; }

        [Required]
        [StringLength(50)]
        public string Status { get; set; }

        [StringLength(6)]
        public string CreatedBy { get; set; }

        public DateTime CreatedDate { get; set; }

        [StringLength(6)]
        public string UpdatedBy { get; set; }

        public DateTime? UpdatedDate { get; set; }
    }
}
