namespace ValuePlus.Domain.eSM
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("SMControlGroup")]
    public partial class SMControlGroup
    {
        [Key]
        public int HQID { get; set; }

        [StringLength(10)]
        public string FieldType { get; set; }

        public int? CSPProjectID { get; set; }

        public int? CustomerID { get; set; }

        public int? ProdcutLine { get; set; }

        [StringLength(10)]
        public string Status { get; set; }

        [StringLength(10)]
        public string CreatedBy { get; set; }

        public DateTime? CreatedDate { get; set; }

        [StringLength(10)]
        public string UpdatedBy { get; set; }

        public DateTime? UpdatedDate { get; set; }

        public bool? PartialAsFulSend { get; set; }

        public bool? NeedReconfirm { get; set; }

        [StringLength(50)]
        public string ReconfirmBy { get; set; }

        public bool? ShipmentComeFromPO { get; set; }
    }
}
