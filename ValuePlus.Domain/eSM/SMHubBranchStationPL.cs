namespace ValuePlus.Domain.eSM
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("SMHubBranchStationPL")]
    public partial class SMHubBranchStationPL
    {
        [Key]
        public int HQID { get; set; }

        [Required]
        [StringLength(3)]
        public string StationID { get; set; }

        [Required]
        [StringLength(3)]
        public string BranchID { get; set; }

        public int ProductLineID { get; set; }

        [StringLength(6)]
        public string CreatedBy { get; set; }

        public DateTime CreatedDate { get; set; }

        [StringLength(6)]
        public string UpdatedBy { get; set; }

        public DateTime? UpdatedDate { get; set; }

        [Column(TypeName = "timestamp")]
        [MaxLength(8)]
        [Timestamp]
        public byte[] Version { get; set; }

        [StringLength(50)]
        public string Status { get; set; }
    }
}
