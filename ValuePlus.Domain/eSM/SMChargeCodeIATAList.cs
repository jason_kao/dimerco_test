namespace ValuePlus.Domain.eSM
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("SMChargeCodeIATAList")]
    public partial class SMChargeCodeIATAList
    {
        [Key]
        public int HQID { get; set; }

        [StringLength(5)]
        public string IATAChargeCode { get; set; }

        [StringLength(500)]
        public string IATAChargeCodeDesc { get; set; }

        [StringLength(50)]
        public string Ref { get; set; }
    }
}
