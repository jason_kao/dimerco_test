namespace ValuePlus.Domain.eSM
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("SMCountry")]
    public partial class SMCountry
    {
        [Key]
        public int HQID { get; set; }

        [Required]
        [StringLength(5)]
        public string CountryCode { get; set; }

        [StringLength(255)]
        public string CountryName { get; set; }

        public int? AreaID { get; set; }

        public int? GRegionID { get; set; }

        [StringLength(10)]
        public string PrefixNumber { get; set; }

        [Required]
        [StringLength(10)]
        public string Status { get; set; }

        [StringLength(6)]
        public string CreatedBy { get; set; }

        public DateTime CreatedDate { get; set; }

        [StringLength(6)]
        public string UpdatedBy { get; set; }

        public DateTime? UpdatedDate { get; set; }

        [Column(TypeName = "timestamp")]
        [MaxLength(8)]
        [Timestamp]
        public byte[] Version { get; set; }

        [StringLength(3)]
        public string Currency { get; set; }

        public int? LengthUOMID { get; set; }

        public int? WeightUOMID { get; set; }

        public int? VolumeUOMID { get; set; }

        [StringLength(20)]
        public string ShowState { get; set; }

        public bool? MandatoryState { get; set; }

        [StringLength(20)]
        public string ShowZip { get; set; }

        public bool? MandatoryZip { get; set; }

        [Required]
        [StringLength(50)]
        public string VATName { get; set; }

        [StringLength(255)]
        public string LocalCountyName { get; set; }

        public bool? MandatoryAMSZip { get; set; }
    }
}
