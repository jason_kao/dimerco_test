namespace ValuePlus.Domain.eSM
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("SMSeaPort")]
    public partial class SMSeaPort
    {
        [Key]
        public int HQID { get; set; }

        [Required]
        [StringLength(5)]
        public string SeaPortCode { get; set; }

        [Required]
        [StringLength(255)]
        public string SeaPortName { get; set; }

        public int? CityID { get; set; }

        [Required]
        [StringLength(10)]
        public string Status { get; set; }

        [StringLength(6)]
        public string CreatedBy { get; set; }

        public DateTime CreatedDate { get; set; }

        [StringLength(6)]
        public string UpdatedBy { get; set; }

        public DateTime? UpdatedDate { get; set; }

        [Column(TypeName = "timestamp")]
        [MaxLength(8)]
        [Timestamp]
        public byte[] Version { get; set; }

        [StringLength(10)]
        public string AMSSeaPortCode { get; set; }

        [StringLength(50)]
        public string ISF5Port { get; set; }

        [StringLength(20)]
        public string UNLocationCode { get; set; }
    }
}
