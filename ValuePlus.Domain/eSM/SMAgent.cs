namespace ValuePlus.Domain.eSM
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("SMAgent")]
    public partial class SMAgent
    {
        [Key]
        public int HQID { get; set; }

        [Required]
        [StringLength(3)]
        public string StationID { get; set; }

        [Required]
        [StringLength(10)]
        public string AgentCode { get; set; }

        public int? AgentProfileID { get; set; }

        public int? CustomerID { get; set; }

        public int? CityID { get; set; }

        [Required]
        [StringLength(255)]
        public string AgentName { get; set; }

        [StringLength(255)]
        public string AgentName1 { get; set; }

        [StringLength(255)]
        public string AgentAddress1 { get; set; }

        [StringLength(255)]
        public string AgentAddress2 { get; set; }

        [StringLength(255)]
        public string AgentAddress3 { get; set; }

        [StringLength(255)]
        public string AgentAddress4 { get; set; }

        [StringLength(255)]
        public string AgentAddress5 { get; set; }

        [StringLength(50)]
        public string AgentPhone { get; set; }

        [StringLength(10)]
        public string AgentPhoneExt { get; set; }

        [StringLength(50)]
        public string AgentFax { get; set; }

        [StringLength(10)]
        public string AgentFaxExt { get; set; }

        [StringLength(10)]
        public string AgentZip { get; set; }

        [StringLength(255)]
        public string AgentWebSite { get; set; }

        public int? AgentID { get; set; }

        [Required]
        [StringLength(10)]
        public string Status { get; set; }

        [StringLength(6)]
        public string CreatedBy { get; set; }

        public DateTime CreatedDate { get; set; }

        [StringLength(6)]
        public string UpdatedBy { get; set; }

        public DateTime? UpdatedDate { get; set; }

        [Column(TypeName = "timestamp")]
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        [MaxLength(8)]
        public byte[] Version { get; set; }

        [StringLength(50)]
        public string Longitute { get; set; }

        [StringLength(50)]
        public string Latitude { get; set; }

        [StringLength(100)]
        public string ProductLine { get; set; }

        [StringLength(200)]
        public string Branch { get; set; }

        [StringLength(50)]
        public string Continent { get; set; }

        [StringLength(10)]
        public string extShow { get; set; }

        [StringLength(500)]
        public string eMail { get; set; }
    }
}
