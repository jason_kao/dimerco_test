namespace ValuePlus.Domain.eSM
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("SMVATCountry")]
    public partial class SMVATCountry
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int HQID { get; set; }

        public int CountryID { get; set; }

        [Required]
        [StringLength(2)]
        public string VATCode { get; set; }

        [StringLength(255)]
        public string VATName { get; set; }

        public double? VATRate { get; set; }

        [Required]
        [StringLength(10)]
        public string Status { get; set; }

        [StringLength(6)]
        public string CreatedBy { get; set; }

        public DateTime CreatedDate { get; set; }

        [StringLength(6)]
        public string UpdatedBy { get; set; }

        public DateTime? UpdatedDate { get; set; }

        [Column(TypeName = "timestamp")]
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        [MaxLength(8)]
        public byte[] Version { get; set; }
    }
}
