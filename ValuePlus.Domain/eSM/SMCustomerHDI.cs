namespace ValuePlus.Domain.eSM
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("SMCustomerHDI")]
    public partial class SMCustomerHDI
    {
        [Key]
        public int HQID { get; set; }

        public int? CustomerID { get; set; }

        public int? HDIID { get; set; }

        [StringLength(2000)]
        public string HDIDescription { get; set; }

        [StringLength(50)]
        public string Mode { get; set; }

        [StringLength(500)]
        public string Forms { get; set; }

        [Required]
        [StringLength(10)]
        public string Status { get; set; }

        [StringLength(50)]
        public string CreatedBy { get; set; }

        public DateTime? CreatedDate { get; set; }

        [StringLength(50)]
        public string UpdatedBy { get; set; }

        public DateTime? UpdatedDate { get; set; }

        [Column(TypeName = "timestamp")]
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        [MaxLength(8)]
        public byte[] Version { get; set; }

        public int? OriginHDIID { get; set; }

        [StringLength(50)]
        public string HDIType { get; set; }
    }
}
