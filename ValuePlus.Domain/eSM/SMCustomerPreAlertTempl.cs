namespace ValuePlus.Domain.eSM
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("SMCustomerPreAlertTempl")]
    public partial class SMCustomerPreAlertTempl
    {
        public int ID { get; set; }

        public int CustomerID { get; set; }

        public int ProductLineID { get; set; }

        [StringLength(20)]
        public string AWBType { get; set; }

        public int? FormType { get; set; }

        [StringLength(255)]
        public string Subject { get; set; }

        [StringLength(1)]
        public string NeedMaster { get; set; }

        [StringLength(1)]
        public string NeedHouse { get; set; }

        [StringLength(1)]
        public string NeedDebitNote { get; set; }

        [StringLength(1)]
        public string NeedArrivalNotice { get; set; }

        public string MailContent { get; set; }

        [Required]
        [StringLength(50)]
        public string Status { get; set; }

        [StringLength(6)]
        public string CreatedBy { get; set; }

        public DateTime CreatedDate { get; set; }

        [StringLength(6)]
        public string UpdatedBy { get; set; }

        public DateTime? UpdatedDate { get; set; }

        [Column(TypeName = "timestamp")]
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        [MaxLength(8)]
        public byte[] Version { get; set; }

        [StringLength(1)]
        public string NeedCommInv { get; set; }

        [StringLength(1)]
        public string NeedPackingList { get; set; }

        [StringLength(1)]
        public string SendCustomer { get; set; }

        [StringLength(1)]
        public string SendShipper { get; set; }

        [StringLength(1)]
        public string SendCNEE { get; set; }

        [StringLength(1)]
        public string SendNotify { get; set; }

        [StringLength(1)]
        public string SendBBAgent { get; set; }

        [StringLength(1)]
        public string SendThirtyParty { get; set; }

        [StringLength(1)]
        public string SendFinalImport { get; set; }

        [StringLength(1)]
        public string SendDestAgent { get; set; }
    }
}
