namespace ValuePlus.Domain.eSM
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("SMCustomerPL")]
    public partial class SMCustomerPL
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int HQID { get; set; }

        [Key]
        [Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int CustomerID { get; set; }

        [Key]
        [Column(Order = 1)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int ProductLineID { get; set; }

        public bool? IsStation { get; set; }

        public bool? IsForeignAgent { get; set; }

        public bool? IsLocalAgent { get; set; }

        public bool? IsAirLine { get; set; }

        public bool? IsOceanCarrier { get; set; }

        public bool? IsCustomer { get; set; }

        public bool? IsShipper { get; set; }

        public bool? IsCNEE { get; set; }

        public bool? Is3rdParty { get; set; }

        public bool? IsNotify { get; set; }

        public bool? IsBroker { get; set; }

        public int? DASCarrierCountryID { get; set; }

        [Required]
        [StringLength(50)]
        public string Status { get; set; }

        [StringLength(6)]
        public string CreatedBy { get; set; }

        public DateTime CreatedDate { get; set; }

        [StringLength(6)]
        public string UpdatedBy { get; set; }

        public DateTime? UpdatedDate { get; set; }

        [Column(TypeName = "timestamp")]
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        [MaxLength(8)]
        public byte[] Version { get; set; }

        public bool? IsForward { get; set; }

        public bool? IsTrucker { get; set; }

        public bool? IsVendor { get; set; }

        public bool? IsBillTo { get; set; }

        public int? SalesPersonID { get; set; }

        public int? CustomerServiceID { get; set; }

        public bool? IsWarehouse { get; set; }

        [Required]
        [StringLength(1)]
        public string Language { get; set; }

        [StringLength(500)]
        public string ContactPerson { get; set; }

        public bool? IsMNC { get; set; }

        [StringLength(1)]
        public string NeedBookConfirm { get; set; }

        [StringLength(1)]
        public string POA { get; set; }

        public bool? IsCoLoad { get; set; }

        public bool? IsBrrowin { get; set; }

        public bool? AllowCoLoadOut { get; set; }

        public bool? IsBorrowin { get; set; }

        public virtual SMCustomer SMCustomer { get; set; }
    }
}
