namespace ValuePlus.Domain.eSM
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("SMDIMDistrict")]
    public partial class SMDIMDistrict
    {
        [Key]
        public int HQID { get; set; }

        [Required]
        [StringLength(10)]
        public string DimDistrictCode { get; set; }

        public int DimRegionID { get; set; }

        [StringLength(255)]
        public string DimDistrictName { get; set; }

        [Required]
        [StringLength(10)]
        public string Status { get; set; }

        [StringLength(6)]
        public string CreatedBy { get; set; }

        public DateTime CreatedDate { get; set; }

        [StringLength(6)]
        public string UpdatedBy { get; set; }

        public DateTime? UpdatedDate { get; set; }

        [Column(TypeName = "timestamp")]
        [MaxLength(8)]
        [Timestamp]
        public byte[] Version { get; set; }

        public virtual SMDIMRegion SMDIMRegion { get; set; }
    }
}
