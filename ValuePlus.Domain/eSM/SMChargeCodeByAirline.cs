namespace ValuePlus.Domain.eSM
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("SMChargeCodeByAirline")]
    public partial class SMChargeCodeByAirline
    {
        public int ID { get; set; }

        [StringLength(3)]
        public string StationID { get; set; }

        public int? AirlineHQID { get; set; }

        public int? SubChargeID { get; set; }

        public int? ChargeID { get; set; }

        public int? EDIChargeID { get; set; }

        [StringLength(10)]
        public string Createdby { get; set; }

        public DateTime? CreatedDate { get; set; }

        [StringLength(10)]
        public string Updatedby { get; set; }

        public DateTime? UpdatedDate { get; set; }
    }
}
