namespace ValuePlus.Domain.eSM
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("SMEDIChargeRuleCode")]
    public partial class SMEDIChargeRuleCode
    {
        [Key]
        public int HQID { get; set; }

        public int? RuleID { get; set; }

        [StringLength(10)]
        public string ChargeCode { get; set; }

        [StringLength(100)]
        public string ChargeMeaning { get; set; }

        public int? DIMChargeID { get; set; }

        [StringLength(10)]
        public string DIMChargeCode { get; set; }

        [StringLength(10)]
        public string Status { get; set; }

        [StringLength(10)]
        public string CreatedBy { get; set; }

        public DateTime? CreatedDate { get; set; }

        [StringLength(10)]
        public string UpdatedBy { get; set; }

        public DateTime? UpdatedDate { get; set; }
    }
}
