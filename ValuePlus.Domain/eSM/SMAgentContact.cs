namespace ValuePlus.Domain.eSM
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("SMAgentContact")]
    public partial class SMAgentContact
    {
        [Key]
        public int HQID { get; set; }

        public int? Agentid { get; set; }

        [StringLength(50)]
        public string Password { get; set; }

        [StringLength(50)]
        public string Title { get; set; }

        [StringLength(200)]
        public string ContactPerson { get; set; }

        [StringLength(200)]
        public string JobTitle { get; set; }

        [StringLength(50)]
        public string Depart { get; set; }

        [StringLength(200)]
        public string TelPhone { get; set; }

        [StringLength(200)]
        public string eMail { get; set; }

        [StringLength(200)]
        public string Mobile { get; set; }

        [StringLength(10)]
        public string ExtShow { get; set; }

        public int? Seq { get; set; }

        [StringLength(1)]
        public string Admin { get; set; }

        [Required]
        [StringLength(10)]
        public string Status { get; set; }

        [StringLength(10)]
        public string ExtPhone { get; set; }

        [StringLength(100)]
        public string FaxNo { get; set; }

        [StringLength(50)]
        public string CreatedBy { get; set; }

        public DateTime? CreatedDate { get; set; }

        [StringLength(50)]
        public string UpdatedBy { get; set; }

        public DateTime? UpdatedDate { get; set; }
    }
}
