namespace ValuePlus.Domain.eSM
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("SMAirPort")]
    public partial class SMAirPort
    {
        [Key]
        public int HQID { get; set; }

        [Required]
        [StringLength(5)]
        public string AirPortCode { get; set; }

        [StringLength(255)]
        public string AirPortName { get; set; }

        public int? CityID { get; set; }

        [Required]
        [StringLength(10)]
        public string Status { get; set; }

        [StringLength(6)]
        public string CreatedBy { get; set; }

        public DateTime CreatedDate { get; set; }

        [StringLength(6)]
        public string UpdatedBy { get; set; }

        public DateTime? UpdatedDate { get; set; }

        [Column(TypeName = "timestamp")]
        [MaxLength(8)]
        [Timestamp]
        public byte[] Version { get; set; }

        public bool? IsInternational { get; set; }

        public bool? IsDomestic { get; set; }
    }
}
