namespace ValuePlus.Domain.eSM
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("SMCurrency")]
    public partial class SMCurrency
    {
        [Key]
        public int HQID { get; set; }

        [Required]
        [StringLength(5)]
        public string CurrencyCode { get; set; }

        [Required]
        [StringLength(255)]
        public string CurrencyName { get; set; }

        public decimal? Decimal { get; set; }

        public decimal? RoundUnit { get; set; }

        public decimal? MinAmount { get; set; }

        public decimal? RoundUp { get; set; }

        [StringLength(50)]
        public string MinRoundUnit { get; set; }

        [StringLength(50)]
        public string AMTDecimal { get; set; }

        [StringLength(50)]
        public string VATDecimal { get; set; }

        [StringLength(50)]
        public string CurrencyFlag { get; set; }

        [Required]
        [StringLength(10)]
        public string Status { get; set; }

        [StringLength(6)]
        public string CreatedBy { get; set; }

        public DateTime CreatedDate { get; set; }

        [StringLength(6)]
        public string UpdatedBy { get; set; }

        public DateTime? UpdatedDate { get; set; }

        [Column(TypeName = "timestamp")]
        [MaxLength(8)]
        [Timestamp]
        public byte[] Version { get; set; }

        public int? GlobalSeq { get; set; }
    }
}
