namespace ValuePlus.Domain.eSM
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("SMANDimension")]
    public partial class SMANDimension
    {
        [Key]
        public int HQID { get; set; }

        [Required]
        [StringLength(3)]
        public string ANDIMCode { get; set; }

        [Required]
        [StringLength(30)]
        public string ANDIMDescription { get; set; }

        [Required]
        [StringLength(6)]
        public string CreatedBy { get; set; }

        public DateTime CreatedDate { get; set; }

        [StringLength(6)]
        public string UpdatedBy { get; set; }

        public DateTime? UpdatedDate { get; set; }
    }
}
