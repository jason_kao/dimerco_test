namespace ValuePlus.Domain.eSM
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("SMGroupMilestone")]
    public partial class SMGroupMilestone
    {
        [Key]
        [Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int GroupID { get; set; }

        [Key]
        [Column(Order = 1)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int MilestoneID { get; set; }

        [StringLength(100)]
        public string MilestoneName { get; set; }

        [StringLength(1)]
        public string InputType { get; set; }

        [StringLength(100)]
        public string Description { get; set; }

        public int? SeqNum { get; set; }

        [StringLength(1)]
        public string Usage { get; set; }
    }
}
