namespace ValuePlus.Domain.eSM
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("SMANActionRule")]
    public partial class SMANActionRule
    {
        [Key]
        public int HQID { get; set; }

        [Required]
        [StringLength(4)]
        public string ANFunctionCode { get; set; }

        [Required]
        [StringLength(3)]
        public string Type1Level { get; set; }

        [Required]
        [StringLength(3)]
        public string Type2Level { get; set; }

        [StringLength(3)]
        public string Type3Level { get; set; }

        [Required]
        [StringLength(2)]
        public string ANActionCode { get; set; }

        [Required]
        [StringLength(6)]
        public string CreatedBy { get; set; }

        public DateTime CreatedDate { get; set; }

        [StringLength(6)]
        public string UpdatedBy { get; set; }

        public DateTime? UpdatedDate { get; set; }

        [StringLength(50)]
        public string MixedLabel { get; set; }
    }
}
