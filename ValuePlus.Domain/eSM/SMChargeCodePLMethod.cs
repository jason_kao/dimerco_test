namespace ValuePlus.Domain.eSM
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("SMChargeCodePLMethod")]
    public partial class SMChargeCodePLMethod
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int HQID { get; set; }

        [Key]
        [Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int ChargeCodeID { get; set; }

        [Key]
        [Column(Order = 1)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int ProductLineID { get; set; }

        [Key]
        [Column(Order = 2)]
        [StringLength(50)]
        public string ChargeMethod { get; set; }

        [StringLength(500)]
        public string ChargeUOM { get; set; }

        public int? UOMPara { get; set; }

        public bool? MultiEntry { get; set; }

        [StringLength(500)]
        public string Calculation { get; set; }

        [StringLength(50)]
        public string CreatedBy { get; set; }

        public DateTime? CreatedDate { get; set; }

        [StringLength(50)]
        public string UpdatedBy { get; set; }

        public DateTime? UpdatedDate { get; set; }

        [StringLength(10)]
        public string Status { get; set; }

        [StringLength(500)]
        public string Remark { get; set; }
    }
}
