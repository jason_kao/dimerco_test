namespace ValuePlus.Domain.eSM
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("SMCustomerAddress")]
    public partial class SMCustomerAddress
    {
        [Key]
        public int HQID { get; set; }

        public int CustomerID { get; set; }

        [StringLength(500)]
        public string FormType { get; set; }

        [StringLength(500)]
        public string Mode { get; set; }

        [StringLength(255)]
        public string CustomerName { get; set; }

        [StringLength(255)]
        public string CustomerAddress1 { get; set; }

        [StringLength(255)]
        public string CustomerAddress2 { get; set; }

        [StringLength(255)]
        public string CustomerAddress3 { get; set; }

        [StringLength(255)]
        public string CustomerAddress4 { get; set; }

        [StringLength(255)]
        public string CustomerAddress5 { get; set; }

        [StringLength(50)]
        public string CustomerPhone { get; set; }

        [StringLength(50)]
        public string CustomerFax { get; set; }

        [StringLength(50)]
        public string CustomerEmail { get; set; }

        [Required]
        [StringLength(50)]
        public string Status { get; set; }

        [StringLength(6)]
        public string CreatedBy { get; set; }

        public DateTime CreatedDate { get; set; }

        [StringLength(6)]
        public string UpdatedBy { get; set; }

        public DateTime? UpdatedDate { get; set; }

        [StringLength(50)]
        public string ZIP { get; set; }

        public int? CountryID { get; set; }

        public int? StateID { get; set; }

        public int? CityID { get; set; }

        [StringLength(50)]
        public string CityName { get; set; }

        [StringLength(50)]
        public string AreaCode { get; set; }
    }
}
