namespace ValuePlus.Domain.eSM
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("SMAirLine")]
    public partial class SMAirLine
    {
        [Key]
        public int HQID { get; set; }

        [Required]
        [StringLength(3)]
        public string StationID { get; set; }

        [Required]
        [StringLength(10)]
        public string AirLineCode { get; set; }

        public int? CustomerID { get; set; }

        public int? CityID { get; set; }

        [Required]
        [StringLength(255)]
        public string AirLineName { get; set; }

        [StringLength(255)]
        public string AirLineName1 { get; set; }

        [StringLength(255)]
        public string AirLineAddress1 { get; set; }

        [StringLength(255)]
        public string AirLineAddress2 { get; set; }

        [StringLength(255)]
        public string AirLineAddress3 { get; set; }

        [StringLength(255)]
        public string AirLineAddress4 { get; set; }

        [StringLength(255)]
        public string AirLineAddress5 { get; set; }

        [StringLength(50)]
        public string AirLinePhone { get; set; }

        [StringLength(10)]
        public string AirLinePhoneExt { get; set; }

        [StringLength(50)]
        public string AirLineFax { get; set; }

        [StringLength(10)]
        public string AirLineFaxExt { get; set; }

        [StringLength(10)]
        public string AirLineZip { get; set; }

        [StringLength(255)]
        public string AirLineWebSite { get; set; }

        [Required]
        [StringLength(10)]
        public string Status { get; set; }

        [StringLength(6)]
        public string CreatedBy { get; set; }

        public DateTime CreatedDate { get; set; }

        [StringLength(6)]
        public string UpdatedBy { get; set; }

        public DateTime? UpdatedDate { get; set; }

        [Column(TypeName = "timestamp")]
        [MaxLength(8)]
        [Timestamp]
        public byte[] Version { get; set; }

        public bool? IsInternational { get; set; }

        public bool? IsDomestic { get; set; }
    }
}
