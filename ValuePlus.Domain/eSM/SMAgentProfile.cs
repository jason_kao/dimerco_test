namespace ValuePlus.Domain.eSM
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("SMAgentProfile")]
    public partial class SMAgentProfile
    {
        [Key]
        public int HQID { get; set; }

        [Required]
        [StringLength(10)]
        public string AgentProfileCode { get; set; }

        [StringLength(255)]
        public string AgentProfileName { get; set; }

        [StringLength(50)]
        public string FormOfAgreement { get; set; }

        public DateTime? EffectiveDate { get; set; }

        public DateTime? ExpiredDate { get; set; }

        [Required]
        [StringLength(10)]
        public string Status { get; set; }

        [Column(TypeName = "ntext")]
        public string ContractParty { get; set; }

        [Column(TypeName = "ntext")]
        public string handling { get; set; }

        [Column(TypeName = "ntext")]
        public string ProfitShare { get; set; }

        [Column(TypeName = "ntext")]
        public string PaymentTerm { get; set; }

        [Column(TypeName = "ntext")]
        public string RemittanceMethod { get; set; }

        [Column(TypeName = "ntext")]
        public string Insurance { get; set; }

        [Column(TypeName = "ntext")]
        public string OtherInformation { get; set; }

        [StringLength(50)]
        public string Settlement { get; set; }

        public DateTime? StartDate { get; set; }

        public DateTime? EndDate { get; set; }

        [StringLength(200)]
        public string Remark { get; set; }

        public int? AgentID { get; set; }

        [StringLength(6)]
        public string CreatedBy { get; set; }

        public DateTime CreatedDate { get; set; }

        [StringLength(6)]
        public string UpdatedBy { get; set; }

        public DateTime? UpdatedDate { get; set; }

        [Column(TypeName = "timestamp")]
        [MaxLength(8)]
        [Timestamp]
        public byte[] Version { get; set; }

        [StringLength(500)]
        public string Licenses { get; set; }
    }
}
