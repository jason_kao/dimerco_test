namespace ValuePlus.Domain.eSM
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("SMANActionPlan")]
    public partial class SMANActionPlan
    {
        [Key]
        public int HQID { get; set; }

        [Required]
        [StringLength(2)]
        public string ANActionCode { get; set; }

        [Required]
        [StringLength(30)]
        public string ANActionDescription { get; set; }

        [Required]
        [StringLength(6)]
        public string CreatedBy { get; set; }

        public DateTime CreatedDate { get; set; }

        [StringLength(6)]
        public string UpdatedBy { get; set; }

        public DateTime? UpdatedDate { get; set; }
    }
}
