namespace ValuePlus.Domain.eSM
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("SMChargeCode")]
    public partial class SMChargeCode
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public SMChargeCode()
        {
            SMChargeCodeGLCode = new HashSet<SMChargeCodeGLCode>();
            SMChargeCodeMode = new HashSet<SMChargeCodeMode>();
            SMChargeCodePL = new HashSet<SMChargeCodePL>();
        }

        [Key]
        public int HQID { get; set; }

        [Required]
        [StringLength(10)]
        public string ChargeCode { get; set; }

        public int SubCategoryID { get; set; }

        [StringLength(255)]
        public string ChargeCodeName { get; set; }

        [StringLength(50)]
        public string GLSales { get; set; }

        [StringLength(50)]
        public string GLCost { get; set; }

        [StringLength(50)]
        public string GLExpense { get; set; }

        [StringLength(50)]
        public string DueTo { get; set; }

        [StringLength(50)]
        public string ApplyTo { get; set; }

        [StringLength(255)]
        public string ApplyToCode { get; set; }

        [StringLength(255)]
        public string UOM { get; set; }

        [Required]
        [StringLength(10)]
        public string Status { get; set; }

        [StringLength(6)]
        public string CreatedBy { get; set; }

        public DateTime CreatedDate { get; set; }

        [StringLength(6)]
        public string UpdatedBy { get; set; }

        public DateTime? UpdatedDate { get; set; }

        [Column(TypeName = "timestamp")]
        [MaxLength(8)]
        [Timestamp]
        public byte[] Version { get; set; }

        [StringLength(10)]
        public string DAWBDueTo { get; set; }

        [StringLength(10)]
        public string MBLDueTo { get; set; }

        [StringLength(20)]
        public string SaleCostType { get; set; }

        [StringLength(20)]
        public string SaleCostType2 { get; set; }

        [Column(TypeName = "ntext")]
        public string Remark { get; set; }

        public bool IsProfitShare { get; set; }

        public int? InvoiceType { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<SMChargeCodeGLCode> SMChargeCodeGLCode { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<SMChargeCodeMode> SMChargeCodeMode { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<SMChargeCodePL> SMChargeCodePL { get; set; }
    }
}
