namespace ValuePlus.Domain.eSM
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("SMEDIChargeRuleCarrier")]
    public partial class SMEDIChargeRuleCarrier
    {
        [Key]
        public int HQID { get; set; }

        public int? RuleID { get; set; }

        public int? AirLineID { get; set; }

        [StringLength(10)]
        public string AilLineCode { get; set; }

        [StringLength(50)]
        public string Status { get; set; }

        [StringLength(10)]
        public string CreatedBy { get; set; }

        public DateTime? CreatedDate { get; set; }

        [StringLength(10)]
        public string UpdatedBy { get; set; }

        public DateTime? UpdatedDate { get; set; }
    }
}
