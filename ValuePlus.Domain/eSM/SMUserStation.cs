namespace ValuePlus.Domain.eSM
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("SMUserStation")]
    public partial class SMUserStation
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int HQID { get; set; }

        [Key]
        [Column(Order = 0)]
        [StringLength(10)]
        public string UserID { get; set; }

        [Key]
        [Column(Order = 1)]
        [StringLength(10)]
        public string StationID { get; set; }

        [StringLength(10)]
        public string Status { get; set; }
    }
}
