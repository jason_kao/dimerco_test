namespace ValuePlus.Domain.eSM
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("SMReminderRule")]
    public partial class SMReminderRule
    {
        [Key]
        public int HQID { get; set; }

        [StringLength(50)]
        public string Name { get; set; }

        [StringLength(10)]
        public string Code { get; set; }

        [StringLength(1)]
        public string Type { get; set; }

        [StringLength(100)]
        public string Description { get; set; }

        public int? Category { get; set; }

        public int? Severity { get; set; }

        public int? PID { get; set; }

        [StringLength(2)]
        public string Mode { get; set; }

        [StringLength(10)]
        public string CreatedBy { get; set; }

        public DateTime? CreatedDate { get; set; }

        [StringLength(10)]
        public string UpdatedBy { get; set; }

        public DateTime? UpdatedDate { get; set; }

        [StringLength(20)]
        public string Status { get; set; }
    }
}
