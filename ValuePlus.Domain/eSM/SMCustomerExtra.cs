namespace ValuePlus.Domain.eSM
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("SMCustomerExtra")]
    public partial class SMCustomerExtra
    {
        [Key]
        public int HQID { get; set; }

        public int CustomerID { get; set; }

        [StringLength(10)]
        public string GlobalCode { get; set; }

        [Required]
        [StringLength(10)]
        public string ExtraType { get; set; }

        [Required]
        [StringLength(50)]
        public string FieldName { get; set; }

        [StringLength(50)]
        public string MapName { get; set; }

        [StringLength(1)]
        public string Mandatory { get; set; }

        [StringLength(1000)]
        public string PickupData { get; set; }

        [StringLength(10)]
        public string Status { get; set; }

        [StringLength(3)]
        public string ProductLine { get; set; }

        [StringLength(2)]
        public string ModeCode { get; set; }

        [StringLength(10)]
        public string CreatedBy { get; set; }

        public DateTime? CreatedDate { get; set; }

        [StringLength(10)]
        public string UpdatedBy { get; set; }

        public DateTime? UpdatedDate { get; set; }
    }
}
