namespace ValuePlus.Domain.eSM
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("SMCustomerHDIPlace")]
    public partial class SMCustomerHDIPlace
    {
        [Key]
        public int HQID { get; set; }

        public int HDIID { get; set; }

        [StringLength(50)]
        public string FromPlaceType { get; set; }

        public int? FromPlaceID { get; set; }

        [StringLength(50)]
        public string ToPlaceType { get; set; }

        public int? ToPlaceID { get; set; }

        [StringLength(50)]
        public string CreatedBy { get; set; }

        public DateTime? CreatedDate { get; set; }

        [StringLength(50)]
        public string UpdatedBy { get; set; }

        public DateTime? UpdatedDate { get; set; }

        public bool? Flag { get; set; }
    }
}
