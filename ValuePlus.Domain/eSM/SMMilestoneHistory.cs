namespace ValuePlus.Domain.eSM
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("SMMilestoneHistory")]
    public partial class SMMilestoneHistory
    {
        [Key]
        [Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int ID { get; set; }

        [Key]
        [Column(Order = 1)]
        [StringLength(6)]
        public string StationID { get; set; }

        [StringLength(20)]
        public string keyValue { get; set; }

        public int? Productline { get; set; }

        public int? MilestoneID { get; set; }

        public DateTime? MilestoneTime { get; set; }

        [StringLength(255)]
        public string IrrReason { get; set; }

        [StringLength(5)]
        public string ApprovedBy { get; set; }

        [StringLength(50)]
        public string ExtraValue1 { get; set; }

        [StringLength(50)]
        public string ExtraValue2 { get; set; }

        public int? UTCOffSet { get; set; }

        public int? Status { get; set; }

        public DateTime? CreatedDT { get; set; }

        [StringLength(5)]
        public string CreatedBy { get; set; }
    }
}
