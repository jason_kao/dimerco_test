namespace ValuePlus.Domain.eSM
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("SMAgentControlPL")]
    public partial class SMAgentControlPL
    {
        [Key]
        public int HQID { get; set; }

        public int? VendorID { get; set; }

        public int? PLID { get; set; }

        [StringLength(200)]
        public string SpecializedMarket { get; set; }

        [StringLength(200)]
        public string Network { get; set; }

        [StringLength(50)]
        public string PICPersonal { get; set; }

        [StringLength(200)]
        public string OthersInfo { get; set; }

        [StringLength(500)]
        public string Reason { get; set; }

        public bool? hasCoload { get; set; }

        public bool? hasBorrowIn { get; set; }

        [StringLength(1)]
        public string Status { get; set; }

        [StringLength(50)]
        public string CreateBy { get; set; }

        public DateTime? CreateDate { get; set; }

        [StringLength(50)]
        public string UpdateBy { get; set; }

        public DateTime? UpdateDate { get; set; }
    }
}
