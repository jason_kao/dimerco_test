namespace ValuePlus.Domain.eSM
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("SMOceanCarrier")]
    public partial class SMOceanCarrier
    {
        [Key]
        public int HQID { get; set; }

        [Required]
        [StringLength(3)]
        public string StationID { get; set; }

        [Required]
        [StringLength(10)]
        public string OceanCarrierCode { get; set; }

        public int? CustomerID { get; set; }

        public int? CityID { get; set; }

        [Required]
        [StringLength(255)]
        public string OceanCarrierName { get; set; }

        [StringLength(255)]
        public string OceanCarrierName1 { get; set; }

        [StringLength(255)]
        public string OceanCarrierAddress1 { get; set; }

        [StringLength(255)]
        public string OceanCarrierAddress2 { get; set; }

        [StringLength(255)]
        public string OceanCarrierAddress3 { get; set; }

        [StringLength(255)]
        public string OceanCarrierAddress4 { get; set; }

        [StringLength(255)]
        public string OceanCarrierAddress5 { get; set; }

        [StringLength(50)]
        public string OceanCarrierPhone { get; set; }

        [StringLength(10)]
        public string OceanCarrierPhoneExt { get; set; }

        [StringLength(50)]
        public string OceanCarrierFax { get; set; }

        [StringLength(10)]
        public string OceanCarrierFaxExt { get; set; }

        [StringLength(10)]
        public string OceanCarrierZip { get; set; }

        [StringLength(255)]
        public string OceanCarrierWebSite { get; set; }

        [Required]
        [StringLength(10)]
        public string Status { get; set; }

        [StringLength(6)]
        public string CreatedBy { get; set; }

        public DateTime CreatedDate { get; set; }

        [StringLength(6)]
        public string UpdatedBy { get; set; }

        public DateTime? UpdatedDate { get; set; }

        [Column(TypeName = "timestamp")]
        [MaxLength(8)]
        [Timestamp]
        public byte[] Version { get; set; }

        [StringLength(50)]
        public string SCAC { get; set; }
    }
}
