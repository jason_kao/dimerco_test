namespace ValuePlus.Domain.eSM
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("SMTimeZone")]
    public partial class SMTimeZone
    {
        [Key]
        public int HQID { get; set; }

        [Required]
        [StringLength(50)]
        public string TimeZoneName { get; set; }

        [Required]
        [StringLength(100)]
        public string DisplayName { get; set; }

        [Required]
        [StringLength(50)]
        public string StandardName { get; set; }

        [Required]
        [StringLength(50)]
        public string DaylightName { get; set; }

        public int UTCOffSet { get; set; }
    }
}
