namespace ValuePlus.Domain.eSM
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("SMAgentFormPlace")]
    public partial class SMAgentFormPlace
    {
        [Key]
        public int HQID { get; set; }

        public int VendorPLID { get; set; }

        [StringLength(50)]
        public string FromPlaceType { get; set; }

        public int? FromPlaceID { get; set; }

        [StringLength(50)]
        public string ToPlaceType { get; set; }

        public int? ToPlaceID { get; set; }

        [StringLength(10)]
        public string Createby { get; set; }

        public DateTime? Createdate { get; set; }

        [StringLength(10)]
        public string Updateby { get; set; }

        public DateTime? Updatedate { get; set; }
    }
}
