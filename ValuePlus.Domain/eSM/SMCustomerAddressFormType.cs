namespace ValuePlus.Domain.eSM
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("SMCustomerAddressFormType")]
    public partial class SMCustomerAddressFormType
    {
        [Key]
        public int HQID { get; set; }

        [Required]
        [StringLength(10)]
        public string FormTypeCode { get; set; }

        [StringLength(50)]
        public string FormTypeName { get; set; }

        [Required]
        [StringLength(10)]
        public string Status { get; set; }

        public int? Seq { get; set; }

        public bool? Show { get; set; }
    }
}
