namespace ValuePlus.Domain.eSM
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("SMDepartment")]
    public partial class SMDepartment
    {
        [Key]
        public int HQID { get; set; }

        [Required]
        [StringLength(10)]
        public string DepartmentCode { get; set; }

        [StringLength(255)]
        public string DepartmentName { get; set; }

        [Required]
        [StringLength(3)]
        public string StationID { get; set; }

        [Required]
        [StringLength(10)]
        public string Status { get; set; }

        [StringLength(6)]
        public string CreatedBy { get; set; }

        public DateTime CreatedDate { get; set; }

        [StringLength(6)]
        public string UpdatedBy { get; set; }

        public DateTime? UpdatedDate { get; set; }

        [Column(TypeName = "timestamp")]
        [MaxLength(8)]
        [Timestamp]
        public byte[] Version { get; set; }

        [StringLength(10)]
        public string CRPDepartmentCode { get; set; }
    }
}
