namespace ValuePlus.Domain.eSM
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("SMLocalChargeUnit")]
    public partial class SMLocalChargeUnit
    {
        [Key]
        public int HQID { get; set; }

        public int ProductLineID { get; set; }

        public int? ModeID { get; set; }

        [Required]
        [StringLength(50)]
        public string UnitCode { get; set; }

        [Required]
        [StringLength(50)]
        public string UnitName { get; set; }
    }
}
