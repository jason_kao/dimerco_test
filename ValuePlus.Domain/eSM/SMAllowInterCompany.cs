namespace ValuePlus.Domain.eSM
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("SMAllowInterCompany")]
    public partial class SMAllowInterCompany
    {
        [Key]
        public int HQID { get; set; }

        [StringLength(10)]
        public string StationID { get; set; }

        public int? CustomerID { get; set; }

        public DateTime? CreatedDate { get; set; }

        [StringLength(10)]
        public string CreatedBy { get; set; }

        public DateTime? UpdatedDate { get; set; }

        [StringLength(10)]
        public string UpdatedBy { get; set; }

        [StringLength(1)]
        public string Status { get; set; }
    }
}
