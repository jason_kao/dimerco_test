namespace ValuePlus.Domain.eSM
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("SMLeadSource")]
    public partial class SMLeadSource
    {
        [Key]
        public int HQID { get; set; }

        [Required]
        [StringLength(10)]
        public string LeadSourceCode { get; set; }

        [Required]
        [StringLength(255)]
        public string LeadSourceName { get; set; }

        public int LeadSourceGroupID { get; set; }

        [Required]
        [StringLength(50)]
        public string Status { get; set; }

        [StringLength(6)]
        public string CreatedBy { get; set; }

        public DateTime CreatedDate { get; set; }

        [StringLength(6)]
        public string UpdatedBy { get; set; }

        public DateTime? UpdatedDate { get; set; }
    }
}
