namespace ValuePlus.Domain.eSM
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("SMMilestoneGroup")]
    public partial class SMMilestoneGroup
    {
        [Key]
        [Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int CustomerID { get; set; }

        [Key]
        [Column(Order = 1)]
        public int MilestoneGroupID { get; set; }

        [StringLength(10)]
        public string CreatedBy { get; set; }

        public DateTime? CreatedDate { get; set; }

        [StringLength(10)]
        public string UpdatedBy { get; set; }

        public DateTime? UpdatedDate { get; set; }

        public int? Productline { get; set; }

        [StringLength(10)]
        public string GlobalCode { get; set; }

        [StringLength(3)]
        public string MoveCode { get; set; }

        public int? ProjectID { get; set; }

        [StringLength(3)]
        public string SHPTTYPE { get; set; }
    }
}
