namespace ValuePlus.Domain.eSM
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("SMGlobalRate")]
    public partial class SMGlobalRate
    {
        [Key]
        public int HQID { get; set; }

        [StringLength(3)]
        public string FromCurrency { get; set; }

        [StringLength(3)]
        public string ToCurrency { get; set; }

        public decimal? ConvertRate { get; set; }

        public DateTime? RateDate { get; set; }
    }
}
