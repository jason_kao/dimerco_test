namespace ValuePlus.Domain.eSM
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("SMCustomerFormPlace")]
    public partial class SMCustomerFormPlace
    {
        [Key]
        public int HQID { get; set; }

        public int FormID { get; set; }

        [StringLength(50)]
        public string FromPlaceType { get; set; }

        public int? FromPlaceID { get; set; }

        [StringLength(50)]
        public string ToPlaceType { get; set; }

        public int? ToPlaceID { get; set; }
    }
}
