namespace ValuePlus.Domain.eSM
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("SMMilestoneDetail")]
    public partial class SMMilestoneDetail
    {
        [Key]
        [Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int ID { get; set; }

        [Key]
        [Column(Order = 1)]
        [StringLength(6)]
        public string StationID { get; set; }

        [StringLength(20)]
        public string ParentkeyValue { get; set; }

        [StringLength(20)]
        public string ContainerKey { get; set; }

        [StringLength(20)]
        public string POKey { get; set; }

        [StringLength(20)]
        public string SOKey { get; set; }

        public int? ProductLine { get; set; }

        public int? MilestoneID { get; set; }

        public DateTime? MilestoneTime { get; set; }

        [StringLength(50)]
        public string ExtraValue1 { get; set; }

        [StringLength(50)]
        public string ExtraValue2 { get; set; }

        public int? UTCOffset { get; set; }

        public int? Status { get; set; }

        public DateTime? CreatedDT { get; set; }

        [StringLength(5)]
        public string CreatedBy { get; set; }
    }
}
