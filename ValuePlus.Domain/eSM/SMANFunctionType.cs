namespace ValuePlus.Domain.eSM
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("SMANFunctionType")]
    public partial class SMANFunctionType
    {
        [Key]
        public int HQID { get; set; }

        [StringLength(4)]
        public string ANFunctionCode { get; set; }

        [StringLength(30)]
        public string ANFunctionName { get; set; }

        [Required]
        [StringLength(3)]
        public string ANDIMCode1 { get; set; }

        [Required]
        [StringLength(3)]
        public string ANDIMCode2 { get; set; }

        [StringLength(3)]
        public string ANDIMCode3 { get; set; }

        [Required]
        [StringLength(6)]
        public string CreatedBy { get; set; }

        public DateTime CreatedDate { get; set; }

        [StringLength(6)]
        public string UpdatedBy { get; set; }

        public DateTime? UpdatedDate { get; set; }

        [StringLength(1)]
        public string Repeter { get; set; }

        [StringLength(30)]
        public string Pattern { get; set; }

        [StringLength(10)]
        public string Status { get; set; }
    }
}
