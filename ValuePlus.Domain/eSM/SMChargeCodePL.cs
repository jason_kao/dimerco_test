namespace ValuePlus.Domain.eSM
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("SMChargeCodePL")]
    public partial class SMChargeCodePL
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int HQID { get; set; }

        [Key]
        [Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int ChargeCodeID { get; set; }

        [Key]
        [Column(Order = 1)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int ProductLineID { get; set; }

        [StringLength(50)]
        public string ExportType { get; set; }

        [StringLength(50)]
        public string ImportType { get; set; }

        [Required]
        [StringLength(10)]
        public string Status { get; set; }

        [StringLength(6)]
        public string CreatedBy { get; set; }

        public DateTime CreatedDate { get; set; }

        [StringLength(6)]
        public string UpdatedBy { get; set; }

        public DateTime? UpdatedDate { get; set; }

        [Column(TypeName = "timestamp")]
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        [MaxLength(8)]
        public byte[] Version { get; set; }

        [StringLength(2)]
        public string VATPP { get; set; }

        [StringLength(2)]
        public string VATCC { get; set; }

        [StringLength(2)]
        public string VatCost { get; set; }

        [StringLength(10)]
        public string SGLCode { get; set; }

        [StringLength(10)]
        public string CGLCode { get; set; }

        [StringLength(50)]
        public string UOMType { get; set; }

        public bool? IsUsed { get; set; }

        [StringLength(50)]
        public string UOMType2 { get; set; }

        public virtual SMChargeCode SMChargeCode { get; set; }

        public virtual SMProductLine SMProductLine { get; set; }
    }
}
