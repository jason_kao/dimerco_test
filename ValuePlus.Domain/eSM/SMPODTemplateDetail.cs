namespace ValuePlus.Domain.eSM
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("SMPODTemplateDetail")]
    public partial class SMPODTemplateDetail
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int HQID { get; set; }

        [Key]
        [Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int PODTEMPLID { get; set; }

        [StringLength(5)]
        public string Mode { get; set; }

        [Key]
        [Column(Order = 1)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int Sequence { get; set; }

        [StringLength(20)]
        public string Milestone { get; set; }

        [StringLength(50)]
        public string MilestoneDesc { get; set; }

        [StringLength(50)]
        public string ReasonDesc { get; set; }

        [StringLength(50)]
        public string DateTable { get; set; }

        [StringLength(50)]
        public string DateField { get; set; }

        [StringLength(50)]
        public string PartyTable { get; set; }

        [StringLength(50)]
        public string PartyField { get; set; }

        public bool FRequired { get; set; }

        public bool FDate { get; set; }

        public bool FParty { get; set; }

        public bool FReason { get; set; }

        [StringLength(10)]
        public string CreatedBy { get; set; }

        public DateTime? CreatedDate { get; set; }

        [StringLength(10)]
        public string UpdatedBy { get; set; }

        public DateTime? UpdatedDate { get; set; }
    }
}
