namespace ValuePlus.Domain.eSM
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("SMPODTemplate")]
    public partial class SMPODTemplate
    {
        [Key]
        public int HQID { get; set; }

        public int CustomerID { get; set; }

        public int? DEPT { get; set; }

        public int? DSTN { get; set; }

        [StringLength(100)]
        public string PODTEMPLDesc { get; set; }

        [StringLength(10)]
        public string GLobalCode { get; set; }

        [StringLength(10)]
        public string CreatedBy { get; set; }

        public DateTime? CreatedDate { get; set; }

        [StringLength(10)]
        public string UpdatedBy { get; set; }

        public DateTime? UpdatedDate { get; set; }

        [StringLength(50)]
        public string TradeTerm { get; set; }
    }
}
