namespace ValuePlus.Domain.eSM
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("SMUOM")]
    public partial class SMUOM
    {
        [Key]
        public int HQID { get; set; }

        [Required]
        [StringLength(10)]
        public string UOMCode { get; set; }

        [StringLength(255)]
        public string UOMName { get; set; }

        [StringLength(50)]
        public string Decimal { get; set; }

        [StringLength(50)]
        public string Rounding { get; set; }

        [Required]
        [StringLength(20)]
        public string UOMType { get; set; }

        [StringLength(10)]
        public string Status { get; set; }

        [StringLength(6)]
        public string CreatedBy { get; set; }

        public DateTime CreatedDate { get; set; }

        [StringLength(6)]
        public string UpdatedBy { get; set; }

        public DateTime? UpdatedDate { get; set; }

        [Column(TypeName = "timestamp")]
        [MaxLength(8)]
        [Timestamp]
        public byte[] Version { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? Height { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? Length { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? Width { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? GrossWeight { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? TareWeight { get; set; }
    }
}
