namespace ValuePlus.Domain.eSM
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    public partial class eSM_Entities : DbContext
    {
        public eSM_Entities()
            : base("name=ReSM")
        {
        }

        public virtual DbSet<SMAgent> SMAgent { get; set; }
        public virtual DbSet<SMAgentContact> SMAgentContact { get; set; }
        public virtual DbSet<SMAgentControlPL> SMAgentControlPL { get; set; }
        public virtual DbSet<SMAgentFormPlace> SMAgentFormPlace { get; set; }
        public virtual DbSet<SMAgentProfile> SMAgentProfile { get; set; }
        public virtual DbSet<SMAirLine> SMAirLine { get; set; }
        public virtual DbSet<SMAirPort> SMAirPort { get; set; }
        public virtual DbSet<SMAllowInterCompany> SMAllowInterCompany { get; set; }
        public virtual DbSet<SMANActionPlan> SMANActionPlan { get; set; }
        public virtual DbSet<SMANActionRule> SMANActionRule { get; set; }
        public virtual DbSet<SMANDimension> SMANDimension { get; set; }
        public virtual DbSet<SMANFunctionType> SMANFunctionType { get; set; }
        public virtual DbSet<SMANLevelRange> SMANLevelRange { get; set; }
        public virtual DbSet<SMANMailReceivedList> SMANMailReceivedList { get; set; }
        public virtual DbSet<SMAppAgentSetting> SMAppAgentSetting { get; set; }
        public virtual DbSet<SMArea> SMArea { get; set; }
        public virtual DbSet<SMBusinessEntity> SMBusinessEntity { get; set; }
        public virtual DbSet<SMChargeCode> SMChargeCode { get; set; }
        public virtual DbSet<SMChargeCodeByAirline> SMChargeCodeByAirline { get; set; }
        public virtual DbSet<SMChargeCodeCategory> SMChargeCodeCategory { get; set; }
        public virtual DbSet<SMChargeCodeGLCode> SMChargeCodeGLCode { get; set; }
        public virtual DbSet<SMChargeCodeIATAList> SMChargeCodeIATAList { get; set; }
        public virtual DbSet<SMChargeCodeLocal> SMChargeCodeLocal { get; set; }
        public virtual DbSet<SMChargeCodeMode> SMChargeCodeMode { get; set; }
        public virtual DbSet<SMChargeCodePL> SMChargeCodePL { get; set; }
        public virtual DbSet<SMChargeCodePLCountry> SMChargeCodePLCountry { get; set; }
        public virtual DbSet<SMChargeCodePLMethod> SMChargeCodePLMethod { get; set; }
        public virtual DbSet<SMChargeCodeSubCategory> SMChargeCodeSubCategory { get; set; }
        public virtual DbSet<SMChargeCodeSubCategoryPL> SMChargeCodeSubCategoryPL { get; set; }
        public virtual DbSet<SMChargeCodeUOM> SMChargeCodeUOM { get; set; }
        public virtual DbSet<SMChartofAccount> SMChartofAccount { get; set; }
        public virtual DbSet<SMCity> SMCity { get; set; }
        public virtual DbSet<SMCityPL> SMCityPL { get; set; }
        public virtual DbSet<SMCodeSetting> SMCodeSetting { get; set; }
        public virtual DbSet<SMControlGroup> SMControlGroup { get; set; }
        public virtual DbSet<SMControlGroupDetail> SMControlGroupDetail { get; set; }
        public virtual DbSet<SMCountry> SMCountry { get; set; }
        public virtual DbSet<SMCountryHual> SMCountryHual { get; set; }
        public virtual DbSet<SMCreditTerm> SMCreditTerm { get; set; }
        public virtual DbSet<SMCreditTermDIMCountry> SMCreditTermDIMCountry { get; set; }
        public virtual DbSet<SMCreditTermPL> SMCreditTermPL { get; set; }
        public virtual DbSet<SMCurrency> SMCurrency { get; set; }
        public virtual DbSet<SMCustomer> SMCustomer { get; set; }
        public virtual DbSet<SMCustomerAddress> SMCustomerAddress { get; set; }
        public virtual DbSet<SMCustomerAddressFormType> SMCustomerAddressFormType { get; set; }
        public virtual DbSet<SMCustomerAddressMode> SMCustomerAddressMode { get; set; }
        public virtual DbSet<SMCustomerContact> SMCustomerContact { get; set; }
        public virtual DbSet<SMCustomerContactPL> SMCustomerContactPL { get; set; }
        public virtual DbSet<SMCustomerExtra> SMCustomerExtra { get; set; }
        public virtual DbSet<SMCustomerFormMail> SMCustomerFormMail { get; set; }
        public virtual DbSet<SMCustomerFormPlace> SMCustomerFormPlace { get; set; }
        public virtual DbSet<SMCustomerForms> SMCustomerForms { get; set; }
        public virtual DbSet<SMCustomerHDI> SMCustomerHDI { get; set; }
        public virtual DbSet<SMCustomerHDIPlace> SMCustomerHDIPlace { get; set; }
        public virtual DbSet<SMCustomerLogin> SMCustomerLogin { get; set; }
        public virtual DbSet<SMCustomerMap> SMCustomerMap { get; set; }
        public virtual DbSet<SMCustomerPL> SMCustomerPL { get; set; }
        public virtual DbSet<SMCustomerPreAlertTempl> SMCustomerPreAlertTempl { get; set; }
        public virtual DbSet<SMCustomerPreAlertTemplMap> SMCustomerPreAlertTemplMap { get; set; }
        public virtual DbSet<SMCustomerRegister> SMCustomerRegister { get; set; }
        public virtual DbSet<SMDepartment> SMDepartment { get; set; }
        public virtual DbSet<SMDepartmentCRP> SMDepartmentCRP { get; set; }
        public virtual DbSet<SMDIMCountry> SMDIMCountry { get; set; }
        public virtual DbSet<SMDIMDistrict> SMDIMDistrict { get; set; }
        public virtual DbSet<SMDIMRegion> SMDIMRegion { get; set; }
        public virtual DbSet<SMDistrict> SMDistrict { get; set; }
        public virtual DbSet<SMDivision> SMDivision { get; set; }
        public virtual DbSet<SMDTMode> SMDTMode { get; set; }
        public virtual DbSet<SMeAWBAddressSetting> SMeAWBAddressSetting { get; set; }
        public virtual DbSet<SMEDI> SMEDI { get; set; }
        public virtual DbSet<SMEDIChargeRuleCarrier> SMEDIChargeRuleCarrier { get; set; }
        public virtual DbSet<SMEDIChargeRuleCode> SMEDIChargeRuleCode { get; set; }
        public virtual DbSet<SMEDIType> SMEDIType { get; set; }
        public virtual DbSet<SMFieldSetting> SMFieldSetting { get; set; }
        public virtual DbSet<SMGlobalRate> SMGlobalRate { get; set; }
        public virtual DbSet<SMGRegion> SMGRegion { get; set; }
        public virtual DbSet<SMGroupMilestone> SMGroupMilestone { get; set; }
        public virtual DbSet<SMGroupRoleSetting> SMGroupRoleSetting { get; set; }
        public virtual DbSet<SMHubBranchStationPL> SMHubBranchStationPL { get; set; }
        public virtual DbSet<SMIndustrialCategory> SMIndustrialCategory { get; set; }
        public virtual DbSet<SMIndustrialGroup> SMIndustrialGroup { get; set; }
        public virtual DbSet<SMISFData> SMISFData { get; set; }
        public virtual DbSet<SMLeadSource> SMLeadSource { get; set; }
        public virtual DbSet<SMLeadSourceGroup> SMLeadSourceGroup { get; set; }
        public virtual DbSet<SMLELevel1> SMLELevel1 { get; set; }
        public virtual DbSet<SMLELevel2> SMLELevel2 { get; set; }
        public virtual DbSet<SMLERegion> SMLERegion { get; set; }
        public virtual DbSet<SMLocalChargeUnit> SMLocalChargeUnit { get; set; }
        public virtual DbSet<SMMailSetting> SMMailSetting { get; set; }
        public virtual DbSet<SMMilestone> SMMilestone { get; set; }
        public virtual DbSet<SMMilestoneDetail> SMMilestoneDetail { get; set; }
        public virtual DbSet<SMMilestoneGroup> SMMilestoneGroup { get; set; }
        public virtual DbSet<SMMilestoneHistory> SMMilestoneHistory { get; set; }
        public virtual DbSet<SMMode> SMMode { get; set; }
        public virtual DbSet<SMMove> SMMove { get; set; }
        public virtual DbSet<SMOceanCarrier> SMOceanCarrier { get; set; }
        public virtual DbSet<SMPayType> SMPayType { get; set; }
        public virtual DbSet<SMPODTemplate> SMPODTemplate { get; set; }
        public virtual DbSet<SMPODTemplateDetail> SMPODTemplateDetail { get; set; }
        public virtual DbSet<SMProductLine> SMProductLine { get; set; }
        public virtual DbSet<SMProject> SMProject { get; set; }
        public virtual DbSet<SMRateClass> SMRateClass { get; set; }
        public virtual DbSet<SMReminderRule> SMReminderRule { get; set; }
        public virtual DbSet<SMSeaPort> SMSeaPort { get; set; }
        public virtual DbSet<SMServiceType> SMServiceType { get; set; }
        public virtual DbSet<SMShipmentType> SMShipmentType { get; set; }
        public virtual DbSet<SMShowRate> SMShowRate { get; set; }
        public virtual DbSet<SMSiteURLMap> SMSiteURLMap { get; set; }
        public virtual DbSet<SMSPL> SMSPL { get; set; }
        public virtual DbSet<SMState> SMState { get; set; }
        public virtual DbSet<SMStation> SMStation { get; set; }
        public virtual DbSet<SMStationConfig> SMStationConfig { get; set; }
        public virtual DbSet<SMTariffCode> SMTariffCode { get; set; }
        public virtual DbSet<SMTimeZone> SMTimeZone { get; set; }
        public virtual DbSet<SMTradeTerm> SMTradeTerm { get; set; }
        public virtual DbSet<SMUOM> SMUOM { get; set; }
        public virtual DbSet<SMUOMConversion> SMUOMConversion { get; set; }
        public virtual DbSet<SMUOMPL> SMUOMPL { get; set; }
        public virtual DbSet<SMUser> SMUser { get; set; }
        public virtual DbSet<SMUserRight> SMUserRight { get; set; }
        public virtual DbSet<SMUserStation> SMUserStation { get; set; }
        public virtual DbSet<SMVATCountry> SMVATCountry { get; set; }
        public virtual DbSet<SMVesselName> SMVesselName { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<SMAgent>()
                .Property(e => e.StationID)
                .IsUnicode(false);

            modelBuilder.Entity<SMAgent>()
                .Property(e => e.AgentCode)
                .IsUnicode(false);

            modelBuilder.Entity<SMAgent>()
                .Property(e => e.AgentPhone)
                .IsUnicode(false);

            modelBuilder.Entity<SMAgent>()
                .Property(e => e.AgentPhoneExt)
                .IsUnicode(false);

            modelBuilder.Entity<SMAgent>()
                .Property(e => e.AgentFax)
                .IsUnicode(false);

            modelBuilder.Entity<SMAgent>()
                .Property(e => e.AgentFaxExt)
                .IsUnicode(false);

            modelBuilder.Entity<SMAgent>()
                .Property(e => e.AgentZip)
                .IsUnicode(false);

            modelBuilder.Entity<SMAgent>()
                .Property(e => e.Status)
                .IsUnicode(false);

            modelBuilder.Entity<SMAgent>()
                .Property(e => e.CreatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<SMAgent>()
                .Property(e => e.UpdatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<SMAgent>()
                .Property(e => e.Version)
                .IsFixedLength();

            modelBuilder.Entity<SMAgent>()
                .Property(e => e.Longitute)
                .IsUnicode(false);

            modelBuilder.Entity<SMAgent>()
                .Property(e => e.Latitude)
                .IsUnicode(false);

            modelBuilder.Entity<SMAgent>()
                .Property(e => e.ProductLine)
                .IsUnicode(false);

            modelBuilder.Entity<SMAgent>()
                .Property(e => e.Branch)
                .IsUnicode(false);

            modelBuilder.Entity<SMAgent>()
                .Property(e => e.Continent)
                .IsUnicode(false);

            modelBuilder.Entity<SMAgent>()
                .Property(e => e.extShow)
                .IsUnicode(false);

            modelBuilder.Entity<SMAgentContact>()
                .Property(e => e.Password)
                .IsUnicode(false);

            modelBuilder.Entity<SMAgentContact>()
                .Property(e => e.ExtShow)
                .IsUnicode(false);

            modelBuilder.Entity<SMAgentContact>()
                .Property(e => e.Admin)
                .IsUnicode(false);

            modelBuilder.Entity<SMAgentContact>()
                .Property(e => e.Status)
                .IsUnicode(false);

            modelBuilder.Entity<SMAgentContact>()
                .Property(e => e.ExtPhone)
                .IsUnicode(false);

            modelBuilder.Entity<SMAgentContact>()
                .Property(e => e.FaxNo)
                .IsUnicode(false);

            modelBuilder.Entity<SMAgentContact>()
                .Property(e => e.CreatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<SMAgentContact>()
                .Property(e => e.UpdatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<SMAgentControlPL>()
                .Property(e => e.SpecializedMarket)
                .IsUnicode(false);

            modelBuilder.Entity<SMAgentControlPL>()
                .Property(e => e.Network)
                .IsUnicode(false);

            modelBuilder.Entity<SMAgentControlPL>()
                .Property(e => e.PICPersonal)
                .IsUnicode(false);

            modelBuilder.Entity<SMAgentControlPL>()
                .Property(e => e.OthersInfo)
                .IsUnicode(false);

            modelBuilder.Entity<SMAgentControlPL>()
                .Property(e => e.Reason)
                .IsUnicode(false);

            modelBuilder.Entity<SMAgentControlPL>()
                .Property(e => e.Status)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<SMAgentControlPL>()
                .Property(e => e.CreateBy)
                .IsUnicode(false);

            modelBuilder.Entity<SMAgentControlPL>()
                .Property(e => e.UpdateBy)
                .IsUnicode(false);

            modelBuilder.Entity<SMAgentFormPlace>()
                .Property(e => e.FromPlaceType)
                .IsUnicode(false);

            modelBuilder.Entity<SMAgentFormPlace>()
                .Property(e => e.ToPlaceType)
                .IsUnicode(false);

            modelBuilder.Entity<SMAgentFormPlace>()
                .Property(e => e.Createby)
                .IsUnicode(false);

            modelBuilder.Entity<SMAgentFormPlace>()
                .Property(e => e.Updateby)
                .IsUnicode(false);

            modelBuilder.Entity<SMAgentProfile>()
                .Property(e => e.AgentProfileCode)
                .IsUnicode(false);

            modelBuilder.Entity<SMAgentProfile>()
                .Property(e => e.Status)
                .IsUnicode(false);

            modelBuilder.Entity<SMAgentProfile>()
                .Property(e => e.CreatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<SMAgentProfile>()
                .Property(e => e.UpdatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<SMAgentProfile>()
                .Property(e => e.Version)
                .IsFixedLength();

            modelBuilder.Entity<SMAirLine>()
                .Property(e => e.StationID)
                .IsUnicode(false);

            modelBuilder.Entity<SMAirLine>()
                .Property(e => e.AirLineCode)
                .IsUnicode(false);

            modelBuilder.Entity<SMAirLine>()
                .Property(e => e.AirLinePhone)
                .IsUnicode(false);

            modelBuilder.Entity<SMAirLine>()
                .Property(e => e.AirLinePhoneExt)
                .IsUnicode(false);

            modelBuilder.Entity<SMAirLine>()
                .Property(e => e.AirLineFax)
                .IsUnicode(false);

            modelBuilder.Entity<SMAirLine>()
                .Property(e => e.AirLineFaxExt)
                .IsUnicode(false);

            modelBuilder.Entity<SMAirLine>()
                .Property(e => e.AirLineZip)
                .IsUnicode(false);

            modelBuilder.Entity<SMAirLine>()
                .Property(e => e.Status)
                .IsUnicode(false);

            modelBuilder.Entity<SMAirLine>()
                .Property(e => e.CreatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<SMAirLine>()
                .Property(e => e.UpdatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<SMAirLine>()
                .Property(e => e.Version)
                .IsFixedLength();

            modelBuilder.Entity<SMAirPort>()
                .Property(e => e.AirPortCode)
                .IsUnicode(false);

            modelBuilder.Entity<SMAirPort>()
                .Property(e => e.Status)
                .IsUnicode(false);

            modelBuilder.Entity<SMAirPort>()
                .Property(e => e.CreatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<SMAirPort>()
                .Property(e => e.UpdatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<SMAirPort>()
                .Property(e => e.Version)
                .IsFixedLength();

            modelBuilder.Entity<SMAllowInterCompany>()
                .Property(e => e.StationID)
                .IsUnicode(false);

            modelBuilder.Entity<SMAllowInterCompany>()
                .Property(e => e.CreatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<SMAllowInterCompany>()
                .Property(e => e.UpdatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<SMAllowInterCompany>()
                .Property(e => e.Status)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<SMANActionPlan>()
                .Property(e => e.ANActionCode)
                .IsUnicode(false);

            modelBuilder.Entity<SMANActionPlan>()
                .Property(e => e.ANActionDescription)
                .IsUnicode(false);

            modelBuilder.Entity<SMANActionPlan>()
                .Property(e => e.CreatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<SMANActionPlan>()
                .Property(e => e.UpdatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<SMANActionRule>()
                .Property(e => e.ANFunctionCode)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<SMANActionRule>()
                .Property(e => e.Type1Level)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<SMANActionRule>()
                .Property(e => e.Type2Level)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<SMANActionRule>()
                .Property(e => e.Type3Level)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<SMANActionRule>()
                .Property(e => e.ANActionCode)
                .IsUnicode(false);

            modelBuilder.Entity<SMANActionRule>()
                .Property(e => e.CreatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<SMANActionRule>()
                .Property(e => e.UpdatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<SMANActionRule>()
                .Property(e => e.MixedLabel)
                .IsUnicode(false);

            modelBuilder.Entity<SMANDimension>()
                .Property(e => e.ANDIMCode)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<SMANDimension>()
                .Property(e => e.ANDIMDescription)
                .IsUnicode(false);

            modelBuilder.Entity<SMANDimension>()
                .Property(e => e.CreatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<SMANDimension>()
                .Property(e => e.UpdatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<SMANFunctionType>()
                .Property(e => e.ANFunctionCode)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<SMANFunctionType>()
                .Property(e => e.ANFunctionName)
                .IsUnicode(false);

            modelBuilder.Entity<SMANFunctionType>()
                .Property(e => e.ANDIMCode1)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<SMANFunctionType>()
                .Property(e => e.ANDIMCode2)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<SMANFunctionType>()
                .Property(e => e.ANDIMCode3)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<SMANFunctionType>()
                .Property(e => e.CreatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<SMANFunctionType>()
                .Property(e => e.UpdatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<SMANFunctionType>()
                .Property(e => e.Repeter)
                .IsUnicode(false);

            modelBuilder.Entity<SMANFunctionType>()
                .Property(e => e.Pattern)
                .IsUnicode(false);

            modelBuilder.Entity<SMANFunctionType>()
                .Property(e => e.Status)
                .IsUnicode(false);

            modelBuilder.Entity<SMANLevelRange>()
                .Property(e => e.ANDIMCode)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<SMANLevelRange>()
                .Property(e => e.LevelCode)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<SMANLevelRange>()
                .Property(e => e.CreatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<SMANLevelRange>()
                .Property(e => e.UpdatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<SMANMailReceivedList>()
                .Property(e => e.ANFunctionCode)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<SMANMailReceivedList>()
                .Property(e => e.PartyType)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<SMANMailReceivedList>()
                .Property(e => e.MailType)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<SMANMailReceivedList>()
                .Property(e => e.UserID)
                .IsUnicode(false);

            modelBuilder.Entity<SMANMailReceivedList>()
                .Property(e => e.Status)
                .IsUnicode(false);

            modelBuilder.Entity<SMANMailReceivedList>()
                .Property(e => e.CreatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<SMANMailReceivedList>()
                .Property(e => e.UpdatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<SMAppAgentSetting>()
                .Property(e => e.StationID)
                .IsUnicode(false);

            modelBuilder.Entity<SMAppAgentSetting>()
                .Property(e => e.Mode)
                .IsUnicode(false);

            modelBuilder.Entity<SMAppAgentSetting>()
                .Property(e => e.Status)
                .IsUnicode(false);

            modelBuilder.Entity<SMAppAgentSetting>()
                .Property(e => e.CreatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<SMAppAgentSetting>()
                .Property(e => e.UpdatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<SMArea>()
                .Property(e => e.AreaCode)
                .IsUnicode(false);

            modelBuilder.Entity<SMArea>()
                .Property(e => e.Status)
                .IsUnicode(false);

            modelBuilder.Entity<SMArea>()
                .Property(e => e.CreatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<SMArea>()
                .Property(e => e.UpdatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<SMArea>()
                .Property(e => e.Version)
                .IsFixedLength();

            modelBuilder.Entity<SMBusinessEntity>()
                .Property(e => e.StationID)
                .IsUnicode(false);

            modelBuilder.Entity<SMBusinessEntity>()
                .Property(e => e.ParentID)
                .IsUnicode(false);

            modelBuilder.Entity<SMBusinessEntity>()
                .Property(e => e.CreatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<SMBusinessEntity>()
                .Property(e => e.UpdatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<SMBusinessEntity>()
                .Property(e => e.Version)
                .IsFixedLength();

            modelBuilder.Entity<SMBusinessEntity>()
                .Property(e => e.Status)
                .IsUnicode(false);

            modelBuilder.Entity<SMChargeCode>()
                .Property(e => e.ChargeCode)
                .IsUnicode(false);

            modelBuilder.Entity<SMChargeCode>()
                .Property(e => e.GLSales)
                .IsUnicode(false);

            modelBuilder.Entity<SMChargeCode>()
                .Property(e => e.GLCost)
                .IsUnicode(false);

            modelBuilder.Entity<SMChargeCode>()
                .Property(e => e.GLExpense)
                .IsUnicode(false);

            modelBuilder.Entity<SMChargeCode>()
                .Property(e => e.DueTo)
                .IsUnicode(false);

            modelBuilder.Entity<SMChargeCode>()
                .Property(e => e.ApplyTo)
                .IsUnicode(false);

            modelBuilder.Entity<SMChargeCode>()
                .Property(e => e.ApplyToCode)
                .IsUnicode(false);

            modelBuilder.Entity<SMChargeCode>()
                .Property(e => e.Status)
                .IsUnicode(false);

            modelBuilder.Entity<SMChargeCode>()
                .Property(e => e.CreatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<SMChargeCode>()
                .Property(e => e.UpdatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<SMChargeCode>()
                .Property(e => e.Version)
                .IsFixedLength();

            modelBuilder.Entity<SMChargeCode>()
                .Property(e => e.DAWBDueTo)
                .IsUnicode(false);

            modelBuilder.Entity<SMChargeCode>()
                .Property(e => e.MBLDueTo)
                .IsUnicode(false);

            modelBuilder.Entity<SMChargeCode>()
                .Property(e => e.SaleCostType)
                .IsUnicode(false);

            modelBuilder.Entity<SMChargeCode>()
                .Property(e => e.SaleCostType2)
                .IsUnicode(false);

            modelBuilder.Entity<SMChargeCode>()
                .HasMany(e => e.SMChargeCodeGLCode)
                .WithOptional(e => e.SMChargeCode)
                .HasForeignKey(e => e.ChargeCodeID);

            modelBuilder.Entity<SMChargeCode>()
                .HasMany(e => e.SMChargeCodeMode)
                .WithRequired(e => e.SMChargeCode)
                .HasForeignKey(e => e.ChargeCodeID)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<SMChargeCode>()
                .HasMany(e => e.SMChargeCodePL)
                .WithRequired(e => e.SMChargeCode)
                .HasForeignKey(e => e.ChargeCodeID)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<SMChargeCodeByAirline>()
                .Property(e => e.StationID)
                .IsUnicode(false);

            modelBuilder.Entity<SMChargeCodeByAirline>()
                .Property(e => e.Createdby)
                .IsUnicode(false);

            modelBuilder.Entity<SMChargeCodeByAirline>()
                .Property(e => e.Updatedby)
                .IsUnicode(false);

            modelBuilder.Entity<SMChargeCodeCategory>()
                .Property(e => e.CategoryCode)
                .IsUnicode(false);

            modelBuilder.Entity<SMChargeCodeCategory>()
                .Property(e => e.Status)
                .IsUnicode(false);

            modelBuilder.Entity<SMChargeCodeCategory>()
                .Property(e => e.CreatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<SMChargeCodeCategory>()
                .Property(e => e.UpdatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<SMChargeCodeCategory>()
                .Property(e => e.Version)
                .IsFixedLength();

            modelBuilder.Entity<SMChargeCodeGLCode>()
                .Property(e => e.ModeCode)
                .IsUnicode(false);

            modelBuilder.Entity<SMChargeCodeGLCode>()
                .Property(e => e.SGLCode)
                .IsUnicode(false);

            modelBuilder.Entity<SMChargeCodeGLCode>()
                .Property(e => e.CGLCode)
                .IsUnicode(false);

            modelBuilder.Entity<SMChargeCodeGLCode>()
                .Property(e => e.CreatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<SMChargeCodeGLCode>()
                .Property(e => e.UpdatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<SMChargeCodeGLCode>()
                .Property(e => e.Version)
                .IsFixedLength();

            modelBuilder.Entity<SMChargeCodeIATAList>()
                .Property(e => e.IATAChargeCode)
                .IsUnicode(false);

            modelBuilder.Entity<SMChargeCodeIATAList>()
                .Property(e => e.IATAChargeCodeDesc)
                .IsUnicode(false);

            modelBuilder.Entity<SMChargeCodeIATAList>()
                .Property(e => e.Ref)
                .IsUnicode(false);

            modelBuilder.Entity<SMChargeCodeLocal>()
                .Property(e => e.StationID)
                .IsUnicode(false);

            modelBuilder.Entity<SMChargeCodeMode>()
                .Property(e => e.CSType)
                .IsUnicode(false);

            modelBuilder.Entity<SMChargeCodeMode>()
                .Property(e => e.Status)
                .IsUnicode(false);

            modelBuilder.Entity<SMChargeCodeMode>()
                .Property(e => e.CreatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<SMChargeCodeMode>()
                .Property(e => e.UpdatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<SMChargeCodeMode>()
                .Property(e => e.Version)
                .IsFixedLength();

            modelBuilder.Entity<SMChargeCodeMode>()
                .Property(e => e.UOMType)
                .IsUnicode(false);

            modelBuilder.Entity<SMChargeCodePL>()
                .Property(e => e.ExportType)
                .IsUnicode(false);

            modelBuilder.Entity<SMChargeCodePL>()
                .Property(e => e.ImportType)
                .IsUnicode(false);

            modelBuilder.Entity<SMChargeCodePL>()
                .Property(e => e.Status)
                .IsUnicode(false);

            modelBuilder.Entity<SMChargeCodePL>()
                .Property(e => e.CreatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<SMChargeCodePL>()
                .Property(e => e.UpdatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<SMChargeCodePL>()
                .Property(e => e.Version)
                .IsFixedLength();

            modelBuilder.Entity<SMChargeCodePL>()
                .Property(e => e.VATPP)
                .IsUnicode(false);

            modelBuilder.Entity<SMChargeCodePL>()
                .Property(e => e.VATCC)
                .IsUnicode(false);

            modelBuilder.Entity<SMChargeCodePL>()
                .Property(e => e.VatCost)
                .IsUnicode(false);

            modelBuilder.Entity<SMChargeCodePL>()
                .Property(e => e.SGLCode)
                .IsUnicode(false);

            modelBuilder.Entity<SMChargeCodePL>()
                .Property(e => e.CGLCode)
                .IsUnicode(false);

            modelBuilder.Entity<SMChargeCodePL>()
                .Property(e => e.UOMType)
                .IsUnicode(false);

            modelBuilder.Entity<SMChargeCodePL>()
                .Property(e => e.UOMType2)
                .IsUnicode(false);

            modelBuilder.Entity<SMChargeCodePLCountry>()
                .Property(e => e.VATPP)
                .IsUnicode(false);

            modelBuilder.Entity<SMChargeCodePLCountry>()
                .Property(e => e.VATCC)
                .IsUnicode(false);

            modelBuilder.Entity<SMChargeCodePLCountry>()
                .Property(e => e.VATCost)
                .IsUnicode(false);

            modelBuilder.Entity<SMChargeCodePLCountry>()
                .Property(e => e.Status)
                .IsUnicode(false);

            modelBuilder.Entity<SMChargeCodePLCountry>()
                .Property(e => e.CreatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<SMChargeCodePLCountry>()
                .Property(e => e.UpdatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<SMChargeCodePLCountry>()
                .Property(e => e.Version)
                .IsFixedLength();

            modelBuilder.Entity<SMChargeCodePLMethod>()
                .Property(e => e.ChargeMethod)
                .IsUnicode(false);

            modelBuilder.Entity<SMChargeCodePLMethod>()
                .Property(e => e.ChargeUOM)
                .IsUnicode(false);

            modelBuilder.Entity<SMChargeCodePLMethod>()
                .Property(e => e.Calculation)
                .IsUnicode(false);

            modelBuilder.Entity<SMChargeCodePLMethod>()
                .Property(e => e.CreatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<SMChargeCodePLMethod>()
                .Property(e => e.UpdatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<SMChargeCodePLMethod>()
                .Property(e => e.Status)
                .IsUnicode(false);

            modelBuilder.Entity<SMChargeCodeSubCategory>()
                .Property(e => e.SubCategoryCode)
                .IsUnicode(false);

            modelBuilder.Entity<SMChargeCodeSubCategory>()
                .Property(e => e.Status)
                .IsUnicode(false);

            modelBuilder.Entity<SMChargeCodeSubCategory>()
                .Property(e => e.CreatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<SMChargeCodeSubCategory>()
                .Property(e => e.UpdatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<SMChargeCodeSubCategory>()
                .Property(e => e.Version)
                .IsFixedLength();

            modelBuilder.Entity<SMChargeCodeSubCategoryPL>()
                .Property(e => e.Status)
                .IsUnicode(false);

            modelBuilder.Entity<SMChargeCodeSubCategoryPL>()
                .Property(e => e.CreatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<SMChargeCodeSubCategoryPL>()
                .Property(e => e.UpdatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<SMChargeCodeSubCategoryPL>()
                .Property(e => e.Version)
                .IsFixedLength();

            modelBuilder.Entity<SMChargeCodeUOM>()
                .Property(e => e.ChargeUOMCode)
                .IsUnicode(false);

            modelBuilder.Entity<SMChargeCodeUOM>()
                .Property(e => e.Status)
                .IsUnicode(false);

            modelBuilder.Entity<SMChargeCodeUOM>()
                .Property(e => e.CreatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<SMChargeCodeUOM>()
                .Property(e => e.UpdatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<SMChargeCodeUOM>()
                .Property(e => e.Version)
                .IsFixedLength();

            modelBuilder.Entity<SMChartofAccount>()
                .Property(e => e.GLCode)
                .IsUnicode(false);

            modelBuilder.Entity<SMChartofAccount>()
                .Property(e => e.AccountType)
                .IsUnicode(false);

            modelBuilder.Entity<SMChartofAccount>()
                .Property(e => e.Currency)
                .IsUnicode(false);

            modelBuilder.Entity<SMChartofAccount>()
                .Property(e => e.Level)
                .IsUnicode(false);

            modelBuilder.Entity<SMChartofAccount>()
                .Property(e => e.ChargeCode)
                .IsUnicode(false);

            modelBuilder.Entity<SMChartofAccount>()
                .Property(e => e.Status)
                .IsUnicode(false);

            modelBuilder.Entity<SMChartofAccount>()
                .Property(e => e.CreatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<SMChartofAccount>()
                .Property(e => e.UpdatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<SMChartofAccount>()
                .Property(e => e.Version)
                .IsFixedLength();

            modelBuilder.Entity<SMChartofAccount>()
                .Property(e => e.Dr)
                .IsUnicode(false);

            modelBuilder.Entity<SMCity>()
                .Property(e => e.CityCode)
                .IsUnicode(false);

            modelBuilder.Entity<SMCity>()
                .Property(e => e.CityPhone)
                .IsUnicode(false);

            modelBuilder.Entity<SMCity>()
                .Property(e => e.Status)
                .IsUnicode(false);

            modelBuilder.Entity<SMCity>()
                .Property(e => e.CreatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<SMCity>()
                .Property(e => e.UpdatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<SMCity>()
                .Property(e => e.Version)
                .IsFixedLength();

            modelBuilder.Entity<SMCity>()
                .Property(e => e.IATACode)
                .IsUnicode(false);

            modelBuilder.Entity<SMCity>()
                .Property(e => e.CALocCode)
                .IsUnicode(false);

            modelBuilder.Entity<SMCity>()
                .Property(e => e.UNCityCode)
                .IsUnicode(false);

            modelBuilder.Entity<SMCityPL>()
                .Property(e => e.ExportGateway)
                .IsUnicode(false);

            modelBuilder.Entity<SMCityPL>()
                .Property(e => e.ImportGateway)
                .IsUnicode(false);

            modelBuilder.Entity<SMCityPL>()
                .Property(e => e.Status)
                .IsUnicode(false);

            modelBuilder.Entity<SMCityPL>()
                .Property(e => e.CreatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<SMCityPL>()
                .Property(e => e.UpdatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<SMCityPL>()
                .Property(e => e.Version)
                .IsFixedLength();

            modelBuilder.Entity<SMCodeSetting>()
                .Property(e => e.CodeGroup)
                .IsUnicode(false);

            modelBuilder.Entity<SMCodeSetting>()
                .Property(e => e.Code)
                .IsUnicode(false);

            modelBuilder.Entity<SMCodeSetting>()
                .Property(e => e.Status)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<SMControlGroup>()
                .Property(e => e.FieldType)
                .IsUnicode(false);

            modelBuilder.Entity<SMControlGroup>()
                .Property(e => e.Status)
                .IsUnicode(false);

            modelBuilder.Entity<SMControlGroup>()
                .Property(e => e.CreatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<SMControlGroup>()
                .Property(e => e.UpdatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<SMControlGroup>()
                .Property(e => e.ReconfirmBy)
                .IsUnicode(false);

            modelBuilder.Entity<SMControlGroupDetail>()
                .Property(e => e.CreatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<SMControlGroupDetail>()
                .Property(e => e.UpdatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<SMControlGroupDetail>()
                .Property(e => e.IssueStation)
                .IsUnicode(false);

            modelBuilder.Entity<SMControlGroupDetail>()
                .Property(e => e.BillTo)
                .IsUnicode(false);

            modelBuilder.Entity<SMControlGroupDetail>()
                .Property(e => e.ChargeCode)
                .IsUnicode(false);

            modelBuilder.Entity<SMCountry>()
                .Property(e => e.CountryCode)
                .IsUnicode(false);

            modelBuilder.Entity<SMCountry>()
                .Property(e => e.PrefixNumber)
                .IsUnicode(false);

            modelBuilder.Entity<SMCountry>()
                .Property(e => e.Status)
                .IsUnicode(false);

            modelBuilder.Entity<SMCountry>()
                .Property(e => e.CreatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<SMCountry>()
                .Property(e => e.UpdatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<SMCountry>()
                .Property(e => e.Version)
                .IsFixedLength();

            modelBuilder.Entity<SMCountry>()
                .Property(e => e.Currency)
                .IsUnicode(false);

            modelBuilder.Entity<SMCountryHual>()
                .Property(e => e.HualMethod)
                .IsUnicode(false);

            modelBuilder.Entity<SMCountryHual>()
                .Property(e => e.Status)
                .IsUnicode(false);

            modelBuilder.Entity<SMCountryHual>()
                .Property(e => e.CreatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<SMCountryHual>()
                .Property(e => e.UpdatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<SMCountryHual>()
                .Property(e => e.Version)
                .IsFixedLength();

            modelBuilder.Entity<SMCreditTerm>()
                .Property(e => e.CreditTermCode)
                .IsUnicode(false);

            modelBuilder.Entity<SMCreditTerm>()
                .Property(e => e.Status)
                .IsUnicode(false);

            modelBuilder.Entity<SMCreditTerm>()
                .Property(e => e.CreatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<SMCreditTerm>()
                .Property(e => e.UpdatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<SMCreditTerm>()
                .Property(e => e.Version)
                .IsFixedLength();

            modelBuilder.Entity<SMCreditTermDIMCountry>()
                .Property(e => e.Status)
                .IsUnicode(false);

            modelBuilder.Entity<SMCreditTermDIMCountry>()
                .Property(e => e.CreatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<SMCreditTermDIMCountry>()
                .Property(e => e.UpdatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<SMCreditTermDIMCountry>()
                .Property(e => e.Version)
                .IsFixedLength();

            modelBuilder.Entity<SMCreditTermPL>()
                .Property(e => e.Status)
                .IsUnicode(false);

            modelBuilder.Entity<SMCreditTermPL>()
                .Property(e => e.CreatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<SMCreditTermPL>()
                .Property(e => e.UpdatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<SMCreditTermPL>()
                .Property(e => e.Version)
                .IsFixedLength();

            modelBuilder.Entity<SMCurrency>()
                .Property(e => e.CurrencyCode)
                .IsUnicode(false);

            modelBuilder.Entity<SMCurrency>()
                .Property(e => e.Decimal)
                .HasPrecision(18, 0);

            modelBuilder.Entity<SMCurrency>()
                .Property(e => e.RoundUnit)
                .HasPrecision(18, 0);

            modelBuilder.Entity<SMCurrency>()
                .Property(e => e.MinAmount)
                .HasPrecision(18, 0);

            modelBuilder.Entity<SMCurrency>()
                .Property(e => e.RoundUp)
                .HasPrecision(18, 0);

            modelBuilder.Entity<SMCurrency>()
                .Property(e => e.MinRoundUnit)
                .IsUnicode(false);

            modelBuilder.Entity<SMCurrency>()
                .Property(e => e.AMTDecimal)
                .IsUnicode(false);

            modelBuilder.Entity<SMCurrency>()
                .Property(e => e.VATDecimal)
                .IsUnicode(false);

            modelBuilder.Entity<SMCurrency>()
                .Property(e => e.Status)
                .IsUnicode(false);

            modelBuilder.Entity<SMCurrency>()
                .Property(e => e.CreatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<SMCurrency>()
                .Property(e => e.UpdatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<SMCurrency>()
                .Property(e => e.Version)
                .IsFixedLength();

            modelBuilder.Entity<SMCustomer>()
                .Property(e => e.StationID)
                .IsUnicode(false);

            modelBuilder.Entity<SMCustomer>()
                .Property(e => e.CustomerCode)
                .IsUnicode(false);

            modelBuilder.Entity<SMCustomer>()
                .Property(e => e.GlobalCode)
                .IsUnicode(false);

            modelBuilder.Entity<SMCustomer>()
                .Property(e => e.Phone)
                .IsUnicode(false);

            modelBuilder.Entity<SMCustomer>()
                .Property(e => e.PhoneExt)
                .IsUnicode(false);

            modelBuilder.Entity<SMCustomer>()
                .Property(e => e.Fax)
                .IsUnicode(false);

            modelBuilder.Entity<SMCustomer>()
                .Property(e => e.FaxExt)
                .IsUnicode(false);

            modelBuilder.Entity<SMCustomer>()
                .Property(e => e.Zip)
                .IsUnicode(false);

            modelBuilder.Entity<SMCustomer>()
                .Property(e => e.TradeTerm)
                .IsUnicode(false);

            modelBuilder.Entity<SMCustomer>()
                .Property(e => e.ShipmentType)
                .IsUnicode(false);

            modelBuilder.Entity<SMCustomer>()
                .Property(e => e.ServiceType)
                .IsUnicode(false);

            modelBuilder.Entity<SMCustomer>()
                .Property(e => e.AirMove)
                .IsUnicode(false);

            modelBuilder.Entity<SMCustomer>()
                .Property(e => e.OceanMove)
                .IsUnicode(false);

            modelBuilder.Entity<SMCustomer>()
                .Property(e => e.AirLineCode)
                .IsUnicode(false);

            modelBuilder.Entity<SMCustomer>()
                .Property(e => e.TPLetterCode)
                .IsUnicode(false);

            modelBuilder.Entity<SMCustomer>()
                .Property(e => e.VAT)
                .IsUnicode(false);

            modelBuilder.Entity<SMCustomer>()
                .Property(e => e.Status)
                .IsUnicode(false);

            modelBuilder.Entity<SMCustomer>()
                .Property(e => e.CreatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<SMCustomer>()
                .Property(e => e.UpdatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<SMCustomer>()
                .Property(e => e.Version)
                .IsFixedLength();

            modelBuilder.Entity<SMCustomer>()
                .Property(e => e.CreatedStationID)
                .IsUnicode(false);

            modelBuilder.Entity<SMCustomer>()
                .Property(e => e.VendorPostingGLCode)
                .IsUnicode(false);

            modelBuilder.Entity<SMCustomer>()
                .Property(e => e.CustType)
                .IsUnicode(false);

            modelBuilder.Entity<SMCustomer>()
                .Property(e => e.CapitalAmount)
                .IsUnicode(false);

            modelBuilder.Entity<SMCustomer>()
                .Property(e => e.EstablishedDate)
                .IsUnicode(false);

            modelBuilder.Entity<SMCustomer>()
                .Property(e => e.AnnualRevenue)
                .IsUnicode(false);

            modelBuilder.Entity<SMCustomer>()
                .Property(e => e.CustomerLevel)
                .IsUnicode(false);

            modelBuilder.Entity<SMCustomer>()
                .HasMany(e => e.SMCustomerContact)
                .WithRequired(e => e.SMCustomer)
                .HasForeignKey(e => e.CustomerID)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<SMCustomer>()
                .HasMany(e => e.SMCustomerPL)
                .WithRequired(e => e.SMCustomer)
                .HasForeignKey(e => e.CustomerID)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<SMCustomer>()
                .HasMany(e => e.SMCustomerRegister)
                .WithRequired(e => e.SMCustomer)
                .HasForeignKey(e => e.CustomerID)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<SMCustomerAddress>()
                .Property(e => e.FormType)
                .IsUnicode(false);

            modelBuilder.Entity<SMCustomerAddress>()
                .Property(e => e.Mode)
                .IsUnicode(false);

            modelBuilder.Entity<SMCustomerAddress>()
                .Property(e => e.CustomerPhone)
                .IsUnicode(false);

            modelBuilder.Entity<SMCustomerAddress>()
                .Property(e => e.CustomerFax)
                .IsUnicode(false);

            modelBuilder.Entity<SMCustomerAddress>()
                .Property(e => e.CustomerEmail)
                .IsUnicode(false);

            modelBuilder.Entity<SMCustomerAddress>()
                .Property(e => e.Status)
                .IsUnicode(false);

            modelBuilder.Entity<SMCustomerAddress>()
                .Property(e => e.CreatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<SMCustomerAddress>()
                .Property(e => e.UpdatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<SMCustomerAddress>()
                .Property(e => e.ZIP)
                .IsUnicode(false);

            modelBuilder.Entity<SMCustomerAddress>()
                .Property(e => e.AreaCode)
                .IsUnicode(false);

            modelBuilder.Entity<SMCustomerAddressFormType>()
                .Property(e => e.FormTypeCode)
                .IsUnicode(false);

            modelBuilder.Entity<SMCustomerAddressFormType>()
                .Property(e => e.Status)
                .IsUnicode(false);

            modelBuilder.Entity<SMCustomerAddressMode>()
                .Property(e => e.Status)
                .IsUnicode(false);

            modelBuilder.Entity<SMCustomerAddressMode>()
                .Property(e => e.CreatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<SMCustomerAddressMode>()
                .Property(e => e.UpdatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<SMCustomerContact>()
                .Property(e => e.Tel_H)
                .IsUnicode(false);

            modelBuilder.Entity<SMCustomerContact>()
                .Property(e => e.Tel_M)
                .IsUnicode(false);

            modelBuilder.Entity<SMCustomerContact>()
                .Property(e => e.Tel_O)
                .IsUnicode(false);

            modelBuilder.Entity<SMCustomerContact>()
                .Property(e => e.Tel_OExt)
                .IsUnicode(false);

            modelBuilder.Entity<SMCustomerContact>()
                .Property(e => e.Fax)
                .IsUnicode(false);

            modelBuilder.Entity<SMCustomerContact>()
                .Property(e => e.Status)
                .IsUnicode(false);

            modelBuilder.Entity<SMCustomerContact>()
                .Property(e => e.CreatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<SMCustomerContact>()
                .Property(e => e.UpdatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<SMCustomerContact>()
                .Property(e => e.Version)
                .IsFixedLength();

            modelBuilder.Entity<SMCustomerContactPL>()
                .Property(e => e.Status)
                .IsUnicode(false);

            modelBuilder.Entity<SMCustomerContactPL>()
                .Property(e => e.CreatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<SMCustomerContactPL>()
                .Property(e => e.UpdatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<SMCustomerExtra>()
                .Property(e => e.GlobalCode)
                .IsUnicode(false);

            modelBuilder.Entity<SMCustomerExtra>()
                .Property(e => e.ExtraType)
                .IsUnicode(false);

            modelBuilder.Entity<SMCustomerExtra>()
                .Property(e => e.FieldName)
                .IsUnicode(false);

            modelBuilder.Entity<SMCustomerExtra>()
                .Property(e => e.Mandatory)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<SMCustomerExtra>()
                .Property(e => e.Status)
                .IsUnicode(false);

            modelBuilder.Entity<SMCustomerExtra>()
                .Property(e => e.ProductLine)
                .IsUnicode(false);

            modelBuilder.Entity<SMCustomerExtra>()
                .Property(e => e.ModeCode)
                .IsUnicode(false);

            modelBuilder.Entity<SMCustomerExtra>()
                .Property(e => e.CreatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<SMCustomerExtra>()
                .Property(e => e.UpdatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<SMCustomerFormMail>()
                .Property(e => e.UserType)
                .IsUnicode(false);

            modelBuilder.Entity<SMCustomerFormMail>()
                .Property(e => e.UserID)
                .IsUnicode(false);

            modelBuilder.Entity<SMCustomerFormMail>()
                .Property(e => e.Status)
                .IsUnicode(false);

            modelBuilder.Entity<SMCustomerFormMail>()
                .Property(e => e.CreatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<SMCustomerFormMail>()
                .Property(e => e.UpdatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<SMCustomerFormMail>()
                .Property(e => e.SID)
                .IsUnicode(false);

            modelBuilder.Entity<SMCustomerFormPlace>()
                .Property(e => e.FromPlaceType)
                .IsUnicode(false);

            modelBuilder.Entity<SMCustomerFormPlace>()
                .Property(e => e.ToPlaceType)
                .IsUnicode(false);

            modelBuilder.Entity<SMCustomerForms>()
                .Property(e => e.eFormName)
                .IsUnicode(false);

            modelBuilder.Entity<SMCustomerForms>()
                .Property(e => e.Mode)
                .IsUnicode(false);

            modelBuilder.Entity<SMCustomerForms>()
                .Property(e => e.VIA)
                .IsUnicode(false);

            modelBuilder.Entity<SMCustomerForms>()
                .Property(e => e.FileFormat)
                .IsUnicode(false);

            modelBuilder.Entity<SMCustomerForms>()
                .Property(e => e.Status)
                .IsUnicode(false);

            modelBuilder.Entity<SMCustomerForms>()
                .Property(e => e.CreatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<SMCustomerForms>()
                .Property(e => e.UpdatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<SMCustomerForms>()
                .Property(e => e.Version)
                .IsFixedLength();

            modelBuilder.Entity<SMCustomerHDI>()
                .Property(e => e.Mode)
                .IsUnicode(false);

            modelBuilder.Entity<SMCustomerHDI>()
                .Property(e => e.Forms)
                .IsUnicode(false);

            modelBuilder.Entity<SMCustomerHDI>()
                .Property(e => e.Status)
                .IsUnicode(false);

            modelBuilder.Entity<SMCustomerHDI>()
                .Property(e => e.CreatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<SMCustomerHDI>()
                .Property(e => e.UpdatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<SMCustomerHDI>()
                .Property(e => e.Version)
                .IsFixedLength();

            modelBuilder.Entity<SMCustomerHDI>()
                .Property(e => e.HDIType)
                .IsUnicode(false);

            modelBuilder.Entity<SMCustomerHDIPlace>()
                .Property(e => e.FromPlaceType)
                .IsUnicode(false);

            modelBuilder.Entity<SMCustomerHDIPlace>()
                .Property(e => e.ToPlaceType)
                .IsUnicode(false);

            modelBuilder.Entity<SMCustomerHDIPlace>()
                .Property(e => e.CreatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<SMCustomerHDIPlace>()
                .Property(e => e.UpdatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<SMCustomerLogin>()
                .Property(e => e.MapType)
                .IsUnicode(false);

            modelBuilder.Entity<SMCustomerLogin>()
                .Property(e => e.MapSeq)
                .IsUnicode(false);

            modelBuilder.Entity<SMCustomerLogin>()
                .Property(e => e.UserID)
                .IsUnicode(false);

            modelBuilder.Entity<SMCustomerLogin>()
                .Property(e => e.CreatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<SMCustomerLogin>()
                .Property(e => e.UpdatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<SMCustomerMap>()
                .Property(e => e.MapType)
                .IsUnicode(false);

            modelBuilder.Entity<SMCustomerMap>()
                .Property(e => e.StationID)
                .IsUnicode(false);

            modelBuilder.Entity<SMCustomerMap>()
                .Property(e => e.CreatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<SMCustomerMap>()
                .Property(e => e.UpdatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<SMCustomerMap>()
                .Property(e => e.Status)
                .IsUnicode(false);

            modelBuilder.Entity<SMCustomerMap>()
                .Property(e => e.LoginID)
                .IsUnicode(false);

            modelBuilder.Entity<SMCustomerPL>()
                .Property(e => e.Status)
                .IsUnicode(false);

            modelBuilder.Entity<SMCustomerPL>()
                .Property(e => e.CreatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<SMCustomerPL>()
                .Property(e => e.UpdatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<SMCustomerPL>()
                .Property(e => e.Version)
                .IsFixedLength();

            modelBuilder.Entity<SMCustomerPL>()
                .Property(e => e.Language)
                .IsUnicode(false);

            modelBuilder.Entity<SMCustomerPL>()
                .Property(e => e.NeedBookConfirm)
                .IsUnicode(false);

            modelBuilder.Entity<SMCustomerPL>()
                .Property(e => e.POA)
                .IsUnicode(false);

            modelBuilder.Entity<SMCustomerPreAlertTempl>()
                .Property(e => e.AWBType)
                .IsUnicode(false);

            modelBuilder.Entity<SMCustomerPreAlertTempl>()
                .Property(e => e.NeedMaster)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<SMCustomerPreAlertTempl>()
                .Property(e => e.NeedHouse)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<SMCustomerPreAlertTempl>()
                .Property(e => e.NeedDebitNote)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<SMCustomerPreAlertTempl>()
                .Property(e => e.NeedArrivalNotice)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<SMCustomerPreAlertTempl>()
                .Property(e => e.Status)
                .IsUnicode(false);

            modelBuilder.Entity<SMCustomerPreAlertTempl>()
                .Property(e => e.CreatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<SMCustomerPreAlertTempl>()
                .Property(e => e.UpdatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<SMCustomerPreAlertTempl>()
                .Property(e => e.Version)
                .IsFixedLength();

            modelBuilder.Entity<SMCustomerPreAlertTempl>()
                .Property(e => e.NeedCommInv)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<SMCustomerPreAlertTempl>()
                .Property(e => e.NeedPackingList)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<SMCustomerPreAlertTempl>()
                .Property(e => e.SendCustomer)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<SMCustomerPreAlertTempl>()
                .Property(e => e.SendShipper)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<SMCustomerPreAlertTempl>()
                .Property(e => e.SendCNEE)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<SMCustomerPreAlertTempl>()
                .Property(e => e.SendNotify)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<SMCustomerPreAlertTempl>()
                .Property(e => e.SendBBAgent)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<SMCustomerPreAlertTempl>()
                .Property(e => e.SendThirtyParty)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<SMCustomerPreAlertTempl>()
                .Property(e => e.SendFinalImport)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<SMCustomerPreAlertTempl>()
                .Property(e => e.SendDestAgent)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<SMCustomerPreAlertTemplMap>()
                .Property(e => e.AWBType)
                .IsUnicode(false);

            modelBuilder.Entity<SMCustomerPreAlertTemplMap>()
                .Property(e => e.AliasCode)
                .IsUnicode(false);

            modelBuilder.Entity<SMCustomerPreAlertTemplMap>()
                .Property(e => e.MapName)
                .IsUnicode(false);

            modelBuilder.Entity<SMCustomerPreAlertTemplMap>()
                .Property(e => e.ShowHeader)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<SMCustomerPreAlertTemplMap>()
                .Property(e => e.ShowContent)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<SMCustomerPreAlertTemplMap>()
                .Property(e => e.Status)
                .IsUnicode(false);

            modelBuilder.Entity<SMCustomerPreAlertTemplMap>()
                .Property(e => e.CreatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<SMCustomerPreAlertTemplMap>()
                .Property(e => e.UpdatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<SMCustomerRegister>()
                .Property(e => e.RegisterCode)
                .IsUnicode(false);

            modelBuilder.Entity<SMCustomerRegister>()
                .Property(e => e.Status)
                .IsUnicode(false);

            modelBuilder.Entity<SMCustomerRegister>()
                .Property(e => e.CreatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<SMCustomerRegister>()
                .Property(e => e.UpdatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<SMCustomerRegister>()
                .Property(e => e.Version)
                .IsFixedLength();

            modelBuilder.Entity<SMDepartment>()
                .Property(e => e.DepartmentCode)
                .IsUnicode(false);

            modelBuilder.Entity<SMDepartment>()
                .Property(e => e.StationID)
                .IsUnicode(false);

            modelBuilder.Entity<SMDepartment>()
                .Property(e => e.Status)
                .IsUnicode(false);

            modelBuilder.Entity<SMDepartment>()
                .Property(e => e.CreatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<SMDepartment>()
                .Property(e => e.UpdatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<SMDepartment>()
                .Property(e => e.Version)
                .IsFixedLength();

            modelBuilder.Entity<SMDepartment>()
                .Property(e => e.CRPDepartmentCode)
                .IsUnicode(false);

            modelBuilder.Entity<SMDepartmentCRP>()
                .Property(e => e.DepartmentCode)
                .IsUnicode(false);

            modelBuilder.Entity<SMDepartmentCRP>()
                .Property(e => e.StationID)
                .IsUnicode(false);

            modelBuilder.Entity<SMDepartmentCRP>()
                .Property(e => e.Status)
                .IsUnicode(false);

            modelBuilder.Entity<SMDepartmentCRP>()
                .Property(e => e.CreatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<SMDepartmentCRP>()
                .Property(e => e.UpdatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<SMDepartmentCRP>()
                .Property(e => e.Version)
                .IsFixedLength();

            modelBuilder.Entity<SMDIMCountry>()
                .Property(e => e.DimCountryCode)
                .IsUnicode(false);

            modelBuilder.Entity<SMDIMCountry>()
                .Property(e => e.Status)
                .IsUnicode(false);

            modelBuilder.Entity<SMDIMCountry>()
                .Property(e => e.CreatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<SMDIMCountry>()
                .Property(e => e.UpdatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<SMDIMCountry>()
                .Property(e => e.Version)
                .IsFixedLength();

            modelBuilder.Entity<SMDIMDistrict>()
                .Property(e => e.DimDistrictCode)
                .IsUnicode(false);

            modelBuilder.Entity<SMDIMDistrict>()
                .Property(e => e.Status)
                .IsUnicode(false);

            modelBuilder.Entity<SMDIMDistrict>()
                .Property(e => e.CreatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<SMDIMDistrict>()
                .Property(e => e.UpdatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<SMDIMDistrict>()
                .Property(e => e.Version)
                .IsFixedLength();

            modelBuilder.Entity<SMDIMRegion>()
                .Property(e => e.DimRegionCode)
                .IsUnicode(false);

            modelBuilder.Entity<SMDIMRegion>()
                .Property(e => e.Status)
                .IsUnicode(false);

            modelBuilder.Entity<SMDIMRegion>()
                .Property(e => e.CreatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<SMDIMRegion>()
                .Property(e => e.UpdatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<SMDIMRegion>()
                .Property(e => e.Version)
                .IsFixedLength();

            modelBuilder.Entity<SMDIMRegion>()
                .Property(e => e.IsBlockShpt)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<SMDIMRegion>()
                .HasMany(e => e.SMDIMCountry)
                .WithRequired(e => e.SMDIMRegion)
                .HasForeignKey(e => e.DimRegionID)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<SMDIMRegion>()
                .HasMany(e => e.SMDIMDistrict)
                .WithRequired(e => e.SMDIMRegion)
                .HasForeignKey(e => e.DimRegionID)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<SMDistrict>()
                .Property(e => e.DistrictCode)
                .IsUnicode(false);

            modelBuilder.Entity<SMDistrict>()
                .Property(e => e.Status)
                .IsUnicode(false);

            modelBuilder.Entity<SMDistrict>()
                .Property(e => e.CreatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<SMDistrict>()
                .Property(e => e.UpdatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<SMDistrict>()
                .Property(e => e.Version)
                .IsFixedLength();

            modelBuilder.Entity<SMDivision>()
                .Property(e => e.DivisionCode)
                .IsUnicode(false);

            modelBuilder.Entity<SMDivision>()
                .Property(e => e.Status)
                .IsUnicode(false);

            modelBuilder.Entity<SMDivision>()
                .Property(e => e.CreatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<SMDivision>()
                .Property(e => e.UpdatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<SMDivision>()
                .Property(e => e.Version)
                .IsFixedLength();

            modelBuilder.Entity<SMDTMode>()
                .Property(e => e.ModeCode)
                .IsUnicode(false);

            modelBuilder.Entity<SMDTMode>()
                .Property(e => e.Status)
                .IsUnicode(false);

            modelBuilder.Entity<SMDTMode>()
                .Property(e => e.CreatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<SMDTMode>()
                .Property(e => e.UpdatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<SMDTMode>()
                .Property(e => e.Version)
                .IsFixedLength();

            modelBuilder.Entity<SMeAWBAddressSetting>()
                .Property(e => e.FromPlaceType)
                .IsUnicode(false);

            modelBuilder.Entity<SMeAWBAddressSetting>()
                .Property(e => e.ToPlaceType)
                .IsUnicode(false);

            modelBuilder.Entity<SMeAWBAddressSetting>()
                .Property(e => e.FromStation)
                .IsUnicode(false);

            modelBuilder.Entity<SMeAWBAddressSetting>()
                .Property(e => e.ToStation)
                .IsUnicode(false);

            modelBuilder.Entity<SMeAWBAddressSetting>()
                .Property(e => e.RelateAirLine)
                .IsUnicode(false);

            modelBuilder.Entity<SMeAWBAddressSetting>()
                .Property(e => e.Status)
                .IsUnicode(false);

            modelBuilder.Entity<SMeAWBAddressSetting>()
                .Property(e => e.CreatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<SMeAWBAddressSetting>()
                .Property(e => e.UpdatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<SMEDI>()
                .Property(e => e.StationID)
                .IsUnicode(false);

            modelBuilder.Entity<SMEDI>()
                .Property(e => e.CreatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<SMEDI>()
                .Property(e => e.UpdatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<SMEDI>()
                .Property(e => e.UpdatedDate)
                .IsFixedLength();

            modelBuilder.Entity<SMEDI>()
                .Property(e => e.Remark)
                .IsUnicode(false);

            modelBuilder.Entity<SMEDIChargeRuleCarrier>()
                .Property(e => e.AilLineCode)
                .IsUnicode(false);

            modelBuilder.Entity<SMEDIChargeRuleCarrier>()
                .Property(e => e.Status)
                .IsUnicode(false);

            modelBuilder.Entity<SMEDIChargeRuleCarrier>()
                .Property(e => e.CreatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<SMEDIChargeRuleCarrier>()
                .Property(e => e.UpdatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<SMEDIChargeRuleCode>()
                .Property(e => e.ChargeCode)
                .IsUnicode(false);

            modelBuilder.Entity<SMEDIChargeRuleCode>()
                .Property(e => e.DIMChargeCode)
                .IsUnicode(false);

            modelBuilder.Entity<SMEDIChargeRuleCode>()
                .Property(e => e.Status)
                .IsUnicode(false);

            modelBuilder.Entity<SMEDIChargeRuleCode>()
                .Property(e => e.CreatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<SMEDIChargeRuleCode>()
                .Property(e => e.UpdatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<SMEDIType>()
                .Property(e => e.EDIType)
                .IsUnicode(false);

            modelBuilder.Entity<SMEDIType>()
                .Property(e => e.CreatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<SMEDIType>()
                .Property(e => e.UpdatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<SMFieldSetting>()
                .Property(e => e.FieldType)
                .IsUnicode(false);

            modelBuilder.Entity<SMFieldSetting>()
                .Property(e => e.FieldName)
                .IsUnicode(false);

            modelBuilder.Entity<SMFieldSetting>()
                .Property(e => e.CreatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<SMFieldSetting>()
                .Property(e => e.UpdatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<SMGlobalRate>()
                .Property(e => e.FromCurrency)
                .IsUnicode(false);

            modelBuilder.Entity<SMGlobalRate>()
                .Property(e => e.ToCurrency)
                .IsUnicode(false);

            modelBuilder.Entity<SMGlobalRate>()
                .Property(e => e.ConvertRate)
                .HasPrecision(18, 4);

            modelBuilder.Entity<SMGRegion>()
                .Property(e => e.RegionCode)
                .IsUnicode(false);

            modelBuilder.Entity<SMGRegion>()
                .Property(e => e.Status)
                .IsUnicode(false);

            modelBuilder.Entity<SMGRegion>()
                .Property(e => e.CreatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<SMGRegion>()
                .Property(e => e.UpdatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<SMGRegion>()
                .Property(e => e.Version)
                .IsFixedLength();

            modelBuilder.Entity<SMGroupMilestone>()
                .Property(e => e.InputType)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<SMGroupMilestone>()
                .Property(e => e.Description)
                .IsFixedLength();

            modelBuilder.Entity<SMGroupMilestone>()
                .Property(e => e.Usage)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<SMGroupRoleSetting>()
                .Property(e => e.RoleNo)
                .IsUnicode(false);

            modelBuilder.Entity<SMGroupRoleSetting>()
                .Property(e => e.RoleType)
                .IsUnicode(false);

            modelBuilder.Entity<SMGroupRoleSetting>()
                .Property(e => e.UserID)
                .IsUnicode(false);

            modelBuilder.Entity<SMGroupRoleSetting>()
                .Property(e => e.ApplyMode)
                .IsUnicode(false);

            modelBuilder.Entity<SMGroupRoleSetting>()
                .Property(e => e.Status)
                .IsUnicode(false);

            modelBuilder.Entity<SMGroupRoleSetting>()
                .Property(e => e.CreatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<SMGroupRoleSetting>()
                .Property(e => e.UpdatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<SMHubBranchStationPL>()
                .Property(e => e.StationID)
                .IsUnicode(false);

            modelBuilder.Entity<SMHubBranchStationPL>()
                .Property(e => e.BranchID)
                .IsUnicode(false);

            modelBuilder.Entity<SMHubBranchStationPL>()
                .Property(e => e.CreatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<SMHubBranchStationPL>()
                .Property(e => e.UpdatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<SMHubBranchStationPL>()
                .Property(e => e.Version)
                .IsFixedLength();

            modelBuilder.Entity<SMHubBranchStationPL>()
                .Property(e => e.Status)
                .IsUnicode(false);

            modelBuilder.Entity<SMIndustrialCategory>()
                .Property(e => e.IndustryCode)
                .IsUnicode(false);

            modelBuilder.Entity<SMIndustrialCategory>()
                .Property(e => e.Status)
                .IsUnicode(false);

            modelBuilder.Entity<SMIndustrialCategory>()
                .Property(e => e.CreatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<SMIndustrialCategory>()
                .Property(e => e.UpdatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<SMIndustrialCategory>()
                .Property(e => e.Version)
                .IsFixedLength();

            modelBuilder.Entity<SMIndustrialGroup>()
                .Property(e => e.IndustryGroupCode)
                .IsUnicode(false);

            modelBuilder.Entity<SMIndustrialGroup>()
                .Property(e => e.Status)
                .IsUnicode(false);

            modelBuilder.Entity<SMIndustrialGroup>()
                .Property(e => e.CreatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<SMIndustrialGroup>()
                .Property(e => e.UpdatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<SMIndustrialGroup>()
                .Property(e => e.Version)
                .IsFixedLength();

            modelBuilder.Entity<SMISFData>()
                .Property(e => e.Status)
                .IsUnicode(false);

            modelBuilder.Entity<SMISFData>()
                .Property(e => e.CreatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<SMISFData>()
                .Property(e => e.UpdatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<SMLeadSource>()
                .Property(e => e.LeadSourceCode)
                .IsUnicode(false);

            modelBuilder.Entity<SMLeadSource>()
                .Property(e => e.Status)
                .IsUnicode(false);

            modelBuilder.Entity<SMLeadSource>()
                .Property(e => e.CreatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<SMLeadSource>()
                .Property(e => e.UpdatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<SMLeadSourceGroup>()
                .Property(e => e.LeadSourceGroupCode)
                .IsUnicode(false);

            modelBuilder.Entity<SMLeadSourceGroup>()
                .Property(e => e.Status)
                .IsUnicode(false);

            modelBuilder.Entity<SMLeadSourceGroup>()
                .Property(e => e.CreatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<SMLeadSourceGroup>()
                .Property(e => e.UpdatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<SMLELevel1>()
                .Property(e => e.Level1Code)
                .IsUnicode(false);

            modelBuilder.Entity<SMLELevel1>()
                .Property(e => e.Status)
                .IsUnicode(false);

            modelBuilder.Entity<SMLELevel1>()
                .Property(e => e.CreatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<SMLELevel1>()
                .Property(e => e.UpdatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<SMLELevel1>()
                .Property(e => e.Version)
                .IsFixedLength();

            modelBuilder.Entity<SMLELevel2>()
                .Property(e => e.Level2Code)
                .IsUnicode(false);

            modelBuilder.Entity<SMLELevel2>()
                .Property(e => e.Status)
                .IsUnicode(false);

            modelBuilder.Entity<SMLELevel2>()
                .Property(e => e.CreatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<SMLELevel2>()
                .Property(e => e.UpdatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<SMLELevel2>()
                .Property(e => e.Version)
                .IsFixedLength();

            modelBuilder.Entity<SMLERegion>()
                .Property(e => e.RegionCode)
                .IsUnicode(false);

            modelBuilder.Entity<SMLERegion>()
                .Property(e => e.Status)
                .IsUnicode(false);

            modelBuilder.Entity<SMLERegion>()
                .Property(e => e.CreatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<SMLERegion>()
                .Property(e => e.UpdatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<SMLERegion>()
                .Property(e => e.Version)
                .IsFixedLength();

            modelBuilder.Entity<SMLocalChargeUnit>()
                .Property(e => e.UnitCode)
                .IsUnicode(false);

            modelBuilder.Entity<SMLocalChargeUnit>()
                .Property(e => e.UnitName)
                .IsUnicode(false);

            modelBuilder.Entity<SMMailSetting>()
                .Property(e => e.Title)
                .IsUnicode(false);

            modelBuilder.Entity<SMMailSetting>()
                .Property(e => e.SMTPServer)
                .IsUnicode(false);

            modelBuilder.Entity<SMMailSetting>()
                .Property(e => e.Encode)
                .IsUnicode(false);

            modelBuilder.Entity<SMMailSetting>()
                .Property(e => e.Account)
                .IsUnicode(false);

            modelBuilder.Entity<SMMailSetting>()
                .Property(e => e.Password)
                .IsUnicode(false);

            modelBuilder.Entity<SMMailSetting>()
                .Property(e => e.AccountMail)
                .IsUnicode(false);

            modelBuilder.Entity<SMMailSetting>()
                .Property(e => e.DisplayName)
                .IsUnicode(false);

            modelBuilder.Entity<SMMailSetting>()
                .Property(e => e.Status)
                .IsUnicode(false);

            modelBuilder.Entity<SMMailSetting>()
                .Property(e => e.CreatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<SMMailSetting>()
                .Property(e => e.UpdatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<SMMailSetting>()
                .Property(e => e.Version)
                .IsFixedLength();

            modelBuilder.Entity<SMMilestone>()
                .Property(e => e.Mode)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<SMMilestone>()
                .Property(e => e.InputType)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<SMMilestone>()
                .Property(e => e.EntryControl)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<SMMilestone>()
                .Property(e => e.Description)
                .IsFixedLength();

            modelBuilder.Entity<SMMilestone>()
                .Property(e => e.Status)
                .IsUnicode(false);

            modelBuilder.Entity<SMMilestone>()
                .Property(e => e.IMPStatus)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<SMMilestone>()
                .Property(e => e.ViewType)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<SMMilestone>()
                .Property(e => e.EstAct)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<SMMilestoneDetail>()
                .Property(e => e.StationID)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<SMMilestoneDetail>()
                .Property(e => e.ParentkeyValue)
                .IsUnicode(false);

            modelBuilder.Entity<SMMilestoneDetail>()
                .Property(e => e.ContainerKey)
                .IsUnicode(false);

            modelBuilder.Entity<SMMilestoneDetail>()
                .Property(e => e.POKey)
                .IsUnicode(false);

            modelBuilder.Entity<SMMilestoneDetail>()
                .Property(e => e.SOKey)
                .IsUnicode(false);

            modelBuilder.Entity<SMMilestoneDetail>()
                .Property(e => e.ExtraValue1)
                .IsUnicode(false);

            modelBuilder.Entity<SMMilestoneDetail>()
                .Property(e => e.ExtraValue2)
                .IsUnicode(false);

            modelBuilder.Entity<SMMilestoneDetail>()
                .Property(e => e.CreatedBy)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<SMMilestoneGroup>()
                .Property(e => e.CreatedBy)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<SMMilestoneGroup>()
                .Property(e => e.UpdatedBy)
                .IsFixedLength();

            modelBuilder.Entity<SMMilestoneGroup>()
                .Property(e => e.GlobalCode)
                .IsUnicode(false);

            modelBuilder.Entity<SMMilestoneGroup>()
                .Property(e => e.MoveCode)
                .IsUnicode(false);

            modelBuilder.Entity<SMMilestoneGroup>()
                .Property(e => e.SHPTTYPE)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<SMMilestoneHistory>()
                .Property(e => e.StationID)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<SMMilestoneHistory>()
                .Property(e => e.keyValue)
                .IsUnicode(false);

            modelBuilder.Entity<SMMilestoneHistory>()
                .Property(e => e.ApprovedBy)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<SMMilestoneHistory>()
                .Property(e => e.ExtraValue1)
                .IsUnicode(false);

            modelBuilder.Entity<SMMilestoneHistory>()
                .Property(e => e.ExtraValue2)
                .IsUnicode(false);

            modelBuilder.Entity<SMMilestoneHistory>()
                .Property(e => e.CreatedBy)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<SMMode>()
                .Property(e => e.ModeCode)
                .IsUnicode(false);

            modelBuilder.Entity<SMMode>()
                .Property(e => e.Status)
                .IsUnicode(false);

            modelBuilder.Entity<SMMode>()
                .Property(e => e.CreatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<SMMode>()
                .Property(e => e.UpdatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<SMMode>()
                .Property(e => e.Version)
                .IsFixedLength();

            modelBuilder.Entity<SMMove>()
                .Property(e => e.MoveCode)
                .IsUnicode(false);

            modelBuilder.Entity<SMMove>()
                .Property(e => e.MoveType)
                .IsUnicode(false);

            modelBuilder.Entity<SMMove>()
                .Property(e => e.Status)
                .IsUnicode(false);

            modelBuilder.Entity<SMMove>()
                .Property(e => e.CreatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<SMMove>()
                .Property(e => e.UpdatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<SMMove>()
                .Property(e => e.Version)
                .IsFixedLength();

            modelBuilder.Entity<SMOceanCarrier>()
                .Property(e => e.StationID)
                .IsUnicode(false);

            modelBuilder.Entity<SMOceanCarrier>()
                .Property(e => e.OceanCarrierCode)
                .IsUnicode(false);

            modelBuilder.Entity<SMOceanCarrier>()
                .Property(e => e.OceanCarrierPhone)
                .IsUnicode(false);

            modelBuilder.Entity<SMOceanCarrier>()
                .Property(e => e.OceanCarrierPhoneExt)
                .IsUnicode(false);

            modelBuilder.Entity<SMOceanCarrier>()
                .Property(e => e.OceanCarrierFax)
                .IsUnicode(false);

            modelBuilder.Entity<SMOceanCarrier>()
                .Property(e => e.OceanCarrierFaxExt)
                .IsUnicode(false);

            modelBuilder.Entity<SMOceanCarrier>()
                .Property(e => e.OceanCarrierZip)
                .IsUnicode(false);

            modelBuilder.Entity<SMOceanCarrier>()
                .Property(e => e.Status)
                .IsUnicode(false);

            modelBuilder.Entity<SMOceanCarrier>()
                .Property(e => e.CreatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<SMOceanCarrier>()
                .Property(e => e.UpdatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<SMOceanCarrier>()
                .Property(e => e.Version)
                .IsFixedLength();

            modelBuilder.Entity<SMOceanCarrier>()
                .Property(e => e.SCAC)
                .IsUnicode(false);

            modelBuilder.Entity<SMPayType>()
                .Property(e => e.PayTypeCode)
                .IsUnicode(false);

            modelBuilder.Entity<SMPayType>()
                .Property(e => e.Status)
                .IsUnicode(false);

            modelBuilder.Entity<SMPayType>()
                .Property(e => e.CreatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<SMPayType>()
                .Property(e => e.UpdatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<SMPayType>()
                .Property(e => e.Version)
                .IsFixedLength();

            modelBuilder.Entity<SMPODTemplate>()
                .Property(e => e.PODTEMPLDesc)
                .IsUnicode(false);

            modelBuilder.Entity<SMPODTemplate>()
                .Property(e => e.GLobalCode)
                .IsUnicode(false);

            modelBuilder.Entity<SMPODTemplate>()
                .Property(e => e.CreatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<SMPODTemplate>()
                .Property(e => e.UpdatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<SMPODTemplate>()
                .Property(e => e.TradeTerm)
                .IsUnicode(false);

            modelBuilder.Entity<SMPODTemplateDetail>()
                .Property(e => e.Mode)
                .IsUnicode(false);

            modelBuilder.Entity<SMPODTemplateDetail>()
                .Property(e => e.Milestone)
                .IsUnicode(false);

            modelBuilder.Entity<SMPODTemplateDetail>()
                .Property(e => e.DateTable)
                .IsUnicode(false);

            modelBuilder.Entity<SMPODTemplateDetail>()
                .Property(e => e.DateField)
                .IsUnicode(false);

            modelBuilder.Entity<SMPODTemplateDetail>()
                .Property(e => e.PartyTable)
                .IsUnicode(false);

            modelBuilder.Entity<SMPODTemplateDetail>()
                .Property(e => e.PartyField)
                .IsUnicode(false);

            modelBuilder.Entity<SMPODTemplateDetail>()
                .Property(e => e.CreatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<SMPODTemplateDetail>()
                .Property(e => e.UpdatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<SMProductLine>()
                .Property(e => e.ProductLineCode)
                .IsUnicode(false);

            modelBuilder.Entity<SMProductLine>()
                .Property(e => e.ExporImp)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<SMProductLine>()
                .Property(e => e.Status)
                .IsUnicode(false);

            modelBuilder.Entity<SMProductLine>()
                .Property(e => e.CreatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<SMProductLine>()
                .Property(e => e.UpdatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<SMProductLine>()
                .Property(e => e.Version)
                .IsFixedLength();

            modelBuilder.Entity<SMProductLine>()
                .Property(e => e.ChargeCodeUOMType)
                .IsUnicode(false);

            modelBuilder.Entity<SMProductLine>()
                .Property(e => e.ChargeCodeUOMType2)
                .IsUnicode(false);

            modelBuilder.Entity<SMProductLine>()
                .Property(e => e.ProductLineShowCode)
                .IsUnicode(false);

            modelBuilder.Entity<SMProductLine>()
                .HasMany(e => e.SMChargeCodePL)
                .WithRequired(e => e.SMProductLine)
                .HasForeignKey(e => e.ProductLineID)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<SMProject>()
                .Property(e => e.ProjectCode)
                .IsUnicode(false);

            modelBuilder.Entity<SMProject>()
                .Property(e => e.Status)
                .IsUnicode(false);

            modelBuilder.Entity<SMProject>()
                .Property(e => e.CreatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<SMProject>()
                .Property(e => e.UpdatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<SMProject>()
                .Property(e => e.Version)
                .IsFixedLength();

            modelBuilder.Entity<SMRateClass>()
                .Property(e => e.RateClassCode)
                .IsUnicode(false);

            modelBuilder.Entity<SMRateClass>()
                .Property(e => e.Status)
                .IsUnicode(false);

            modelBuilder.Entity<SMRateClass>()
                .Property(e => e.CreatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<SMRateClass>()
                .Property(e => e.UpdatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<SMRateClass>()
                .Property(e => e.Version)
                .IsFixedLength();

            modelBuilder.Entity<SMReminderRule>()
                .Property(e => e.Name)
                .IsUnicode(false);

            modelBuilder.Entity<SMReminderRule>()
                .Property(e => e.Code)
                .IsUnicode(false);

            modelBuilder.Entity<SMReminderRule>()
                .Property(e => e.Type)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<SMReminderRule>()
                .Property(e => e.Description)
                .IsUnicode(false);

            modelBuilder.Entity<SMReminderRule>()
                .Property(e => e.Mode)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<SMReminderRule>()
                .Property(e => e.CreatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<SMReminderRule>()
                .Property(e => e.UpdatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<SMReminderRule>()
                .Property(e => e.Status)
                .IsUnicode(false);

            modelBuilder.Entity<SMSeaPort>()
                .Property(e => e.SeaPortCode)
                .IsUnicode(false);

            modelBuilder.Entity<SMSeaPort>()
                .Property(e => e.Status)
                .IsUnicode(false);

            modelBuilder.Entity<SMSeaPort>()
                .Property(e => e.CreatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<SMSeaPort>()
                .Property(e => e.UpdatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<SMSeaPort>()
                .Property(e => e.Version)
                .IsFixedLength();

            modelBuilder.Entity<SMSeaPort>()
                .Property(e => e.AMSSeaPortCode)
                .IsUnicode(false);

            modelBuilder.Entity<SMSeaPort>()
                .Property(e => e.ISF5Port)
                .IsUnicode(false);

            modelBuilder.Entity<SMSeaPort>()
                .Property(e => e.UNLocationCode)
                .IsUnicode(false);

            modelBuilder.Entity<SMServiceType>()
                .Property(e => e.ServiceTypeCode)
                .IsUnicode(false);

            modelBuilder.Entity<SMServiceType>()
                .Property(e => e.Status)
                .IsUnicode(false);

            modelBuilder.Entity<SMServiceType>()
                .Property(e => e.CreatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<SMServiceType>()
                .Property(e => e.UpdatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<SMServiceType>()
                .Property(e => e.Version)
                .IsFixedLength();

            modelBuilder.Entity<SMShipmentType>()
                .Property(e => e.ShipmentTypeCode)
                .IsUnicode(false);

            modelBuilder.Entity<SMShipmentType>()
                .Property(e => e.Status)
                .IsUnicode(false);

            modelBuilder.Entity<SMShipmentType>()
                .Property(e => e.CreatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<SMShipmentType>()
                .Property(e => e.UpdatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<SMShipmentType>()
                .Property(e => e.Version)
                .IsFixedLength();

            modelBuilder.Entity<SMShowRate>()
                .Property(e => e.ShowRateCode)
                .IsUnicode(false);

            modelBuilder.Entity<SMShowRate>()
                .Property(e => e.Status)
                .IsUnicode(false);

            modelBuilder.Entity<SMShowRate>()
                .Property(e => e.CreatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<SMShowRate>()
                .Property(e => e.UpdatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<SMShowRate>()
                .Property(e => e.Version)
                .IsFixedLength();

            modelBuilder.Entity<SMSiteURLMap>()
                .Property(e => e.SiteCode)
                .IsUnicode(false);

            modelBuilder.Entity<SMSiteURLMap>()
                .Property(e => e.SiteURL)
                .IsUnicode(false);

            modelBuilder.Entity<SMSiteURLMap>()
                .Property(e => e.Status)
                .IsUnicode(false);

            modelBuilder.Entity<SMSiteURLMap>()
                .Property(e => e.CreatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<SMSiteURLMap>()
                .Property(e => e.UpdatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<SMSiteURLMap>()
                .Property(e => e.Version)
                .IsFixedLength();

            modelBuilder.Entity<SMSPL>()
                .Property(e => e.CreatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<SMSPL>()
                .Property(e => e.UpdatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<SMState>()
                .Property(e => e.StateCode)
                .IsUnicode(false);

            modelBuilder.Entity<SMState>()
                .Property(e => e.Status)
                .IsUnicode(false);

            modelBuilder.Entity<SMState>()
                .Property(e => e.CreatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<SMState>()
                .Property(e => e.UpdatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<SMState>()
                .Property(e => e.Version)
                .IsFixedLength();

            modelBuilder.Entity<SMStation>()
                .Property(e => e.StationID)
                .IsUnicode(false);

            modelBuilder.Entity<SMStation>()
                .Property(e => e.StationCode)
                .IsUnicode(false);

            modelBuilder.Entity<SMStation>()
                .Property(e => e.StationType)
                .IsUnicode(false);

            modelBuilder.Entity<SMStation>()
                .Property(e => e.HomeCurrency)
                .IsUnicode(false);

            modelBuilder.Entity<SMStation>()
                .Property(e => e.ReportCurrency)
                .IsUnicode(false);

            modelBuilder.Entity<SMStation>()
                .Property(e => e.StationPhone)
                .IsUnicode(false);

            modelBuilder.Entity<SMStation>()
                .Property(e => e.StationPhoneExt)
                .IsUnicode(false);

            modelBuilder.Entity<SMStation>()
                .Property(e => e.StationFax)
                .IsUnicode(false);

            modelBuilder.Entity<SMStation>()
                .Property(e => e.StationFaxExt)
                .IsUnicode(false);

            modelBuilder.Entity<SMStation>()
                .Property(e => e.StationZIP)
                .IsUnicode(false);

            modelBuilder.Entity<SMStation>()
                .Property(e => e.MonthFrom)
                .HasPrecision(18, 0);

            modelBuilder.Entity<SMStation>()
                .Property(e => e.GLNOARBank)
                .IsUnicode(false);

            modelBuilder.Entity<SMStation>()
                .Property(e => e.GLNOAPBank)
                .IsUnicode(false);

            modelBuilder.Entity<SMStation>()
                .Property(e => e.GLNOExch)
                .IsUnicode(false);

            modelBuilder.Entity<SMStation>()
                .Property(e => e.GLNOUnexch)
                .IsUnicode(false);

            modelBuilder.Entity<SMStation>()
                .Property(e => e.GLNOYE)
                .IsUnicode(false);

            modelBuilder.Entity<SMStation>()
                .Property(e => e.SiteURL)
                .IsUnicode(false);

            modelBuilder.Entity<SMStation>()
                .Property(e => e.IATACode)
                .IsUnicode(false);

            modelBuilder.Entity<SMStation>()
                .Property(e => e.Status)
                .IsUnicode(false);

            modelBuilder.Entity<SMStation>()
                .Property(e => e.CreatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<SMStation>()
                .Property(e => e.UpdatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<SMStation>()
                .Property(e => e.Version)
                .IsFixedLength();

            modelBuilder.Entity<SMStation>()
                .Property(e => e.DBName)
                .IsUnicode(false);

            modelBuilder.Entity<SMStation>()
                .Property(e => e.StationIP)
                .IsUnicode(false);

            modelBuilder.Entity<SMStation>()
                .Property(e => e.StationLevel)
                .IsUnicode(false);

            modelBuilder.Entity<SMStation>()
                .Property(e => e.ParentStationID)
                .IsUnicode(false);

            modelBuilder.Entity<SMStation>()
                .Property(e => e.Longitute)
                .IsUnicode(false);

            modelBuilder.Entity<SMStation>()
                .Property(e => e.Latitude)
                .IsUnicode(false);

            modelBuilder.Entity<SMStation>()
                .Property(e => e.InboxName)
                .IsUnicode(false);

            modelBuilder.Entity<SMStation>()
                .Property(e => e.eMailAcct)
                .IsUnicode(false);

            modelBuilder.Entity<SMStation>()
                .Property(e => e.ProductLine)
                .IsUnicode(false);

            modelBuilder.Entity<SMStation>()
                .Property(e => e.CubeDBName)
                .IsUnicode(false);

            modelBuilder.Entity<SMStation>()
                .Property(e => e.UsedWMSSYS)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<SMStation>()
                .Property(e => e.UsedTMSSYS)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<SMStation>()
                .Property(e => e.LocalWebURL)
                .IsUnicode(false);

            modelBuilder.Entity<SMStation>()
                .Property(e => e.TMSLanguage)
                .IsUnicode(false);

            modelBuilder.Entity<SMStation>()
                .Property(e => e.LocalWebPort)
                .IsUnicode(false);

            modelBuilder.Entity<SMStation>()
                .Property(e => e.FTPRoot)
                .IsUnicode(false);

            modelBuilder.Entity<SMStation>()
                .Property(e => e.LocalWebURL2)
                .IsUnicode(false);

            modelBuilder.Entity<SMStation>()
                .Property(e => e.LocalWebPort2)
                .IsUnicode(false);

            modelBuilder.Entity<SMStation>()
                .Property(e => e.LocalWebURL3)
                .IsUnicode(false);

            modelBuilder.Entity<SMStation>()
                .Property(e => e.LocalWebPort3)
                .IsUnicode(false);

            modelBuilder.Entity<SMStation>()
                .Property(e => e.LocalWebURL4)
                .IsUnicode(false);

            modelBuilder.Entity<SMStation>()
                .Property(e => e.LocalWebPort4)
                .IsUnicode(false);

            modelBuilder.Entity<SMStationConfig>()
                .Property(e => e.StationID)
                .IsUnicode(false);

            modelBuilder.Entity<SMStationConfig>()
                .Property(e => e.InboxName)
                .IsUnicode(false);

            modelBuilder.Entity<SMStationConfig>()
                .Property(e => e.eMailAcct)
                .IsUnicode(false);

            modelBuilder.Entity<SMStationConfig>()
                .Property(e => e.StationCode)
                .IsUnicode(false);

            modelBuilder.Entity<SMTariffCode>()
                .Property(e => e.TariffCode)
                .IsUnicode(false);

            modelBuilder.Entity<SMTariffCode>()
                .Property(e => e.Status)
                .IsUnicode(false);

            modelBuilder.Entity<SMTariffCode>()
                .Property(e => e.CreatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<SMTariffCode>()
                .Property(e => e.UpdatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<SMTariffCode>()
                .Property(e => e.Version)
                .IsFixedLength();

            modelBuilder.Entity<SMTimeZone>()
                .Property(e => e.TimeZoneName)
                .IsUnicode(false);

            modelBuilder.Entity<SMTimeZone>()
                .Property(e => e.DisplayName)
                .IsUnicode(false);

            modelBuilder.Entity<SMTimeZone>()
                .Property(e => e.StandardName)
                .IsUnicode(false);

            modelBuilder.Entity<SMTimeZone>()
                .Property(e => e.DaylightName)
                .IsUnicode(false);

            modelBuilder.Entity<SMTradeTerm>()
                .Property(e => e.TradeTermCode)
                .IsUnicode(false);

            modelBuilder.Entity<SMTradeTerm>()
                .Property(e => e.Status)
                .IsUnicode(false);

            modelBuilder.Entity<SMTradeTerm>()
                .Property(e => e.CreatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<SMTradeTerm>()
                .Property(e => e.UpdatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<SMTradeTerm>()
                .Property(e => e.Version)
                .IsFixedLength();

            modelBuilder.Entity<SMTradeTerm>()
                .Property(e => e.FRT)
                .IsUnicode(false);

            modelBuilder.Entity<SMTradeTerm>()
                .Property(e => e.Other)
                .IsUnicode(false);

            modelBuilder.Entity<SMTradeTerm>()
                .Property(e => e.MoveCode)
                .IsUnicode(false);

            modelBuilder.Entity<SMUOM>()
                .Property(e => e.UOMCode)
                .IsUnicode(false);

            modelBuilder.Entity<SMUOM>()
                .Property(e => e.Decimal)
                .IsUnicode(false);

            modelBuilder.Entity<SMUOM>()
                .Property(e => e.Rounding)
                .IsUnicode(false);

            modelBuilder.Entity<SMUOM>()
                .Property(e => e.UOMType)
                .IsUnicode(false);

            modelBuilder.Entity<SMUOM>()
                .Property(e => e.Status)
                .IsUnicode(false);

            modelBuilder.Entity<SMUOM>()
                .Property(e => e.CreatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<SMUOM>()
                .Property(e => e.UpdatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<SMUOM>()
                .Property(e => e.Version)
                .IsFixedLength();

            modelBuilder.Entity<SMUOMConversion>()
                .Property(e => e.Factor)
                .HasPrecision(18, 0);

            modelBuilder.Entity<SMUOMConversion>()
                .Property(e => e.Decimal)
                .HasPrecision(18, 0);

            modelBuilder.Entity<SMUOMConversion>()
                .Property(e => e.Rounding)
                .HasPrecision(18, 0);

            modelBuilder.Entity<SMUOMConversion>()
                .Property(e => e.Status)
                .IsUnicode(false);

            modelBuilder.Entity<SMUOMConversion>()
                .Property(e => e.CreatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<SMUOMConversion>()
                .Property(e => e.UpdatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<SMUOMConversion>()
                .Property(e => e.Version)
                .IsFixedLength();

            modelBuilder.Entity<SMUOMPL>()
                .Property(e => e.Status)
                .IsUnicode(false);

            modelBuilder.Entity<SMUOMPL>()
                .Property(e => e.CreatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<SMUOMPL>()
                .Property(e => e.UpdatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<SMUOMPL>()
                .Property(e => e.Version)
                .IsFixedLength();

            modelBuilder.Entity<SMUser>()
                .Property(e => e.UserID)
                .IsUnicode(false);

            modelBuilder.Entity<SMUser>()
                .Property(e => e.DepartmentID)
                .IsUnicode(false);

            modelBuilder.Entity<SMUser>()
                .Property(e => e.StationID)
                .IsUnicode(false);

            modelBuilder.Entity<SMUser>()
                .Property(e => e.Status)
                .IsUnicode(false);

            modelBuilder.Entity<SMUser>()
                .Property(e => e.CreatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<SMUser>()
                .Property(e => e.UpdatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<SMUser>()
                .Property(e => e.Version)
                .IsFixedLength();

            modelBuilder.Entity<SMUser>()
                .Property(e => e.LastLoginIP)
                .IsUnicode(false);

            modelBuilder.Entity<SMUser>()
                .Property(e => e.Admin)
                .IsUnicode(false);

            modelBuilder.Entity<SMUser>()
                .Property(e => e.AliasLoginID)
                .IsUnicode(false);

            modelBuilder.Entity<SMUser>()
                .Property(e => e.CostCenter1)
                .IsUnicode(false);

            modelBuilder.Entity<SMUser>()
                .Property(e => e.CostCenter2)
                .IsUnicode(false);

            modelBuilder.Entity<SMUser>()
                .Property(e => e.CostCenter3)
                .IsUnicode(false);

            modelBuilder.Entity<SMUser>()
                .Property(e => e.IATANo)
                .IsUnicode(false);

            modelBuilder.Entity<SMUserRight>()
                .Property(e => e.FunctionCode)
                .IsUnicode(false);

            modelBuilder.Entity<SMUserRight>()
                .Property(e => e.UserID)
                .IsUnicode(false);

            modelBuilder.Entity<SMUserRight>()
                .Property(e => e.CreatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<SMUserRight>()
                .Property(e => e.UpdatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<SMUserStation>()
                .Property(e => e.UserID)
                .IsUnicode(false);

            modelBuilder.Entity<SMUserStation>()
                .Property(e => e.StationID)
                .IsUnicode(false);

            modelBuilder.Entity<SMUserStation>()
                .Property(e => e.Status)
                .IsUnicode(false);

            modelBuilder.Entity<SMVATCountry>()
                .Property(e => e.VATCode)
                .IsUnicode(false);

            modelBuilder.Entity<SMVATCountry>()
                .Property(e => e.Status)
                .IsUnicode(false);

            modelBuilder.Entity<SMVATCountry>()
                .Property(e => e.CreatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<SMVATCountry>()
                .Property(e => e.UpdatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<SMVATCountry>()
                .Property(e => e.Version)
                .IsFixedLength();

            modelBuilder.Entity<SMVesselName>()
                .Property(e => e.VesselCode)
                .IsUnicode(false);

            modelBuilder.Entity<SMVesselName>()
                .Property(e => e.VesselFlag)
                .IsUnicode(false);

            modelBuilder.Entity<SMVesselName>()
                .Property(e => e.Status)
                .IsUnicode(false);

            modelBuilder.Entity<SMVesselName>()
                .Property(e => e.CreatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<SMVesselName>()
                .Property(e => e.UpdatedBy)
                .IsUnicode(false);
        }
    }
}
