namespace ValuePlus.Domain.eSM
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("SMMilestone")]
    public partial class SMMilestone
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int HQID { get; set; }

        [StringLength(2)]
        public string Mode { get; set; }

        [Required]
        [StringLength(100)]
        public string MilestoneName { get; set; }

        [StringLength(1)]
        public string InputType { get; set; }

        [StringLength(1)]
        public string EntryControl { get; set; }

        [StringLength(100)]
        public string Description { get; set; }

        public int? SeqNum { get; set; }

        [StringLength(10)]
        public string Status { get; set; }

        [StringLength(1)]
        public string IMPStatus { get; set; }

        [StringLength(1)]
        public string ViewType { get; set; }

        [StringLength(1)]
        public string EstAct { get; set; }

        public DateTime? CreatedDate { get; set; }
    }
}
