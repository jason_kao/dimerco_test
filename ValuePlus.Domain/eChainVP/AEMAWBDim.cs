namespace ValuePlus.Domain.eChainVP
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("AEMAWBDim")]
    public partial class AEMAWBDim
    {
        public int ID { get; set; }

        public int MAWBID { get; set; }

        public double Length { get; set; }

        public double Width { get; set; }

        public double Height { get; set; }

        public int PCS { get; set; }

        public double? Factor { get; set; }

        public double? VWT { get; set; }

        public double? CBM { get; set; }

        [StringLength(50)]
        public string VWTUOM { get; set; }

        [StringLength(50)]
        public string DIMUOM { get; set; }

        public double? CUFT { get; set; }

        [Required]
        [StringLength(10)]
        public string StationID { get; set; }

        [Required]
        [StringLength(6)]
        public string CreatedBy { get; set; }

        public DateTime CreatedDate { get; set; }

        [StringLength(6)]
        public string UpdatedBy { get; set; }

        public DateTime? UpdatedDate { get; set; }

        [Column(TypeName = "timestamp")]
        [MaxLength(8)]
        [Timestamp]
        public byte[] Version { get; set; }
    }
}
