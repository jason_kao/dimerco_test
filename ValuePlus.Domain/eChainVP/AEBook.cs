namespace ValuePlus.Domain.eChainVP
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("AEBook")]
    public partial class AEBook
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ID { get; set; }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int HAWBID { get; set; }

        [StringLength(10)]
        public string Area { get; set; }

        [StringLength(50)]
        public string ServiceLevel { get; set; }

        public decimal? BookingCWT { get; set; }

        public decimal? BookingVolume { get; set; }

        [StringLength(100)]
        public string ShipperLocalName { get; set; }

        [StringLength(100)]
        public string LocalCommodity { get; set; }

        [StringLength(50)]
        public string DocType { get; set; }

        [StringLength(10)]
        public string DSTNStationID { get; set; }

        [StringLength(10)]
        public string Status { get; set; }

        [StringLength(50)]
        public string CS { get; set; }

        [StringLength(50)]
        public string Rate { get; set; }

        [StringLength(200)]
        public string Remark { get; set; }

        [StringLength(200)]
        public string ShipingRemark { get; set; }

        [StringLength(50)]
        public string Carrier { get; set; }

        [StringLength(50)]
        public string SalesPerson { get; set; }

        [StringLength(10)]
        public string CreatedBy { get; set; }

        public DateTime? CreatedDate { get; set; }

        [StringLength(10)]
        public string UpdatedBy { get; set; }

        public DateTime? UpdatedDate { get; set; }

        [StringLength(50)]
        public string LoadStation1 { get; set; }

        [StringLength(50)]
        public string LoadMasterLoader1 { get; set; }

        [StringLength(50)]
        public string LoadCarrier1 { get; set; }

        [StringLength(50)]
        public string LoadMAWBNo1 { get; set; }

        [StringLength(50)]
        public string LoadEstCost1 { get; set; }

        [StringLength(50)]
        public string LoadFLTNo1 { get; set; }

        [StringLength(50)]
        public string LoadTerminal1 { get; set; }

        [StringLength(50)]
        public string LoadStation2 { get; set; }

        [StringLength(50)]
        public string LoadMasterLoader2 { get; set; }

        [StringLength(50)]
        public string LoadCarrier2 { get; set; }

        [StringLength(50)]
        public string LoadMAWBNo2 { get; set; }

        [StringLength(50)]
        public string LoadEstCost2 { get; set; }

        [StringLength(50)]
        public string LoadFLTNo2 { get; set; }

        [StringLength(1)]
        public string BookSend { get; set; }

        [StringLength(50)]
        public string WHCargoStatus { get; set; }

        [StringLength(50)]
        public string WHLocation { get; set; }

        public decimal? WHVolume { get; set; }

        [StringLength(200)]
        public string WHDimension { get; set; }

        [StringLength(50)]
        public string RTHexiaoDanNo { get; set; }

        public DateTime? RTTuiShuiDate { get; set; }

        [StringLength(50)]
        public string CSStatus { get; set; }

        public int? BookingPCS { get; set; }

        [StringLength(10)]
        public string BookingPCSUOM { get; set; }

        public decimal? BookingGWT { get; set; }

        [StringLength(10)]
        public string BookingWTUOM { get; set; }

        [StringLength(10)]
        public string BookingVolumeUOM { get; set; }

        public DateTime? HubCargoDate { get; set; }

        [StringLength(50)]
        public string HubServiceLevel { get; set; }
    }
}
