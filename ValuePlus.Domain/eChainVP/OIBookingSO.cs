namespace ValuePlus.Domain.eChainVP
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("OIBookingSO")]
    public partial class OIBookingSO
    {
        public int ID { get; set; }

        [StringLength(5)]
        public string StationID { get; set; }

        [StringLength(5)]
        public string SoFor { get; set; }

        [StringLength(5)]
        public string SrcStationID { get; set; }

        public int? SrcID { get; set; }

        public int? BookingID { get; set; }

        public int? HBLID { get; set; }

        [StringLength(60)]
        public string SONo { get; set; }

        public int? PCS { get; set; }

        [StringLength(10)]
        public string PCSUOM { get; set; }

        public decimal? Weight { get; set; }

        [StringLength(10)]
        public string WeightUOM { get; set; }

        public decimal? CBM { get; set; }

        [StringLength(10)]
        public string CBMUOM { get; set; }

        [StringLength(1000)]
        public string Description { get; set; }

        [StringLength(5)]
        public string DSTNStationID { get; set; }

        public int? DSTNID { get; set; }

        [StringLength(50)]
        public string CreatedBy { get; set; }

        public DateTime? CreatedDT { get; set; }

        [StringLength(50)]
        public string UpdatedBy { get; set; }

        public DateTime? UpdatedDT { get; set; }

        [Column(TypeName = "timestamp")]
        [MaxLength(8)]
        [Timestamp]
        public byte[] Version { get; set; }

        [StringLength(20)]
        public string CTNRType { get; set; }

        [StringLength(20)]
        public string CTNRNo { get; set; }
    }
}
