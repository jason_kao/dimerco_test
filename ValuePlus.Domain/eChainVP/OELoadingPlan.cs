namespace ValuePlus.Domain.eChainVP
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("OELoadingPlan")]
    public partial class OELoadingPlan
    {
        public int ID { get; set; }

        [StringLength(5)]
        public string StationID { get; set; }

        public int? LoadPlanID { get; set; }

        public int? MBLID { get; set; }

        [StringLength(20)]
        public string LoadingPlanNo { get; set; }

        [StringLength(60)]
        public string OceanVessel { get; set; }

        [StringLength(60)]
        public string OceanVoyage { get; set; }

        public int? SHPR { get; set; }

        public int? CoLoaderAgent { get; set; }

        [StringLength(60)]
        public string CarrierSONo { get; set; }

        [StringLength(20)]
        public string CTNRType { get; set; }

        [StringLength(40)]
        public string CTNRNo { get; set; }

        [StringLength(40)]
        public string SealNo { get; set; }

        public int? HBLCount { get; set; }

        public int? PCS { get; set; }

        public int? Customer { get; set; }

        public int? CNEE { get; set; }

        public int? CoLoader { get; set; }

        public int? NTFY { get; set; }

        public int? BookingParty { get; set; }

        public int? Carrier { get; set; }

        [StringLength(15)]
        public string ShptType { get; set; }

        [StringLength(15)]
        public string MoveType { get; set; }

        [StringLength(15)]
        public string FreightPayType { get; set; }

        public int? PReceipt { get; set; }

        public DateTime? PReceiptETD { get; set; }

        public DateTime? PReceiptATD { get; set; }

        public int? PLoading { get; set; }

        public DateTime? PLoadingETD { get; set; }

        public DateTime? PLoadingATD { get; set; }

        public int? PDischarge { get; set; }

        public DateTime? PDischargeETD { get; set; }

        public DateTime? PDischargeATD { get; set; }

        public int? PDelivery { get; set; }

        public DateTime? PDeliveryETD { get; set; }

        public DateTime? PDeliveryATD { get; set; }

        [StringLength(10)]
        public string Status { get; set; }

        [StringLength(50)]
        public string CreatedBy { get; set; }

        public DateTime? CreatedDT { get; set; }

        [StringLength(50)]
        public string UpdatedBy { get; set; }

        public DateTime? UpdatedDT { get; set; }

        [Column(TypeName = "timestamp")]
        [MaxLength(8)]
        [Timestamp]
        public byte[] Version { get; set; }

        [StringLength(255)]
        public string Instruction { get; set; }
    }
}
