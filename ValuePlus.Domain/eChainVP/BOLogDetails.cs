namespace ValuePlus.Domain.eChainVP
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class BOLogDetails
    {
        public int Id { get; set; }

        [StringLength(100)]
        public string EntryClass { get; set; }

        [StringLength(100)]
        public string FunctionName { get; set; }

        [StringLength(255)]
        public string FunctionParameters { get; set; }

        [StringLength(50)]
        public string ClientIP { get; set; }

        public string ExceptionMessage { get; set; }

        public DateTime? CreatedTime { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? ExecutionSeconds { get; set; }
    }
}
