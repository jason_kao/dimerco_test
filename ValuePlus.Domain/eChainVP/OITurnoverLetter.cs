namespace ValuePlus.Domain.eChainVP
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("OITurnoverLetter")]
    public partial class OITurnoverLetter
    {
        public int ID { get; set; }

        [StringLength(50)]
        public string HBLID { get; set; }

        [StringLength(5)]
        public string Mode { get; set; }

        [StringLength(5)]
        public string StationID { get; set; }

        [StringLength(200)]
        public string StationName { get; set; }

        [StringLength(200)]
        public string StationAddress { get; set; }

        [StringLength(100)]
        public string StationTelFax { get; set; }

        [StringLength(200)]
        public string Attention { get; set; }

        [StringLength(200)]
        public string CNEE { get; set; }

        [StringLength(200)]
        public string FreightLocation { get; set; }

        [StringLength(50)]
        public string MasterBLNo { get; set; }

        [StringLength(50)]
        public string HouseBLNo { get; set; }

        [StringLength(50)]
        public string AMSBLNo { get; set; }

        [StringLength(50)]
        public string SubHBLNo { get; set; }

        [StringLength(50)]
        public string Vessel { get; set; }

        [StringLength(50)]
        public string Voyage { get; set; }

        [StringLength(50)]
        public string PortLoading { get; set; }

        [StringLength(50)]
        public string PortDischarge { get; set; }

        [StringLength(50)]
        public string LastFreeDay { get; set; }

        [StringLength(50)]
        public string PlaceDelivery { get; set; }

        [StringLength(50)]
        public string ITNo { get; set; }

        [StringLength(50)]
        public string ITDate { get; set; }

        [StringLength(50)]
        public string ITPort { get; set; }

        [StringLength(4000)]
        public string ContainerNo { get; set; }

        [StringLength(300)]
        public string NoOfPkgs { get; set; }

        [StringLength(4000)]
        public string Goods { get; set; }

        [StringLength(300)]
        public string GWT { get; set; }

        [StringLength(300)]
        public string Measurement { get; set; }

        [StringLength(1000)]
        public string Remarks { get; set; }

        [StringLength(50)]
        public string PreparedBy { get; set; }

        [StringLength(50)]
        public string PreparedDT { get; set; }

        [StringLength(50)]
        public string CreatedBy { get; set; }

        public DateTime? CreatedDT { get; set; }

        [StringLength(50)]
        public string UpdatedBy { get; set; }

        public DateTime? UpdatedDT { get; set; }
    }
}
