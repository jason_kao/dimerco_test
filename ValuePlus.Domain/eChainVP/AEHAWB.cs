namespace ValuePlus.Domain.eChainVP
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("AEHAWB")]
    public partial class AEHAWB
    {
        public int ID { get; set; }

        public int? MAWBID { get; set; }

        public int? ParentAWB { get; set; }

        [StringLength(50)]
        public string HAWBNo { get; set; }

        public int? Customer { get; set; }

        public int? BBAgent { get; set; }

        public int? Shipper { get; set; }

        public int? CNEE { get; set; }

        public int? Notify { get; set; }

        public int? ThirdParty { get; set; }

        [StringLength(6)]
        public string Sales { get; set; }

        [StringLength(5)]
        public string FRT { get; set; }

        [StringLength(5)]
        public string Other { get; set; }

        [StringLength(5)]
        public string Move { get; set; }

        public int? ActPCS { get; set; }

        [StringLength(10)]
        public string ActPCSUOM { get; set; }

        public int? SPUnit { get; set; }

        [StringLength(5)]
        public string SPUnitUOM { get; set; }

        public double? GWT { get; set; }

        public double? VWT { get; set; }

        public double? CWT { get; set; }

        [StringLength(5)]
        public string WTUOM { get; set; }

        [StringLength(5)]
        public string ClassRate { get; set; }

        public double? Rate { get; set; }

        [StringLength(5)]
        public string Currency { get; set; }

        public bool? ShowRate { get; set; }

        [StringLength(255)]
        public string ExportLIC { get; set; }

        [StringLength(20)]
        public string Quantity { get; set; }

        [StringLength(20)]
        public string CommInv { get; set; }

        [StringLength(750)]
        public string DESC { get; set; }

        [StringLength(255)]
        public string Remark { get; set; }

        [Column(TypeName = "ntext")]
        public string Marks { get; set; }

        [StringLength(50)]
        public string SPINST { get; set; }

        [StringLength(5)]
        public string AWBType { get; set; }

        public int? PortOfDEPT { get; set; }

        public int? PortOfDSTN { get; set; }

        public int? PlaceOfRCPT { get; set; }

        public int? PlaceOfDELV { get; set; }

        [StringLength(5)]
        public string TradeTerm { get; set; }

        public DateTime? IssueDate { get; set; }

        [StringLength(10)]
        public string PrintPerson { get; set; }

        public double? DueToAgent { get; set; }

        public double? DueToCarrier { get; set; }

        public DateTime? ArriDate { get; set; }

        public DateTime? DeptDate { get; set; }

        public DateTime? AvaiDate { get; set; }

        public double? CUFT { get; set; }

        public bool? CYM { get; set; }

        public bool? NoExpect { get; set; }

        public int? CustomsDeclareParty { get; set; }

        public int? CustomsHandledBy { get; set; }

        [StringLength(10)]
        public string CustomsDocType { get; set; }

        public DateTime? CustomsAvaDate { get; set; }

        public DateTime? CustomsDecDate { get; set; }

        [StringLength(255)]
        public string CustomsRemark { get; set; }

        [StringLength(50)]
        public string CustomsStatus { get; set; }

        public int? WHActPCS { get; set; }

        [StringLength(5)]
        public string WHActPCSUOM { get; set; }

        public double? WHActWT { get; set; }

        [StringLength(5)]
        public string WHActWTUOM { get; set; }

        public double? WHActVWT { get; set; }

        [StringLength(5)]
        public string WHActVWTUOM { get; set; }

        [StringLength(20)]
        public string WHLocation { get; set; }

        public DateTime? WHArrivalDate { get; set; }

        public DateTime? WHPulloutDate { get; set; }

        [StringLength(50)]
        public string WHContainer { get; set; }

        [StringLength(255)]
        public string WHRemark { get; set; }

        [StringLength(50)]
        public string WHCargoStatus { get; set; }

        [StringLength(20)]
        public string WHReceiptNo { get; set; }

        [StringLength(20)]
        public string PreAlertUser { get; set; }

        public DateTime? PreAlertDate { get; set; }

        public int? BookedPCS { get; set; }

        [StringLength(5)]
        public string BookedWTUOM { get; set; }

        public double? BookedGWT { get; set; }

        public double? BookedVWT { get; set; }

        [StringLength(10)]
        public string Status { get; set; }

        [Required]
        [StringLength(10)]
        public string StationID { get; set; }

        [StringLength(10)]
        public string ServiceLevel { get; set; }

        [Required]
        [StringLength(6)]
        public string CreatedBy { get; set; }

        public DateTime CreatedDate { get; set; }

        [Required]
        [StringLength(6)]
        public string UpdatedBy { get; set; }

        public DateTime? UpdatedDate { get; set; }

        [Column(TypeName = "timestamp")]
        [MaxLength(8)]
        [Timestamp]
        public byte[] Version { get; set; }

        [StringLength(10)]
        public string SStationID { get; set; }

        [StringLength(50)]
        public string ShptType { get; set; }

        [StringLength(50)]
        public string ReferenceCode { get; set; }

        [StringLength(20)]
        public string SPL { get; set; }

        [StringLength(10)]
        public string IMPStation { get; set; }

        [StringLength(20)]
        public string BKNO { get; set; }

        [StringLength(500)]
        public string ConfirmID { get; set; }

        [StringLength(50)]
        public string HSCode { get; set; }

        [StringLength(50)]
        public string UNCode { get; set; }

        [StringLength(15)]
        public string MTSNO { get; set; }

        public int? ImportAgent { get; set; }

        public int? SourceID { get; set; }

        [StringLength(50)]
        public string FRTParty { get; set; }

        [StringLength(3)]
        public string DBID { get; set; }

        [StringLength(50)]
        public string Extra0 { get; set; }

        [StringLength(50)]
        public string Extra1 { get; set; }

        [StringLength(50)]
        public string Extra2 { get; set; }

        [StringLength(50)]
        public string Extra3 { get; set; }

        [StringLength(50)]
        public string Extra4 { get; set; }

        [StringLength(50)]
        public string Extra5 { get; set; }

        [StringLength(50)]
        public string Extra6 { get; set; }

        [StringLength(50)]
        public string Extra7 { get; set; }

        [StringLength(50)]
        public string Extra8 { get; set; }

        [StringLength(50)]
        public string Extra9 { get; set; }

        public int? CopyAIID { get; set; }
    }
}
