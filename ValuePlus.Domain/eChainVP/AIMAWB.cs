namespace ValuePlus.Domain.eChainVP
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("AIMAWB")]
    public partial class AIMAWB
    {
        public int ID { get; set; }

        [StringLength(20)]
        public string MAWBNo { get; set; }

        [StringLength(5)]
        public string Other { get; set; }

        [StringLength(5)]
        public string FRT { get; set; }

        public int? MAWBType { get; set; }

        public int? IssueCity { get; set; }

        public DateTime? IssueDate { get; set; }

        [StringLength(13)]
        public string LotNo { get; set; }

        public double? GWT { get; set; }

        public double? CWT { get; set; }

        [StringLength(5)]
        public string Currency { get; set; }

        public double? Show { get; set; }

        public double? CTCT { get; set; }

        public double? Comm { get; set; }

        public double? ExComm { get; set; }

        public int? Issuer { get; set; }

        public int? Shipper { get; set; }

        public int? CNEE { get; set; }

        public int? CoLoad { get; set; }

        public int? Notify { get; set; }

        [StringLength(5)]
        public string WTUOM { get; set; }

        public int? PCS { get; set; }

        [StringLength(50)]
        public string SPINST { get; set; }

        [StringLength(50)]
        public string Status { get; set; }

        [StringLength(255)]
        public string PackagingDescription { get; set; }

        public int? PortOfDEPT { get; set; }

        public int? PortOfDSTN { get; set; }

        [StringLength(5)]
        public string PCSUOM { get; set; }

        [StringLength(5)]
        public string ClassRate { get; set; }

        [StringLength(20)]
        public string IssuerType { get; set; }

        [Required]
        [StringLength(10)]
        public string StationID { get; set; }

        [Required]
        [StringLength(6)]
        public string CreatedBy { get; set; }

        public DateTime CreatedDate { get; set; }

        [Required]
        [StringLength(6)]
        public string UpdatedBy { get; set; }

        public DateTime UpdatedDate { get; set; }

        [Column(TypeName = "timestamp")]
        [MaxLength(8)]
        [Timestamp]
        public byte[] Version { get; set; }

        [StringLength(10)]
        public string SStationID { get; set; }

        public double? VWT { get; set; }

        public int? SPL { get; set; }

        [StringLength(1)]
        public string ISAC { get; set; }

        [StringLength(6)]
        public string ISACBy { get; set; }

        public DateTime? ISACDate { get; set; }

        [StringLength(3)]
        public string DBID { get; set; }

        [StringLength(6)]
        public string DYMBy { get; set; }

        public DateTime? DYMDate { get; set; }
    }
}
