namespace ValuePlus.Domain.eChainVP
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("AEMAWBPrintLog")]
    public partial class AEMAWBPrintLog
    {
        public int ID { get; set; }

        public int MAWBID { get; set; }

        [Required]
        [StringLength(10)]
        public string UpdatedBy { get; set; }

        public DateTime UpdatedDate { get; set; }

        [Required]
        [StringLength(10)]
        public string StationID { get; set; }

        [Required]
        [StringLength(10)]
        public string CreateBy { get; set; }

        public DateTime? CreatedDate { get; set; }

        [StringLength(30)]
        public string ReportName { get; set; }
    }
}
