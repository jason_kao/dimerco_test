namespace ValuePlus.Domain.eChainVP
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("OESO")]
    public partial class OESO
    {
        public int ID { get; set; }

        [StringLength(5)]
        public string StationID { get; set; }

        [StringLength(5)]
        public string SoFor { get; set; }

        public int? HBLID { get; set; }

        public int? MBLID { get; set; }

        [StringLength(60)]
        public string SONo { get; set; }

        public int? PCS { get; set; }

        [StringLength(10)]
        public string PCSUOM { get; set; }

        public decimal? Weight { get; set; }

        [StringLength(10)]
        public string WeightUOM { get; set; }

        public decimal? CBM { get; set; }

        [StringLength(10)]
        public string CBMUOM { get; set; }

        [StringLength(20)]
        public string CTNRType { get; set; }

        [StringLength(40)]
        public string CTNRNo { get; set; }

        [StringLength(40)]
        public string SealNo { get; set; }

        public DateTime? ATA { get; set; }

        public string Marks { get; set; }

        public string Description { get; set; }

        [StringLength(50)]
        public string CreatedBy { get; set; }

        public DateTime? CreatedDT { get; set; }

        [StringLength(50)]
        public string UpdatedBy { get; set; }

        public DateTime? UpdatedDT { get; set; }

        [Column(TypeName = "timestamp")]
        [MaxLength(8)]
        [Timestamp]
        public byte[] Version { get; set; }

        public int? SrcID { get; set; }

        [StringLength(5)]
        public string SrcStationID { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? Height { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? Width { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? Length { get; set; }

        public decimal? Ton { get; set; }
    }
}
