namespace ValuePlus.Domain.eChainVP
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("AIEntry")]
    public partial class AIEntry
    {
        public int ID { get; set; }

        public int MAWBID { get; set; }

        [StringLength(20)]
        public string Damage { get; set; }

        [StringLength(20)]
        public string Shortage { get; set; }

        [StringLength(20)]
        public string Offload { get; set; }

        public DateTime? CustomsDate { get; set; }

        [StringLength(20)]
        public string CustomsEntry { get; set; }

        public int? EntryPort { get; set; }

        [StringLength(20)]
        public string FLTNo { get; set; }

        public int? CargoAt { get; set; }

        public int? TotalPCS { get; set; }

        public double? GWT { get; set; }

        [StringLength(20)]
        public string EntryType { get; set; }

        [StringLength(50)]
        public string Storage { get; set; }

        public DateTime? LandingDate { get; set; }

        public int? Carrier { get; set; }

        [Required]
        [StringLength(10)]
        public string StationID { get; set; }

        [Required]
        [StringLength(6)]
        public string CreatedBy { get; set; }

        public DateTime CreatedDate { get; set; }

        [Required]
        [StringLength(6)]
        public string UpdatedBy { get; set; }

        public DateTime UpdatedDate { get; set; }

        [Column(TypeName = "timestamp")]
        [MaxLength(8)]
        [Timestamp]
        public byte[] Version { get; set; }

        public int? Carriers { get; set; }

        public int? Avail { get; set; }

        [StringLength(255)]
        public string AvailAddr { get; set; }

        public DateTime? AvailDate { get; set; }
    }
}
