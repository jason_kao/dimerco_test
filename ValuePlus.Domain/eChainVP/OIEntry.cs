namespace ValuePlus.Domain.eChainVP
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("OIEntry")]
    public partial class OIEntry
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ID { get; set; }

        [Key]
        [Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int HBLID { get; set; }

        [Key]
        [Column(Order = 1)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int MBLID { get; set; }

        public decimal? PManifest { get; set; }

        public decimal? POutturn { get; set; }

        public decimal? PShort { get; set; }

        public decimal? PSurplus { get; set; }

        public decimal? PPillaged { get; set; }

        public decimal? PDamaged { get; set; }

        public decimal? WManifest { get; set; }

        public decimal? WOutturn { get; set; }

        public decimal? WShort { get; set; }

        public decimal? WSurplus { get; set; }

        public decimal? WPillaged { get; set; }

        public decimal? WDamaged { get; set; }

        public decimal? VManifest { get; set; }

        public decimal? VOutturn { get; set; }

        public decimal? VShort { get; set; }

        public decimal? VSurplus { get; set; }

        public decimal? VPillaged { get; set; }

        public decimal? VDamaged { get; set; }

        [StringLength(10)]
        public string CreatedBy { get; set; }

        public DateTime? CreatedDate { get; set; }

        [StringLength(10)]
        public string UpdatedBy { get; set; }

        public DateTime? UpdatedDate { get; set; }
    }
}
