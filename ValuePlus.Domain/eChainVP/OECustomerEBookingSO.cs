namespace ValuePlus.Domain.eChainVP
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("OECustomerEBookingSO")]
    public partial class OECustomerEBookingSO
    {
        public int ID { get; set; }

        [StringLength(5)]
        public string StationID { get; set; }

        public int? SrcID { get; set; }

        public int? BKID { get; set; }

        public int? HBLID { get; set; }

        public int? PCS { get; set; }

        [StringLength(10)]
        public string PCSUOM { get; set; }

        public decimal? Weight { get; set; }

        [StringLength(10)]
        public string WeightUOM { get; set; }

        public decimal? CBM { get; set; }

        [StringLength(10)]
        public string CBMUOM { get; set; }

        [StringLength(20)]
        public string CTNRType { get; set; }

        [StringLength(1000)]
        public string Marks { get; set; }

        [StringLength(1000)]
        public string Description { get; set; }

        [StringLength(50)]
        public string CreatedBy { get; set; }

        public DateTime? CreatedDT { get; set; }

        [StringLength(50)]
        public string UpdatedBy { get; set; }

        public DateTime? UpdatedDT { get; set; }
    }
}
