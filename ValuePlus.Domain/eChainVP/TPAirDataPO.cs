namespace ValuePlus.Domain.eChainVP
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("TPAirDataPO")]
    public partial class TPAirDataPO
    {
        public int ID { get; set; }

        [Required]
        [StringLength(10)]
        public string StationID { get; set; }

        [Required]
        [StringLength(15)]
        public string TPID { get; set; }

        [StringLength(50)]
        public string PONo { get; set; }

        public DateTime? Date { get; set; }

        public int? Quantity { get; set; }

        [StringLength(50)]
        public string PlaceOfDELV { get; set; }

        [StringLength(20)]
        public string Item { get; set; }

        [StringLength(20)]
        public string InvoiceNo { get; set; }

        [Column(TypeName = "timestamp")]
        [MaxLength(8)]
        [Timestamp]
        public byte[] Version { get; set; }

        [Required]
        [StringLength(6)]
        public string CreatedBy { get; set; }

        public DateTime CreatedDate { get; set; }

        [Required]
        [StringLength(6)]
        public string UpdatedBy { get; set; }

        public DateTime UpdatedDate { get; set; }

        public int? SourceID { get; set; }

        [StringLength(50)]
        public string POExtra1 { get; set; }

        [StringLength(50)]
        public string POExtra2 { get; set; }

        [StringLength(50)]
        public string POExtra3 { get; set; }

        [StringLength(50)]
        public string POExtra4 { get; set; }

        [StringLength(50)]
        public string POExtra5 { get; set; }

        [StringLength(50)]
        public string POExtra6 { get; set; }

        public DateTime? PODate { get; set; }

        [StringLength(20)]
        public string SONO { get; set; }

        public int? SCTNS { get; set; }

        [StringLength(4)]
        public string Curr { get; set; }

        public decimal? Amount { get; set; }

        [StringLength(60)]
        public string Remark { get; set; }

        public int? POMSID { get; set; }

        [StringLength(50)]
        public string UOM { get; set; }
    }
}
