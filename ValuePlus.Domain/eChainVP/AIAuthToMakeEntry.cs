namespace ValuePlus.Domain.eChainVP
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("AIAuthToMakeEntry")]
    public partial class AIAuthToMakeEntry
    {
        [Key]
        [Column(Order = 0)]
        public int ID { get; set; }

        [Key]
        [Column(Order = 1)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int HAWBID { get; set; }

        [Key]
        [Column(Order = 2)]
        [StringLength(6)]
        public string CreatedBy { get; set; }

        [Key]
        [Column(Order = 3)]
        public DateTime CreatedDate { get; set; }

        [Key]
        [Column(Order = 4)]
        [StringLength(6)]
        public string UpdatedBy { get; set; }

        [Key]
        [Column(Order = 5)]
        public DateTime UpdatedDate { get; set; }

        [Key]
        [Column(Order = 6, TypeName = "timestamp")]
        [MaxLength(8)]
        [Timestamp]
        public byte[] Version { get; set; }

        [StringLength(20)]
        public string ImportOn { get; set; }

        [StringLength(100)]
        public string VIA { get; set; }

        [StringLength(20)]
        public string From { get; set; }

        [StringLength(20)]
        public string FlightNO { get; set; }

        [StringLength(20)]
        public string MAWBNO { get; set; }

        [StringLength(20)]
        public string ITNo { get; set; }

        [StringLength(100)]
        public string EntryPort { get; set; }

        [StringLength(20)]
        public string LotNo { get; set; }

        [StringLength(20)]
        public string HAWBNo { get; set; }

        [StringLength(255)]
        public string FreightLoc { get; set; }

        [StringLength(100)]
        public string HAWB { get; set; }

        [StringLength(100)]
        public string SubHAWB { get; set; }

        [StringLength(255)]
        public string DESC { get; set; }

        [StringLength(100)]
        public string PCS { get; set; }

        [StringLength(100)]
        public string Weight { get; set; }

        [StringLength(100)]
        public string DueTo { get; set; }

        [StringLength(100)]
        public string Storage { get; set; }

        [StringLength(100)]
        public string Consignee { get; set; }

        [StringLength(100)]
        public string AuthoriseTo { get; set; }

        [StringLength(100)]
        public string AttorneyInFact { get; set; }

        [StringLength(100)]
        public string Broker { get; set; }

        [StringLength(100)]
        public string MCHEDImport { get; set; }

        [StringLength(20)]
        public string ITDate { get; set; }
    }
}
