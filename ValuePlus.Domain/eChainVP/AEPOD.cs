namespace ValuePlus.Domain.eChainVP
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("AEPOD")]
    public partial class AEPOD
    {
        public int ID { get; set; }

        public int HAWBID { get; set; }

        [StringLength(20)]
        public string FLTNo { get; set; }

        public DateTime? ATA { get; set; }

        [StringLength(20)]
        public string SCHReleaseNo { get; set; }

        public DateTime? SCHReleaseDate { get; set; }

        public int? SCHNotifyParty { get; set; }

        public DateTime? SCHNotifyDate { get; set; }

        public int? SCHDocReleaseParty { get; set; }

        public DateTime? SCHDocReleaseDate { get; set; }

        [StringLength(10)]
        public string SCHDocReleaseType { get; set; }

        [StringLength(10)]
        public string SCHNotifyType { get; set; }

        [StringLength(50)]
        public string SCHCargoSignBy { get; set; }

        public DateTime? SCHLicenseDate { get; set; }

        public DateTime? SCHCustomDate { get; set; }

        [StringLength(50)]
        public string SCHDocSignBy { get; set; }

        public int? SCHCargoReleaseParty { get; set; }

        public DateTime? SCHCargoReleaseDate { get; set; }

        [StringLength(10)]
        public string SCHCargoReleaseType { get; set; }

        [Required]
        [StringLength(10)]
        public string StationID { get; set; }

        [Required]
        [StringLength(6)]
        public string CreatedBy { get; set; }

        public DateTime CreatedDate { get; set; }

        [Required]
        [StringLength(6)]
        public string UpdatedBy { get; set; }

        public DateTime? UpdatedDate { get; set; }

        [Column(TypeName = "timestamp")]
        [MaxLength(8)]
        [Timestamp]
        public byte[] Version { get; set; }

        [StringLength(50)]
        public string PODTemplate { get; set; }

        public DateTime? SCHCargoTerminalDate { get; set; }
    }
}
