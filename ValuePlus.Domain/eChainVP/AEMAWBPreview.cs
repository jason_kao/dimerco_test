namespace ValuePlus.Domain.eChainVP
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("AEMAWBPreview")]
    public partial class AEMAWBPreview
    {
        public int ID { get; set; }

        [StringLength(255)]
        public string ShipperInfo { get; set; }

        public int? MAWBID { get; set; }

        [StringLength(255)]
        public string CNEEInfo { get; set; }

        [StringLength(255)]
        public string IssuingAgentInfo { get; set; }

        [StringLength(50)]
        public string AgentCode { get; set; }

        [StringLength(50)]
        public string PortOfDEPT { get; set; }

        [StringLength(255)]
        public string ArriveOne { get; set; }

        [StringLength(255)]
        public string ArriveTwo { get; set; }

        [StringLength(255)]
        public string ArriveThree { get; set; }

        [StringLength(255)]
        public string FLTOne { get; set; }

        [StringLength(255)]
        public string FLTTwo { get; set; }

        [StringLength(255)]
        public string FLTThree { get; set; }

        [StringLength(50)]
        public string PortOfDSTN { get; set; }

        [StringLength(255)]
        public string HandlingInfo { get; set; }

        [StringLength(255)]
        public string IssuerInfo { get; set; }

        [StringLength(255)]
        public string NotifyInfo { get; set; }

        [StringLength(3)]
        public string Currency { get; set; }

        [StringLength(2)]
        public string FrtPayType { get; set; }

        [StringLength(2)]
        public string OtherPayType { get; set; }

        [StringLength(255)]
        public string DecForCarriage { get; set; }

        [StringLength(255)]
        public string DecForCustoms { get; set; }

        [StringLength(255)]
        public string AmountOfIns { get; set; }

        [StringLength(255)]
        public string WeightCharge { get; set; }

        [StringLength(255)]
        public string TotalAmount { get; set; }

        [StringLength(255)]
        public string OtherCharges { get; set; }

        [StringLength(255)]
        public string SHPRCertify { get; set; }

        [StringLength(255)]
        public string CarrierCertify { get; set; }

        public int? PCS { get; set; }

        public double? GWT { get; set; }

        [StringLength(2)]
        public string WTUOM { get; set; }

        [StringLength(255)]
        public string RateClass { get; set; }

        public double? CWT { get; set; }

        public double? Rate { get; set; }

        [StringLength(255)]
        public string MAWBNo { get; set; }

        [StringLength(255)]
        public string NatureOfGoods { get; set; }

        [StringLength(255)]
        public string NatureOfGoods1 { get; set; }

        [StringLength(255)]
        public string NatureOfGoods2 { get; set; }

        [StringLength(255)]
        public string NatureOfGoods3 { get; set; }

        [StringLength(255)]
        public string NatureOfGoods4 { get; set; }

        [StringLength(255)]
        public string ShipperName { get; set; }

        [StringLength(255)]
        public string CNEEName { get; set; }

        [StringLength(255)]
        public string IssuingAgentName { get; set; }

        [StringLength(255)]
        public string NotifyName { get; set; }

        [StringLength(255)]
        public string PayTypeInfo { get; set; }

        [StringLength(255)]
        public string PrepaidDueAgent { get; set; }

        [StringLength(255)]
        public string CollectDueAgent { get; set; }

        [StringLength(255)]
        public string PrepaidDueCarrier { get; set; }

        [StringLength(255)]
        public string CollectDueCarrier { get; set; }

        [StringLength(255)]
        public string TotalPrepaid { get; set; }

        [StringLength(255)]
        public string TotalCollect { get; set; }

        [StringLength(255)]
        public string PrepaidValuation { get; set; }

        [StringLength(255)]
        public string CollectValuation { get; set; }

        [StringLength(255)]
        public string CollectTax { get; set; }

        [StringLength(255)]
        public string PrepaidTax { get; set; }

        public int? TotalPCS { get; set; }

        public double? TotalGWT { get; set; }

        public double? Amount { get; set; }

        [StringLength(255)]
        public string Marks { get; set; }

        public DateTime? IssueDate { get; set; }

        [Required]
        [StringLength(10)]
        public string StationID { get; set; }

        [Required]
        [StringLength(6)]
        public string CreatedBy { get; set; }

        public DateTime CreatedDate { get; set; }

        [Required]
        [StringLength(6)]
        public string UpdatedBy { get; set; }

        public DateTime UpdatedDate { get; set; }

        [Column(TypeName = "timestamp")]
        [MaxLength(8)]
        [Timestamp]
        public byte[] Version { get; set; }

        [StringLength(50)]
        public string AccountNo { get; set; }

        public int? CNEE_AltAddID { get; set; }

        public int? Shipper_AltAddID { get; set; }

        [StringLength(6)]
        public string PrintBy { get; set; }

        public DateTime? PrintDate { get; set; }

        [StringLength(25)]
        public string CANNo { get; set; }

        [StringLength(50)]
        public string FootShow { get; set; }

        [StringLength(10)]
        public string LotNo { get; set; }

        [StringLength(5)]
        public string TradeTerm { get; set; }
    }
}
