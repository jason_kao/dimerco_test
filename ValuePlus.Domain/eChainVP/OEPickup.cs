namespace ValuePlus.Domain.eChainVP
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("OEPickup")]
    public partial class OEPickup
    {
        public int ID { get; set; }

        [StringLength(5)]
        public string StationID { get; set; }

        public int? HBLID { get; set; }

        [StringLength(10)]
        public string PickupType { get; set; }

        public int? PickupFrom { get; set; }

        [StringLength(255)]
        public string PickupAddress { get; set; }

        public DateTime? PickupAvail { get; set; }

        public DateTime? PickupActual { get; set; }

        public int? DeliverTo { get; set; }

        [StringLength(255)]
        public string DeliverAddress { get; set; }

        public DateTime? DeliverETD { get; set; }

        public DateTime? DeliverATD { get; set; }

        [StringLength(100)]
        public string ContactPerson { get; set; }

        [StringLength(60)]
        public string Trucker { get; set; }

        [StringLength(255)]
        public string Remarks { get; set; }

        public int? PCS { get; set; }

        [StringLength(10)]
        public string PCSUOM { get; set; }

        public double? WT { get; set; }

        [StringLength(10)]
        public string WTUOM { get; set; }

        public double? CBM { get; set; }

        [StringLength(10)]
        public string CBMUOM { get; set; }

        [StringLength(255)]
        public string Marks { get; set; }

        [StringLength(255)]
        public string Description { get; set; }

        [StringLength(1)]
        public string NeedINV { get; set; }

        [StringLength(1)]
        public string NeedPL { get; set; }

        [StringLength(50)]
        public string CreatedBy { get; set; }

        public DateTime? CreatedDT { get; set; }

        [StringLength(50)]
        public string UpdatedBy { get; set; }

        public DateTime? UpdatedDT { get; set; }

        [Column(TypeName = "timestamp")]
        [MaxLength(8)]
        [Timestamp]
        public byte[] Version { get; set; }

        [StringLength(80)]
        public string ContactNumber { get; set; }

        [StringLength(60)]
        public string SONO { get; set; }

        [StringLength(100)]
        public string DeliveryContactPerson { get; set; }

        [StringLength(80)]
        public string DeliveryContactNumber { get; set; }

        [StringLength(60)]
        public string DeliveryTrucker { get; set; }

        [StringLength(1)]
        public string DeliveryNeedINV { get; set; }

        [StringLength(1)]
        public string DeliveryNeedPL { get; set; }

        [StringLength(255)]
        public string DeliveryRemarks { get; set; }

        [StringLength(20)]
        public string SOListType { get; set; }

        [StringLength(50)]
        public string OrderNo { get; set; }

        [StringLength(40)]
        public string CTNRNo { get; set; }

        [StringLength(40)]
        public string SealNo { get; set; }

        [StringLength(1)]
        public string NeedBooking { get; set; }

        [StringLength(1)]
        public string DeliveryNeedBooking { get; set; }

        public int? SOListCount { get; set; }

        [StringLength(100)]
        public string PickupFromName { get; set; }

        [StringLength(100)]
        public string DeliverToName { get; set; }

        public int? PickupFromTWO { get; set; }

        [StringLength(100)]
        public string PickupFromNameTWO { get; set; }

        [StringLength(255)]
        public string PickupAddressTWO { get; set; }

        public DateTime? PickupAvailTWO { get; set; }

        public DateTime? PickupActualTWO { get; set; }

        [StringLength(100)]
        public string PickupContactPersonTWO { get; set; }

        [StringLength(80)]
        public string PickupContactNumberTWO { get; set; }

        [StringLength(60)]
        public string PickupTruckerTWO { get; set; }

        [StringLength(1)]
        public string PickupNeedINVTWO { get; set; }

        [StringLength(1)]
        public string PickupNeedPLTWO { get; set; }

        [StringLength(1)]
        public string PickupNeedBookingTWO { get; set; }

        [StringLength(255)]
        public string PickupRemarksTWO { get; set; }

        public int? DeliveryToTWO { get; set; }

        [StringLength(100)]
        public string DeliveryToNameTWO { get; set; }

        [StringLength(255)]
        public string DeliveryAddressTWO { get; set; }

        public DateTime? DeliveryAvailTWO { get; set; }

        public DateTime? DeliveryActualTWO { get; set; }

        [StringLength(100)]
        public string DeliveryContactPersonTWO { get; set; }

        [StringLength(80)]
        public string DeliveryContactNumberTWO { get; set; }

        [StringLength(60)]
        public string DeliveryTruckerTWO { get; set; }

        [StringLength(1)]
        public string DeliveryNeedINVTWO { get; set; }

        [StringLength(1)]
        public string DeliveryNeedPLTWO { get; set; }

        [StringLength(1)]
        public string DeliveryNeedBookingTWO { get; set; }

        [StringLength(255)]
        public string DeliveryRemarksTWO { get; set; }
    }
}
