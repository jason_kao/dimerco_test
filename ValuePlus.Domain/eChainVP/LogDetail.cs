namespace ValuePlus.Domain.eChainVP
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("LogDetail")]
    public partial class LogDetail
    {
        public int Id { get; set; }

        [Required]
        [StringLength(50)]
        public string StationId { get; set; }

        public Guid PageId { get; set; }

        [Required]
        public string Url { get; set; }

        [Required]
        [StringLength(50)]
        public string Type { get; set; }

        public double LoadSeconds { get; set; }

        public DateTime? CreateTime { get; set; }
    }
}
