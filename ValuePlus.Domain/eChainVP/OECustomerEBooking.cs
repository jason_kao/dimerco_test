namespace ValuePlus.Domain.eChainVP
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("OECustomerEBooking")]
    public partial class OECustomerEBooking
    {
        public int ID { get; set; }

        public int? CustID { get; set; }

        public int? SrcID { get; set; }

        public int? HBLID { get; set; }

        [StringLength(10)]
        public string StationID { get; set; }

        [StringLength(20)]
        public string BookingNo { get; set; }

        public int? PLoading { get; set; }

        public DateTime? PLoadingETD { get; set; }

        public int? PDelivery { get; set; }

        [StringLength(20)]
        public string MoveType { get; set; }

        [StringLength(50)]
        public string NatureOfGoodsType { get; set; }

        [StringLength(10)]
        public string OPType1 { get; set; }

        [StringLength(10)]
        public string OPType3 { get; set; }

        [StringLength(50)]
        public string OPType3Detail { get; set; }

        [StringLength(50)]
        public string OPType3Remark { get; set; }

        [StringLength(10)]
        public string OPType2 { get; set; }

        [StringLength(50)]
        public string OPTypeDetail { get; set; }

        [StringLength(20)]
        public string FreightPayType { get; set; }

        [StringLength(20)]
        public string TradeType { get; set; }

        [StringLength(50)]
        public string ClientPIC { get; set; }

        [StringLength(50)]
        public string ClientEmail { get; set; }

        [StringLength(50)]
        public string ClientTel { get; set; }

        [StringLength(50)]
        public string ClientFax { get; set; }

        [StringLength(1)]
        public string IsSelfConsol { get; set; }

        public int? ConsolGroup { get; set; }

        [StringLength(50)]
        public string CreatedBy { get; set; }

        public DateTime? CreatedDate { get; set; }

        [StringLength(50)]
        public string UpdatedBy { get; set; }

        public DateTime? UpdatedDate { get; set; }

        [StringLength(5)]
        public string HBLType { get; set; }

        public DateTime? CargoReadyDate { get; set; }
    }
}
