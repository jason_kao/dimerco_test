namespace ValuePlus.Domain.eChainVP
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("AEColoadBooking")]
    public partial class AEColoadBooking
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ID { get; set; }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int BookingID { get; set; }

        public int? ColoadID { get; set; }

        [StringLength(10)]
        public string LinkStation { get; set; }

        public int? DEPT { get; set; }

        public int? DSTN { get; set; }

        public int? Shipper { get; set; }

        [StringLength(100)]
        public string ShipperName { get; set; }

        [StringLength(200)]
        public string ShipperAddress { get; set; }

        public int? ShipperMap { get; set; }

        public int? CNEE { get; set; }

        [StringLength(100)]
        public string CNEEName { get; set; }

        [StringLength(200)]
        public string CNEEAddress { get; set; }

        public int? CNEEMap { get; set; }

        public int? Notify { get; set; }

        [StringLength(100)]
        public string NotifyName { get; set; }

        [StringLength(200)]
        public string NotifyAddress { get; set; }

        public int? NotifyMap { get; set; }

        public DateTime? AvailDate { get; set; }

        public DateTime? DeptDate { get; set; }

        public DateTime? ArriDate { get; set; }

        public int? PCS { get; set; }

        [StringLength(20)]
        public string PCSUOM { get; set; }

        public decimal? GWT { get; set; }

        [StringLength(20)]
        public string GWTUOM { get; set; }

        public decimal? Volume { get; set; }

        [StringLength(20)]
        public string VolumeUOM { get; set; }

        [StringLength(200)]
        public string Descript { get; set; }

        [StringLength(200)]
        public string ExportLIC { get; set; }

        [StringLength(200)]
        public string CommINV { get; set; }

        [StringLength(200)]
        public string Quantity { get; set; }

        [StringLength(500)]
        public string Remarks { get; set; }

        [StringLength(500)]
        public string Marks { get; set; }

        [StringLength(50)]
        public string CreatedBy { get; set; }

        public DateTime? CreatedDate { get; set; }

        [StringLength(50)]
        public string UpdatedBy { get; set; }

        public DateTime? UpdatedDate { get; set; }

        public DateTime? ReceivedDate { get; set; }

        public DateTime? ProcessedDate { get; set; }

        [StringLength(10)]
        public string Status { get; set; }

        [StringLength(10)]
        public string AWBType { get; set; }

        public int? HAWBID { get; set; }

        [StringLength(50)]
        public string ServiceLevel { get; set; }

        [StringLength(50)]
        public string DocType { get; set; }

        [StringLength(50)]
        public string Rate { get; set; }

        [StringLength(50)]
        public string Carrier { get; set; }
    }
}
