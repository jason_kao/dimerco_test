namespace ValuePlus.Domain.eChainVP
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("TPAIRData")]
    public partial class TPAIRData
    {
        public int ID { get; set; }

        [Required]
        [StringLength(3)]
        public string StationID { get; set; }

        [Required]
        [StringLength(15)]
        public string TPID { get; set; }

        public int CustomerID { get; set; }

        [StringLength(3)]
        public string SourceStationID { get; set; }

        [StringLength(1)]
        public string SourceType { get; set; }

        public int? SourceID { get; set; }

        [Required]
        [StringLength(30)]
        public string MasterNo { get; set; }

        [Required]
        [StringLength(30)]
        public string HouseNo { get; set; }

        [StringLength(30)]
        public string Lot { get; set; }

        public int? Shipper { get; set; }

        public int? Cnee { get; set; }

        public int? Receipt { get; set; }

        public int? Delivery { get; set; }

        public int? ORGN { get; set; }

        public int? DSTN { get; set; }

        [StringLength(30)]
        public string NetureOfGood { get; set; }

        public int? ActPCS { get; set; }

        [StringLength(10)]
        public string ActPCSUOM { get; set; }

        public double? CWT { get; set; }

        [StringLength(5)]
        public string WTUOM { get; set; }

        public int? SalesPerson { get; set; }

        public DateTime? ATD { get; set; }

        public DateTime? ATA { get; set; }

        [Required]
        [StringLength(1)]
        public string LotStatus { get; set; }

        [Required]
        [StringLength(1)]
        public string IsManiFast { get; set; }

        [Column(TypeName = "timestamp")]
        [MaxLength(8)]
        [Timestamp]
        public byte[] Version { get; set; }

        [StringLength(6)]
        public string CreatedBy { get; set; }

        public DateTime? CreatedDate { get; set; }

        [Required]
        [StringLength(6)]
        public string UpdatedBy { get; set; }

        public DateTime UpdatedDate { get; set; }

        public DateTime? IssueDate { get; set; }

        [StringLength(20)]
        public string ServiceLevel { get; set; }

        [StringLength(20)]
        public string Scale { get; set; }

        [Column(TypeName = "smalldatetime")]
        public DateTime? PickupDate { get; set; }

        public double? VWT { get; set; }

        [StringLength(30)]
        public string PreMaster { get; set; }

        [StringLength(50)]
        public string ConfirmID { get; set; }

        [StringLength(10)]
        public string ShptType { get; set; }

        [StringLength(50)]
        public string OnboardFLT { get; set; }

        [StringLength(1)]
        public string isDAS { get; set; }

        [StringLength(255)]
        public string DESC { get; set; }

        [StringLength(255)]
        public string Remark { get; set; }

        [StringLength(255)]
        public string Marks { get; set; }
    }
}
