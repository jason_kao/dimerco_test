namespace ValuePlus.Domain.eChainVP
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("OIBooking")]
    public partial class OIBooking
    {
        public int ID { get; set; }

        [StringLength(20)]
        public string BookingNo { get; set; }

        [StringLength(5)]
        public string StationID { get; set; }

        [StringLength(5)]
        public string SrcStationID { get; set; }

        public int? SrcID { get; set; }

        public int? HBLID { get; set; }

        public int? PCS { get; set; }

        public int? Customer { get; set; }

        public int? SHPR { get; set; }

        public int? CNEE { get; set; }

        public int? NTFY { get; set; }

        [StringLength(60)]
        public string OceanVessel { get; set; }

        public int? PReceipt { get; set; }

        public int? PLoading { get; set; }

        public int? PDischarge { get; set; }

        public int? PDelivery { get; set; }

        [StringLength(1)]
        public string IsISF { get; set; }

        [StringLength(1)]
        public string IsAMS { get; set; }

        [StringLength(1)]
        public string IsCoLoadIn { get; set; }

        [StringLength(255)]
        public string BookingRemark { get; set; }

        [StringLength(20)]
        public string ShptType { get; set; }

        [StringLength(20)]
        public string MoveType { get; set; }

        [StringLength(20)]
        public string FreightPayType { get; set; }

        [StringLength(10)]
        public string FCalculateType { get; set; }

        [StringLength(20)]
        public string TradeType { get; set; }

        public DateTime? PReceiptETD { get; set; }

        public DateTime? PLoadingETD { get; set; }

        public DateTime? PDischargeETD { get; set; }

        public DateTime? PDeliveryETD { get; set; }

        public DateTime? CargoReadyDate { get; set; }

        public DateTime? BookingCutOffDate { get; set; }

        [StringLength(10)]
        public string AssignTo { get; set; }

        [StringLength(10)]
        public string SalesType { get; set; }

        [StringLength(100)]
        public string CTNRInWord { get; set; }

        [StringLength(10)]
        public string DecValueCurr { get; set; }

        [StringLength(50)]
        public string TelexRelease { get; set; }

        [StringLength(1)]
        public string IsDraft { get; set; }

        [StringLength(1)]
        public string IsVoid { get; set; }

        [StringLength(5)]
        public string DSTNStationID { get; set; }

        public int? DSTNID { get; set; }

        [StringLength(50)]
        public string BookingSendBy { get; set; }

        public DateTime? BookingSendDT { get; set; }

        [StringLength(250)]
        public string QuoteType { get; set; }

        [StringLength(50)]
        public string CreatedBy { get; set; }

        public DateTime? CreatedDT { get; set; }

        [StringLength(50)]
        public string UpdatedBy { get; set; }

        public DateTime? UpdatedDT { get; set; }

        [StringLength(50)]
        public string Status { get; set; }

        [StringLength(255)]
        public string StatusDetail { get; set; }

        [StringLength(50)]
        public string QuoteNo { get; set; }

        [Column(TypeName = "timestamp")]
        [MaxLength(8)]
        [Timestamp]
        public byte[] Version { get; set; }

        [StringLength(80)]
        public string OceanVoyage { get; set; }
    }
}
