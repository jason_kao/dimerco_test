namespace ValuePlus.Domain.eChainVP
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("AIITEntry")]
    public partial class AIITEntry
    {
        public int ID { get; set; }

        public int? ForwardingType { get; set; }

        [StringLength(8)]
        public string ForwardingITNo { get; set; }

        public int? TransportBy { get; set; }

        [StringLength(20)]
        public string BillProNo { get; set; }

        public int? IEServiceBy { get; set; }

        [StringLength(12)]
        public string IECarrierAWBNo { get; set; }

        public DateTime? ETD { get; set; }

        public DateTime? ETA { get; set; }

        public DateTime? ATD { get; set; }

        public DateTime? ATA { get; set; }

        public int MAWBID { get; set; }

        [Required]
        [StringLength(10)]
        public string StationID { get; set; }

        [Required]
        [StringLength(6)]
        public string CreatedBy { get; set; }

        public DateTime CreatedDate { get; set; }

        [Required]
        [StringLength(6)]
        public string UpdatedBy { get; set; }

        public DateTime UpdatedDate { get; set; }

        [Column(TypeName = "timestamp")]
        [MaxLength(8)]
        [Timestamp]
        public byte[] Version { get; set; }
    }
}
