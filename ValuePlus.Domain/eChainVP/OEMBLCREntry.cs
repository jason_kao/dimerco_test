namespace ValuePlus.Domain.eChainVP
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("OEMBLCREntry")]
    public partial class OEMBLCREntry
    {
        public int ID { get; set; }

        public int? MBLID { get; set; }

        [StringLength(5)]
        public string MBLType { get; set; }

        [StringLength(5)]
        public string StationID { get; set; }

        [StringLength(50)]
        public string FAXTO { get; set; }

        [StringLength(50)]
        public string CompanyATTN { get; set; }

        [StringLength(50)]
        public string CompanyATTNTEL { get; set; }

        [StringLength(50)]
        public string CompanyATTNFAX { get; set; }

        public DateTime? PrintDate { get; set; }

        [StringLength(50)]
        public string HBLNO { get; set; }

        [StringLength(50)]
        public string DeliveryNo { get; set; }

        [StringLength(50)]
        public string OperateType { get; set; }

        [StringLength(100)]
        public string VSLVOY { get; set; }

        [StringLength(50)]
        public string MBLNO { get; set; }

        [StringLength(50)]
        public string CartonType { get; set; }

        [StringLength(20)]
        public string POL { get; set; }

        [StringLength(50)]
        public string DEST { get; set; }

        [StringLength(20)]
        public string ETD { get; set; }

        [StringLength(255)]
        public string ReMark { get; set; }

        [StringLength(255)]
        public string Extra1 { get; set; }

        [StringLength(255)]
        public string Extra2 { get; set; }

        [StringLength(255)]
        public string Extra3 { get; set; }

        public DateTime? CreatedDT { get; set; }

        [StringLength(50)]
        public string CreatedBy { get; set; }

        public DateTime? UpdatedDT { get; set; }

        [StringLength(50)]
        public string UpdatedBy { get; set; }
    }
}
