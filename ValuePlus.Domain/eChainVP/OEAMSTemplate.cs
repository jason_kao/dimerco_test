namespace ValuePlus.Domain.eChainVP
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("OEAMSTemplate")]
    public partial class OEAMSTemplate
    {
        public int ID { get; set; }

        public int? TemplateID { get; set; }

        [StringLength(50)]
        public string TemplateName { get; set; }

        [StringLength(255)]
        public string SHPRName { get; set; }

        [StringLength(255)]
        public string SHPRAddress1 { get; set; }

        [StringLength(255)]
        public string SHPRAddress2 { get; set; }

        [StringLength(50)]
        public string SHPRCity { get; set; }

        [StringLength(50)]
        public string SHPRState { get; set; }

        [StringLength(10)]
        public string SHPRZip { get; set; }

        [StringLength(50)]
        public string SHPRPhone { get; set; }

        [StringLength(50)]
        public string SHPRCountry { get; set; }

        [StringLength(255)]
        public string SHPRContactName { get; set; }

        [StringLength(255)]
        public string CNEEName { get; set; }

        [StringLength(255)]
        public string CNEEAddress1 { get; set; }

        [StringLength(255)]
        public string CNEEAddress2 { get; set; }

        [StringLength(50)]
        public string CNEECity { get; set; }

        [StringLength(50)]
        public string CNEEState { get; set; }

        [StringLength(10)]
        public string CNEEZip { get; set; }

        [StringLength(50)]
        public string CNEEPhone { get; set; }

        [StringLength(50)]
        public string CNEECountry { get; set; }

        [StringLength(255)]
        public string CNEEContactName { get; set; }

        [StringLength(255)]
        public string NTFYName { get; set; }

        [StringLength(255)]
        public string NTFYAddress1 { get; set; }

        [StringLength(255)]
        public string NTFYAddress2 { get; set; }

        [StringLength(50)]
        public string NTFYCity { get; set; }

        [StringLength(50)]
        public string NTFYState { get; set; }

        [StringLength(10)]
        public string NTFYZip { get; set; }

        [StringLength(50)]
        public string NTFYPhone { get; set; }

        [StringLength(50)]
        public string NTFYCountry { get; set; }

        [StringLength(255)]
        public string NTFYContactName { get; set; }

        [StringLength(255)]
        public string NTFY2Name { get; set; }

        [StringLength(255)]
        public string NTFY2Address1 { get; set; }

        [StringLength(255)]
        public string NTFY2Address2 { get; set; }

        [StringLength(50)]
        public string NTFY2City { get; set; }

        [StringLength(50)]
        public string NTFY2State { get; set; }

        [StringLength(50)]
        public string NTFY2Zip { get; set; }

        [StringLength(50)]
        public string NTFY2Phone { get; set; }

        [StringLength(50)]
        public string NTFY2Country { get; set; }

        [StringLength(255)]
        public string NTFY2ContactName { get; set; }

        [StringLength(1)]
        public string IsVoid { get; set; }
    }
}
