namespace ValuePlus.Domain.eChainVP
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("OIINBondENDouane")]
    public partial class OIINBondENDouane
    {
        public int ID { get; set; }

        [StringLength(50)]
        public string HBLID { get; set; }

        [StringLength(5)]
        public string Mode { get; set; }

        [StringLength(5)]
        public string StationID { get; set; }

        [StringLength(200)]
        public string StationName { get; set; }

        [StringLength(200)]
        public string StationAddress { get; set; }

        [StringLength(50)]
        public string StationTel { get; set; }

        [StringLength(50)]
        public string StationFAX { get; set; }

        [StringLength(50)]
        public string USPort { get; set; }

        [StringLength(50)]
        public string InTransit { get; set; }

        [StringLength(50)]
        public string ManifestFrom { get; set; }

        [StringLength(50)]
        public string ManifestTo { get; set; }

        [StringLength(250)]
        public string CNEEInfo { get; set; }

        [StringLength(250)]
        public string SHPRInfo { get; set; }

        [StringLength(50)]
        public string AcquittalNo { get; set; }

        [StringLength(50)]
        public string CarrierCode { get; set; }

        [StringLength(50)]
        public string CargoControlNo { get; set; }

        [StringLength(50)]
        public string PreviousCargoControlNo { get; set; }

        [StringLength(150)]
        public string PKGS { get; set; }

        [StringLength(500)]
        public string Marks { get; set; }

        [StringLength(150)]
        public string WT { get; set; }

        [StringLength(50)]
        public string LotNoAndHBLNo { get; set; }

        [StringLength(50)]
        public string PLanding { get; set; }

        [StringLength(50)]
        public string Carrier { get; set; }

        [StringLength(500)]
        public string CustomsClearBy { get; set; }

        [StringLength(500)]
        public string CustomsTEL { get; set; }

        [StringLength(50)]
        public string CustomsFAX { get; set; }

        [StringLength(500)]
        public string LocationGoods { get; set; }

        [StringLength(50)]
        public string LocationTEL { get; set; }

        [StringLength(50)]
        public string LocationFAX { get; set; }

        [StringLength(50)]
        public string Vehicle { get; set; }

        [StringLength(500)]
        public string CartageBy { get; set; }

        [StringLength(50)]
        public string ETA { get; set; }

        [StringLength(50)]
        public string Storage { get; set; }

        [StringLength(50)]
        public string CreatedBy { get; set; }

        public DateTime? CreatedDT { get; set; }

        [StringLength(50)]
        public string UpdatedBy { get; set; }

        public DateTime? UpdatedDT { get; set; }

        [StringLength(250)]
        public string ReportName { get; set; }
    }
}
