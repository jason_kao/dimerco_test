namespace ValuePlus.Domain.eChainVP
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("AIHAWBDocRequired")]
    public partial class AIHAWBDocRequired
    {
        public int ID { get; set; }

        public int? DocID { get; set; }

        public int PickupID { get; set; }

        [StringLength(50)]
        public string DocName { get; set; }

        public bool Confirmed { get; set; }

        public bool IsPickup { get; set; }

        [Required]
        [StringLength(10)]
        public string StationID { get; set; }

        [Required]
        [StringLength(6)]
        public string CreatedBy { get; set; }

        public DateTime CreatedDate { get; set; }

        [Required]
        [StringLength(6)]
        public string UpdatedBy { get; set; }

        public DateTime UpdatedDate { get; set; }

        [Column(TypeName = "timestamp")]
        [MaxLength(8)]
        [Timestamp]
        public byte[] Version { get; set; }

        [StringLength(50)]
        public string DocOtherName { get; set; }
    }
}
