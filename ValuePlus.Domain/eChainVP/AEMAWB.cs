namespace ValuePlus.Domain.eChainVP
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("AEMAWB")]
    public partial class AEMAWB
    {
        public int ID { get; set; }

        [Required]
        [StringLength(20)]
        public string MAWBNo { get; set; }

        [StringLength(5)]
        public string Other { get; set; }

        [StringLength(5)]
        public string FRT { get; set; }

        public int? MAWBType { get; set; }

        public int? IssueCity { get; set; }

        public DateTime? IssueDate { get; set; }

        [StringLength(13)]
        public string LotNo { get; set; }

        public double? GWT { get; set; }

        public double? CWT { get; set; }

        [StringLength(5)]
        public string Currency { get; set; }

        public double? Show { get; set; }

        public double? CTCT { get; set; }

        public double? Comm { get; set; }

        public double? ExComm { get; set; }

        public int? Issuer { get; set; }

        public int? Shipper { get; set; }

        public int? CNEE { get; set; }

        public int? Notify { get; set; }

        [StringLength(5)]
        public string WTUOM { get; set; }

        [StringLength(5)]
        public string PCSUOM { get; set; }

        public int? PCS { get; set; }

        [StringLength(5)]
        public string ClassRate { get; set; }

        [StringLength(50)]
        public string SPINST { get; set; }

        [StringLength(10)]
        public string Status { get; set; }

        [StringLength(255)]
        public string PackagingDescription { get; set; }

        public int? PortOfDEPT { get; set; }

        public int? PortOfDSTN { get; set; }

        public int? PrintPerson { get; set; }

        public double? DueToAgent { get; set; }

        public double? DueToCarrier { get; set; }

        [StringLength(6)]
        public string PreAlertBy { get; set; }

        public DateTime? LastPreAlertTime { get; set; }

        public DateTime? LasteManifestTime { get; set; }

        [StringLength(6)]
        public string eManifestBy { get; set; }

        [StringLength(10)]
        public string IssuerType { get; set; }

        [StringLength(50)]
        public string isAMS { get; set; }

        [Required]
        [StringLength(10)]
        public string StationID { get; set; }

        [Required]
        [StringLength(6)]
        public string CreatedBy { get; set; }

        public DateTime CreatedDate { get; set; }

        [Required]
        [StringLength(6)]
        public string UpdatedBy { get; set; }

        public DateTime UpdatedDate { get; set; }

        [Column(TypeName = "timestamp")]
        [MaxLength(8)]
        [Timestamp]
        public byte[] Version { get; set; }

        [StringLength(50)]
        public string CYMNo { get; set; }

        public double? VWT { get; set; }

        public int? SPL { get; set; }

        [StringLength(3)]
        public string DBID { get; set; }

        [StringLength(10)]
        public string AMSBy { get; set; }

        public DateTime? AMSDate { get; set; }

        [Required]
        [StringLength(1)]
        public string ReWeight { get; set; }

        [StringLength(10)]
        public string WeightBy { get; set; }

        public DateTime? WeightDate { get; set; }
    }
}
