namespace ValuePlus.Domain.eChainVP
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("OEColoadBooking")]
    public partial class OEColoadBooking
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ID { get; set; }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int BookingID { get; set; }

        public int? ColoadID { get; set; }

        [StringLength(10)]
        public string LinkStation { get; set; }

        public int? PReceipt { get; set; }

        public int? PLoading { get; set; }

        public int? PDischarge { get; set; }

        public int? PDelivery { get; set; }

        public int? Shipper { get; set; }

        [StringLength(100)]
        public string ShipperName { get; set; }

        [StringLength(200)]
        public string ShipperAddress { get; set; }

        public int? ShipperMap { get; set; }

        public int? CNEE { get; set; }

        [StringLength(100)]
        public string CNEEName { get; set; }

        [StringLength(200)]
        public string CNEEAddress { get; set; }

        public int? CNEEMap { get; set; }

        public int? Notify { get; set; }

        [StringLength(100)]
        public string NotifyName { get; set; }

        [StringLength(200)]
        public string NotifyAddress { get; set; }

        public int? NotifyMap { get; set; }

        public int? AlsoNotify { get; set; }

        [StringLength(100)]
        public string AlsoNotifyName { get; set; }

        [StringLength(200)]
        public string AlsoNotifyAddress { get; set; }

        public int? AlsoNotifyMap { get; set; }

        public int? Truck { get; set; }

        [StringLength(100)]
        public string TruckName { get; set; }

        [StringLength(200)]
        public string TruckAddress { get; set; }

        public int? TruckMap { get; set; }

        [StringLength(200)]
        public string CargoLoadingLocation { get; set; }

        public DateTime? PReceiptETD { get; set; }

        public DateTime? PLoadingETD { get; set; }

        public DateTime? PDischargeETD { get; set; }

        public DateTime? PDeliveryETD { get; set; }

        public DateTime? PLoadingATD { get; set; }

        public DateTime? FinalDestETD { get; set; }

        public DateTime? CargoAvailableDate { get; set; }

        [StringLength(255)]
        public string Instruction { get; set; }

        [StringLength(20)]
        public string MoveType { get; set; }

        [StringLength(20)]
        public string FreightPayType { get; set; }

        [StringLength(50)]
        public string NatureOfGoodsType { get; set; }

        [StringLength(20)]
        public string LoadType { get; set; }

        [StringLength(50)]
        public string LastMilestone { get; set; }

        public DateTime? MilestoneDate1 { get; set; }

        public DateTime? MilestoneDate2 { get; set; }

        public DateTime? MilestoneDate3 { get; set; }

        public DateTime? MilestoneDate4 { get; set; }

        [StringLength(50)]
        public string MBLNO { get; set; }

        public int? PCS { get; set; }

        [StringLength(10)]
        public string PCSUOM { get; set; }

        public decimal? Weight { get; set; }

        [StringLength(10)]
        public string WeightUOM { get; set; }

        public decimal? CBM { get; set; }

        [StringLength(10)]
        public string CBMUOM { get; set; }

        public int? SOCount { get; set; }

        [StringLength(20)]
        public string CTNRType { get; set; }

        [StringLength(1000)]
        public string Marks { get; set; }

        [StringLength(1000)]
        public string Description { get; set; }

        [StringLength(10)]
        public string Send { get; set; }

        [StringLength(50)]
        public string CreatedBy { get; set; }

        public DateTime? CreatedDate { get; set; }

        [StringLength(50)]
        public string UpdatedBy { get; set; }

        public DateTime? UpdatedDate { get; set; }

        public DateTime? ReceivedDate { get; set; }

        public DateTime? ProcessedDate { get; set; }

        [StringLength(10)]
        public string Status { get; set; }

        [StringLength(10)]
        public string OWBType { get; set; }

        public int? HBLID { get; set; }
    }
}
