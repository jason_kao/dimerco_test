namespace ValuePlus.Domain.eChainVP
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class OEAMS
    {
        public int ID { get; set; }

        public int? HBLID { get; set; }

        [StringLength(10)]
        public string IssuerSCAC { get; set; }

        [StringLength(50)]
        public string PreCarrierReceipt { get; set; }

        [StringLength(20)]
        public string StatusIndicator { get; set; }

        [StringLength(8)]
        public string AmendmentCode { get; set; }

        [StringLength(20)]
        public string BookingNo { get; set; }

        [StringLength(40)]
        public string SplitBill { get; set; }

        [StringLength(15)]
        public string HandlingGroup { get; set; }

        [StringLength(20)]
        public string SNPPartyCode { get; set; }

        [StringLength(10)]
        public string AdditionalSNPParties { get; set; }

        [StringLength(10)]
        public string NotifyPartyCode2 { get; set; }

        public int? TotalNetWeight { get; set; }

        [StringLength(50)]
        public string TotalNetWeightUOM { get; set; }

        public int? TotalVolume { get; set; }

        [StringLength(10)]
        public string TotalVolumeUOM { get; set; }

        [StringLength(20)]
        public string HarmonizedCode { get; set; }

        public int? CashValue { get; set; }

        public DateTime? CreatedDT { get; set; }

        [StringLength(50)]
        public string CreatedBy { get; set; }

        public DateTime? UpdatedDT { get; set; }

        [StringLength(50)]
        public string UpdatedBy { get; set; }

        [Column(TypeName = "timestamp")]
        [MaxLength(8)]
        [Timestamp]
        public byte[] Version { get; set; }

        [StringLength(60)]
        public string OceanVessel { get; set; }

        [StringLength(60)]
        public string OceanVoyage { get; set; }

        [StringLength(100)]
        public string VesselName { get; set; }

        [StringLength(50)]
        public string SCACCode { get; set; }

        [StringLength(50)]
        public string VesselFlag { get; set; }

        [StringLength(50)]
        public string PDischarge { get; set; }

        [StringLength(50)]
        public string PLoading { get; set; }

        public DateTime? PDischargeETD { get; set; }

        public DateTime? PLoadingETD { get; set; }

        [StringLength(20)]
        public string HBLNo { get; set; }

        [StringLength(50)]
        public string PDelivery { get; set; }

        [StringLength(50)]
        public string PReceipt { get; set; }

        [StringLength(50)]
        public string CoLoader { get; set; }

        [StringLength(20)]
        public string MBLNO { get; set; }

        [StringLength(20)]
        public string AgencyUniqueCode { get; set; }

        [StringLength(255)]
        public string SHPRName { get; set; }

        [StringLength(255)]
        public string SHPRAddress1 { get; set; }

        [StringLength(255)]
        public string SHPRAddress2 { get; set; }

        [StringLength(50)]
        public string SHPRCity { get; set; }

        [StringLength(50)]
        public string SHPRState { get; set; }

        [StringLength(10)]
        public string SHPRZip { get; set; }

        [StringLength(50)]
        public string SHPRPhone { get; set; }

        [StringLength(50)]
        public string SHPRCountry { get; set; }

        [StringLength(255)]
        public string SHPRContactName { get; set; }

        [StringLength(255)]
        public string CNEEName { get; set; }

        [StringLength(255)]
        public string CNEEAddress1 { get; set; }

        [StringLength(255)]
        public string CNEEAddress2 { get; set; }

        [StringLength(50)]
        public string CNEECity { get; set; }

        [StringLength(50)]
        public string CNEEState { get; set; }

        [StringLength(10)]
        public string CNEEZip { get; set; }

        [StringLength(50)]
        public string CNEEPhone { get; set; }

        [StringLength(50)]
        public string CNEECountry { get; set; }

        [StringLength(255)]
        public string CNEEContactName { get; set; }

        [StringLength(255)]
        public string NTFYName { get; set; }

        [StringLength(255)]
        public string NTFYAddress1 { get; set; }

        [StringLength(255)]
        public string NTFYAddress2 { get; set; }

        [StringLength(50)]
        public string NTFYCity { get; set; }

        [StringLength(50)]
        public string NTFYState { get; set; }

        [StringLength(10)]
        public string NTFYZip { get; set; }

        [StringLength(50)]
        public string NTFYPhone { get; set; }

        [StringLength(50)]
        public string NTFYCountry { get; set; }

        [StringLength(255)]
        public string NTFYContactName { get; set; }

        [StringLength(50)]
        public string HazardCode { get; set; }

        [StringLength(20)]
        public string HazardCodeQualifier { get; set; }

        [StringLength(30)]
        public string UVICode { get; set; }

        [StringLength(5)]
        public string UnladingForeignPort { get; set; }

        [StringLength(1)]
        public string ISF5INDICATOR { get; set; }

        [StringLength(11)]
        public string commoditycode { get; set; }

        [StringLength(5)]
        public string length { get; set; }

        [StringLength(8)]
        public string width { get; set; }

        [StringLength(8)]
        public string height { get; set; }

        [StringLength(4)]
        public string ISOequipmenttype { get; set; }

        [StringLength(1)]
        public string loadedempty { get; set; }

        [StringLength(2)]
        public string equipmentdesccode { get; set; }

        [StringLength(10)]
        public string AdditionalSNPParties2 { get; set; }

        [StringLength(10)]
        public string NotifyPartyCode1 { get; set; }

        public int? commodityitemnumber { get; set; }

        [StringLength(50)]
        public string CNTRNo { get; set; }

        [StringLength(20)]
        public string SealNumber1 { get; set; }

        [StringLength(20)]
        public string SealNumber2 { get; set; }

        public int? PCS { get; set; }

        [StringLength(10)]
        public string PCSUOM { get; set; }

        public int? NetWeight { get; set; }

        [StringLength(10)]
        public string NetWeightUOM { get; set; }

        [StringLength(5)]
        public string LoadOrEmpty { get; set; }

        [StringLength(10)]
        public string TypeOfService { get; set; }

        [StringLength(20)]
        public string CountryOfOrigin { get; set; }

        [StringLength(1000)]
        public string Description { get; set; }

        [StringLength(1000)]
        public string Marks { get; set; }

        [StringLength(10)]
        public string SmallestPackageType { get; set; }

        [StringLength(255)]
        public string NTFY2Name { get; set; }

        [StringLength(255)]
        public string NTFY2Address1 { get; set; }

        [StringLength(255)]
        public string NTFY2Address2 { get; set; }

        [StringLength(50)]
        public string NTFY2City { get; set; }

        [StringLength(50)]
        public string NTFY2State { get; set; }

        [StringLength(50)]
        public string NTFY2Zip { get; set; }

        [StringLength(50)]
        public string NTFY2Phone { get; set; }

        [StringLength(50)]
        public string NTFY2Country { get; set; }

        [StringLength(255)]
        public string NTFY2ContactName { get; set; }
    }
}
