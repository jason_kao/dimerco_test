namespace ValuePlus.Domain.eChainVP
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("OIHBL")]
    public partial class OIHBL
    {
        public int ID { get; set; }

        [StringLength(20)]
        public string HBLNo { get; set; }

        [StringLength(5)]
        public string StationID { get; set; }

        [StringLength(5)]
        public string SrcStationID { get; set; }

        public int? SrcID { get; set; }

        public int? MBLID { get; set; }

        public int? PCS { get; set; }

        public int? Customer { get; set; }

        public int? SHPR { get; set; }

        public int? CNEE { get; set; }

        public int? NTFY { get; set; }

        public int? ThirdParty { get; set; }

        public int? PReceipt { get; set; }

        public int? PLoading { get; set; }

        public int? PDischarge { get; set; }

        public int? PDelivery { get; set; }

        public int? FinalDest { get; set; }

        public int? CoLoader { get; set; }

        public int? Carrier { get; set; }

        public int? FWDAgent { get; set; }

        public int? DESTAgent { get; set; }

        [StringLength(255)]
        public string Instruction { get; set; }

        [StringLength(20)]
        public string ShptType { get; set; }

        [StringLength(20)]
        public string MoveType { get; set; }

        [StringLength(20)]
        public string FreightPayType { get; set; }

        [StringLength(20)]
        public string TradeType { get; set; }

        public DateTime? PReceiptETD { get; set; }

        public DateTime? PLoadingETD { get; set; }

        public DateTime? PDischargeETD { get; set; }

        public DateTime? PDeliveryETD { get; set; }

        public DateTime? FinalDestETD { get; set; }

        public DateTime? PLoadingATD { get; set; }

        public int? SalesPerson { get; set; }

        [StringLength(10)]
        public string SalesType { get; set; }

        [StringLength(100)]
        public string ExportRef { get; set; }

        [StringLength(100)]
        public string CTNRInWord { get; set; }

        public double? DecValue { get; set; }

        [StringLength(10)]
        public string DecValueCurr { get; set; }

        [StringLength(50)]
        public string TelexRelease { get; set; }

        [StringLength(1)]
        public string IsDraft { get; set; }

        [StringLength(1)]
        public string IsBookmark { get; set; }

        [StringLength(1)]
        public string IsVoid { get; set; }

        [StringLength(50)]
        public string SHNumOfDR { get; set; }

        [StringLength(100)]
        public string USOTILic { get; set; }

        [StringLength(100)]
        public string USSVILic { get; set; }

        [StringLength(80)]
        public string USExportRef { get; set; }

        [StringLength(60)]
        public string USDeclValue { get; set; }

        [StringLength(50)]
        public string CreatedBy { get; set; }

        public DateTime? CreatedDT { get; set; }

        [StringLength(50)]
        public string UpdatedBy { get; set; }

        public DateTime? UpdatedDT { get; set; }

        [Column(TypeName = "timestamp")]
        [MaxLength(8)]
        [Timestamp]
        public byte[] Version { get; set; }

        [StringLength(50)]
        public string PreAlertBy { get; set; }

        public DateTime? PreAlertDT { get; set; }

        public DateTime? TelexReleaseDT { get; set; }

        public int? OBLIssued { get; set; }

        [StringLength(10)]
        public string FCalculateType { get; set; }

        [StringLength(30)]
        public string ServiceType { get; set; }

        [StringLength(250)]
        public string QuoteType { get; set; }

        [StringLength(30)]
        public string CYMBy { get; set; }

        public DateTime? CYMDT { get; set; }

        public int? FRTLocation { get; set; }

        [StringLength(50)]
        public string AMSBLNo { get; set; }

        [StringLength(50)]
        public string ITNo { get; set; }

        [StringLength(50)]
        public string NatureOfGoodsType { get; set; }

        public int? AlsoNTFY { get; set; }

        [StringLength(10)]
        public string IMPStation { get; set; }

        public int? ITPort { get; set; }

        public DateTime? ITDate { get; set; }

        [StringLength(15)]
        public string MTSNO { get; set; }

        public int? FinalLocation { get; set; }

        public int? Broker { get; set; }

        [StringLength(50)]
        public string Extra0 { get; set; }

        [StringLength(50)]
        public string Extra1 { get; set; }

        [StringLength(50)]
        public string Extra2 { get; set; }

        [StringLength(50)]
        public string Extra3 { get; set; }

        [StringLength(50)]
        public string Extra4 { get; set; }

        [StringLength(50)]
        public string Extra5 { get; set; }

        [StringLength(50)]
        public string Extra6 { get; set; }

        [StringLength(50)]
        public string Extra7 { get; set; }

        [StringLength(50)]
        public string Extra8 { get; set; }

        [StringLength(50)]
        public string Extra9 { get; set; }

        [StringLength(20)]
        public string BookingNo { get; set; }

        [StringLength(20)]
        public string CustBookingNo { get; set; }

        [StringLength(20)]
        public string UNNumber { get; set; }

        public DateTime? GoodsAvailableDate { get; set; }

        public DateTime? StorageStartDate { get; set; }

        public DateTime? PReceiptATD { get; set; }

        public DateTime? PDeliveryATD { get; set; }

        public decimal? RevenueTon { get; set; }

        [StringLength(40)]
        public string CCN { get; set; }
    }
}
