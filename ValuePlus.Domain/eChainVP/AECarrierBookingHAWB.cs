namespace ValuePlus.Domain.eChainVP
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("AECarrierBookingHAWB")]
    public partial class AECarrierBookingHAWB
    {
        public int ID { get; set; }

        public int HAWBID { get; set; }

        public int DEPT { get; set; }

        public int DSTN { get; set; }

        [StringLength(20)]
        public string BookedFLT { get; set; }

        [StringLength(20)]
        public string OnboardFLT { get; set; }

        public DateTime? ETD { get; set; }

        public DateTime? ETA { get; set; }

        public DateTime? ATA { get; set; }

        public DateTime? ATD { get; set; }

        [Required]
        [StringLength(10)]
        public string StationID { get; set; }

        [Required]
        [StringLength(6)]
        public string CreatedBy { get; set; }

        public DateTime CreatedDate { get; set; }

        [StringLength(6)]
        public string UpdatedBy { get; set; }

        public DateTime? UpdatedDate { get; set; }

        [Column(TypeName = "timestamp")]
        [MaxLength(8)]
        [Timestamp]
        public byte[] Version { get; set; }
    }
}
