namespace ValuePlus.Domain.eChainVP
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("OIPOD")]
    public partial class OIPOD
    {
        public int ID { get; set; }

        [StringLength(5)]
        public string StationID { get; set; }

        public int? HBLID { get; set; }

        public DateTime? DocumentPickDT { get; set; }

        public DateTime? NotifyPartyDT { get; set; }

        public int? NotifyParty { get; set; }

        public DateTime? CustomsReleaseDT { get; set; }

        [StringLength(60)]
        public string CustomsReleaseNo { get; set; }

        public DateTime? DocumentReleaseDT { get; set; }

        public int? DocumentReleaseParty { get; set; }

        [StringLength(20)]
        public string DocumentReleaseType { get; set; }

        public DateTime? FreightReleaseDT { get; set; }

        public int? FreightReleaseParty { get; set; }

        [StringLength(20)]
        public string FreightReleaseType { get; set; }

        [StringLength(50)]
        public string CreatedBy { get; set; }

        public DateTime? CreatedDT { get; set; }

        [StringLength(50)]
        public string UpdatedBy { get; set; }

        public DateTime? UpdatedDT { get; set; }

        [Column(TypeName = "timestamp")]
        [MaxLength(8)]
        [Timestamp]
        public byte[] Version { get; set; }

        [StringLength(20)]
        public string NotifyPartyType { get; set; }

        public DateTime? ContainerUnstuffDT { get; set; }

        [StringLength(500)]
        public string Remarks { get; set; }
    }
}
