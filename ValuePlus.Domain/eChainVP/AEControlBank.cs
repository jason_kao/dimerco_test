namespace ValuePlus.Domain.eChainVP
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("AEControlBank")]
    public partial class AEControlBank
    {
        public int ID { get; set; }

        [Required]
        [StringLength(20)]
        public string MAWBNo { get; set; }

        [StringLength(12)]
        public string LotNo { get; set; }

        public int Issuer { get; set; }

        public DateTime? AssignedDate { get; set; }

        public DateTime ReceivedDate { get; set; }

        public int BorrowIn { get; set; }

        public int? BorrowOutAgent { get; set; }

        public DateTime? BorrowOutDate { get; set; }

        [StringLength(6)]
        public string BorrowOutBy { get; set; }

        public int City { get; set; }

        public DateTime? UsedDate { get; set; }

        [StringLength(6)]
        public string UsedBy { get; set; }

        [StringLength(20)]
        public string Status { get; set; }

        [Required]
        [StringLength(10)]
        public string StationID { get; set; }

        [Required]
        [StringLength(6)]
        public string CreatedBy { get; set; }

        public DateTime CreatedDate { get; set; }

        [Required]
        [StringLength(6)]
        public string UpdatedBy { get; set; }

        public DateTime UpdatedDate { get; set; }

        [Column(TypeName = "timestamp")]
        [MaxLength(8)]
        [Timestamp]
        public byte[] Version { get; set; }
    }
}
