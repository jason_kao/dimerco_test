namespace ValuePlus.Domain.eChainVP
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("OEFLading")]
    public partial class OEFLading
    {
        public int ID { get; set; }

        [StringLength(5)]
        public string StationID { get; set; }

        public int? HBLID { get; set; }

        [StringLength(20)]
        public string HBLNo { get; set; }

        [StringLength(100)]
        public string SONumber { get; set; }

        [StringLength(50)]
        public string FMCNumber { get; set; }

        [StringLength(100)]
        public string SHPR { get; set; }

        [StringLength(100)]
        public string CNEE { get; set; }

        [StringLength(100)]
        public string DESTAgent { get; set; }

        [StringLength(100)]
        public string NTFY { get; set; }

        [StringLength(200)]
        public string AlsoNTFY { get; set; }

        [StringLength(100)]
        public string ExportReference { get; set; }

        [StringLength(50)]
        public string Origin { get; set; }

        [StringLength(30)]
        public string PreCarriageBy { get; set; }

        [StringLength(60)]
        public string PReceipt { get; set; }

        [StringLength(100)]
        public string Vessel { get; set; }

        [StringLength(100)]
        public string Voyage { get; set; }

        [StringLength(40)]
        public string PLoading { get; set; }

        [StringLength(40)]
        public string PDischarge { get; set; }

        [StringLength(10)]
        public string Movement { get; set; }

        [StringLength(40)]
        public string PDelivery { get; set; }

        [StringLength(120)]
        public string CTNRInWords { get; set; }

        [StringLength(60)]
        public string PayAt { get; set; }

        [StringLength(60)]
        public string DateOnBoard { get; set; }

        [StringLength(10)]
        public string NoOfOrgBL { get; set; }

        [StringLength(60)]
        public string IssuePlaceDate { get; set; }

        [StringLength(15)]
        public string DeclareValue { get; set; }

        [Column(TypeName = "ntext")]
        public string CTNRNo { get; set; }

        [StringLength(500)]
        public string PCS { get; set; }

        [Column(TypeName = "ntext")]
        public string GoodsDesc { get; set; }

        [StringLength(500)]
        public string GWT { get; set; }

        [StringLength(500)]
        public string Measurement { get; set; }

        public DateTime? CreatedDT { get; set; }

        [StringLength(50)]
        public string CreatedBy { get; set; }

        public DateTime? UpdatedDT { get; set; }

        [StringLength(50)]
        public string UpdatedBy { get; set; }

        [StringLength(5)]
        public string OEType { get; set; }

        [StringLength(150)]
        public string StationInfo { get; set; }

        [StringLength(500)]
        public string TariffCharge { get; set; }

        [StringLength(500)]
        public string TariffCollect { get; set; }

        [StringLength(500)]
        public string TariffCollectTotal { get; set; }

        [StringLength(500)]
        public string TariffItemNo { get; set; }

        [StringLength(500)]
        public string TariffPrepaid { get; set; }

        [StringLength(500)]
        public string TariffPrepaidTotal { get; set; }

        [Column(TypeName = "ntext")]
        public string Marks { get; set; }

        [StringLength(50)]
        public string SumPCS { get; set; }

        [StringLength(50)]
        public string SumGWT { get; set; }

        [StringLength(50)]
        public string SumCBM { get; set; }

        [StringLength(500)]
        public string HBLRemarks { get; set; }

        [StringLength(50)]
        public string HBLConfirmationSendBy { get; set; }

        public DateTime? HBLConfirmationSendDT { get; set; }

        [StringLength(50)]
        public string PrintBy { get; set; }

        [StringLength(10)]
        public string PDeliveryCode { get; set; }

        public DateTime? FeedbackDT { get; set; }

        public DateTime? ReturnDT { get; set; }

        public DateTime? PrintDT { get; set; }

        public int? CNEE_AltAddID { get; set; }

        public int? SHPR_AltAddID { get; set; }
    }
}
