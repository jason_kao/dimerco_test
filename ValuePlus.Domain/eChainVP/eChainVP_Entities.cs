namespace ValuePlus.Domain.eChainVP
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    public partial class eChainVP_Entities : DbContext
    {
        public eChainVP_Entities()
            : base("name=eChainVP")
        {
        }

        public virtual DbSet<AEAPPHAWBNo> AEAPPHAWBNo { get; set; }
        public virtual DbSet<AEAWBSUM> AEAWBSUM { get; set; }
        public virtual DbSet<AEBook> AEBook { get; set; }
        public virtual DbSet<AEBookConfig> AEBookConfig { get; set; }
        public virtual DbSet<AEBookConsole> AEBookConsole { get; set; }
        public virtual DbSet<AEBookConsoleLoad> AEBookConsoleLoad { get; set; }
        public virtual DbSet<AEBookConsoleStation> AEBookConsoleStation { get; set; }
        public virtual DbSet<AECarrierBooking> AECarrierBooking { get; set; }
        public virtual DbSet<AECarrierBookingHAWB> AECarrierBookingHAWB { get; set; }
        public virtual DbSet<AEColoadBooking> AEColoadBooking { get; set; }
        public virtual DbSet<AEControlBank> AEControlBank { get; set; }
        public virtual DbSet<AEHAWB> AEHAWB { get; set; }
        public virtual DbSet<AEHAWBDim> AEHAWBDim { get; set; }
        public virtual DbSet<AEHAWBDocRequired> AEHAWBDocRequired { get; set; }
        public virtual DbSet<AEHAWBINST> AEHAWBINST { get; set; }
        public virtual DbSet<AEHAWBPickup> AEHAWBPickup { get; set; }
        public virtual DbSet<AEHAWBPO> AEHAWBPO { get; set; }
        public virtual DbSet<AEHAWBPreview> AEHAWBPreview { get; set; }
        public virtual DbSet<AEHAWBPrintLog> AEHAWBPrintLog { get; set; }
        public virtual DbSet<AEHAWBSUM> AEHAWBSUM { get; set; }
        public virtual DbSet<AEMAWB> AEMAWB { get; set; }
        public virtual DbSet<AEMAWBDim> AEMAWBDim { get; set; }
        public virtual DbSet<AEMAWBINST> AEMAWBINST { get; set; }
        public virtual DbSet<AEMAWBPreview> AEMAWBPreview { get; set; }
        public virtual DbSet<AEMAWBPrintLog> AEMAWBPrintLog { get; set; }
        public virtual DbSet<AEPOD> AEPOD { get; set; }
        public virtual DbSet<AEPODData> AEPODData { get; set; }
        public virtual DbSet<AEPODMilestone> AEPODMilestone { get; set; }
        public virtual DbSet<AEReConfirmLog> AEReConfirmLog { get; set; }
        public virtual DbSet<AIAWBSUM> AIAWBSUM { get; set; }
        public virtual DbSet<AICarrierBooking> AICarrierBooking { get; set; }
        public virtual DbSet<AIEntry> AIEntry { get; set; }
        public virtual DbSet<AIEntryDT> AIEntryDT { get; set; }
        public virtual DbSet<AIHAWB> AIHAWB { get; set; }
        public virtual DbSet<AIHAWBAgentInfo> AIHAWBAgentInfo { get; set; }
        public virtual DbSet<AIHAWBAgentInfoMap> AIHAWBAgentInfoMap { get; set; }
        public virtual DbSet<AIHAWBDim> AIHAWBDim { get; set; }
        public virtual DbSet<AIHAWBDocRequired> AIHAWBDocRequired { get; set; }
        public virtual DbSet<AIHAWBINST> AIHAWBINST { get; set; }
        public virtual DbSet<AIHAWBPickup> AIHAWBPickup { get; set; }
        public virtual DbSet<AIHAWBPO> AIHAWBPO { get; set; }
        public virtual DbSet<AIHAWBPreview> AIHAWBPreview { get; set; }
        public virtual DbSet<AIHAWBPrintLog> AIHAWBPrintLog { get; set; }
        public virtual DbSet<AIITBank> AIITBank { get; set; }
        public virtual DbSet<AIITDT> AIITDT { get; set; }
        public virtual DbSet<AIITEntry> AIITEntry { get; set; }
        public virtual DbSet<AIMAWB> AIMAWB { get; set; }
        public virtual DbSet<AIMAWBDim> AIMAWBDim { get; set; }
        public virtual DbSet<AIMAWBINST> AIMAWBINST { get; set; }
        public virtual DbSet<AIMAWBPreview> AIMAWBPreview { get; set; }
        public virtual DbSet<AIMAWBPrintLog> AIMAWBPrintLog { get; set; }
        public virtual DbSet<AIPODMilestone> AIPODMilestone { get; set; }
        public virtual DbSet<BOLogDetails> BOLogDetails { get; set; }
        public virtual DbSet<LogDetail> LogDetail { get; set; }
        public virtual DbSet<OEACI> OEACI { get; set; }
        public virtual DbSet<OEAMS> OEAMS { get; set; }
        public virtual DbSet<OEAMSContainer> OEAMSContainer { get; set; }
        public virtual DbSet<OEAMSTemplate> OEAMSTemplate { get; set; }
        public virtual DbSet<OEAWBSUM> OEAWBSUM { get; set; }
        public virtual DbSet<OEBookingConfirmation> OEBookingConfirmation { get; set; }
        public virtual DbSet<OEBookingRemark> OEBookingRemark { get; set; }
        public virtual DbSet<OEColoadBooking> OEColoadBooking { get; set; }
        public virtual DbSet<OECustomerEBooking> OECustomerEBooking { get; set; }
        public virtual DbSet<OECustomerEBookingInfoMap> OECustomerEBookingInfoMap { get; set; }
        public virtual DbSet<OECustomerEBookingPO> OECustomerEBookingPO { get; set; }
        public virtual DbSet<OECustomerEBookingSO> OECustomerEBookingSO { get; set; }
        public virtual DbSet<OEFLading> OEFLading { get; set; }
        public virtual DbSet<OEFLadingRet> OEFLadingRet { get; set; }
        public virtual DbSet<OEFLadingSORet> OEFLadingSORet { get; set; }
        public virtual DbSet<OEFTPLading> OEFTPLading { get; set; }
        public virtual DbSet<OEHBL> OEHBL { get; set; }
        public virtual DbSet<OEHBLWHEntry> OEHBLWHEntry { get; set; }
        public virtual DbSet<OEHBLWS> OEHBLWS { get; set; }
        public virtual DbSet<OEHBLWSTemplate> OEHBLWSTemplate { get; set; }
        public virtual DbSet<OELoadingPlan> OELoadingPlan { get; set; }
        public virtual DbSet<OEMBL> OEMBL { get; set; }
        public virtual DbSet<OEMBLCREntry> OEMBLCREntry { get; set; }
        public virtual DbSet<OEOrderPreview> OEOrderPreview { get; set; }
        public virtual DbSet<OEPickup> OEPickup { get; set; }
        public virtual DbSet<OEPO> OEPO { get; set; }
        public virtual DbSet<OEPOD> OEPOD { get; set; }
        public virtual DbSet<OESO> OESO { get; set; }
        public virtual DbSet<OETariff> OETariff { get; set; }
        public virtual DbSet<OIArrivalNotice> OIArrivalNotice { get; set; }
        public virtual DbSet<OIAWBSUM> OIAWBSUM { get; set; }
        public virtual DbSet<OIBooking> OIBooking { get; set; }
        public virtual DbSet<OIBookingPO> OIBookingPO { get; set; }
        public virtual DbSet<OIBookingRemark> OIBookingRemark { get; set; }
        public virtual DbSet<OIBookingSO> OIBookingSO { get; set; }
        public virtual DbSet<OIEntry> OIEntry { get; set; }
        public virtual DbSet<OIFLading> OIFLading { get; set; }
        public virtual DbSet<OIHBL> OIHBL { get; set; }
        public virtual DbSet<OIHBLAgentInfo> OIHBLAgentInfo { get; set; }
        public virtual DbSet<OIHBLAgentInfoMap> OIHBLAgentInfoMap { get; set; }
        public virtual DbSet<OIINBondENDouane> OIINBondENDouane { get; set; }
        public virtual DbSet<OIMBL> OIMBL { get; set; }
        public virtual DbSet<OIPickup> OIPickup { get; set; }
        public virtual DbSet<OIPO> OIPO { get; set; }
        public virtual DbSet<OIPOD> OIPOD { get; set; }
        public virtual DbSet<OISO> OISO { get; set; }
        public virtual DbSet<TPAAWBSUM> TPAAWBSUM { get; set; }
        public virtual DbSet<TPAIRData> TPAIRData { get; set; }
        public virtual DbSet<TPAirDataPO> TPAirDataPO { get; set; }
        public virtual DbSet<TPOAWBSUM> TPOAWBSUM { get; set; }
        public virtual DbSet<TPOceanData> TPOceanData { get; set; }
        public virtual DbSet<TPOceanDataContainer> TPOceanDataContainer { get; set; }
        public virtual DbSet<TPOceanDataPO> TPOceanDataPO { get; set; }
        public virtual DbSet<TPOceanPreview> TPOceanPreview { get; set; }
        public virtual DbSet<AEBarCode> AEBarCode { get; set; }
        public virtual DbSet<AEBookConsoleDimension> AEBookConsoleDimension { get; set; }
        public virtual DbSet<AIAuthToMakeEntry> AIAuthToMakeEntry { get; set; }
        public virtual DbSet<OEAMSISFMailLog> OEAMSISFMailLog { get; set; }
        public virtual DbSet<OEDR> OEDR { get; set; }
        public virtual DbSet<OEHBLWSAssignRecord> OEHBLWSAssignRecord { get; set; }
        public virtual DbSet<OITurnoverLetter> OITurnoverLetter { get; set; }
        public virtual DbSet<OLAPTemp> OLAPTemp { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<AEAPPHAWBNo>()
                .Property(e => e.HAWBNO)
                .IsUnicode(false);

            modelBuilder.Entity<AEAPPHAWBNo>()
                .Property(e => e.StationID)
                .IsUnicode(false);

            modelBuilder.Entity<AEAPPHAWBNo>()
                .Property(e => e.Status)
                .IsFixedLength();

            modelBuilder.Entity<AEAPPHAWBNo>()
                .Property(e => e.CreatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<AEAPPHAWBNo>()
                .Property(e => e.UpdatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<AEAPPHAWBNo>()
                .Property(e => e.Version)
                .IsFixedLength();

            modelBuilder.Entity<AEAWBSUM>()
                .Property(e => e.ReportCurrency)
                .IsUnicode(false);

            modelBuilder.Entity<AEAWBSUM>()
                .Property(e => e.LocalCurrency)
                .IsUnicode(false);

            modelBuilder.Entity<AEAWBSUM>()
                .Property(e => e.STATIONID)
                .IsUnicode(false);

            modelBuilder.Entity<AEAWBSUM>()
                .Property(e => e.IDType)
                .IsUnicode(false);

            modelBuilder.Entity<AEAWBSUM>()
                .Property(e => e.Version)
                .IsFixedLength();

            modelBuilder.Entity<AEAWBSUM>()
                .Property(e => e.CreateBy)
                .IsUnicode(false);

            modelBuilder.Entity<AEAWBSUM>()
                .Property(e => e.UpdatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<AEBook>()
                .Property(e => e.Area)
                .IsUnicode(false);

            modelBuilder.Entity<AEBook>()
                .Property(e => e.BookingVolume)
                .HasPrecision(18, 3);

            modelBuilder.Entity<AEBook>()
                .Property(e => e.DSTNStationID)
                .IsUnicode(false);

            modelBuilder.Entity<AEBook>()
                .Property(e => e.CreatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<AEBook>()
                .Property(e => e.UpdatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<AEBook>()
                .Property(e => e.LoadStation1)
                .IsUnicode(false);

            modelBuilder.Entity<AEBook>()
                .Property(e => e.LoadStation2)
                .IsUnicode(false);

            modelBuilder.Entity<AEBook>()
                .Property(e => e.LoadCarrier2)
                .IsUnicode(false);

            modelBuilder.Entity<AEBook>()
                .Property(e => e.BookSend)
                .IsUnicode(false);

            modelBuilder.Entity<AEBook>()
                .Property(e => e.WHDimension)
                .IsUnicode(false);

            modelBuilder.Entity<AEBook>()
                .Property(e => e.BookingPCSUOM)
                .IsUnicode(false);

            modelBuilder.Entity<AEBook>()
                .Property(e => e.BookingWTUOM)
                .IsUnicode(false);

            modelBuilder.Entity<AEBook>()
                .Property(e => e.BookingVolumeUOM)
                .IsUnicode(false);

            modelBuilder.Entity<AEBookConfig>()
                .Property(e => e.Status)
                .IsUnicode(false);

            modelBuilder.Entity<AEBookConfig>()
                .Property(e => e.CreatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<AEBookConfig>()
                .Property(e => e.UpdatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<AEBookConsole>()
                .Property(e => e.StationID)
                .IsUnicode(false);

            modelBuilder.Entity<AEBookConsole>()
                .Property(e => e.OrgStationID)
                .IsUnicode(false);

            modelBuilder.Entity<AEBookConsole>()
                .Property(e => e.Area)
                .IsUnicode(false);

            modelBuilder.Entity<AEBookConsole>()
                .Property(e => e.DEPT)
                .IsUnicode(false);

            modelBuilder.Entity<AEBookConsole>()
                .Property(e => e.DSTN)
                .IsUnicode(false);

            modelBuilder.Entity<AEBookConsole>()
                .Property(e => e.BookingPCSUOM)
                .IsUnicode(false);

            modelBuilder.Entity<AEBookConsole>()
                .Property(e => e.ShipperID)
                .IsUnicode(false);

            modelBuilder.Entity<AEBookConsole>()
                .Property(e => e.Carrier)
                .IsUnicode(false);

            modelBuilder.Entity<AEBookConsole>()
                .Property(e => e.BookingNo)
                .IsUnicode(false);

            modelBuilder.Entity<AEBookConsole>()
                .Property(e => e.CreatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<AEBookConsole>()
                .Property(e => e.UpdatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<AEBookConsole>()
                .Property(e => e.LoadCarrier)
                .IsUnicode(false);

            modelBuilder.Entity<AEBookConsole>()
                .Property(e => e.LoadVolume)
                .HasPrecision(18, 3);

            modelBuilder.Entity<AEBookConsole>()
                .Property(e => e.LoadCreatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<AEBookConsole>()
                .Property(e => e.LoadUpdatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<AEBookConsole>()
                .Property(e => e.WHVolume)
                .HasPrecision(18, 3);

            modelBuilder.Entity<AEBookConsole>()
                .Property(e => e.WHCreatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<AEBookConsole>()
                .Property(e => e.WHUpdatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<AEBookConsole>()
                .Property(e => e.WHDimension)
                .IsUnicode(false);

            modelBuilder.Entity<AEBookConsole>()
                .Property(e => e.CSCreatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<AEBookConsole>()
                .Property(e => e.CSUpdatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<AEBookConsole>()
                .Property(e => e.RTHandBookCourier)
                .IsUnicode(false);

            modelBuilder.Entity<AEBookConsole>()
                .Property(e => e.RTHexiaoDanCourier)
                .IsUnicode(false);

            modelBuilder.Entity<AEBookConsole>()
                .Property(e => e.RTTuiShuiCourier)
                .IsUnicode(false);

            modelBuilder.Entity<AEBookConsole>()
                .Property(e => e.RTCreatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<AEBookConsole>()
                .Property(e => e.RTUpdatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<AEBookConsole>()
                .Property(e => e.Status)
                .IsUnicode(false);

            modelBuilder.Entity<AEBookConsole>()
                .Property(e => e.AWBType)
                .IsUnicode(false);

            modelBuilder.Entity<AEBookConsole>()
                .Property(e => e.BookSend)
                .IsUnicode(false);

            modelBuilder.Entity<AEBookConsole>()
                .Property(e => e.RTStatus)
                .IsUnicode(false);

            modelBuilder.Entity<AEBookConsoleLoad>()
                .Property(e => e.LoadCarrier)
                .IsUnicode(false);

            modelBuilder.Entity<AEBookConsoleLoad>()
                .Property(e => e.LoadArea)
                .IsUnicode(false);

            modelBuilder.Entity<AEBookConsoleLoad>()
                .Property(e => e.LoadVolume)
                .HasPrecision(18, 3);

            modelBuilder.Entity<AEBookConsoleLoad>()
                .Property(e => e.CreatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<AEBookConsoleLoad>()
                .Property(e => e.UpdatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<AEBookConsoleLoad>()
                .Property(e => e.Status)
                .IsUnicode(false);

            modelBuilder.Entity<AEBookConsoleStation>()
                .Property(e => e.ConsoleStationID)
                .IsUnicode(false);

            modelBuilder.Entity<AEBookConsoleStation>()
                .Property(e => e.Status)
                .IsUnicode(false);

            modelBuilder.Entity<AEBookConsoleStation>()
                .Property(e => e.CreatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<AEBookConsoleStation>()
                .Property(e => e.UpdatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<AECarrierBooking>()
                .Property(e => e.BookedFLT)
                .IsUnicode(false);

            modelBuilder.Entity<AECarrierBooking>()
                .Property(e => e.OnboardFLT)
                .IsUnicode(false);

            modelBuilder.Entity<AECarrierBooking>()
                .Property(e => e.StationID)
                .IsUnicode(false);

            modelBuilder.Entity<AECarrierBooking>()
                .Property(e => e.CreatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<AECarrierBooking>()
                .Property(e => e.UpdatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<AECarrierBooking>()
                .Property(e => e.Version)
                .IsFixedLength();

            modelBuilder.Entity<AECarrierBookingHAWB>()
                .Property(e => e.BookedFLT)
                .IsUnicode(false);

            modelBuilder.Entity<AECarrierBookingHAWB>()
                .Property(e => e.OnboardFLT)
                .IsUnicode(false);

            modelBuilder.Entity<AECarrierBookingHAWB>()
                .Property(e => e.StationID)
                .IsUnicode(false);

            modelBuilder.Entity<AECarrierBookingHAWB>()
                .Property(e => e.CreatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<AECarrierBookingHAWB>()
                .Property(e => e.UpdatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<AECarrierBookingHAWB>()
                .Property(e => e.Version)
                .IsFixedLength();

            modelBuilder.Entity<AEColoadBooking>()
                .Property(e => e.LinkStation)
                .IsUnicode(false);

            modelBuilder.Entity<AEColoadBooking>()
                .Property(e => e.PCSUOM)
                .IsUnicode(false);

            modelBuilder.Entity<AEColoadBooking>()
                .Property(e => e.GWTUOM)
                .IsUnicode(false);

            modelBuilder.Entity<AEColoadBooking>()
                .Property(e => e.Volume)
                .HasPrecision(18, 3);

            modelBuilder.Entity<AEColoadBooking>()
                .Property(e => e.VolumeUOM)
                .IsUnicode(false);

            modelBuilder.Entity<AEColoadBooking>()
                .Property(e => e.CreatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<AEColoadBooking>()
                .Property(e => e.UpdatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<AEColoadBooking>()
                .Property(e => e.Status)
                .IsUnicode(false);

            modelBuilder.Entity<AEColoadBooking>()
                .Property(e => e.AWBType)
                .IsUnicode(false);

            modelBuilder.Entity<AEControlBank>()
                .Property(e => e.MAWBNo)
                .IsUnicode(false);

            modelBuilder.Entity<AEControlBank>()
                .Property(e => e.LotNo)
                .IsUnicode(false);

            modelBuilder.Entity<AEControlBank>()
                .Property(e => e.BorrowOutBy)
                .IsUnicode(false);

            modelBuilder.Entity<AEControlBank>()
                .Property(e => e.UsedBy)
                .IsUnicode(false);

            modelBuilder.Entity<AEControlBank>()
                .Property(e => e.Status)
                .IsUnicode(false);

            modelBuilder.Entity<AEControlBank>()
                .Property(e => e.StationID)
                .IsUnicode(false);

            modelBuilder.Entity<AEControlBank>()
                .Property(e => e.CreatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<AEControlBank>()
                .Property(e => e.UpdatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<AEControlBank>()
                .Property(e => e.Version)
                .IsFixedLength();

            modelBuilder.Entity<AEHAWB>()
                .Property(e => e.HAWBNo)
                .IsUnicode(false);

            modelBuilder.Entity<AEHAWB>()
                .Property(e => e.Sales)
                .IsUnicode(false);

            modelBuilder.Entity<AEHAWB>()
                .Property(e => e.FRT)
                .IsUnicode(false);

            modelBuilder.Entity<AEHAWB>()
                .Property(e => e.Other)
                .IsUnicode(false);

            modelBuilder.Entity<AEHAWB>()
                .Property(e => e.Move)
                .IsUnicode(false);

            modelBuilder.Entity<AEHAWB>()
                .Property(e => e.ActPCSUOM)
                .IsUnicode(false);

            modelBuilder.Entity<AEHAWB>()
                .Property(e => e.SPUnitUOM)
                .IsUnicode(false);

            modelBuilder.Entity<AEHAWB>()
                .Property(e => e.WTUOM)
                .IsUnicode(false);

            modelBuilder.Entity<AEHAWB>()
                .Property(e => e.ClassRate)
                .IsUnicode(false);

            modelBuilder.Entity<AEHAWB>()
                .Property(e => e.Currency)
                .IsUnicode(false);

            modelBuilder.Entity<AEHAWB>()
                .Property(e => e.Quantity)
                .IsUnicode(false);

            modelBuilder.Entity<AEHAWB>()
                .Property(e => e.SPINST)
                .IsUnicode(false);

            modelBuilder.Entity<AEHAWB>()
                .Property(e => e.AWBType)
                .IsUnicode(false);

            modelBuilder.Entity<AEHAWB>()
                .Property(e => e.TradeTerm)
                .IsUnicode(false);

            modelBuilder.Entity<AEHAWB>()
                .Property(e => e.PrintPerson)
                .IsUnicode(false);

            modelBuilder.Entity<AEHAWB>()
                .Property(e => e.CustomsDocType)
                .IsUnicode(false);

            modelBuilder.Entity<AEHAWB>()
                .Property(e => e.CustomsStatus)
                .IsUnicode(false);

            modelBuilder.Entity<AEHAWB>()
                .Property(e => e.WHActPCSUOM)
                .IsUnicode(false);

            modelBuilder.Entity<AEHAWB>()
                .Property(e => e.WHActWTUOM)
                .IsUnicode(false);

            modelBuilder.Entity<AEHAWB>()
                .Property(e => e.WHActVWTUOM)
                .IsUnicode(false);

            modelBuilder.Entity<AEHAWB>()
                .Property(e => e.WHCargoStatus)
                .IsUnicode(false);

            modelBuilder.Entity<AEHAWB>()
                .Property(e => e.BookedWTUOM)
                .IsUnicode(false);

            modelBuilder.Entity<AEHAWB>()
                .Property(e => e.Status)
                .IsUnicode(false);

            modelBuilder.Entity<AEHAWB>()
                .Property(e => e.StationID)
                .IsUnicode(false);

            modelBuilder.Entity<AEHAWB>()
                .Property(e => e.ServiceLevel)
                .IsUnicode(false);

            modelBuilder.Entity<AEHAWB>()
                .Property(e => e.CreatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<AEHAWB>()
                .Property(e => e.UpdatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<AEHAWB>()
                .Property(e => e.Version)
                .IsFixedLength();

            modelBuilder.Entity<AEHAWB>()
                .Property(e => e.SStationID)
                .IsUnicode(false);

            modelBuilder.Entity<AEHAWB>()
                .Property(e => e.ShptType)
                .IsUnicode(false);

            modelBuilder.Entity<AEHAWB>()
                .Property(e => e.ReferenceCode)
                .IsUnicode(false);

            modelBuilder.Entity<AEHAWB>()
                .Property(e => e.SPL)
                .IsUnicode(false);

            modelBuilder.Entity<AEHAWB>()
                .Property(e => e.IMPStation)
                .IsUnicode(false);

            modelBuilder.Entity<AEHAWB>()
                .Property(e => e.BKNO)
                .IsUnicode(false);

            modelBuilder.Entity<AEHAWB>()
                .Property(e => e.ConfirmID)
                .IsUnicode(false);

            modelBuilder.Entity<AEHAWB>()
                .Property(e => e.HSCode)
                .IsUnicode(false);

            modelBuilder.Entity<AEHAWB>()
                .Property(e => e.UNCode)
                .IsUnicode(false);

            modelBuilder.Entity<AEHAWB>()
                .Property(e => e.MTSNO)
                .IsUnicode(false);

            modelBuilder.Entity<AEHAWB>()
                .Property(e => e.FRTParty)
                .IsUnicode(false);

            modelBuilder.Entity<AEHAWB>()
                .Property(e => e.DBID)
                .IsUnicode(false);

            modelBuilder.Entity<AEHAWBDim>()
                .Property(e => e.VWTUOM)
                .IsUnicode(false);

            modelBuilder.Entity<AEHAWBDim>()
                .Property(e => e.DIMUOM)
                .IsUnicode(false);

            modelBuilder.Entity<AEHAWBDim>()
                .Property(e => e.StationID)
                .IsUnicode(false);

            modelBuilder.Entity<AEHAWBDim>()
                .Property(e => e.CreatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<AEHAWBDim>()
                .Property(e => e.UpdatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<AEHAWBDim>()
                .Property(e => e.Version)
                .IsFixedLength();

            modelBuilder.Entity<AEHAWBDocRequired>()
                .Property(e => e.StationID)
                .IsUnicode(false);

            modelBuilder.Entity<AEHAWBDocRequired>()
                .Property(e => e.CreatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<AEHAWBDocRequired>()
                .Property(e => e.UpdatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<AEHAWBDocRequired>()
                .Property(e => e.Version)
                .IsFixedLength();

            modelBuilder.Entity<AEHAWBINST>()
                .Property(e => e.From)
                .IsUnicode(false);

            modelBuilder.Entity<AEHAWBINST>()
                .Property(e => e.Category)
                .IsUnicode(false);

            modelBuilder.Entity<AEHAWBINST>()
                .Property(e => e.StationID)
                .IsUnicode(false);

            modelBuilder.Entity<AEHAWBINST>()
                .Property(e => e.CreatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<AEHAWBINST>()
                .Property(e => e.UpdatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<AEHAWBINST>()
                .Property(e => e.Version)
                .IsFixedLength();

            modelBuilder.Entity<AEHAWBPickup>()
                .Property(e => e.StationID)
                .IsUnicode(false);

            modelBuilder.Entity<AEHAWBPickup>()
                .Property(e => e.CreatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<AEHAWBPickup>()
                .Property(e => e.UpdatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<AEHAWBPickup>()
                .Property(e => e.Version)
                .IsFixedLength();

            modelBuilder.Entity<AEHAWBPickup>()
                .Property(e => e.WeightUOM)
                .IsUnicode(false);

            modelBuilder.Entity<AEHAWBPickup>()
                .Property(e => e.SendBy)
                .IsUnicode(false);

            modelBuilder.Entity<AEHAWBPO>()
                .Property(e => e.PONo)
                .IsUnicode(false);

            modelBuilder.Entity<AEHAWBPO>()
                .Property(e => e.StationID)
                .IsUnicode(false);

            modelBuilder.Entity<AEHAWBPO>()
                .Property(e => e.CreatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<AEHAWBPO>()
                .Property(e => e.UpdatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<AEHAWBPO>()
                .Property(e => e.Version)
                .IsFixedLength();

            modelBuilder.Entity<AEHAWBPO>()
                .Property(e => e.SONO)
                .IsUnicode(false);

            modelBuilder.Entity<AEHAWBPO>()
                .Property(e => e.Curr)
                .IsUnicode(false);

            modelBuilder.Entity<AEHAWBPO>()
                .Property(e => e.UOM)
                .IsUnicode(false);

            modelBuilder.Entity<AEHAWBPreview>()
                .Property(e => e.RateClass)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<AEHAWBPreview>()
                .Property(e => e.StationID)
                .IsUnicode(false);

            modelBuilder.Entity<AEHAWBPreview>()
                .Property(e => e.CreatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<AEHAWBPreview>()
                .Property(e => e.UpdatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<AEHAWBPreview>()
                .Property(e => e.Version)
                .IsFixedLength();

            modelBuilder.Entity<AEHAWBPreview>()
                .Property(e => e.FromAI)
                .IsUnicode(false);

            modelBuilder.Entity<AEHAWBPreview>()
                .Property(e => e.ORGShipper)
                .IsUnicode(false);

            modelBuilder.Entity<AEHAWBPreview>()
                .Property(e => e.ORGPort)
                .IsUnicode(false);

            modelBuilder.Entity<AEHAWBPreview>()
                .Property(e => e.PrintBy)
                .IsUnicode(false);

            modelBuilder.Entity<AEHAWBPreview>()
                .Property(e => e.FootShow)
                .IsUnicode(false);

            modelBuilder.Entity<AEHAWBPreview>()
                .Property(e => e.LotNo)
                .IsUnicode(false);

            modelBuilder.Entity<AEHAWBPreview>()
                .Property(e => e.TradeTerm)
                .IsUnicode(false);

            modelBuilder.Entity<AEHAWBPrintLog>()
                .Property(e => e.UpdatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<AEHAWBPrintLog>()
                .Property(e => e.StationID)
                .IsUnicode(false);

            modelBuilder.Entity<AEHAWBPrintLog>()
                .Property(e => e.CreateBy)
                .IsUnicode(false);

            modelBuilder.Entity<AEHAWBPrintLog>()
                .Property(e => e.ReportName)
                .IsUnicode(false);

            modelBuilder.Entity<AEHAWBSUM>()
                .Property(e => e.ReportCurrency)
                .IsUnicode(false);

            modelBuilder.Entity<AEHAWBSUM>()
                .Property(e => e.LocalCurrency)
                .IsUnicode(false);

            modelBuilder.Entity<AEHAWBSUM>()
                .Property(e => e.STATIONID)
                .IsUnicode(false);

            modelBuilder.Entity<AEHAWBSUM>()
                .Property(e => e.IDType)
                .IsUnicode(false);

            modelBuilder.Entity<AEHAWBSUM>()
                .Property(e => e.Version)
                .IsFixedLength();

            modelBuilder.Entity<AEHAWBSUM>()
                .Property(e => e.CreateBy)
                .IsUnicode(false);

            modelBuilder.Entity<AEHAWBSUM>()
                .Property(e => e.UpdatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<AEMAWB>()
                .Property(e => e.MAWBNo)
                .IsUnicode(false);

            modelBuilder.Entity<AEMAWB>()
                .Property(e => e.Other)
                .IsUnicode(false);

            modelBuilder.Entity<AEMAWB>()
                .Property(e => e.FRT)
                .IsUnicode(false);

            modelBuilder.Entity<AEMAWB>()
                .Property(e => e.LotNo)
                .IsUnicode(false);

            modelBuilder.Entity<AEMAWB>()
                .Property(e => e.Currency)
                .IsUnicode(false);

            modelBuilder.Entity<AEMAWB>()
                .Property(e => e.WTUOM)
                .IsUnicode(false);

            modelBuilder.Entity<AEMAWB>()
                .Property(e => e.PCSUOM)
                .IsUnicode(false);

            modelBuilder.Entity<AEMAWB>()
                .Property(e => e.ClassRate)
                .IsUnicode(false);

            modelBuilder.Entity<AEMAWB>()
                .Property(e => e.SPINST)
                .IsUnicode(false);

            modelBuilder.Entity<AEMAWB>()
                .Property(e => e.Status)
                .IsUnicode(false);

            modelBuilder.Entity<AEMAWB>()
                .Property(e => e.PreAlertBy)
                .IsUnicode(false);

            modelBuilder.Entity<AEMAWB>()
                .Property(e => e.eManifestBy)
                .IsUnicode(false);

            modelBuilder.Entity<AEMAWB>()
                .Property(e => e.isAMS)
                .IsUnicode(false);

            modelBuilder.Entity<AEMAWB>()
                .Property(e => e.StationID)
                .IsUnicode(false);

            modelBuilder.Entity<AEMAWB>()
                .Property(e => e.CreatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<AEMAWB>()
                .Property(e => e.UpdatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<AEMAWB>()
                .Property(e => e.Version)
                .IsFixedLength();

            modelBuilder.Entity<AEMAWB>()
                .Property(e => e.CYMNo)
                .IsUnicode(false);

            modelBuilder.Entity<AEMAWB>()
                .Property(e => e.DBID)
                .IsUnicode(false);

            modelBuilder.Entity<AEMAWB>()
                .Property(e => e.AMSBy)
                .IsUnicode(false);

            modelBuilder.Entity<AEMAWB>()
                .Property(e => e.ReWeight)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<AEMAWB>()
                .Property(e => e.WeightBy)
                .IsUnicode(false);

            modelBuilder.Entity<AEMAWBDim>()
                .Property(e => e.VWTUOM)
                .IsUnicode(false);

            modelBuilder.Entity<AEMAWBDim>()
                .Property(e => e.DIMUOM)
                .IsUnicode(false);

            modelBuilder.Entity<AEMAWBDim>()
                .Property(e => e.StationID)
                .IsUnicode(false);

            modelBuilder.Entity<AEMAWBDim>()
                .Property(e => e.CreatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<AEMAWBDim>()
                .Property(e => e.UpdatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<AEMAWBDim>()
                .Property(e => e.Version)
                .IsFixedLength();

            modelBuilder.Entity<AEMAWBINST>()
                .Property(e => e.From)
                .IsUnicode(false);

            modelBuilder.Entity<AEMAWBINST>()
                .Property(e => e.Category)
                .IsUnicode(false);

            modelBuilder.Entity<AEMAWBINST>()
                .Property(e => e.StationID)
                .IsUnicode(false);

            modelBuilder.Entity<AEMAWBINST>()
                .Property(e => e.CreatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<AEMAWBINST>()
                .Property(e => e.UpdatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<AEMAWBINST>()
                .Property(e => e.Version)
                .IsFixedLength();

            modelBuilder.Entity<AEMAWBPreview>()
                .Property(e => e.StationID)
                .IsUnicode(false);

            modelBuilder.Entity<AEMAWBPreview>()
                .Property(e => e.CreatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<AEMAWBPreview>()
                .Property(e => e.UpdatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<AEMAWBPreview>()
                .Property(e => e.Version)
                .IsFixedLength();

            modelBuilder.Entity<AEMAWBPreview>()
                .Property(e => e.PrintBy)
                .IsUnicode(false);

            modelBuilder.Entity<AEMAWBPreview>()
                .Property(e => e.FootShow)
                .IsUnicode(false);

            modelBuilder.Entity<AEMAWBPreview>()
                .Property(e => e.LotNo)
                .IsUnicode(false);

            modelBuilder.Entity<AEMAWBPreview>()
                .Property(e => e.TradeTerm)
                .IsUnicode(false);

            modelBuilder.Entity<AEMAWBPrintLog>()
                .Property(e => e.UpdatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<AEMAWBPrintLog>()
                .Property(e => e.StationID)
                .IsUnicode(false);

            modelBuilder.Entity<AEMAWBPrintLog>()
                .Property(e => e.CreateBy)
                .IsUnicode(false);

            modelBuilder.Entity<AEMAWBPrintLog>()
                .Property(e => e.ReportName)
                .IsUnicode(false);

            modelBuilder.Entity<AEPOD>()
                .Property(e => e.FLTNo)
                .IsUnicode(false);

            modelBuilder.Entity<AEPOD>()
                .Property(e => e.SCHDocReleaseType)
                .IsUnicode(false);

            modelBuilder.Entity<AEPOD>()
                .Property(e => e.SCHNotifyType)
                .IsUnicode(false);

            modelBuilder.Entity<AEPOD>()
                .Property(e => e.SCHCargoReleaseType)
                .IsUnicode(false);

            modelBuilder.Entity<AEPOD>()
                .Property(e => e.StationID)
                .IsUnicode(false);

            modelBuilder.Entity<AEPOD>()
                .Property(e => e.CreatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<AEPOD>()
                .Property(e => e.UpdatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<AEPOD>()
                .Property(e => e.Version)
                .IsFixedLength();

            modelBuilder.Entity<AEPOD>()
                .Property(e => e.PODTemplate)
                .IsUnicode(false);

            modelBuilder.Entity<AEPODMilestone>()
                .Property(e => e.StationID)
                .IsUnicode(false);

            modelBuilder.Entity<AEPODMilestone>()
                .Property(e => e.Mode)
                .IsUnicode(false);

            modelBuilder.Entity<AEPODMilestone>()
                .Property(e => e.MSParty)
                .IsUnicode(false);

            modelBuilder.Entity<AEPODMilestone>()
                .Property(e => e.MSSignBy)
                .IsUnicode(false);

            modelBuilder.Entity<AEPODMilestone>()
                .Property(e => e.MSReason)
                .IsUnicode(false);

            modelBuilder.Entity<AEPODMilestone>()
                .Property(e => e.CreatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<AEPODMilestone>()
                .Property(e => e.UpdatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<AEReConfirmLog>()
                .Property(e => e.StationID)
                .IsUnicode(false);

            modelBuilder.Entity<AEReConfirmLog>()
                .Property(e => e.CreatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<AEReConfirmLog>()
                .Property(e => e.UpdatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<AIAWBSUM>()
                .Property(e => e.ReportCurrency)
                .IsUnicode(false);

            modelBuilder.Entity<AIAWBSUM>()
                .Property(e => e.LocalCurrency)
                .IsUnicode(false);

            modelBuilder.Entity<AIAWBSUM>()
                .Property(e => e.STATIONID)
                .IsUnicode(false);

            modelBuilder.Entity<AIAWBSUM>()
                .Property(e => e.IDType)
                .IsUnicode(false);

            modelBuilder.Entity<AIAWBSUM>()
                .Property(e => e.Version)
                .IsFixedLength();

            modelBuilder.Entity<AIAWBSUM>()
                .Property(e => e.CreateBy)
                .IsUnicode(false);

            modelBuilder.Entity<AIAWBSUM>()
                .Property(e => e.UpdatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<AICarrierBooking>()
                .Property(e => e.BookedFLT)
                .IsUnicode(false);

            modelBuilder.Entity<AICarrierBooking>()
                .Property(e => e.OnboardFLT)
                .IsUnicode(false);

            modelBuilder.Entity<AICarrierBooking>()
                .Property(e => e.StationID)
                .IsUnicode(false);

            modelBuilder.Entity<AICarrierBooking>()
                .Property(e => e.CreatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<AICarrierBooking>()
                .Property(e => e.UpdatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<AICarrierBooking>()
                .Property(e => e.Version)
                .IsFixedLength();

            modelBuilder.Entity<AIEntry>()
                .Property(e => e.Damage)
                .IsUnicode(false);

            modelBuilder.Entity<AIEntry>()
                .Property(e => e.Shortage)
                .IsUnicode(false);

            modelBuilder.Entity<AIEntry>()
                .Property(e => e.Offload)
                .IsUnicode(false);

            modelBuilder.Entity<AIEntry>()
                .Property(e => e.CustomsEntry)
                .IsUnicode(false);

            modelBuilder.Entity<AIEntry>()
                .Property(e => e.FLTNo)
                .IsUnicode(false);

            modelBuilder.Entity<AIEntry>()
                .Property(e => e.EntryType)
                .IsUnicode(false);

            modelBuilder.Entity<AIEntry>()
                .Property(e => e.Storage)
                .IsUnicode(false);

            modelBuilder.Entity<AIEntry>()
                .Property(e => e.StationID)
                .IsUnicode(false);

            modelBuilder.Entity<AIEntry>()
                .Property(e => e.CreatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<AIEntry>()
                .Property(e => e.UpdatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<AIEntry>()
                .Property(e => e.Version)
                .IsFixedLength();

            modelBuilder.Entity<AIEntryDT>()
                .Property(e => e.StationID)
                .IsUnicode(false);

            modelBuilder.Entity<AIEntryDT>()
                .Property(e => e.CreatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<AIEntryDT>()
                .Property(e => e.UpdatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<AIEntryDT>()
                .Property(e => e.Version)
                .IsFixedLength();

            modelBuilder.Entity<AIEntryDT>()
                .Property(e => e.Remark)
                .IsUnicode(false);

            modelBuilder.Entity<AIHAWB>()
                .Property(e => e.Status)
                .IsUnicode(false);

            modelBuilder.Entity<AIHAWB>()
                .Property(e => e.Sales)
                .IsUnicode(false);

            modelBuilder.Entity<AIHAWB>()
                .Property(e => e.FRT)
                .IsUnicode(false);

            modelBuilder.Entity<AIHAWB>()
                .Property(e => e.Other)
                .IsUnicode(false);

            modelBuilder.Entity<AIHAWB>()
                .Property(e => e.Move)
                .IsUnicode(false);

            modelBuilder.Entity<AIHAWB>()
                .Property(e => e.SPUnitUOM)
                .IsUnicode(false);

            modelBuilder.Entity<AIHAWB>()
                .Property(e => e.WTUOM)
                .IsUnicode(false);

            modelBuilder.Entity<AIHAWB>()
                .Property(e => e.ClassRate)
                .IsUnicode(false);

            modelBuilder.Entity<AIHAWB>()
                .Property(e => e.Currency)
                .IsUnicode(false);

            modelBuilder.Entity<AIHAWB>()
                .Property(e => e.Quantity)
                .IsUnicode(false);

            modelBuilder.Entity<AIHAWB>()
                .Property(e => e.SPINST)
                .IsUnicode(false);

            modelBuilder.Entity<AIHAWB>()
                .Property(e => e.AWBType)
                .IsUnicode(false);

            modelBuilder.Entity<AIHAWB>()
                .Property(e => e.TradeTerm)
                .IsUnicode(false);

            modelBuilder.Entity<AIHAWB>()
                .Property(e => e.PrintPerson)
                .IsUnicode(false);

            modelBuilder.Entity<AIHAWB>()
                .Property(e => e.ActPCSUOM)
                .IsUnicode(false);

            modelBuilder.Entity<AIHAWB>()
                .Property(e => e.SCHDocReleaseType)
                .IsUnicode(false);

            modelBuilder.Entity<AIHAWB>()
                .Property(e => e.SCHNotifyType)
                .IsUnicode(false);

            modelBuilder.Entity<AIHAWB>()
                .Property(e => e.SCHCargoReleaseType)
                .IsUnicode(false);

            modelBuilder.Entity<AIHAWB>()
                .Property(e => e.CustomsDocType)
                .IsUnicode(false);

            modelBuilder.Entity<AIHAWB>()
                .Property(e => e.CustomsStatus)
                .IsUnicode(false);

            modelBuilder.Entity<AIHAWB>()
                .Property(e => e.WHActPCSUOM)
                .IsUnicode(false);

            modelBuilder.Entity<AIHAWB>()
                .Property(e => e.WHActWTUOM)
                .IsUnicode(false);

            modelBuilder.Entity<AIHAWB>()
                .Property(e => e.WHActVWTUOM)
                .IsUnicode(false);

            modelBuilder.Entity<AIHAWB>()
                .Property(e => e.WHCargoStatus)
                .IsUnicode(false);

            modelBuilder.Entity<AIHAWB>()
                .Property(e => e.WHReceiptNo)
                .IsUnicode(false);

            modelBuilder.Entity<AIHAWB>()
                .Property(e => e.HAWBNo)
                .IsUnicode(false);

            modelBuilder.Entity<AIHAWB>()
                .Property(e => e.StationID)
                .IsUnicode(false);

            modelBuilder.Entity<AIHAWB>()
                .Property(e => e.CreatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<AIHAWB>()
                .Property(e => e.UpdatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<AIHAWB>()
                .Property(e => e.Version)
                .IsFixedLength();

            modelBuilder.Entity<AIHAWB>()
                .Property(e => e.SStationID)
                .IsUnicode(false);

            modelBuilder.Entity<AIHAWB>()
                .Property(e => e.ShptType)
                .IsUnicode(false);

            modelBuilder.Entity<AIHAWB>()
                .Property(e => e.SPL)
                .IsUnicode(false);

            modelBuilder.Entity<AIHAWB>()
                .Property(e => e.IMPStation)
                .IsUnicode(false);

            modelBuilder.Entity<AIHAWB>()
                .Property(e => e.ReferenceCode)
                .IsUnicode(false);

            modelBuilder.Entity<AIHAWB>()
                .Property(e => e.ServiceLevel)
                .IsUnicode(false);

            modelBuilder.Entity<AIHAWB>()
                .Property(e => e.PODTemplate)
                .IsUnicode(false);

            modelBuilder.Entity<AIHAWB>()
                .Property(e => e.MTSNO)
                .IsUnicode(false);

            modelBuilder.Entity<AIHAWB>()
                .Property(e => e.FRTParty)
                .IsUnicode(false);

            modelBuilder.Entity<AIHAWB>()
                .Property(e => e.DBID)
                .IsUnicode(false);

            modelBuilder.Entity<AIHAWB>()
                .Property(e => e.CopyAEBy)
                .IsUnicode(false);

            modelBuilder.Entity<AIHAWBAgentInfo>()
                .Property(e => e.StationID)
                .IsUnicode(false);

            modelBuilder.Entity<AIHAWBAgentInfo>()
                .Property(e => e.CreatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<AIHAWBAgentInfo>()
                .Property(e => e.UpdatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<AIHAWBAgentInfoMap>()
                .Property(e => e.StationID)
                .IsUnicode(false);

            modelBuilder.Entity<AIHAWBAgentInfoMap>()
                .Property(e => e.CompanyCode)
                .IsUnicode(false);

            modelBuilder.Entity<AIHAWBAgentInfoMap>()
                .Property(e => e.CreatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<AIHAWBAgentInfoMap>()
                .Property(e => e.UpdatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<AIHAWBDim>()
                .Property(e => e.VWTUOM)
                .IsUnicode(false);

            modelBuilder.Entity<AIHAWBDim>()
                .Property(e => e.DIMUOM)
                .IsUnicode(false);

            modelBuilder.Entity<AIHAWBDim>()
                .Property(e => e.StationID)
                .IsUnicode(false);

            modelBuilder.Entity<AIHAWBDim>()
                .Property(e => e.CreatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<AIHAWBDim>()
                .Property(e => e.UpdatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<AIHAWBDim>()
                .Property(e => e.Version)
                .IsFixedLength();

            modelBuilder.Entity<AIHAWBDocRequired>()
                .Property(e => e.StationID)
                .IsUnicode(false);

            modelBuilder.Entity<AIHAWBDocRequired>()
                .Property(e => e.CreatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<AIHAWBDocRequired>()
                .Property(e => e.UpdatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<AIHAWBDocRequired>()
                .Property(e => e.Version)
                .IsFixedLength();

            modelBuilder.Entity<AIHAWBINST>()
                .Property(e => e.From)
                .IsUnicode(false);

            modelBuilder.Entity<AIHAWBINST>()
                .Property(e => e.Category)
                .IsUnicode(false);

            modelBuilder.Entity<AIHAWBINST>()
                .Property(e => e.StationID)
                .IsUnicode(false);

            modelBuilder.Entity<AIHAWBINST>()
                .Property(e => e.CreatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<AIHAWBINST>()
                .Property(e => e.UpdatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<AIHAWBINST>()
                .Property(e => e.Version)
                .IsFixedLength();

            modelBuilder.Entity<AIHAWBPickup>()
                .Property(e => e.StationID)
                .IsUnicode(false);

            modelBuilder.Entity<AIHAWBPickup>()
                .Property(e => e.CreatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<AIHAWBPickup>()
                .Property(e => e.UpdatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<AIHAWBPickup>()
                .Property(e => e.Version)
                .IsFixedLength();

            modelBuilder.Entity<AIHAWBPickup>()
                .Property(e => e.WeightUOM)
                .IsUnicode(false);

            modelBuilder.Entity<AIHAWBPickup>()
                .Property(e => e.SendBy)
                .IsUnicode(false);

            modelBuilder.Entity<AIHAWBPickup>()
                .Property(e => e.InvolvedBy)
                .IsUnicode(false);

            modelBuilder.Entity<AIHAWBPO>()
                .Property(e => e.PONo)
                .IsUnicode(false);

            modelBuilder.Entity<AIHAWBPO>()
                .Property(e => e.StationID)
                .IsUnicode(false);

            modelBuilder.Entity<AIHAWBPO>()
                .Property(e => e.CreatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<AIHAWBPO>()
                .Property(e => e.UpdatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<AIHAWBPO>()
                .Property(e => e.Version)
                .IsFixedLength();

            modelBuilder.Entity<AIHAWBPO>()
                .Property(e => e.SONO)
                .IsUnicode(false);

            modelBuilder.Entity<AIHAWBPO>()
                .Property(e => e.Curr)
                .IsUnicode(false);

            modelBuilder.Entity<AIHAWBPO>()
                .Property(e => e.UOM)
                .IsUnicode(false);

            modelBuilder.Entity<AIHAWBPreview>()
                .Property(e => e.StationID)
                .IsUnicode(false);

            modelBuilder.Entity<AIHAWBPreview>()
                .Property(e => e.CreatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<AIHAWBPreview>()
                .Property(e => e.UpdatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<AIHAWBPreview>()
                .Property(e => e.Version)
                .IsFixedLength();

            modelBuilder.Entity<AIHAWBPrintLog>()
                .Property(e => e.UpdatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<AIHAWBPrintLog>()
                .Property(e => e.StationID)
                .IsUnicode(false);

            modelBuilder.Entity<AIHAWBPrintLog>()
                .Property(e => e.CreateBy)
                .IsUnicode(false);

            modelBuilder.Entity<AIHAWBPrintLog>()
                .Property(e => e.ReportName)
                .IsUnicode(false);

            modelBuilder.Entity<AIITBank>()
                .Property(e => e.StationID)
                .IsUnicode(false);

            modelBuilder.Entity<AIITBank>()
                .Property(e => e.CreatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<AIITBank>()
                .Property(e => e.UpdatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<AIITBank>()
                .Property(e => e.Version)
                .IsFixedLength();

            modelBuilder.Entity<AIITDT>()
                .Property(e => e.StationID)
                .IsUnicode(false);

            modelBuilder.Entity<AIITDT>()
                .Property(e => e.CreatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<AIITDT>()
                .Property(e => e.UpdatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<AIITDT>()
                .Property(e => e.Version)
                .IsFixedLength();

            modelBuilder.Entity<AIITEntry>()
                .Property(e => e.StationID)
                .IsUnicode(false);

            modelBuilder.Entity<AIITEntry>()
                .Property(e => e.CreatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<AIITEntry>()
                .Property(e => e.UpdatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<AIITEntry>()
                .Property(e => e.Version)
                .IsFixedLength();

            modelBuilder.Entity<AIMAWB>()
                .Property(e => e.MAWBNo)
                .IsUnicode(false);

            modelBuilder.Entity<AIMAWB>()
                .Property(e => e.Other)
                .IsUnicode(false);

            modelBuilder.Entity<AIMAWB>()
                .Property(e => e.FRT)
                .IsUnicode(false);

            modelBuilder.Entity<AIMAWB>()
                .Property(e => e.LotNo)
                .IsUnicode(false);

            modelBuilder.Entity<AIMAWB>()
                .Property(e => e.Currency)
                .IsUnicode(false);

            modelBuilder.Entity<AIMAWB>()
                .Property(e => e.WTUOM)
                .IsUnicode(false);

            modelBuilder.Entity<AIMAWB>()
                .Property(e => e.SPINST)
                .IsUnicode(false);

            modelBuilder.Entity<AIMAWB>()
                .Property(e => e.Status)
                .IsUnicode(false);

            modelBuilder.Entity<AIMAWB>()
                .Property(e => e.PackagingDescription)
                .IsUnicode(false);

            modelBuilder.Entity<AIMAWB>()
                .Property(e => e.PCSUOM)
                .IsUnicode(false);

            modelBuilder.Entity<AIMAWB>()
                .Property(e => e.ClassRate)
                .IsUnicode(false);

            modelBuilder.Entity<AIMAWB>()
                .Property(e => e.IssuerType)
                .IsUnicode(false);

            modelBuilder.Entity<AIMAWB>()
                .Property(e => e.StationID)
                .IsUnicode(false);

            modelBuilder.Entity<AIMAWB>()
                .Property(e => e.CreatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<AIMAWB>()
                .Property(e => e.UpdatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<AIMAWB>()
                .Property(e => e.Version)
                .IsFixedLength();

            modelBuilder.Entity<AIMAWB>()
                .Property(e => e.SStationID)
                .IsUnicode(false);

            modelBuilder.Entity<AIMAWB>()
                .Property(e => e.ISAC)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<AIMAWB>()
                .Property(e => e.ISACBy)
                .IsUnicode(false);

            modelBuilder.Entity<AIMAWB>()
                .Property(e => e.DBID)
                .IsUnicode(false);

            modelBuilder.Entity<AIMAWB>()
                .Property(e => e.DYMBy)
                .IsUnicode(false);

            modelBuilder.Entity<AIMAWBDim>()
                .Property(e => e.VWTUOM)
                .IsUnicode(false);

            modelBuilder.Entity<AIMAWBDim>()
                .Property(e => e.DIMUOM)
                .IsUnicode(false);

            modelBuilder.Entity<AIMAWBDim>()
                .Property(e => e.StationID)
                .IsUnicode(false);

            modelBuilder.Entity<AIMAWBDim>()
                .Property(e => e.CreatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<AIMAWBDim>()
                .Property(e => e.UpdatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<AIMAWBDim>()
                .Property(e => e.Version)
                .IsFixedLength();

            modelBuilder.Entity<AIMAWBINST>()
                .Property(e => e.From)
                .IsUnicode(false);

            modelBuilder.Entity<AIMAWBINST>()
                .Property(e => e.Category)
                .IsUnicode(false);

            modelBuilder.Entity<AIMAWBINST>()
                .Property(e => e.StationID)
                .IsUnicode(false);

            modelBuilder.Entity<AIMAWBINST>()
                .Property(e => e.CreatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<AIMAWBINST>()
                .Property(e => e.UpdatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<AIMAWBINST>()
                .Property(e => e.Version)
                .IsFixedLength();

            modelBuilder.Entity<AIMAWBPreview>()
                .Property(e => e.RateClass)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<AIMAWBPreview>()
                .Property(e => e.StationID)
                .IsUnicode(false);

            modelBuilder.Entity<AIMAWBPreview>()
                .Property(e => e.CreatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<AIMAWBPreview>()
                .Property(e => e.UpdatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<AIMAWBPreview>()
                .Property(e => e.Version)
                .IsFixedLength();

            modelBuilder.Entity<AIMAWBPrintLog>()
                .Property(e => e.UpdatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<AIMAWBPrintLog>()
                .Property(e => e.StationID)
                .IsUnicode(false);

            modelBuilder.Entity<AIMAWBPrintLog>()
                .Property(e => e.CreateBy)
                .IsUnicode(false);

            modelBuilder.Entity<AIMAWBPrintLog>()
                .Property(e => e.ReportName)
                .IsUnicode(false);

            modelBuilder.Entity<AIPODMilestone>()
                .Property(e => e.StationID)
                .IsUnicode(false);

            modelBuilder.Entity<AIPODMilestone>()
                .Property(e => e.Mode)
                .IsUnicode(false);

            modelBuilder.Entity<AIPODMilestone>()
                .Property(e => e.MSParty)
                .IsUnicode(false);

            modelBuilder.Entity<AIPODMilestone>()
                .Property(e => e.MSSignBy)
                .IsUnicode(false);

            modelBuilder.Entity<AIPODMilestone>()
                .Property(e => e.MSReason)
                .IsUnicode(false);

            modelBuilder.Entity<AIPODMilestone>()
                .Property(e => e.CreatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<AIPODMilestone>()
                .Property(e => e.UpdatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<OEACI>()
                .Property(e => e.SupplementaryRefNo)
                .IsUnicode(false);

            modelBuilder.Entity<OEACI>()
                .Property(e => e.CargoControlNo)
                .IsUnicode(false);

            modelBuilder.Entity<OEACI>()
                .Property(e => e.PrimaryBL)
                .IsUnicode(false);

            modelBuilder.Entity<OEACI>()
                .Property(e => e.Type)
                .IsUnicode(false);

            modelBuilder.Entity<OEACI>()
                .Property(e => e.PortName)
                .IsUnicode(false);

            modelBuilder.Entity<OEACI>()
                .Property(e => e.Status)
                .IsUnicode(false);

            modelBuilder.Entity<OEACI>()
                .Property(e => e.Volume)
                .HasPrecision(10, 2);

            modelBuilder.Entity<OEACI>()
                .Property(e => e.VolumeUOM)
                .IsUnicode(false);

            modelBuilder.Entity<OEACI>()
                .Property(e => e.CreatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<OEACI>()
                .Property(e => e.UpdatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<OEACI>()
                .Property(e => e.Version)
                .IsFixedLength();

            modelBuilder.Entity<OEAMS>()
                .Property(e => e.IssuerSCAC)
                .IsUnicode(false);

            modelBuilder.Entity<OEAMS>()
                .Property(e => e.PreCarrierReceipt)
                .IsUnicode(false);

            modelBuilder.Entity<OEAMS>()
                .Property(e => e.StatusIndicator)
                .IsUnicode(false);

            modelBuilder.Entity<OEAMS>()
                .Property(e => e.AmendmentCode)
                .IsUnicode(false);

            modelBuilder.Entity<OEAMS>()
                .Property(e => e.BookingNo)
                .IsUnicode(false);

            modelBuilder.Entity<OEAMS>()
                .Property(e => e.SplitBill)
                .IsUnicode(false);

            modelBuilder.Entity<OEAMS>()
                .Property(e => e.HandlingGroup)
                .IsUnicode(false);

            modelBuilder.Entity<OEAMS>()
                .Property(e => e.SNPPartyCode)
                .IsUnicode(false);

            modelBuilder.Entity<OEAMS>()
                .Property(e => e.AdditionalSNPParties)
                .IsUnicode(false);

            modelBuilder.Entity<OEAMS>()
                .Property(e => e.NotifyPartyCode2)
                .IsUnicode(false);

            modelBuilder.Entity<OEAMS>()
                .Property(e => e.TotalNetWeightUOM)
                .IsUnicode(false);

            modelBuilder.Entity<OEAMS>()
                .Property(e => e.TotalVolumeUOM)
                .IsUnicode(false);

            modelBuilder.Entity<OEAMS>()
                .Property(e => e.HarmonizedCode)
                .IsUnicode(false);

            modelBuilder.Entity<OEAMS>()
                .Property(e => e.CreatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<OEAMS>()
                .Property(e => e.UpdatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<OEAMS>()
                .Property(e => e.Version)
                .IsFixedLength();

            modelBuilder.Entity<OEAMS>()
                .Property(e => e.PDischarge)
                .IsUnicode(false);

            modelBuilder.Entity<OEAMS>()
                .Property(e => e.PLoading)
                .IsUnicode(false);

            modelBuilder.Entity<OEAMS>()
                .Property(e => e.HBLNo)
                .IsUnicode(false);

            modelBuilder.Entity<OEAMS>()
                .Property(e => e.PDelivery)
                .IsUnicode(false);

            modelBuilder.Entity<OEAMS>()
                .Property(e => e.PReceipt)
                .IsUnicode(false);

            modelBuilder.Entity<OEAMS>()
                .Property(e => e.CoLoader)
                .IsUnicode(false);

            modelBuilder.Entity<OEAMS>()
                .Property(e => e.MBLNO)
                .IsUnicode(false);

            modelBuilder.Entity<OEAMS>()
                .Property(e => e.AgencyUniqueCode)
                .IsUnicode(false);

            modelBuilder.Entity<OEAMS>()
                .Property(e => e.SHPRZip)
                .IsUnicode(false);

            modelBuilder.Entity<OEAMS>()
                .Property(e => e.SHPRPhone)
                .IsUnicode(false);

            modelBuilder.Entity<OEAMS>()
                .Property(e => e.CNEEZip)
                .IsUnicode(false);

            modelBuilder.Entity<OEAMS>()
                .Property(e => e.CNEEPhone)
                .IsUnicode(false);

            modelBuilder.Entity<OEAMS>()
                .Property(e => e.NTFYZip)
                .IsUnicode(false);

            modelBuilder.Entity<OEAMS>()
                .Property(e => e.NTFYPhone)
                .IsUnicode(false);

            modelBuilder.Entity<OEAMS>()
                .Property(e => e.AdditionalSNPParties2)
                .IsUnicode(false);

            modelBuilder.Entity<OEAMS>()
                .Property(e => e.NotifyPartyCode1)
                .IsUnicode(false);

            modelBuilder.Entity<OEAMS>()
                .Property(e => e.SealNumber1)
                .IsUnicode(false);

            modelBuilder.Entity<OEAMS>()
                .Property(e => e.SealNumber2)
                .IsUnicode(false);

            modelBuilder.Entity<OEAMS>()
                .Property(e => e.PCSUOM)
                .IsUnicode(false);

            modelBuilder.Entity<OEAMS>()
                .Property(e => e.NetWeightUOM)
                .IsUnicode(false);

            modelBuilder.Entity<OEAMS>()
                .Property(e => e.LoadOrEmpty)
                .IsUnicode(false);

            modelBuilder.Entity<OEAMS>()
                .Property(e => e.TypeOfService)
                .IsUnicode(false);

            modelBuilder.Entity<OEAMS>()
                .Property(e => e.CountryOfOrigin)
                .IsUnicode(false);

            modelBuilder.Entity<OEAMS>()
                .Property(e => e.SmallestPackageType)
                .IsUnicode(false);

            modelBuilder.Entity<OEAMS>()
                .Property(e => e.NTFY2Zip)
                .IsUnicode(false);

            modelBuilder.Entity<OEAMS>()
                .Property(e => e.NTFY2Phone)
                .IsUnicode(false);

            modelBuilder.Entity<OEAMSContainer>()
                .Property(e => e.SealNumber1)
                .IsUnicode(false);

            modelBuilder.Entity<OEAMSContainer>()
                .Property(e => e.SealNumber2)
                .IsUnicode(false);

            modelBuilder.Entity<OEAMSContainer>()
                .Property(e => e.PCS)
                .IsUnicode(false);

            modelBuilder.Entity<OEAMSContainer>()
                .Property(e => e.PCSUOM)
                .IsUnicode(false);

            modelBuilder.Entity<OEAMSContainer>()
                .Property(e => e.NetWeightUOM)
                .IsUnicode(false);

            modelBuilder.Entity<OEAMSContainer>()
                .Property(e => e.LoadOrEmpty)
                .IsUnicode(false);

            modelBuilder.Entity<OEAMSContainer>()
                .Property(e => e.TypeOfService)
                .IsUnicode(false);

            modelBuilder.Entity<OEAMSContainer>()
                .Property(e => e.CountryOfOrigin)
                .IsUnicode(false);

            modelBuilder.Entity<OEAMSTemplate>()
                .Property(e => e.TemplateName)
                .IsUnicode(false);

            modelBuilder.Entity<OEAMSTemplate>()
                .Property(e => e.SHPRZip)
                .IsUnicode(false);

            modelBuilder.Entity<OEAMSTemplate>()
                .Property(e => e.SHPRPhone)
                .IsUnicode(false);

            modelBuilder.Entity<OEAMSTemplate>()
                .Property(e => e.CNEEZip)
                .IsUnicode(false);

            modelBuilder.Entity<OEAMSTemplate>()
                .Property(e => e.CNEEPhone)
                .IsUnicode(false);

            modelBuilder.Entity<OEAMSTemplate>()
                .Property(e => e.NTFYZip)
                .IsUnicode(false);

            modelBuilder.Entity<OEAMSTemplate>()
                .Property(e => e.NTFYPhone)
                .IsUnicode(false);

            modelBuilder.Entity<OEAMSTemplate>()
                .Property(e => e.NTFY2Zip)
                .IsUnicode(false);

            modelBuilder.Entity<OEAMSTemplate>()
                .Property(e => e.NTFY2Phone)
                .IsUnicode(false);

            modelBuilder.Entity<OEAMSTemplate>()
                .Property(e => e.IsVoid)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<OEAWBSUM>()
                .Property(e => e.IDType)
                .IsUnicode(false);

            modelBuilder.Entity<OEAWBSUM>()
                .Property(e => e.STATIONID)
                .IsUnicode(false);

            modelBuilder.Entity<OEAWBSUM>()
                .Property(e => e.ReportCurrency)
                .IsUnicode(false);

            modelBuilder.Entity<OEAWBSUM>()
                .Property(e => e.LocalCurrency)
                .IsUnicode(false);

            modelBuilder.Entity<OEAWBSUM>()
                .Property(e => e.Version)
                .IsFixedLength();

            modelBuilder.Entity<OEAWBSUM>()
                .Property(e => e.CreateBy)
                .IsUnicode(false);

            modelBuilder.Entity<OEAWBSUM>()
                .Property(e => e.UpdatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<OEBookingRemark>()
                .Property(e => e.StationID)
                .IsUnicode(false);

            modelBuilder.Entity<OEBookingRemark>()
                .Property(e => e.SrcStationID)
                .IsUnicode(false);

            modelBuilder.Entity<OEBookingRemark>()
                .Property(e => e.CreatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<OEBookingRemark>()
                .Property(e => e.UpdatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<OEColoadBooking>()
                .Property(e => e.LinkStation)
                .IsUnicode(false);

            modelBuilder.Entity<OEColoadBooking>()
                .Property(e => e.Instruction)
                .IsUnicode(false);

            modelBuilder.Entity<OEColoadBooking>()
                .Property(e => e.MoveType)
                .IsUnicode(false);

            modelBuilder.Entity<OEColoadBooking>()
                .Property(e => e.FreightPayType)
                .IsUnicode(false);

            modelBuilder.Entity<OEColoadBooking>()
                .Property(e => e.LoadType)
                .IsUnicode(false);

            modelBuilder.Entity<OEColoadBooking>()
                .Property(e => e.LastMilestone)
                .IsUnicode(false);

            modelBuilder.Entity<OEColoadBooking>()
                .Property(e => e.PCSUOM)
                .IsUnicode(false);

            modelBuilder.Entity<OEColoadBooking>()
                .Property(e => e.WeightUOM)
                .IsUnicode(false);

            modelBuilder.Entity<OEColoadBooking>()
                .Property(e => e.CBM)
                .HasPrecision(18, 4);

            modelBuilder.Entity<OEColoadBooking>()
                .Property(e => e.CBMUOM)
                .IsUnicode(false);

            modelBuilder.Entity<OEColoadBooking>()
                .Property(e => e.CTNRType)
                .IsUnicode(false);

            modelBuilder.Entity<OEColoadBooking>()
                .Property(e => e.Send)
                .IsUnicode(false);

            modelBuilder.Entity<OEColoadBooking>()
                .Property(e => e.CreatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<OEColoadBooking>()
                .Property(e => e.UpdatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<OEColoadBooking>()
                .Property(e => e.Status)
                .IsUnicode(false);

            modelBuilder.Entity<OEColoadBooking>()
                .Property(e => e.OWBType)
                .IsUnicode(false);

            modelBuilder.Entity<OECustomerEBooking>()
                .Property(e => e.StationID)
                .IsUnicode(false);

            modelBuilder.Entity<OECustomerEBooking>()
                .Property(e => e.BookingNo)
                .IsUnicode(false);

            modelBuilder.Entity<OECustomerEBooking>()
                .Property(e => e.MoveType)
                .IsUnicode(false);

            modelBuilder.Entity<OECustomerEBooking>()
                .Property(e => e.FreightPayType)
                .IsUnicode(false);

            modelBuilder.Entity<OECustomerEBooking>()
                .Property(e => e.TradeType)
                .IsUnicode(false);

            modelBuilder.Entity<OECustomerEBooking>()
                .Property(e => e.IsSelfConsol)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<OECustomerEBooking>()
                .Property(e => e.CreatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<OECustomerEBooking>()
                .Property(e => e.UpdatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<OECustomerEBooking>()
                .Property(e => e.HBLType)
                .IsUnicode(false);

            modelBuilder.Entity<OECustomerEBookingInfoMap>()
                .Property(e => e.StationID)
                .IsUnicode(false);

            modelBuilder.Entity<OECustomerEBookingInfoMap>()
                .Property(e => e.CompanyCode)
                .IsUnicode(false);

            modelBuilder.Entity<OECustomerEBookingInfoMap>()
                .Property(e => e.Type)
                .IsUnicode(false);

            modelBuilder.Entity<OECustomerEBookingInfoMap>()
                .Property(e => e.CreatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<OECustomerEBookingInfoMap>()
                .Property(e => e.UpdatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<OECustomerEBookingPO>()
                .Property(e => e.StationID)
                .IsUnicode(false);

            modelBuilder.Entity<OECustomerEBookingPO>()
                .Property(e => e.PONo)
                .IsUnicode(false);

            modelBuilder.Entity<OECustomerEBookingPO>()
                .Property(e => e.CreatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<OECustomerEBookingPO>()
                .Property(e => e.UpdatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<OECustomerEBookingSO>()
                .Property(e => e.StationID)
                .IsUnicode(false);

            modelBuilder.Entity<OECustomerEBookingSO>()
                .Property(e => e.PCSUOM)
                .IsUnicode(false);

            modelBuilder.Entity<OECustomerEBookingSO>()
                .Property(e => e.WeightUOM)
                .IsUnicode(false);

            modelBuilder.Entity<OECustomerEBookingSO>()
                .Property(e => e.CBM)
                .HasPrecision(18, 4);

            modelBuilder.Entity<OECustomerEBookingSO>()
                .Property(e => e.CBMUOM)
                .IsUnicode(false);

            modelBuilder.Entity<OECustomerEBookingSO>()
                .Property(e => e.CTNRType)
                .IsUnicode(false);

            modelBuilder.Entity<OECustomerEBookingSO>()
                .Property(e => e.CreatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<OECustomerEBookingSO>()
                .Property(e => e.UpdatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<OEFLading>()
                .Property(e => e.StationID)
                .IsUnicode(false);

            modelBuilder.Entity<OEFLading>()
                .Property(e => e.HBLNo)
                .IsUnicode(false);

            modelBuilder.Entity<OEFLading>()
                .Property(e => e.SONumber)
                .IsUnicode(false);

            modelBuilder.Entity<OEFLading>()
                .Property(e => e.FMCNumber)
                .IsUnicode(false);

            modelBuilder.Entity<OEFLading>()
                .Property(e => e.NoOfOrgBL)
                .IsUnicode(false);

            modelBuilder.Entity<OEFLading>()
                .Property(e => e.DeclareValue)
                .IsUnicode(false);

            modelBuilder.Entity<OEFLading>()
                .Property(e => e.CreatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<OEFLading>()
                .Property(e => e.UpdatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<OEFLading>()
                .Property(e => e.PrintBy)
                .IsUnicode(false);

            modelBuilder.Entity<OEFLading>()
                .Property(e => e.PDeliveryCode)
                .IsUnicode(false);

            modelBuilder.Entity<OEFLadingRet>()
                .Property(e => e.StationID)
                .IsUnicode(false);

            modelBuilder.Entity<OEFLadingRet>()
                .Property(e => e.HBLNo)
                .IsUnicode(false);

            modelBuilder.Entity<OEFLadingRet>()
                .Property(e => e.SONumber)
                .IsUnicode(false);

            modelBuilder.Entity<OEFLadingRet>()
                .Property(e => e.FMCNumber)
                .IsUnicode(false);

            modelBuilder.Entity<OEFLadingRet>()
                .Property(e => e.NoOfOrgBL)
                .IsUnicode(false);

            modelBuilder.Entity<OEFLadingRet>()
                .Property(e => e.DeclareValue)
                .IsUnicode(false);

            modelBuilder.Entity<OEFLadingRet>()
                .Property(e => e.CreatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<OEFLadingRet>()
                .Property(e => e.UpdatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<OEFLadingSORet>()
                .Property(e => e.StationID)
                .IsUnicode(false);

            modelBuilder.Entity<OEFLadingSORet>()
                .Property(e => e.SoFor)
                .IsUnicode(false);

            modelBuilder.Entity<OEFLadingSORet>()
                .Property(e => e.SONo)
                .IsUnicode(false);

            modelBuilder.Entity<OEFLadingSORet>()
                .Property(e => e.PCSUOM)
                .IsUnicode(false);

            modelBuilder.Entity<OEFLadingSORet>()
                .Property(e => e.WeightUOM)
                .IsUnicode(false);

            modelBuilder.Entity<OEFLadingSORet>()
                .Property(e => e.CBM)
                .HasPrecision(18, 4);

            modelBuilder.Entity<OEFLadingSORet>()
                .Property(e => e.CBMUOM)
                .IsUnicode(false);

            modelBuilder.Entity<OEFLadingSORet>()
                .Property(e => e.CTNRType)
                .IsUnicode(false);

            modelBuilder.Entity<OEFLadingSORet>()
                .Property(e => e.CTNRNo)
                .IsUnicode(false);

            modelBuilder.Entity<OEFLadingSORet>()
                .Property(e => e.SealNo)
                .IsUnicode(false);

            modelBuilder.Entity<OEFLadingSORet>()
                .Property(e => e.CreatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<OEFLadingSORet>()
                .Property(e => e.UpdatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<OEFLadingSORet>()
                .Property(e => e.Version)
                .IsFixedLength();

            modelBuilder.Entity<OEFLadingSORet>()
                .Property(e => e.SrcStationID)
                .IsUnicode(false);

            modelBuilder.Entity<OEFTPLading>()
                .Property(e => e.StationID)
                .IsUnicode(false);

            modelBuilder.Entity<OEFTPLading>()
                .Property(e => e.HBLNo)
                .IsUnicode(false);

            modelBuilder.Entity<OEFTPLading>()
                .Property(e => e.SONumber)
                .IsUnicode(false);

            modelBuilder.Entity<OEFTPLading>()
                .Property(e => e.FMCNumber)
                .IsUnicode(false);

            modelBuilder.Entity<OEFTPLading>()
                .Property(e => e.NoOfOrgBL)
                .IsUnicode(false);

            modelBuilder.Entity<OEFTPLading>()
                .Property(e => e.DeclareValue)
                .IsUnicode(false);

            modelBuilder.Entity<OEFTPLading>()
                .Property(e => e.CreatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<OEFTPLading>()
                .Property(e => e.UpdatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<OEHBL>()
                .Property(e => e.HBLNo)
                .IsUnicode(false);

            modelBuilder.Entity<OEHBL>()
                .Property(e => e.StationID)
                .IsUnicode(false);

            modelBuilder.Entity<OEHBL>()
                .Property(e => e.Instruction)
                .IsUnicode(false);

            modelBuilder.Entity<OEHBL>()
                .Property(e => e.ShptType)
                .IsUnicode(false);

            modelBuilder.Entity<OEHBL>()
                .Property(e => e.MoveType)
                .IsUnicode(false);

            modelBuilder.Entity<OEHBL>()
                .Property(e => e.FreightPayType)
                .IsUnicode(false);

            modelBuilder.Entity<OEHBL>()
                .Property(e => e.TradeType)
                .IsUnicode(false);

            modelBuilder.Entity<OEHBL>()
                .Property(e => e.SalesType)
                .IsUnicode(false);

            modelBuilder.Entity<OEHBL>()
                .Property(e => e.ExportRef)
                .IsUnicode(false);

            modelBuilder.Entity<OEHBL>()
                .Property(e => e.CTNRInWord)
                .IsUnicode(false);

            modelBuilder.Entity<OEHBL>()
                .Property(e => e.DecValueCurr)
                .IsUnicode(false);

            modelBuilder.Entity<OEHBL>()
                .Property(e => e.TelexRelease)
                .IsUnicode(false);

            modelBuilder.Entity<OEHBL>()
                .Property(e => e.IsDraft)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<OEHBL>()
                .Property(e => e.IsBookmark)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<OEHBL>()
                .Property(e => e.IsVoid)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<OEHBL>()
                .Property(e => e.SHNumOfDR)
                .IsUnicode(false);

            modelBuilder.Entity<OEHBL>()
                .Property(e => e.USOTILic)
                .IsUnicode(false);

            modelBuilder.Entity<OEHBL>()
                .Property(e => e.USSVILic)
                .IsUnicode(false);

            modelBuilder.Entity<OEHBL>()
                .Property(e => e.USExportRef)
                .IsUnicode(false);

            modelBuilder.Entity<OEHBL>()
                .Property(e => e.USDeclValue)
                .IsUnicode(false);

            modelBuilder.Entity<OEHBL>()
                .Property(e => e.CreatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<OEHBL>()
                .Property(e => e.UpdatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<OEHBL>()
                .Property(e => e.Version)
                .IsFixedLength();

            modelBuilder.Entity<OEHBL>()
                .Property(e => e.PreAlertBy)
                .IsUnicode(false);

            modelBuilder.Entity<OEHBL>()
                .Property(e => e.BookingNo)
                .IsUnicode(false);

            modelBuilder.Entity<OEHBL>()
                .Property(e => e.SrcStationID)
                .IsUnicode(false);

            modelBuilder.Entity<OEHBL>()
                .Property(e => e.FCalculateType)
                .IsUnicode(false);

            modelBuilder.Entity<OEHBL>()
                .Property(e => e.ServiceType)
                .IsUnicode(false);

            modelBuilder.Entity<OEHBL>()
                .Property(e => e.CYMBy)
                .IsUnicode(false);

            modelBuilder.Entity<OEHBL>()
                .Property(e => e.IMPStation)
                .IsUnicode(false);

            modelBuilder.Entity<OEHBL>()
                .Property(e => e.QuoteTypeCreatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<OEHBL>()
                .Property(e => e.ReQuoteTypeCreatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<OEHBL>()
                .Property(e => e.MTSNO)
                .IsUnicode(false);

            modelBuilder.Entity<OEHBL>()
                .Property(e => e.AssignTo)
                .IsUnicode(false);

            modelBuilder.Entity<OEHBL>()
                .Property(e => e.BookingRemark)
                .IsUnicode(false);

            modelBuilder.Entity<OEHBL>()
                .Property(e => e.BookingSendBy)
                .IsUnicode(false);

            modelBuilder.Entity<OEHBL>()
                .Property(e => e.DSTNStationID)
                .IsUnicode(false);

            modelBuilder.Entity<OEHBL>()
                .Property(e => e.ConfirmID)
                .IsUnicode(false);

            modelBuilder.Entity<OEHBL>()
                .Property(e => e.QuoteNo)
                .IsUnicode(false);

            modelBuilder.Entity<OEHBL>()
                .Property(e => e.BookingStatus)
                .IsUnicode(false);

            modelBuilder.Entity<OEHBL>()
                .Property(e => e.IsISF)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<OEHBL>()
                .Property(e => e.IsAMS)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<OEHBL>()
                .Property(e => e.IsCoLoadIn)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<OEHBL>()
                .Property(e => e.CustBookingNo)
                .IsUnicode(false);

            modelBuilder.Entity<OEHBL>()
                .Property(e => e.UNNumber)
                .IsUnicode(false);

            modelBuilder.Entity<OEHBL>()
                .Property(e => e.RevenueTon)
                .HasPrecision(18, 4);

            modelBuilder.Entity<OEHBL>()
                .Property(e => e.CCN)
                .IsUnicode(false);

            modelBuilder.Entity<OEHBLWHEntry>()
                .Property(e => e.StationID)
                .IsUnicode(false);

            modelBuilder.Entity<OEHBLWHEntry>()
                .Property(e => e.CreatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<OEHBLWHEntry>()
                .Property(e => e.UpdatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<OEHBLWS>()
                .Property(e => e.ROType)
                .IsFixedLength();

            modelBuilder.Entity<OEHBLWS>()
                .Property(e => e.StationID)
                .IsUnicode(false);

            modelBuilder.Entity<OEHBLWS>()
                .Property(e => e.SrcStationID)
                .IsUnicode(false);

            modelBuilder.Entity<OEHBLWSTemplate>()
                .Property(e => e.ROType)
                .IsFixedLength();

            modelBuilder.Entity<OEHBLWSTemplate>()
                .Property(e => e.StationID)
                .IsUnicode(false);

            modelBuilder.Entity<OELoadingPlan>()
                .Property(e => e.StationID)
                .IsUnicode(false);

            modelBuilder.Entity<OELoadingPlan>()
                .Property(e => e.LoadingPlanNo)
                .IsUnicode(false);

            modelBuilder.Entity<OELoadingPlan>()
                .Property(e => e.OceanVessel)
                .IsUnicode(false);

            modelBuilder.Entity<OELoadingPlan>()
                .Property(e => e.OceanVoyage)
                .IsUnicode(false);

            modelBuilder.Entity<OELoadingPlan>()
                .Property(e => e.CarrierSONo)
                .IsUnicode(false);

            modelBuilder.Entity<OELoadingPlan>()
                .Property(e => e.CTNRType)
                .IsUnicode(false);

            modelBuilder.Entity<OELoadingPlan>()
                .Property(e => e.CTNRNo)
                .IsUnicode(false);

            modelBuilder.Entity<OELoadingPlan>()
                .Property(e => e.SealNo)
                .IsUnicode(false);

            modelBuilder.Entity<OELoadingPlan>()
                .Property(e => e.ShptType)
                .IsUnicode(false);

            modelBuilder.Entity<OELoadingPlan>()
                .Property(e => e.MoveType)
                .IsUnicode(false);

            modelBuilder.Entity<OELoadingPlan>()
                .Property(e => e.FreightPayType)
                .IsUnicode(false);

            modelBuilder.Entity<OELoadingPlan>()
                .Property(e => e.Status)
                .IsUnicode(false);

            modelBuilder.Entity<OELoadingPlan>()
                .Property(e => e.CreatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<OELoadingPlan>()
                .Property(e => e.UpdatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<OELoadingPlan>()
                .Property(e => e.Version)
                .IsFixedLength();

            modelBuilder.Entity<OELoadingPlan>()
                .Property(e => e.Instruction)
                .IsUnicode(false);

            modelBuilder.Entity<OEMBL>()
                .Property(e => e.StationID)
                .IsUnicode(false);

            modelBuilder.Entity<OEMBL>()
                .Property(e => e.MBLType)
                .IsUnicode(false);

            modelBuilder.Entity<OEMBL>()
                .Property(e => e.MBLNo)
                .IsUnicode(false);

            modelBuilder.Entity<OEMBL>()
                .Property(e => e.LotNo)
                .IsUnicode(false);

            modelBuilder.Entity<OEMBL>()
                .Property(e => e.OceanVessel)
                .IsUnicode(false);

            modelBuilder.Entity<OEMBL>()
                .Property(e => e.OceanVoyage)
                .IsUnicode(false);

            modelBuilder.Entity<OEMBL>()
                .Property(e => e.FeederVessel)
                .IsUnicode(false);

            modelBuilder.Entity<OEMBL>()
                .Property(e => e.FeederVoyage)
                .IsUnicode(false);

            modelBuilder.Entity<OEMBL>()
                .Property(e => e.ShptType)
                .IsUnicode(false);

            modelBuilder.Entity<OEMBL>()
                .Property(e => e.MoveType)
                .IsUnicode(false);

            modelBuilder.Entity<OEMBL>()
                .Property(e => e.FreightPayType)
                .IsUnicode(false);

            modelBuilder.Entity<OEMBL>()
                .Property(e => e.IsBookmark)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<OEMBL>()
                .Property(e => e.Status)
                .IsUnicode(false);

            modelBuilder.Entity<OEMBL>()
                .Property(e => e.CreatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<OEMBL>()
                .Property(e => e.UpdatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<OEMBL>()
                .Property(e => e.Version)
                .IsFixedLength();

            modelBuilder.Entity<OEMBL>()
                .Property(e => e.PreAlertBy)
                .IsUnicode(false);

            modelBuilder.Entity<OEMBL>()
                .Property(e => e.eManifestBy)
                .IsUnicode(false);

            modelBuilder.Entity<OEMBL>()
                .Property(e => e.SrcStationID)
                .IsUnicode(false);

            modelBuilder.Entity<OEMBL>()
                .Property(e => e.TradeType)
                .IsUnicode(false);

            modelBuilder.Entity<OEMBL>()
                .Property(e => e.FCalculateType)
                .IsUnicode(false);

            modelBuilder.Entity<OEMBL>()
                .Property(e => e.ServiceType)
                .IsUnicode(false);

            modelBuilder.Entity<OEMBL>()
                .Property(e => e.BookingNO)
                .IsUnicode(false);

            modelBuilder.Entity<OEMBL>()
                .Property(e => e.IMPStation)
                .IsUnicode(false);

            modelBuilder.Entity<OEMBL>()
                .Property(e => e.Instruction)
                .IsUnicode(false);

            modelBuilder.Entity<OEMBL>()
                .Property(e => e.RefNumber)
                .IsUnicode(false);

            modelBuilder.Entity<OEMBL>()
                .Property(e => e.MTSNO)
                .IsUnicode(false);

            modelBuilder.Entity<OEMBL>()
                .Property(e => e.ColoaderMBLNo)
                .IsUnicode(false);

            modelBuilder.Entity<OEMBL>()
                .Property(e => e.CargoControlNo)
                .IsUnicode(false);

            modelBuilder.Entity<OEMBLCREntry>()
                .Property(e => e.MBLType)
                .IsUnicode(false);

            modelBuilder.Entity<OEMBLCREntry>()
                .Property(e => e.StationID)
                .IsUnicode(false);

            modelBuilder.Entity<OEMBLCREntry>()
                .Property(e => e.CreatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<OEMBLCREntry>()
                .Property(e => e.UpdatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<OEOrderPreview>()
                .Property(e => e.StationID)
                .IsUnicode(false);

            modelBuilder.Entity<OEOrderPreview>()
                .Property(e => e.PickupType)
                .IsUnicode(false);

            modelBuilder.Entity<OEOrderPreview>()
                .Property(e => e.PCS)
                .IsUnicode(false);

            modelBuilder.Entity<OEOrderPreview>()
                .Property(e => e.PCSUOM)
                .IsUnicode(false);

            modelBuilder.Entity<OEOrderPreview>()
                .Property(e => e.WT)
                .IsUnicode(false);

            modelBuilder.Entity<OEOrderPreview>()
                .Property(e => e.WTUOM)
                .IsUnicode(false);

            modelBuilder.Entity<OEOrderPreview>()
                .Property(e => e.CBMUOM)
                .IsUnicode(false);

            modelBuilder.Entity<OEOrderPreview>()
                .Property(e => e.NeedINV)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<OEOrderPreview>()
                .Property(e => e.NeedPL)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<OEOrderPreview>()
                .Property(e => e.CreatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<OEOrderPreview>()
                .Property(e => e.UpdatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<OEOrderPreview>()
                .Property(e => e.Version)
                .IsFixedLength();

            modelBuilder.Entity<OEOrderPreview>()
                .Property(e => e.DeliveryNeedINV)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<OEOrderPreview>()
                .Property(e => e.DeliveryNeedPL)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<OEOrderPreview>()
                .Property(e => e.NeedBooking)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<OEOrderPreview>()
                .Property(e => e.DeliveryNeedBooking)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<OEOrderPreview>()
                .Property(e => e.PickupNeedINVTWO)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<OEOrderPreview>()
                .Property(e => e.PickupNeedPLTWO)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<OEOrderPreview>()
                .Property(e => e.PickupNeedBookingTWO)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<OEOrderPreview>()
                .Property(e => e.DeliveryNeedINVTWO)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<OEOrderPreview>()
                .Property(e => e.DeliveryNeedPLTWO)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<OEOrderPreview>()
                .Property(e => e.DeliveryNeedBookingTWO)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<OEPickup>()
                .Property(e => e.StationID)
                .IsUnicode(false);

            modelBuilder.Entity<OEPickup>()
                .Property(e => e.PickupType)
                .IsUnicode(false);

            modelBuilder.Entity<OEPickup>()
                .Property(e => e.PCSUOM)
                .IsUnicode(false);

            modelBuilder.Entity<OEPickup>()
                .Property(e => e.WTUOM)
                .IsUnicode(false);

            modelBuilder.Entity<OEPickup>()
                .Property(e => e.CBMUOM)
                .IsUnicode(false);

            modelBuilder.Entity<OEPickup>()
                .Property(e => e.NeedINV)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<OEPickup>()
                .Property(e => e.NeedPL)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<OEPickup>()
                .Property(e => e.CreatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<OEPickup>()
                .Property(e => e.UpdatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<OEPickup>()
                .Property(e => e.Version)
                .IsFixedLength();

            modelBuilder.Entity<OEPickup>()
                .Property(e => e.DeliveryNeedINV)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<OEPickup>()
                .Property(e => e.DeliveryNeedPL)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<OEPickup>()
                .Property(e => e.CTNRNo)
                .IsUnicode(false);

            modelBuilder.Entity<OEPickup>()
                .Property(e => e.SealNo)
                .IsUnicode(false);

            modelBuilder.Entity<OEPickup>()
                .Property(e => e.NeedBooking)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<OEPickup>()
                .Property(e => e.DeliveryNeedBooking)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<OEPickup>()
                .Property(e => e.DeliverToName)
                .IsUnicode(false);

            modelBuilder.Entity<OEPickup>()
                .Property(e => e.PickupNeedINVTWO)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<OEPickup>()
                .Property(e => e.PickupNeedPLTWO)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<OEPickup>()
                .Property(e => e.PickupNeedBookingTWO)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<OEPickup>()
                .Property(e => e.DeliveryNeedINVTWO)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<OEPickup>()
                .Property(e => e.DeliveryNeedPLTWO)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<OEPickup>()
                .Property(e => e.DeliveryNeedBookingTWO)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<OEPO>()
                .Property(e => e.StationID)
                .IsUnicode(false);

            modelBuilder.Entity<OEPO>()
                .Property(e => e.PONo)
                .IsUnicode(false);

            modelBuilder.Entity<OEPO>()
                .Property(e => e.SerialNo)
                .IsUnicode(false);

            modelBuilder.Entity<OEPO>()
                .Property(e => e.Remarks)
                .IsUnicode(false);

            modelBuilder.Entity<OEPO>()
                .Property(e => e.POParty)
                .IsUnicode(false);

            modelBuilder.Entity<OEPO>()
                .Property(e => e.CreatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<OEPO>()
                .Property(e => e.UpdatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<OEPO>()
                .Property(e => e.Version)
                .IsFixedLength();

            modelBuilder.Entity<OEPO>()
                .Property(e => e.ItemNo)
                .IsUnicode(false);

            modelBuilder.Entity<OEPO>()
                .Property(e => e.Description)
                .IsUnicode(false);

            modelBuilder.Entity<OEPO>()
                .Property(e => e.Quantity)
                .IsUnicode(false);

            modelBuilder.Entity<OEPO>()
                .Property(e => e.SONO)
                .IsUnicode(false);

            modelBuilder.Entity<OEPO>()
                .Property(e => e.Curr)
                .IsUnicode(false);

            modelBuilder.Entity<OEPO>()
                .Property(e => e.SrcStationID)
                .IsUnicode(false);

            modelBuilder.Entity<OEPO>()
                .Property(e => e.ShptType)
                .IsUnicode(false);

            modelBuilder.Entity<OEPO>()
                .Property(e => e.UOM)
                .IsUnicode(false);

            modelBuilder.Entity<OEPOD>()
                .Property(e => e.StationID)
                .IsUnicode(false);

            modelBuilder.Entity<OEPOD>()
                .Property(e => e.SrcStationID)
                .IsUnicode(false);

            modelBuilder.Entity<OEPOD>()
                .Property(e => e.CustomsReleaseNo)
                .IsUnicode(false);

            modelBuilder.Entity<OEPOD>()
                .Property(e => e.DocumentReleaseType)
                .IsUnicode(false);

            modelBuilder.Entity<OEPOD>()
                .Property(e => e.FreightReleaseType)
                .IsUnicode(false);

            modelBuilder.Entity<OEPOD>()
                .Property(e => e.CreatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<OEPOD>()
                .Property(e => e.UpdatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<OEPOD>()
                .Property(e => e.Version)
                .IsFixedLength();

            modelBuilder.Entity<OEPOD>()
                .Property(e => e.NotifyPartyType)
                .IsUnicode(false);

            modelBuilder.Entity<OESO>()
                .Property(e => e.StationID)
                .IsUnicode(false);

            modelBuilder.Entity<OESO>()
                .Property(e => e.SoFor)
                .IsUnicode(false);

            modelBuilder.Entity<OESO>()
                .Property(e => e.SONo)
                .IsUnicode(false);

            modelBuilder.Entity<OESO>()
                .Property(e => e.PCSUOM)
                .IsUnicode(false);

            modelBuilder.Entity<OESO>()
                .Property(e => e.Weight)
                .HasPrecision(18, 3);

            modelBuilder.Entity<OESO>()
                .Property(e => e.WeightUOM)
                .IsUnicode(false);

            modelBuilder.Entity<OESO>()
                .Property(e => e.CBM)
                .HasPrecision(18, 4);

            modelBuilder.Entity<OESO>()
                .Property(e => e.CBMUOM)
                .IsUnicode(false);

            modelBuilder.Entity<OESO>()
                .Property(e => e.CTNRType)
                .IsUnicode(false);

            modelBuilder.Entity<OESO>()
                .Property(e => e.CTNRNo)
                .IsUnicode(false);

            modelBuilder.Entity<OESO>()
                .Property(e => e.SealNo)
                .IsUnicode(false);

            modelBuilder.Entity<OESO>()
                .Property(e => e.CreatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<OESO>()
                .Property(e => e.UpdatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<OESO>()
                .Property(e => e.Version)
                .IsFixedLength();

            modelBuilder.Entity<OESO>()
                .Property(e => e.SrcStationID)
                .IsUnicode(false);

            modelBuilder.Entity<OESO>()
                .Property(e => e.Ton)
                .HasPrecision(18, 4);

            modelBuilder.Entity<OETariff>()
                .Property(e => e.StationID)
                .IsUnicode(false);

            modelBuilder.Entity<OETariff>()
                .Property(e => e.TariffNo)
                .IsUnicode(false);

            modelBuilder.Entity<OETariff>()
                .Property(e => e.Currency)
                .IsUnicode(false);

            modelBuilder.Entity<OETariff>()
                .Property(e => e.CreatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<OETariff>()
                .Property(e => e.UpdatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<OETariff>()
                .Property(e => e.Version)
                .IsFixedLength();

            modelBuilder.Entity<OIAWBSUM>()
                .Property(e => e.IDType)
                .IsUnicode(false);

            modelBuilder.Entity<OIAWBSUM>()
                .Property(e => e.STATIONID)
                .IsUnicode(false);

            modelBuilder.Entity<OIAWBSUM>()
                .Property(e => e.ReportCurrency)
                .IsUnicode(false);

            modelBuilder.Entity<OIAWBSUM>()
                .Property(e => e.LocalCurrency)
                .IsUnicode(false);

            modelBuilder.Entity<OIAWBSUM>()
                .Property(e => e.Version)
                .IsFixedLength();

            modelBuilder.Entity<OIAWBSUM>()
                .Property(e => e.CreateBy)
                .IsUnicode(false);

            modelBuilder.Entity<OIAWBSUM>()
                .Property(e => e.UpdatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<OIBooking>()
                .Property(e => e.BookingNo)
                .IsUnicode(false);

            modelBuilder.Entity<OIBooking>()
                .Property(e => e.StationID)
                .IsUnicode(false);

            modelBuilder.Entity<OIBooking>()
                .Property(e => e.SrcStationID)
                .IsUnicode(false);

            modelBuilder.Entity<OIBooking>()
                .Property(e => e.IsISF)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<OIBooking>()
                .Property(e => e.IsAMS)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<OIBooking>()
                .Property(e => e.IsCoLoadIn)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<OIBooking>()
                .Property(e => e.BookingRemark)
                .IsUnicode(false);

            modelBuilder.Entity<OIBooking>()
                .Property(e => e.ShptType)
                .IsUnicode(false);

            modelBuilder.Entity<OIBooking>()
                .Property(e => e.MoveType)
                .IsUnicode(false);

            modelBuilder.Entity<OIBooking>()
                .Property(e => e.FreightPayType)
                .IsUnicode(false);

            modelBuilder.Entity<OIBooking>()
                .Property(e => e.FCalculateType)
                .IsUnicode(false);

            modelBuilder.Entity<OIBooking>()
                .Property(e => e.TradeType)
                .IsUnicode(false);

            modelBuilder.Entity<OIBooking>()
                .Property(e => e.AssignTo)
                .IsUnicode(false);

            modelBuilder.Entity<OIBooking>()
                .Property(e => e.SalesType)
                .IsUnicode(false);

            modelBuilder.Entity<OIBooking>()
                .Property(e => e.CTNRInWord)
                .IsUnicode(false);

            modelBuilder.Entity<OIBooking>()
                .Property(e => e.DecValueCurr)
                .IsUnicode(false);

            modelBuilder.Entity<OIBooking>()
                .Property(e => e.TelexRelease)
                .IsUnicode(false);

            modelBuilder.Entity<OIBooking>()
                .Property(e => e.IsDraft)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<OIBooking>()
                .Property(e => e.IsVoid)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<OIBooking>()
                .Property(e => e.DSTNStationID)
                .IsUnicode(false);

            modelBuilder.Entity<OIBooking>()
                .Property(e => e.CreatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<OIBooking>()
                .Property(e => e.UpdatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<OIBooking>()
                .Property(e => e.Status)
                .IsUnicode(false);

            modelBuilder.Entity<OIBooking>()
                .Property(e => e.StatusDetail)
                .IsUnicode(false);

            modelBuilder.Entity<OIBooking>()
                .Property(e => e.QuoteNo)
                .IsUnicode(false);

            modelBuilder.Entity<OIBooking>()
                .Property(e => e.Version)
                .IsFixedLength();

            modelBuilder.Entity<OIBookingPO>()
                .Property(e => e.StationID)
                .IsUnicode(false);

            modelBuilder.Entity<OIBookingPO>()
                .Property(e => e.SrcStationID)
                .IsUnicode(false);

            modelBuilder.Entity<OIBookingPO>()
                .Property(e => e.PONo)
                .IsUnicode(false);

            modelBuilder.Entity<OIBookingPO>()
                .Property(e => e.ItemNo)
                .IsUnicode(false);

            modelBuilder.Entity<OIBookingPO>()
                .Property(e => e.Description)
                .IsUnicode(false);

            modelBuilder.Entity<OIBookingPO>()
                .Property(e => e.CreatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<OIBookingPO>()
                .Property(e => e.UpdatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<OIBookingPO>()
                .Property(e => e.Version)
                .IsFixedLength();

            modelBuilder.Entity<OIBookingPO>()
                .Property(e => e.Quantity)
                .IsUnicode(false);

            modelBuilder.Entity<OIBookingRemark>()
                .Property(e => e.StationID)
                .IsUnicode(false);

            modelBuilder.Entity<OIBookingRemark>()
                .Property(e => e.SrcStationID)
                .IsUnicode(false);

            modelBuilder.Entity<OIBookingRemark>()
                .Property(e => e.CreatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<OIBookingRemark>()
                .Property(e => e.UpdatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<OIBookingSO>()
                .Property(e => e.StationID)
                .IsUnicode(false);

            modelBuilder.Entity<OIBookingSO>()
                .Property(e => e.SoFor)
                .IsUnicode(false);

            modelBuilder.Entity<OIBookingSO>()
                .Property(e => e.SrcStationID)
                .IsUnicode(false);

            modelBuilder.Entity<OIBookingSO>()
                .Property(e => e.SONo)
                .IsUnicode(false);

            modelBuilder.Entity<OIBookingSO>()
                .Property(e => e.PCSUOM)
                .IsUnicode(false);

            modelBuilder.Entity<OIBookingSO>()
                .Property(e => e.WeightUOM)
                .IsUnicode(false);

            modelBuilder.Entity<OIBookingSO>()
                .Property(e => e.CBM)
                .HasPrecision(18, 4);

            modelBuilder.Entity<OIBookingSO>()
                .Property(e => e.CBMUOM)
                .IsUnicode(false);

            modelBuilder.Entity<OIBookingSO>()
                .Property(e => e.DSTNStationID)
                .IsUnicode(false);

            modelBuilder.Entity<OIBookingSO>()
                .Property(e => e.CreatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<OIBookingSO>()
                .Property(e => e.UpdatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<OIBookingSO>()
                .Property(e => e.Version)
                .IsFixedLength();

            modelBuilder.Entity<OIBookingSO>()
                .Property(e => e.CTNRType)
                .IsUnicode(false);

            modelBuilder.Entity<OIBookingSO>()
                .Property(e => e.CTNRNo)
                .IsUnicode(false);

            modelBuilder.Entity<OIEntry>()
                .Property(e => e.CreatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<OIEntry>()
                .Property(e => e.UpdatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<OIFLading>()
                .Property(e => e.StationID)
                .IsUnicode(false);

            modelBuilder.Entity<OIFLading>()
                .Property(e => e.HBLNo)
                .IsUnicode(false);

            modelBuilder.Entity<OIFLading>()
                .Property(e => e.SONumber)
                .IsUnicode(false);

            modelBuilder.Entity<OIFLading>()
                .Property(e => e.FMCNumber)
                .IsUnicode(false);

            modelBuilder.Entity<OIFLading>()
                .Property(e => e.NoOfOrgBL)
                .IsUnicode(false);

            modelBuilder.Entity<OIFLading>()
                .Property(e => e.DeclareValue)
                .IsUnicode(false);

            modelBuilder.Entity<OIFLading>()
                .Property(e => e.CreatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<OIFLading>()
                .Property(e => e.UpdatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<OIHBL>()
                .Property(e => e.HBLNo)
                .IsUnicode(false);

            modelBuilder.Entity<OIHBL>()
                .Property(e => e.StationID)
                .IsUnicode(false);

            modelBuilder.Entity<OIHBL>()
                .Property(e => e.SrcStationID)
                .IsUnicode(false);

            modelBuilder.Entity<OIHBL>()
                .Property(e => e.Instruction)
                .IsUnicode(false);

            modelBuilder.Entity<OIHBL>()
                .Property(e => e.ShptType)
                .IsUnicode(false);

            modelBuilder.Entity<OIHBL>()
                .Property(e => e.MoveType)
                .IsUnicode(false);

            modelBuilder.Entity<OIHBL>()
                .Property(e => e.FreightPayType)
                .IsUnicode(false);

            modelBuilder.Entity<OIHBL>()
                .Property(e => e.TradeType)
                .IsUnicode(false);

            modelBuilder.Entity<OIHBL>()
                .Property(e => e.SalesType)
                .IsUnicode(false);

            modelBuilder.Entity<OIHBL>()
                .Property(e => e.ExportRef)
                .IsUnicode(false);

            modelBuilder.Entity<OIHBL>()
                .Property(e => e.CTNRInWord)
                .IsUnicode(false);

            modelBuilder.Entity<OIHBL>()
                .Property(e => e.DecValueCurr)
                .IsUnicode(false);

            modelBuilder.Entity<OIHBL>()
                .Property(e => e.TelexRelease)
                .IsUnicode(false);

            modelBuilder.Entity<OIHBL>()
                .Property(e => e.IsDraft)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<OIHBL>()
                .Property(e => e.IsBookmark)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<OIHBL>()
                .Property(e => e.IsVoid)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<OIHBL>()
                .Property(e => e.SHNumOfDR)
                .IsUnicode(false);

            modelBuilder.Entity<OIHBL>()
                .Property(e => e.USOTILic)
                .IsUnicode(false);

            modelBuilder.Entity<OIHBL>()
                .Property(e => e.USSVILic)
                .IsUnicode(false);

            modelBuilder.Entity<OIHBL>()
                .Property(e => e.USExportRef)
                .IsUnicode(false);

            modelBuilder.Entity<OIHBL>()
                .Property(e => e.USDeclValue)
                .IsUnicode(false);

            modelBuilder.Entity<OIHBL>()
                .Property(e => e.CreatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<OIHBL>()
                .Property(e => e.UpdatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<OIHBL>()
                .Property(e => e.Version)
                .IsFixedLength();

            modelBuilder.Entity<OIHBL>()
                .Property(e => e.PreAlertBy)
                .IsUnicode(false);

            modelBuilder.Entity<OIHBL>()
                .Property(e => e.FCalculateType)
                .IsUnicode(false);

            modelBuilder.Entity<OIHBL>()
                .Property(e => e.ServiceType)
                .IsUnicode(false);

            modelBuilder.Entity<OIHBL>()
                .Property(e => e.QuoteType)
                .IsUnicode(false);

            modelBuilder.Entity<OIHBL>()
                .Property(e => e.CYMBy)
                .IsUnicode(false);

            modelBuilder.Entity<OIHBL>()
                .Property(e => e.IMPStation)
                .IsUnicode(false);

            modelBuilder.Entity<OIHBL>()
                .Property(e => e.MTSNO)
                .IsUnicode(false);

            modelBuilder.Entity<OIHBL>()
                .Property(e => e.BookingNo)
                .IsUnicode(false);

            modelBuilder.Entity<OIHBL>()
                .Property(e => e.CustBookingNo)
                .IsUnicode(false);

            modelBuilder.Entity<OIHBL>()
                .Property(e => e.UNNumber)
                .IsUnicode(false);

            modelBuilder.Entity<OIHBL>()
                .Property(e => e.RevenueTon)
                .HasPrecision(18, 4);

            modelBuilder.Entity<OIHBL>()
                .Property(e => e.CCN)
                .IsUnicode(false);

            modelBuilder.Entity<OIHBLAgentInfo>()
                .Property(e => e.StationID)
                .IsUnicode(false);

            modelBuilder.Entity<OIHBLAgentInfo>()
                .Property(e => e.CreatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<OIHBLAgentInfo>()
                .Property(e => e.UpdatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<OIHBLAgentInfoMap>()
                .Property(e => e.StationID)
                .IsUnicode(false);

            modelBuilder.Entity<OIHBLAgentInfoMap>()
                .Property(e => e.CompanyCode)
                .IsUnicode(false);

            modelBuilder.Entity<OIHBLAgentInfoMap>()
                .Property(e => e.CreatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<OIHBLAgentInfoMap>()
                .Property(e => e.UpdatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<OIMBL>()
                .Property(e => e.StationID)
                .IsUnicode(false);

            modelBuilder.Entity<OIMBL>()
                .Property(e => e.SrcStationID)
                .IsUnicode(false);

            modelBuilder.Entity<OIMBL>()
                .Property(e => e.MBLNo)
                .IsUnicode(false);

            modelBuilder.Entity<OIMBL>()
                .Property(e => e.LotNo)
                .IsUnicode(false);

            modelBuilder.Entity<OIMBL>()
                .Property(e => e.OceanVessel)
                .IsUnicode(false);

            modelBuilder.Entity<OIMBL>()
                .Property(e => e.OceanVoyage)
                .IsUnicode(false);

            modelBuilder.Entity<OIMBL>()
                .Property(e => e.FeederVessel)
                .IsUnicode(false);

            modelBuilder.Entity<OIMBL>()
                .Property(e => e.FeederVoyage)
                .IsUnicode(false);

            modelBuilder.Entity<OIMBL>()
                .Property(e => e.ShptType)
                .IsUnicode(false);

            modelBuilder.Entity<OIMBL>()
                .Property(e => e.MoveType)
                .IsUnicode(false);

            modelBuilder.Entity<OIMBL>()
                .Property(e => e.FreightPayType)
                .IsUnicode(false);

            modelBuilder.Entity<OIMBL>()
                .Property(e => e.IsBookmark)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<OIMBL>()
                .Property(e => e.Status)
                .IsUnicode(false);

            modelBuilder.Entity<OIMBL>()
                .Property(e => e.CreatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<OIMBL>()
                .Property(e => e.UpdatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<OIMBL>()
                .Property(e => e.Version)
                .IsFixedLength();

            modelBuilder.Entity<OIMBL>()
                .Property(e => e.MBLType)
                .IsUnicode(false);

            modelBuilder.Entity<OIMBL>()
                .Property(e => e.PreAlertBy)
                .IsUnicode(false);

            modelBuilder.Entity<OIMBL>()
                .Property(e => e.TradeType)
                .IsUnicode(false);

            modelBuilder.Entity<OIMBL>()
                .Property(e => e.FCalculateType)
                .IsUnicode(false);

            modelBuilder.Entity<OIMBL>()
                .Property(e => e.ServiceType)
                .IsUnicode(false);

            modelBuilder.Entity<OIMBL>()
                .Property(e => e.QuoteType)
                .IsUnicode(false);

            modelBuilder.Entity<OIMBL>()
                .Property(e => e.MTSNO)
                .IsUnicode(false);

            modelBuilder.Entity<OIMBL>()
                .Property(e => e.ColoaderMBLNo)
                .IsUnicode(false);

            modelBuilder.Entity<OIPickup>()
                .Property(e => e.StationID)
                .IsUnicode(false);

            modelBuilder.Entity<OIPickup>()
                .Property(e => e.SrcStationID)
                .IsUnicode(false);

            modelBuilder.Entity<OIPickup>()
                .Property(e => e.PickupType)
                .IsUnicode(false);

            modelBuilder.Entity<OIPickup>()
                .Property(e => e.PCSUOM)
                .IsUnicode(false);

            modelBuilder.Entity<OIPickup>()
                .Property(e => e.WTUOM)
                .IsUnicode(false);

            modelBuilder.Entity<OIPickup>()
                .Property(e => e.CBMUOM)
                .IsUnicode(false);

            modelBuilder.Entity<OIPickup>()
                .Property(e => e.NeedINV)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<OIPickup>()
                .Property(e => e.NeedPL)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<OIPickup>()
                .Property(e => e.CreatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<OIPickup>()
                .Property(e => e.UpdatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<OIPickup>()
                .Property(e => e.Version)
                .IsFixedLength();

            modelBuilder.Entity<OIPickup>()
                .Property(e => e.DeliveryNeedINV)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<OIPickup>()
                .Property(e => e.DeliveryNeedPL)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<OIPickup>()
                .Property(e => e.CTNRNo)
                .IsUnicode(false);

            modelBuilder.Entity<OIPickup>()
                .Property(e => e.SealNo)
                .IsUnicode(false);

            modelBuilder.Entity<OIPickup>()
                .Property(e => e.NeedBooking)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<OIPickup>()
                .Property(e => e.DeliveryNeedBooking)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<OIPickup>()
                .Property(e => e.DeliverToName)
                .IsUnicode(false);

            modelBuilder.Entity<OIPickup>()
                .Property(e => e.PickupNeedINVTWO)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<OIPickup>()
                .Property(e => e.PickupNeedPLTWO)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<OIPickup>()
                .Property(e => e.PickupNeedBookingTWO)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<OIPickup>()
                .Property(e => e.DeliveryNeedINVTWO)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<OIPickup>()
                .Property(e => e.DeliveryNeedPLTWO)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<OIPickup>()
                .Property(e => e.DeliveryNeedBookingTWO)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<OIPO>()
                .Property(e => e.StationID)
                .IsUnicode(false);

            modelBuilder.Entity<OIPO>()
                .Property(e => e.SrcStationID)
                .IsUnicode(false);

            modelBuilder.Entity<OIPO>()
                .Property(e => e.PONo)
                .IsUnicode(false);

            modelBuilder.Entity<OIPO>()
                .Property(e => e.SerialNo)
                .IsUnicode(false);

            modelBuilder.Entity<OIPO>()
                .Property(e => e.Remarks)
                .IsUnicode(false);

            modelBuilder.Entity<OIPO>()
                .Property(e => e.POParty)
                .IsUnicode(false);

            modelBuilder.Entity<OIPO>()
                .Property(e => e.CreatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<OIPO>()
                .Property(e => e.UpdatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<OIPO>()
                .Property(e => e.Version)
                .IsFixedLength();

            modelBuilder.Entity<OIPO>()
                .Property(e => e.ItemNo)
                .IsUnicode(false);

            modelBuilder.Entity<OIPO>()
                .Property(e => e.Description)
                .IsUnicode(false);

            modelBuilder.Entity<OIPO>()
                .Property(e => e.Quantity)
                .IsUnicode(false);

            modelBuilder.Entity<OIPO>()
                .Property(e => e.SONO)
                .IsUnicode(false);

            modelBuilder.Entity<OIPO>()
                .Property(e => e.Curr)
                .IsUnicode(false);

            modelBuilder.Entity<OIPO>()
                .Property(e => e.UOM)
                .IsUnicode(false);

            modelBuilder.Entity<OIPOD>()
                .Property(e => e.StationID)
                .IsUnicode(false);

            modelBuilder.Entity<OIPOD>()
                .Property(e => e.CustomsReleaseNo)
                .IsUnicode(false);

            modelBuilder.Entity<OIPOD>()
                .Property(e => e.DocumentReleaseType)
                .IsUnicode(false);

            modelBuilder.Entity<OIPOD>()
                .Property(e => e.FreightReleaseType)
                .IsUnicode(false);

            modelBuilder.Entity<OIPOD>()
                .Property(e => e.CreatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<OIPOD>()
                .Property(e => e.UpdatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<OIPOD>()
                .Property(e => e.Version)
                .IsFixedLength();

            modelBuilder.Entity<OIPOD>()
                .Property(e => e.NotifyPartyType)
                .IsUnicode(false);

            modelBuilder.Entity<OISO>()
                .Property(e => e.StationID)
                .IsUnicode(false);

            modelBuilder.Entity<OISO>()
                .Property(e => e.SrcStationID)
                .IsUnicode(false);

            modelBuilder.Entity<OISO>()
                .Property(e => e.SoFor)
                .IsUnicode(false);

            modelBuilder.Entity<OISO>()
                .Property(e => e.SONo)
                .IsUnicode(false);

            modelBuilder.Entity<OISO>()
                .Property(e => e.PCSUOM)
                .IsUnicode(false);

            modelBuilder.Entity<OISO>()
                .Property(e => e.Weight)
                .HasPrecision(18, 3);

            modelBuilder.Entity<OISO>()
                .Property(e => e.WeightUOM)
                .IsUnicode(false);

            modelBuilder.Entity<OISO>()
                .Property(e => e.CBM)
                .HasPrecision(18, 4);

            modelBuilder.Entity<OISO>()
                .Property(e => e.CBMUOM)
                .IsUnicode(false);

            modelBuilder.Entity<OISO>()
                .Property(e => e.CTNRType)
                .IsUnicode(false);

            modelBuilder.Entity<OISO>()
                .Property(e => e.CTNRNo)
                .IsUnicode(false);

            modelBuilder.Entity<OISO>()
                .Property(e => e.SealNo)
                .IsUnicode(false);

            modelBuilder.Entity<OISO>()
                .Property(e => e.CreatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<OISO>()
                .Property(e => e.UpdatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<OISO>()
                .Property(e => e.Version)
                .IsFixedLength();

            modelBuilder.Entity<OISO>()
                .Property(e => e.Ton)
                .HasPrecision(18, 4);

            modelBuilder.Entity<TPAAWBSUM>()
                .Property(e => e.STATIONID)
                .IsUnicode(false);

            modelBuilder.Entity<TPAAWBSUM>()
                .Property(e => e.IDType)
                .IsUnicode(false);

            modelBuilder.Entity<TPAAWBSUM>()
                .Property(e => e.ReportCurrency)
                .IsUnicode(false);

            modelBuilder.Entity<TPAAWBSUM>()
                .Property(e => e.LocalCurrency)
                .IsUnicode(false);

            modelBuilder.Entity<TPAAWBSUM>()
                .Property(e => e.Version)
                .IsFixedLength();

            modelBuilder.Entity<TPAAWBSUM>()
                .Property(e => e.CreateBy)
                .IsUnicode(false);

            modelBuilder.Entity<TPAAWBSUM>()
                .Property(e => e.UpdatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<TPAIRData>()
                .Property(e => e.StationID)
                .IsUnicode(false);

            modelBuilder.Entity<TPAIRData>()
                .Property(e => e.TPID)
                .IsUnicode(false);

            modelBuilder.Entity<TPAIRData>()
                .Property(e => e.SourceStationID)
                .IsUnicode(false);

            modelBuilder.Entity<TPAIRData>()
                .Property(e => e.SourceType)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<TPAIRData>()
                .Property(e => e.MasterNo)
                .IsUnicode(false);

            modelBuilder.Entity<TPAIRData>()
                .Property(e => e.HouseNo)
                .IsUnicode(false);

            modelBuilder.Entity<TPAIRData>()
                .Property(e => e.Lot)
                .IsUnicode(false);

            modelBuilder.Entity<TPAIRData>()
                .Property(e => e.NetureOfGood)
                .IsUnicode(false);

            modelBuilder.Entity<TPAIRData>()
                .Property(e => e.ActPCSUOM)
                .IsUnicode(false);

            modelBuilder.Entity<TPAIRData>()
                .Property(e => e.WTUOM)
                .IsUnicode(false);

            modelBuilder.Entity<TPAIRData>()
                .Property(e => e.LotStatus)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<TPAIRData>()
                .Property(e => e.IsManiFast)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<TPAIRData>()
                .Property(e => e.Version)
                .IsFixedLength();

            modelBuilder.Entity<TPAIRData>()
                .Property(e => e.CreatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<TPAIRData>()
                .Property(e => e.UpdatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<TPAIRData>()
                .Property(e => e.ServiceLevel)
                .IsUnicode(false);

            modelBuilder.Entity<TPAIRData>()
                .Property(e => e.Scale)
                .IsUnicode(false);

            modelBuilder.Entity<TPAIRData>()
                .Property(e => e.PreMaster)
                .IsUnicode(false);

            modelBuilder.Entity<TPAIRData>()
                .Property(e => e.ConfirmID)
                .IsUnicode(false);

            modelBuilder.Entity<TPAIRData>()
                .Property(e => e.ShptType)
                .IsUnicode(false);

            modelBuilder.Entity<TPAIRData>()
                .Property(e => e.OnboardFLT)
                .IsUnicode(false);

            modelBuilder.Entity<TPAIRData>()
                .Property(e => e.isDAS)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<TPAirDataPO>()
                .Property(e => e.StationID)
                .IsUnicode(false);

            modelBuilder.Entity<TPAirDataPO>()
                .Property(e => e.TPID)
                .IsUnicode(false);

            modelBuilder.Entity<TPAirDataPO>()
                .Property(e => e.Version)
                .IsFixedLength();

            modelBuilder.Entity<TPAirDataPO>()
                .Property(e => e.CreatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<TPAirDataPO>()
                .Property(e => e.UpdatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<TPAirDataPO>()
                .Property(e => e.SONO)
                .IsUnicode(false);

            modelBuilder.Entity<TPAirDataPO>()
                .Property(e => e.Curr)
                .IsUnicode(false);

            modelBuilder.Entity<TPAirDataPO>()
                .Property(e => e.Amount)
                .HasPrecision(18, 6);

            modelBuilder.Entity<TPAirDataPO>()
                .Property(e => e.UOM)
                .IsUnicode(false);

            modelBuilder.Entity<TPOAWBSUM>()
                .Property(e => e.STATIONID)
                .IsUnicode(false);

            modelBuilder.Entity<TPOAWBSUM>()
                .Property(e => e.IDType)
                .IsUnicode(false);

            modelBuilder.Entity<TPOAWBSUM>()
                .Property(e => e.ReportCurrency)
                .IsUnicode(false);

            modelBuilder.Entity<TPOAWBSUM>()
                .Property(e => e.LocalCurrency)
                .IsUnicode(false);

            modelBuilder.Entity<TPOAWBSUM>()
                .Property(e => e.Version)
                .IsFixedLength();

            modelBuilder.Entity<TPOAWBSUM>()
                .Property(e => e.CreateBy)
                .IsUnicode(false);

            modelBuilder.Entity<TPOAWBSUM>()
                .Property(e => e.UpdatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<TPOceanData>()
                .Property(e => e.StationID)
                .IsUnicode(false);

            modelBuilder.Entity<TPOceanData>()
                .Property(e => e.TPID)
                .IsUnicode(false);

            modelBuilder.Entity<TPOceanData>()
                .Property(e => e.SourceStationID)
                .IsUnicode(false);

            modelBuilder.Entity<TPOceanData>()
                .Property(e => e.SourceType)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<TPOceanData>()
                .Property(e => e.MasterNo)
                .IsUnicode(false);

            modelBuilder.Entity<TPOceanData>()
                .Property(e => e.HouseNo)
                .IsUnicode(false);

            modelBuilder.Entity<TPOceanData>()
                .Property(e => e.Lot)
                .IsUnicode(false);

            modelBuilder.Entity<TPOceanData>()
                .Property(e => e.NetureOfGood)
                .IsUnicode(false);

            modelBuilder.Entity<TPOceanData>()
                .Property(e => e.HType)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<TPOceanData>()
                .Property(e => e.LotStatus)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<TPOceanData>()
                .Property(e => e.IsManiFast)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<TPOceanData>()
                .Property(e => e.Version)
                .IsFixedLength();

            modelBuilder.Entity<TPOceanData>()
                .Property(e => e.CreatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<TPOceanData>()
                .Property(e => e.UpdatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<TPOceanData>()
                .Property(e => e.MoveType)
                .IsUnicode(false);

            modelBuilder.Entity<TPOceanData>()
                .Property(e => e.RefNumber)
                .IsUnicode(false);

            modelBuilder.Entity<TPOceanData>()
                .Property(e => e.PreMaster)
                .IsUnicode(false);

            modelBuilder.Entity<TPOceanData>()
                .Property(e => e.ConfirmID)
                .IsUnicode(false);

            modelBuilder.Entity<TPOceanData>()
                .Property(e => e.ShptType)
                .IsUnicode(false);

            modelBuilder.Entity<TPOceanData>()
                .Property(e => e.HBLControlNo)
                .IsUnicode(false);

            modelBuilder.Entity<TPOceanData>()
                .Property(e => e.BookingNo)
                .IsUnicode(false);

            modelBuilder.Entity<TPOceanDataContainer>()
                .Property(e => e.StationID)
                .IsUnicode(false);

            modelBuilder.Entity<TPOceanDataContainer>()
                .Property(e => e.TPID)
                .IsUnicode(false);

            modelBuilder.Entity<TPOceanDataContainer>()
                .Property(e => e.ContainerType)
                .IsUnicode(false);

            modelBuilder.Entity<TPOceanDataContainer>()
                .Property(e => e.PCSUOM)
                .IsUnicode(false);

            modelBuilder.Entity<TPOceanDataContainer>()
                .Property(e => e.WTUOM)
                .IsUnicode(false);

            modelBuilder.Entity<TPOceanDataContainer>()
                .Property(e => e.Version)
                .IsFixedLength();

            modelBuilder.Entity<TPOceanDataContainer>()
                .Property(e => e.CreatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<TPOceanDataContainer>()
                .Property(e => e.UpdatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<TPOceanDataPO>()
                .Property(e => e.StationID)
                .IsUnicode(false);

            modelBuilder.Entity<TPOceanDataPO>()
                .Property(e => e.TPID)
                .IsUnicode(false);

            modelBuilder.Entity<TPOceanDataPO>()
                .Property(e => e.SourceStationID)
                .IsUnicode(false);

            modelBuilder.Entity<TPOceanDataPO>()
                .Property(e => e.PONo)
                .IsUnicode(false);

            modelBuilder.Entity<TPOceanDataPO>()
                .Property(e => e.SerialNo)
                .IsUnicode(false);

            modelBuilder.Entity<TPOceanDataPO>()
                .Property(e => e.Remarks)
                .IsUnicode(false);

            modelBuilder.Entity<TPOceanDataPO>()
                .Property(e => e.ItemNo)
                .IsUnicode(false);

            modelBuilder.Entity<TPOceanDataPO>()
                .Property(e => e.Description)
                .IsUnicode(false);

            modelBuilder.Entity<TPOceanDataPO>()
                .Property(e => e.Quantity)
                .IsUnicode(false);

            modelBuilder.Entity<TPOceanDataPO>()
                .Property(e => e.SONO)
                .IsUnicode(false);

            modelBuilder.Entity<TPOceanDataPO>()
                .Property(e => e.Curr)
                .IsUnicode(false);

            modelBuilder.Entity<TPOceanDataPO>()
                .Property(e => e.CreatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<TPOceanDataPO>()
                .Property(e => e.UpdatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<TPOceanDataPO>()
                .Property(e => e.UOM)
                .IsUnicode(false);

            modelBuilder.Entity<TPOceanPreview>()
                .Property(e => e.StationID)
                .IsUnicode(false);

            modelBuilder.Entity<TPOceanPreview>()
                .Property(e => e.HBLNo)
                .IsUnicode(false);

            modelBuilder.Entity<TPOceanPreview>()
                .Property(e => e.SONumber)
                .IsUnicode(false);

            modelBuilder.Entity<TPOceanPreview>()
                .Property(e => e.FMCNumber)
                .IsUnicode(false);

            modelBuilder.Entity<TPOceanPreview>()
                .Property(e => e.NoOfOrgBL)
                .IsUnicode(false);

            modelBuilder.Entity<TPOceanPreview>()
                .Property(e => e.DeclareValue)
                .IsUnicode(false);

            modelBuilder.Entity<TPOceanPreview>()
                .Property(e => e.CreatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<TPOceanPreview>()
                .Property(e => e.UpdatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<TPOceanPreview>()
                .Property(e => e.TelexRelease)
                .IsUnicode(false);

            modelBuilder.Entity<AEBarCode>()
                .Property(e => e.CreatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<AEBarCode>()
                .Property(e => e.UpdatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<AEBookConsoleDimension>()
                .Property(e => e.OrgStationID)
                .IsUnicode(false);

            modelBuilder.Entity<AEBookConsoleDimension>()
                .Property(e => e.UOM)
                .IsUnicode(false);

            modelBuilder.Entity<AEBookConsoleDimension>()
                .Property(e => e.Volume)
                .HasPrecision(18, 3);

            modelBuilder.Entity<AEBookConsoleDimension>()
                .Property(e => e.VWT)
                .HasPrecision(18, 1);

            modelBuilder.Entity<AEBookConsoleDimension>()
                .Property(e => e.CreatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<AEBookConsoleDimension>()
                .Property(e => e.UpdatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<AIAuthToMakeEntry>()
                .Property(e => e.CreatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<AIAuthToMakeEntry>()
                .Property(e => e.UpdatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<AIAuthToMakeEntry>()
                .Property(e => e.Version)
                .IsFixedLength();

            modelBuilder.Entity<AIAuthToMakeEntry>()
                .Property(e => e.ITDate)
                .IsUnicode(false);

            modelBuilder.Entity<OEAMSISFMailLog>()
                .Property(e => e.StationID)
                .IsUnicode(false);

            modelBuilder.Entity<OEAMSISFMailLog>()
                .Property(e => e.KeyValue)
                .IsUnicode(false);

            modelBuilder.Entity<OEAMSISFMailLog>()
                .Property(e => e.CreatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<OEAMSISFMailLog>()
                .Property(e => e.UpdatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<OEAMSISFMailLog>()
                .Property(e => e.MailType)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<OEAMSISFMailLog>()
                .Property(e => e.DType)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<OEAMSISFMailLog>()
                .Property(e => e.MType)
                .IsUnicode(false);

            modelBuilder.Entity<OEAMSISFMailLog>()
                .Property(e => e.MilestoneStatus)
                .IsUnicode(false);

            modelBuilder.Entity<OEAMSISFMailLog>()
                .Property(e => e.MailTo)
                .IsUnicode(false);

            modelBuilder.Entity<OEAMSISFMailLog>()
                .Property(e => e.SendStatus)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<OEHBLWSAssignRecord>()
                .Property(e => e.StationID)
                .IsUnicode(false);

            modelBuilder.Entity<OLAPTemp>()
                .Property(e => e.UserID)
                .IsUnicode(false);

            modelBuilder.Entity<OLAPTemp>()
                .Property(e => e.RepName)
                .IsUnicode(false);

            modelBuilder.Entity<OLAPTemp>()
                .Property(e => e.RepCategory)
                .IsUnicode(false);

            modelBuilder.Entity<OLAPTemp>()
                .Property(e => e.XMLData)
                .IsUnicode(false);

            modelBuilder.Entity<OLAPTemp>()
                .Property(e => e.CSXMLData)
                .IsUnicode(false);

            modelBuilder.Entity<OLAPTemp>()
                .Property(e => e.CSType)
                .IsUnicode(false);
        }
    }
}
