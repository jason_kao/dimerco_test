namespace ValuePlus.Domain.eChainVP
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("OEDR")]
    public partial class OEDR
    {
        public int ID { get; set; }

        public int? MBLID { get; set; }

        [StringLength(400)]
        public string MBLNo { get; set; }

        [StringLength(5)]
        public string StationID { get; set; }

        [StringLength(50)]
        public string DRNo { get; set; }

        [StringLength(50)]
        public string BLNo { get; set; }

        [StringLength(500)]
        public string SHPR { get; set; }

        [StringLength(500)]
        public string CNEE { get; set; }

        [StringLength(500)]
        public string NTFY { get; set; }

        [StringLength(50)]
        public string PreCarriage { get; set; }

        [StringLength(50)]
        public string PReceipt { get; set; }

        [StringLength(100)]
        public string Vessel { get; set; }

        [StringLength(100)]
        public string Voyage { get; set; }

        [StringLength(40)]
        public string PLoading { get; set; }

        [StringLength(40)]
        public string PDischarge { get; set; }

        [StringLength(40)]
        public string PDelivery { get; set; }

        [StringLength(100)]
        public string DEST { get; set; }

        [StringLength(50)]
        public string CNTRNo { get; set; }

        [StringLength(500)]
        public string SealNo { get; set; }

        [StringLength(50)]
        public string PCS { get; set; }

        [StringLength(600)]
        public string GoodsDesc { get; set; }

        [StringLength(50)]
        public string GWT { get; set; }

        [StringLength(50)]
        public string CBM { get; set; }

        [StringLength(250)]
        public string CNTRInWords { get; set; }

        [StringLength(50)]
        public string FRTCharge { get; set; }

        [StringLength(50)]
        public string RevenueTons { get; set; }

        [StringLength(10)]
        public string Rate { get; set; }

        [StringLength(50)]
        public string Per { get; set; }

        [StringLength(10)]
        public string PP { get; set; }

        [StringLength(10)]
        public string CC { get; set; }

        [StringLength(50)]
        public string ExRate { get; set; }

        [StringLength(10)]
        public string PPAt { get; set; }

        [StringLength(10)]
        public string CCAt { get; set; }

        [StringLength(250)]
        public string IssuePlace { get; set; }

        [StringLength(50)]
        public string TotalPP { get; set; }

        [StringLength(50)]
        public string RCY { get; set; }

        [StringLength(50)]
        public string RCFS { get; set; }

        [StringLength(50)]
        public string RDoor { get; set; }

        [StringLength(50)]
        public string DCY { get; set; }

        [StringLength(50)]
        public string DCFS { get; set; }

        [StringLength(50)]
        public string DDoor { get; set; }

        [StringLength(50)]
        public string TempRequired { get; set; }

        [StringLength(50)]
        public string TempF { get; set; }

        [StringLength(50)]
        public string TempC { get; set; }

        [StringLength(50)]
        public string Ordinary { get; set; }

        [StringLength(50)]
        public string Reefer { get; set; }

        [StringLength(50)]
        public string Dangerous { get; set; }

        [StringLength(50)]
        public string Auto { get; set; }

        [StringLength(50)]
        public string Liquid { get; set; }

        [StringLength(50)]
        public string LiveAnimal { get; set; }

        [StringLength(50)]
        public string Bulk { get; set; }

        [StringLength(250)]
        public string Other { get; set; }

        [StringLength(250)]
        public string OtherDesc { get; set; }

        [StringLength(50)]
        public string ChangeVessel { get; set; }

        [StringLength(50)]
        public string CanBatch { get; set; }

        [StringLength(50)]
        public string LoadingDate { get; set; }

        [StringLength(50)]
        public string EffectiveDate { get; set; }

        [StringLength(50)]
        public string Amount { get; set; }

        [StringLength(50)]
        public string PrintDate { get; set; }

        public DateTime? CreatedDT { get; set; }

        [StringLength(50)]
        public string CreatedBy { get; set; }

        public DateTime? UpdatedDT { get; set; }

        [StringLength(50)]
        public string UpdatedBy { get; set; }

        [StringLength(50)]
        public string SONo { get; set; }

        [StringLength(60)]
        public string ETD { get; set; }

        [StringLength(60)]
        public string OPName { get; set; }

        [StringLength(60)]
        public string OPTELNo { get; set; }
    }
}
