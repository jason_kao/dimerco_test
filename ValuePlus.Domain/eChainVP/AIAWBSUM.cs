namespace ValuePlus.Domain.eChainVP
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("AIAWBSUM")]
    public partial class AIAWBSUM
    {
        public double? FRT_ECOST { get; set; }

        public double? Extra_ECOST { get; set; }

        public double? Other_ECOST { get; set; }

        public double? FVAT_ECOST { get; set; }

        public double? EVAT_ECOST { get; set; }

        public double? OVAT_ECOST { get; set; }

        public double? FRT_Sales_PP { get; set; }

        public double? Extra_Sales_PP { get; set; }

        public double? Other_Sales_PP { get; set; }

        public double? FVAT_Sales_PP { get; set; }

        public double? EVAT_Sales_PP { get; set; }

        public double? OVAT_Sales_PP { get; set; }

        public double? FRT_Sales_CC { get; set; }

        public double? Extra_Sales_CC { get; set; }

        public double? Other_Sales_CC { get; set; }

        public double? FVAT_Sales_CC { get; set; }

        public double? EVAT_Sales_CC { get; set; }

        public double? OVAT_Sales_CC { get; set; }

        public double? Rpt_FRT_ECOST { get; set; }

        public double? Rpt_Extra_ECOST { get; set; }

        public double? Rpt_Other_ECOST { get; set; }

        public double? Rpt_FVAT_ECOST { get; set; }

        public double? Rpt_EVAT_ECOST { get; set; }

        public double? Rpt_OVAT_ECOST { get; set; }

        public double? Rpt_FRT_Sales_PP { get; set; }

        public double? Rpt_Extra_Sales_PP { get; set; }

        public double? Rpt_Other_Sales_PP { get; set; }

        public double? Rpt_FVAT_Sales_PP { get; set; }

        public double? Rpt_EVAT_Sales_PP { get; set; }

        public double? Rpt_OVAT_Sales_PP { get; set; }

        public double? Rpt_FRT_Sales_CC { get; set; }

        public double? Rpt_Extra_Sales_CC { get; set; }

        public double? Rpt_Other_Sales_CC { get; set; }

        public double? Rpt_FVAT_Sales_CC { get; set; }

        public double? Rpt_EVAT_Sales_CC { get; set; }

        public double? Rpt_OVAT_Sales_CC { get; set; }

        [StringLength(50)]
        public string ReportCurrency { get; set; }

        [StringLength(10)]
        public string LocalCurrency { get; set; }

        public int? NOHOUSE { get; set; }

        public int? NOPCS { get; set; }

        public double? NOCWT { get; set; }

        [Required]
        [StringLength(10)]
        public string STATIONID { get; set; }

        public int SourceID { get; set; }

        [Required]
        [StringLength(6)]
        public string IDType { get; set; }

        [Column(TypeName = "timestamp")]
        [MaxLength(8)]
        [Timestamp]
        public byte[] Version { get; set; }

        [Required]
        [StringLength(6)]
        public string CreateBy { get; set; }

        public DateTime CreatedTime { get; set; }

        [Required]
        [StringLength(6)]
        public string UpdatedBy { get; set; }

        public DateTime UpdatedTime { get; set; }

        public int ID { get; set; }

        public double? Extra_FRT_Sales_PP { get; set; }

        public double? Extra_Other_Sales_PP { get; set; }

        public double? Extra_FRT_Sales_CC { get; set; }

        public double? Extra_Other_Sales_CC { get; set; }

        public double? Rpt_Extra_FRT_Sales_PP { get; set; }

        public double? Rpt_Extra_Other_Sales_PP { get; set; }

        public double? Rpt_Extra_FRT_Sales_CC { get; set; }

        public double? Rpt_Extra_Other_Sales_CC { get; set; }

        public double? EFVAT_Sales_PP { get; set; }

        public double? EOVAT_Sales_PP { get; set; }

        public double? EFVAT_Sales_CC { get; set; }

        public double? EOVAT_Sales_CC { get; set; }

        public double? Rpt_EFVAT_Sales_PP { get; set; }

        public double? Rpt_EOVAT_Sales_PP { get; set; }

        public double? Rpt_EFVAT_Sales_CC { get; set; }

        public double? Rpt_EOVAT_Sales_CC { get; set; }

        public double? Extra_FRT_ECost { get; set; }

        public double? Extra_Other_ECost { get; set; }

        public double? EFVAT_ECOST { get; set; }

        public double? EOVAT_ECOST { get; set; }

        public double? Rpt_Extra_FRT_ECost { get; set; }

        public double? Rpt_Extra_Other_ECost { get; set; }

        public double? Rpt_EFVAT_ECOST { get; set; }

        public double? Rpt_EOVAT_ECOST { get; set; }

        public double? CUS_Sales_PP { get; set; }

        public double? CUS_VAT_Sales_PP { get; set; }

        public double? Extra_CUS_Sales_PP { get; set; }

        public double? Extra_CUS_VAT_Sales_PP { get; set; }

        public double? CUS_Sales_CC { get; set; }

        public double? CUS_VAT_Sales_CC { get; set; }

        public double? Extra_CUS_Sales_CC { get; set; }

        public double? Extra_CUS_VAT_Sales_CC { get; set; }

        public double? CUS_ECOST { get; set; }

        public double? CUS_VAT_ECOST { get; set; }

        public double? Extra_CUS_ECOST { get; set; }

        public double? Extra_CUS_VAT_ECOST { get; set; }

        public double? DNDim_Ecost { get; set; }

        public double? DNDIm_Cus_Ecost { get; set; }

        public double? Extra_DNDim_Ecost { get; set; }

        public double? Extra_DNDIm_Cus_Ecost { get; set; }

        public double? Rpt_CUS_Sales_PP { get; set; }

        public double? Rpt_CUS_VAT_Sales_PP { get; set; }

        public double? Rpt_Extra_CUS_Sales_PP { get; set; }

        public double? Rpt_Extra_CUS_VAT_Sales_PP { get; set; }

        public double? Rpt_CUS_Sales_CC { get; set; }

        public double? Rpt_CUS_VAT_Sales_CC { get; set; }

        public double? Rpt_Extra_CUS_Sales_CC { get; set; }

        public double? Rpt_Extra_CUS_VAT_Sales_CC { get; set; }

        public double? Rpt_CUS_ECOST { get; set; }

        public double? Rpt_CUS_VAT_ECOST { get; set; }

        public double? Rpt_Extra_CUS_ECOST { get; set; }

        public double? Rpt_Extra_CUS_VAT_ECOST { get; set; }

        public double? Rpt_DNDim_Ecost { get; set; }

        public double? Rpt_DNDIm_Cus_Ecost { get; set; }

        public double? Rpt_Extra_DNDim_Ecost { get; set; }

        public double? Rpt_Extra_DNDIm_Cus_Ecost { get; set; }

        public double? OverseaDN_Sales { get; set; }

        public double? Rpt_OverseaDN_Sales { get; set; }

        public double? DUTY_Sales { get; set; }

        public double? Rpt_DUTY_Sales { get; set; }
    }
}
