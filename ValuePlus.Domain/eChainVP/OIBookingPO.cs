namespace ValuePlus.Domain.eChainVP
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("OIBookingPO")]
    public partial class OIBookingPO
    {
        public int ID { get; set; }

        [StringLength(5)]
        public string StationID { get; set; }

        public int? BookingID { get; set; }

        [StringLength(5)]
        public string SrcStationID { get; set; }

        public int? SrcID { get; set; }

        public int? HBLID { get; set; }

        [StringLength(30)]
        public string PONo { get; set; }

        [StringLength(60)]
        public string ItemNo { get; set; }

        [StringLength(200)]
        public string Description { get; set; }

        [StringLength(50)]
        public string CreatedBy { get; set; }

        public DateTime? CreatedDT { get; set; }

        [StringLength(50)]
        public string UpdatedBy { get; set; }

        public DateTime? UpdatedDT { get; set; }

        [Column(TypeName = "timestamp")]
        [MaxLength(8)]
        [Timestamp]
        public byte[] Version { get; set; }

        [StringLength(100)]
        public string Quantity { get; set; }
    }
}
