namespace ValuePlus.Domain.eChainVP
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("AIHAWBPickup")]
    public partial class AIHAWBPickup
    {
        public int ID { get; set; }

        [StringLength(255)]
        public string Address { get; set; }

        public int? Truck { get; set; }

        public DateTime? ActualPickup { get; set; }

        [StringLength(255)]
        public string Remark { get; set; }

        public bool? IsPickup { get; set; }

        public int PickupFrom { get; set; }

        public DateTime? AvailPickup { get; set; }

        public int HAWBID { get; set; }

        [Required]
        [StringLength(10)]
        public string StationID { get; set; }

        [Required]
        [StringLength(6)]
        public string CreatedBy { get; set; }

        public DateTime CreatedDate { get; set; }

        [Required]
        [StringLength(6)]
        public string UpdatedBy { get; set; }

        public DateTime UpdatedDate { get; set; }

        [Column(TypeName = "timestamp")]
        [MaxLength(8)]
        [Timestamp]
        public byte[] Version { get; set; }

        [StringLength(50)]
        public string VehicleNo { get; set; }

        [StringLength(50)]
        public string Driver { get; set; }

        [StringLength(50)]
        public string Phone { get; set; }

        public bool? Show { get; set; }

        public int? DELVTo { get; set; }

        [StringLength(255)]
        public string DELVAddr { get; set; }

        public double? Weight { get; set; }

        [StringLength(10)]
        public string WeightUOM { get; set; }

        public DateTime? ActualPickupTo { get; set; }

        public DateTime? AvailPickupTo { get; set; }

        public double? CWT { get; set; }

        public int? PCS { get; set; }

        [StringLength(50)]
        public string PCSUOM { get; set; }

        [StringLength(10)]
        public string SendBy { get; set; }

        public DateTime? SendDate { get; set; }

        public int? ConsolePickupID { get; set; }

        public int? ConsoleCount { get; set; }

        [StringLength(4)]
        public string InvolvedBy { get; set; }
    }
}
