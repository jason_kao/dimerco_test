namespace ValuePlus.Domain.eChainVP
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("OECustomerEBookingPO")]
    public partial class OECustomerEBookingPO
    {
        public int ID { get; set; }

        [StringLength(5)]
        public string StationID { get; set; }

        public int? SrcID { get; set; }

        public int? BKID { get; set; }

        public int? HBLID { get; set; }

        [StringLength(30)]
        public string PONo { get; set; }

        [StringLength(50)]
        public string CreatedBy { get; set; }

        public DateTime? CreatedDT { get; set; }

        [StringLength(50)]
        public string UpdatedBy { get; set; }

        public DateTime? UpdatedDT { get; set; }
    }
}
