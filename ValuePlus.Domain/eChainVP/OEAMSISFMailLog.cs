namespace ValuePlus.Domain.eChainVP
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("OEAMSISFMailLog")]
    public partial class OEAMSISFMailLog
    {
        public int ID { get; set; }

        public int? MBLID { get; set; }

        public int? HBLID { get; set; }

        [StringLength(5)]
        public string StationID { get; set; }

        [StringLength(20)]
        public string KeyValue { get; set; }

        public int? CustomerHQID { get; set; }

        [StringLength(255)]
        public string CustomerName { get; set; }

        public DateTime? ETD { get; set; }

        [StringLength(50)]
        public string CreatedBy { get; set; }

        [StringLength(50)]
        public string UpdatedBy { get; set; }

        [StringLength(1)]
        public string MailType { get; set; }

        [StringLength(1)]
        public string DType { get; set; }

        [StringLength(10)]
        public string MType { get; set; }

        public int? MilestoneID { get; set; }

        public DateTime? MilestoneTime { get; set; }

        [StringLength(100)]
        public string MilestoneStatus { get; set; }

        [StringLength(1000)]
        public string MailTo { get; set; }

        [StringLength(1)]
        public string SendStatus { get; set; }

        public DateTime? SendDate { get; set; }

        public int? Version { get; set; }
    }
}
