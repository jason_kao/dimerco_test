namespace ValuePlus.Domain.eChainVP
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("OEFLadingSORet")]
    public partial class OEFLadingSORet
    {
        public int ID { get; set; }

        [StringLength(5)]
        public string StationID { get; set; }

        public int? SOID { get; set; }

        [StringLength(5)]
        public string SoFor { get; set; }

        public int? HBLID { get; set; }

        public int? MBLID { get; set; }

        [StringLength(60)]
        public string SONo { get; set; }

        public int? PCS { get; set; }

        [StringLength(10)]
        public string PCSUOM { get; set; }

        public decimal? Weight { get; set; }

        [StringLength(10)]
        public string WeightUOM { get; set; }

        public decimal? CBM { get; set; }

        [StringLength(10)]
        public string CBMUOM { get; set; }

        [StringLength(20)]
        public string CTNRType { get; set; }

        [StringLength(40)]
        public string CTNRNo { get; set; }

        [StringLength(40)]
        public string SealNo { get; set; }

        public DateTime? ATA { get; set; }

        [StringLength(1000)]
        public string Marks { get; set; }

        [StringLength(1000)]
        public string Description { get; set; }

        [StringLength(50)]
        public string CreatedBy { get; set; }

        public DateTime? CreatedDT { get; set; }

        [StringLength(50)]
        public string UpdatedBy { get; set; }

        public DateTime? UpdatedDT { get; set; }

        [Column(TypeName = "timestamp")]
        [MaxLength(8)]
        [Timestamp]
        public byte[] Version { get; set; }

        public int? SrcID { get; set; }

        [StringLength(5)]
        public string SrcStationID { get; set; }
    }
}
