namespace ValuePlus.Domain.eChainVP
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("AEHAWBPrintLog")]
    public partial class AEHAWBPrintLog
    {
        public int ID { get; set; }

        public int HAWBID { get; set; }

        [Required]
        [StringLength(10)]
        public string UpdatedBy { get; set; }

        public DateTime UpdatedDate { get; set; }

        [Required]
        [StringLength(10)]
        public string StationID { get; set; }

        [Required]
        [StringLength(10)]
        public string CreateBy { get; set; }

        public DateTime? CreatedDate { get; set; }

        [StringLength(30)]
        public string ReportName { get; set; }
    }
}
