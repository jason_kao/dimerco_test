namespace ValuePlus.Domain.eChainVP
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("OEMBL")]
    public partial class OEMBL
    {
        public int ID { get; set; }

        [StringLength(5)]
        public string StationID { get; set; }

        [StringLength(5)]
        public string MBLType { get; set; }

        [StringLength(20)]
        public string MBLNo { get; set; }

        [StringLength(20)]
        public string LotNo { get; set; }

        public int? HBLCount { get; set; }

        public int? PCS { get; set; }

        public int? Customer { get; set; }

        public int? SHPR { get; set; }

        public int? CNEE { get; set; }

        public int? CoLoader { get; set; }

        public int? NTFY { get; set; }

        public int? DELAgent { get; set; }

        public int? BookingParty { get; set; }

        public int? Carrier { get; set; }

        [StringLength(60)]
        public string OceanVessel { get; set; }

        [StringLength(60)]
        public string OceanVoyage { get; set; }

        [StringLength(60)]
        public string FeederVessel { get; set; }

        [StringLength(60)]
        public string FeederVoyage { get; set; }

        [StringLength(15)]
        public string ShptType { get; set; }

        [StringLength(15)]
        public string MoveType { get; set; }

        [StringLength(15)]
        public string FreightPayType { get; set; }

        public int? PReceipt { get; set; }

        public DateTime? PReceiptETD { get; set; }

        public DateTime? PReceiptATD { get; set; }

        public int? PLoading { get; set; }

        public DateTime? PLoadingETD { get; set; }

        public DateTime? PLoadingATD { get; set; }

        public int? PDischarge { get; set; }

        public DateTime? PDischargeETD { get; set; }

        public DateTime? PDischargeATD { get; set; }

        public int? PDelivery { get; set; }

        public DateTime? PDeliveryETD { get; set; }

        public DateTime? PDeliveryATD { get; set; }

        public int? FinalDEST { get; set; }

        public DateTime? FinalDESTETD { get; set; }

        public DateTime? FinalDESTATD { get; set; }

        [StringLength(1)]
        public string IsBookmark { get; set; }

        [StringLength(10)]
        public string Status { get; set; }

        [StringLength(50)]
        public string CreatedBy { get; set; }

        public DateTime? CreatedDT { get; set; }

        [StringLength(50)]
        public string UpdatedBy { get; set; }

        public DateTime? UpdatedDT { get; set; }

        [Column(TypeName = "timestamp")]
        [MaxLength(8)]
        [Timestamp]
        public byte[] Version { get; set; }

        [StringLength(50)]
        public string PreAlertBy { get; set; }

        public DateTime? PreAlertDT { get; set; }

        [StringLength(50)]
        public string eManifestBy { get; set; }

        public DateTime? eManifestDT { get; set; }

        public int? SrcID { get; set; }

        [StringLength(5)]
        public string SrcStationID { get; set; }

        [StringLength(20)]
        public string TradeType { get; set; }

        [StringLength(10)]
        public string FCalculateType { get; set; }

        [StringLength(30)]
        public string ServiceType { get; set; }

        [StringLength(250)]
        public string QuoteType { get; set; }

        [StringLength(20)]
        public string BookingNO { get; set; }

        public int? salesperson { get; set; }

        [StringLength(50)]
        public string NatureOfGoodsType { get; set; }

        [StringLength(10)]
        public string IMPStation { get; set; }

        public int? ThirdParty { get; set; }

        [StringLength(50)]
        public string OE3PManifestSendBy { get; set; }

        public DateTime? OE3PManifestSendDT { get; set; }

        public int? AlsoNTFY { get; set; }

        [StringLength(255)]
        public string Instruction { get; set; }

        [StringLength(20)]
        public string RefNumber { get; set; }

        [StringLength(15)]
        public string MTSNO { get; set; }

        [StringLength(50)]
        public string ColoaderMBLNo { get; set; }

        [StringLength(40)]
        public string CargoControlNo { get; set; }
    }
}
