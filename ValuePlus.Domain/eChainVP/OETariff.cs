namespace ValuePlus.Domain.eChainVP
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("OETariff")]
    public partial class OETariff
    {
        public int ID { get; set; }

        [StringLength(5)]
        public string StationID { get; set; }

        public int? HBLID { get; set; }

        [StringLength(50)]
        public string TariffNo { get; set; }

        [StringLength(10)]
        public string Currency { get; set; }

        public double? Amount { get; set; }

        public double? CollectAmount { get; set; }

        public double? PrepaidAmount { get; set; }

        [StringLength(50)]
        public string CreatedBy { get; set; }

        public DateTime? CreatedDT { get; set; }

        [StringLength(50)]
        public string UpdatedBy { get; set; }

        public DateTime? UpdatedDT { get; set; }

        [Column(TypeName = "timestamp")]
        [MaxLength(8)]
        [Timestamp]
        public byte[] Version { get; set; }
    }
}
