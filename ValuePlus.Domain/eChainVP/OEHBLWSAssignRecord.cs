namespace ValuePlus.Domain.eChainVP
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("OEHBLWSAssignRecord")]
    public partial class OEHBLWSAssignRecord
    {
        public int ID { get; set; }

        public int? HBLID { get; set; }

        [StringLength(20)]
        public string AssignTo1 { get; set; }

        [StringLength(20)]
        public string AssignTo2 { get; set; }

        [StringLength(20)]
        public string AssignTo3 { get; set; }

        [StringLength(50)]
        public string CreatedBy { get; set; }

        public DateTime? CreatedTime { get; set; }

        [StringLength(50)]
        public string UpdatedBy { get; set; }

        public DateTime? UpdatedTime { get; set; }

        [StringLength(5)]
        public string StationID { get; set; }
    }
}
