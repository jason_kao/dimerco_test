namespace ValuePlus.Domain.eChainVP
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("OLAPTemp")]
    public partial class OLAPTemp
    {
        [Key]
        [Column(Order = 0)]
        public int ReportID { get; set; }

        [Key]
        [Column(Order = 1)]
        [StringLength(15)]
        public string UserID { get; set; }

        [Key]
        [Column(Order = 2)]
        public DateTime BuildTime { get; set; }

        [StringLength(40)]
        public string RepName { get; set; }

        [StringLength(40)]
        public string RepCategory { get; set; }

        [Column(TypeName = "text")]
        public string XMLData { get; set; }

        [Column(TypeName = "text")]
        public string CSXMLData { get; set; }

        [StringLength(50)]
        public string CSType { get; set; }
    }
}
