namespace ValuePlus.Domain.eChainVP
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("OEHBLWHEntry")]
    public partial class OEHBLWHEntry
    {
        public int ID { get; set; }

        public int? HBLID { get; set; }

        [StringLength(5)]
        public string StationID { get; set; }

        [StringLength(50)]
        public string ClientName { get; set; }

        [StringLength(50)]
        public string ATTN { get; set; }

        [StringLength(50)]
        public string CustomsNo { get; set; }

        [StringLength(100)]
        public string CustomsName { get; set; }

        [StringLength(50)]
        public string DeliveryNo { get; set; }

        [StringLength(20)]
        public string PKGS { get; set; }

        [StringLength(20)]
        public string GW { get; set; }

        [StringLength(20)]
        public string VOL { get; set; }

        [StringLength(50)]
        public string CartonType { get; set; }

        [StringLength(255)]
        public string Marks { get; set; }

        [StringLength(20)]
        public string POL { get; set; }

        [StringLength(50)]
        public string ETD { get; set; }

        [StringLength(50)]
        public string DeliveryDate { get; set; }

        [StringLength(20)]
        public string DeliveryExtra { get; set; }

        [StringLength(50)]
        public string WarehouseName { get; set; }

        [StringLength(255)]
        public string WarehouseAddress { get; set; }

        [StringLength(50)]
        public string WHContact { get; set; }

        [StringLength(50)]
        public string WHContactTEL { get; set; }

        [StringLength(50)]
        public string CustomsDeliveryDate { get; set; }

        [StringLength(20)]
        public string CustomsDeliveryExtra { get; set; }

        [StringLength(255)]
        public string CompanyAddress { get; set; }

        [StringLength(50)]
        public string CompanyATTN { get; set; }

        [StringLength(50)]
        public string CompanyATTNTEL { get; set; }

        [StringLength(50)]
        public string CompanyATTNFAX { get; set; }

        [StringLength(50)]
        public string CompanyATTNEmail { get; set; }

        [StringLength(255)]
        public string ReMark { get; set; }

        [StringLength(255)]
        public string Extra1 { get; set; }

        [StringLength(255)]
        public string Extra2 { get; set; }

        [StringLength(255)]
        public string Extra3 { get; set; }

        public DateTime? CreatedDT { get; set; }

        [StringLength(50)]
        public string CreatedBy { get; set; }

        public DateTime? UpdatedDT { get; set; }

        [StringLength(50)]
        public string UpdatedBy { get; set; }
    }
}
