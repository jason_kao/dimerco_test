namespace ValuePlus.Domain.eChainVP
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("TPOceanData")]
    public partial class TPOceanData
    {
        public int ID { get; set; }

        [Required]
        [StringLength(3)]
        public string StationID { get; set; }

        [Required]
        [StringLength(15)]
        public string TPID { get; set; }

        public int CustomerID { get; set; }

        [StringLength(3)]
        public string SourceStationID { get; set; }

        [StringLength(1)]
        public string SourceType { get; set; }

        public int? SourceID { get; set; }

        [Required]
        [StringLength(30)]
        public string MasterNo { get; set; }

        [Required]
        [StringLength(30)]
        public string HouseNo { get; set; }

        [StringLength(30)]
        public string Lot { get; set; }

        public int Shipper { get; set; }

        public int Cnee { get; set; }

        public int Receipt { get; set; }

        public int Delivery { get; set; }

        public int POL { get; set; }

        public int POD { get; set; }

        [StringLength(30)]
        public string NetureOfGood { get; set; }

        [Required]
        [StringLength(3)]
        public string HType { get; set; }

        public double? CBM { get; set; }

        public int? SalesPerson { get; set; }

        public DateTime? ATD { get; set; }

        public DateTime? ATA { get; set; }

        [Required]
        [StringLength(1)]
        public string LotStatus { get; set; }

        [Required]
        [StringLength(1)]
        public string IsManiFast { get; set; }

        [Column(TypeName = "timestamp")]
        [MaxLength(8)]
        [Timestamp]
        public byte[] Version { get; set; }

        [StringLength(6)]
        public string CreatedBy { get; set; }

        public DateTime? CreatedDate { get; set; }

        [Required]
        [StringLength(6)]
        public string UpdatedBy { get; set; }

        public DateTime UpdatedDate { get; set; }

        public DateTime? IssueDate { get; set; }

        [StringLength(100)]
        public string Vessel { get; set; }

        [StringLength(100)]
        public string Voyage { get; set; }

        [StringLength(20)]
        public string MoveType { get; set; }

        [StringLength(255)]
        public string Marks { get; set; }

        [StringLength(255)]
        public string Description { get; set; }

        [StringLength(255)]
        public string Container { get; set; }

        public int? PCS { get; set; }

        [StringLength(30)]
        public string PCSUOM { get; set; }

        public decimal? Weight { get; set; }

        [StringLength(30)]
        public string WUOM { get; set; }

        [StringLength(20)]
        public string RefNumber { get; set; }

        [StringLength(30)]
        public string PreMaster { get; set; }

        [StringLength(50)]
        public string ConfirmID { get; set; }

        [StringLength(10)]
        public string ShptType { get; set; }

        [StringLength(10)]
        public string HBLControlNo { get; set; }

        public DateTime? PickupDateTime { get; set; }

        public DateTime? PReceiptETD { get; set; }

        public DateTime? PLoadingETD { get; set; }

        public DateTime? PDischargeETD { get; set; }

        public DateTime? PDeliveryETD { get; set; }

        public DateTime? FinalDestETD { get; set; }

        public DateTime? PLoadingATD { get; set; }

        public int? Carrier { get; set; }

        [StringLength(20)]
        public string BookingNo { get; set; }
    }
}
