namespace ValuePlus.Domain.eChainVP
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class OEHBLWS
    {
        public int ID { get; set; }

        public int? HBLID { get; set; }

        [StringLength(20)]
        public string BKNo { get; set; }

        [StringLength(20)]
        public string HBLNo { get; set; }

        [StringLength(20)]
        public string LotNo { get; set; }

        [StringLength(20)]
        public string MBLNo { get; set; }

        [StringLength(200)]
        public string PONo { get; set; }

        public int? CustomerCode { get; set; }

        [StringLength(500)]
        public string ReMark { get; set; }

        [StringLength(100)]
        public string Title { get; set; }

        [StringLength(50)]
        public string QuotationNo { get; set; }

        [StringLength(50)]
        public string ClientName { get; set; }

        [StringLength(50)]
        public string ClientTel { get; set; }

        [StringLength(50)]
        public string ClientFax { get; set; }

        [StringLength(50)]
        public string ClientMB { get; set; }

        [StringLength(50)]
        public string ClientPIC { get; set; }

        [StringLength(50)]
        public string DeliveryNo { get; set; }

        [StringLength(20)]
        public string PKGS { get; set; }

        [StringLength(20)]
        public string GW { get; set; }

        [StringLength(20)]
        public string VOL { get; set; }

        [StringLength(20)]
        public string CartonType { get; set; }

        [StringLength(20)]
        public string POL { get; set; }

        [StringLength(50)]
        public string DEST { get; set; }

        public DateTime? ETD { get; set; }

        public DateTime? ETA { get; set; }

        [StringLength(10)]
        public string Carrier { get; set; }

        [StringLength(10)]
        public string BKAgent { get; set; }

        [StringLength(10)]
        public string DESTAgent { get; set; }

        [StringLength(20)]
        public string ReferenceNo { get; set; }

        [StringLength(100)]
        public string VSLVOY { get; set; }

        [StringLength(10)]
        public string OPType1 { get; set; }

        [StringLength(10)]
        public string OPType2 { get; set; }

        [StringLength(50)]
        public string OPTypeDetail { get; set; }

        [StringLength(2)]
        public string FreightPayableMBL { get; set; }

        [StringLength(2)]
        public string FreightPayableHBL { get; set; }

        [StringLength(50)]
        public string ThirdPartyPlace { get; set; }

        [StringLength(50)]
        public string Other { get; set; }

        [StringLength(50)]
        public string BLRelease { get; set; }

        [StringLength(2)]
        public string ROType { get; set; }

        [StringLength(50)]
        public string ChargeFRT { get; set; }

        [StringLength(50)]
        public string ChargeFRT0 { get; set; }

        [StringLength(50)]
        public string ChargeFRT1 { get; set; }

        [StringLength(50)]
        public string ChargeFRT2 { get; set; }

        [StringLength(50)]
        public string ChargeBAFEBS { get; set; }

        [StringLength(50)]
        public string ChargeBAFEBS0 { get; set; }

        [StringLength(50)]
        public string ChargeBAFEBS1 { get; set; }

        [StringLength(50)]
        public string ChargeBAFEBS2 { get; set; }

        [StringLength(50)]
        public string ChargeSelect1 { get; set; }

        [StringLength(50)]
        public string ChargeSelect2 { get; set; }

        [StringLength(50)]
        public string ChargeCIC { get; set; }

        [StringLength(50)]
        public string ChargeCIC0 { get; set; }

        [StringLength(50)]
        public string ChargeCIC1 { get; set; }

        [StringLength(50)]
        public string ChargeCIC2 { get; set; }

        [StringLength(50)]
        public string ChargeAMSACI { get; set; }

        [StringLength(50)]
        public string ChargeAMSACI0 { get; set; }

        [StringLength(50)]
        public string ChargeAMSACI1 { get; set; }

        [StringLength(50)]
        public string ChargeAMSACI2 { get; set; }

        [StringLength(50)]
        public string ChargeLOC { get; set; }

        [StringLength(50)]
        public string ChargeLOC0 { get; set; }

        [StringLength(50)]
        public string ChargeLOC1 { get; set; }

        [StringLength(50)]
        public string ChargeLOC2 { get; set; }

        [StringLength(50)]
        public string ChargeBKF { get; set; }

        [StringLength(50)]
        public string ChargeBKF0 { get; set; }

        [StringLength(50)]
        public string ChargeBKF1 { get; set; }

        [StringLength(50)]
        public string ChargeBKF2 { get; set; }

        [StringLength(50)]
        public string ChargeTHC { get; set; }

        [StringLength(50)]
        public string ChargeTHC0 { get; set; }

        [StringLength(50)]
        public string ChargeTHC1 { get; set; }

        [StringLength(50)]
        public string ChargeTHC2 { get; set; }

        [StringLength(50)]
        public string ChargeCUS { get; set; }

        [StringLength(50)]
        public string ChargeCUS0 { get; set; }

        [StringLength(50)]
        public string ChargeCUS1 { get; set; }

        [StringLength(50)]
        public string ChargeCUS2 { get; set; }

        [StringLength(50)]
        public string ChargeINS { get; set; }

        [StringLength(50)]
        public string ChargeINS0 { get; set; }

        [StringLength(50)]
        public string ChargeINS1 { get; set; }

        [StringLength(50)]
        public string ChargeINS2 { get; set; }

        [StringLength(50)]
        public string ChargeCMI { get; set; }

        [StringLength(50)]
        public string ChargeCMI0 { get; set; }

        [StringLength(50)]
        public string ChargeCMI1 { get; set; }

        [StringLength(50)]
        public string ChargeCMI2 { get; set; }

        [StringLength(50)]
        public string ChargeCTG { get; set; }

        [StringLength(50)]
        public string ChargeCTG0 { get; set; }

        [StringLength(50)]
        public string ChargeCTG1 { get; set; }

        [StringLength(50)]
        public string ChargeCTG2 { get; set; }

        [StringLength(50)]
        public string ChargeUUF { get; set; }

        [StringLength(50)]
        public string ChargeUUF0 { get; set; }

        [StringLength(50)]
        public string ChargeUUF1 { get; set; }

        [StringLength(50)]
        public string ChargeUUF2 { get; set; }

        [StringLength(50)]
        public string ChargeFUM { get; set; }

        [StringLength(50)]
        public string ChargeFUM0 { get; set; }

        [StringLength(50)]
        public string ChargeFUM1 { get; set; }

        [StringLength(50)]
        public string ChargeFUM2 { get; set; }

        [StringLength(50)]
        public string ChargeDOC { get; set; }

        [StringLength(50)]
        public string ChargeDOC0 { get; set; }

        [StringLength(50)]
        public string ChargeDOC1 { get; set; }

        [StringLength(50)]
        public string ChargeDOC2 { get; set; }

        [StringLength(50)]
        public string ChargeCOU { get; set; }

        [StringLength(50)]
        public string ChargeCOU0 { get; set; }

        [StringLength(50)]
        public string ChargeCOU1 { get; set; }

        [StringLength(50)]
        public string ChargeCOU2 { get; set; }

        [StringLength(50)]
        public string ChargeDDUDDP { get; set; }

        [StringLength(50)]
        public string ChargeDDUDDP0 { get; set; }

        [StringLength(50)]
        public string ChargeDDUDDP1 { get; set; }

        [StringLength(50)]
        public string ChargeDDUDDP2 { get; set; }

        [StringLength(50)]
        public string ChargeRefund { get; set; }

        [StringLength(50)]
        public string ChargeRefund0 { get; set; }

        [StringLength(50)]
        public string ChargeRefund1 { get; set; }

        [StringLength(50)]
        public string ChargeRefund2 { get; set; }

        [StringLength(50)]
        public string ChargeBINS { get; set; }

        [StringLength(50)]
        public string ChargeBINS0 { get; set; }

        [StringLength(50)]
        public string ChargeBINS1 { get; set; }

        [StringLength(50)]
        public string ChargeBINS2 { get; set; }

        [StringLength(50)]
        public string ChargeISF { get; set; }

        [StringLength(50)]
        public string ChargeISF0 { get; set; }

        [StringLength(50)]
        public string ChargeISF1 { get; set; }

        [StringLength(50)]
        public string ChargeISF2 { get; set; }

        [StringLength(50)]
        public string ChargeHC { get; set; }

        [StringLength(50)]
        public string ChargeHC0 { get; set; }

        [StringLength(50)]
        public string ChargeHC1 { get; set; }

        [StringLength(50)]
        public string ChargeHC2 { get; set; }

        [StringLength(50)]
        public string ChargeAdd1 { get; set; }

        [StringLength(50)]
        public string ChargeAdd10 { get; set; }

        [StringLength(50)]
        public string ChargeAdd11 { get; set; }

        [StringLength(50)]
        public string ChargeAdd12 { get; set; }

        [StringLength(50)]
        public string ChargeAdd13 { get; set; }

        [StringLength(50)]
        public string ChargeAdd2 { get; set; }

        [StringLength(50)]
        public string ChargeAdd20 { get; set; }

        [StringLength(50)]
        public string ChargeAdd21 { get; set; }

        [StringLength(50)]
        public string ChargeAdd22 { get; set; }

        [StringLength(50)]
        public string ChargeAdd23 { get; set; }

        [StringLength(50)]
        public string ChargeAdd3 { get; set; }

        [StringLength(50)]
        public string ChargeAdd30 { get; set; }

        [StringLength(50)]
        public string ChargeAdd31 { get; set; }

        [StringLength(50)]
        public string ChargeAdd32 { get; set; }

        [StringLength(50)]
        public string ChargeAdd33 { get; set; }

        [StringLength(50)]
        public string ChargeAddName1 { get; set; }

        [StringLength(50)]
        public string ChargeAddName11 { get; set; }

        [StringLength(50)]
        public string ChargeAddName2 { get; set; }

        [StringLength(50)]
        public string ChargeAddName21 { get; set; }

        [StringLength(50)]
        public string ChargeAddName3 { get; set; }

        [StringLength(50)]
        public string ChargeAddName31 { get; set; }

        [StringLength(50)]
        public string LocalCHGPS { get; set; }

        [StringLength(50)]
        public string LocalCHGPS0 { get; set; }

        [StringLength(50)]
        public string LocalCHGPS1 { get; set; }

        [StringLength(50)]
        public string LocalCHGPS2 { get; set; }

        [StringLength(50)]
        public string OFPS { get; set; }

        [StringLength(50)]
        public string OFPS0 { get; set; }

        [StringLength(50)]
        public string OFCRTOOverSales { get; set; }

        [StringLength(50)]
        public string OFCRTOOverSales0 { get; set; }

        [StringLength(50)]
        public string PS { get; set; }

        [StringLength(50)]
        public string HC { get; set; }

        [StringLength(50)]
        public string DebitTo { get; set; }

        [StringLength(50)]
        public string CreditTo { get; set; }

        [StringLength(50)]
        public string OP { get; set; }

        [StringLength(50)]
        public string Sales { get; set; }

        [StringLength(50)]
        public string ApproveBy { get; set; }

        [StringLength(50)]
        public string GLDBy { get; set; }

        [StringLength(50)]
        public string GP { get; set; }

        [StringLength(10)]
        public string CreatedBy { get; set; }

        public DateTime? CreatedTime { get; set; }

        [StringLength(10)]
        public string UpdatedBy { get; set; }

        public DateTime? UpdatedTime { get; set; }

        [Column(TypeName = "ntext")]
        public string PrintContent { get; set; }

        public int? SHPRHQID { get; set; }

        [StringLength(50)]
        public string ClientEmail { get; set; }

        [StringLength(100)]
        public string ClientAddress { get; set; }

        [StringLength(10)]
        public string Coloader { get; set; }

        [StringLength(10)]
        public string OPType3 { get; set; }

        [StringLength(50)]
        public string OPType3Detail { get; set; }

        [StringLength(50)]
        public string OPType3Remark { get; set; }

        [StringLength(5)]
        public string StationID { get; set; }

        [StringLength(5)]
        public string SrcStationID { get; set; }

        public int? SrcID { get; set; }
    }
}
