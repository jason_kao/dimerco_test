namespace ValuePlus.Domain.eChainVP
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("AIHAWBAgentInfo")]
    public partial class AIHAWBAgentInfo
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ID { get; set; }

        [StringLength(50)]
        public string StationID { get; set; }

        public int? AgentID { get; set; }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int HAWBID { get; set; }

        public int? ShipperID { get; set; }

        public int? ShipperMapID { get; set; }

        public int? CNEEID { get; set; }

        public int? CNEEMapID { get; set; }

        public int? NTFYID { get; set; }

        public int? NTFYMapID { get; set; }

        [StringLength(50)]
        public string CreatedBy { get; set; }

        public DateTime CreatedDate { get; set; }

        [StringLength(50)]
        public string UpdatedBy { get; set; }

        public DateTime? UpdatedDate { get; set; }
    }
}
