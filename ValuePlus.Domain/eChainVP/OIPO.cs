namespace ValuePlus.Domain.eChainVP
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("OIPO")]
    public partial class OIPO
    {
        public int ID { get; set; }

        [StringLength(5)]
        public string StationID { get; set; }

        [StringLength(5)]
        public string SrcStationID { get; set; }

        public int? SrcID { get; set; }

        public int? HBLID { get; set; }

        [StringLength(30)]
        public string PONo { get; set; }

        [StringLength(60)]
        public string SerialNo { get; set; }

        [StringLength(200)]
        public string Remarks { get; set; }

        [StringLength(15)]
        public string POParty { get; set; }

        [StringLength(50)]
        public string CreatedBy { get; set; }

        public DateTime? CreatedDT { get; set; }

        [StringLength(50)]
        public string UpdatedBy { get; set; }

        public DateTime? UpdatedDT { get; set; }

        [Column(TypeName = "timestamp")]
        [MaxLength(8)]
        [Timestamp]
        public byte[] Version { get; set; }

        [StringLength(60)]
        public string ItemNo { get; set; }

        [StringLength(200)]
        public string Description { get; set; }

        [StringLength(100)]
        public string Quantity { get; set; }

        [StringLength(30)]
        public string SONO { get; set; }

        [StringLength(20)]
        public string InvoiceNo { get; set; }

        [StringLength(4)]
        public string Curr { get; set; }

        [StringLength(50)]
        public string FinalDestination { get; set; }

        public DateTime? PODate { get; set; }

        public DateTime? InvoiceDate { get; set; }

        public decimal? Amount { get; set; }

        public int? SCTNS { get; set; }

        [StringLength(50)]
        public string POExtra1 { get; set; }

        [StringLength(50)]
        public string POExtra2 { get; set; }

        [StringLength(50)]
        public string POExtra3 { get; set; }

        [StringLength(50)]
        public string POExtra4 { get; set; }

        [StringLength(50)]
        public string POExtra5 { get; set; }

        [StringLength(50)]
        public string POExtra6 { get; set; }

        public int? POMSID { get; set; }

        [StringLength(50)]
        public string UOM { get; set; }
    }
}
