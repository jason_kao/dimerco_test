namespace ValuePlus.Domain.eChainVP
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("AEBookConsoleDimension")]
    public partial class AEBookConsoleDimension
    {
        public int ID { get; set; }

        [StringLength(50)]
        public string OrgStationID { get; set; }

        public int? OrgHAWBID { get; set; }

        public int? Length { get; set; }

        public int? Width { get; set; }

        public int? Height { get; set; }

        [StringLength(50)]
        public string UOM { get; set; }

        public int? PCS { get; set; }

        public decimal? Volume { get; set; }

        public decimal? VWT { get; set; }

        [StringLength(50)]
        public string CreatedBy { get; set; }

        public DateTime? CreatedDate { get; set; }

        [StringLength(50)]
        public string UpdatedBy { get; set; }

        public DateTime? UpdatedDate { get; set; }
    }
}
