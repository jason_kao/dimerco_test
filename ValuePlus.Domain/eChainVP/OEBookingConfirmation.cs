namespace ValuePlus.Domain.eChainVP
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("OEBookingConfirmation")]
    public partial class OEBookingConfirmation
    {
        public int ID { get; set; }

        [StringLength(50)]
        public string HBLID { get; set; }

        [StringLength(5)]
        public string Mode { get; set; }

        [StringLength(5)]
        public string StationID { get; set; }

        [StringLength(200)]
        public string StationName { get; set; }

        [StringLength(200)]
        public string StationAddress { get; set; }

        [StringLength(100)]
        public string StationTelFax { get; set; }

        [StringLength(200)]
        public string Attention { get; set; }

        [StringLength(200)]
        public string FeederVessel { get; set; }

        [StringLength(200)]
        public string FeederVoyage { get; set; }

        [StringLength(200)]
        public string OceanVessel { get; set; }

        [StringLength(200)]
        public string OceanVoyage { get; set; }

        [StringLength(200)]
        public string BookingNo { get; set; }

        [StringLength(200)]
        public string ReferenceNo { get; set; }

        [StringLength(200)]
        public string ShipCallNo { get; set; }

        [StringLength(200)]
        public string PlaceOfDel { get; set; }

        [StringLength(200)]
        public string PortOfLoading { get; set; }

        [StringLength(200)]
        public string ETD { get; set; }

        [StringLength(200)]
        public string PortOfDisCharge { get; set; }

        [StringLength(200)]
        public string ETA { get; set; }

        [StringLength(500)]
        public string ContainerNo { get; set; }

        [StringLength(500)]
        public string NoOfPkags { get; set; }

        [StringLength(500)]
        public string Goods { get; set; }

        [StringLength(500)]
        public string GWT { get; set; }

        [StringLength(500)]
        public string Measurement { get; set; }

        [StringLength(200)]
        public string EqPULocation { get; set; }

        [StringLength(200)]
        public string EqReturnLocation { get; set; }

        [StringLength(200)]
        public string LoadingLocation { get; set; }

        [StringLength(200)]
        public string CloseTime { get; set; }

        [StringLength(200)]
        public string TerminalLocation { get; set; }

        [StringLength(500)]
        public string Remarks { get; set; }

        [StringLength(200)]
        public string FaxedBy { get; set; }

        [StringLength(200)]
        public string FaxedDT { get; set; }

        [StringLength(50)]
        public string PreparedBy { get; set; }

        [StringLength(50)]
        public string PreparedDT { get; set; }

        [StringLength(50)]
        public string CreatedBy { get; set; }

        public DateTime? CreatedDT { get; set; }

        [StringLength(50)]
        public string UpdatedBy { get; set; }

        public DateTime? UpdatedDT { get; set; }

        [StringLength(15)]
        public string OEType { get; set; }
    }
}
