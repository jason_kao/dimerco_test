namespace ValuePlus.Domain.eChainVP
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("OIArrivalNotice")]
    public partial class OIArrivalNotice
    {
        public int ID { get; set; }

        [StringLength(50)]
        public string HBLID { get; set; }

        [StringLength(5)]
        public string StationID { get; set; }

        [StringLength(200)]
        public string StationName { get; set; }

        [StringLength(255)]
        public string Address { get; set; }

        [StringLength(50)]
        public string Tel { get; set; }

        [StringLength(50)]
        public string Fax { get; set; }

        [StringLength(100)]
        public string Shipper { get; set; }

        [StringLength(50)]
        public string MBL { get; set; }

        [StringLength(50)]
        public string AMSBL { get; set; }

        [StringLength(250)]
        public string Cosignee { get; set; }

        [StringLength(50)]
        public string HBL { get; set; }

        [StringLength(50)]
        public string SubHBL { get; set; }

        [StringLength(100)]
        public string Vessel { get; set; }

        [StringLength(100)]
        public string Voyage { get; set; }

        [StringLength(40)]
        public string PLoading { get; set; }

        [StringLength(50)]
        public string ETD { get; set; }

        [StringLength(40)]
        public string PDischarge { get; set; }

        [StringLength(50)]
        public string ETA { get; set; }

        [StringLength(40)]
        public string PDelivery { get; set; }

        [StringLength(50)]
        public string DestinationETA { get; set; }

        [StringLength(250)]
        public string FreightLocation { get; set; }

        [StringLength(50)]
        public string ITNumber { get; set; }

        [StringLength(50)]
        public string ITDate { get; set; }

        [StringLength(50)]
        public string ITPort { get; set; }

        [StringLength(500)]
        public string ContainerNos { get; set; }

        [StringLength(300)]
        public string Pkgs { get; set; }

        [StringLength(500)]
        public string Goods { get; set; }

        [StringLength(300)]
        public string GrossWeight { get; set; }

        [StringLength(500)]
        public string Measurement { get; set; }

        [StringLength(200)]
        public string Charge { get; set; }

        [StringLength(200)]
        public string Collect { get; set; }

        [StringLength(50)]
        public string Total { get; set; }

        [StringLength(500)]
        public string StandardRemark { get; set; }

        [StringLength(500)]
        public string InputRemark { get; set; }

        [StringLength(50)]
        public string PreparedBy { get; set; }

        [StringLength(50)]
        public string PreparedDate { get; set; }

        [StringLength(50)]
        public string ReferenceNo { get; set; }

        [StringLength(50)]
        public string CreatedBy { get; set; }

        public DateTime? CreatedDT { get; set; }

        [StringLength(50)]
        public string UpdatedBy { get; set; }

        public DateTime? UpdatedDT { get; set; }

        [StringLength(300)]
        public string NotifyParty { get; set; }

        [StringLength(50)]
        public string TelexRelease { get; set; }

        [StringLength(250)]
        public string Broker { get; set; }

        [StringLength(250)]
        public string FinalLocation { get; set; }
    }
}
