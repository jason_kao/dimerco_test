namespace ValuePlus.Domain.eChainVP
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("OEAMSContainer")]
    public partial class OEAMSContainer
    {
        public int ID { get; set; }

        public int? HBLID { get; set; }

        [StringLength(50)]
        public string CNTRNo { get; set; }

        [StringLength(20)]
        public string SealNumber1 { get; set; }

        [StringLength(20)]
        public string SealNumber2 { get; set; }

        [StringLength(200)]
        public string PCS { get; set; }

        [StringLength(10)]
        public string PCSUOM { get; set; }

        [StringLength(200)]
        public string NetWeight { get; set; }

        [StringLength(10)]
        public string NetWeightUOM { get; set; }

        [StringLength(5)]
        public string LoadOrEmpty { get; set; }

        [StringLength(10)]
        public string TypeOfService { get; set; }

        [StringLength(20)]
        public string CountryOfOrigin { get; set; }

        [StringLength(1000)]
        public string Description { get; set; }

        [StringLength(1000)]
        public string Marks { get; set; }

        [StringLength(200)]
        public string CommodityCode { get; set; }

        [StringLength(200)]
        public string CashValue { get; set; }

        [StringLength(50)]
        public string HazardCode { get; set; }

        [StringLength(20)]
        public string HazardCodeQualifier { get; set; }

        [StringLength(20)]
        public string CTNRType { get; set; }
    }
}
