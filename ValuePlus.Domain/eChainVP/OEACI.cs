namespace ValuePlus.Domain.eChainVP
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("OEACI")]
    public partial class OEACI
    {
        public int ID { get; set; }

        public int? HBLID { get; set; }

        [StringLength(20)]
        public string SupplementaryRefNo { get; set; }

        [StringLength(20)]
        public string CargoControlNo { get; set; }

        [StringLength(20)]
        public string PrimaryBL { get; set; }

        [StringLength(10)]
        public string Type { get; set; }

        [StringLength(50)]
        public string PortName { get; set; }

        [StringLength(10)]
        public string Status { get; set; }

        public decimal? Volume { get; set; }

        [StringLength(10)]
        public string VolumeUOM { get; set; }

        public DateTime? CreatedDT { get; set; }

        [StringLength(50)]
        public string CreatedBy { get; set; }

        public DateTime? UpdatedDT { get; set; }

        [StringLength(50)]
        public string UpdatedBy { get; set; }

        [Column(TypeName = "timestamp")]
        [MaxLength(8)]
        [Timestamp]
        public byte[] Version { get; set; }
    }
}
