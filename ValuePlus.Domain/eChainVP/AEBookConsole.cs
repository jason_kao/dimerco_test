namespace ValuePlus.Domain.eChainVP
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("AEBookConsole")]
    public partial class AEBookConsole
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ID { get; set; }

        [Required]
        [StringLength(10)]
        public string StationID { get; set; }

        [Key]
        [Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int OrgHAWBID { get; set; }

        [Key]
        [Column(Order = 1)]
        [StringLength(10)]
        public string OrgStationID { get; set; }

        public DateTime? CargoAvailDate { get; set; }

        [StringLength(10)]
        public string Area { get; set; }

        [StringLength(50)]
        public string ServiceLevel { get; set; }

        [StringLength(10)]
        public string DEPT { get; set; }

        [StringLength(10)]
        public string DSTN { get; set; }

        public int? BookingPCS { get; set; }

        [StringLength(10)]
        public string BookingPCSUOM { get; set; }

        public decimal? BookingWeight { get; set; }

        public decimal? BookingCWT { get; set; }

        public decimal? BookingVolume { get; set; }

        [StringLength(10)]
        public string ShipperID { get; set; }

        [StringLength(100)]
        public string ShipperLocalName { get; set; }

        [StringLength(100)]
        public string Commodity { get; set; }

        [StringLength(50)]
        public string DocType { get; set; }

        [StringLength(50)]
        public string Carrier { get; set; }

        [StringLength(50)]
        public string HAWBNo { get; set; }

        [StringLength(50)]
        public string CS { get; set; }

        [StringLength(50)]
        public string Rate { get; set; }

        [StringLength(50)]
        public string Sales { get; set; }

        [StringLength(200)]
        public string ShipingRemark { get; set; }

        [StringLength(50)]
        public string BookingNo { get; set; }

        [StringLength(50)]
        public string HeXiaoDanNo { get; set; }

        [StringLength(50)]
        public string TuiDanNo { get; set; }

        [StringLength(10)]
        public string CreatedBy { get; set; }

        public DateTime? CreatedDate { get; set; }

        [StringLength(10)]
        public string UpdatedBy { get; set; }

        public DateTime? UpdatedDate { get; set; }

        [StringLength(50)]
        public string LoadMasterLoader { get; set; }

        [StringLength(50)]
        public string LoadMAWBNo { get; set; }

        [StringLength(50)]
        public string LoadCarrier { get; set; }

        [StringLength(50)]
        public string LoadFLTNo { get; set; }

        public DateTime? LoadETD { get; set; }

        public DateTime? LoadETA { get; set; }

        public DateTime? LoadATD { get; set; }

        public int? LoadPCS { get; set; }

        [StringLength(50)]
        public string LoadPCSType { get; set; }

        public decimal? LoadWeight { get; set; }

        [StringLength(50)]
        public string LoadWeightType { get; set; }

        public decimal? LoadVolume { get; set; }

        [StringLength(50)]
        public string LoadVolumeType { get; set; }

        public decimal? LoadVWT { get; set; }

        [StringLength(10)]
        public string LoadTerminal { get; set; }

        [StringLength(50)]
        public string LoadPallet { get; set; }

        [StringLength(10)]
        public string LoadStatus { get; set; }

        [StringLength(50)]
        public string LoadEstCost { get; set; }

        [StringLength(200)]
        public string LoadRemark { get; set; }

        [StringLength(50)]
        public string LoadCreatedBy { get; set; }

        public DateTime? LoadCreatedDate { get; set; }

        [StringLength(50)]
        public string LoadUpdatedBy { get; set; }

        public DateTime? LoadUpdatedDate { get; set; }

        public int? WHPCS { get; set; }

        [StringLength(50)]
        public string WHPCSType { get; set; }

        public decimal? WHWeight { get; set; }

        [StringLength(50)]
        public string WHWeightType { get; set; }

        public decimal? WHVolume { get; set; }

        [StringLength(50)]
        public string WHVolumeType { get; set; }

        [StringLength(50)]
        public string WHCargoStatus { get; set; }

        [StringLength(200)]
        public string WHRemark { get; set; }

        [StringLength(50)]
        public string WHLocation { get; set; }

        public DateTime? WHPulloutDT { get; set; }

        [StringLength(50)]
        public string WHReceiptNo { get; set; }

        [StringLength(50)]
        public string WHCreatedBy { get; set; }

        public DateTime? WHCreatedDate { get; set; }

        [StringLength(50)]
        public string WHUpdatedBy { get; set; }

        public DateTime? WHUpdatedDate { get; set; }

        public DateTime? WHHandoverDate { get; set; }

        public DateTime? WHArrivalDate { get; set; }

        [StringLength(50)]
        public string WHPallet { get; set; }

        [StringLength(200)]
        public string WHDimension { get; set; }

        public int? CSDeclareParty { get; set; }

        public int? CSHandledBy { get; set; }

        public DateTime? CSDecDate { get; set; }

        public DateTime? CSReleaseDate { get; set; }

        [StringLength(50)]
        public string CSStatus { get; set; }

        [StringLength(200)]
        public string CSRemark { get; set; }

        public DateTime? CSAvailDate { get; set; }

        [StringLength(50)]
        public string CSCreatedBy { get; set; }

        public DateTime? CSCreatedDate { get; set; }

        [StringLength(50)]
        public string CSUpdatedBy { get; set; }

        public DateTime? CSUpdatedDate { get; set; }

        [StringLength(50)]
        public string RTHandBookNo { get; set; }

        public DateTime? RTHandBookDate { get; set; }

        public DateTime? RTHandBookOverDate { get; set; }

        [StringLength(50)]
        public string RTHandBookSignBy { get; set; }

        [StringLength(50)]
        public string RTHandBookCourier { get; set; }

        [StringLength(50)]
        public string RTHexiaoDanNo { get; set; }

        public DateTime? RTHexiaoDanDate { get; set; }

        public DateTime? RTHexiaoDanOverDate { get; set; }

        [StringLength(50)]
        public string RTHexiaoDanSignBy { get; set; }

        [StringLength(50)]
        public string RTHexiaoDanCourier { get; set; }

        [StringLength(50)]
        public string RTTuiShuiNo { get; set; }

        public DateTime? RTTuiShuiDate { get; set; }

        public DateTime? RTTuiShuiOverDate { get; set; }

        [StringLength(50)]
        public string RTTuiShuiSignBy { get; set; }

        [StringLength(50)]
        public string RTTuiShuiCourier { get; set; }

        [StringLength(200)]
        public string RTRemark { get; set; }

        [StringLength(50)]
        public string RTCreatedBy { get; set; }

        public DateTime? RTCreatedDate { get; set; }

        [StringLength(50)]
        public string RTUpdatedBy { get; set; }

        public DateTime? RTUpdatedDate { get; set; }

        [StringLength(200)]
        public string Remark { get; set; }

        [StringLength(5)]
        public string Status { get; set; }

        public DateTime? ETD { get; set; }

        [StringLength(50)]
        public string SalesPerson { get; set; }

        public DateTime? WHCargoDate { get; set; }

        [StringLength(50)]
        public string WHServiceLevel { get; set; }

        [StringLength(5)]
        public string AWBType { get; set; }

        [StringLength(10)]
        public string BookSend { get; set; }

        [StringLength(10)]
        public string BookingWTUOM { get; set; }

        [StringLength(10)]
        public string BookingVolumeUOM { get; set; }

        [StringLength(50)]
        public string LoadMasterLoadName { get; set; }

        public int? LoadID { get; set; }

        public int? LoadPlanID { get; set; }

        [StringLength(50)]
        public string AgentNo { get; set; }

        [StringLength(50)]
        public string RTStatus { get; set; }

        [StringLength(400)]
        public string HubRemark { get; set; }

        public decimal? WHCWT { get; set; }
    }
}
