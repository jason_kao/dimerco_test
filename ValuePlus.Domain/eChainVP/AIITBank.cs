namespace ValuePlus.Domain.eChainVP
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("AIITBank")]
    public partial class AIITBank
    {
        public int ID { get; set; }

        [Required]
        [StringLength(255)]
        public string ITNo { get; set; }

        [StringLength(12)]
        public string MAWBNo { get; set; }

        public int BorrowIn { get; set; }

        public DateTime? ReceivedDate { get; set; }

        public DateTime? BorrowOutDate { get; set; }

        [StringLength(5)]
        public string BorrowOutBy { get; set; }

        public int? BorrowOutAgent { get; set; }

        public DateTime? AssignedDate { get; set; }

        public int City { get; set; }

        public DateTime? UsedDate { get; set; }

        [StringLength(5)]
        public string UsedBy { get; set; }

        [StringLength(8)]
        public string LotNo { get; set; }

        [StringLength(10)]
        public string Status { get; set; }

        public int? Issuer { get; set; }

        public int Mode { get; set; }

        [Required]
        [StringLength(10)]
        public string StationID { get; set; }

        [Required]
        [StringLength(6)]
        public string CreatedBy { get; set; }

        public DateTime CreatedDate { get; set; }

        [Required]
        [StringLength(6)]
        public string UpdatedBy { get; set; }

        public DateTime UpdatedDate { get; set; }

        [Column(TypeName = "timestamp")]
        [MaxLength(8)]
        [Timestamp]
        public byte[] Version { get; set; }
    }
}
