namespace ValuePlus.Domain.eChainVP
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("OIHBLAgentInfoMap")]
    public partial class OIHBLAgentInfoMap
    {
        public int ID { get; set; }

        [StringLength(50)]
        public string StationID { get; set; }

        public int AgentID { get; set; }

        public int? MapCustomerID { get; set; }

        [StringLength(50)]
        public string CompanyCode { get; set; }

        [StringLength(80)]
        public string CompanyName { get; set; }

        [Required]
        [StringLength(200)]
        public string CompanyAddress { get; set; }

        [StringLength(50)]
        public string CreatedBy { get; set; }

        public DateTime? CreatedDate { get; set; }

        [StringLength(50)]
        public string UpdatedBy { get; set; }

        public DateTime? UpdatedDate { get; set; }
    }
}
