namespace ValuePlus.Domain.eChainVP
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("OIBookingRemark")]
    public partial class OIBookingRemark
    {
        public int ID { get; set; }

        public int? HBLID { get; set; }

        [StringLength(5)]
        public string StationID { get; set; }

        [StringLength(5)]
        public string SrcStationID { get; set; }

        public int? SrcID { get; set; }

        [StringLength(255)]
        public string BookingRemark { get; set; }

        [StringLength(50)]
        public string CreatedBy { get; set; }

        public DateTime? CreatedDT { get; set; }

        [StringLength(50)]
        public string UpdatedBy { get; set; }

        public DateTime? UpdatedDT { get; set; }
    }
}
