namespace ValuePlus.Domain.eChainVP
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("AEReConfirmLog")]
    public partial class AEReConfirmLog
    {
        public int ID { get; set; }

        [StringLength(50)]
        public string StationID { get; set; }

        public int HAWBID { get; set; }

        [StringLength(200)]
        public string OldCode { get; set; }

        [StringLength(200)]
        public string NewCode { get; set; }

        [StringLength(50)]
        public string CreatedBy { get; set; }

        public DateTime? CreatedDate { get; set; }

        [StringLength(50)]
        public string UpdatedBy { get; set; }

        public DateTime? UpdatedDate { get; set; }
    }
}
