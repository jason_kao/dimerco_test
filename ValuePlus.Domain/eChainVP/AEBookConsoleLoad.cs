namespace ValuePlus.Domain.eChainVP
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("AEBookConsoleLoad")]
    public partial class AEBookConsoleLoad
    {
        public int ID { get; set; }

        [StringLength(50)]
        public string LoadMasterLoader { get; set; }

        [StringLength(50)]
        public string LoadMAWBNo { get; set; }

        [StringLength(50)]
        public string MAWBNoExtra { get; set; }

        [StringLength(50)]
        public string LoadCarrier { get; set; }

        public int? LoadDSTN { get; set; }

        [StringLength(10)]
        public string LoadArea { get; set; }

        public DateTime? LoadDate { get; set; }

        [StringLength(50)]
        public string LoadFLTNo { get; set; }

        public DateTime? LoadETD { get; set; }

        public DateTime? LoadETA { get; set; }

        public DateTime? LoadATD { get; set; }

        public DateTime? LoadATA { get; set; }

        public int? LoadPCS { get; set; }

        [StringLength(50)]
        public string LoadPCSType { get; set; }

        public decimal? LoadWeight { get; set; }

        [StringLength(50)]
        public string LoadWeightType { get; set; }

        public decimal? LoadVolume { get; set; }

        [StringLength(50)]
        public string LoadVolumeType { get; set; }

        public decimal? LoadVWT { get; set; }

        [StringLength(10)]
        public string LoadTerminal { get; set; }

        [StringLength(50)]
        public string LoadPallet { get; set; }

        [StringLength(10)]
        public string LoadStatus { get; set; }

        [StringLength(50)]
        public string LoadEstCost { get; set; }

        [StringLength(200)]
        public string LoadRemark { get; set; }

        [StringLength(50)]
        public string CreatedBy { get; set; }

        public DateTime? CreatedDate { get; set; }

        [StringLength(50)]
        public string UpdatedBy { get; set; }

        public DateTime? UpdatedDate { get; set; }

        [StringLength(200)]
        public string Remark { get; set; }

        [StringLength(50)]
        public string LoadMasterLoadName { get; set; }

        public int? DEPT1 { get; set; }

        public int? DSTN1 { get; set; }

        [StringLength(50)]
        public string FLTNo1 { get; set; }

        public DateTime? ETD1 { get; set; }

        public DateTime? ETA1 { get; set; }

        public DateTime? ATD1 { get; set; }

        public DateTime? ATA1 { get; set; }

        public int? DEPT2 { get; set; }

        public int? DSTN2 { get; set; }

        [StringLength(50)]
        public string FLTNo2 { get; set; }

        public DateTime? ETD2 { get; set; }

        public DateTime? ETA2 { get; set; }

        public DateTime? ATD2 { get; set; }

        public DateTime? ATA2 { get; set; }

        [StringLength(10)]
        public string Status { get; set; }
    }
}
