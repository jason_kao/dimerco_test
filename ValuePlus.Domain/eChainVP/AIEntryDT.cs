namespace ValuePlus.Domain.eChainVP
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("AIEntryDT")]
    public partial class AIEntryDT
    {
        public int ID { get; set; }

        public int HAWBID { get; set; }

        public int EntryID { get; set; }

        public int? EntryPCS { get; set; }

        public double? EntryWT { get; set; }

        [Required]
        [StringLength(10)]
        public string StationID { get; set; }

        [Required]
        [StringLength(6)]
        public string CreatedBy { get; set; }

        public DateTime CreatedDate { get; set; }

        [Required]
        [StringLength(6)]
        public string UpdatedBy { get; set; }

        public DateTime UpdatedDate { get; set; }

        [Column(TypeName = "timestamp")]
        [MaxLength(8)]
        [Timestamp]
        public byte[] Version { get; set; }

        public int? Damage { get; set; }

        public int? Offload { get; set; }

        public int? Shortage { get; set; }

        public int? Supplus { get; set; }

        public double? DamageWT { get; set; }

        public double? ShortageWT { get; set; }

        public double? SupplusWT { get; set; }

        [StringLength(255)]
        public string Remark { get; set; }
    }
}
