namespace ValuePlus.Domain.eSAM
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("SAMergeCustList")]
    public partial class SAMergeCustList
    {
        public int ID { get; set; }

        public int? ToCustomerID { get; set; }

        public int? DisposeCustID { get; set; }

        public DateTime? CreatedDate { get; set; }

        [StringLength(10)]
        public string Status { get; set; }

        [StringLength(10)]
        public string CreatedUserID { get; set; }

        public DateTime? UpdatedDate { get; set; }

        [StringLength(10)]
        public string UpdatedUserID { get; set; }

        [StringLength(100)]
        public string Remark { get; set; }
    }
}
