namespace ValuePlus.Domain.eSAM
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("SAQuoteContact")]
    public partial class SAQuoteContact
    {
        public int ID { get; set; }

        [StringLength(10)]
        public string UserID { get; set; }

        [StringLength(10)]
        public string UserStation { get; set; }

        public string CallingCard { get; set; }

        [StringLength(10)]
        public string ContactOrder { get; set; }

        [StringLength(10)]
        public string sType { get; set; }

        public int? QuoteID { get; set; }

        [StringLength(15)]
        public string QuoteNo { get; set; }

        public virtual SAQuotes SAQuotes { get; set; }
    }
}
