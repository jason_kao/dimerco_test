namespace ValuePlus.Domain.eSAM
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("SAQueryStack")]
    public partial class SAQueryStack
    {
        public int ID { get; set; }

        [StringLength(10)]
        public string UserID { get; set; }

        public int? TotalTimes { get; set; }

        [StringLength(255)]
        public string QueryStr { get; set; }

        [StringLength(20)]
        public string Views { get; set; }

        [StringLength(20)]
        public string Types { get; set; }
    }
}
