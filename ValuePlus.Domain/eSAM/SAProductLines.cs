namespace ValuePlus.Domain.eSAM
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class SAProductLines
    {
        public int ID { get; set; }

        public int LID { get; set; }

        public int PID { get; set; }

        [StringLength(10)]
        public string Status { get; set; }

        [StringLength(3)]
        public string CreatedStationID { get; set; }

        [StringLength(10)]
        public string CreatedUserID { get; set; }

        [StringLength(3)]
        public string OwnerStationID { get; set; }

        [StringLength(10)]
        public string OwnerUserID { get; set; }

        [StringLength(10)]
        public string UpdatedUserID { get; set; }

        public DateTime? UpdatedDate { get; set; }

        public DateTime? CreatedDate { get; set; }

        [Required]
        [StringLength(1)]
        public string Secured { get; set; }

        [StringLength(6)]
        public string FLeadOwner { get; set; }

        public DateTime? FSecuredDT { get; set; }

        [StringLength(1)]
        public string KeyAccount { get; set; }
    }
}
