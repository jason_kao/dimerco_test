namespace ValuePlus.Domain.eSAM
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class SAQuoteToolSurcharges
    {
        public int ID { get; set; }

        public int? QuoteID { get; set; }

        public int? FreightID { get; set; }

        [StringLength(20)]
        public string MIN { get; set; }

        [StringLength(20)]
        public string CBM { get; set; }

        [StringLength(20)]
        public string TON { get; set; }

        [StringLength(20)]
        public string SetFiling { get; set; }

        [StringLength(20)]
        public string SellingRate1 { get; set; }

        [StringLength(20)]
        public string SellingRate2 { get; set; }

        [StringLength(20)]
        public string SellingRate3 { get; set; }

        [StringLength(20)]
        public string SellingRate4 { get; set; }

        [StringLength(20)]
        public string SellingRate5 { get; set; }

        [StringLength(20)]
        public string SellingRate6 { get; set; }

        [StringLength(20)]
        public string SellingRate7 { get; set; }

        [StringLength(20)]
        public string SellingRate8 { get; set; }

        [StringLength(20)]
        public string SellingRate9 { get; set; }

        [StringLength(20)]
        public string SellingRate10 { get; set; }

        [StringLength(10)]
        public string CreatedUserID { get; set; }

        public DateTime? CreatedDate { get; set; }

        [StringLength(10)]
        public string UpdatedUserID { get; set; }

        public DateTime? UpdatedDate { get; set; }

        [StringLength(20)]
        public string sType { get; set; }

        [StringLength(200)]
        public string Remarks { get; set; }

        [StringLength(500)]
        public string LaneSegment { get; set; }

        [StringLength(255)]
        public string ChargeCode { get; set; }

        [StringLength(50)]
        public string CurrencyCode { get; set; }

        [StringLength(255)]
        public string SubChargeCode { get; set; }

        public int? ChargeCodeID { get; set; }

        public int? SubChargeCodeID { get; set; }

        public virtual SAQuotes SAQuotes { get; set; }
    }
}
