namespace ValuePlus.Domain.eSAM
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class SAQuoteTerms
    {
        public int ID { get; set; }

        [StringLength(255)]
        public string Title { get; set; }

        public string Contents { get; set; }

        [StringLength(10)]
        public string IsSelected { get; set; }

        [StringLength(20)]
        public string QuoteNo { get; set; }

        public string Attachments { get; set; }

        public int? QuoteID { get; set; }

        public int? TermOrder { get; set; }

        public virtual SAQuotes SAQuotes { get; set; }
    }
}
