namespace ValuePlus.Domain.eSAM
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("SASCDefinition")]
    public partial class SASCDefinition
    {
        [Key]
        public int HQID { get; set; }

        public int? PartyLvl { get; set; }

        [StringLength(20)]
        public string PartyType { get; set; }

        [StringLength(20)]
        public string PartyCode { get; set; }

        [StringLength(20)]
        public string PartyID { get; set; }

        public int? DefinitionTypeID { get; set; }

        public int? Value { get; set; }

        [StringLength(50)]
        public string ValueUnit { get; set; }

        [StringLength(10)]
        public string Status { get; set; }

        public DateTime? CreatedDate { get; set; }

        [StringLength(10)]
        public string CreatedBy { get; set; }

        public DateTime? UpdatedDate { get; set; }

        [StringLength(10)]
        public string UpdatedBy { get; set; }

        public virtual SASCCode SASCCode { get; set; }
    }
}
