namespace ValuePlus.Domain.eSAM
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class SAQuotes
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public SAQuotes()
        {
            SAQuoteChargeCity = new HashSet<SAQuoteChargeCity>();
            SAQuoteContact = new HashSet<SAQuoteContact>();
            SAQuoteFreights = new HashSet<SAQuoteFreights>();
            SAQuotePickupDelivery = new HashSet<SAQuotePickupDelivery>();
            SAQuoteSection = new HashSet<SAQuoteSection>();
            SAQuoteTerms = new HashSet<SAQuoteTerms>();
            SAQuoteToolFreights = new HashSet<SAQuoteToolFreights>();
            SAQuoteToolLocalChargeCity = new HashSet<SAQuoteToolLocalChargeCity>();
            SAQuoteToolSurcharges = new HashSet<SAQuoteToolSurcharges>();
            SATermOfShipping = new HashSet<SATermOfShipping>();
        }

        public int ID { get; set; }

        public int? LID { get; set; }

        public int? PID { get; set; }

        [StringLength(15)]
        public string QuoteNo { get; set; }

        [StringLength(10)]
        public string Status { get; set; }

        [StringLength(50)]
        public string AttentionTo { get; set; }

        public DateTime? EffectiveDate { get; set; }

        public DateTime? ExpiredDate { get; set; }

        public int? TradeTermID { get; set; }

        public int? CreditTermID { get; set; }

        [StringLength(20)]
        public string CreditLimitAmount { get; set; }

        public int? CreditLimitCurrID { get; set; }

        public string Remark { get; set; }

        [StringLength(10)]
        public string BookMark { get; set; }

        [StringLength(100)]
        public string FRHeaderBody { get; set; }

        [StringLength(10)]
        public string ApprovalInfo { get; set; }

        [StringLength(10)]
        public string AcceptInfo { get; set; }

        [StringLength(10)]
        public string CreatedUserID { get; set; }

        public DateTime? CreatedDate { get; set; }

        [StringLength(10)]
        public string UpdatedUserID { get; set; }

        public DateTime? UpdatedDate { get; set; }

        [StringLength(50)]
        public string ApprovalBy { get; set; }

        public DateTime? ApprovalDate { get; set; }

        [StringLength(50)]
        public string AcceptBy { get; set; }

        public DateTime? AcceptDate { get; set; }

        [StringLength(10)]
        public string sType { get; set; }

        [StringLength(10)]
        public string OnePWD { get; set; }

        [StringLength(10)]
        public string Title1 { get; set; }

        [StringLength(10)]
        public string Title2 { get; set; }

        [StringLength(10)]
        public string Title3 { get; set; }

        [StringLength(10)]
        public string Title4 { get; set; }

        [StringLength(10)]
        public string Title5 { get; set; }

        [StringLength(10)]
        public string Title6 { get; set; }

        [StringLength(10)]
        public string Title7 { get; set; }

        [StringLength(10)]
        public string Title8 { get; set; }

        [StringLength(10)]
        public string Title9 { get; set; }

        [StringLength(10)]
        public string Title10 { get; set; }

        [StringLength(50)]
        public string BKDescription { get; set; }

        public bool? IsGenerate { get; set; }

        [StringLength(255)]
        public string ProjectExplanation { get; set; }

        public bool? IsMNC { get; set; }

        [StringLength(10)]
        public string PrevailRate { get; set; }

        [StringLength(10)]
        public string PrevailRateFlag { get; set; }

        [StringLength(20)]
        public string DTSTradeMode { get; set; }

        public int? VendorID { get; set; }

        public int? ProjectID { get; set; }

        [StringLength(5)]
        public string StationID { get; set; }

        public int SF { get; set; }

        public int PayableDay { get; set; }

        [Required]
        [StringLength(1)]
        public string NRA { get; set; }

        public bool? IsAccessor { get; set; }

        [StringLength(10)]
        public string MoveType { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<SAQuoteChargeCity> SAQuoteChargeCity { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<SAQuoteContact> SAQuoteContact { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<SAQuoteFreights> SAQuoteFreights { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<SAQuotePickupDelivery> SAQuotePickupDelivery { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<SAQuoteSection> SAQuoteSection { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<SAQuoteTerms> SAQuoteTerms { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<SAQuoteToolFreights> SAQuoteToolFreights { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<SAQuoteToolLocalChargeCity> SAQuoteToolLocalChargeCity { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<SAQuoteToolSurcharges> SAQuoteToolSurcharges { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<SATermOfShipping> SATermOfShipping { get; set; }
    }
}
