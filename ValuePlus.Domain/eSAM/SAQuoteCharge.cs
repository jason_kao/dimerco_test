namespace ValuePlus.Domain.eSAM
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("SAQuoteCharge")]
    public partial class SAQuoteCharge
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public SAQuoteCharge()
        {
            SAQuoteChargeCondition = new HashSet<SAQuoteChargeCondition>();
            SAQuoteChargeItem = new HashSet<SAQuoteChargeItem>();
        }

        public int ID { get; set; }

        public int? QuoteCityID { get; set; }

        [StringLength(10)]
        public string OrderNo { get; set; }

        [StringLength(50)]
        public string ChargeCodeDesc { get; set; }

        public int? ChargeCodeID { get; set; }

        [StringLength(255)]
        public string Remark { get; set; }

        [StringLength(20)]
        public string CreatedUserID { get; set; }

        public DateTime? CreatedDate { get; set; }

        [StringLength(20)]
        public string UpdatedUserID { get; set; }

        public DateTime? UpdatedDate { get; set; }

        [StringLength(20)]
        public string MinCost { get; set; }

        [StringLength(20)]
        public string MinProfit { get; set; }

        [StringLength(20)]
        public string MinSelling { get; set; }

        [StringLength(20)]
        public string FlatCost { get; set; }

        [StringLength(20)]
        public string FlatProfit { get; set; }

        [StringLength(20)]
        public string FlatSelling { get; set; }

        public int? MinUOM { get; set; }

        public int? FlatUOM { get; set; }

        [StringLength(10)]
        public string FLType { get; set; }

        [StringLength(10)]
        public string PPCC { get; set; }

        [StringLength(1000)]
        public string Remark2 { get; set; }

        [StringLength(50)]
        public string Method { get; set; }

        public int? CurrencyID { get; set; }

        public int? GroupID { get; set; }

        [StringLength(1000)]
        public string SalesRemark { get; set; }

        public virtual SAQuoteChargeCity SAQuoteChargeCity { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<SAQuoteChargeCondition> SAQuoteChargeCondition { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<SAQuoteChargeItem> SAQuoteChargeItem { get; set; }
    }
}
