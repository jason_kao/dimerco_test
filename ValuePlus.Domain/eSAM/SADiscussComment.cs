namespace ValuePlus.Domain.eSAM
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("SADiscussComment")]
    public partial class SADiscussComment
    {
        public int ID { get; set; }

        [StringLength(20)]
        public string Status { get; set; }

        public string Comments { get; set; }

        [StringLength(10)]
        public string ReplyUserID { get; set; }

        public DateTime? CommentTime { get; set; }

        [StringLength(20)]
        public string SourceType { get; set; }

        public int? SourceID { get; set; }

        [Column(TypeName = "ntext")]
        public string Attachments { get; set; }
    }
}
