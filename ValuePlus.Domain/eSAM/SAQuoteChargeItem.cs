namespace ValuePlus.Domain.eSAM
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("SAQuoteChargeItem")]
    public partial class SAQuoteChargeItem
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public SAQuoteChargeItem()
        {
            SAQuoteFreightHistoricalRate = new HashSet<SAQuoteFreightHistoricalRate>();
        }

        public int ID { get; set; }

        public int? ChargeID { get; set; }

        [StringLength(20)]
        public string Unit { get; set; }

        [StringLength(20)]
        public string UOMID { get; set; }

        [StringLength(20)]
        public string Cost { get; set; }

        [StringLength(20)]
        public string Profit { get; set; }

        [StringLength(20)]
        public string Selling { get; set; }

        [StringLength(10)]
        public string CreatedUserID { get; set; }

        public DateTime? CreatedDate { get; set; }

        [StringLength(10)]
        public string UpdatedUserID { get; set; }

        public DateTime? UpdatedDate { get; set; }

        [StringLength(10)]
        public string FLItem { get; set; }

        [StringLength(50)]
        public string PPCC { get; set; }

        [StringLength(150)]
        public string Remark { get; set; }

        public int? ConditionID { get; set; }

        public virtual SAQuoteCharge SAQuoteCharge { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<SAQuoteFreightHistoricalRate> SAQuoteFreightHistoricalRate { get; set; }
    }
}
