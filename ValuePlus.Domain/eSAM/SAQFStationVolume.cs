namespace ValuePlus.Domain.eSAM
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("SAQFStationVolume")]
    public partial class SAQFStationVolume
    {
        public int ID { get; set; }

        public int MasterID { get; set; }

        public int ExportID { get; set; }

        [Required]
        [StringLength(3)]
        public string StationID { get; set; }

        public int Volume { get; set; }

        [Required]
        [StringLength(6)]
        public string CreatedBy { get; set; }

        public DateTime? CreatedDate { get; set; }

        [StringLength(6)]
        public string UpdatedBy { get; set; }

        public DateTime? UpdatedDate { get; set; }
    }
}
