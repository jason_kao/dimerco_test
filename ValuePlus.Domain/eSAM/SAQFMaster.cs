namespace ValuePlus.Domain.eSAM
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("SAQFMaster")]
    public partial class SAQFMaster
    {
        public int ID { get; set; }

        public int? CarrierID { get; set; }

        public DateTime? StartingOfPeriod { get; set; }

        public DateTime? EndOfPeriod { get; set; }

        public int? MQC { get; set; }

        [StringLength(10)]
        public string Unit { get; set; }

        [Required]
        [StringLength(6)]
        public string CreatedBy { get; set; }

        public DateTime? CreatedDate { get; set; }

        [StringLength(6)]
        public string UpdatedBy { get; set; }

        public DateTime? UpdatedDate { get; set; }
    }
}
