namespace ValuePlus.Domain.eSAM
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("SAAutoSendMail")]
    public partial class SAAutoSendMail
    {
        public int ID { get; set; }

        public int? SalesTaskID { get; set; }

        public DateTime? CreatedDate { get; set; }
    }
}
