namespace ValuePlus.Domain.eSAM
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("SAMergeCheck")]
    public partial class SAMergeCheck
    {
        public int ID { get; set; }

        [StringLength(20)]
        public string StationIP { get; set; }

        [StringLength(20)]
        public string DBName { get; set; }

        public int? MergeTableID { get; set; }

        [StringLength(1)]
        public string IsCheck { get; set; }

        public DateTime? CreatedDate { get; set; }
    }
}
