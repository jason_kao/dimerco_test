namespace ValuePlus.Domain.eSAM
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("SAQuoteFreightHistoricalRate")]
    public partial class SAQuoteFreightHistoricalRate
    {
        [Key]
        public int HQID { get; set; }

        public int? LID { get; set; }

        public int? OriginCityID { get; set; }

        public int? DestinationCityID { get; set; }

        public int? PloadingPortID { get; set; }

        public int? PDischargePortID { get; set; }

        public int? CarrierID { get; set; }

        public int? ChargeItemID { get; set; }

        public decimal? MinSellingRate { get; set; }

        public decimal? MaxSellingRate { get; set; }

        public decimal? LastYearSellingRate { get; set; }

        public decimal? LastTwoYearSellingRate { get; set; }

        public decimal? LastMonthSellingRate { get; set; }

        public decimal? LastTwoMonthSellingRate { get; set; }

        [StringLength(1)]
        public string isAllQuote { get; set; }

        public DateTime? CreatedDate { get; set; }

        public virtual SAQuoteChargeItem SAQuoteChargeItem { get; set; }
    }
}
