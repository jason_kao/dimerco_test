namespace ValuePlus.Domain.eSAM
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("SMStation")]
    public partial class SMStation
    {
        public int ID { get; set; }

        public int? StationID { get; set; }

        [StringLength(20)]
        public string StationCode { get; set; }

        [StringLength(100)]
        public string Description { get; set; }
    }
}
