namespace ValuePlus.Domain.eSAM
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("SMCustomer")]
    public partial class SMCustomer
    {
        public int ID { get; set; }

        [Required]
        [StringLength(3)]
        public string StationID { get; set; }

        public int CustomerID { get; set; }

        [Required]
        [StringLength(10)]
        public string CustomerCode { get; set; }

        [StringLength(255)]
        public string CustomerName { get; set; }

        public int? CityID { get; set; }

        public int? IndustryID { get; set; }

        [StringLength(10)]
        public string GlobalCode { get; set; }

        [StringLength(255)]
        public string CustomerName1 { get; set; }

        [StringLength(255)]
        public string CustomerAddress1 { get; set; }

        [StringLength(255)]
        public string CustomerAddress2 { get; set; }

        [StringLength(255)]
        public string CustomerAddress3 { get; set; }

        [StringLength(255)]
        public string CustomerAddress4 { get; set; }

        [StringLength(255)]
        public string CustomerAddress5 { get; set; }

        [StringLength(50)]
        public string Phone { get; set; }

        [StringLength(10)]
        public string PhoneExt { get; set; }

        [StringLength(50)]
        public string Fax { get; set; }

        [StringLength(10)]
        public string FaxExt { get; set; }

        [StringLength(10)]
        public string Zip { get; set; }

        [StringLength(255)]
        public string WebSite { get; set; }

        [StringLength(10)]
        public string TradeTerm { get; set; }

        [StringLength(10)]
        public string ShipmentType { get; set; }

        [StringLength(10)]
        public string ServiceType { get; set; }

        [StringLength(10)]
        public string AirMove { get; set; }

        [StringLength(10)]
        public string OceanMove { get; set; }

        [StringLength(10)]
        public string AirLineCode { get; set; }

        [StringLength(10)]
        public string TPLetterCode { get; set; }

        [StringLength(255)]
        public string Marks1 { get; set; }

        [StringLength(255)]
        public string Marks2 { get; set; }

        [StringLength(255)]
        public string Marks3 { get; set; }

        [StringLength(255)]
        public string Marks4 { get; set; }

        [StringLength(255)]
        public string Marks5 { get; set; }

        [StringLength(255)]
        public string NatureofGoods1 { get; set; }

        [StringLength(255)]
        public string NatureofGoods2 { get; set; }

        [StringLength(255)]
        public string Commodity { get; set; }

        [StringLength(10)]
        public string VAT { get; set; }

        [Required]
        [StringLength(10)]
        public string Status { get; set; }

        [StringLength(6)]
        public string CreatedBy { get; set; }

        public DateTime CreatedDate { get; set; }

        [StringLength(6)]
        public string UpdatedBy { get; set; }

        public DateTime? UpdatedDate { get; set; }

        [Column(TypeName = "timestamp")]
        [MaxLength(8)]
        [Timestamp]
        public byte[] Version { get; set; }

        public bool IsToACS { get; set; }

        [StringLength(20)]
        public string City { get; set; }

        [StringLength(20)]
        public string State { get; set; }

        public bool? ExistCustomer { get; set; }

        [StringLength(3)]
        public string CreatedStationID { get; set; }

        [StringLength(10)]
        public string VendorPostingGLCode { get; set; }

        [StringLength(255)]
        public string LocalName { get; set; }

        public int? LeadSourceID { get; set; }
    }
}
