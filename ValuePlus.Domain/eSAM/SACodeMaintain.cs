namespace ValuePlus.Domain.eSAM
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("SACodeMaintain")]
    public partial class SACodeMaintain
    {
        public int ID { get; set; }

        [StringLength(50)]
        public string Title { get; set; }

        [StringLength(50)]
        public string Class { get; set; }

        [StringLength(50)]
        public string Contents { get; set; }

        [StringLength(60)]
        public string Remark1 { get; set; }

        [StringLength(60)]
        public string Remark2 { get; set; }

        [StringLength(50)]
        public string Item { get; set; }

        [StringLength(10)]
        public string OrderNo { get; set; }

        [StringLength(10)]
        public string CreatedUserID { get; set; }

        public DateTime? CreatedDate { get; set; }

        [StringLength(10)]
        public string UpdateUserID { get; set; }

        public DateTime? UpdateDate { get; set; }
    }
}
