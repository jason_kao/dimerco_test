namespace ValuePlus.Domain.eSAM
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("SAMergeCheckDetail")]
    public partial class SAMergeCheckDetail
    {
        public int ID { get; set; }

        public int? tocustomerid { get; set; }

        public int? disposecustid { get; set; }

        [Required]
        [StringLength(6)]
        public string stationcode { get; set; }

        [StringLength(50)]
        public string StationIP { get; set; }

        [StringLength(50)]
        public string DBName { get; set; }

        [StringLength(50)]
        public string FunctionName { get; set; }

        [StringLength(50)]
        public string TableName { get; set; }

        [StringLength(50)]
        public string ColumnName { get; set; }

        [StringLength(50)]
        public string Module { get; set; }

        [StringLength(50)]
        public string Owner { get; set; }

        [StringLength(10)]
        public string Status { get; set; }
    }
}
