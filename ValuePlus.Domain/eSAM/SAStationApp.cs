namespace ValuePlus.Domain.eSAM
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("SAStationApp")]
    public partial class SAStationApp
    {
        public int ID { get; set; }

        [StringLength(10)]
        public string StationID { get; set; }

        [StringLength(2)]
        public string PID { get; set; }

        [StringLength(2)]
        public string Mode { get; set; }

        [StringLength(2)]
        public string Lvl { get; set; }

        [StringLength(10)]
        public string A1st { get; set; }

        [StringLength(10)]
        public string A1stD { get; set; }

        [StringLength(10)]
        public string A2nd { get; set; }

        [StringLength(10)]
        public string A2ndD { get; set; }

        public DateTime? CreatedDT { get; set; }

        [StringLength(10)]
        public string CreatedBy { get; set; }

        public DateTime? UpdatedDT { get; set; }

        [StringLength(10)]
        public string UpdatedBy { get; set; }

        [Column(TypeName = "timestamp")]
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        [MaxLength(8)]
        public byte[] Version { get; set; }
    }
}
