namespace ValuePlus.Domain.eSAM
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("SASalesTaskPL")]
    public partial class SASalesTaskPL
    {
        public int ID { get; set; }

        public int? SMPLID { get; set; }

        public int? SalesTaskID { get; set; }

        [StringLength(10)]
        public string CreatedUserID { get; set; }

        public DateTime? CreatedDate { get; set; }

        [StringLength(10)]
        public string UpdatedUserID { get; set; }

        public DateTime? UpdatedDate { get; set; }

        [StringLength(10)]
        public string Status { get; set; }

        public virtual SASalesTask SASalesTask { get; set; }
    }
}
