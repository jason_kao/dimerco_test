namespace ValuePlus.Domain.eSAM
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("SAAppLife")]
    public partial class SAAppLife
    {
        public int ID { get; set; }

        [StringLength(50)]
        public string AppName { get; set; }

        public DateTime? UpdatedDate { get; set; }
    }
}
