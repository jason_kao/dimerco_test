namespace ValuePlus.Domain.eSAM
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("SAQuoteChargeCondition")]
    public partial class SAQuoteChargeCondition
    {
        public int ID { get; set; }

        [StringLength(50)]
        public string Condition { get; set; }

        [StringLength(10)]
        public string CreatedUserID { get; set; }

        public DateTime? CreatedDate { get; set; }

        public int? SAChargeID { get; set; }

        [StringLength(10)]
        public string UpdatedUserID { get; set; }

        public DateTime? UPdatedDate { get; set; }

        [StringLength(1000)]
        public string Remark { get; set; }

        public virtual SAQuoteCharge SAQuoteCharge { get; set; }
    }
}
