namespace ValuePlus.Domain.eSAM
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("SACollectPotentialCustomerInfo")]
    public partial class SACollectPotentialCustomerInfo
    {
        public int ID { get; set; }

        [StringLength(255)]
        public string Type { get; set; }

        [StringLength(255)]
        public string Company { get; set; }

        [StringLength(255)]
        public string ContactName { get; set; }

        [StringLength(255)]
        public string ContactJobTitle { get; set; }

        public int? Country { get; set; }

        public int? State { get; set; }

        public int? City { get; set; }

        public int? Zip { get; set; }

        [StringLength(255)]
        public string Address { get; set; }

        [StringLength(255)]
        public string ContactEmail { get; set; }

        [StringLength(255)]
        public string ContactPhone { get; set; }

        [StringLength(255)]
        public string ContactFax { get; set; }

        public int? IndustryID { get; set; }

        [StringLength(255)]
        public string ServiceType { get; set; }

        [StringLength(255)]
        public string ServiceSource { get; set; }

        [StringLength(255)]
        public string ContactSubject { get; set; }

        public string Comments { get; set; }

        [StringLength(255)]
        public string CreatedBy { get; set; }

        public DateTime? CreatedDate { get; set; }

        [StringLength(255)]
        public string UpdatedBy { get; set; }

        public DateTime? UpdatedDate { get; set; }

        public int? PLID { get; set; }

        [StringLength(1)]
        public string isFeed { get; set; }
    }
}
