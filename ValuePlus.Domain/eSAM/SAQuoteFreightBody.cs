namespace ValuePlus.Domain.eSAM
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("SAQuoteFreightBody")]
    public partial class SAQuoteFreightBody
    {
        public int ID { get; set; }

        public int? FID { get; set; }

        [StringLength(10)]
        public string Cost1 { get; set; }

        [StringLength(10)]
        public string Cost2 { get; set; }

        [StringLength(10)]
        public string Cost3 { get; set; }

        [StringLength(10)]
        public string Cost4 { get; set; }

        [StringLength(10)]
        public string Cost5 { get; set; }

        [StringLength(10)]
        public string Cost6 { get; set; }

        [StringLength(10)]
        public string Cost7 { get; set; }

        [StringLength(10)]
        public string Cost8 { get; set; }

        [StringLength(10)]
        public string Cost9 { get; set; }

        [StringLength(10)]
        public string Cost10 { get; set; }

        [StringLength(10)]
        public string Profit1 { get; set; }

        [StringLength(10)]
        public string Profit2 { get; set; }

        [StringLength(10)]
        public string Profit3 { get; set; }

        [StringLength(10)]
        public string Profit4 { get; set; }

        [StringLength(10)]
        public string Profit5 { get; set; }

        [StringLength(10)]
        public string Profit6 { get; set; }

        [StringLength(10)]
        public string Profit7 { get; set; }

        [StringLength(10)]
        public string Profit8 { get; set; }

        [StringLength(10)]
        public string Profit9 { get; set; }

        [StringLength(10)]
        public string Profit10 { get; set; }

        public virtual SAQuoteFreights SAQuoteFreights { get; set; }
    }
}
