namespace ValuePlus.Domain.eSAM
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("SASecuredDataTmp")]
    public partial class SASecuredDataTmp
    {
        [StringLength(100)]
        public string ConfirmId { get; set; }

        public DateTime? IssueDate { get; set; }

        [StringLength(50)]
        public string ShptNo { get; set; }

        [StringLength(20)]
        public string LotNo { get; set; }

        [Key]
        [StringLength(10)]
        public string StationID { get; set; }

        public long? Rnk { get; set; }
    }
}
