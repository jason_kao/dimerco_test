namespace ValuePlus.Domain.eSAM
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class SASCScoreRules
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public SASCScoreRules()
        {
            SASCItems = new HashSet<SASCItems>();
        }

        [Key]
        public int HQID { get; set; }

        public int? ItemID { get; set; }

        public int? SortID { get; set; }

        public int? StartPCT { get; set; }

        public int? EndPCT { get; set; }

        public bool? isByPCT { get; set; }

        public double? FixedScore { get; set; }

        [StringLength(10)]
        public string Status { get; set; }

        public DateTime? CreatedDate { get; set; }

        [StringLength(10)]
        public string CreatedBy { get; set; }

        public DateTime? UpdatedDate { get; set; }

        [StringLength(10)]
        public string UpdatedBy { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<SASCItems> SASCItems { get; set; }
    }
}
