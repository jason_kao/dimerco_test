namespace ValuePlus.Domain.eSAM
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("SMCustomerContact")]
    public partial class SMCustomerContact
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ID { get; set; }

        [Key]
        [Column(Order = 0)]
        [StringLength(3)]
        public string StationID { get; set; }

        [Key]
        [Column(Order = 1)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int CustomerID { get; set; }

        [Key]
        [Column(Order = 2)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int LineNo { get; set; }

        [StringLength(50)]
        public string FirstName { get; set; }

        [StringLength(50)]
        public string LastName { get; set; }

        [StringLength(50)]
        public string FullName { get; set; }

        [StringLength(50)]
        public string Tel_H { get; set; }

        [StringLength(50)]
        public string Tel_M { get; set; }

        [StringLength(50)]
        public string Tel_O { get; set; }

        [StringLength(10)]
        public string Tel_OExt { get; set; }

        [StringLength(50)]
        public string Fax { get; set; }

        [StringLength(50)]
        public string Birthday { get; set; }

        [StringLength(50)]
        public string Email { get; set; }

        [StringLength(50)]
        public string Title { get; set; }

        [Required]
        [StringLength(10)]
        public string Status { get; set; }

        [StringLength(6)]
        public string CreatedBy { get; set; }

        public DateTime CreatedDate { get; set; }

        [StringLength(6)]
        public string UpdatedBy { get; set; }

        public DateTime? UpdatedDate { get; set; }

        [Column(TypeName = "timestamp")]
        [MaxLength(8)]
        [Timestamp]
        public byte[] Version { get; set; }

        [StringLength(50)]
        public string Role { get; set; }

        [StringLength(50)]
        public string Department { get; set; }

        [StringLength(50)]
        public string Location { get; set; }

        [StringLength(50)]
        public string Boss { get; set; }

        [StringLength(50)]
        public string Assistant { get; set; }

        [StringLength(50)]
        public string Language { get; set; }

        [StringLength(50)]
        public string JoinedCompany { get; set; }

        [StringLength(50)]
        public string JoinedIndustry { get; set; }

        [StringLength(50)]
        public string Hobby { get; set; }

        [StringLength(50)]
        public string Address { get; set; }

        [StringLength(50)]
        public string City { get; set; }

        [StringLength(50)]
        public string State { get; set; }

        [StringLength(50)]
        public string Country { get; set; }

        [StringLength(50)]
        public string ZIP { get; set; }

        [StringLength(50)]
        public string PersonalInformation { get; set; }

        [StringLength(50)]
        public string trContent { get; set; }

        [StringLength(50)]
        public string FamilyMembers { get; set; }

        [StringLength(50)]
        public string Names { get; set; }

        [StringLength(50)]
        public string Titles { get; set; }

        public DateTime? LastContactDate { get; set; }

        [StringLength(50)]
        public string LastContact { get; set; }

        [StringLength(50)]
        public string VIP { get; set; }

        [StringLength(50)]
        public string LocalName { get; set; }

        [StringLength(50)]
        public string Fax2 { get; set; }
    }
}
