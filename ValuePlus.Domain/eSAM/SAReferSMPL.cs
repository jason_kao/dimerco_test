namespace ValuePlus.Domain.eSAM
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("SAReferSMPL")]
    public partial class SAReferSMPL
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int SMHQID { get; set; }

        [StringLength(20)]
        public string Description { get; set; }

        [StringLength(10)]
        public string Status { get; set; }
    }
}
