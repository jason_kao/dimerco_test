namespace ValuePlus.Domain.eSAM
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("SAOneOffShipmentInfo")]
    public partial class SAOneOffShipmentInfo
    {
        public int ID { get; set; }

        [StringLength(50)]
        public string GWT { get; set; }

        [StringLength(50)]
        public string VWT { get; set; }

        [StringLength(50)]
        public string CWT { get; set; }

        [StringLength(50)]
        public string PCS { get; set; }

        [StringLength(200)]
        public string PickAddr { get; set; }

        [StringLength(200)]
        public string DevAddr { get; set; }

        public string Dimension { get; set; }

        public int QuoteID { get; set; }

        [StringLength(10)]
        public string CreatedUserID { get; set; }

        public DateTime? CreatedDate { get; set; }

        [StringLength(10)]
        public string UpdatedUserID { get; set; }

        public DateTime? UpdatedDate { get; set; }

        [StringLength(200)]
        public string CustomerRef { get; set; }

        [StringLength(200)]
        public string LaneSegment { get; set; }
    }
}
