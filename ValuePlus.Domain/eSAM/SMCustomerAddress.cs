namespace ValuePlus.Domain.eSAM
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("SMCustomerAddress")]
    public partial class SMCustomerAddress
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ID { get; set; }

        [Key]
        [Column(Order = 0)]
        [StringLength(3)]
        public string StationID { get; set; }

        [Key]
        [Column(Order = 1)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int CustomerID { get; set; }

        [Key]
        [Column(Order = 2)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int LineNo { get; set; }

        [Required]
        [StringLength(20)]
        public string FormType { get; set; }

        [Required]
        [StringLength(50)]
        public string Mode { get; set; }

        [StringLength(255)]
        public string CustomerName { get; set; }

        [StringLength(255)]
        public string CustomerAddress1 { get; set; }

        [StringLength(255)]
        public string CustomerAddress2 { get; set; }

        [StringLength(255)]
        public string CustomerAddress3 { get; set; }

        [StringLength(255)]
        public string CustomerAddress4 { get; set; }

        [StringLength(255)]
        public string CustomerAddress5 { get; set; }

        [StringLength(50)]
        public string CustomerPhone { get; set; }

        [StringLength(50)]
        public string CustomerFax { get; set; }

        [StringLength(50)]
        public string CustomerEmail { get; set; }

        [Required]
        [StringLength(50)]
        public string Status { get; set; }

        [StringLength(6)]
        public string CreatedBy { get; set; }

        public DateTime CreatedDate { get; set; }

        [StringLength(6)]
        public string UpdatedBy { get; set; }

        public DateTime? UpdatedDate { get; set; }
    }
}
