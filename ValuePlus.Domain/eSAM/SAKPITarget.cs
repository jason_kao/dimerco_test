namespace ValuePlus.Domain.eSAM
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("SAKPITarget")]
    public partial class SAKPITarget
    {
        [Key]
        [Column(Order = 0)]
        public int ID { get; set; }

        [Key]
        [Column(Order = 1)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int KPIID { get; set; }

        [Key]
        [Column(Order = 2)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int Target { get; set; }

        [StringLength(10)]
        public string Status { get; set; }

        [StringLength(20)]
        public string DeptType { get; set; }

        [StringLength(20)]
        public string DeptCode { get; set; }

        [StringLength(10)]
        public string CreatedUserID { get; set; }

        public DateTime? CreatedDate { get; set; }

        [StringLength(10)]
        public string UpdateUserID { get; set; }

        public DateTime? UpdateDate { get; set; }

        public int? score { get; set; }
    }
}
