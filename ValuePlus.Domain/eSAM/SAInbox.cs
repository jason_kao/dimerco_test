namespace ValuePlus.Domain.eSAM
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("SAInbox")]
    public partial class SAInbox
    {
        public int ID { get; set; }

        [StringLength(10)]
        public string SourceType { get; set; }

        public int? SourceID { get; set; }

        public string Comments { get; set; }

        [StringLength(30)]
        public string Status { get; set; }

        [StringLength(10)]
        public string ToUserID { get; set; }

        [StringLength(10)]
        public string CreatedUserID { get; set; }

        [StringLength(10)]
        public string UpdatedUserID { get; set; }

        public DateTime? CreatedDate { get; set; }

        public DateTime? UpdatedDate { get; set; }

        [StringLength(30)]
        public string ApprovalSN { get; set; }

        [StringLength(1)]
        public string AppLvl { get; set; }

        [StringLength(1)]
        public string StageLvl { get; set; }
    }
}
