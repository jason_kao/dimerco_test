namespace ValuePlus.Domain.eSAM
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("SAMergeExcHistory")]
    public partial class SAMergeExcHistory
    {
        public int ID { get; set; }

        public DateTime? CreatedDate { get; set; }

        public DateTime? UpdatedDate { get; set; }

        public int? MergeID { get; set; }

        public int? TableID { get; set; }

        [StringLength(10)]
        public string Status { get; set; }

        [StringLength(10)]
        public string StationID { get; set; }

        [StringLength(30)]
        public string DBName { get; set; }

        [StringLength(30)]
        public string StationIP { get; set; }

        public int? CNT { get; set; }
    }
}
