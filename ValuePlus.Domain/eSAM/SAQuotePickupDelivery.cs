namespace ValuePlus.Domain.eSAM
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("SAQuotePickupDelivery")]
    public partial class SAQuotePickupDelivery
    {
        public int ID { get; set; }

        public int? QuoteID { get; set; }

        public int? CustomerID { get; set; }

        [StringLength(50)]
        public string ContactName { get; set; }

        [StringLength(50)]
        public string ContactPhone { get; set; }

        [StringLength(255)]
        public string Country { get; set; }

        [StringLength(255)]
        public string State { get; set; }

        [StringLength(255)]
        public string City { get; set; }

        [StringLength(50)]
        public string Zip { get; set; }

        [StringLength(255)]
        public string Address { get; set; }

        [StringLength(10)]
        public string PD { get; set; }

        public virtual SAQuotes SAQuotes { get; set; }
    }
}
