namespace ValuePlus.Domain.eSAM
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("SAUserList")]
    public partial class SAUserList
    {
        public int ID { get; set; }

        [Required]
        [StringLength(10)]
        public string UserID { get; set; }

        [StringLength(50)]
        public string UserName { get; set; }

        [StringLength(50)]
        public string StationCode { get; set; }

        [StringLength(5)]
        public string SalesLevel { get; set; }

        public string CallingCard { get; set; }

        [StringLength(100)]
        public string Mail { get; set; }

        [StringLength(20)]
        public string Phone { get; set; }

        [StringLength(10)]
        public string Ext { get; set; }

        [StringLength(20)]
        public string CPhone { get; set; }

        [StringLength(20)]
        public string JobTitle { get; set; }

        [StringLength(10)]
        public string Status { get; set; }

        [StringLength(5)]
        public string ExceptAccess { get; set; }

        [StringLength(10)]
        public string CreatedUserID { get; set; }

        public DateTime? CreatedDate { get; set; }

        [StringLength(10)]
        public string UpdateUserID { get; set; }

        public DateTime? UpdateDate { get; set; }

        [StringLength(10)]
        public string StationType { get; set; }

        [StringLength(10)]
        public string Deputy { get; set; }

        [StringLength(10)]
        public string StationID { get; set; }

        public bool? PL1 { get; set; }

        public bool? PL2 { get; set; }

        public bool? PL3 { get; set; }

        public bool? PL4 { get; set; }

        public bool? PL5 { get; set; }

        public bool? PL6 { get; set; }

        public bool? PL7 { get; set; }

        public bool? PL8 { get; set; }

        public bool? PL9 { get; set; }

        public bool? PL10 { get; set; }

        public bool? PL11 { get; set; }

        public bool? PL12 { get; set; }

        [StringLength(10)]
        public string CityOrder { get; set; }

        [Required]
        [StringLength(1)]
        public string ExcelQuote { get; set; }
    }
}
