namespace ValuePlus.Domain.eSAM
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("SASalesTaskFeedback")]
    public partial class SASalesTaskFeedback
    {
        public int ID { get; set; }

        public int taskID { get; set; }

        [StringLength(2)]
        public string customerType { get; set; }

        public int? Step1TitleID { get; set; }

        public int? Step2TitleID { get; set; }

        public int? Step3TitleID { get; set; }

        public int? Step4TitleID { get; set; }

        [StringLength(50)]
        public string Step1ItemID { get; set; }

        [StringLength(50)]
        public string Step2ItemID { get; set; }

        [StringLength(50)]
        public string Step3ItemID { get; set; }

        [StringLength(50)]
        public string Step4ItemID { get; set; }

        [StringLength(100)]
        public string itemInfo { get; set; }

        [StringLength(10)]
        public string Status { get; set; }

        [StringLength(10)]
        public string CreateBy { get; set; }

        public DateTime? CreateDate { get; set; }

        [StringLength(10)]
        public string UpdatedBy { get; set; }

        public DateTime? UpdatedDate { get; set; }

        public virtual SASalesTask SASalesTask { get; set; }
    }
}
