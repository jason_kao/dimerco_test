namespace ValuePlus.Domain.eSAM
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class SAHouse20130306
    {
        [Key]
        [Column(Order = 0)]
        [StringLength(10)]
        public string Mode { get; set; }

        [Key]
        [Column(Order = 1)]
        [StringLength(10)]
        public string ProductlineID { get; set; }

        [StringLength(50)]
        public string House { get; set; }

        public int? CustomerID { get; set; }

        public int? FromCity { get; set; }

        [Key]
        [Column(Order = 2)]
        [StringLength(10)]
        public string FromCityCode { get; set; }

        public int? DEPT { get; set; }

        [Key]
        [Column(Order = 3)]
        [StringLength(10)]
        public string DEPTCode { get; set; }

        public int? DSTN { get; set; }

        [Key]
        [Column(Order = 4)]
        [StringLength(10)]
        public string DSTNCode { get; set; }

        public int? ToCity { get; set; }

        [Key]
        [Column(Order = 5)]
        [StringLength(10)]
        public string ToCityCode { get; set; }

        public double? GWT { get; set; }

        public double? CWT { get; set; }

        [StringLength(10)]
        public string WTUOM { get; set; }

        public double? CBM { get; set; }

        public double? TEU { get; set; }

        public int? C20 { get; set; }

        public int? C40 { get; set; }

        public int? C40H { get; set; }

        public int? C45 { get; set; }

        [StringLength(5)]
        public string AWBType { get; set; }

        [StringLength(10)]
        public string ConfirmID { get; set; }

        [StringLength(10)]
        public string ServiceType { get; set; }

        [Key]
        [Column(Order = 6)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int MAWBID { get; set; }

        [StringLength(13)]
        public string LotNo { get; set; }

        [StringLength(50)]
        public string Master { get; set; }

        public DateTime? IssueDate { get; set; }
    }
}
