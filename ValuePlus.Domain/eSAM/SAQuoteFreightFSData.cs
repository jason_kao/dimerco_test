namespace ValuePlus.Domain.eSAM
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("SAQuoteFreightFSData")]
    public partial class SAQuoteFreightFSData
    {
        public int QuoteFreightID { get; set; }

        public DateTime? FSecuredate { get; set; }

        [StringLength(50)]
        public string FSecuredShpt { get; set; }

        [StringLength(50)]
        public string FSecuredOwnerUserID { get; set; }

        public int ID { get; set; }

        public virtual SAQuoteFreights SAQuoteFreights { get; set; }
    }
}
