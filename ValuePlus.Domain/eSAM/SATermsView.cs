namespace ValuePlus.Domain.eSAM
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("SATermsView")]
    public partial class SATermsView
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int TermID { get; set; }

        [StringLength(50)]
        public string ProductLine { get; set; }

        [StringLength(50)]
        public string Todepts { get; set; }

        [StringLength(50)]
        public string VPID { get; set; }
    }
}
