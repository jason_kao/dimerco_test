namespace ValuePlus.Domain.eSAM
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("SAFilesUpload")]
    public partial class SAFilesUpload
    {
        [Key]
        [Column(Order = 0)]
        public int ID { get; set; }

        [Key]
        [Column(Order = 1)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int FileID { get; set; }

        [Key]
        [Column(Order = 2)]
        [StringLength(50)]
        public string Status { get; set; }

        [StringLength(255)]
        public string ErrorMsg { get; set; }
    }
}
