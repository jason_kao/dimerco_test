namespace ValuePlus.Domain.eSAM
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("SASalesTask")]
    public partial class SASalesTask
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public SASalesTask()
        {
            SASalesTaskFeedback = new HashSet<SASalesTaskFeedback>();
            SASalesTaskPL = new HashSet<SASalesTaskPL>();
            SASalesTaskSigned = new HashSet<SASalesTaskSigned>();
        }

        public int ID { get; set; }

        public int? LID { get; set; }

        [StringLength(255)]
        public string Subject { get; set; }

        [StringLength(50)]
        public string AssignTo { get; set; }

        [StringLength(255)]
        public string Attendees { get; set; }

        [StringLength(50)]
        public string Contact { get; set; }

        public string SalesTaskInfo { get; set; }

        [StringLength(255)]
        public string Remarks { get; set; }

        public DateTime? StartDate { get; set; }

        public DateTime? EndDate { get; set; }

        public DateTime? DueDate { get; set; }

        [StringLength(10)]
        public string Status { get; set; }

        [StringLength(50)]
        public string LocationCity { get; set; }

        [StringLength(255)]
        public string LocationAddress { get; set; }

        [StringLength(50)]
        public string LogType { get; set; }

        [StringLength(10)]
        public string Priority { get; set; }

        [StringLength(10)]
        public string TaskOwnerID { get; set; }

        public bool? AllDayEvent { get; set; }

        [StringLength(50)]
        public string AttendeeContact { get; set; }

        [StringLength(10)]
        public string CreatedUserID { get; set; }

        public DateTime? CreatedDate { get; set; }

        [StringLength(10)]
        public string UpdatedUserID { get; set; }

        public DateTime? UpdatedDate { get; set; }

        [StringLength(40)]
        public string SubjectType { get; set; }

        public bool? isDel { get; set; }

        public int? noticeIndex { get; set; }

        public DateTime? noticeTime { get; set; }

        public int? LogTypeID { get; set; }

        public int? SubjectTypeID { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<SASalesTaskFeedback> SASalesTaskFeedback { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<SASalesTaskPL> SASalesTaskPL { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<SASalesTaskSigned> SASalesTaskSigned { get; set; }
    }
}
