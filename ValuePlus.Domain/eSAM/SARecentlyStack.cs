namespace ValuePlus.Domain.eSAM
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("SARecentlyStack")]
    public partial class SARecentlyStack
    {
        public int ID { get; set; }

        [StringLength(10)]
        public string CreatedUserid { get; set; }

        public DateTime? CreatedDate { get; set; }

        [StringLength(20)]
        public string SourceType { get; set; }

        public int? SourceID { get; set; }
    }
}
