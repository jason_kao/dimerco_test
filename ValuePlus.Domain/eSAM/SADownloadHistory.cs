namespace ValuePlus.Domain.eSAM
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("SADownloadHistory")]
    public partial class SADownloadHistory
    {
        public int ID { get; set; }

        [StringLength(10)]
        public string UserID { get; set; }

        [StringLength(20)]
        public string Mode { get; set; }

        public DateTime? CreatedDate { get; set; }

        [StringLength(300)]
        public string Condition { get; set; }

        [StringLength(50)]
        public string FileName { get; set; }
    }
}
