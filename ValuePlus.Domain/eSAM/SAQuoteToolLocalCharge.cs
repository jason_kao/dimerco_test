namespace ValuePlus.Domain.eSAM
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("SAQuoteToolLocalCharge")]
    public partial class SAQuoteToolLocalCharge
    {
        public int ID { get; set; }

        public int MasterID { get; set; }

        [StringLength(10)]
        public string Min { get; set; }

        [StringLength(10)]
        public string Filling { get; set; }

        [StringLength(10)]
        public string CBM { get; set; }

        [StringLength(10)]
        public string TON { get; set; }

        [StringLength(20)]
        public string SellingRate1 { get; set; }

        [StringLength(20)]
        public string SellingRate2 { get; set; }

        [StringLength(20)]
        public string SellingRate3 { get; set; }

        [StringLength(20)]
        public string SellingRate4 { get; set; }

        [StringLength(20)]
        public string SellingRate5 { get; set; }

        [StringLength(20)]
        public string SellingRate6 { get; set; }

        [StringLength(20)]
        public string SellingRate7 { get; set; }

        [StringLength(20)]
        public string SellingRate8 { get; set; }

        [StringLength(20)]
        public string SellingRate9 { get; set; }

        [StringLength(20)]
        public string SellingRate10 { get; set; }

        [StringLength(200)]
        public string Remarks { get; set; }

        public DateTime? UpdatedDate { get; set; }

        [StringLength(10)]
        public string UpdatedBy { get; set; }

        public DateTime? CreatedDate { get; set; }

        [StringLength(10)]
        public string CreatedBy { get; set; }

        [StringLength(255)]
        public string ChargeCode { get; set; }

        [StringLength(255)]
        public string SubChargeCode { get; set; }

        public int? ChargeCodeID { get; set; }

        public int? SubChargeCodeID { get; set; }

        public virtual SAQuoteToolLocalChargeCity SAQuoteToolLocalChargeCity { get; set; }
    }
}
