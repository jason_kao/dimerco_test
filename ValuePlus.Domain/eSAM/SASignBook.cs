namespace ValuePlus.Domain.eSAM
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("SASignBook")]
    public partial class SASignBook
    {
        public int id { get; set; }

        [Required]
        [StringLength(20)]
        public string UserID { get; set; }

        public DateTime? UpdatedDate { get; set; }
    }
}
