namespace ValuePlus.Domain.eSAM
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("SASpecialMapping")]
    public partial class SASpecialMapping
    {
        public int ID { get; set; }

        [StringLength(10)]
        public string UserID { get; set; }

        [StringLength(10)]
        public string StationID { get; set; }

        [StringLength(10)]
        public string CreatedUserID { get; set; }

        public DateTime? CreatedDate { get; set; }

        [StringLength(10)]
        public string UpdatedUserID { get; set; }

        public DateTime? UpdatedDate { get; set; }
    }
}
