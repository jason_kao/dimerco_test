namespace ValuePlus.Domain.eSAM
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class SAFiles_T
    {
        public int ID { get; set; }

        [StringLength(100)]
        public string FileName { get; set; }

        [StringLength(100)]
        public string FileType { get; set; }

        [StringLength(100)]
        public string Contents { get; set; }

        [StringLength(255)]
        public string ShowName { get; set; }

        [StringLength(10)]
        public string Size { get; set; }

        [StringLength(100)]
        public string Position { get; set; }

        public DateTime? CreatedDate { get; set; }

        [StringLength(10)]
        public string CreatedUserID { get; set; }

        [StringLength(10)]
        public string Status { get; set; }

        [StringLength(10)]
        public string UpdatedUserID { get; set; }

        public DateTime? UpdatedDate { get; set; }

        [StringLength(10)]
        public string SourceID { get; set; }

        [StringLength(10)]
        public string SourceType { get; set; }

        [StringLength(10)]
        public string SubFileName { get; set; }

        [StringLength(200)]
        public string Upload { get; set; }
    }
}
