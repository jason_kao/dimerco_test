namespace ValuePlus.Domain.eSAM
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("SAMNCUsed")]
    public partial class SAMNCUsed
    {
        public int ID { get; set; }

        [StringLength(10)]
        public string UserID { get; set; }

        public int? MNCID { get; set; }

        public DateTime? UpdatedDate { get; set; }
    }
}
