namespace ValuePlus.Domain.eSAM
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("SMDeployVersion")]
    public partial class SMDeployVersion
    {
        [Key]
        [Column(Order = 0)]
        public int HQID { get; set; }

        [StringLength(500)]
        public string StationIP { get; set; }

        [Key]
        [Column(Order = 1)]
        public DateTime RunDate { get; set; }

        [StringLength(50)]
        public string Version { get; set; }

        [Column(TypeName = "text")]
        public string Error { get; set; }
    }
}
