namespace ValuePlus.Domain.eSAM
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("SATaskQueryStack")]
    public partial class SATaskQueryStack
    {
        public int ID { get; set; }

        [StringLength(255)]
        public string Title { get; set; }

        [StringLength(255)]
        public string TotalTimes { get; set; }

        [StringLength(255)]
        public string QueryStr { get; set; }

        [StringLength(255)]
        public string Views { get; set; }
    }
}
