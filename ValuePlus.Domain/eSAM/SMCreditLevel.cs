namespace ValuePlus.Domain.eSAM
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("SMCreditLevel")]
    public partial class SMCreditLevel
    {
        [Key]
        public int HQID { get; set; }

        [StringLength(2)]
        public string CreditLevelCode { get; set; }

        [StringLength(20)]
        public string CreditLevelName { get; set; }

        [StringLength(10)]
        public string Status { get; set; }

        [StringLength(6)]
        public string CreatedBy { get; set; }

        public DateTime? CreatedDate { get; set; }

        [StringLength(6)]
        public string UpdatedBy { get; set; }

        public DateTime? UpdatedDate { get; set; }

        public int? Seq { get; set; }
    }
}
