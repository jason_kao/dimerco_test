namespace ValuePlus.Domain.eSAM
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class SAQuoteFreights
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public SAQuoteFreights()
        {
            SAQuoteFreightBody = new HashSet<SAQuoteFreightBody>();
            SAQuoteFreightFSData = new HashSet<SAQuoteFreightFSData>();
        }

        public int ID { get; set; }

        public int? OriginCityID { get; set; }

        public int? DestinationCityID { get; set; }

        public int? CarrierID { get; set; }

        [StringLength(20)]
        public string ServiceLevel { get; set; }

        public int? UOMID { get; set; }

        public int? CurrencyID { get; set; }

        [StringLength(20)]
        public string WeeklyAllotment { get; set; }

        [StringLength(20)]
        public string Frequency { get; set; }

        [StringLength(20)]
        public string TransitTime { get; set; }

        [StringLength(200)]
        public string Remark { get; set; }

        public int? QuoteID { get; set; }

        [StringLength(20)]
        public string SellingRate1 { get; set; }

        [StringLength(20)]
        public string SellingRate2 { get; set; }

        [StringLength(20)]
        public string SellingRate3 { get; set; }

        [StringLength(20)]
        public string SellingRate4 { get; set; }

        [StringLength(20)]
        public string SellingRate5 { get; set; }

        [StringLength(20)]
        public string SellingRate6 { get; set; }

        [StringLength(20)]
        public string SellingRate7 { get; set; }

        [StringLength(20)]
        public string SellingRate8 { get; set; }

        [StringLength(20)]
        public string SellingRate9 { get; set; }

        [StringLength(20)]
        public string SellingRate10 { get; set; }

        [StringLength(50)]
        public string Commodity { get; set; }

        public int? PloadingPortID { get; set; }

        public int? PDischargePortID { get; set; }

        [StringLength(10)]
        public string CreatedUserID { get; set; }

        public DateTime? CreatedDate { get; set; }

        [StringLength(10)]
        public string UpdatedUserID { get; set; }

        public DateTime? UpdatedDate { get; set; }

        [StringLength(100)]
        public string OtherValue1 { get; set; }

        [StringLength(100)]
        public string OtherValue2 { get; set; }

        [StringLength(100)]
        public string OtherValue3 { get; set; }

        [StringLength(10)]
        public string EIType { get; set; }

        [StringLength(5)]
        public string PPCC { get; set; }

        [StringLength(50)]
        public string NatureOfGoods { get; set; }

        public int? FreightPty { get; set; }

        public int? OriginPty { get; set; }

        public int? DestPty { get; set; }

        [StringLength(6)]
        public string FLeadOwner { get; set; }

        public DateTime? FSecuredDT { get; set; }

        [StringLength(20)]
        public string EstTonnage { get; set; }

        [StringLength(20)]
        public string EstRevenue { get; set; }

        [StringLength(20)]
        public string EstGP { get; set; }

        public int? GroupID { get; set; }

        [StringLength(20)]
        public string FShptNo { get; set; }

        [StringLength(10)]
        public string FStationId { get; set; }

        [StringLength(20)]
        public string FLotNo { get; set; }

        public int? CityGroupID { get; set; }

        [StringLength(20)]
        public string CBMToWT { get; set; }

        public int? CBMToWTUOMID { get; set; }

        public DateTime? LSecuredDT { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<SAQuoteFreightBody> SAQuoteFreightBody { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<SAQuoteFreightFSData> SAQuoteFreightFSData { get; set; }

        public virtual SAQuotes SAQuotes { get; set; }
    }
}
