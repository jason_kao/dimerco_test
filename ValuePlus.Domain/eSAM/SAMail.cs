namespace ValuePlus.Domain.eSAM
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("SAMail")]
    public partial class SAMail
    {
        public int ID { get; set; }

        [StringLength(10)]
        public string FrmUserID { get; set; }

        [StringLength(255)]
        public string TOs { get; set; }

        [StringLength(255)]
        public string CCs { get; set; }

        public string Subject { get; set; }

        [Column(TypeName = "ntext")]
        public string Message { get; set; }

        [StringLength(255)]
        public string Attachment { get; set; }

        public DateTime? Date { get; set; }

        public int? SourceID { get; set; }

        [StringLength(10)]
        public string SourceType { get; set; }
    }
}
