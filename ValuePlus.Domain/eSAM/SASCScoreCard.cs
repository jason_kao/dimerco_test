namespace ValuePlus.Domain.eSAM
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("SASCScoreCard")]
    public partial class SASCScoreCard
    {
        [Key]
        public int HQID { get; set; }

        [StringLength(10)]
        public string UserID { get; set; }

        [StringLength(50)]
        public string ItemName { get; set; }

        public int? ItemCategoryID { get; set; }

        public int? CalculateTypeID { get; set; }

        public int? ItemTypeID { get; set; }

        public int? CustomerTypeID { get; set; }

        [StringLength(5)]
        public string ModeType { get; set; }

        [StringLength(3)]
        public string HomeCurrency { get; set; }

        public int? KPITarget { get; set; }

        public double? Value { get; set; }

        public double? Score { get; set; }

        public int? YY { get; set; }

        public int? MM { get; set; }

        public DateTime? CreatedDate { get; set; }

        [StringLength(10)]
        public string CreatedBy { get; set; }

        public DateTime? UpdatedDate { get; set; }

        [StringLength(10)]
        public string UpdatedBy { get; set; }

        [StringLength(10)]
        public string Status { get; set; }

        [StringLength(30)]
        public string ScoreTarget { get; set; }

        [StringLength(30)]
        public string rate { get; set; }
    }
}
