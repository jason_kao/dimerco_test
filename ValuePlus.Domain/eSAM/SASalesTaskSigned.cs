namespace ValuePlus.Domain.eSAM
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("SASalesTaskSigned")]
    public partial class SASalesTaskSigned
    {
        public int ID { get; set; }

        public int taskID { get; set; }

        public double lng { get; set; }

        public double lat { get; set; }

        [StringLength(10)]
        public string CreateBy { get; set; }

        public DateTime? CreateDate { get; set; }

        [StringLength(10)]
        public string UpdatedBy { get; set; }

        public DateTime? UpdatedDate { get; set; }

        public virtual SASalesTask SASalesTask { get; set; }
    }
}
