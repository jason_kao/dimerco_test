namespace ValuePlus.Domain.eSAM
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("SAQuoteChargeCity")]
    public partial class SAQuoteChargeCity
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public SAQuoteChargeCity()
        {
            SAQuoteCharge = new HashSet<SAQuoteCharge>();
        }

        public int ID { get; set; }

        public int? CityID { get; set; }

        public int? QuoteID { get; set; }

        public int? CurrencyID { get; set; }

        public int? UOMID { get; set; }

        [StringLength(10)]
        public string CityType { get; set; }

        [StringLength(10)]
        public string ChargeType { get; set; }

        [StringLength(10)]
        public string CreatedUserID { get; set; }

        public DateTime? CreatedDate { get; set; }

        [StringLength(10)]
        public string UpdatedUserID { get; set; }

        public DateTime? UpdatedDate { get; set; }

        [StringLength(20)]
        public string sType { get; set; }

        public int? OriginCityID { get; set; }

        public int? DestinationCityID { get; set; }

        public int? POL { get; set; }

        public int? POD { get; set; }

        [StringLength(50)]
        public string Method { get; set; }

        public int? GroupID { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<SAQuoteCharge> SAQuoteCharge { get; set; }

        public virtual SAQuotes SAQuotes { get; set; }
    }
}
