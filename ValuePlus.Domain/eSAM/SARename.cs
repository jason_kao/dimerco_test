namespace ValuePlus.Domain.eSAM
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("SARename")]
    public partial class SARename
    {
        public int ID { get; set; }

        public int? CustomerID { get; set; }

        [StringLength(20)]
        public string eSupportNo { get; set; }

        public DateTime? CreatedDate { get; set; }

        [StringLength(50)]
        public string NewName { get; set; }

        [StringLength(10)]
        public string CreatedUserID { get; set; }

        [StringLength(50)]
        public string Status { get; set; }

        public DateTime? UpdatedDate { get; set; }
    }
}
