namespace ValuePlus.Domain.eSAM
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("SACredit")]
    public partial class SACredit
    {
        public int ID { get; set; }

        public int LID { get; set; }

        public int PID { get; set; }

        [StringLength(20)]
        public string CreditRef { get; set; }

        [StringLength(10)]
        public string ReceivedDate { get; set; }

        [StringLength(50)]
        public string BusiLiense { get; set; }

        [StringLength(50)]
        public string MajorCommodity { get; set; }

        public int? DayNum { get; set; }

        [StringLength(50)]
        public string MajorBankInfo { get; set; }

        [StringLength(50)]
        public string CapitalAmount { get; set; }

        [StringLength(50)]
        public string EstablishedDate { get; set; }

        [StringLength(50)]
        public string NoofEmployee { get; set; }

        [StringLength(50)]
        public string AnnualRevenue { get; set; }

        public int? CreditTermID { get; set; }

        public int? CurrencyID { get; set; }

        [StringLength(10)]
        public string Status { get; set; }

        [StringLength(20)]
        public string Amount { get; set; }

        [StringLength(10)]
        public string CreatedUserID { get; set; }

        [StringLength(10)]
        public string UpdatedUserID { get; set; }

        public DateTime? UpdatedDate { get; set; }

        [StringLength(10)]
        public string CreditTermType { get; set; }

        [StringLength(255)]
        public string PLRemark { get; set; }

        [StringLength(50)]
        public string DueDate { get; set; }

        public DateTime? CreatedDate { get; set; }

        public int? CapitalCurrencyID { get; set; }

        public bool? Export { get; set; }

        public bool? Import { get; set; }

        public bool? DC { get; set; }

        public bool? SL { get; set; }

        public bool? VM { get; set; }

        public bool? TR { get; set; }

        public int PayableDay { get; set; }

        public int? PaymentDate1 { get; set; }

        public int? PaymentDate2 { get; set; }

        [StringLength(3)]
        public string StationID { get; set; }

        public DateTime? ApprovedDate { get; set; }

        public DateTime? ExpiredDate { get; set; }

        [StringLength(1)]
        public string ApprovalLvl { get; set; }

        [StringLength(1)]
        public string isAddedReqInfo { get; set; }

        public int? ClosingDate { get; set; }
    }
}
