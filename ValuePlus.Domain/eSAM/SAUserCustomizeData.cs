namespace ValuePlus.Domain.eSAM
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("SAUserCustomizeData")]
    public partial class SAUserCustomizeData
    {
        [Key]
        [Column(Order = 0)]
        public int ID { get; set; }

        [Key]
        [Column(Order = 1)]
        [StringLength(6)]
        public string UserID { get; set; }

        [Key]
        [Column(Order = 2)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int DataType { get; set; }

        [Key]
        [Column(Order = 3)]
        [StringLength(50)]
        public string DataValue { get; set; }

        [Key]
        [Column(Order = 4)]
        [StringLength(10)]
        public string UpdatedBy { get; set; }

        [Key]
        [Column(Order = 5)]
        public DateTime UpdatedDate { get; set; }
    }
}
