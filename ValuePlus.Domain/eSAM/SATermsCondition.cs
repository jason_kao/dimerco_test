namespace ValuePlus.Domain.eSAM
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("SATermsCondition")]
    public partial class SATermsCondition
    {
        public int ID { get; set; }

        public int? TermOrder { get; set; }

        [StringLength(10)]
        public string IsSelected { get; set; }

        public string Contents { get; set; }

        [StringLength(10)]
        public string DeptType { get; set; }

        [StringLength(10)]
        public string FrmID { get; set; }

        public DateTime? CreatedDate { get; set; }

        [StringLength(10)]
        public string CreatedUserID { get; set; }

        [StringLength(10)]
        public string UpdatedUserID { get; set; }

        public DateTime? UpdatedDate { get; set; }
    }
}
