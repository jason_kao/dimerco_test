namespace ValuePlus.Domain.eSAM
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class SASalesRoutes
    {
        public int ID { get; set; }

        public int? PID { get; set; }

        public int? OriginCityID { get; set; }

        public int? DestinationCityID { get; set; }

        [StringLength(20)]
        public string Duty1 { get; set; }

        [StringLength(20)]
        public string Duty2 { get; set; }

        public int? Probability { get; set; }

        [StringLength(10)]
        public string CreatedUserID { get; set; }

        public DateTime? CreatedDate { get; set; }

        [StringLength(10)]
        public string UpdatedUserID { get; set; }

        public DateTime? UpdatedDate { get; set; }
    }
}
