namespace ValuePlus.Domain.eSAM
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("SAOneOffCharge")]
    public partial class SAOneOffCharge
    {
        public int ID { get; set; }

        [StringLength(200)]
        public string ChargeName { get; set; }

        [StringLength(20)]
        public string CurrencyCode { get; set; }

        [StringLength(200)]
        public string Price { get; set; }

        public int? ChargeType { get; set; }

        public int QuoteID { get; set; }

        [StringLength(200)]
        public string CityName { get; set; }

        [StringLength(10)]
        public string CreatedUserID { get; set; }

        public DateTime? CreatedDate { get; set; }

        [StringLength(10)]
        public string UpdatedUserID { get; set; }

        public DateTime? UpdatedDate { get; set; }
    }
}
