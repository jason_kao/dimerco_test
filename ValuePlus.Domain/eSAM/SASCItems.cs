namespace ValuePlus.Domain.eSAM
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class SASCItems
    {
        [Key]
        public int HQID { get; set; }

        public int? GroupID { get; set; }

        public int? ItemCategoryID { get; set; }

        public int? CaculateTypeID { get; set; }

        public int? ItemTypeID { get; set; }

        public int? CustomerTypeID { get; set; }

        [StringLength(10)]
        public string ModeType { get; set; }

        [StringLength(50)]
        public string ItemName { get; set; }

        public double? Score { get; set; }

        public double? Target { get; set; }

        public bool? isMonthTarget { get; set; }

        public double? MonthTarget1 { get; set; }

        public double? MonthTarget2 { get; set; }

        public double? MonthTarget3 { get; set; }

        public double? MonthTarget4 { get; set; }

        public double? MonthTarget5 { get; set; }

        public double? MonthTarget6 { get; set; }

        public double? MonthTarget7 { get; set; }

        public double? MonthTarget8 { get; set; }

        public double? MonthTarget9 { get; set; }

        public double? MonthTarget10 { get; set; }

        public double? MonthTarget11 { get; set; }

        public double? MonthTarget12 { get; set; }

        public bool? isScoreRules { get; set; }

        public int? ScoreRulesID { get; set; }

        [StringLength(10)]
        public string Status { get; set; }

        public DateTime? CreatedDate { get; set; }

        [StringLength(10)]
        public string CreatedBy { get; set; }

        public DateTime? UpdatedDate { get; set; }

        [StringLength(10)]
        public string UpdatedBy { get; set; }

        public virtual SASCCode SASCCode { get; set; }

        public virtual SASCCode SASCCode1 { get; set; }

        public virtual SASCCode SASCCode2 { get; set; }

        public virtual SASCCode SASCCode3 { get; set; }

        public virtual SASCGroup SASCGroup { get; set; }

        public virtual SASCScoreRules SASCScoreRules { get; set; }
    }
}
