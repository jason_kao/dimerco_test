namespace ValuePlus.Domain.eSAM
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("SAMNCPotentialLead")]
    public partial class SAMNCPotentialLead
    {
        public int ID { get; set; }

        [Required]
        [StringLength(200)]
        public string ProjectName { get; set; }

        [StringLength(15)]
        public string ProjectCode { get; set; }

        [StringLength(10)]
        public string GlobalCode { get; set; }

        [Required]
        [StringLength(20)]
        public string ProductLine { get; set; }

        [StringLength(6)]
        public string RegMKT { get; set; }

        [StringLength(6)]
        public string GrpRep { get; set; }

        [StringLength(6)]
        public string AccMgr { get; set; }

        [StringLength(6)]
        public string AssAccMgr1 { get; set; }

        [StringLength(6)]
        public string AssAccMgr2 { get; set; }

        [StringLength(6)]
        public string AssAccMgr3 { get; set; }

        [StringLength(6)]
        public string AssAccMgr4 { get; set; }

        [StringLength(6)]
        public string AssAccMgr5 { get; set; }

        [StringLength(6)]
        public string Assistant { get; set; }

        public int? CustomerID { get; set; }

        [StringLength(20)]
        public string Status { get; set; }

        [StringLength(6)]
        public string SendBy { get; set; }

        public DateTime? SendDate { get; set; }

        [StringLength(6)]
        public string ApprovedBy { get; set; }

        public DateTime? ApprovedDate { get; set; }

        [Required]
        [StringLength(6)]
        public string CreatedBy { get; set; }

        public DateTime CreatedDate { get; set; }

        [Required]
        [StringLength(6)]
        public string UpdatedBy { get; set; }

        public DateTime UpdatedDate { get; set; }

        [Column(TypeName = "timestamp")]
        [MaxLength(8)]
        [Timestamp]
        public byte[] Version { get; set; }
    }
}
