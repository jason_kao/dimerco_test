namespace ValuePlus.Domain.eSAM
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class SAPRFreights
    {
        public int ID { get; set; }

        public int? PID { get; set; }

        [StringLength(10)]
        public string Status { get; set; }

        [StringLength(10)]
        public string Type { get; set; }

        public int? OrigCityID { get; set; }

        public int? DestCityID { get; set; }

        public int? PLoadingPortID { get; set; }

        public int? PDischargePortID { get; set; }

        public int? CurrencyID { get; set; }

        public int? CarrierID { get; set; }

        [StringLength(20)]
        public string Frequency { get; set; }

        [StringLength(20)]
        public string TransitTime { get; set; }

        public DateTime? EffectiveDate { get; set; }

        [Column(TypeName = "xml")]
        public string PromotionRate { get; set; }

        [Column(TypeName = "xml")]
        public string Remark { get; set; }

        [StringLength(10)]
        public string CreatedUserID { get; set; }

        public DateTime? CreatedDate { get; set; }

        [StringLength(10)]
        public string UpdatedUserID { get; set; }

        public DateTime? UpdatedDate { get; set; }
    }
}
