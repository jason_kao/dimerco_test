namespace ValuePlus.Domain.eSAM
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("SAStationList")]
    public partial class SAStationList
    {
        [StringLength(10)]
        public string StationCode { get; set; }

        [StringLength(10)]
        public string CreditAcc { get; set; }

        [StringLength(10)]
        public string StationName { get; set; }

        [StringLength(10)]
        public string LocalCurr { get; set; }

        [StringLength(10)]
        public string CreditAmount { get; set; }

        [StringLength(10)]
        public string CreditAir451st { get; set; }

        [StringLength(10)]
        public string CreditAir451stDeputy { get; set; }

        [StringLength(10)]
        public string CreditAir452nd { get; set; }

        [StringLength(10)]
        public string CreditAir452ndDeputy { get; set; }

        [StringLength(10)]
        public string CreditAir601st { get; set; }

        [StringLength(10)]
        public string CreditAir601stDeputy { get; set; }

        [StringLength(10)]
        public string CreditAir602nd { get; set; }

        [StringLength(10)]
        public string CreditAir602ndDeputy { get; set; }

        [StringLength(10)]
        public string CreditSea301st { get; set; }

        [StringLength(10)]
        public string CreditSea301stDeputy { get; set; }

        [StringLength(10)]
        public string CreditSea302nd { get; set; }

        [StringLength(10)]
        public string CreditSea302ndDeputy { get; set; }

        [StringLength(10)]
        public string CreditSea451st { get; set; }

        [StringLength(10)]
        public string CreditSea451stDeputy { get; set; }

        [StringLength(10)]
        public string CreditSea452nd { get; set; }

        [StringLength(10)]
        public string CreditSea452ndDeputy { get; set; }

        [StringLength(10)]
        public string CreditSea601st { get; set; }

        [StringLength(10)]
        public string CreditSea601stDeputy { get; set; }

        [StringLength(10)]
        public string CreditSea602nd { get; set; }

        [StringLength(10)]
        public string CreditSea602ndDeputy { get; set; }

        [StringLength(10)]
        public string CreditAir751st { get; set; }

        [StringLength(10)]
        public string CreditAir751stDeputy { get; set; }

        [StringLength(10)]
        public string CreditAir752nd { get; set; }

        [StringLength(10)]
        public string CreditAir752ndDeputy { get; set; }

        [StringLength(10)]
        public string CreditAir901st { get; set; }

        [StringLength(10)]
        public string CreditAir901stDeputy { get; set; }

        [StringLength(10)]
        public string CreditAir902nd { get; set; }

        [StringLength(10)]
        public string CreditAir902ndDeputy { get; set; }

        [StringLength(10)]
        public string CreditSea751st { get; set; }

        [StringLength(10)]
        public string CreditSea751stDeputy { get; set; }

        [StringLength(10)]
        public string CreditSea752nd { get; set; }

        [StringLength(10)]
        public string CreditSea752ndDeputy { get; set; }

        [StringLength(10)]
        public string CreditWMS451st { get; set; }

        [StringLength(10)]
        public string CreditWMS451stDeputy { get; set; }

        [StringLength(10)]
        public string CreditWMS452nd { get; set; }

        [StringLength(10)]
        public string CreditWMS452ndDeputy { get; set; }

        [StringLength(10)]
        public string CreditWMS1st { get; set; }

        [StringLength(10)]
        public string CreditWMS1stDeputy { get; set; }

        [StringLength(10)]
        public string CreditWMS2nd { get; set; }

        [StringLength(10)]
        public string CreditWMS2ndDeputy { get; set; }

        [StringLength(10)]
        public string CreditWMS601st { get; set; }

        [StringLength(10)]
        public string CreditWMS601stDeputy { get; set; }

        [StringLength(10)]
        public string CreditWMS602nd { get; set; }

        [StringLength(10)]
        public string CreditWMS602ndDeputy { get; set; }

        [StringLength(10)]
        public string CreditWMS301st { get; set; }

        [StringLength(10)]
        public string CreditWMS301stDeputy { get; set; }

        [StringLength(10)]
        public string CreditWMS302nd { get; set; }

        [StringLength(10)]
        public string CreditWMS302ndDeputy { get; set; }

        [StringLength(10)]
        public string CreditDAS451st { get; set; }

        [StringLength(10)]
        public string CreditDAS451stDeputy { get; set; }

        [StringLength(10)]
        public string CreditDAS452nd { get; set; }

        [StringLength(10)]
        public string CreditDAS452ndDeputy { get; set; }

        [StringLength(10)]
        public string CreditDAS601st { get; set; }

        [StringLength(10)]
        public string CreditDAS601stDeputy { get; set; }

        [StringLength(10)]
        public string CreditDAS602nd { get; set; }

        [StringLength(10)]
        public string CreditDAS602ndDeputy { get; set; }

        [StringLength(10)]
        public string CreditDAS1st { get; set; }

        [StringLength(10)]
        public string CreditDAS1stDeputy { get; set; }

        [StringLength(10)]
        public string CreditDAS2nd { get; set; }

        [StringLength(10)]
        public string CreditDAS2ndDeputy { get; set; }

        [StringLength(10)]
        public string Quote1st { get; set; }

        [StringLength(10)]
        public string Quote1stDeputy { get; set; }

        [StringLength(10)]
        public string Quote2nd { get; set; }

        [StringLength(10)]
        public string Quote2ndDeputy { get; set; }

        [StringLength(50)]
        public string Address { get; set; }

        [StringLength(20)]
        public string Phone { get; set; }

        [StringLength(10)]
        public string Credit1st { get; set; }

        [StringLength(10)]
        public string Credit1stDeputy { get; set; }

        [StringLength(10)]
        public string Credit2nd { get; set; }

        [StringLength(10)]
        public string Credit2ndDeputy { get; set; }

        [StringLength(10)]
        public string POD { get; set; }

        [StringLength(10)]
        public string POL { get; set; }

        [StringLength(10)]
        public string StationLevel { get; set; }

        [StringLength(10)]
        public string NoAir15 { get; set; }

        [StringLength(10)]
        public string NoSea07 { get; set; }

        [StringLength(10)]
        public string NoAir30 { get; set; }

        [StringLength(10)]
        public string NoSea15 { get; set; }

        [StringLength(10)]
        public string CreditUserID { get; set; }

        public DateTime? CreditDate { get; set; }

        [StringLength(10)]
        public string UpdateUserID { get; set; }

        public DateTime? UpdateDate { get; set; }

        [Key]
        [Column(Order = 0)]
        [StringLength(10)]
        public string StationID { get; set; }

        public int? ObjectCNT { get; set; }

        [StringLength(10)]
        public string creditair401st { get; set; }

        [StringLength(10)]
        public string creditair401stDeputy { get; set; }

        [StringLength(10)]
        public string creditair402nd { get; set; }

        [StringLength(10)]
        public string creditair402ndDeputy { get; set; }

        [StringLength(10)]
        public string creditSea401st { get; set; }

        [StringLength(10)]
        public string creditSea401stDeputy { get; set; }

        [StringLength(10)]
        public string creditSea402nd { get; set; }

        [StringLength(10)]
        public string creditSea402ndDeputy { get; set; }

        [StringLength(10)]
        public string creditSea71st { get; set; }

        [StringLength(10)]
        public string creditSea71stDeputy { get; set; }

        [StringLength(10)]
        public string creditSea72nd { get; set; }

        [StringLength(10)]
        public string creditSea72ndDeputy { get; set; }

        [StringLength(10)]
        public string SelfApp { get; set; }

        [Key]
        [Column(Order = 1)]
        public bool QuoteAppbyPID { get; set; }

        [Key]
        [Column(Order = 2)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int QuoteType { get; set; }

        [StringLength(10)]
        public string CreditMTS301st { get; set; }

        [StringLength(10)]
        public string CreditMTS301stDeputy { get; set; }

        [StringLength(10)]
        public string CreditMTS302nd { get; set; }

        [StringLength(10)]
        public string CreditMTS302ndDeputy { get; set; }

        [StringLength(10)]
        public string CreditMTS451st { get; set; }

        [StringLength(10)]
        public string CreditMTS451stDeputy { get; set; }

        [StringLength(10)]
        public string CreditMTS452nd { get; set; }

        [StringLength(10)]
        public string CreditMTS452ndDeputy { get; set; }

        [StringLength(10)]
        public string CreditMTS601st { get; set; }

        [StringLength(10)]
        public string CreditMTS601stDeputy { get; set; }

        [StringLength(10)]
        public string CreditMTS602nd { get; set; }

        [StringLength(10)]
        public string CreditMTS602ndDeputy { get; set; }

        [StringLength(10)]
        public string CreditMTS751st { get; set; }

        [StringLength(10)]
        public string CreditMTS751stDeputy { get; set; }

        [StringLength(10)]
        public string CreditMTS752nd { get; set; }

        [StringLength(10)]
        public string CreditMTS752ndDeputy { get; set; }

        [StringLength(10)]
        public string CreditTMS301st { get; set; }

        [StringLength(10)]
        public string CreditTMS301stDeputy { get; set; }

        [StringLength(10)]
        public string CreditTMS302nd { get; set; }

        [StringLength(10)]
        public string CreditTMS302ndDeputy { get; set; }

        [StringLength(10)]
        public string CreditTMS451st { get; set; }

        [StringLength(10)]
        public string CreditTMS451stDeputy { get; set; }

        [StringLength(10)]
        public string CreditTMS452nd { get; set; }

        [StringLength(10)]
        public string CreditTMS452ndDeputy { get; set; }

        [StringLength(10)]
        public string CreditTMS601st { get; set; }

        [StringLength(10)]
        public string CreditTMS601stDeputy { get; set; }

        [StringLength(10)]
        public string CreditTMS602nd { get; set; }

        [StringLength(10)]
        public string CreditTMS602ndDeputy { get; set; }

        [StringLength(10)]
        public string CreditTMS751st { get; set; }

        [StringLength(10)]
        public string CreditTMS751stDeputy { get; set; }

        [StringLength(10)]
        public string CreditTMS752nd { get; set; }

        [StringLength(10)]
        public string CreditTMS752ndDeputy { get; set; }

        [StringLength(10)]
        public string UpdatedUserID { get; set; }

        public DateTime? UpdatedDate { get; set; }

        public int? CreditDay { get; set; }

        public int? QuoteLang { get; set; }
    }
}
