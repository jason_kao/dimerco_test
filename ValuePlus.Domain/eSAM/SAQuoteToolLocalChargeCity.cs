namespace ValuePlus.Domain.eSAM
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("SAQuoteToolLocalChargeCity")]
    public partial class SAQuoteToolLocalChargeCity
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public SAQuoteToolLocalChargeCity()
        {
            SAQuoteToolLocalCharge = new HashSet<SAQuoteToolLocalCharge>();
            SAQuoteToolLocalChargeFee = new HashSet<SAQuoteToolLocalChargeFee>();
        }

        public int ID { get; set; }

        public int QuoteID { get; set; }

        [Required]
        [StringLength(255)]
        public string CityCodes { get; set; }

        [Required]
        [StringLength(3)]
        public string CurencyCode { get; set; }

        [Required]
        [StringLength(3)]
        public string UOMCode { get; set; }

        [Required]
        [StringLength(10)]
        public string Mode { get; set; }

        [StringLength(20)]
        public string WeightBreak1 { get; set; }

        [StringLength(20)]
        public string WeightBreak2 { get; set; }

        [StringLength(20)]
        public string WeightBreak3 { get; set; }

        [StringLength(20)]
        public string WeightBreak4 { get; set; }

        [StringLength(20)]
        public string WeightBreak5 { get; set; }

        [StringLength(20)]
        public string WeightBreak6 { get; set; }

        [StringLength(20)]
        public string WeightBreak7 { get; set; }

        [StringLength(20)]
        public string WeightBreak8 { get; set; }

        [StringLength(20)]
        public string WeightBreak9 { get; set; }

        [StringLength(20)]
        public string WeightBreak10 { get; set; }

        public int? UOM1 { get; set; }

        public int? UOM2 { get; set; }

        public int? UOM3 { get; set; }

        public int? UOM4 { get; set; }

        public int? UOM5 { get; set; }

        public int? UOM6 { get; set; }

        public int? UOM7 { get; set; }

        public int? UOM8 { get; set; }

        public int? UOM9 { get; set; }

        public int? UOM10 { get; set; }

        public DateTime? UpdatedDate { get; set; }

        [StringLength(10)]
        public string UpdatedBy { get; set; }

        public DateTime? CreatedDate { get; set; }

        [StringLength(10)]
        public string CreatedBy { get; set; }

        public virtual SAQuotes SAQuotes { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<SAQuoteToolLocalCharge> SAQuoteToolLocalCharge { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<SAQuoteToolLocalChargeFee> SAQuoteToolLocalChargeFee { get; set; }
    }
}
