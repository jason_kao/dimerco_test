namespace ValuePlus.Domain.eSAM
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    public partial class eSAM_Entities : DbContext
    {
        public eSAM_Entities()
            : base("name=eChainVP_eSAM")
        {
        }

        public virtual DbSet<SAAppLife> SAAppLife { get; set; }
        public virtual DbSet<SAAutoSendMail> SAAutoSendMail { get; set; }
        public virtual DbSet<SACheckList> SACheckList { get; set; }
        public virtual DbSet<SACodeMaintain> SACodeMaintain { get; set; }
        public virtual DbSet<SACollectPotentialCustomerInfo> SACollectPotentialCustomerInfo { get; set; }
        public virtual DbSet<SAContactQueryStack> SAContactQueryStack { get; set; }
        public virtual DbSet<SACredit> SACredit { get; set; }
        public virtual DbSet<SACrossStation> SACrossStation { get; set; }
        public virtual DbSet<SACustGroup> SACustGroup { get; set; }
        public virtual DbSet<SACustRelated> SACustRelated { get; set; }
        public virtual DbSet<SADiscussComment> SADiscussComment { get; set; }
        public virtual DbSet<SADownloadHistory> SADownloadHistory { get; set; }
        public virtual DbSet<SAFiles> SAFiles { get; set; }
        public virtual DbSet<SAFiles_T> SAFiles_T { get; set; }
        public virtual DbSet<SAInbox> SAInbox { get; set; }
        public virtual DbSet<SAMail> SAMail { get; set; }
        public virtual DbSet<SAMergeCheck> SAMergeCheck { get; set; }
        public virtual DbSet<SAMergeCheckDetail> SAMergeCheckDetail { get; set; }
        public virtual DbSet<SAMergeCustList> SAMergeCustList { get; set; }
        public virtual DbSet<SAMergeCustTable> SAMergeCustTable { get; set; }
        public virtual DbSet<SAMergeEffectStation> SAMergeEffectStation { get; set; }
        public virtual DbSet<SAMergeExcHistory> SAMergeExcHistory { get; set; }
        public virtual DbSet<SAMNCPotentialLead> SAMNCPotentialLead { get; set; }
        public virtual DbSet<SAMNCUsed> SAMNCUsed { get; set; }
        public virtual DbSet<SAOneOffCharge> SAOneOffCharge { get; set; }
        public virtual DbSet<SAOneOffShipmentInfo> SAOneOffShipmentInfo { get; set; }
        public virtual DbSet<SAPotentialLead> SAPotentialLead { get; set; }
        public virtual DbSet<SAPRFreights> SAPRFreights { get; set; }
        public virtual DbSet<SAProcess> SAProcess { get; set; }
        public virtual DbSet<SAProductLines> SAProductLines { get; set; }
        public virtual DbSet<SAQFExportInfo> SAQFExportInfo { get; set; }
        public virtual DbSet<SAQFMaster> SAQFMaster { get; set; }
        public virtual DbSet<SAQFMasterMaillingSetting> SAQFMasterMaillingSetting { get; set; }
        public virtual DbSet<SAQFStationVolume> SAQFStationVolume { get; set; }
        public virtual DbSet<SAQueryStack> SAQueryStack { get; set; }
        public virtual DbSet<SAQuoteCharge> SAQuoteCharge { get; set; }
        public virtual DbSet<SAQuoteChargeCity> SAQuoteChargeCity { get; set; }
        public virtual DbSet<SAQuoteChargeCondition> SAQuoteChargeCondition { get; set; }
        public virtual DbSet<SAQuoteChargeItem> SAQuoteChargeItem { get; set; }
        public virtual DbSet<SAQuoteContact> SAQuoteContact { get; set; }
        public virtual DbSet<SAQuoteFreightBody> SAQuoteFreightBody { get; set; }
        public virtual DbSet<SAQuoteFreightFSData> SAQuoteFreightFSData { get; set; }
        public virtual DbSet<SAQuoteFreightHistoricalRate> SAQuoteFreightHistoricalRate { get; set; }
        public virtual DbSet<SAQuoteFreights> SAQuoteFreights { get; set; }
        public virtual DbSet<SAQuotePickupDelivery> SAQuotePickupDelivery { get; set; }
        public virtual DbSet<SAQuotes> SAQuotes { get; set; }
        public virtual DbSet<SAQuoteSection> SAQuoteSection { get; set; }
        public virtual DbSet<SAQuoteTerms> SAQuoteTerms { get; set; }
        public virtual DbSet<SAQuoteToolFreights> SAQuoteToolFreights { get; set; }
        public virtual DbSet<SAQuoteToolLocalCharge> SAQuoteToolLocalCharge { get; set; }
        public virtual DbSet<SAQuoteToolLocalChargeCity> SAQuoteToolLocalChargeCity { get; set; }
        public virtual DbSet<SAQuoteToolLocalChargeFee> SAQuoteToolLocalChargeFee { get; set; }
        public virtual DbSet<SAQuoteToolSurcharges> SAQuoteToolSurcharges { get; set; }
        public virtual DbSet<SARecentlyStack> SARecentlyStack { get; set; }
        public virtual DbSet<SARename> SARename { get; set; }
        public virtual DbSet<SASalesRoutes> SASalesRoutes { get; set; }
        public virtual DbSet<SASalesTask> SASalesTask { get; set; }
        public virtual DbSet<SASalesTaskFeedback> SASalesTaskFeedback { get; set; }
        public virtual DbSet<SASalesTaskPL> SASalesTaskPL { get; set; }
        public virtual DbSet<SASalesTaskSigned> SASalesTaskSigned { get; set; }
        public virtual DbSet<SASCCode> SASCCode { get; set; }
        public virtual DbSet<SASCDefinition> SASCDefinition { get; set; }
        public virtual DbSet<SASCGroup> SASCGroup { get; set; }
        public virtual DbSet<SASCItems> SASCItems { get; set; }
        public virtual DbSet<SASCScoreCard> SASCScoreCard { get; set; }
        public virtual DbSet<SASCScoreRules> SASCScoreRules { get; set; }
        public virtual DbSet<SASCUserGroup> SASCUserGroup { get; set; }
        public virtual DbSet<SASignBook> SASignBook { get; set; }
        public virtual DbSet<SASpecialMapping> SASpecialMapping { get; set; }
        public virtual DbSet<SAStationApp> SAStationApp { get; set; }
        public virtual DbSet<SAStationListPL> SAStationListPL { get; set; }
        public virtual DbSet<SAStationRelated> SAStationRelated { get; set; }
        public virtual DbSet<SATaskAttendee> SATaskAttendee { get; set; }
        public virtual DbSet<SATaskQueryStack> SATaskQueryStack { get; set; }
        public virtual DbSet<SATermOfShipping> SATermOfShipping { get; set; }
        public virtual DbSet<SATermsCondition> SATermsCondition { get; set; }
        public virtual DbSet<SAUserList> SAUserList { get; set; }
        public virtual DbSet<SAUserRelated> SAUserRelated { get; set; }
        public virtual DbSet<SAUserReportTo> SAUserReportTo { get; set; }
        public virtual DbSet<SAUserRole> SAUserRole { get; set; }
        public virtual DbSet<SMCreditApprovalLevel> SMCreditApprovalLevel { get; set; }
        public virtual DbSet<SMCreditLevel> SMCreditLevel { get; set; }
        public virtual DbSet<SMCustomer> SMCustomer { get; set; }
        public virtual DbSet<SMCustomerAddress> SMCustomerAddress { get; set; }
        public virtual DbSet<SMCustomerAddressMode> SMCustomerAddressMode { get; set; }
        public virtual DbSet<SMCustomerContact> SMCustomerContact { get; set; }
        public virtual DbSet<SMCustomerFormMail> SMCustomerFormMail { get; set; }
        public virtual DbSet<SMCustomerPL> SMCustomerPL { get; set; }
        public virtual DbSet<SMDocument> SMDocument { get; set; }
        public virtual DbSet<SMSalesPerson> SMSalesPerson { get; set; }
        public virtual DbSet<SMTrucker> SMTrucker { get; set; }
        public virtual DbSet<SMWarehouse> SMWarehouse { get; set; }
        public virtual DbSet<SMWLocation> SMWLocation { get; set; }
        public virtual DbSet<SAContactReportTo> SAContactReportTo { get; set; }
        public virtual DbSet<SADelRelativeRight> SADelRelativeRight { get; set; }
        public virtual DbSet<SAFilesUpload> SAFilesUpload { get; set; }
        public virtual DbSet<SAHouse20130306> SAHouse20130306 { get; set; }
        public virtual DbSet<SAKPI> SAKPI { get; set; }
        public virtual DbSet<SAKPITarget> SAKPITarget { get; set; }
        public virtual DbSet<SAPotentialLeadComment> SAPotentialLeadComment { get; set; }
        public virtual DbSet<SAPotentialLeadContact> SAPotentialLeadContact { get; set; }
        public virtual DbSet<SAReferSMPL> SAReferSMPL { get; set; }
        public virtual DbSet<SASecuredDataTmp> SASecuredDataTmp { get; set; }
        public virtual DbSet<SASerialNumber> SASerialNumber { get; set; }
        public virtual DbSet<SAStationList> SAStationList { get; set; }
        public virtual DbSet<SATermsView> SATermsView { get; set; }
        public virtual DbSet<SAUserCustomizeData> SAUserCustomizeData { get; set; }
        public virtual DbSet<SMDeployVersion> SMDeployVersion { get; set; }
        public virtual DbSet<SMStation> SMStation { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<SAAppLife>()
                .Property(e => e.AppName)
                .IsUnicode(false);

            modelBuilder.Entity<SACheckList>()
                .Property(e => e.StationIP)
                .IsUnicode(false);

            modelBuilder.Entity<SACheckList>()
                .Property(e => e.ServerName)
                .IsUnicode(false);

            modelBuilder.Entity<SACheckList>()
                .Property(e => e.Status)
                .IsUnicode(false);

            modelBuilder.Entity<SACheckList>()
                .Property(e => e.Cmd)
                .IsUnicode(false);

            modelBuilder.Entity<SACheckList>()
                .Property(e => e.StationCode)
                .IsUnicode(false);

            modelBuilder.Entity<SACheckList>()
                .Property(e => e.CustomerName)
                .IsUnicode(false);

            modelBuilder.Entity<SACodeMaintain>()
                .Property(e => e.Title)
                .IsUnicode(false);

            modelBuilder.Entity<SACodeMaintain>()
                .Property(e => e.Class)
                .IsUnicode(false);

            modelBuilder.Entity<SACodeMaintain>()
                .Property(e => e.Remark1)
                .IsUnicode(false);

            modelBuilder.Entity<SACodeMaintain>()
                .Property(e => e.Remark2)
                .IsUnicode(false);

            modelBuilder.Entity<SACodeMaintain>()
                .Property(e => e.Item)
                .IsUnicode(false);

            modelBuilder.Entity<SACodeMaintain>()
                .Property(e => e.OrderNo)
                .IsUnicode(false);

            modelBuilder.Entity<SACodeMaintain>()
                .Property(e => e.CreatedUserID)
                .IsUnicode(false);

            modelBuilder.Entity<SACodeMaintain>()
                .Property(e => e.UpdateUserID)
                .IsUnicode(false);

            modelBuilder.Entity<SACollectPotentialCustomerInfo>()
                .Property(e => e.isFeed)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<SACredit>()
                .Property(e => e.CreditRef)
                .IsUnicode(false);

            modelBuilder.Entity<SACredit>()
                .Property(e => e.ReceivedDate)
                .IsUnicode(false);

            modelBuilder.Entity<SACredit>()
                .Property(e => e.CapitalAmount)
                .IsUnicode(false);

            modelBuilder.Entity<SACredit>()
                .Property(e => e.EstablishedDate)
                .IsUnicode(false);

            modelBuilder.Entity<SACredit>()
                .Property(e => e.AnnualRevenue)
                .IsUnicode(false);

            modelBuilder.Entity<SACredit>()
                .Property(e => e.Status)
                .IsUnicode(false);

            modelBuilder.Entity<SACredit>()
                .Property(e => e.Amount)
                .IsUnicode(false);

            modelBuilder.Entity<SACredit>()
                .Property(e => e.CreatedUserID)
                .IsUnicode(false);

            modelBuilder.Entity<SACredit>()
                .Property(e => e.UpdatedUserID)
                .IsUnicode(false);

            modelBuilder.Entity<SACredit>()
                .Property(e => e.CreditTermType)
                .IsUnicode(false);

            modelBuilder.Entity<SACredit>()
                .Property(e => e.DueDate)
                .IsUnicode(false);

            modelBuilder.Entity<SACredit>()
                .Property(e => e.StationID)
                .IsUnicode(false);

            modelBuilder.Entity<SACredit>()
                .Property(e => e.ApprovalLvl)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<SACredit>()
                .Property(e => e.isAddedReqInfo)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<SACrossStation>()
                .Property(e => e.UserID)
                .IsUnicode(false);

            modelBuilder.Entity<SACrossStation>()
                .Property(e => e.StationID)
                .IsUnicode(false);

            modelBuilder.Entity<SACrossStation>()
                .Property(e => e.StationType)
                .IsUnicode(false);

            modelBuilder.Entity<SACrossStation>()
                .Property(e => e.CreatedUserID)
                .IsUnicode(false);

            modelBuilder.Entity<SACrossStation>()
                .Property(e => e.UpdateUserID)
                .IsUnicode(false);

            modelBuilder.Entity<SACustGroup>()
                .Property(e => e.Depttype)
                .IsUnicode(false);

            modelBuilder.Entity<SACustGroup>()
                .Property(e => e.CreatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<SACustGroup>()
                .Property(e => e.UpdatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<SACustGroup>()
                .Property(e => e.sType)
                .IsUnicode(false);

            modelBuilder.Entity<SACustRelated>()
                .Property(e => e.CreatedUserID)
                .IsUnicode(false);

            modelBuilder.Entity<SACustRelated>()
                .Property(e => e.UpdatedUserID)
                .IsUnicode(false);

            modelBuilder.Entity<SACustRelated>()
                .Property(e => e.PID)
                .IsUnicode(false);

            modelBuilder.Entity<SACustRelated>()
                .Property(e => e.Status)
                .IsUnicode(false);

            modelBuilder.Entity<SADiscussComment>()
                .Property(e => e.Status)
                .IsUnicode(false);

            modelBuilder.Entity<SADiscussComment>()
                .Property(e => e.ReplyUserID)
                .IsUnicode(false);

            modelBuilder.Entity<SADiscussComment>()
                .Property(e => e.SourceType)
                .IsUnicode(false);

            modelBuilder.Entity<SADownloadHistory>()
                .Property(e => e.UserID)
                .IsUnicode(false);

            modelBuilder.Entity<SADownloadHistory>()
                .Property(e => e.Mode)
                .IsUnicode(false);

            modelBuilder.Entity<SADownloadHistory>()
                .Property(e => e.Condition)
                .IsUnicode(false);

            modelBuilder.Entity<SADownloadHistory>()
                .Property(e => e.FileName)
                .IsUnicode(false);

            modelBuilder.Entity<SAFiles>()
                .Property(e => e.Size)
                .IsUnicode(false);

            modelBuilder.Entity<SAFiles>()
                .Property(e => e.Position)
                .IsUnicode(false);

            modelBuilder.Entity<SAFiles>()
                .Property(e => e.CreatedUserID)
                .IsUnicode(false);

            modelBuilder.Entity<SAFiles>()
                .Property(e => e.Status)
                .IsUnicode(false);

            modelBuilder.Entity<SAFiles>()
                .Property(e => e.UpdatedUserID)
                .IsUnicode(false);

            modelBuilder.Entity<SAFiles>()
                .Property(e => e.SourceID)
                .IsUnicode(false);

            modelBuilder.Entity<SAFiles>()
                .Property(e => e.SourceType)
                .IsUnicode(false);

            modelBuilder.Entity<SAFiles>()
                .Property(e => e.SubFileName)
                .IsUnicode(false);

            modelBuilder.Entity<SAFiles_T>()
                .Property(e => e.Size)
                .IsUnicode(false);

            modelBuilder.Entity<SAFiles_T>()
                .Property(e => e.Position)
                .IsUnicode(false);

            modelBuilder.Entity<SAFiles_T>()
                .Property(e => e.CreatedUserID)
                .IsUnicode(false);

            modelBuilder.Entity<SAFiles_T>()
                .Property(e => e.Status)
                .IsUnicode(false);

            modelBuilder.Entity<SAFiles_T>()
                .Property(e => e.UpdatedUserID)
                .IsUnicode(false);

            modelBuilder.Entity<SAFiles_T>()
                .Property(e => e.SourceID)
                .IsUnicode(false);

            modelBuilder.Entity<SAFiles_T>()
                .Property(e => e.SourceType)
                .IsUnicode(false);

            modelBuilder.Entity<SAFiles_T>()
                .Property(e => e.SubFileName)
                .IsUnicode(false);

            modelBuilder.Entity<SAFiles_T>()
                .Property(e => e.Upload)
                .IsUnicode(false);

            modelBuilder.Entity<SAInbox>()
                .Property(e => e.SourceType)
                .IsUnicode(false);

            modelBuilder.Entity<SAInbox>()
                .Property(e => e.Status)
                .IsUnicode(false);

            modelBuilder.Entity<SAInbox>()
                .Property(e => e.ToUserID)
                .IsUnicode(false);

            modelBuilder.Entity<SAInbox>()
                .Property(e => e.CreatedUserID)
                .IsUnicode(false);

            modelBuilder.Entity<SAInbox>()
                .Property(e => e.UpdatedUserID)
                .IsUnicode(false);

            modelBuilder.Entity<SAInbox>()
                .Property(e => e.ApprovalSN)
                .IsUnicode(false);

            modelBuilder.Entity<SAInbox>()
                .Property(e => e.AppLvl)
                .IsUnicode(false);

            modelBuilder.Entity<SAInbox>()
                .Property(e => e.StageLvl)
                .IsUnicode(false);

            modelBuilder.Entity<SAMail>()
                .Property(e => e.FrmUserID)
                .IsUnicode(false);

            modelBuilder.Entity<SAMail>()
                .Property(e => e.TOs)
                .IsUnicode(false);

            modelBuilder.Entity<SAMail>()
                .Property(e => e.CCs)
                .IsUnicode(false);

            modelBuilder.Entity<SAMail>()
                .Property(e => e.SourceType)
                .IsUnicode(false);

            modelBuilder.Entity<SAMergeCheck>()
                .Property(e => e.StationIP)
                .IsUnicode(false);

            modelBuilder.Entity<SAMergeCheck>()
                .Property(e => e.DBName)
                .IsUnicode(false);

            modelBuilder.Entity<SAMergeCheck>()
                .Property(e => e.IsCheck)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<SAMergeCheckDetail>()
                .Property(e => e.stationcode)
                .IsUnicode(false);

            modelBuilder.Entity<SAMergeCheckDetail>()
                .Property(e => e.StationIP)
                .IsUnicode(false);

            modelBuilder.Entity<SAMergeCheckDetail>()
                .Property(e => e.DBName)
                .IsUnicode(false);

            modelBuilder.Entity<SAMergeCheckDetail>()
                .Property(e => e.FunctionName)
                .IsUnicode(false);

            modelBuilder.Entity<SAMergeCheckDetail>()
                .Property(e => e.TableName)
                .IsUnicode(false);

            modelBuilder.Entity<SAMergeCheckDetail>()
                .Property(e => e.ColumnName)
                .IsUnicode(false);

            modelBuilder.Entity<SAMergeCheckDetail>()
                .Property(e => e.Module)
                .IsUnicode(false);

            modelBuilder.Entity<SAMergeCheckDetail>()
                .Property(e => e.Owner)
                .IsUnicode(false);

            modelBuilder.Entity<SAMergeCheckDetail>()
                .Property(e => e.Status)
                .IsUnicode(false);

            modelBuilder.Entity<SAMergeCustList>()
                .Property(e => e.Status)
                .IsUnicode(false);

            modelBuilder.Entity<SAMergeCustList>()
                .Property(e => e.CreatedUserID)
                .IsUnicode(false);

            modelBuilder.Entity<SAMergeCustList>()
                .Property(e => e.UpdatedUserID)
                .IsUnicode(false);

            modelBuilder.Entity<SAMergeCustList>()
                .Property(e => e.Remark)
                .IsUnicode(false);

            modelBuilder.Entity<SAMergeCustTable>()
                .Property(e => e.Module)
                .IsUnicode(false);

            modelBuilder.Entity<SAMergeCustTable>()
                .Property(e => e.FunctionName)
                .IsUnicode(false);

            modelBuilder.Entity<SAMergeCustTable>()
                .Property(e => e.TableName)
                .IsUnicode(false);

            modelBuilder.Entity<SAMergeCustTable>()
                .Property(e => e.ColumnName)
                .IsUnicode(false);

            modelBuilder.Entity<SAMergeCustTable>()
                .Property(e => e.Owner)
                .IsUnicode(false);

            modelBuilder.Entity<SAMergeCustTable>()
                .Property(e => e.CreatedBY)
                .IsUnicode(false);

            modelBuilder.Entity<SAMergeCustTable>()
                .Property(e => e.UpdatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<SAMergeCustTable>()
                .Property(e => e.Status)
                .IsUnicode(false);

            modelBuilder.Entity<SAMergeCustTable>()
                .Property(e => e.IsReSM)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<SAMergeCustTable>()
                .Property(e => e.Action)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<SAMergeCustTable>()
                .Property(e => e.IsHQ)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<SAMergeCustTable>()
                .Property(e => e.IsCheck)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<SAMergeEffectStation>()
                .Property(e => e.StationID)
                .IsUnicode(false);

            modelBuilder.Entity<SAMergeEffectStation>()
                .Property(e => e.Remark)
                .IsUnicode(false);

            modelBuilder.Entity<SAMergeEffectStation>()
                .Property(e => e.Status)
                .IsUnicode(false);

            modelBuilder.Entity<SAMergeEffectStation>()
                .Property(e => e.UpdatedUserID)
                .IsUnicode(false);

            modelBuilder.Entity<SAMergeExcHistory>()
                .Property(e => e.Status)
                .IsUnicode(false);

            modelBuilder.Entity<SAMergeExcHistory>()
                .Property(e => e.StationID)
                .IsUnicode(false);

            modelBuilder.Entity<SAMergeExcHistory>()
                .Property(e => e.DBName)
                .IsUnicode(false);

            modelBuilder.Entity<SAMergeExcHistory>()
                .Property(e => e.StationIP)
                .IsUnicode(false);

            modelBuilder.Entity<SAMNCPotentialLead>()
                .Property(e => e.ProjectName)
                .IsUnicode(false);

            modelBuilder.Entity<SAMNCPotentialLead>()
                .Property(e => e.ProjectCode)
                .IsUnicode(false);

            modelBuilder.Entity<SAMNCPotentialLead>()
                .Property(e => e.GlobalCode)
                .IsUnicode(false);

            modelBuilder.Entity<SAMNCPotentialLead>()
                .Property(e => e.ProductLine)
                .IsUnicode(false);

            modelBuilder.Entity<SAMNCPotentialLead>()
                .Property(e => e.RegMKT)
                .IsUnicode(false);

            modelBuilder.Entity<SAMNCPotentialLead>()
                .Property(e => e.GrpRep)
                .IsUnicode(false);

            modelBuilder.Entity<SAMNCPotentialLead>()
                .Property(e => e.AccMgr)
                .IsUnicode(false);

            modelBuilder.Entity<SAMNCPotentialLead>()
                .Property(e => e.AssAccMgr1)
                .IsUnicode(false);

            modelBuilder.Entity<SAMNCPotentialLead>()
                .Property(e => e.AssAccMgr2)
                .IsUnicode(false);

            modelBuilder.Entity<SAMNCPotentialLead>()
                .Property(e => e.AssAccMgr3)
                .IsUnicode(false);

            modelBuilder.Entity<SAMNCPotentialLead>()
                .Property(e => e.AssAccMgr4)
                .IsUnicode(false);

            modelBuilder.Entity<SAMNCPotentialLead>()
                .Property(e => e.AssAccMgr5)
                .IsUnicode(false);

            modelBuilder.Entity<SAMNCPotentialLead>()
                .Property(e => e.Assistant)
                .IsUnicode(false);

            modelBuilder.Entity<SAMNCPotentialLead>()
                .Property(e => e.Status)
                .IsUnicode(false);

            modelBuilder.Entity<SAMNCPotentialLead>()
                .Property(e => e.SendBy)
                .IsUnicode(false);

            modelBuilder.Entity<SAMNCPotentialLead>()
                .Property(e => e.ApprovedBy)
                .IsUnicode(false);

            modelBuilder.Entity<SAMNCPotentialLead>()
                .Property(e => e.CreatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<SAMNCPotentialLead>()
                .Property(e => e.UpdatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<SAMNCPotentialLead>()
                .Property(e => e.Version)
                .IsFixedLength();

            modelBuilder.Entity<SAMNCUsed>()
                .Property(e => e.UserID)
                .IsUnicode(false);

            modelBuilder.Entity<SAOneOffCharge>()
                .Property(e => e.ChargeName)
                .IsUnicode(false);

            modelBuilder.Entity<SAOneOffCharge>()
                .Property(e => e.CurrencyCode)
                .IsUnicode(false);

            modelBuilder.Entity<SAOneOffCharge>()
                .Property(e => e.Price)
                .IsUnicode(false);

            modelBuilder.Entity<SAOneOffCharge>()
                .Property(e => e.CreatedUserID)
                .IsUnicode(false);

            modelBuilder.Entity<SAOneOffCharge>()
                .Property(e => e.UpdatedUserID)
                .IsUnicode(false);

            modelBuilder.Entity<SAOneOffShipmentInfo>()
                .Property(e => e.GWT)
                .IsUnicode(false);

            modelBuilder.Entity<SAOneOffShipmentInfo>()
                .Property(e => e.VWT)
                .IsUnicode(false);

            modelBuilder.Entity<SAOneOffShipmentInfo>()
                .Property(e => e.CWT)
                .IsUnicode(false);

            modelBuilder.Entity<SAOneOffShipmentInfo>()
                .Property(e => e.PCS)
                .IsUnicode(false);

            modelBuilder.Entity<SAOneOffShipmentInfo>()
                .Property(e => e.CreatedUserID)
                .IsUnicode(false);

            modelBuilder.Entity<SAOneOffShipmentInfo>()
                .Property(e => e.UpdatedUserID)
                .IsUnicode(false);

            modelBuilder.Entity<SAPotentialLead>()
                .Property(e => e.POSTALCODE)
                .IsUnicode(false);

            modelBuilder.Entity<SAPotentialLead>()
                .Property(e => e.PHONENUMBER)
                .IsUnicode(false);

            modelBuilder.Entity<SAPotentialLead>()
                .Property(e => e.PhoneExt)
                .IsUnicode(false);

            modelBuilder.Entity<SAPotentialLead>()
                .Property(e => e.FAXNUMBER)
                .IsUnicode(false);

            modelBuilder.Entity<SAPotentialLead>()
                .Property(e => e.FaxExt)
                .IsUnicode(false);

            modelBuilder.Entity<SAPotentialLead>()
                .Property(e => e.DUNSNUMBER)
                .IsUnicode(false);

            modelBuilder.Entity<SAPotentialLead>()
                .Property(e => e.SALES)
                .IsUnicode(false);

            modelBuilder.Entity<SAPotentialLead>()
                .Property(e => e.TOTALEMPLOYEES)
                .IsUnicode(false);

            modelBuilder.Entity<SAPotentialLead>()
                .Property(e => e.YEARESTABLISHED)
                .IsUnicode(false);

            modelBuilder.Entity<SAPotentialLead>()
                .Property(e => e.IMPORTPCNT)
                .IsUnicode(false);

            modelBuilder.Entity<SAPotentialLead>()
                .Property(e => e.IMPORTVALUE)
                .IsUnicode(false);

            modelBuilder.Entity<SAPotentialLead>()
                .Property(e => e.IMPORTTONNAGE)
                .IsUnicode(false);

            modelBuilder.Entity<SAPotentialLead>()
                .Property(e => e.IMPORTTEUS)
                .IsUnicode(false);

            modelBuilder.Entity<SAPotentialLead>()
                .Property(e => e.IMPORTSHIPMENTS)
                .IsUnicode(false);

            modelBuilder.Entity<SAPotentialLead>()
                .Property(e => e.IMPORTCOUNTRYSHIPMENTS1)
                .IsUnicode(false);

            modelBuilder.Entity<SAPotentialLead>()
                .Property(e => e.IMPORTCOUNTRYSHIPMENTS2)
                .IsUnicode(false);

            modelBuilder.Entity<SAPotentialLead>()
                .Property(e => e.IMPORTCOUNTRYSHIPMENTS3)
                .IsUnicode(false);

            modelBuilder.Entity<SAPotentialLead>()
                .Property(e => e.IMPORTCOUNTRYSHIPMENTS4)
                .IsUnicode(false);

            modelBuilder.Entity<SAPotentialLead>()
                .Property(e => e.IMPORTCOUNTRYSHIPMENTS5)
                .IsUnicode(false);

            modelBuilder.Entity<SAPotentialLead>()
                .Property(e => e.IMPORTPORTSHIPMENTS1)
                .IsUnicode(false);

            modelBuilder.Entity<SAPotentialLead>()
                .Property(e => e.IMPORTPORTSHIPMENTS2)
                .IsUnicode(false);

            modelBuilder.Entity<SAPotentialLead>()
                .Property(e => e.IMPORTPORTSHIPMENTS3)
                .IsUnicode(false);

            modelBuilder.Entity<SAPotentialLead>()
                .Property(e => e.IMPORTPORTSHIPMENTS4)
                .IsUnicode(false);

            modelBuilder.Entity<SAPotentialLead>()
                .Property(e => e.IMPORTPORTSHIPMENTS5)
                .IsUnicode(false);

            modelBuilder.Entity<SAPotentialLead>()
                .Property(e => e.IMPORTCOMMODITYSHIPMENTS1)
                .IsUnicode(false);

            modelBuilder.Entity<SAPotentialLead>()
                .Property(e => e.IMPORTCOMMODITYSHIPMENTS2)
                .IsUnicode(false);

            modelBuilder.Entity<SAPotentialLead>()
                .Property(e => e.IMPORTCOMMODITYSHIPMENTS3)
                .IsUnicode(false);

            modelBuilder.Entity<SAPotentialLead>()
                .Property(e => e.IMPORTCOMMODITYSHIPMENTS4)
                .IsUnicode(false);

            modelBuilder.Entity<SAPotentialLead>()
                .Property(e => e.IMPORTCOMMODITYSHIPMENTS5)
                .IsUnicode(false);

            modelBuilder.Entity<SAPotentialLead>()
                .Property(e => e.EXPORTPCNT)
                .IsUnicode(false);

            modelBuilder.Entity<SAPotentialLead>()
                .Property(e => e.EXPORTVALUE)
                .IsUnicode(false);

            modelBuilder.Entity<SAPotentialLead>()
                .Property(e => e.EXPORTTONNAGE)
                .IsUnicode(false);

            modelBuilder.Entity<SAPotentialLead>()
                .Property(e => e.EXPORTTEUS)
                .IsUnicode(false);

            modelBuilder.Entity<SAPotentialLead>()
                .Property(e => e.EXPORTSHIPMENTS)
                .IsUnicode(false);

            modelBuilder.Entity<SAPotentialLead>()
                .Property(e => e.EXPORTCOUNTRYSHIPMENTS1)
                .IsUnicode(false);

            modelBuilder.Entity<SAPotentialLead>()
                .Property(e => e.EXPORTCOUNTRYSHIPMENTS2)
                .IsUnicode(false);

            modelBuilder.Entity<SAPotentialLead>()
                .Property(e => e.EXPORTCOUNTRYSHIPMENTS3)
                .IsUnicode(false);

            modelBuilder.Entity<SAPotentialLead>()
                .Property(e => e.EXPORTCOUNTRYSHIPMENTS4)
                .IsUnicode(false);

            modelBuilder.Entity<SAPotentialLead>()
                .Property(e => e.EXPORTCOUNTRYSHIPMENTS5)
                .IsUnicode(false);

            modelBuilder.Entity<SAPotentialLead>()
                .Property(e => e.EXPORTPORTSHIPMENTS1)
                .IsUnicode(false);

            modelBuilder.Entity<SAPotentialLead>()
                .Property(e => e.EXPORTPORTSHIPMENTS2)
                .IsUnicode(false);

            modelBuilder.Entity<SAPotentialLead>()
                .Property(e => e.EXPORTPORTSHIPMENTS3)
                .IsUnicode(false);

            modelBuilder.Entity<SAPotentialLead>()
                .Property(e => e.EXPORTPORTSHIPMENTS4)
                .IsUnicode(false);

            modelBuilder.Entity<SAPotentialLead>()
                .Property(e => e.EXPORTPORTSHIPMENTS5)
                .IsUnicode(false);

            modelBuilder.Entity<SAPotentialLead>()
                .Property(e => e.EXPORTCOMMODITYSHIPMENTS1)
                .IsUnicode(false);

            modelBuilder.Entity<SAPotentialLead>()
                .Property(e => e.EXPORTCOMMODITYSHIPMENTS2)
                .IsUnicode(false);

            modelBuilder.Entity<SAPotentialLead>()
                .Property(e => e.EXPORTCOMMODITYSHIPMENTS3)
                .IsUnicode(false);

            modelBuilder.Entity<SAPotentialLead>()
                .Property(e => e.EXPORTCOMMODITYSHIPMENTS4)
                .IsUnicode(false);

            modelBuilder.Entity<SAPotentialLead>()
                .Property(e => e.EXPORTCOMMODITYSHIPMENTS5)
                .IsUnicode(false);

            modelBuilder.Entity<SAPotentialLead>()
                .Property(e => e.PRIMARYSIC)
                .IsUnicode(false);

            modelBuilder.Entity<SAPotentialLead>()
                .Property(e => e.Status)
                .IsUnicode(false);

            modelBuilder.Entity<SAPotentialLead>()
                .Property(e => e.StationID)
                .IsUnicode(false);

            modelBuilder.Entity<SAPotentialLead>()
                .Property(e => e.OwnerID)
                .IsUnicode(false);

            modelBuilder.Entity<SAPotentialLead>()
                .Property(e => e.CreatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<SAPotentialLead>()
                .Property(e => e.UpdatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<SAPotentialLead>()
                .Property(e => e.AssignedBy)
                .IsUnicode(false);

            modelBuilder.Entity<SAPRFreights>()
                .Property(e => e.Status)
                .IsUnicode(false);

            modelBuilder.Entity<SAPRFreights>()
                .Property(e => e.Type)
                .IsUnicode(false);

            modelBuilder.Entity<SAPRFreights>()
                .Property(e => e.Frequency)
                .IsUnicode(false);

            modelBuilder.Entity<SAPRFreights>()
                .Property(e => e.TransitTime)
                .IsUnicode(false);

            modelBuilder.Entity<SAPRFreights>()
                .Property(e => e.CreatedUserID)
                .IsUnicode(false);

            modelBuilder.Entity<SAPRFreights>()
                .Property(e => e.UpdatedUserID)
                .IsUnicode(false);

            modelBuilder.Entity<SAProcess>()
                .Property(e => e.AppName)
                .IsUnicode(false);

            modelBuilder.Entity<SAProcess>()
                .Property(e => e.Status)
                .IsUnicode(false);

            modelBuilder.Entity<SAProcess>()
                .Property(e => e.CreatedUserID)
                .IsUnicode(false);

            modelBuilder.Entity<SAProcess>()
                .Property(e => e.UpdatedUserID)
                .IsUnicode(false);

            modelBuilder.Entity<SAProcess>()
                .Property(e => e.ServerIP)
                .IsUnicode(false);

            modelBuilder.Entity<SAProductLines>()
                .Property(e => e.Status)
                .IsUnicode(false);

            modelBuilder.Entity<SAProductLines>()
                .Property(e => e.CreatedStationID)
                .IsUnicode(false);

            modelBuilder.Entity<SAProductLines>()
                .Property(e => e.CreatedUserID)
                .IsUnicode(false);

            modelBuilder.Entity<SAProductLines>()
                .Property(e => e.OwnerStationID)
                .IsUnicode(false);

            modelBuilder.Entity<SAProductLines>()
                .Property(e => e.OwnerUserID)
                .IsUnicode(false);

            modelBuilder.Entity<SAProductLines>()
                .Property(e => e.UpdatedUserID)
                .IsUnicode(false);

            modelBuilder.Entity<SAProductLines>()
                .Property(e => e.Secured)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<SAProductLines>()
                .Property(e => e.FLeadOwner)
                .IsUnicode(false);

            modelBuilder.Entity<SAProductLines>()
                .Property(e => e.KeyAccount)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<SAQFExportInfo>()
                .Property(e => e.Type)
                .IsUnicode(false);

            modelBuilder.Entity<SAQFExportInfo>()
                .Property(e => e.CreatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<SAQFExportInfo>()
                .Property(e => e.UpdatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<SAQFMaster>()
                .Property(e => e.Unit)
                .IsUnicode(false);

            modelBuilder.Entity<SAQFMaster>()
                .Property(e => e.CreatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<SAQFMaster>()
                .Property(e => e.UpdatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<SAQFMasterMaillingSetting>()
                .Property(e => e.Type)
                .IsUnicode(false);

            modelBuilder.Entity<SAQFMasterMaillingSetting>()
                .Property(e => e.UserID)
                .IsUnicode(false);

            modelBuilder.Entity<SAQFMasterMaillingSetting>()
                .Property(e => e.CreatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<SAQFMasterMaillingSetting>()
                .Property(e => e.UpdatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<SAQFStationVolume>()
                .Property(e => e.StationID)
                .IsUnicode(false);

            modelBuilder.Entity<SAQFStationVolume>()
                .Property(e => e.CreatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<SAQFStationVolume>()
                .Property(e => e.UpdatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<SAQueryStack>()
                .Property(e => e.UserID)
                .IsUnicode(false);

            modelBuilder.Entity<SAQueryStack>()
                .Property(e => e.QueryStr)
                .IsUnicode(false);

            modelBuilder.Entity<SAQueryStack>()
                .Property(e => e.Views)
                .IsUnicode(false);

            modelBuilder.Entity<SAQueryStack>()
                .Property(e => e.Types)
                .IsUnicode(false);

            modelBuilder.Entity<SAQuoteCharge>()
                .Property(e => e.OrderNo)
                .IsUnicode(false);

            modelBuilder.Entity<SAQuoteCharge>()
                .Property(e => e.ChargeCodeDesc)
                .IsUnicode(false);

            modelBuilder.Entity<SAQuoteCharge>()
                .Property(e => e.CreatedUserID)
                .IsUnicode(false);

            modelBuilder.Entity<SAQuoteCharge>()
                .Property(e => e.UpdatedUserID)
                .IsUnicode(false);

            modelBuilder.Entity<SAQuoteCharge>()
                .Property(e => e.MinCost)
                .IsUnicode(false);

            modelBuilder.Entity<SAQuoteCharge>()
                .Property(e => e.MinProfit)
                .IsUnicode(false);

            modelBuilder.Entity<SAQuoteCharge>()
                .Property(e => e.MinSelling)
                .IsUnicode(false);

            modelBuilder.Entity<SAQuoteCharge>()
                .Property(e => e.FlatCost)
                .IsUnicode(false);

            modelBuilder.Entity<SAQuoteCharge>()
                .Property(e => e.FlatProfit)
                .IsUnicode(false);

            modelBuilder.Entity<SAQuoteCharge>()
                .Property(e => e.FlatSelling)
                .IsUnicode(false);

            modelBuilder.Entity<SAQuoteCharge>()
                .Property(e => e.FLType)
                .IsUnicode(false);

            modelBuilder.Entity<SAQuoteCharge>()
                .Property(e => e.PPCC)
                .IsUnicode(false);

            modelBuilder.Entity<SAQuoteCharge>()
                .Property(e => e.Method)
                .IsUnicode(false);

            modelBuilder.Entity<SAQuoteCharge>()
                .HasMany(e => e.SAQuoteChargeCondition)
                .WithOptional(e => e.SAQuoteCharge)
                .HasForeignKey(e => e.SAChargeID);

            modelBuilder.Entity<SAQuoteCharge>()
                .HasMany(e => e.SAQuoteChargeItem)
                .WithOptional(e => e.SAQuoteCharge)
                .HasForeignKey(e => e.ChargeID);

            modelBuilder.Entity<SAQuoteChargeCity>()
                .Property(e => e.CityType)
                .IsUnicode(false);

            modelBuilder.Entity<SAQuoteChargeCity>()
                .Property(e => e.ChargeType)
                .IsUnicode(false);

            modelBuilder.Entity<SAQuoteChargeCity>()
                .Property(e => e.CreatedUserID)
                .IsUnicode(false);

            modelBuilder.Entity<SAQuoteChargeCity>()
                .Property(e => e.UpdatedUserID)
                .IsUnicode(false);

            modelBuilder.Entity<SAQuoteChargeCity>()
                .Property(e => e.sType)
                .IsUnicode(false);

            modelBuilder.Entity<SAQuoteChargeCity>()
                .Property(e => e.Method)
                .IsUnicode(false);

            modelBuilder.Entity<SAQuoteChargeCity>()
                .HasMany(e => e.SAQuoteCharge)
                .WithOptional(e => e.SAQuoteChargeCity)
                .HasForeignKey(e => e.QuoteCityID);

            modelBuilder.Entity<SAQuoteChargeCondition>()
                .Property(e => e.CreatedUserID)
                .IsUnicode(false);

            modelBuilder.Entity<SAQuoteChargeCondition>()
                .Property(e => e.UpdatedUserID)
                .IsFixedLength();

            modelBuilder.Entity<SAQuoteChargeCondition>()
                .Property(e => e.Remark)
                .IsUnicode(false);

            modelBuilder.Entity<SAQuoteChargeItem>()
                .Property(e => e.Unit)
                .IsUnicode(false);

            modelBuilder.Entity<SAQuoteChargeItem>()
                .Property(e => e.UOMID)
                .IsUnicode(false);

            modelBuilder.Entity<SAQuoteChargeItem>()
                .Property(e => e.Cost)
                .IsUnicode(false);

            modelBuilder.Entity<SAQuoteChargeItem>()
                .Property(e => e.Profit)
                .IsUnicode(false);

            modelBuilder.Entity<SAQuoteChargeItem>()
                .Property(e => e.Selling)
                .IsUnicode(false);

            modelBuilder.Entity<SAQuoteChargeItem>()
                .Property(e => e.CreatedUserID)
                .IsUnicode(false);

            modelBuilder.Entity<SAQuoteChargeItem>()
                .Property(e => e.UpdatedUserID)
                .IsUnicode(false);

            modelBuilder.Entity<SAQuoteChargeItem>()
                .Property(e => e.FLItem)
                .IsUnicode(false);

            modelBuilder.Entity<SAQuoteChargeItem>()
                .Property(e => e.PPCC)
                .IsUnicode(false);

            modelBuilder.Entity<SAQuoteChargeItem>()
                .Property(e => e.Remark)
                .IsUnicode(false);

            modelBuilder.Entity<SAQuoteChargeItem>()
                .HasMany(e => e.SAQuoteFreightHistoricalRate)
                .WithOptional(e => e.SAQuoteChargeItem)
                .HasForeignKey(e => e.ChargeItemID);

            modelBuilder.Entity<SAQuoteContact>()
                .Property(e => e.UserID)
                .IsUnicode(false);

            modelBuilder.Entity<SAQuoteContact>()
                .Property(e => e.UserStation)
                .IsUnicode(false);

            modelBuilder.Entity<SAQuoteContact>()
                .Property(e => e.sType)
                .IsUnicode(false);

            modelBuilder.Entity<SAQuoteContact>()
                .Property(e => e.QuoteNo)
                .IsUnicode(false);

            modelBuilder.Entity<SAQuoteFreightBody>()
                .Property(e => e.Cost1)
                .IsUnicode(false);

            modelBuilder.Entity<SAQuoteFreightBody>()
                .Property(e => e.Cost2)
                .IsUnicode(false);

            modelBuilder.Entity<SAQuoteFreightBody>()
                .Property(e => e.Cost3)
                .IsUnicode(false);

            modelBuilder.Entity<SAQuoteFreightBody>()
                .Property(e => e.Cost4)
                .IsUnicode(false);

            modelBuilder.Entity<SAQuoteFreightBody>()
                .Property(e => e.Cost5)
                .IsUnicode(false);

            modelBuilder.Entity<SAQuoteFreightBody>()
                .Property(e => e.Cost6)
                .IsUnicode(false);

            modelBuilder.Entity<SAQuoteFreightBody>()
                .Property(e => e.Cost7)
                .IsUnicode(false);

            modelBuilder.Entity<SAQuoteFreightBody>()
                .Property(e => e.Cost8)
                .IsUnicode(false);

            modelBuilder.Entity<SAQuoteFreightBody>()
                .Property(e => e.Cost9)
                .IsUnicode(false);

            modelBuilder.Entity<SAQuoteFreightBody>()
                .Property(e => e.Cost10)
                .IsUnicode(false);

            modelBuilder.Entity<SAQuoteFreightBody>()
                .Property(e => e.Profit1)
                .IsUnicode(false);

            modelBuilder.Entity<SAQuoteFreightBody>()
                .Property(e => e.Profit2)
                .IsUnicode(false);

            modelBuilder.Entity<SAQuoteFreightBody>()
                .Property(e => e.Profit3)
                .IsUnicode(false);

            modelBuilder.Entity<SAQuoteFreightBody>()
                .Property(e => e.Profit4)
                .IsUnicode(false);

            modelBuilder.Entity<SAQuoteFreightBody>()
                .Property(e => e.Profit5)
                .IsUnicode(false);

            modelBuilder.Entity<SAQuoteFreightBody>()
                .Property(e => e.Profit6)
                .IsUnicode(false);

            modelBuilder.Entity<SAQuoteFreightBody>()
                .Property(e => e.Profit7)
                .IsUnicode(false);

            modelBuilder.Entity<SAQuoteFreightBody>()
                .Property(e => e.Profit8)
                .IsUnicode(false);

            modelBuilder.Entity<SAQuoteFreightBody>()
                .Property(e => e.Profit9)
                .IsUnicode(false);

            modelBuilder.Entity<SAQuoteFreightBody>()
                .Property(e => e.Profit10)
                .IsUnicode(false);

            modelBuilder.Entity<SAQuoteFreightFSData>()
                .Property(e => e.FSecuredShpt)
                .IsUnicode(false);

            modelBuilder.Entity<SAQuoteFreightFSData>()
                .Property(e => e.FSecuredOwnerUserID)
                .IsUnicode(false);

            modelBuilder.Entity<SAQuoteFreightHistoricalRate>()
                .Property(e => e.isAllQuote)
                .IsFixedLength();

            modelBuilder.Entity<SAQuoteFreights>()
                .Property(e => e.ServiceLevel)
                .IsUnicode(false);

            modelBuilder.Entity<SAQuoteFreights>()
                .Property(e => e.WeeklyAllotment)
                .IsUnicode(false);

            modelBuilder.Entity<SAQuoteFreights>()
                .Property(e => e.Frequency)
                .IsUnicode(false);

            modelBuilder.Entity<SAQuoteFreights>()
                .Property(e => e.TransitTime)
                .IsUnicode(false);

            modelBuilder.Entity<SAQuoteFreights>()
                .Property(e => e.SellingRate1)
                .IsUnicode(false);

            modelBuilder.Entity<SAQuoteFreights>()
                .Property(e => e.SellingRate2)
                .IsUnicode(false);

            modelBuilder.Entity<SAQuoteFreights>()
                .Property(e => e.SellingRate3)
                .IsUnicode(false);

            modelBuilder.Entity<SAQuoteFreights>()
                .Property(e => e.SellingRate4)
                .IsUnicode(false);

            modelBuilder.Entity<SAQuoteFreights>()
                .Property(e => e.SellingRate5)
                .IsUnicode(false);

            modelBuilder.Entity<SAQuoteFreights>()
                .Property(e => e.SellingRate6)
                .IsUnicode(false);

            modelBuilder.Entity<SAQuoteFreights>()
                .Property(e => e.SellingRate7)
                .IsUnicode(false);

            modelBuilder.Entity<SAQuoteFreights>()
                .Property(e => e.SellingRate8)
                .IsUnicode(false);

            modelBuilder.Entity<SAQuoteFreights>()
                .Property(e => e.SellingRate9)
                .IsUnicode(false);

            modelBuilder.Entity<SAQuoteFreights>()
                .Property(e => e.SellingRate10)
                .IsUnicode(false);

            modelBuilder.Entity<SAQuoteFreights>()
                .Property(e => e.Commodity)
                .IsUnicode(false);

            modelBuilder.Entity<SAQuoteFreights>()
                .Property(e => e.CreatedUserID)
                .IsUnicode(false);

            modelBuilder.Entity<SAQuoteFreights>()
                .Property(e => e.UpdatedUserID)
                .IsUnicode(false);

            modelBuilder.Entity<SAQuoteFreights>()
                .Property(e => e.OtherValue1)
                .IsUnicode(false);

            modelBuilder.Entity<SAQuoteFreights>()
                .Property(e => e.OtherValue2)
                .IsUnicode(false);

            modelBuilder.Entity<SAQuoteFreights>()
                .Property(e => e.OtherValue3)
                .IsUnicode(false);

            modelBuilder.Entity<SAQuoteFreights>()
                .Property(e => e.EIType)
                .IsUnicode(false);

            modelBuilder.Entity<SAQuoteFreights>()
                .Property(e => e.PPCC)
                .IsUnicode(false);

            modelBuilder.Entity<SAQuoteFreights>()
                .Property(e => e.NatureOfGoods)
                .IsUnicode(false);

            modelBuilder.Entity<SAQuoteFreights>()
                .Property(e => e.FLeadOwner)
                .IsUnicode(false);

            modelBuilder.Entity<SAQuoteFreights>()
                .Property(e => e.EstTonnage)
                .IsUnicode(false);

            modelBuilder.Entity<SAQuoteFreights>()
                .Property(e => e.EstRevenue)
                .IsUnicode(false);

            modelBuilder.Entity<SAQuoteFreights>()
                .Property(e => e.EstGP)
                .IsUnicode(false);

            modelBuilder.Entity<SAQuoteFreights>()
                .Property(e => e.FShptNo)
                .IsUnicode(false);

            modelBuilder.Entity<SAQuoteFreights>()
                .Property(e => e.FStationId)
                .IsUnicode(false);

            modelBuilder.Entity<SAQuoteFreights>()
                .Property(e => e.FLotNo)
                .IsUnicode(false);

            modelBuilder.Entity<SAQuoteFreights>()
                .Property(e => e.CBMToWT)
                .IsUnicode(false);

            modelBuilder.Entity<SAQuoteFreights>()
                .HasMany(e => e.SAQuoteFreightBody)
                .WithOptional(e => e.SAQuoteFreights)
                .HasForeignKey(e => e.FID);

            modelBuilder.Entity<SAQuoteFreights>()
                .HasMany(e => e.SAQuoteFreightFSData)
                .WithRequired(e => e.SAQuoteFreights)
                .HasForeignKey(e => e.QuoteFreightID)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<SAQuotePickupDelivery>()
                .Property(e => e.ContactPhone)
                .IsUnicode(false);

            modelBuilder.Entity<SAQuotePickupDelivery>()
                .Property(e => e.PD)
                .IsUnicode(false);

            modelBuilder.Entity<SAQuotes>()
                .Property(e => e.QuoteNo)
                .IsUnicode(false);

            modelBuilder.Entity<SAQuotes>()
                .Property(e => e.Status)
                .IsUnicode(false);

            modelBuilder.Entity<SAQuotes>()
                .Property(e => e.AttentionTo)
                .IsUnicode(false);

            modelBuilder.Entity<SAQuotes>()
                .Property(e => e.CreditLimitAmount)
                .IsUnicode(false);

            modelBuilder.Entity<SAQuotes>()
                .Property(e => e.BookMark)
                .IsUnicode(false);

            modelBuilder.Entity<SAQuotes>()
                .Property(e => e.FRHeaderBody)
                .IsUnicode(false);

            modelBuilder.Entity<SAQuotes>()
                .Property(e => e.ApprovalInfo)
                .IsUnicode(false);

            modelBuilder.Entity<SAQuotes>()
                .Property(e => e.AcceptInfo)
                .IsUnicode(false);

            modelBuilder.Entity<SAQuotes>()
                .Property(e => e.CreatedUserID)
                .IsUnicode(false);

            modelBuilder.Entity<SAQuotes>()
                .Property(e => e.UpdatedUserID)
                .IsUnicode(false);

            modelBuilder.Entity<SAQuotes>()
                .Property(e => e.sType)
                .IsUnicode(false);

            modelBuilder.Entity<SAQuotes>()
                .Property(e => e.OnePWD)
                .IsUnicode(false);

            modelBuilder.Entity<SAQuotes>()
                .Property(e => e.Title1)
                .IsUnicode(false);

            modelBuilder.Entity<SAQuotes>()
                .Property(e => e.Title2)
                .IsUnicode(false);

            modelBuilder.Entity<SAQuotes>()
                .Property(e => e.Title3)
                .IsUnicode(false);

            modelBuilder.Entity<SAQuotes>()
                .Property(e => e.Title4)
                .IsUnicode(false);

            modelBuilder.Entity<SAQuotes>()
                .Property(e => e.Title5)
                .IsUnicode(false);

            modelBuilder.Entity<SAQuotes>()
                .Property(e => e.Title6)
                .IsUnicode(false);

            modelBuilder.Entity<SAQuotes>()
                .Property(e => e.Title7)
                .IsUnicode(false);

            modelBuilder.Entity<SAQuotes>()
                .Property(e => e.Title8)
                .IsUnicode(false);

            modelBuilder.Entity<SAQuotes>()
                .Property(e => e.Title9)
                .IsUnicode(false);

            modelBuilder.Entity<SAQuotes>()
                .Property(e => e.Title10)
                .IsUnicode(false);

            modelBuilder.Entity<SAQuotes>()
                .Property(e => e.BKDescription)
                .IsUnicode(false);

            modelBuilder.Entity<SAQuotes>()
                .Property(e => e.ProjectExplanation)
                .IsUnicode(false);

            modelBuilder.Entity<SAQuotes>()
                .Property(e => e.PrevailRate)
                .IsUnicode(false);

            modelBuilder.Entity<SAQuotes>()
                .Property(e => e.PrevailRateFlag)
                .IsUnicode(false);

            modelBuilder.Entity<SAQuotes>()
                .Property(e => e.StationID)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<SAQuotes>()
                .Property(e => e.NRA)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<SAQuotes>()
                .Property(e => e.MoveType)
                .IsUnicode(false);

            modelBuilder.Entity<SAQuotes>()
                .HasMany(e => e.SAQuoteChargeCity)
                .WithOptional(e => e.SAQuotes)
                .HasForeignKey(e => e.QuoteID);

            modelBuilder.Entity<SAQuotes>()
                .HasMany(e => e.SAQuoteContact)
                .WithOptional(e => e.SAQuotes)
                .HasForeignKey(e => e.QuoteID);

            modelBuilder.Entity<SAQuotes>()
                .HasMany(e => e.SAQuoteFreights)
                .WithOptional(e => e.SAQuotes)
                .HasForeignKey(e => e.QuoteID);

            modelBuilder.Entity<SAQuotes>()
                .HasMany(e => e.SAQuotePickupDelivery)
                .WithOptional(e => e.SAQuotes)
                .HasForeignKey(e => e.QuoteID);

            modelBuilder.Entity<SAQuotes>()
                .HasMany(e => e.SAQuoteSection)
                .WithOptional(e => e.SAQuotes)
                .HasForeignKey(e => e.QuoteID);

            modelBuilder.Entity<SAQuotes>()
                .HasMany(e => e.SAQuoteTerms)
                .WithOptional(e => e.SAQuotes)
                .HasForeignKey(e => e.QuoteID);

            modelBuilder.Entity<SAQuotes>()
                .HasMany(e => e.SAQuoteToolFreights)
                .WithOptional(e => e.SAQuotes)
                .HasForeignKey(e => e.QuoteID);

            modelBuilder.Entity<SAQuotes>()
                .HasMany(e => e.SAQuoteToolLocalChargeCity)
                .WithRequired(e => e.SAQuotes)
                .HasForeignKey(e => e.QuoteID)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<SAQuotes>()
                .HasMany(e => e.SAQuoteToolSurcharges)
                .WithOptional(e => e.SAQuotes)
                .HasForeignKey(e => e.QuoteID);

            modelBuilder.Entity<SAQuotes>()
                .HasMany(e => e.SATermOfShipping)
                .WithRequired(e => e.SAQuotes)
                .HasForeignKey(e => e.QuoteID)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<SAQuoteSection>()
                .Property(e => e.QuoteNo)
                .IsUnicode(false);

            modelBuilder.Entity<SAQuoteSection>()
                .Property(e => e.Status)
                .IsUnicode(false);

            modelBuilder.Entity<SAQuoteSection>()
                .Property(e => e.CreatedStationID)
                .IsUnicode(false);

            modelBuilder.Entity<SAQuoteSection>()
                .Property(e => e.CreatedUserID)
                .IsUnicode(false);

            modelBuilder.Entity<SAQuoteSection>()
                .Property(e => e.Section1Break)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<SAQuoteSection>()
                .Property(e => e.Section2Break)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<SAQuoteSection>()
                .Property(e => e.Section3Break)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<SAQuoteSection>()
                .Property(e => e.ShowArea1)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<SAQuoteSection>()
                .Property(e => e.ShowArea2)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<SAQuoteSection>()
                .Property(e => e.ShowArea3)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<SAQuoteSection>()
                .Property(e => e.DocName)
                .IsFixedLength();

            modelBuilder.Entity<SAQuoteSection>()
                .Property(e => e.DocUrl)
                .IsFixedLength();

            modelBuilder.Entity<SAQuoteSection>()
                .Property(e => e.Section4Break)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<SAQuoteSection>()
                .Property(e => e.ShowArea4)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<SAQuoteTerms>()
                .Property(e => e.IsSelected)
                .IsUnicode(false);

            modelBuilder.Entity<SAQuoteTerms>()
                .Property(e => e.QuoteNo)
                .IsUnicode(false);

            modelBuilder.Entity<SAQuoteToolFreights>()
                .Property(e => e.PlaceOfReceipt)
                .IsUnicode(false);

            modelBuilder.Entity<SAQuoteToolFreights>()
                .Property(e => e.PortOfLoad)
                .IsUnicode(false);

            modelBuilder.Entity<SAQuoteToolFreights>()
                .Property(e => e.PortOfDischarge)
                .IsUnicode(false);

            modelBuilder.Entity<SAQuoteToolFreights>()
                .Property(e => e.PlaceOfDelivery)
                .IsUnicode(false);

            modelBuilder.Entity<SAQuoteToolFreights>()
                .Property(e => e.CarrierCode)
                .IsUnicode(false);

            modelBuilder.Entity<SAQuoteToolFreights>()
                .Property(e => e.TransitTime)
                .IsUnicode(false);

            modelBuilder.Entity<SAQuoteToolFreights>()
                .Property(e => e.MIN)
                .IsUnicode(false);

            modelBuilder.Entity<SAQuoteToolFreights>()
                .Property(e => e.CBM)
                .IsUnicode(false);

            modelBuilder.Entity<SAQuoteToolFreights>()
                .Property(e => e.TON)
                .IsUnicode(false);

            modelBuilder.Entity<SAQuoteToolFreights>()
                .Property(e => e.SellingRate1)
                .IsUnicode(false);

            modelBuilder.Entity<SAQuoteToolFreights>()
                .Property(e => e.SellingRate2)
                .IsUnicode(false);

            modelBuilder.Entity<SAQuoteToolFreights>()
                .Property(e => e.SellingRate3)
                .IsUnicode(false);

            modelBuilder.Entity<SAQuoteToolFreights>()
                .Property(e => e.SellingRate4)
                .IsUnicode(false);

            modelBuilder.Entity<SAQuoteToolFreights>()
                .Property(e => e.SellingRate5)
                .IsUnicode(false);

            modelBuilder.Entity<SAQuoteToolFreights>()
                .Property(e => e.SellingRate6)
                .IsUnicode(false);

            modelBuilder.Entity<SAQuoteToolFreights>()
                .Property(e => e.SellingRate7)
                .IsUnicode(false);

            modelBuilder.Entity<SAQuoteToolFreights>()
                .Property(e => e.SellingRate8)
                .IsUnicode(false);

            modelBuilder.Entity<SAQuoteToolFreights>()
                .Property(e => e.SellingRate9)
                .IsUnicode(false);

            modelBuilder.Entity<SAQuoteToolFreights>()
                .Property(e => e.SellingRate10)
                .IsUnicode(false);

            modelBuilder.Entity<SAQuoteToolFreights>()
                .Property(e => e.Commodity)
                .IsUnicode(false);

            modelBuilder.Entity<SAQuoteToolFreights>()
                .Property(e => e.CreatedUserID)
                .IsUnicode(false);

            modelBuilder.Entity<SAQuoteToolFreights>()
                .Property(e => e.UpdatedUserID)
                .IsUnicode(false);

            modelBuilder.Entity<SAQuoteToolFreights>()
                .Property(e => e.ServiceLevel)
                .IsUnicode(false);

            modelBuilder.Entity<SAQuoteToolFreights>()
                .Property(e => e.sType)
                .IsUnicode(false);

            modelBuilder.Entity<SAQuoteToolFreights>()
                .Property(e => e.CurrencyCode)
                .IsUnicode(false);

            modelBuilder.Entity<SAQuoteToolLocalCharge>()
                .Property(e => e.Min)
                .IsUnicode(false);

            modelBuilder.Entity<SAQuoteToolLocalCharge>()
                .Property(e => e.Filling)
                .IsUnicode(false);

            modelBuilder.Entity<SAQuoteToolLocalCharge>()
                .Property(e => e.CBM)
                .IsUnicode(false);

            modelBuilder.Entity<SAQuoteToolLocalCharge>()
                .Property(e => e.TON)
                .IsUnicode(false);

            modelBuilder.Entity<SAQuoteToolLocalCharge>()
                .Property(e => e.SellingRate1)
                .IsUnicode(false);

            modelBuilder.Entity<SAQuoteToolLocalCharge>()
                .Property(e => e.SellingRate2)
                .IsUnicode(false);

            modelBuilder.Entity<SAQuoteToolLocalCharge>()
                .Property(e => e.SellingRate3)
                .IsUnicode(false);

            modelBuilder.Entity<SAQuoteToolLocalCharge>()
                .Property(e => e.SellingRate4)
                .IsUnicode(false);

            modelBuilder.Entity<SAQuoteToolLocalCharge>()
                .Property(e => e.SellingRate5)
                .IsUnicode(false);

            modelBuilder.Entity<SAQuoteToolLocalCharge>()
                .Property(e => e.SellingRate6)
                .IsUnicode(false);

            modelBuilder.Entity<SAQuoteToolLocalCharge>()
                .Property(e => e.SellingRate7)
                .IsUnicode(false);

            modelBuilder.Entity<SAQuoteToolLocalCharge>()
                .Property(e => e.SellingRate8)
                .IsUnicode(false);

            modelBuilder.Entity<SAQuoteToolLocalCharge>()
                .Property(e => e.SellingRate9)
                .IsUnicode(false);

            modelBuilder.Entity<SAQuoteToolLocalCharge>()
                .Property(e => e.SellingRate10)
                .IsUnicode(false);

            modelBuilder.Entity<SAQuoteToolLocalCharge>()
                .Property(e => e.UpdatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<SAQuoteToolLocalCharge>()
                .Property(e => e.CreatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<SAQuoteToolLocalCharge>()
                .Property(e => e.ChargeCode)
                .IsUnicode(false);

            modelBuilder.Entity<SAQuoteToolLocalCharge>()
                .Property(e => e.SubChargeCode)
                .IsUnicode(false);

            modelBuilder.Entity<SAQuoteToolLocalChargeCity>()
                .Property(e => e.CityCodes)
                .IsUnicode(false);

            modelBuilder.Entity<SAQuoteToolLocalChargeCity>()
                .Property(e => e.CurencyCode)
                .IsUnicode(false);

            modelBuilder.Entity<SAQuoteToolLocalChargeCity>()
                .Property(e => e.UOMCode)
                .IsUnicode(false);

            modelBuilder.Entity<SAQuoteToolLocalChargeCity>()
                .Property(e => e.Mode)
                .IsUnicode(false);

            modelBuilder.Entity<SAQuoteToolLocalChargeCity>()
                .Property(e => e.WeightBreak1)
                .IsUnicode(false);

            modelBuilder.Entity<SAQuoteToolLocalChargeCity>()
                .Property(e => e.WeightBreak2)
                .IsUnicode(false);

            modelBuilder.Entity<SAQuoteToolLocalChargeCity>()
                .Property(e => e.WeightBreak3)
                .IsUnicode(false);

            modelBuilder.Entity<SAQuoteToolLocalChargeCity>()
                .Property(e => e.WeightBreak4)
                .IsUnicode(false);

            modelBuilder.Entity<SAQuoteToolLocalChargeCity>()
                .Property(e => e.WeightBreak5)
                .IsUnicode(false);

            modelBuilder.Entity<SAQuoteToolLocalChargeCity>()
                .Property(e => e.WeightBreak6)
                .IsUnicode(false);

            modelBuilder.Entity<SAQuoteToolLocalChargeCity>()
                .Property(e => e.WeightBreak7)
                .IsUnicode(false);

            modelBuilder.Entity<SAQuoteToolLocalChargeCity>()
                .Property(e => e.WeightBreak8)
                .IsUnicode(false);

            modelBuilder.Entity<SAQuoteToolLocalChargeCity>()
                .Property(e => e.WeightBreak9)
                .IsUnicode(false);

            modelBuilder.Entity<SAQuoteToolLocalChargeCity>()
                .Property(e => e.WeightBreak10)
                .IsUnicode(false);

            modelBuilder.Entity<SAQuoteToolLocalChargeCity>()
                .Property(e => e.UpdatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<SAQuoteToolLocalChargeCity>()
                .Property(e => e.CreatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<SAQuoteToolLocalChargeCity>()
                .HasMany(e => e.SAQuoteToolLocalCharge)
                .WithRequired(e => e.SAQuoteToolLocalChargeCity)
                .HasForeignKey(e => e.MasterID)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<SAQuoteToolLocalChargeCity>()
                .HasMany(e => e.SAQuoteToolLocalChargeFee)
                .WithRequired(e => e.SAQuoteToolLocalChargeCity)
                .HasForeignKey(e => e.MasterID)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<SAQuoteToolLocalChargeFee>()
                .Property(e => e.CityCode)
                .IsUnicode(false);

            modelBuilder.Entity<SAQuoteToolLocalChargeFee>()
                .Property(e => e.Min)
                .IsUnicode(false);

            modelBuilder.Entity<SAQuoteToolLocalChargeFee>()
                .Property(e => e.SellingRate1)
                .IsUnicode(false);

            modelBuilder.Entity<SAQuoteToolLocalChargeFee>()
                .Property(e => e.SellingRate2)
                .IsUnicode(false);

            modelBuilder.Entity<SAQuoteToolLocalChargeFee>()
                .Property(e => e.SellingRate3)
                .IsUnicode(false);

            modelBuilder.Entity<SAQuoteToolLocalChargeFee>()
                .Property(e => e.SellingRate4)
                .IsUnicode(false);

            modelBuilder.Entity<SAQuoteToolLocalChargeFee>()
                .Property(e => e.SellingRate5)
                .IsUnicode(false);

            modelBuilder.Entity<SAQuoteToolLocalChargeFee>()
                .Property(e => e.SellingRate6)
                .IsUnicode(false);

            modelBuilder.Entity<SAQuoteToolLocalChargeFee>()
                .Property(e => e.SellingRate7)
                .IsUnicode(false);

            modelBuilder.Entity<SAQuoteToolLocalChargeFee>()
                .Property(e => e.SellingRate8)
                .IsUnicode(false);

            modelBuilder.Entity<SAQuoteToolLocalChargeFee>()
                .Property(e => e.SellingRate9)
                .IsUnicode(false);

            modelBuilder.Entity<SAQuoteToolLocalChargeFee>()
                .Property(e => e.SellingRate10)
                .IsUnicode(false);

            modelBuilder.Entity<SAQuoteToolLocalChargeFee>()
                .Property(e => e.FueSurcharge)
                .IsUnicode(false);

            modelBuilder.Entity<SAQuoteToolLocalChargeFee>()
                .Property(e => e.UpdatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<SAQuoteToolLocalChargeFee>()
                .Property(e => e.CreatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<SAQuoteToolSurcharges>()
                .Property(e => e.MIN)
                .IsUnicode(false);

            modelBuilder.Entity<SAQuoteToolSurcharges>()
                .Property(e => e.CBM)
                .IsUnicode(false);

            modelBuilder.Entity<SAQuoteToolSurcharges>()
                .Property(e => e.TON)
                .IsUnicode(false);

            modelBuilder.Entity<SAQuoteToolSurcharges>()
                .Property(e => e.SetFiling)
                .IsUnicode(false);

            modelBuilder.Entity<SAQuoteToolSurcharges>()
                .Property(e => e.SellingRate1)
                .IsUnicode(false);

            modelBuilder.Entity<SAQuoteToolSurcharges>()
                .Property(e => e.SellingRate2)
                .IsUnicode(false);

            modelBuilder.Entity<SAQuoteToolSurcharges>()
                .Property(e => e.SellingRate3)
                .IsUnicode(false);

            modelBuilder.Entity<SAQuoteToolSurcharges>()
                .Property(e => e.SellingRate4)
                .IsUnicode(false);

            modelBuilder.Entity<SAQuoteToolSurcharges>()
                .Property(e => e.SellingRate5)
                .IsUnicode(false);

            modelBuilder.Entity<SAQuoteToolSurcharges>()
                .Property(e => e.SellingRate6)
                .IsUnicode(false);

            modelBuilder.Entity<SAQuoteToolSurcharges>()
                .Property(e => e.SellingRate7)
                .IsUnicode(false);

            modelBuilder.Entity<SAQuoteToolSurcharges>()
                .Property(e => e.SellingRate8)
                .IsUnicode(false);

            modelBuilder.Entity<SAQuoteToolSurcharges>()
                .Property(e => e.SellingRate9)
                .IsUnicode(false);

            modelBuilder.Entity<SAQuoteToolSurcharges>()
                .Property(e => e.SellingRate10)
                .IsUnicode(false);

            modelBuilder.Entity<SAQuoteToolSurcharges>()
                .Property(e => e.CreatedUserID)
                .IsUnicode(false);

            modelBuilder.Entity<SAQuoteToolSurcharges>()
                .Property(e => e.UpdatedUserID)
                .IsUnicode(false);

            modelBuilder.Entity<SAQuoteToolSurcharges>()
                .Property(e => e.sType)
                .IsUnicode(false);

            modelBuilder.Entity<SAQuoteToolSurcharges>()
                .Property(e => e.Remarks)
                .IsUnicode(false);

            modelBuilder.Entity<SAQuoteToolSurcharges>()
                .Property(e => e.LaneSegment)
                .IsUnicode(false);

            modelBuilder.Entity<SAQuoteToolSurcharges>()
                .Property(e => e.ChargeCode)
                .IsUnicode(false);

            modelBuilder.Entity<SAQuoteToolSurcharges>()
                .Property(e => e.CurrencyCode)
                .IsUnicode(false);

            modelBuilder.Entity<SAQuoteToolSurcharges>()
                .Property(e => e.SubChargeCode)
                .IsUnicode(false);

            modelBuilder.Entity<SARecentlyStack>()
                .Property(e => e.CreatedUserid)
                .IsUnicode(false);

            modelBuilder.Entity<SARecentlyStack>()
                .Property(e => e.SourceType)
                .IsUnicode(false);

            modelBuilder.Entity<SARename>()
                .Property(e => e.eSupportNo)
                .IsUnicode(false);

            modelBuilder.Entity<SARename>()
                .Property(e => e.NewName)
                .IsUnicode(false);

            modelBuilder.Entity<SARename>()
                .Property(e => e.CreatedUserID)
                .IsUnicode(false);

            modelBuilder.Entity<SARename>()
                .Property(e => e.Status)
                .IsUnicode(false);

            modelBuilder.Entity<SASalesRoutes>()
                .Property(e => e.Duty1)
                .IsUnicode(false);

            modelBuilder.Entity<SASalesRoutes>()
                .Property(e => e.Duty2)
                .IsUnicode(false);

            modelBuilder.Entity<SASalesRoutes>()
                .Property(e => e.CreatedUserID)
                .IsUnicode(false);

            modelBuilder.Entity<SASalesRoutes>()
                .Property(e => e.UpdatedUserID)
                .IsUnicode(false);

            modelBuilder.Entity<SASalesTask>()
                .Property(e => e.AssignTo)
                .IsUnicode(false);

            modelBuilder.Entity<SASalesTask>()
                .Property(e => e.Attendees)
                .IsUnicode(false);

            modelBuilder.Entity<SASalesTask>()
                .Property(e => e.Status)
                .IsUnicode(false);

            modelBuilder.Entity<SASalesTask>()
                .Property(e => e.LogType)
                .IsUnicode(false);

            modelBuilder.Entity<SASalesTask>()
                .Property(e => e.Priority)
                .IsUnicode(false);

            modelBuilder.Entity<SASalesTask>()
                .Property(e => e.TaskOwnerID)
                .IsUnicode(false);

            modelBuilder.Entity<SASalesTask>()
                .Property(e => e.AttendeeContact)
                .IsUnicode(false);

            modelBuilder.Entity<SASalesTask>()
                .Property(e => e.CreatedUserID)
                .IsUnicode(false);

            modelBuilder.Entity<SASalesTask>()
                .Property(e => e.UpdatedUserID)
                .IsUnicode(false);

            modelBuilder.Entity<SASalesTask>()
                .Property(e => e.SubjectType)
                .IsUnicode(false);

            modelBuilder.Entity<SASalesTask>()
                .HasMany(e => e.SASalesTaskFeedback)
                .WithRequired(e => e.SASalesTask)
                .HasForeignKey(e => e.taskID)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<SASalesTask>()
                .HasMany(e => e.SASalesTaskPL)
                .WithOptional(e => e.SASalesTask)
                .HasForeignKey(e => e.SalesTaskID);

            modelBuilder.Entity<SASalesTask>()
                .HasMany(e => e.SASalesTaskSigned)
                .WithRequired(e => e.SASalesTask)
                .HasForeignKey(e => e.taskID)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<SASalesTaskFeedback>()
                .Property(e => e.customerType)
                .IsUnicode(false);

            modelBuilder.Entity<SASalesTaskFeedback>()
                .Property(e => e.Step1ItemID)
                .IsUnicode(false);

            modelBuilder.Entity<SASalesTaskFeedback>()
                .Property(e => e.Step2ItemID)
                .IsUnicode(false);

            modelBuilder.Entity<SASalesTaskFeedback>()
                .Property(e => e.Step3ItemID)
                .IsUnicode(false);

            modelBuilder.Entity<SASalesTaskFeedback>()
                .Property(e => e.Step4ItemID)
                .IsUnicode(false);

            modelBuilder.Entity<SASalesTaskFeedback>()
                .Property(e => e.Status)
                .IsUnicode(false);

            modelBuilder.Entity<SASalesTaskFeedback>()
                .Property(e => e.CreateBy)
                .IsUnicode(false);

            modelBuilder.Entity<SASalesTaskFeedback>()
                .Property(e => e.UpdatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<SASalesTaskPL>()
                .Property(e => e.CreatedUserID)
                .IsUnicode(false);

            modelBuilder.Entity<SASalesTaskPL>()
                .Property(e => e.UpdatedUserID)
                .IsUnicode(false);

            modelBuilder.Entity<SASalesTaskPL>()
                .Property(e => e.Status)
                .IsUnicode(false);

            modelBuilder.Entity<SASalesTaskSigned>()
                .Property(e => e.CreateBy)
                .IsUnicode(false);

            modelBuilder.Entity<SASalesTaskSigned>()
                .Property(e => e.UpdatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<SASCCode>()
                .Property(e => e.Type)
                .IsUnicode(false);

            modelBuilder.Entity<SASCCode>()
                .Property(e => e.Status)
                .IsUnicode(false);

            modelBuilder.Entity<SASCCode>()
                .Property(e => e.CreatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<SASCCode>()
                .Property(e => e.UpdatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<SASCCode>()
                .HasMany(e => e.SASCDefinition)
                .WithOptional(e => e.SASCCode)
                .HasForeignKey(e => e.DefinitionTypeID);

            modelBuilder.Entity<SASCCode>()
                .HasMany(e => e.SASCItems)
                .WithOptional(e => e.SASCCode)
                .HasForeignKey(e => e.ItemCategoryID);

            modelBuilder.Entity<SASCCode>()
                .HasMany(e => e.SASCItems1)
                .WithOptional(e => e.SASCCode1)
                .HasForeignKey(e => e.CaculateTypeID);

            modelBuilder.Entity<SASCCode>()
                .HasMany(e => e.SASCItems2)
                .WithOptional(e => e.SASCCode2)
                .HasForeignKey(e => e.ItemTypeID);

            modelBuilder.Entity<SASCCode>()
                .HasMany(e => e.SASCItems3)
                .WithOptional(e => e.SASCCode3)
                .HasForeignKey(e => e.CustomerTypeID);

            modelBuilder.Entity<SASCDefinition>()
                .Property(e => e.PartyType)
                .IsUnicode(false);

            modelBuilder.Entity<SASCDefinition>()
                .Property(e => e.PartyCode)
                .IsUnicode(false);

            modelBuilder.Entity<SASCDefinition>()
                .Property(e => e.PartyID)
                .IsUnicode(false);

            modelBuilder.Entity<SASCDefinition>()
                .Property(e => e.ValueUnit)
                .IsUnicode(false);

            modelBuilder.Entity<SASCDefinition>()
                .Property(e => e.Status)
                .IsUnicode(false);

            modelBuilder.Entity<SASCDefinition>()
                .Property(e => e.CreatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<SASCDefinition>()
                .Property(e => e.UpdatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<SASCGroup>()
                .Property(e => e.GroupType)
                .IsUnicode(false);

            modelBuilder.Entity<SASCGroup>()
                .Property(e => e.GroupCode)
                .IsUnicode(false);

            modelBuilder.Entity<SASCGroup>()
                .Property(e => e.GroupSourceID)
                .IsUnicode(false);

            modelBuilder.Entity<SASCGroup>()
                .Property(e => e.GroupPSourceID)
                .IsUnicode(false);

            modelBuilder.Entity<SASCGroup>()
                .Property(e => e.Status)
                .IsUnicode(false);

            modelBuilder.Entity<SASCGroup>()
                .Property(e => e.CreatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<SASCGroup>()
                .Property(e => e.UpdatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<SASCGroup>()
                .HasMany(e => e.SASCItems)
                .WithOptional(e => e.SASCGroup)
                .HasForeignKey(e => e.GroupID);

            modelBuilder.Entity<SASCGroup>()
                .HasMany(e => e.SASCUserGroup)
                .WithOptional(e => e.SASCGroup)
                .HasForeignKey(e => e.GroupID);

            modelBuilder.Entity<SASCItems>()
                .Property(e => e.ModeType)
                .IsUnicode(false);

            modelBuilder.Entity<SASCItems>()
                .Property(e => e.Status)
                .IsUnicode(false);

            modelBuilder.Entity<SASCItems>()
                .Property(e => e.CreatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<SASCItems>()
                .Property(e => e.UpdatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<SASCScoreCard>()
                .Property(e => e.UserID)
                .IsUnicode(false);

            modelBuilder.Entity<SASCScoreCard>()
                .Property(e => e.ModeType)
                .IsUnicode(false);

            modelBuilder.Entity<SASCScoreCard>()
                .Property(e => e.HomeCurrency)
                .IsUnicode(false);

            modelBuilder.Entity<SASCScoreCard>()
                .Property(e => e.CreatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<SASCScoreCard>()
                .Property(e => e.UpdatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<SASCScoreCard>()
                .Property(e => e.Status)
                .IsUnicode(false);

            modelBuilder.Entity<SASCScoreCard>()
                .Property(e => e.ScoreTarget)
                .IsUnicode(false);

            modelBuilder.Entity<SASCScoreCard>()
                .Property(e => e.rate)
                .IsUnicode(false);

            modelBuilder.Entity<SASCScoreRules>()
                .Property(e => e.Status)
                .IsUnicode(false);

            modelBuilder.Entity<SASCScoreRules>()
                .Property(e => e.CreatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<SASCScoreRules>()
                .Property(e => e.UpdatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<SASCScoreRules>()
                .HasMany(e => e.SASCItems)
                .WithOptional(e => e.SASCScoreRules)
                .HasForeignKey(e => e.ScoreRulesID);

            modelBuilder.Entity<SASCUserGroup>()
                .Property(e => e.UserID)
                .IsUnicode(false);

            modelBuilder.Entity<SASCUserGroup>()
                .Property(e => e.Status)
                .IsUnicode(false);

            modelBuilder.Entity<SASCUserGroup>()
                .Property(e => e.CreatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<SASCUserGroup>()
                .Property(e => e.UpdatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<SASpecialMapping>()
                .Property(e => e.UserID)
                .IsUnicode(false);

            modelBuilder.Entity<SASpecialMapping>()
                .Property(e => e.StationID)
                .IsUnicode(false);

            modelBuilder.Entity<SASpecialMapping>()
                .Property(e => e.CreatedUserID)
                .IsUnicode(false);

            modelBuilder.Entity<SASpecialMapping>()
                .Property(e => e.UpdatedUserID)
                .IsUnicode(false);

            modelBuilder.Entity<SAStationApp>()
                .Property(e => e.StationID)
                .IsUnicode(false);

            modelBuilder.Entity<SAStationApp>()
                .Property(e => e.PID)
                .IsUnicode(false);

            modelBuilder.Entity<SAStationApp>()
                .Property(e => e.Mode)
                .IsUnicode(false);

            modelBuilder.Entity<SAStationApp>()
                .Property(e => e.Lvl)
                .IsUnicode(false);

            modelBuilder.Entity<SAStationApp>()
                .Property(e => e.A1st)
                .IsUnicode(false);

            modelBuilder.Entity<SAStationApp>()
                .Property(e => e.A1stD)
                .IsUnicode(false);

            modelBuilder.Entity<SAStationApp>()
                .Property(e => e.A2nd)
                .IsUnicode(false);

            modelBuilder.Entity<SAStationApp>()
                .Property(e => e.A2ndD)
                .IsUnicode(false);

            modelBuilder.Entity<SAStationApp>()
                .Property(e => e.CreatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<SAStationApp>()
                .Property(e => e.UpdatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<SAStationApp>()
                .Property(e => e.Version)
                .IsFixedLength();

            modelBuilder.Entity<SAStationListPL>()
                .Property(e => e.StationID)
                .IsUnicode(false);

            modelBuilder.Entity<SAStationListPL>()
                .Property(e => e.PLID)
                .IsUnicode(false);

            modelBuilder.Entity<SAStationListPL>()
                .Property(e => e.Days)
                .IsUnicode(false);

            modelBuilder.Entity<SAStationListPL>()
                .Property(e => e.UpdatedUserID)
                .IsUnicode(false);

            modelBuilder.Entity<SAStationListPL>()
                .Property(e => e.CreatedUserID)
                .IsUnicode(false);

            modelBuilder.Entity<SAStationRelated>()
                .Property(e => e.PStation)
                .IsUnicode(false);

            modelBuilder.Entity<SAStationRelated>()
                .Property(e => e.StationCode)
                .IsUnicode(false);

            modelBuilder.Entity<SAStationRelated>()
                .Property(e => e.CStation)
                .IsUnicode(false);

            modelBuilder.Entity<SAStationRelated>()
                .Property(e => e.BossStationCode)
                .IsUnicode(false);

            modelBuilder.Entity<SAStationRelated>()
                .Property(e => e.Status)
                .IsUnicode(false);

            modelBuilder.Entity<SATaskAttendee>()
                .Property(e => e.UserID)
                .IsUnicode(false);

            modelBuilder.Entity<SATaskAttendee>()
                .Property(e => e.CreatedUserID)
                .IsUnicode(false);

            modelBuilder.Entity<SATermOfShipping>()
                .Property(e => e.CommoDity)
                .IsUnicode(false);

            modelBuilder.Entity<SATermOfShipping>()
                .Property(e => e.OriTransMode)
                .IsUnicode(false);

            modelBuilder.Entity<SATermOfShipping>()
                .Property(e => e.DestTransMode)
                .IsUnicode(false);

            modelBuilder.Entity<SATermOfShipping>()
                .Property(e => e.OriShipTerm)
                .IsUnicode(false);

            modelBuilder.Entity<SATermOfShipping>()
                .Property(e => e.DestShipTerm)
                .IsUnicode(false);

            modelBuilder.Entity<SATermOfShipping>()
                .Property(e => e.CreatedUserID)
                .IsUnicode(false);

            modelBuilder.Entity<SATermOfShipping>()
                .Property(e => e.UpdatedUserID)
                .IsUnicode(false);

            modelBuilder.Entity<SATermsCondition>()
                .Property(e => e.IsSelected)
                .IsUnicode(false);

            modelBuilder.Entity<SATermsCondition>()
                .Property(e => e.Contents)
                .IsUnicode(false);

            modelBuilder.Entity<SATermsCondition>()
                .Property(e => e.DeptType)
                .IsUnicode(false);

            modelBuilder.Entity<SATermsCondition>()
                .Property(e => e.FrmID)
                .IsUnicode(false);

            modelBuilder.Entity<SATermsCondition>()
                .Property(e => e.CreatedUserID)
                .IsUnicode(false);

            modelBuilder.Entity<SATermsCondition>()
                .Property(e => e.UpdatedUserID)
                .IsUnicode(false);

            modelBuilder.Entity<SAUserList>()
                .Property(e => e.UserID)
                .IsUnicode(false);

            modelBuilder.Entity<SAUserList>()
                .Property(e => e.UserName)
                .IsUnicode(false);

            modelBuilder.Entity<SAUserList>()
                .Property(e => e.StationCode)
                .IsUnicode(false);

            modelBuilder.Entity<SAUserList>()
                .Property(e => e.SalesLevel)
                .IsUnicode(false);

            modelBuilder.Entity<SAUserList>()
                .Property(e => e.CallingCard)
                .IsUnicode(false);

            modelBuilder.Entity<SAUserList>()
                .Property(e => e.Mail)
                .IsUnicode(false);

            modelBuilder.Entity<SAUserList>()
                .Property(e => e.Phone)
                .IsUnicode(false);

            modelBuilder.Entity<SAUserList>()
                .Property(e => e.Ext)
                .IsUnicode(false);

            modelBuilder.Entity<SAUserList>()
                .Property(e => e.CPhone)
                .IsUnicode(false);

            modelBuilder.Entity<SAUserList>()
                .Property(e => e.JobTitle)
                .IsUnicode(false);

            modelBuilder.Entity<SAUserList>()
                .Property(e => e.Status)
                .IsUnicode(false);

            modelBuilder.Entity<SAUserList>()
                .Property(e => e.ExceptAccess)
                .IsUnicode(false);

            modelBuilder.Entity<SAUserList>()
                .Property(e => e.CreatedUserID)
                .IsUnicode(false);

            modelBuilder.Entity<SAUserList>()
                .Property(e => e.UpdateUserID)
                .IsUnicode(false);

            modelBuilder.Entity<SAUserList>()
                .Property(e => e.StationType)
                .IsUnicode(false);

            modelBuilder.Entity<SAUserList>()
                .Property(e => e.Deputy)
                .IsUnicode(false);

            modelBuilder.Entity<SAUserList>()
                .Property(e => e.StationID)
                .IsUnicode(false);

            modelBuilder.Entity<SAUserList>()
                .Property(e => e.CityOrder)
                .IsUnicode(false);

            modelBuilder.Entity<SAUserList>()
                .Property(e => e.ExcelQuote)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<SAUserRelated>()
                .Property(e => e.UserID)
                .IsUnicode(false);

            modelBuilder.Entity<SAUserRelated>()
                .Property(e => e.BossUserID)
                .IsUnicode(false);

            modelBuilder.Entity<SAUserRelated>()
                .Property(e => e.Status)
                .IsUnicode(false);

            modelBuilder.Entity<SAUserRelated>()
                .Property(e => e.CreatedUserID)
                .IsUnicode(false);

            modelBuilder.Entity<SAUserRelated>()
                .Property(e => e.UpdateUserID)
                .IsUnicode(false);

            modelBuilder.Entity<SAUserReportTo>()
                .Property(e => e.BossUserID)
                .IsUnicode(false);

            modelBuilder.Entity<SAUserReportTo>()
                .Property(e => e.UserID)
                .IsUnicode(false);

            modelBuilder.Entity<SAUserReportTo>()
                .Property(e => e.CreatedUserID)
                .IsUnicode(false);

            modelBuilder.Entity<SAUserRole>()
                .Property(e => e.RoleName)
                .IsUnicode(false);

            modelBuilder.Entity<SAUserRole>()
                .Property(e => e.UserID)
                .IsUnicode(false);

            modelBuilder.Entity<SAUserRole>()
                .Property(e => e.Status)
                .IsUnicode(false);

            modelBuilder.Entity<SAUserRole>()
                .Property(e => e.DeptType)
                .IsUnicode(false);

            modelBuilder.Entity<SAUserRole>()
                .Property(e => e.DeptCode)
                .IsUnicode(false);

            modelBuilder.Entity<SAUserRole>()
                .Property(e => e.Item)
                .IsUnicode(false);

            modelBuilder.Entity<SAUserRole>()
                .Property(e => e.ItemName)
                .IsUnicode(false);

            modelBuilder.Entity<SAUserRole>()
                .Property(e => e.CreatedUserID)
                .IsUnicode(false);

            modelBuilder.Entity<SAUserRole>()
                .Property(e => e.UpdateUserID)
                .IsUnicode(false);

            modelBuilder.Entity<SMCreditApprovalLevel>()
                .Property(e => e.CreditLevelID)
                .IsFixedLength();

            modelBuilder.Entity<SMCreditApprovalLevel>()
                .Property(e => e.SettingType)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<SMCreditApprovalLevel>()
                .Property(e => e.CreatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<SMCreditApprovalLevel>()
                .Property(e => e.UpdatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<SMCreditLevel>()
                .Property(e => e.CreditLevelCode)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<SMCreditLevel>()
                .Property(e => e.CreditLevelName)
                .IsUnicode(false);

            modelBuilder.Entity<SMCreditLevel>()
                .Property(e => e.Status)
                .IsUnicode(false);

            modelBuilder.Entity<SMCreditLevel>()
                .Property(e => e.CreatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<SMCreditLevel>()
                .Property(e => e.UpdatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<SMCustomer>()
                .Property(e => e.StationID)
                .IsUnicode(false);

            modelBuilder.Entity<SMCustomer>()
                .Property(e => e.CustomerCode)
                .IsUnicode(false);

            modelBuilder.Entity<SMCustomer>()
                .Property(e => e.GlobalCode)
                .IsUnicode(false);

            modelBuilder.Entity<SMCustomer>()
                .Property(e => e.Phone)
                .IsUnicode(false);

            modelBuilder.Entity<SMCustomer>()
                .Property(e => e.PhoneExt)
                .IsUnicode(false);

            modelBuilder.Entity<SMCustomer>()
                .Property(e => e.Fax)
                .IsUnicode(false);

            modelBuilder.Entity<SMCustomer>()
                .Property(e => e.FaxExt)
                .IsUnicode(false);

            modelBuilder.Entity<SMCustomer>()
                .Property(e => e.Zip)
                .IsUnicode(false);

            modelBuilder.Entity<SMCustomer>()
                .Property(e => e.TradeTerm)
                .IsUnicode(false);

            modelBuilder.Entity<SMCustomer>()
                .Property(e => e.ShipmentType)
                .IsUnicode(false);

            modelBuilder.Entity<SMCustomer>()
                .Property(e => e.ServiceType)
                .IsUnicode(false);

            modelBuilder.Entity<SMCustomer>()
                .Property(e => e.AirMove)
                .IsUnicode(false);

            modelBuilder.Entity<SMCustomer>()
                .Property(e => e.OceanMove)
                .IsUnicode(false);

            modelBuilder.Entity<SMCustomer>()
                .Property(e => e.AirLineCode)
                .IsUnicode(false);

            modelBuilder.Entity<SMCustomer>()
                .Property(e => e.TPLetterCode)
                .IsUnicode(false);

            modelBuilder.Entity<SMCustomer>()
                .Property(e => e.VAT)
                .IsUnicode(false);

            modelBuilder.Entity<SMCustomer>()
                .Property(e => e.Status)
                .IsUnicode(false);

            modelBuilder.Entity<SMCustomer>()
                .Property(e => e.CreatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<SMCustomer>()
                .Property(e => e.UpdatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<SMCustomer>()
                .Property(e => e.Version)
                .IsFixedLength();

            modelBuilder.Entity<SMCustomer>()
                .Property(e => e.City)
                .IsUnicode(false);

            modelBuilder.Entity<SMCustomer>()
                .Property(e => e.State)
                .IsUnicode(false);

            modelBuilder.Entity<SMCustomer>()
                .Property(e => e.CreatedStationID)
                .IsUnicode(false);

            modelBuilder.Entity<SMCustomer>()
                .Property(e => e.VendorPostingGLCode)
                .IsUnicode(false);

            modelBuilder.Entity<SMCustomerAddress>()
                .Property(e => e.StationID)
                .IsUnicode(false);

            modelBuilder.Entity<SMCustomerAddress>()
                .Property(e => e.FormType)
                .IsUnicode(false);

            modelBuilder.Entity<SMCustomerAddress>()
                .Property(e => e.Mode)
                .IsUnicode(false);

            modelBuilder.Entity<SMCustomerAddress>()
                .Property(e => e.CustomerPhone)
                .IsUnicode(false);

            modelBuilder.Entity<SMCustomerAddress>()
                .Property(e => e.CustomerFax)
                .IsUnicode(false);

            modelBuilder.Entity<SMCustomerAddress>()
                .Property(e => e.CustomerEmail)
                .IsUnicode(false);

            modelBuilder.Entity<SMCustomerAddress>()
                .Property(e => e.Status)
                .IsUnicode(false);

            modelBuilder.Entity<SMCustomerAddress>()
                .Property(e => e.CreatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<SMCustomerAddress>()
                .Property(e => e.UpdatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<SMCustomerAddressMode>()
                .Property(e => e.Status)
                .IsUnicode(false);

            modelBuilder.Entity<SMCustomerAddressMode>()
                .Property(e => e.CreatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<SMCustomerAddressMode>()
                .Property(e => e.UpdatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<SMCustomerContact>()
                .Property(e => e.StationID)
                .IsUnicode(false);

            modelBuilder.Entity<SMCustomerContact>()
                .Property(e => e.Tel_H)
                .IsUnicode(false);

            modelBuilder.Entity<SMCustomerContact>()
                .Property(e => e.Tel_M)
                .IsUnicode(false);

            modelBuilder.Entity<SMCustomerContact>()
                .Property(e => e.Tel_O)
                .IsUnicode(false);

            modelBuilder.Entity<SMCustomerContact>()
                .Property(e => e.Tel_OExt)
                .IsUnicode(false);

            modelBuilder.Entity<SMCustomerContact>()
                .Property(e => e.Fax)
                .IsUnicode(false);

            modelBuilder.Entity<SMCustomerContact>()
                .Property(e => e.Birthday)
                .IsUnicode(false);

            modelBuilder.Entity<SMCustomerContact>()
                .Property(e => e.Status)
                .IsUnicode(false);

            modelBuilder.Entity<SMCustomerContact>()
                .Property(e => e.CreatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<SMCustomerContact>()
                .Property(e => e.UpdatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<SMCustomerContact>()
                .Property(e => e.Version)
                .IsFixedLength();

            modelBuilder.Entity<SMCustomerContact>()
                .Property(e => e.Role)
                .IsUnicode(false);

            modelBuilder.Entity<SMCustomerContact>()
                .Property(e => e.Department)
                .IsUnicode(false);

            modelBuilder.Entity<SMCustomerContact>()
                .Property(e => e.Location)
                .IsUnicode(false);

            modelBuilder.Entity<SMCustomerContact>()
                .Property(e => e.Boss)
                .IsUnicode(false);

            modelBuilder.Entity<SMCustomerContact>()
                .Property(e => e.Assistant)
                .IsUnicode(false);

            modelBuilder.Entity<SMCustomerContact>()
                .Property(e => e.Language)
                .IsUnicode(false);

            modelBuilder.Entity<SMCustomerContact>()
                .Property(e => e.JoinedCompany)
                .IsUnicode(false);

            modelBuilder.Entity<SMCustomerContact>()
                .Property(e => e.JoinedIndustry)
                .IsUnicode(false);

            modelBuilder.Entity<SMCustomerContact>()
                .Property(e => e.Hobby)
                .IsUnicode(false);

            modelBuilder.Entity<SMCustomerContact>()
                .Property(e => e.Address)
                .IsUnicode(false);

            modelBuilder.Entity<SMCustomerContact>()
                .Property(e => e.City)
                .IsUnicode(false);

            modelBuilder.Entity<SMCustomerContact>()
                .Property(e => e.State)
                .IsUnicode(false);

            modelBuilder.Entity<SMCustomerContact>()
                .Property(e => e.Country)
                .IsUnicode(false);

            modelBuilder.Entity<SMCustomerContact>()
                .Property(e => e.ZIP)
                .IsUnicode(false);

            modelBuilder.Entity<SMCustomerContact>()
                .Property(e => e.PersonalInformation)
                .IsUnicode(false);

            modelBuilder.Entity<SMCustomerContact>()
                .Property(e => e.trContent)
                .IsUnicode(false);

            modelBuilder.Entity<SMCustomerContact>()
                .Property(e => e.FamilyMembers)
                .IsUnicode(false);

            modelBuilder.Entity<SMCustomerContact>()
                .Property(e => e.Names)
                .IsUnicode(false);

            modelBuilder.Entity<SMCustomerContact>()
                .Property(e => e.Titles)
                .IsUnicode(false);

            modelBuilder.Entity<SMCustomerContact>()
                .Property(e => e.LastContact)
                .IsUnicode(false);

            modelBuilder.Entity<SMCustomerContact>()
                .Property(e => e.VIP)
                .IsUnicode(false);

            modelBuilder.Entity<SMCustomerContact>()
                .Property(e => e.LocalName)
                .IsUnicode(false);

            modelBuilder.Entity<SMCustomerContact>()
                .Property(e => e.Fax2)
                .IsUnicode(false);

            modelBuilder.Entity<SMCustomerFormMail>()
                .Property(e => e.UserID)
                .IsUnicode(false);

            modelBuilder.Entity<SMCustomerFormMail>()
                .Property(e => e.Status)
                .IsUnicode(false);

            modelBuilder.Entity<SMCustomerFormMail>()
                .Property(e => e.CreatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<SMCustomerFormMail>()
                .Property(e => e.UpdatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<SMCustomerPL>()
                .Property(e => e.StationID)
                .IsUnicode(false);

            modelBuilder.Entity<SMCustomerPL>()
                .Property(e => e.Status)
                .IsUnicode(false);

            modelBuilder.Entity<SMCustomerPL>()
                .Property(e => e.CreatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<SMCustomerPL>()
                .Property(e => e.UpdatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<SMCustomerPL>()
                .Property(e => e.Version)
                .IsFixedLength();

            modelBuilder.Entity<SMDocument>()
                .Property(e => e.StationID)
                .IsUnicode(false);

            modelBuilder.Entity<SMDocument>()
                .Property(e => e.DocCode)
                .IsUnicode(false);

            modelBuilder.Entity<SMDocument>()
                .Property(e => e.DocName)
                .IsUnicode(false);

            modelBuilder.Entity<SMDocument>()
                .Property(e => e.Status)
                .IsUnicode(false);

            modelBuilder.Entity<SMDocument>()
                .Property(e => e.CreatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<SMDocument>()
                .Property(e => e.UpdatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<SMDocument>()
                .Property(e => e.Version)
                .IsFixedLength();

            modelBuilder.Entity<SMSalesPerson>()
                .Property(e => e.StationID)
                .IsUnicode(false);

            modelBuilder.Entity<SMSalesPerson>()
                .Property(e => e.SalesPersonCode)
                .IsUnicode(false);

            modelBuilder.Entity<SMSalesPerson>()
                .Property(e => e.SalesPersonName)
                .IsUnicode(false);

            modelBuilder.Entity<SMSalesPerson>()
                .Property(e => e.Status)
                .IsUnicode(false);

            modelBuilder.Entity<SMSalesPerson>()
                .Property(e => e.CreatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<SMSalesPerson>()
                .Property(e => e.UpdatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<SMSalesPerson>()
                .Property(e => e.Version)
                .IsFixedLength();

            modelBuilder.Entity<SMTrucker>()
                .Property(e => e.StationID)
                .IsUnicode(false);

            modelBuilder.Entity<SMTrucker>()
                .Property(e => e.TruckerCode)
                .IsUnicode(false);

            modelBuilder.Entity<SMTrucker>()
                .Property(e => e.Status)
                .IsUnicode(false);

            modelBuilder.Entity<SMTrucker>()
                .Property(e => e.CreatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<SMTrucker>()
                .Property(e => e.UpdatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<SMTrucker>()
                .Property(e => e.Version)
                .IsFixedLength();

            modelBuilder.Entity<SMWarehouse>()
                .Property(e => e.StationID)
                .IsUnicode(false);

            modelBuilder.Entity<SMWarehouse>()
                .Property(e => e.WarehouseCode)
                .IsUnicode(false);

            modelBuilder.Entity<SMWarehouse>()
                .Property(e => e.Status)
                .IsUnicode(false);

            modelBuilder.Entity<SMWarehouse>()
                .Property(e => e.CreatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<SMWarehouse>()
                .Property(e => e.UpdatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<SMWarehouse>()
                .Property(e => e.Version)
                .IsFixedLength();

            modelBuilder.Entity<SMWLocation>()
                .Property(e => e.StationID)
                .IsUnicode(false);

            modelBuilder.Entity<SMWLocation>()
                .Property(e => e.LocationCode)
                .IsUnicode(false);

            modelBuilder.Entity<SMWLocation>()
                .Property(e => e.Status)
                .IsUnicode(false);

            modelBuilder.Entity<SMWLocation>()
                .Property(e => e.CreatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<SMWLocation>()
                .Property(e => e.UpdatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<SMWLocation>()
                .Property(e => e.Version)
                .IsFixedLength();

            modelBuilder.Entity<SAContactReportTo>()
                .Property(e => e.CreatedUserID)
                .IsUnicode(false);

            modelBuilder.Entity<SADelRelativeRight>()
                .Property(e => e.StationID)
                .IsUnicode(false);

            modelBuilder.Entity<SAHouse20130306>()
                .Property(e => e.Mode)
                .IsUnicode(false);

            modelBuilder.Entity<SAHouse20130306>()
                .Property(e => e.ProductlineID)
                .IsUnicode(false);

            modelBuilder.Entity<SAHouse20130306>()
                .Property(e => e.House)
                .IsUnicode(false);

            modelBuilder.Entity<SAHouse20130306>()
                .Property(e => e.FromCityCode)
                .IsUnicode(false);

            modelBuilder.Entity<SAHouse20130306>()
                .Property(e => e.DEPTCode)
                .IsUnicode(false);

            modelBuilder.Entity<SAHouse20130306>()
                .Property(e => e.DSTNCode)
                .IsUnicode(false);

            modelBuilder.Entity<SAHouse20130306>()
                .Property(e => e.ToCityCode)
                .IsUnicode(false);

            modelBuilder.Entity<SAHouse20130306>()
                .Property(e => e.WTUOM)
                .IsUnicode(false);

            modelBuilder.Entity<SAHouse20130306>()
                .Property(e => e.AWBType)
                .IsUnicode(false);

            modelBuilder.Entity<SAHouse20130306>()
                .Property(e => e.ConfirmID)
                .IsUnicode(false);

            modelBuilder.Entity<SAHouse20130306>()
                .Property(e => e.ServiceType)
                .IsUnicode(false);

            modelBuilder.Entity<SAHouse20130306>()
                .Property(e => e.LotNo)
                .IsUnicode(false);

            modelBuilder.Entity<SAHouse20130306>()
                .Property(e => e.Master)
                .IsUnicode(false);

            modelBuilder.Entity<SAKPI>()
                .Property(e => e.Type)
                .IsUnicode(false);

            modelBuilder.Entity<SAKPI>()
                .Property(e => e.Code)
                .IsUnicode(false);

            modelBuilder.Entity<SAKPI>()
                .Property(e => e.Description)
                .IsUnicode(false);

            modelBuilder.Entity<SAKPI>()
                .Property(e => e.frequency)
                .IsUnicode(false);

            modelBuilder.Entity<SAKPI>()
                .Property(e => e.Status)
                .IsUnicode(false);

            modelBuilder.Entity<SAKPI>()
                .Property(e => e.CreatedUserID)
                .IsUnicode(false);

            modelBuilder.Entity<SAKPI>()
                .Property(e => e.UpdateUserID)
                .IsUnicode(false);

            modelBuilder.Entity<SAKPITarget>()
                .Property(e => e.Status)
                .IsUnicode(false);

            modelBuilder.Entity<SAKPITarget>()
                .Property(e => e.DeptType)
                .IsUnicode(false);

            modelBuilder.Entity<SAKPITarget>()
                .Property(e => e.DeptCode)
                .IsUnicode(false);

            modelBuilder.Entity<SAKPITarget>()
                .Property(e => e.CreatedUserID)
                .IsUnicode(false);

            modelBuilder.Entity<SAKPITarget>()
                .Property(e => e.UpdateUserID)
                .IsUnicode(false);

            modelBuilder.Entity<SAPotentialLeadComment>()
                .Property(e => e.Status)
                .IsUnicode(false);

            modelBuilder.Entity<SAPotentialLeadComment>()
                .Property(e => e.ReplyUserID)
                .IsUnicode(false);

            modelBuilder.Entity<SAPotentialLeadComment>()
                .Property(e => e.SourceType)
                .IsUnicode(false);

            modelBuilder.Entity<SAPotentialLeadContact>()
                .Property(e => e.Tel_H)
                .IsUnicode(false);

            modelBuilder.Entity<SAPotentialLeadContact>()
                .Property(e => e.Tel_M)
                .IsUnicode(false);

            modelBuilder.Entity<SAPotentialLeadContact>()
                .Property(e => e.Tel_O)
                .IsUnicode(false);

            modelBuilder.Entity<SAPotentialLeadContact>()
                .Property(e => e.Tel_OExt)
                .IsUnicode(false);

            modelBuilder.Entity<SAPotentialLeadContact>()
                .Property(e => e.Fax)
                .IsUnicode(false);

            modelBuilder.Entity<SAPotentialLeadContact>()
                .Property(e => e.Status)
                .IsUnicode(false);

            modelBuilder.Entity<SAPotentialLeadContact>()
                .Property(e => e.CreatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<SAPotentialLeadContact>()
                .Property(e => e.UpdatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<SAPotentialLeadContact>()
                .Property(e => e.Version)
                .IsFixedLength();

            modelBuilder.Entity<SAReferSMPL>()
                .Property(e => e.Description)
                .IsUnicode(false);

            modelBuilder.Entity<SAReferSMPL>()
                .Property(e => e.Status)
                .IsUnicode(false);

            modelBuilder.Entity<SASecuredDataTmp>()
                .Property(e => e.ConfirmId)
                .IsUnicode(false);

            modelBuilder.Entity<SASecuredDataTmp>()
                .Property(e => e.ShptNo)
                .IsUnicode(false);

            modelBuilder.Entity<SASecuredDataTmp>()
                .Property(e => e.LotNo)
                .IsUnicode(false);

            modelBuilder.Entity<SASecuredDataTmp>()
                .Property(e => e.StationID)
                .IsUnicode(false);

            modelBuilder.Entity<SASerialNumber>()
                .Property(e => e.StationCode)
                .IsUnicode(false);

            modelBuilder.Entity<SASerialNumber>()
                .Property(e => e.CreatedUserID)
                .IsUnicode(false);

            modelBuilder.Entity<SASerialNumber>()
                .Property(e => e.UpdatedUserID)
                .IsUnicode(false);

            modelBuilder.Entity<SAStationList>()
                .Property(e => e.StationCode)
                .IsUnicode(false);

            modelBuilder.Entity<SAStationList>()
                .Property(e => e.CreditAcc)
                .IsUnicode(false);

            modelBuilder.Entity<SAStationList>()
                .Property(e => e.StationName)
                .IsUnicode(false);

            modelBuilder.Entity<SAStationList>()
                .Property(e => e.LocalCurr)
                .IsUnicode(false);

            modelBuilder.Entity<SAStationList>()
                .Property(e => e.CreditAmount)
                .IsUnicode(false);

            modelBuilder.Entity<SAStationList>()
                .Property(e => e.CreditAir451st)
                .IsUnicode(false);

            modelBuilder.Entity<SAStationList>()
                .Property(e => e.CreditAir451stDeputy)
                .IsUnicode(false);

            modelBuilder.Entity<SAStationList>()
                .Property(e => e.CreditAir452nd)
                .IsUnicode(false);

            modelBuilder.Entity<SAStationList>()
                .Property(e => e.CreditAir452ndDeputy)
                .IsUnicode(false);

            modelBuilder.Entity<SAStationList>()
                .Property(e => e.CreditAir601st)
                .IsUnicode(false);

            modelBuilder.Entity<SAStationList>()
                .Property(e => e.CreditAir601stDeputy)
                .IsUnicode(false);

            modelBuilder.Entity<SAStationList>()
                .Property(e => e.CreditAir602nd)
                .IsUnicode(false);

            modelBuilder.Entity<SAStationList>()
                .Property(e => e.CreditAir602ndDeputy)
                .IsUnicode(false);

            modelBuilder.Entity<SAStationList>()
                .Property(e => e.CreditSea301st)
                .IsUnicode(false);

            modelBuilder.Entity<SAStationList>()
                .Property(e => e.CreditSea301stDeputy)
                .IsUnicode(false);

            modelBuilder.Entity<SAStationList>()
                .Property(e => e.CreditSea302nd)
                .IsUnicode(false);

            modelBuilder.Entity<SAStationList>()
                .Property(e => e.CreditSea302ndDeputy)
                .IsUnicode(false);

            modelBuilder.Entity<SAStationList>()
                .Property(e => e.CreditSea451st)
                .IsUnicode(false);

            modelBuilder.Entity<SAStationList>()
                .Property(e => e.CreditSea451stDeputy)
                .IsUnicode(false);

            modelBuilder.Entity<SAStationList>()
                .Property(e => e.CreditSea452nd)
                .IsUnicode(false);

            modelBuilder.Entity<SAStationList>()
                .Property(e => e.CreditSea452ndDeputy)
                .IsUnicode(false);

            modelBuilder.Entity<SAStationList>()
                .Property(e => e.CreditSea601st)
                .IsUnicode(false);

            modelBuilder.Entity<SAStationList>()
                .Property(e => e.CreditSea601stDeputy)
                .IsUnicode(false);

            modelBuilder.Entity<SAStationList>()
                .Property(e => e.CreditSea602nd)
                .IsUnicode(false);

            modelBuilder.Entity<SAStationList>()
                .Property(e => e.CreditSea602ndDeputy)
                .IsUnicode(false);

            modelBuilder.Entity<SAStationList>()
                .Property(e => e.CreditAir751st)
                .IsUnicode(false);

            modelBuilder.Entity<SAStationList>()
                .Property(e => e.CreditAir751stDeputy)
                .IsUnicode(false);

            modelBuilder.Entity<SAStationList>()
                .Property(e => e.CreditAir752nd)
                .IsUnicode(false);

            modelBuilder.Entity<SAStationList>()
                .Property(e => e.CreditAir752ndDeputy)
                .IsUnicode(false);

            modelBuilder.Entity<SAStationList>()
                .Property(e => e.CreditAir901st)
                .IsUnicode(false);

            modelBuilder.Entity<SAStationList>()
                .Property(e => e.CreditAir901stDeputy)
                .IsUnicode(false);

            modelBuilder.Entity<SAStationList>()
                .Property(e => e.CreditAir902nd)
                .IsUnicode(false);

            modelBuilder.Entity<SAStationList>()
                .Property(e => e.CreditAir902ndDeputy)
                .IsUnicode(false);

            modelBuilder.Entity<SAStationList>()
                .Property(e => e.CreditSea751st)
                .IsUnicode(false);

            modelBuilder.Entity<SAStationList>()
                .Property(e => e.CreditSea751stDeputy)
                .IsUnicode(false);

            modelBuilder.Entity<SAStationList>()
                .Property(e => e.CreditSea752nd)
                .IsUnicode(false);

            modelBuilder.Entity<SAStationList>()
                .Property(e => e.CreditSea752ndDeputy)
                .IsUnicode(false);

            modelBuilder.Entity<SAStationList>()
                .Property(e => e.CreditWMS451st)
                .IsUnicode(false);

            modelBuilder.Entity<SAStationList>()
                .Property(e => e.CreditWMS451stDeputy)
                .IsUnicode(false);

            modelBuilder.Entity<SAStationList>()
                .Property(e => e.CreditWMS452nd)
                .IsUnicode(false);

            modelBuilder.Entity<SAStationList>()
                .Property(e => e.CreditWMS452ndDeputy)
                .IsUnicode(false);

            modelBuilder.Entity<SAStationList>()
                .Property(e => e.CreditWMS1st)
                .IsUnicode(false);

            modelBuilder.Entity<SAStationList>()
                .Property(e => e.CreditWMS1stDeputy)
                .IsUnicode(false);

            modelBuilder.Entity<SAStationList>()
                .Property(e => e.CreditWMS2nd)
                .IsUnicode(false);

            modelBuilder.Entity<SAStationList>()
                .Property(e => e.CreditWMS2ndDeputy)
                .IsUnicode(false);

            modelBuilder.Entity<SAStationList>()
                .Property(e => e.CreditWMS601st)
                .IsUnicode(false);

            modelBuilder.Entity<SAStationList>()
                .Property(e => e.CreditWMS601stDeputy)
                .IsUnicode(false);

            modelBuilder.Entity<SAStationList>()
                .Property(e => e.CreditWMS602nd)
                .IsUnicode(false);

            modelBuilder.Entity<SAStationList>()
                .Property(e => e.CreditWMS602ndDeputy)
                .IsUnicode(false);

            modelBuilder.Entity<SAStationList>()
                .Property(e => e.CreditWMS301st)
                .IsUnicode(false);

            modelBuilder.Entity<SAStationList>()
                .Property(e => e.CreditWMS301stDeputy)
                .IsUnicode(false);

            modelBuilder.Entity<SAStationList>()
                .Property(e => e.CreditWMS302nd)
                .IsUnicode(false);

            modelBuilder.Entity<SAStationList>()
                .Property(e => e.CreditWMS302ndDeputy)
                .IsUnicode(false);

            modelBuilder.Entity<SAStationList>()
                .Property(e => e.CreditDAS451st)
                .IsUnicode(false);

            modelBuilder.Entity<SAStationList>()
                .Property(e => e.CreditDAS451stDeputy)
                .IsUnicode(false);

            modelBuilder.Entity<SAStationList>()
                .Property(e => e.CreditDAS452nd)
                .IsUnicode(false);

            modelBuilder.Entity<SAStationList>()
                .Property(e => e.CreditDAS452ndDeputy)
                .IsUnicode(false);

            modelBuilder.Entity<SAStationList>()
                .Property(e => e.CreditDAS601st)
                .IsUnicode(false);

            modelBuilder.Entity<SAStationList>()
                .Property(e => e.CreditDAS601stDeputy)
                .IsUnicode(false);

            modelBuilder.Entity<SAStationList>()
                .Property(e => e.CreditDAS602nd)
                .IsUnicode(false);

            modelBuilder.Entity<SAStationList>()
                .Property(e => e.CreditDAS602ndDeputy)
                .IsUnicode(false);

            modelBuilder.Entity<SAStationList>()
                .Property(e => e.CreditDAS1st)
                .IsUnicode(false);

            modelBuilder.Entity<SAStationList>()
                .Property(e => e.CreditDAS1stDeputy)
                .IsUnicode(false);

            modelBuilder.Entity<SAStationList>()
                .Property(e => e.CreditDAS2nd)
                .IsUnicode(false);

            modelBuilder.Entity<SAStationList>()
                .Property(e => e.CreditDAS2ndDeputy)
                .IsUnicode(false);

            modelBuilder.Entity<SAStationList>()
                .Property(e => e.Quote1st)
                .IsUnicode(false);

            modelBuilder.Entity<SAStationList>()
                .Property(e => e.Quote1stDeputy)
                .IsUnicode(false);

            modelBuilder.Entity<SAStationList>()
                .Property(e => e.Quote2nd)
                .IsUnicode(false);

            modelBuilder.Entity<SAStationList>()
                .Property(e => e.Quote2ndDeputy)
                .IsUnicode(false);

            modelBuilder.Entity<SAStationList>()
                .Property(e => e.Address)
                .IsUnicode(false);

            modelBuilder.Entity<SAStationList>()
                .Property(e => e.Phone)
                .IsUnicode(false);

            modelBuilder.Entity<SAStationList>()
                .Property(e => e.Credit1st)
                .IsUnicode(false);

            modelBuilder.Entity<SAStationList>()
                .Property(e => e.Credit1stDeputy)
                .IsUnicode(false);

            modelBuilder.Entity<SAStationList>()
                .Property(e => e.Credit2nd)
                .IsUnicode(false);

            modelBuilder.Entity<SAStationList>()
                .Property(e => e.Credit2ndDeputy)
                .IsUnicode(false);

            modelBuilder.Entity<SAStationList>()
                .Property(e => e.POD)
                .IsUnicode(false);

            modelBuilder.Entity<SAStationList>()
                .Property(e => e.POL)
                .IsUnicode(false);

            modelBuilder.Entity<SAStationList>()
                .Property(e => e.StationLevel)
                .IsUnicode(false);

            modelBuilder.Entity<SAStationList>()
                .Property(e => e.NoAir15)
                .IsUnicode(false);

            modelBuilder.Entity<SAStationList>()
                .Property(e => e.NoSea07)
                .IsUnicode(false);

            modelBuilder.Entity<SAStationList>()
                .Property(e => e.NoAir30)
                .IsUnicode(false);

            modelBuilder.Entity<SAStationList>()
                .Property(e => e.NoSea15)
                .IsUnicode(false);

            modelBuilder.Entity<SAStationList>()
                .Property(e => e.CreditUserID)
                .IsUnicode(false);

            modelBuilder.Entity<SAStationList>()
                .Property(e => e.UpdateUserID)
                .IsUnicode(false);

            modelBuilder.Entity<SAStationList>()
                .Property(e => e.StationID)
                .IsUnicode(false);

            modelBuilder.Entity<SAStationList>()
                .Property(e => e.creditair401st)
                .IsUnicode(false);

            modelBuilder.Entity<SAStationList>()
                .Property(e => e.creditair401stDeputy)
                .IsUnicode(false);

            modelBuilder.Entity<SAStationList>()
                .Property(e => e.creditair402nd)
                .IsUnicode(false);

            modelBuilder.Entity<SAStationList>()
                .Property(e => e.creditair402ndDeputy)
                .IsUnicode(false);

            modelBuilder.Entity<SAStationList>()
                .Property(e => e.creditSea401st)
                .IsUnicode(false);

            modelBuilder.Entity<SAStationList>()
                .Property(e => e.creditSea401stDeputy)
                .IsUnicode(false);

            modelBuilder.Entity<SAStationList>()
                .Property(e => e.creditSea402nd)
                .IsUnicode(false);

            modelBuilder.Entity<SAStationList>()
                .Property(e => e.creditSea402ndDeputy)
                .IsUnicode(false);

            modelBuilder.Entity<SAStationList>()
                .Property(e => e.creditSea71st)
                .IsUnicode(false);

            modelBuilder.Entity<SAStationList>()
                .Property(e => e.creditSea71stDeputy)
                .IsUnicode(false);

            modelBuilder.Entity<SAStationList>()
                .Property(e => e.creditSea72nd)
                .IsUnicode(false);

            modelBuilder.Entity<SAStationList>()
                .Property(e => e.creditSea72ndDeputy)
                .IsUnicode(false);

            modelBuilder.Entity<SAStationList>()
                .Property(e => e.SelfApp)
                .IsUnicode(false);

            modelBuilder.Entity<SAStationList>()
                .Property(e => e.CreditMTS301st)
                .IsUnicode(false);

            modelBuilder.Entity<SAStationList>()
                .Property(e => e.CreditMTS301stDeputy)
                .IsUnicode(false);

            modelBuilder.Entity<SAStationList>()
                .Property(e => e.CreditMTS302nd)
                .IsUnicode(false);

            modelBuilder.Entity<SAStationList>()
                .Property(e => e.CreditMTS302ndDeputy)
                .IsUnicode(false);

            modelBuilder.Entity<SAStationList>()
                .Property(e => e.CreditMTS451st)
                .IsUnicode(false);

            modelBuilder.Entity<SAStationList>()
                .Property(e => e.CreditMTS451stDeputy)
                .IsUnicode(false);

            modelBuilder.Entity<SAStationList>()
                .Property(e => e.CreditMTS452nd)
                .IsUnicode(false);

            modelBuilder.Entity<SAStationList>()
                .Property(e => e.CreditMTS452ndDeputy)
                .IsUnicode(false);

            modelBuilder.Entity<SAStationList>()
                .Property(e => e.CreditMTS601st)
                .IsUnicode(false);

            modelBuilder.Entity<SAStationList>()
                .Property(e => e.CreditMTS601stDeputy)
                .IsUnicode(false);

            modelBuilder.Entity<SAStationList>()
                .Property(e => e.CreditMTS602nd)
                .IsUnicode(false);

            modelBuilder.Entity<SAStationList>()
                .Property(e => e.CreditMTS602ndDeputy)
                .IsUnicode(false);

            modelBuilder.Entity<SAStationList>()
                .Property(e => e.CreditMTS751st)
                .IsUnicode(false);

            modelBuilder.Entity<SAStationList>()
                .Property(e => e.CreditMTS751stDeputy)
                .IsUnicode(false);

            modelBuilder.Entity<SAStationList>()
                .Property(e => e.CreditMTS752nd)
                .IsUnicode(false);

            modelBuilder.Entity<SAStationList>()
                .Property(e => e.CreditMTS752ndDeputy)
                .IsUnicode(false);

            modelBuilder.Entity<SAStationList>()
                .Property(e => e.CreditTMS301st)
                .IsUnicode(false);

            modelBuilder.Entity<SAStationList>()
                .Property(e => e.CreditTMS301stDeputy)
                .IsUnicode(false);

            modelBuilder.Entity<SAStationList>()
                .Property(e => e.CreditTMS302nd)
                .IsUnicode(false);

            modelBuilder.Entity<SAStationList>()
                .Property(e => e.CreditTMS302ndDeputy)
                .IsUnicode(false);

            modelBuilder.Entity<SAStationList>()
                .Property(e => e.CreditTMS451st)
                .IsUnicode(false);

            modelBuilder.Entity<SAStationList>()
                .Property(e => e.CreditTMS451stDeputy)
                .IsUnicode(false);

            modelBuilder.Entity<SAStationList>()
                .Property(e => e.CreditTMS452nd)
                .IsUnicode(false);

            modelBuilder.Entity<SAStationList>()
                .Property(e => e.CreditTMS452ndDeputy)
                .IsUnicode(false);

            modelBuilder.Entity<SAStationList>()
                .Property(e => e.CreditTMS601st)
                .IsUnicode(false);

            modelBuilder.Entity<SAStationList>()
                .Property(e => e.CreditTMS601stDeputy)
                .IsUnicode(false);

            modelBuilder.Entity<SAStationList>()
                .Property(e => e.CreditTMS602nd)
                .IsUnicode(false);

            modelBuilder.Entity<SAStationList>()
                .Property(e => e.CreditTMS602ndDeputy)
                .IsUnicode(false);

            modelBuilder.Entity<SAStationList>()
                .Property(e => e.CreditTMS751st)
                .IsUnicode(false);

            modelBuilder.Entity<SAStationList>()
                .Property(e => e.CreditTMS751stDeputy)
                .IsUnicode(false);

            modelBuilder.Entity<SAStationList>()
                .Property(e => e.CreditTMS752nd)
                .IsUnicode(false);

            modelBuilder.Entity<SAStationList>()
                .Property(e => e.CreditTMS752ndDeputy)
                .IsUnicode(false);

            modelBuilder.Entity<SAStationList>()
                .Property(e => e.UpdatedUserID)
                .IsUnicode(false);

            modelBuilder.Entity<SAUserCustomizeData>()
                .Property(e => e.UserID)
                .IsFixedLength();

            modelBuilder.Entity<SAUserCustomizeData>()
                .Property(e => e.UpdatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<SMDeployVersion>()
                .Property(e => e.StationIP)
                .IsUnicode(false);

            modelBuilder.Entity<SMDeployVersion>()
                .Property(e => e.Version)
                .IsUnicode(false);

            modelBuilder.Entity<SMDeployVersion>()
                .Property(e => e.Error)
                .IsUnicode(false);

            modelBuilder.Entity<SMStation>()
                .Property(e => e.StationCode)
                .IsUnicode(false);
        }
    }
}
