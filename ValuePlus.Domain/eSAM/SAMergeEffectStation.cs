namespace ValuePlus.Domain.eSAM
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("SAMergeEffectStation")]
    public partial class SAMergeEffectStation
    {
        public int ID { get; set; }

        public int? MergeID { get; set; }

        [StringLength(50)]
        public string StationID { get; set; }

        public int? TableID { get; set; }

        [StringLength(50)]
        public string Remark { get; set; }

        [StringLength(10)]
        public string Status { get; set; }

        public DateTime? UpdatedDate { get; set; }

        [StringLength(10)]
        public string UpdatedUserID { get; set; }
    }
}
