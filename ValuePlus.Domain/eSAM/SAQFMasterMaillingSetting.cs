namespace ValuePlus.Domain.eSAM
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("SAQFMasterMaillingSetting")]
    public partial class SAQFMasterMaillingSetting
    {
        public int ID { get; set; }

        public int? MasterID { get; set; }

        public int? SVID { get; set; }

        [StringLength(1)]
        public string Type { get; set; }

        [StringLength(6)]
        public string UserID { get; set; }

        [Required]
        [StringLength(6)]
        public string CreatedBy { get; set; }

        public DateTime CreatedDate { get; set; }

        [StringLength(6)]
        public string UpdatedBy { get; set; }

        public DateTime? UpdatedDate { get; set; }
    }
}
