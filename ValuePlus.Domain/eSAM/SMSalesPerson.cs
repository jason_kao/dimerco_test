namespace ValuePlus.Domain.eSAM
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("SMSalesPerson")]
    public partial class SMSalesPerson
    {
        public int ID { get; set; }

        [Required]
        [StringLength(3)]
        public string StationID { get; set; }

        public int ProductLineID { get; set; }

        [Required]
        [StringLength(50)]
        public string SalesPersonCode { get; set; }

        [StringLength(50)]
        public string SalesPersonName { get; set; }

        [StringLength(10)]
        public string Status { get; set; }

        [StringLength(6)]
        public string CreatedBy { get; set; }

        public DateTime? CreatedDate { get; set; }

        [StringLength(6)]
        public string UpdatedBy { get; set; }

        public DateTime? UpdatedDate { get; set; }

        [Column(TypeName = "timestamp")]
        [MaxLength(8)]
        [Timestamp]
        public byte[] Version { get; set; }
    }
}
