namespace ValuePlus.Domain.eSAM
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("SASCUserGroup")]
    public partial class SASCUserGroup
    {
        [Key]
        public int HQID { get; set; }

        [StringLength(10)]
        public string UserID { get; set; }

        public int? GroupID { get; set; }

        [StringLength(10)]
        public string Status { get; set; }

        public DateTime? CreatedDate { get; set; }

        [StringLength(10)]
        public string CreatedBy { get; set; }

        public DateTime? UpdatedDate { get; set; }

        [StringLength(10)]
        public string UpdatedBy { get; set; }

        public virtual SASCGroup SASCGroup { get; set; }
    }
}
