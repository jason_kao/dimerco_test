namespace ValuePlus.Domain.eSAM
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class SAQuoteToolFreights
    {
        public int ID { get; set; }

        public int? QuoteID { get; set; }

        [StringLength(100)]
        public string PlaceOfReceipt { get; set; }

        [StringLength(100)]
        public string PortOfLoad { get; set; }

        [StringLength(100)]
        public string PortOfDischarge { get; set; }

        [StringLength(100)]
        public string PlaceOfDelivery { get; set; }

        [StringLength(100)]
        public string CarrierCode { get; set; }

        [StringLength(20)]
        public string TransitTime { get; set; }

        [StringLength(200)]
        public string Remark { get; set; }

        [StringLength(20)]
        public string MIN { get; set; }

        [StringLength(20)]
        public string CBM { get; set; }

        [StringLength(20)]
        public string TON { get; set; }

        [StringLength(20)]
        public string SellingRate1 { get; set; }

        [StringLength(20)]
        public string SellingRate2 { get; set; }

        [StringLength(20)]
        public string SellingRate3 { get; set; }

        [StringLength(20)]
        public string SellingRate4 { get; set; }

        [StringLength(20)]
        public string SellingRate5 { get; set; }

        [StringLength(20)]
        public string SellingRate6 { get; set; }

        [StringLength(20)]
        public string SellingRate7 { get; set; }

        [StringLength(20)]
        public string SellingRate8 { get; set; }

        [StringLength(20)]
        public string SellingRate9 { get; set; }

        [StringLength(20)]
        public string SellingRate10 { get; set; }

        [StringLength(50)]
        public string Commodity { get; set; }

        [StringLength(10)]
        public string CreatedUserID { get; set; }

        public DateTime? CreatedDate { get; set; }

        [StringLength(10)]
        public string UpdatedUserID { get; set; }

        public DateTime? UpdatedDate { get; set; }

        [StringLength(50)]
        public string ServiceLevel { get; set; }

        [StringLength(10)]
        public string sType { get; set; }

        [StringLength(50)]
        public string CurrencyCode { get; set; }

        public int? Seq { get; set; }

        public virtual SAQuotes SAQuotes { get; set; }
    }
}
