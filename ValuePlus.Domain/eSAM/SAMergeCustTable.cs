namespace ValuePlus.Domain.eSAM
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("SAMergeCustTable")]
    public partial class SAMergeCustTable
    {
        public int ID { get; set; }

        [StringLength(50)]
        public string Module { get; set; }

        [StringLength(50)]
        public string FunctionName { get; set; }

        [StringLength(50)]
        public string TableName { get; set; }

        [StringLength(50)]
        public string ColumnName { get; set; }

        [StringLength(50)]
        public string Owner { get; set; }

        public DateTime? CreatedDate { get; set; }

        [StringLength(10)]
        public string CreatedBY { get; set; }

        public DateTime? UpdatedDate { get; set; }

        [StringLength(10)]
        public string UpdatedBy { get; set; }

        [StringLength(10)]
        public string Status { get; set; }

        [StringLength(1)]
        public string IsReSM { get; set; }

        [StringLength(1)]
        public string Action { get; set; }

        [StringLength(1)]
        public string IsHQ { get; set; }

        [StringLength(1)]
        public string IsCheck { get; set; }
    }
}
