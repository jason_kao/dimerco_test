namespace ValuePlus.Domain.eSAM
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("SACheckList")]
    public partial class SACheckList
    {
        public int ID { get; set; }

        [StringLength(20)]
        public string StationIP { get; set; }

        [StringLength(30)]
        public string ServerName { get; set; }

        public int? PLID { get; set; }

        [StringLength(20)]
        public string Status { get; set; }

        [StringLength(255)]
        public string Cmd { get; set; }

        public DateTime? CreatedDate { get; set; }

        public DateTime? UpdatedDate { get; set; }

        public int? SourceID { get; set; }

        [StringLength(10)]
        public string StationCode { get; set; }

        [StringLength(100)]
        public string CustomerName { get; set; }
    }
}
