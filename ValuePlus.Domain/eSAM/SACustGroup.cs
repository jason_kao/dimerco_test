namespace ValuePlus.Domain.eSAM
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("SACustGroup")]
    public partial class SACustGroup
    {
        public int ID { get; set; }

        public int? ParentID { get; set; }

        public int? CustomerID { get; set; }

        [Required]
        [StringLength(10)]
        public string Depttype { get; set; }

        [StringLength(10)]
        public string CreatedBy { get; set; }

        public DateTime? CreatedDate { get; set; }

        [StringLength(10)]
        public string UpdatedBy { get; set; }

        public DateTime? UpdatedDate { get; set; }

        [StringLength(10)]
        public string sType { get; set; }
    }
}
