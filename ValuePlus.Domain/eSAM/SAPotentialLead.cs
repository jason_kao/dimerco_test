namespace ValuePlus.Domain.eSAM
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("SAPotentialLead")]
    public partial class SAPotentialLead
    {
        [Key]
        public int HQID { get; set; }

        [StringLength(255)]
        public string NAME { get; set; }

        [StringLength(255)]
        public string ADDRESS1 { get; set; }

        [StringLength(255)]
        public string CITYNAME { get; set; }

        public int? CityID { get; set; }

        [StringLength(255)]
        public string STATEPROVINCECODE { get; set; }

        [StringLength(20)]
        public string StateID { get; set; }

        [StringLength(10)]
        public string POSTALCODE { get; set; }

        [StringLength(255)]
        public string REGIONNAME { get; set; }

        [StringLength(255)]
        public string COUNTRYNAME { get; set; }

        [StringLength(20)]
        public string CountryID { get; set; }

        [StringLength(50)]
        public string PHONENUMBER { get; set; }

        [StringLength(10)]
        public string PhoneExt { get; set; }

        [StringLength(50)]
        public string FAXNUMBER { get; set; }

        [StringLength(10)]
        public string FaxExt { get; set; }

        [StringLength(255)]
        public string WEBADDRESS1 { get; set; }

        [StringLength(255)]
        public string WEBADDRESS2 { get; set; }

        [StringLength(50)]
        public string DUNSNUMBER { get; set; }

        [StringLength(255)]
        public string PARENTDUNS { get; set; }

        [StringLength(255)]
        public string TOPPARENTDUNS { get; set; }

        [StringLength(50)]
        public string SALES { get; set; }

        [StringLength(255)]
        public string FOREIGNTRADE { get; set; }

        [StringLength(255)]
        public string STOCKTICKER { get; set; }

        [StringLength(50)]
        public string TOTALEMPLOYEES { get; set; }

        [StringLength(255)]
        public string HQSTATUSDESCRIPTION { get; set; }

        [StringLength(255)]
        public string OWNERSHIPDESCRIPTION { get; set; }

        [StringLength(50)]
        public string YEARESTABLISHED { get; set; }

        [StringLength(255)]
        public string SALESSCORE { get; set; }

        [StringLength(50)]
        public string IMPORTPCNT { get; set; }

        [StringLength(50)]
        public string IMPORTVALUE { get; set; }

        [StringLength(50)]
        public string IMPORTTONNAGE { get; set; }

        [StringLength(50)]
        public string IMPORTTEUS { get; set; }

        [StringLength(50)]
        public string IMPORTSHIPMENTS { get; set; }

        [StringLength(255)]
        public string IMPORTCOUNTRY1 { get; set; }

        [StringLength(50)]
        public string IMPORTCOUNTRYSHIPMENTS1 { get; set; }

        [StringLength(255)]
        public string IMPORTCOUNTRY2 { get; set; }

        [StringLength(50)]
        public string IMPORTCOUNTRYSHIPMENTS2 { get; set; }

        [StringLength(255)]
        public string IMPORTCOUNTRY3 { get; set; }

        [StringLength(50)]
        public string IMPORTCOUNTRYSHIPMENTS3 { get; set; }

        [StringLength(255)]
        public string IMPORTCOUNTRY4 { get; set; }

        [StringLength(50)]
        public string IMPORTCOUNTRYSHIPMENTS4 { get; set; }

        [StringLength(255)]
        public string IMPORTCOUNTRY5 { get; set; }

        [StringLength(50)]
        public string IMPORTCOUNTRYSHIPMENTS5 { get; set; }

        [StringLength(255)]
        public string IMPORTPORT1 { get; set; }

        [StringLength(50)]
        public string IMPORTPORTSHIPMENTS1 { get; set; }

        [StringLength(255)]
        public string IMPORTPORT2 { get; set; }

        [StringLength(50)]
        public string IMPORTPORTSHIPMENTS2 { get; set; }

        [StringLength(255)]
        public string IMPORTPORT3 { get; set; }

        [StringLength(50)]
        public string IMPORTPORTSHIPMENTS3 { get; set; }

        [StringLength(255)]
        public string IMPORTPORT4 { get; set; }

        [StringLength(50)]
        public string IMPORTPORTSHIPMENTS4 { get; set; }

        [StringLength(255)]
        public string IMPORTPORT5 { get; set; }

        [StringLength(50)]
        public string IMPORTPORTSHIPMENTS5 { get; set; }

        [StringLength(255)]
        public string IMPORTCOMMODITY1 { get; set; }

        [StringLength(50)]
        public string IMPORTCOMMODITYSHIPMENTS1 { get; set; }

        [StringLength(255)]
        public string IMPORTCOMMODITY2 { get; set; }

        [StringLength(50)]
        public string IMPORTCOMMODITYSHIPMENTS2 { get; set; }

        [StringLength(255)]
        public string IMPORTCOMMODITY3 { get; set; }

        [StringLength(50)]
        public string IMPORTCOMMODITYSHIPMENTS3 { get; set; }

        [StringLength(255)]
        public string IMPORTCOMMODITY4 { get; set; }

        [StringLength(50)]
        public string IMPORTCOMMODITYSHIPMENTS4 { get; set; }

        [StringLength(255)]
        public string IMPORTCOMMODITY5 { get; set; }

        [StringLength(50)]
        public string IMPORTCOMMODITYSHIPMENTS5 { get; set; }

        [StringLength(50)]
        public string EXPORTPCNT { get; set; }

        [StringLength(50)]
        public string EXPORTVALUE { get; set; }

        [StringLength(50)]
        public string EXPORTTONNAGE { get; set; }

        [StringLength(50)]
        public string EXPORTTEUS { get; set; }

        [StringLength(50)]
        public string EXPORTSHIPMENTS { get; set; }

        [StringLength(255)]
        public string EXPORTCOUNTRY1 { get; set; }

        [StringLength(50)]
        public string EXPORTCOUNTRYSHIPMENTS1 { get; set; }

        [StringLength(255)]
        public string EXPORTCOUNTRY2 { get; set; }

        [StringLength(50)]
        public string EXPORTCOUNTRYSHIPMENTS2 { get; set; }

        [StringLength(255)]
        public string EXPORTCOUNTRY3 { get; set; }

        [StringLength(50)]
        public string EXPORTCOUNTRYSHIPMENTS3 { get; set; }

        [StringLength(255)]
        public string EXPORTCOUNTRY4 { get; set; }

        [StringLength(50)]
        public string EXPORTCOUNTRYSHIPMENTS4 { get; set; }

        [StringLength(255)]
        public string EXPORTCOUNTRY5 { get; set; }

        [StringLength(50)]
        public string EXPORTCOUNTRYSHIPMENTS5 { get; set; }

        [StringLength(255)]
        public string EXPORTPORT1 { get; set; }

        [StringLength(50)]
        public string EXPORTPORTSHIPMENTS1 { get; set; }

        [StringLength(255)]
        public string EXPORTPORT2 { get; set; }

        [StringLength(50)]
        public string EXPORTPORTSHIPMENTS2 { get; set; }

        [StringLength(255)]
        public string EXPORTPORT3 { get; set; }

        [StringLength(50)]
        public string EXPORTPORTSHIPMENTS3 { get; set; }

        [StringLength(255)]
        public string EXPORTPORT4 { get; set; }

        [StringLength(50)]
        public string EXPORTPORTSHIPMENTS4 { get; set; }

        [StringLength(255)]
        public string EXPORTPORT5 { get; set; }

        [StringLength(50)]
        public string EXPORTPORTSHIPMENTS5 { get; set; }

        [StringLength(255)]
        public string EXPORTCOMMODITY1 { get; set; }

        [StringLength(50)]
        public string EXPORTCOMMODITYSHIPMENTS1 { get; set; }

        [StringLength(255)]
        public string EXPORTCOMMODITY2 { get; set; }

        [StringLength(50)]
        public string EXPORTCOMMODITYSHIPMENTS2 { get; set; }

        [StringLength(255)]
        public string EXPORTCOMMODITY3 { get; set; }

        [StringLength(50)]
        public string EXPORTCOMMODITYSHIPMENTS3 { get; set; }

        [StringLength(255)]
        public string EXPORTCOMMODITY4 { get; set; }

        [StringLength(50)]
        public string EXPORTCOMMODITYSHIPMENTS4 { get; set; }

        [StringLength(255)]
        public string EXPORTCOMMODITY5 { get; set; }

        [StringLength(50)]
        public string EXPORTCOMMODITYSHIPMENTS5 { get; set; }

        [StringLength(255)]
        public string MAJORITYTRADE { get; set; }

        [StringLength(50)]
        public string PRIMARYSIC { get; set; }

        [StringLength(255)]
        public string PRIMARYSICDESCRIPTION { get; set; }

        [StringLength(255)]
        public string SIC2 { get; set; }

        [StringLength(255)]
        public string SIC2DESCRIPTION { get; set; }

        [StringLength(255)]
        public string SIC3 { get; set; }

        [StringLength(255)]
        public string SIC3DESCRIPTION { get; set; }

        [StringLength(255)]
        public string PRIMARYNAICS { get; set; }

        [StringLength(255)]
        public string PRIMARYNAICSDESCRIPTION { get; set; }

        [StringLength(255)]
        public string NAICS2 { get; set; }

        [StringLength(255)]
        public string NAICS2DESCRIPTION { get; set; }

        [StringLength(255)]
        public string NAICS3 { get; set; }

        [StringLength(255)]
        public string NAICS3DESCRIPTION { get; set; }

        public int? IndustryID { get; set; }

        public int? IndustryGroupID { get; set; }

        [StringLength(255)]
        public string ProductLine { get; set; }

        public int? SAFilesID { get; set; }

        public int? LeadHQID { get; set; }

        [Required]
        [StringLength(10)]
        public string Status { get; set; }

        [Required]
        [StringLength(10)]
        public string StationID { get; set; }

        [StringLength(6)]
        public string OwnerID { get; set; }

        [StringLength(6)]
        public string CreatedBy { get; set; }

        public DateTime CreatedDate { get; set; }

        [StringLength(6)]
        public string UpdatedBy { get; set; }

        public DateTime? UpdatedDate { get; set; }

        [StringLength(6)]
        public string AssignedBy { get; set; }

        public DateTime? AssignedDate { get; set; }
    }
}
