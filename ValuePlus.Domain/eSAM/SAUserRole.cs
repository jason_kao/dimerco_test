namespace ValuePlus.Domain.eSAM
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("SAUserRole")]
    public partial class SAUserRole
    {
        public int ID { get; set; }

        [StringLength(50)]
        public string RoleName { get; set; }

        [StringLength(10)]
        public string UserID { get; set; }

        [StringLength(10)]
        public string Status { get; set; }

        [StringLength(20)]
        public string DeptType { get; set; }

        [StringLength(20)]
        public string DeptCode { get; set; }

        [StringLength(20)]
        public string Item { get; set; }

        [StringLength(20)]
        public string ItemName { get; set; }

        [StringLength(10)]
        public string CreatedUserID { get; set; }

        public DateTime? CreatedDate { get; set; }

        [StringLength(10)]
        public string UpdateUserID { get; set; }

        public DateTime? UpdateDate { get; set; }
    }
}
