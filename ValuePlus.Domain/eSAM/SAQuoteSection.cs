namespace ValuePlus.Domain.eSAM
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("SAQuoteSection")]
    public partial class SAQuoteSection
    {
        public int ID { get; set; }

        [StringLength(15)]
        public string QuoteNo { get; set; }

        public int? OriginID { get; set; }

        [StringLength(10)]
        public string Status { get; set; }

        [StringLength(10)]
        public string CreatedStationID { get; set; }

        [StringLength(10)]
        public string CreatedUserID { get; set; }

        public int? ProductLine { get; set; }

        [StringLength(255)]
        public string CompName { get; set; }

        public string Greeting { get; set; }

        [StringLength(1)]
        public string Section1Break { get; set; }

        [StringLength(1)]
        public string Section2Break { get; set; }

        [StringLength(1)]
        public string Section3Break { get; set; }

        [StringLength(1)]
        public string ShowArea1 { get; set; }

        [StringLength(1)]
        public string ShowArea2 { get; set; }

        [StringLength(1)]
        public string ShowArea3 { get; set; }

        public int? LID { get; set; }

        public int? QuoteID { get; set; }

        [StringLength(10)]
        public string DocName { get; set; }

        [StringLength(10)]
        public string DocUrl { get; set; }

        [StringLength(1)]
        public string Section4Break { get; set; }

        [StringLength(1)]
        public string ShowArea4 { get; set; }

        public virtual SAQuotes SAQuotes { get; set; }
    }
}
