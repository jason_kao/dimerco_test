namespace ValuePlus.Domain.eSAM
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("SAQFExportInfo")]
    public partial class SAQFExportInfo
    {
        public int ID { get; set; }

        public int MasterID { get; set; }

        [StringLength(1)]
        public string Type { get; set; }

        public int? RegionID { get; set; }

        public int? CountryID { get; set; }

        [Required]
        [StringLength(6)]
        public string CreatedBy { get; set; }

        public DateTime? CreatedDate { get; set; }

        [StringLength(6)]
        public string UpdatedBy { get; set; }

        public DateTime? UpdatedDate { get; set; }
    }
}
