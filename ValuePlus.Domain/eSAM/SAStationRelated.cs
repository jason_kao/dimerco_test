namespace ValuePlus.Domain.eSAM
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("SAStationRelated")]
    public partial class SAStationRelated
    {
        public int ID { get; set; }

        [StringLength(10)]
        public string PStation { get; set; }

        [StringLength(10)]
        public string StationCode { get; set; }

        [StringLength(10)]
        public string CStation { get; set; }

        [StringLength(10)]
        public string BossStationCode { get; set; }

        [StringLength(10)]
        public string Status { get; set; }
    }
}
