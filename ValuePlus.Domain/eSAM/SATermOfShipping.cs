namespace ValuePlus.Domain.eSAM
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("SATermOfShipping")]
    public partial class SATermOfShipping
    {
        public int ID { get; set; }

        public int QuoteID { get; set; }

        public string CommoDity { get; set; }

        public bool? IsPrepared { get; set; }

        public bool? IsCollect { get; set; }

        [StringLength(10)]
        public string OriTransMode { get; set; }

        [StringLength(10)]
        public string DestTransMode { get; set; }

        [StringLength(10)]
        public string OriShipTerm { get; set; }

        [StringLength(10)]
        public string DestShipTerm { get; set; }

        public bool? IsOriCustClearance { get; set; }

        public bool? IsDestCustClearance { get; set; }

        public bool? IsOriAdvDuty { get; set; }

        public bool? IsDestAdvDuty { get; set; }

        [StringLength(6)]
        public string CreatedUserID { get; set; }

        public DateTime? CreatedDate { get; set; }

        [StringLength(6)]
        public string UpdatedUserID { get; set; }

        public DateTime? UpdatedDate { get; set; }

        public virtual SAQuotes SAQuotes { get; set; }
    }
}
