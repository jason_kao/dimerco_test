namespace ValuePlus.Domain.eSAM
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("SACustRelated")]
    public partial class SACustRelated
    {
        public int ID { get; set; }

        public int? CustomerID { get; set; }

        public int? QuoteCount { get; set; }

        public DateTime? CreatedDate { get; set; }

        [StringLength(10)]
        public string CreatedUserID { get; set; }

        public DateTime? UpdatedDate { get; set; }

        [StringLength(10)]
        public string UpdatedUserID { get; set; }

        [StringLength(10)]
        public string PID { get; set; }

        [StringLength(10)]
        public string Status { get; set; }
    }
}
