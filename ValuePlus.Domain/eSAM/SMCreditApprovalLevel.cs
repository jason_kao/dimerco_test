namespace ValuePlus.Domain.eSAM
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("SMCreditApprovalLevel")]
    public partial class SMCreditApprovalLevel
    {
        [Key]
        public int HQID { get; set; }

        [StringLength(10)]
        public string CreditLevelID { get; set; }

        [StringLength(2)]
        public string SettingType { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? RangeFrom { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? RangeTo { get; set; }

        [StringLength(6)]
        public string CreatedBy { get; set; }

        public DateTime? CreatedDate { get; set; }

        [StringLength(6)]
        public string UpdatedBy { get; set; }

        public DateTime? UpdatedDate { get; set; }
    }
}
