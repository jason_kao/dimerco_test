namespace ValuePlus.Domain.eSAM
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("SASerialNumber")]
    public partial class SASerialNumber
    {
        [Key]
        [StringLength(10)]
        public string StationCode { get; set; }

        public int? NextAIRCreditNo { get; set; }

        public int? NextAIRQuoteNo { get; set; }

        public int? NextSEACreditNo { get; set; }

        public int? NextSEAQuoteNo { get; set; }

        public int? NextWMSCreditNo { get; set; }

        public int? NextWMSQuoteNo { get; set; }

        public int? NextDASCreditNo { get; set; }

        public int? NextDASQuoteNo { get; set; }

        [StringLength(10)]
        public string CreatedUserID { get; set; }

        public DateTime? CreatedDate { get; set; }

        [StringLength(10)]
        public string UpdatedUserID { get; set; }

        public DateTime? UpdatedDate { get; set; }

        public int? NextMTSCreditNo { get; set; }

        public int? NextMTSQuoteNo { get; set; }

        public int? NextTMSCreditNo { get; set; }

        public int? NextTMSQuoteNo { get; set; }
    }
}
