﻿

var logger = {

	//please chagne stationId and webApi url
	stationId: "apac",
	webApiUrl: "http://localhost:58949",

	pageId: "",
	getUTC: function (date) {
		var year = "" + date.getUTCFullYear();
		var month = "" + (date.getUTCMonth() + 1); if (month.length == 1) { month = "0" + month; }
		var day = "" + date.getUTCDate(); if (day.length == 1) { day = "0" + day; }
		var hour = "" + date.getUTCHours(); if (hour.length == 1) { hour = "0" + hour; }
		var minute = "" + date.getUTCMinutes(); if (minute.length == 1) { minute = "0" + minute; }
		var second = "" + date.getUTCSeconds(); if (second.length == 1) { second = "0" + second; }
		return year + "/" + month + "/" + day + " " + hour + ":" + minute + ":" + second;
	},
	setPageId: function () {
		$.ajax({
			url: self.logger.webApiUrl + "/Log/GetPageId", success: function (result) {
				self.logger.pageId = result.id;
				console.log("PageId=" + result.id);
			}
		});
	},
	pushLog: function (originalUrl, type, loadSeconds) {

		var utcNow = self.logger.getUTC(new Date());
		var params = "stationId=" + self.logger.stationId + "&pageId=" + self.logger.pageId + "&url=" + originalUrl + "&type=" + type + "&loadSeconds=" + loadSeconds + "&createTime=" + utcNow;


		$.ajax({

			type: "POST", url: self.logger.webApiUrl + "/Log/Post?" + params, success: function (result) {
				console.log(result);
			}
		});
	},
	initAjax: function () {

		var ajaxTime;

		$(document).ajaxSend(function (event, jqxhr, settings) {

			ajaxTime = new Date().getTime();
		});

		$(document).ajaxComplete(function (event, xhr, settings) {
			var originalUrl = settings.url;
			if (originalUrl.indexOf(self.logger.webApiUrl) >= 0) {
				return;
			}

			var type = "Ajax";
			var loadedtime = (new Date().getTime() - ajaxTime) / 1000;

			console.log("Call Url=" + originalUrl + "=>ajax loadedtime=" + loadedtime);
			self.logger.pushLog(originalUrl, type, loadedtime);
		});
	},
	getLoadSeconds: function () {

		var loadSeconds = ((window.performance.timing.loadEventStart - window.performance.timing.navigationStart) / 1000)
		//var loadedSeconds = Math.ceil((end.getTime() - performance.timing.navigationStart) / 1000);
		//var loadedSeconds = Math.ceil((performance.now() - performance.timing.domComplete) / 1000);

		return loadSeconds;
	},

	pushPageLog: function () {

		var originalUrl = window.location.href;
		var type = "Page";
		var loadedtime = self.logger.getLoadSeconds();
		console.log("Page loadedtime=" + loadedtime);
		self.logger.pushLog(originalUrl, type, loadedtime);
	}

}

logger.setPageId();
logger.initAjax();
