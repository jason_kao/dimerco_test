﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using ValuePlus.ViewModel;
using System.Threading;
using System.Net;

namespace ValuePlus.Web.App_Infrastructure
{
    public static class ApiManager
    {

        public static async Task<T> Post<T>(string controller, string action, IDictionary<string, string> parameters)
        {
            //Joy to do : chack parameter has value
            return await callApi<T>("POST", controller, action, parameters);
        }

        public static async Task<T> Get<T>(string controller, string action, IDictionary<string, string> parameters = null)
        {
            return await callApi<T>("GET", controller, action, parameters);
        }

        private static async Task<T> callApi<T>(string method, string controller, string action, IDictionary<string, string> parameters)
        {

            var apiServer = "http://localhost:58949";
            var url = string.Format("{0}/{1}/{2}/", apiServer, controller, action);
            var req = HttpWebRequest.CreateHttp("http://www.google.com/");

            /*
            req.ServicePoint.BindIPEndPointDelegate =
  (s, ep, retries) =>
    new IPEndPoint(IPAddress.Parse("10.0.1.10"), 1234 + retries);
    */
            try
            {
                //don't conver in case some exception
                
                HttpResponseMessage response;
                var client = new HttpClient();
                
                if (method == "GET")
                {
                    if (parameters != null && parameters.Count()>0)
                    {
                        url = url + "?";
                        var i = 1;
                        foreach (var param in parameters)
                        {
                            url = url + param.Key + "=" + param.Value;
                            if (i < parameters.Count)
                            {
                                url = url + "&";
                            }
                            i++;
                        }
                    }
                    response = await client.GetAsync(url);
                }
                else
                {
                    var content = new FormUrlEncodedContent(parameters);
                    response = await client.PostAsync(url, content);
                }

                response.EnsureSuccessStatusCode();
                var responseBody = await response.Content.ReadAsStringAsync();
                var result = JsonConvert.DeserializeObject<T>(responseBody);

                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            
        }
    }


}