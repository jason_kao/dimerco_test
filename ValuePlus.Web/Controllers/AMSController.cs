﻿using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;
using ValuePlus.Domain;
using ValuePlus.ViewModel.LocaleChainVP;
using ValuePlus.Web.App_Infrastructure;

namespace ValuePlus.Web.Controllers
{
    public class AMSController : ControllerBase
    {
        // GET: AMS
        //public ActionResult Index()
        //{
        //    return View();
        //}

        public ActionResult Dimension()
        {
            return View();
        }


        public async Task<ActionResult> Search([DataSourceRequest] DataSourceRequest request)
        {
            var parameters = new Dictionary<string, string>();
            parameters.Add("Mode", "AEH");
            parameters.Add("SourceID", "82363");
            var list = await ApiManager.Get<IList<AEHAWBDimViewModel>>("Dimension", "GetSearchList", parameters);
            return Json(list.ToDataSourceResult(request));
        }

        //public async Task<ActionResult> Edit(string custid)
        //{
        //    var parameters = new Dictionary<string, string>();
        //    parameters.Add("hqId", custid);
        //    var data = await ApiManager.Get<AEHAWBDimViewModel>("Station", "GetData", parameters);

        //    return View(data);
        //}


        //[HttpPost]
        //public async Task<ActionResult> Edit(FormCollection formCollection)
        //{
        //    var parameters = formCollection.AllKeys.ToDictionary(k => k, v => formCollection[v]);
        //    var viewModel = await ApiManager.Post<StationSearchViewModel>("Station", "Save", parameters);
        //    this.BindModelState(viewModel);
        //    ViewBag.IsValid = ModelState.IsValid;
        //    return View(viewModel);
        //}


        //[HttpGet]
        //public async Task<JsonResult> GetCountries()
        //{
        //    var list = await ApiManager.Get<IList<SMCountry>>("Station", "GetCountries");
        //    var select = list.Select(p => new SelectListItem() { Value = p.HQID.ToString(), Text = p.CountryCode + "-" + p.CountryName }).ToList();

        //    return Json(select, JsonRequestBehavior.AllowGet);
        //}

        //[HttpGet]
        //public async Task<JsonResult> GetStates(string countryId)
        //{
        //    var parameters = new Dictionary<string, string>();
        //    parameters.Add("countryId", countryId);
        //    var list = await ApiManager.Get<IList<SMState>>("Station", "GetStates", parameters);
        //    var select = list.Select(p => new SelectListItem() { Value = p.HQID.ToString(), Text = p.StateCode + "-" + p.StateName }).ToList();

        //    return Json(select, JsonRequestBehavior.AllowGet);
        //}


        //[HttpGet]
        //public async Task<JsonResult> GetCities(string countryId,string stateId)
        //{
        //    var parameters = new Dictionary<string, string>();
        //    parameters.Add("countryId", countryId);
        //    parameters.Add("stateId", stateId);
        //    var list = await ApiManager.Get<IList<SMCity>>("Station", "GetCities", parameters);
        //    var select = list.Select(p => new SelectListItem() { Value = p.HQID.ToString(), Text = p.CityCode + "-" + p.CityName }).ToList();

        //    return Json(select, JsonRequestBehavior.AllowGet);
        //}

        //[HttpGet]
        //public async Task<JsonResult> GetCitiesByText(string text)
        //{
        //    var parameters = new Dictionary<string, string>();
        //    parameters.Add("text", text);
        //    var list = await ApiManager.Get<IList<SMCity>>("Station", "GetCitiesByText", parameters);
            
        //    return Json(list, JsonRequestBehavior.AllowGet);
        //}
    }

}