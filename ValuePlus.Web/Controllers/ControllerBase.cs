﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ValuePlus.ViewModel;

namespace ValuePlus.Web.Controllers
{
    public class ControllerBase: Controller
    {
        public void BindModelState(ViewModelBase vm)
        {
            foreach (var error in vm.Errors)
            {
                ModelState.AddModelError(error.Key, error.Value);
            }
        }
    }
}