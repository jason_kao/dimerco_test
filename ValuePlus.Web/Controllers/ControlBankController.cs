﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ValuePlus.Web.Controllers
{
    public class ControlBankController : Controller
    {
        // GET: ControlBank
        public ActionResult Index()
        {
            ViewBag.message = "123";
            return View();
        }

        [HttpPost]
        public async Task<ActionResult> Index(FormCollection formCollection)
        {
            var parameters = new Dictionary<string, string>();
            parameters.Add("Number", formCollection["Number"]);
            parameters.Add("txtNumber", formCollection["txtNumber"]);
            parameters.Add("DateFT", formCollection["ddlDateFT"]);
            parameters.Add("DateFrom", formCollection["DateFrom"]);
            parameters.Add("DateTo", formCollection["DateTo"]);
            var viewModel = await ApiManager.Get<ControlBankViewModel>("TestStation", "GetSeachList", parameters);
            return View(viewModel);
        }
    }
}